import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../shared/services/auth.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-remember-password-form",
    templateUrl: "./remember-password-form.component.html",
    styleUrls: ["./remember-password-form.component.scss"]
})
export class RememberPasswordFormComponent implements OnInit {
    browserLang = "en";
    formText: any;
    feedbackMsg: string;
    email: string;
    waiting: boolean = false;
    constructor(private authService: AuthService, private actionModalsService: ActionModalsService) {}

    ngOnInit() {
        this.browserLang = (navigator.language || navigator["userLanguage"]).slice(0, 2);
        this.formText = RPFCLanguages[this.browserLang];
    }

    sendLink() {
        this.waiting = true;
        let body: any = {};
        body.email = this.email;
        this.authService.sendResetRequest(body).subscribe(
            data => {
                this.waiting = false;
                this.feedbackMsg = this.formText.resetLinkSent;
            },
            error => {
                this.waiting = false;
                if (error.status === 404) {
                    this.feedbackMsg = this.formText.emailNotFoundMsg;
                }
            }
        );
    }
}

class RPFCLanguages {
    static en = {
        title: "Reset password",
        inputEmail: " Enter your email",
        rememberPasswordText: "Please enter your email, you will get an link to change your password",
        sendRememberRequest: "Send",
        login: "Login",
        signUp: "Sign Up",
        emailNotFoundMsg: "Email not registered",
        resetLinkSent: "You will recieve a link in your email, click it to change your password."
    };
    static de = {
        title: "Passwort zurücksetzen",
        inputEmail: " Email",
        rememberPasswordText: "Bitte geben Sie Ihre E-Mail-Adresse an. Eine Mail mit dem Link zur Änderung des Passwortes wird Ihnen zugesendet.",
        sendRememberRequest: "Senden",
        login: "Anmelden",
        signUp: "Registrieren",
        emailNotFoundMsg: "Email nicht registriert",
        resetLinkSent: "You will recieve a link in your email click it to change your password."
    };
    static es = {
        title: "Resetar contraseña",
        inputEmail: " Email",
        rememberPasswordText: "Por favor ingrese su email, se le enviará un link para que pueda cambiar su contraseña",
        sendRememberRequest: "Enviar",
        login: "Iniciar sesión",
        signUp: "Crear cuenta",
        emailNotFoundMsg: "Email no registrado",
        resetLinkSent: "You will recieve a link in your email click it to change your password."
    };
}
