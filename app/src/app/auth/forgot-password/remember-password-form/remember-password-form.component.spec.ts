import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RememberPasswordFormComponent } from './remember-password-form.component';

describe('RememberPasswordFormComponent', () => {
  let component: RememberPasswordFormComponent;
  let fixture: ComponentFixture<RememberPasswordFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RememberPasswordFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RememberPasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
