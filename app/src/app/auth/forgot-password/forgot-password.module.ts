import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
// Angular Material
import {MatInputModule, MatFormFieldModule , MatProgressBarModule } from '@angular/material';
import { RememberPasswordFormComponent } from './remember-password-form/remember-password-form.component';


const FORGOT_PASSWORD_ROUTES: Routes = [
    { path: '', component: RememberPasswordFormComponent}
];

@NgModule({
    declarations: [
        RememberPasswordFormComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(FORGOT_PASSWORD_ROUTES),
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatProgressBarModule
    ]
})
export class ForgotPasswordModule { }
