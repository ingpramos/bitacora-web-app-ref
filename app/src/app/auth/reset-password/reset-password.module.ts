import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SetNewPasswordFormComponent } from './set-new-password-form/set-new-password-form.component';
import { Routes, RouterModule } from '@angular/router';
// Angular Material
import {
  MatInputModule,
  MatFormFieldModule
} from '@angular/material';
;

const RESET_PASSWORD_ROUTES: Routes = [
	{ path: '', component: SetNewPasswordFormComponent }
];

@NgModule({
  declarations: [
      SetNewPasswordFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(RESET_PASSWORD_ROUTES),
    FormsModule,
    MatInputModule,
    MatFormFieldModule
  ]
})
export class ResetPasswordModule { }
