import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../../shared/services/auth.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-set-new-password-form",
    templateUrl: "./set-new-password-form.component.html",
    styleUrls: ["./set-new-password-form.component.scss"]
})
export class SetNewPasswordFormComponent implements OnInit {
    browserLang;
    resetFormText;
    obj: any = {};
    user_id;
    token;
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private actionModalService: ActionModalsService
    ) {}

    ngOnInit() {
        this.browserLang = (navigator.language || navigator["userLanguage"]).slice(0, 2);
        this.resetFormText = SNPFCLanguages[this.browserLang];
        this.activatedRoute.paramMap.subscribe(params => {
            this.user_id = params.get("user_id");
            this.token = params.get("token");
        });
    }

    resetOwnPassword() {
        this.obj.user_id = this.user_id;
        this.obj.token = this.token;
        this.authService.resetOwnPassword(this.obj).subscribe(
            data => {
                let title = this.resetFormText.passwordUpdatedSuccessfully;
                this.actionModalService.buildSuccessModal(title, "");
                this.router.navigateByUrl("/auth");
            },
            () => {
                let text = this.resetFormText.passwordUpdateError;
                this.actionModalService.alertError(text);
            }
        );
    }
}

class SNPFCLanguages {
    static en = {
        title: "Change Password",
        inputNewPassword: "Enter your new password",
        inputConfirmNewPassword: "Confirm your new password",
        updatePasswordButton: "Confirm",
        passwordUpdatedSuccessfully: "Password changed successfully",
        passwordUpdateError: "An error ocurred"
    };

    static de = {
        title: "Passwort ändern",
        inputNewPassword: "Neues Passwort eingeben",
        inputConfirmNewPassword: "Neues passwort bestätigen",
        updatePasswordButton: "bestätigen",
        passwordUpdatedSuccessfully: "Passwort erfolgreich geändert",
        passwordUpdateError: "Es ist ein Fehler aufgetreten"
    };

    static es = {
        title: "Cambiar contraseña",
        inputNewPassword: "Nueva contraseña",
        inputConfirmNewPassword: "Confirmar nueva contraseña",
        updatePasswordButton: "Confirmar",
        passwordUpdatedSuccessfully: "Login Exitoso",
        passwordUpdateError: "A ocurrido un error"
    };
}
