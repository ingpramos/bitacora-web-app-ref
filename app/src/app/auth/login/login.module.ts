import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from "@angular/router";
import { LoginPageComponent } from './login-page/login-page.component';

// Angular Material
import {
    MatInputModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
    MatFormFieldModule
} from '@angular/material';


const LOGIN_ROUTES: Routes = [
    { path: '', component: LoginPageComponent }
];

@NgModule({
    declarations: [
        LoginPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(LOGIN_ROUTES),
        FormsModule,
        MatInputModule,
        MatDialogModule,
        MatFormFieldModule
    ],
    providers: [
        { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
    ]
})
export class LoginModule { }
