import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';

@Component({
	selector: 'app-login-page',
	templateUrl: './login-page.component.html',
	styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

	loginText;
	browserLang: any;
	credentials: any = {};
	errorMessage: string;

	constructor(
		private authService: AuthService,
		private _sharedService : SharedService,
		private router: Router,
		public dialog: MatDialog
	) { }

	ngOnInit() {
		this.browserLang = (navigator.language || navigator['userLanguage']).slice(0, 2);
		this.loginText = LPCLanguages[this.browserLang];
	}

	login() {
		if (this.credentials.user_email && this.credentials.password) {
			this._sharedService.startLoading();
			this.authService.login(this.credentials).subscribe(() => {
				this.router.navigateByUrl('/app');
			}, (error) => {
				if (error.status === 400) {
					switch (error.error.error.errorType) {
						case 'not found':
							this._sharedService.stopLoading();
							this.errorMessage = this.loginText.loginError;
							break;
						case 'not confirmed':
							this._sharedService.stopLoading();
							this.errorMessage = this.loginText.pendingConfirmation;
							break;
						default:
							this._sharedService.stopLoading();
							this.errorMessage = this.loginText.loginGeneralError;
							break;
					}
				}
			});
		}
	}
}

class LPCLanguages {
	static en = {
		"title": "Login",
		"inputEmail": " Enter your email",
		"inputPassword": "Enter your password",
		"loginButton": "login",
		"rememberPasswordText": "Please enter your email, you will get an link to change your password",
		"sendRememberRequest": "Send",
		"loginSuccess": "Log in Successfully",
		"loginError": "Wrong Password or email",
		"pendingConfirmation": "Konto nicht bestätigt. Bitte überprüfen Sie Ihre E-Mail und bestätigen Sie Ihr Konto.",
		"loginGeneralError": "An error ocurred",
		"forgotPassword": "I forgot my password",
		"signUp": "Sign Up",
		"emailNotFoundMsg": "Email not registered",
		"resetLinkSent": "You will receive a link in your email click it to change your password."
	};
	static de = {
		"title": "Anmeldung",
		"inputEmail": " Email",
		"inputPassword": "Passwort",
		"loginButton": "Anmelden",
		"rememberPasswordText": "Bitte geben Sie Ihre E-Mail-Adresse an. Eine Mail mit dem Link zur Änderung des Passwortes wird Ihnen zugesendet.",
		"sendRememberRequest": "Senden",
		"loginSuccess": "Login erfolgreich",
		"loginError": "Falsches Passwort oder E-Mail",
		"pendingConfirmation": "Konto nicht bestätigt. Bitte überprüfen Sie Ihre E-Mail und bestätigen Sie Ihr Konto.",
		"loginGeneralError": "Es ist ein Fehler aufgetreten.",
		"forgotPassword": "Passwort vergessen",
		"signUp": "Registrieren",
		"emailNotFoundMsg": "Email nicht registriert",
		"resetLinkSent": "Sie erhalten in Ihrer E-Mail einen Link, indem Sie darauf klicken, um Ihr Passwort zu ändern."
	};
	static es = {
		"title": "Ingresar",
		"inputEmail": " Email",
		"inputPassword": "Contraseña",
		"loginButton": "Iniciar Sesión",
		"rememberPasswordText": "Por favor ingrese su email, se le enviará un link para que pueda cambiar su contraseña",
		"sendRememberRequest": "Enviar",
		"loginSuccess": "Login Exitoso",
		"loginError": "Email o Contraseña incorrecto",
		"pendingConfirmation": "Cuenta no confirmada, por favor revise su correo electrónico y confirme su cuenta.",
		"loginGeneralError": "Ha ocurrido un error",
		"forgotPassword": "Olvidé mi contraseña",
		"signUp": "Crear cuenta",
		"emailNotFoundMsg": "Email no registrado",
		"resetLinkSent": "Recibirá un enlace en su correo electrónico, haga clic para cambiar su contraseña."
	};
}

