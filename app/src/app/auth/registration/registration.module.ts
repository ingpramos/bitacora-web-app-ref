import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from "@angular/router";
import { SignUpPageComponent } from './sign-up-page/sign-up-page.component';
import { RegistrationComponent } from './registration.component';
// Angular Material
import {
	MatInputModule,
	MatNativeDateModule,
	MatSelectModule,
	MatFormFieldModule,
	MatProgressBarModule
} from '@angular/material';


const REGISTRATION_ROUTES: Routes = [
	{ path: '', component: RegistrationComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(REGISTRATION_ROUTES),
		FormsModule,
		MatInputModule,
		MatNativeDateModule,
		MatSelectModule,
		MatFormFieldModule,
		MatProgressBarModule
	],
	declarations: [
		RegistrationComponent,
		SignUpPageComponent
	]
})
export class RegistrationModule { }
