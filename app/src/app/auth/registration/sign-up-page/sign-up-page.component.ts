import { Component, OnInit } from "@angular/core";
import { CompanyService } from "../../../shared/services/company.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../../shared/services/auth.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
@Component({
    selector: "app-sign-up-page",
    templateUrl: "./sign-up-page.component.html",
    styleUrls: ["./sign-up-page.component.scss"]
})
export class SignUpPageComponent implements OnInit {
    browserLang: any;
    signUpFormText: any;
    newCompany: any = {};
    errorMessage: any;
    alerts: any;
    waiting: boolean = false;

    constructor(private router: Router, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        this.browserLang = (navigator.language || navigator["userLanguage"]).slice(0, 2);
        this.signUpFormText = SPCLanguage[this.browserLang];
        this.alerts = this.signUpFormText.alerts;
    }

    ngOnInit() {}
    createCompany() {
        this.waiting = true;
        this.newCompany["user_selected_language"] = this.browserLang;
        this.newCompany["company_id"] = this._companyService.generateCompanyID();
        this._companyService.createCompany(this.newCompany).subscribe(
            response => {
                let text = this.alerts.alertCompanyCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal("", text).then(result => {
                    this.router.navigateByUrl("/auth");
                });
            },
            error => {
                this.waiting = false;
                if (error.status === 403) return (this.errorMessage = this.alerts.alertCompanyAlreayExist);
                this.errorMessage = this.alerts.alertGeneralError;
            }
        );
    }
}

class SPCLanguage {
    static en = {
        title: "New Company Information",
        companyName: "Company Name",
        companyAddress: "Company Address",
        companyId: "Company Id",
        companyTaxId: {
            placeholder: "TIN/EIN Taxpayer ID",
            hint: "TIN/EIN requires between 16 and 34 characters.",
            errorRequired: "Required Field",
            errorMinLength: "your TIN/EIN should have at least 9 characters.",
            errorMaxLength: "your TIN/EIN should have maximum 16 characters."
        },
        companyVAT: "VAT identification number",
        companyCreationDate: "Registry Date",
        companyLogo: "Company Logo",
        companyCountry: "Country",
        companyCity: "City",
        companyZipCode: {
            placeholder: "Postal Code",
            hint: "Postal Code requires between 16 and 34 characters.",
            errorRequired: "Required Field",
            errorMinLength: "your Postal Code should have at least 5 characters.",
            errorMaxLength: "your Postal Code should have maximum 10 characters."
        },
        companyWebsite: "Website",
        companyTelephone: "Telephone",
        companyFax: "Fax",
        companyEmail: {
            placeholder: "Email",
            hint: "Postal Code requires between 16 and 34 characters.",
            errorRequired: "Required Field",
            errorEmail: "Invalid email address."
        },
        password: "Password",
        repeatPassword: {
            placeholder: "Repeat Password",
            hint: "El Código Postal tiene entre 5 y 10 carácteres.",
            errorRequired: "Required Field",
            errorPassword: "Passwords are not equal."
        },
        companyRegistrationCourt: "Registration Court",
        companyBusinessManagerName: "Business Manager",
        companyBusinessManagerLastName: "Business Manager Last Name",
        updateButton: "Update Company Information",
        uploadBrandImageButton: "Upload Image Brand",
        saveButton: "Sign Up",
        forgetButton: "Cancel",
        alertImageUploadedSuccessfully: "Image Uploaded Successfully",
        alertCompanyInfoSuccessfullyUpdated: "Company Information Successfully Updated",
        requiredMessage: "Fields marked * are required fields",
        requiredField: "Required Field",
        forgotPassword: "I forgot my password",
        login: "Login",
        alerts: {
            alertCompanyAlreayExist: "Company or user already registered.",
            alertGeneralError: "An error ocurred.",
            alertCompanyCreatedSuccessfully: "Company successfully registered, you will receive an email with a link to confirm your account."
        }
    };

    static de = {
        title: "Unternehmen registrieren",
        companyName: "Unternehmen",
        companyAddress: "Adresse",
        companyId: "Unternehmen Id",
        companyTaxId: {
            placeholder: "Steueridentifikationsnummer",
            hint: "Steuer-ID erfordert zwischen 9 und 16 Zeichen.",
            errorRequired: "Pflichtfeld",
            errorMinLength: "Ihre Steuer-ID sollte aus mindestens 9 Zeichen bestehen.",
            errorMaxLength: "Ihre Steuer-ID sollte aus maximal 16 Zeichen bestehen."
        },
        companyVAT: "USt-IdNr",
        companyCreationDate: "Registrierungsdatum",
        companyLogo: "Firmenlogo",
        companyCountry: "Land",
        companyCity: "Stadt",
        companyZipCode: {
            placeholder: "Postleitzahl",
            hint: "Postleitzahl erfordert zwischen 5 und 10 Zeichen.",
            errorRequired: "Pflichtfeld",
            errorMinLength: "Ihre Postleitzahl sollte aus mindestens 5 Zeichen bestehen.",
            errorMaxLength: "Ihre Postleitzahl sollte aus maximal 10 Zeichen bestehen."
        },
        companyWebsite: "Website",
        companyTelephone: "Telefon",
        companyFax: "Fax",
        companyEmail: {
            placeholder: "Email",
            hint: "Postleitzahl erfordert zwischen 5 und 10 Zeichen.",
            errorRequired: "Pflichtfeld",
            errorEmail: "ungültige E-Mail"
        },
        password: "Passwort",
        repeatPassword: {
            placeholder: "Passwort wiederholen",
            hint: "El Código Postal tiene entre 5 y 10 carácteres.",
            errorRequired: "Pflichtfeld",
            errorPassword: "Passwörter sind nicht gleich"
        },
        companyRegistrationCourt: "Registergericht",
        companyBusinessManagerName: "Geschäftsführer Vorname",
        companyBusinessManagerLastName: "Geschäftsführer Name",
        updateButton: "Aktualizieren",
        uploadBrandImageButton: "Firma marke hochladen",
        saveButton: "Konto erstellen",
        forgetButton: "Zurück",
        alertImageUploadedSuccessfully: "Bild Erfolgreich gespeichert",
        alertCompanyInfoSuccessfullyUpdated: "Firma Information Erfolgreich Aktualiziert",
        requiredMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder",
        requiredField: "Pflichtfeld",
        forgotPassword: "Passwort vergessen",
        login: "Anmelden",
        alerts: {
            alertCompanyAlreayExist: "Der Unthernehmen oder Benutzer ist schon registriert.",
            alertGeneralError: "Es ist ein Fehler aufgetreten.",
            alertCompanyCreatedSuccessfully: "Unternehmen erfolgreich registriert. Sie erhalten eine E-Mail mit einem Link zur Bestätigung Ihres Kontos."
        }
    };
    static es = {
        title: "Información de la Empresa",
        companyName: "Nombre de la Empresa",
        companyAddress: "Direccion",
        companyId: "Código",
        companyTaxId: {
            placeholder: "Identificación Tributaria",
            hint: " La Identificación Tributaria Postal tiene entre 9 y 16 carácteres.",
            errorRequired: "Campo requerido.",
            errorMinLength: "La Identificación Tributaria tiene al menos 9 carácteres.",
            errorMaxLength: "La Identificación Tributaria tiene máximo 16 carácteres."
        },
        companyVAT: "Número Exportador",
        companyCreationDate: "Fecha de Registro",
        companyLogo: "Logo Empresa",
        companyCountry: "Pais",
        companyCity: "Ciudad",
        companyZipCode: {
            placeholder: "Código Postal",
            hint: "El Código Postal tiene entre 5 y 10 carácteres.",
            errorRequired: "Campo requerido.",
            errorMinLength: "El Código Postal tiene al menos 5 carácteres.",
            errorMaxLength: "El Código Postal tiene máximo 10 carácteres."
        },
        companyWebsite: "Página Web",
        companyTelephone: "Teléfono",
        companyFax: "Fax",
        companyEmail: {
            placeholder: "Email",
            hint: "El Código Postal tiene entre 5 y 10 carácteres.",
            errorRequired: "Campo requerido.",
            errorEmail: "Correo electrónico no válido"
        },
        password: "Contraseña",
        repeatPassword: {
            placeholder: "Confirmar Contraseña",
            hint: "El Código Postal tiene entre 5 y 10 carácteres.",
            errorRequired: "Campo requerido.",
            errorPassword: "Las contraseña no son iguales"
        },
        companyRegistrationCourt: "Tribunal de Registro",
        companyBusinessManagerName: "Nombre del Gerente",
        companyBusinessManagerLastName: "Apellido del Gerente",
        updateButton: "Actualizar Informacion De La Empresa",
        uploadBrandImageButton: "Subir Membrete",
        saveButton: "Crear cuenta",
        forgetButton: "Cancelar",
        alertImageUploadedSuccessfully: "Imagen Actualizada Correctamente.",
        alertCompanyInfoSuccessfullyUpdated: "Informacion De la Empresa Actualizada Exitosamente.",
        requiredMessage: "Los campos con * son requeridos",
        requiredField: "Campo requerido.",
        forgotPassword: "Olvidé mi contraseña",
        login: "Iniciar sesión",
        alerts: {
            alertCompanyAlreayExist: "La Empresa o el Usuario ya estan registrados.",
            alertGeneralError: "Ha ocurrido un error.",
            alertCompanyCreatedSuccessfully: "Empresa creada exitosamente. recibirá un email con un link para confirmar su cuenta."
        }
    };
}
