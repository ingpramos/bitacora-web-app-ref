import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { ConfirmationPageComponent } from './confirmation-page/confirmation-page.component';

const CONFIRMATION_PAGE_ROUTES: Routes = [
    { path: '', component: ConfirmationPageComponent }
];

@NgModule({
    declarations: [ConfirmationPageComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(CONFIRMATION_PAGE_ROUTES),
    ]
})
export class ConfirmCompanyModule { }
