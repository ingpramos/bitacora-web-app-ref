import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from '../../../shared/services/auth.service';
@Component({
    selector: 'app-confirmation-page',
    templateUrl: './confirmation-page.component.html',
    styleUrls: ['./confirmation-page.component.scss']
})

export class ConfirmationPageComponent implements OnInit {

    browserLang: any = 'en';
    confirmationPageText: any = {};
    user_id: any;
    token: any;
    errorMessage: any = "";
    successMeassage:any="";

    constructor(
        private activatedRoute: ActivatedRoute,
        private authService : AuthService
    ) {

    }

    ngOnInit() {
        this.browserLang = (navigator.language || navigator['userLanguage']).slice(0, 2);
        this.confirmationPageText = CPCLanguages[this.browserLang];
        this.activatedRoute.paramMap.subscribe(params => {
            this.user_id = params.get('user_id');
            this.token = params.get('token');
            this.activateCompany();
        });
    }

    activateCompany() {
        let obj: any = {};
        obj.user_id = this.user_id;
        obj.token = this.token;
        this.authService.activateCompany(obj).subscribe(data => {
            this.successMeassage =  this.confirmationPageText.successMessage;
        }, (error) => {
            console.log(error);
            this.errorMessage = this.confirmationPageText.errorMessage;            
        })
    }

}

class CPCLanguages {
    static en = {
        "title": "Confirmation",
        "loginButton": "Login",
        "successMessage": "Company registration confirmed, please login to start using the platform.",
        "errorMessage":"Confirmation failed"
    };
    static de = {
        "title": "Bestätigung",
        "loginButton": "Anmelden",
        "successMessage": "Firmenregistrierung bestätigt, bitte melden Sie sich an, um die Plattform zu nutzen.",
        "errorMessage":"Bestätigung fehlgeschlagen"
    };
    static es = {
        "title": "Confirmación",
        "loginButton": "Iniciar Session",
        "successMessage": "Registro de la empresa confirmado, por favor inicie session para empezar a usar la plataforma.",
        "errorMessage":"Confirmación fallida"
    };
}
