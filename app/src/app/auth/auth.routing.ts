import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';



const AUTH_ROUTES: Routes = [
    {
        path: '', component: AuthComponent, children: [
            //login
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'login', loadChildren: './login/login.module#LoginModule'},
            // registation
            { path: 'sign-up', loadChildren: './registration/registration.module#RegistrationModule' },
            // reset-password
            { path: 'reset-password/:user_id/:token', loadChildren: './reset-password/reset-password.module#ResetPasswordModule' },
            // forgot-password
            { path: 'forgot-password', loadChildren: './forgot-password/forgot-password.module#ForgotPasswordModule' },
            // confirm-company
            { path: 'confirm-company/:user_id/:token', loadChildren: './confirm-company/confirm-company.module#ConfirmCompanyModule'},
        ]
    }
];

export const AuthRouting = RouterModule.forChild(AUTH_ROUTES);