import { HashLocationStrategy, LocationStrategy, CurrencyPipe, registerLocaleData} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayModule } from "@angular/cdk/overlay";
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AuthInterceptor } from './shared/interceptors/auth-interceptor';
import { LimitToPipe } from './shared/pipes/limit-to/limit-to.pipe';
import { SharedModule } from './shared/shared.module';
// adding the locales for german and spanish
import localeDE from '@angular/common/locales/de';
import localeES from '@angular/common/locales/es';
import { FormatDatePipe } from './shared/pipes/format-date/format-date.pipe';
registerLocaleData(localeDE, 'de');
registerLocaleData(localeES, 'es');



@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		BrowserAnimationsModule,
		routing,
		OverlayModule,
		SharedModule
	],
	declarations: [
		AppComponent,
	],
	providers: [
		FormatDatePipe,
		LimitToPipe,
		CurrencyPipe,
		{
			provide: LocationStrategy,
			useClass: HashLocationStrategy
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
