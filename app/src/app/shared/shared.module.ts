import { CommonModule} from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SpinnerComponent } from '../spinner/spinner.component';


@NgModule({
  declarations: [
    // Components
    SpinnerComponent
 ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports: [
    // Components
    SpinnerComponent,
  ]
})

export class SharedModule { }