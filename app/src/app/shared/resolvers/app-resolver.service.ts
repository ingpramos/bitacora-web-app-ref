import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CompanyService } from '../services/company.service';
import { AuthService } from '../services/auth.service';


@Injectable({
	providedIn: 'root'
})
export class AppResolverService implements Resolve<any> {

	constructor(
		private _companyService : CompanyService,
		private _authService : AuthService
	) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
		if (this._authService.isLoggedIn()) {
			return this._companyService.getSessionInfo(this._authService.getUserId());	
		}
	}

}
