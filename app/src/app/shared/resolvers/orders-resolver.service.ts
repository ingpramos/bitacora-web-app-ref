import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { OrdersService } from '../services/orders.service';
import { AuthService } from '../services/auth.service';
@Injectable({
    providedIn: 'root'
})
export class OrdersResolverService implements Resolve<any> {

    constructor(
        private _ordersService: OrdersService,
        private _authService: AuthService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

       return ;

    }
}
