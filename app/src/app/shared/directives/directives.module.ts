import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatCurrencyDirective } from './currency/format-currency.directive';

@NgModule({
  declarations: [
    FormatCurrencyDirective
  ],
  imports: [
    CommonModule
  ],
  exports:[
    FormatCurrencyDirective
  ]
})
export class DirectivesModule { }
