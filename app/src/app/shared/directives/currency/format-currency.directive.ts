import { Directive, ElementRef, HostListener} from "@angular/core";
import { CurrencyPipe } from "@angular/common";
import { SharedService } from "../../services/shared.service";
import { NgModel } from "@angular/forms";


@Directive({
    selector: "[appFormatCurrency]"
})
export class FormatCurrencyDirective {
    constructor(public model: NgModel, private elr: ElementRef, private _sharedService: SharedService, private _currencyPipe: CurrencyPipe) {}
    locale = this._sharedService.user_selected_language;

    @HostListener("keyup", ['$event']) onKeyUp(event) {
        let val = this.elr.nativeElement.value;
        //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
        if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
            return;
        }
        val = this.locale == "en" ? this.formattValueLocaleEN(val): this.formattValueLocaleDE(val);
        this.model.viewToModelUpdate(val);
        this.model.valueAccessor.writeValue(val);        
    }

    @HostListener("input", ['$event']) onInput(event) {
        let val = this.elr.nativeElement.value;
        //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
        if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
            return;
        }
        val = this.locale == "en" ? this.formattValueLocaleEN(val): this.formattValueLocaleDE(val);
        this.model.viewToModelUpdate(val);
        this.model.valueAccessor.writeValue(val);        
    }

    // @HostListener("blur", ['$event']) onBlur(event) {
    //     let val = this.elr.nativeElement.value;
    //     val = this.locale == "en" ? this.formattValueLocaleEN(val): this.formattValueLocaleDE(val);
    //     // this.model.viewToModelUpdate(this.model.value);
    //     // this.model.valueAccessor.writeValue(val);
    // }

    formattValueLocaleEN(val){
        val = val.replace(/,/g, "");
        this.elr.nativeElement.value = "";
        val += '';
        let x = val.split('.');
        let x1 = x[0];
        let x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        let formattedValue = x1 + x2;
        return formattedValue;
    }

    formattValueLocaleDE(val){
        val = val.replace(/\./g, "");
        this.elr.nativeElement.value = "";
        val += '';
        let x = val.split(',');
        let x1 = x[0];
        let x2 = x.length > 1 ? ',' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        let formattedValue = x1 + x2;
        return formattedValue;
    }
}
