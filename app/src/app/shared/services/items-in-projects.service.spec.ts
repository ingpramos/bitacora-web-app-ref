import { TestBed, inject } from '@angular/core/testing';

import { ItemsInProjectsService } from './items-in-projects.service';

describe('ItemsInProjectsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemsInProjectsService]
    });
  });

  it('should be created', inject([ItemsInProjectsService], (service: ItemsInProjectsService) => {
    expect(service).toBeTruthy();
  }));
});
