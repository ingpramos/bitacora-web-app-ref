import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class DeliveryNotesService {
    //Global Info App
    apiUrl: string = this._sharedService.apiUrl;
    sessionInfo;
    company_id;
    user_id;
    user_selected_language;
    pdfDeliveryNotesTexts: any = {};
    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
        this.getDocumentDeliveryNoteTexts().subscribe(data => (this.pdfDeliveryNotesTexts = data));
    }
    // to hold items collection
    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemsList(data) {
        this.itemsList.next(data);
    }
    // to hold invoices collection
    private deliveryNotesList = new BehaviorSubject<any>([]);
    currentDeliveryNotesList = this.deliveryNotesList.asObservable();
    updateDeliveryNotesList(data) {
        this.deliveryNotesList.next(data);
    }
    // to hold new invoice object
    private newDeliveryNotes = new BehaviorSubject<any>({});
    newDeliveryNoteToPush = this.newDeliveryNotes.asObservable();
    passNewDeliveryNote(data: any) {
        this.newDeliveryNotes.next(data);
    }

    getDeliveryNotes() {
        let url = `${this.apiUrl}/deliveryNotes?company_id=${this.company_id}&language=${this.user_selected_language}`;
        this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateDeliveryNotesList(data));
    }

    getDeliveryNotesByPattern(searchPattern) {
        let url = `${this.apiUrl}/deliveryNotesByPattern?company_id=${this.company_id}&language=${this.user_selected_language}&search_pattern=${searchPattern}`;
        this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateDeliveryNotesList(data));
    }

    getAvailableInvoices() {
        let url = `${this.apiUrl}/invoicesAvailable?company_id=${this.company_id}&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response => this.formatInvoices(response["data"])),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    createDeliveryNote(body) {
        let url = `${this.apiUrl}/deliveryNotes`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateDeliveryNote(body) {
        body.invoice_user_id = this.user_id;
        let url = `${this.apiUrl}/deliveryNotes`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteDeliveryNote(deliveryNote_id) {
        let url = `${this.apiUrl}/deliveryNotes?internal_document_id=${deliveryNote_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    closeDeliveryNote(invoice_id) {
        let url = `${this.apiUrl}/closeInvoice?internal_document_id=${invoice_id}`;
        return this.http.put(url, {}).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getCurrentDeliveryNoteNumber() {
        let url = `${this.apiUrl}/documentNumber?company_id=${this.company_id}&document_name=delivery_notes`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getDocumentDeliveryNoteTexts() {
        let url = `${this.apiUrl}/documentContext?company_id=${this.company_id}&language=${this.user_selected_language}&document_name=delivery_notes`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    formatInvoices(collection) {
        // collection of invoices without delivery note
        let formattedInvoices = {};
        formattedInvoices["invoices_numbers"] = [];
        formattedInvoices["internal_documents_ids"] = [];
        formattedInvoices["get_internal_document_id"] = method => {
            let documentIndex = formattedInvoices["invoices_numbers"].indexOf(method);
            let internal_document_id = formattedInvoices["internal_documents_ids"][documentIndex];
            return internal_document_id;
        };
        collection.forEach(obj => {
            formattedInvoices["invoices_numbers"].push(obj["invoice_number"]);
            formattedInvoices["internal_documents_ids"].push(obj["internal_document_id"]);
        });
        formattedInvoices["original_data"] = collection;
        return formattedInvoices;
    }
}
