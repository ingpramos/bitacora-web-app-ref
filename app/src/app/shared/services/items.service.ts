import { catchError, map, retry } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Subject } from "rxjs";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class ItemsService {
    //Global Info App
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    user_selected_language;
    apiUrl: string = this._sharedService.apiUrl;

    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
    }

    results: Subject<any> = new Subject<any>();
    // to hold the array of projects
    private itemsList = new BehaviorSubject<any[]>([]);
    itemsCurrentList = this.itemsList.asObservable();

    updateCurrentList(data) {
        this.itemsList.next(data);
    }

    getItems() {
        let url = `${this.apiUrl}/items?company_id=${this.company_id}`;
        return this.http.get(url).pipe(
            map((response: any) => response.data),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getItemGeneralCurrentStatus(item_id) {
        let url = `${this.apiUrl}/itemGeneralStatus?item_id=${item_id}&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map((response: any) => response.data),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getItemAverageCost(item_id) {
        let url = `${this.apiUrl}/itemAverageCost?item_id=${item_id}`;
        return this.http.get(url).pipe(
            map((response: any) => response.data),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getPendigsItems() {
        let url = `${this.apiUrl}/getPendingItems?company_id=${this.company_id}`;
        return this.http.get(url).pipe(
            map((response: any) => response.data),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getItemsByNameOrCode(searchPattern) {
        let url = `${this.apiUrl}/getItemsByNameOrCode?company_id=${this.company_id}&searchPattern=${searchPattern}`;
        return this.http.get(url).pipe(
            map((response: any) => response.data),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getItemsByFilter(filter_id) {
        let url = `${this.apiUrl}/getItemsByFilter?filter_id=${filter_id}`;
        return this.http.get(url).pipe(map((response: any) => response.data));
    }

    createItem(body) {
        body.company_id = this.company_id;
        let url = `${this.apiUrl}/items?company_id=${this.company_id}`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateItem(body) {
        let item_id = body.item_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/items?company_id=${this.company_id}&item_id=${item_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteItem(item_id) {
        let url = `${this.apiUrl}/items?company_id=${this.company_id}&item_id=${item_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            catchError(this._configService.handleError)
        );
    }

    checkIfItemsExists(item_codes_array) {
        let url = `${this.apiUrl}/checkIfItemsExists?company_id=${this.company_id}`;
        return this.http.post(url, item_codes_array).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertItemsFromExcel(itemsCollection) {
        let url = `${this.apiUrl}/createItems?company_id=${this.company_id}`;
        return this.http.post(url, itemsCollection).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    labelItemsAlreadyInList(itemsAlreadyInList, itemsFromSearch) {
        itemsFromSearch.forEach((ifs: any) => {
            ifs["is_checked"] = false;
            ifs["is_already_in_list"] = false;
            ifs["item_amount_to_show"] = this._sharedService.parseToLocaleString(ifs["item_amount"]).trim();
            ifs["item_buy_price_to_show"] = this._sharedService.parseToLocaleString(ifs["item_buy_price"]).trim();
            ifs["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(ifs["item_sell_price"]).trim();
            itemsAlreadyInList.forEach(iail => {
                if (ifs["item_code"] === iail["item_code"]) {
                    ifs["is_already_in_list"] = true;
                }
            });
        });
    }

    calculateItemTotalActivitiesCostModel(collection) {
        let costModel = {};
        let totalActivitiesCost = collection.reduce((memo: any, obj: any) => {
            return memo + parseFloat(obj["activity_cost_per_unit"]);
        }, 0);
        costModel["totalActivitiesCost"] = totalActivitiesCost.toFixed(2);
        collection.forEach(obj => {
            if (totalActivitiesCost > 0) {
                obj["activity_percentage"] = ((obj.activity_cost_per_unit / totalActivitiesCost) * 100).toFixed(2);
            } else {
                obj["activity_percentage"] = 0;
            }
        });
        costModel["activitiesResume"] = collection;
        return costModel;
    }
}
