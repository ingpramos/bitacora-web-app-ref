import { TestBed, inject } from '@angular/core/testing';

import { OcPdfService } from './oc-pdf.service';

describe('OcPdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OcPdfService]
    });
  });

  it('should be created', inject([OcPdfService], (service: OcPdfService) => {
    expect(service).toBeTruthy();
  }));
});
