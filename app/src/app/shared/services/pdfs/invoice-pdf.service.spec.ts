import { TestBed, inject } from '@angular/core/testing';

import { InvoicePdfService } from './invoice-pdf.service';

describe('InvoicePdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoicePdfService]
    });
  });

  it('should be created', inject([InvoicePdfService], (service: InvoicePdfService) => {
    expect(service).toBeTruthy();
  }));
});
