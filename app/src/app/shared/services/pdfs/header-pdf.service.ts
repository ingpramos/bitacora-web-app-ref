import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class HeaderPdfService {
    constructor() {}

    buildPropertyValueTableWithTitle(collection) {
        let table = {};
        table["widths"] = ["40%", "60%"]; // key property to force the table to take 100% of the availabe place
        table["headerRows"] = 1;
        table["body"] = this.buildPropertyValueWithTitleTableBody(collection);
        return table;
    }

    buildPropertyValueTableWithoutTitle(collection) {
        let table = {};
        table["widths"] = ["30%", "70%"]; // key property to force the table to take 100% of the availabe place
        (table["headerRows"] = 1), (table["body"] = this.buildPropertyValueTableBody(collection));
        return table;
    }

    buildPropertyValueWithTitleTableBody(collection) {
        let tableBody = [];
        collection.forEach(object => {
            let array = [
                { text: object["property"], fontSize: 10, margin: [0, 2, 0, 0] },
                { text: object["value"], fontSize: 10, alignment: "right", margin: [0, 2, 0, 0] }
            ];
            tableBody.push(array);
        });
        tableBody.splice(0, 1);
        let title = collection[0]["property"];
        let formattedTitle = [{ text: title.toUpperCase(), alignment: "center", colSpan: 2, bold: true, fontSize: 12, fillColor: "#eeeeee" }, {}];
        tableBody.unshift(formattedTitle);
        return tableBody;
    }

    buildPropertyValueTableBody(collection) {
        let tableBody = [];
        collection.forEach(object => {
            let array = [{ text: object["property"], fontSize: 10 }, { text: object["value"], fontSize: 10, alignment: "right" }];
            tableBody.push(array);
        });
        return tableBody;
    }

    buildClientTable(array) {
        let table = {};
        table["headerRows"] = 1;
        table["body"] = this.buildClientColumnBody(array);
        table["widths"] = "100%";
        return table;
    }

    buildClientColumnBody(array) {
        let tableBody = [];
        array.forEach(value => {
            let row = [{ text: value, fontSize: 10 }];
            tableBody.push(row);
        });
        return tableBody;
    }

    buildProviderTable(array) {
        let table = {};
        table["headerRows"] = 1;
        table["body"] = this.buildProviderColumnBody(array);
        table["widths"] = "*";
        return table;
    }

    buildProviderColumnBody(array) {
        let tableBody = [];
        array.forEach(value => {
            let row = [{ text: value, fontSize: 10 }];
            tableBody.push(row);
        });
        return tableBody;
    }

    buildDocumentHeader(sessionInfo, image) {
        let info = sessionInfo.company_name + " - " + sessionInfo.company_zip_code + " - " + sessionInfo.company_city;
        let obj = {};
        obj["columns"] = [{ width: "50%", text: info, fontSize: 10 }, { image: image, alignment: "right", width: 210 }];
        return obj;
    }

    getPaginator(currentPage, pageCount){
        return { text:currentPage +" / "+ pageCount, alignment: "right", style:"small", margin: [80,30,60,1]}
    }
}
