import { TestBed, inject } from '@angular/core/testing';

import { BodyPdfService } from './body-pdf.service';

describe('BodyPdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BodyPdfService]
    });
  });

  it('should be created', inject([BodyPdfService], (service: BodyPdfService) => {
    expect(service).toBeTruthy();
  }));
});
