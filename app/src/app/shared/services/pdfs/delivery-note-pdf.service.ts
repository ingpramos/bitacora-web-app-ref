import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";
import { DeliveryNotePdfFunctions } from "./delivery-note-pdf-functions";
import { DeliveryNotesService } from "../delivery-notes.service";
import { FooterPdfService } from "./footer-pdf.service";
import { HeaderPdfService } from "./header-pdf.service";
import { ColumnsNamesService } from "../columns-names.service";
import { BodyPdfService } from "./body-pdf.service";
import * as pdfMake from "pdfmake/build/pdfmake.js";
import * as pdfFonts from "pdfmake/build/vfs_fonts.js";
import { InternalDocumentsService } from "../internal-documents.service";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

declare var require: any;

@Injectable({
    providedIn: "root"
})

export class DeliveryNotePdfService {
    private readonly pdfFonts: any;
    pdfMake: any;
    sessionInfo;
    styles = this._bodyPdfService.buildStyles(); // styles for pdf document
    constructor(
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _deliveryNotesService: DeliveryNotesService,
        private _headerPdfService: HeaderPdfService,
        private _bodyPdfService: BodyPdfService,
        private _footerPdfService: FooterPdfService,
        private _columnsNamesService: ColumnsNamesService
    ) {
        this.pdfMake = pdfMake;
        this._sharedService.sessionInfo.subscribe(data => (this.sessionInfo = data));
    }

    buildDocument(invoiceObj) {
        let cl = this._sharedService.user_selected_language;
        let documentHeader = InvoicePdfLanguages[cl].header;
        let documentBody = InvoicePdfLanguages[cl].body;
        let infoInvoice = DeliveryNotePdfFunctions.getDeliveryNoteData(documentHeader, invoiceObj, cl);
        let infoDocumentCreator = DeliveryNotePdfFunctions.getDocumentCreatorData(documentHeader, invoiceObj);
        let infoClient = DeliveryNotePdfFunctions.getClientData(invoiceObj);
        let infoConditions = DeliveryNotePdfFunctions.getInvoiceConditionsData(documentBody, invoiceObj);
        let image = this._sharedService.imgCodeBase64;
        let pdfDeliveryNotesTexts = this._deliveryNotesService.pdfDeliveryNotesTexts;
        let tableColumnsObj = this._columnsNamesService.getCurrentCustomTableColumnsObject();

        let dd = {
            pageSize: "A4",
            pageMargins: [60, 65, 60, 125],
            defaultStyle: {
                columnGap: 25
            },
            styles: this.styles,
            header:this._headerPdfService.getPaginator,
            content: [
                this._headerPdfService.buildDocumentHeader(this.sessionInfo, image),
                { text: "\n" },
                {
                    columns: [
                        [
                            {
                                table: this._headerPdfService.buildClientTable(infoClient),
                                layout: "noBorders"
                            },
                            { text: "\n" },
                            {
                                table: this._headerPdfService.buildPropertyValueTableWithoutTitle(infoDocumentCreator),
                                layout: "noBorders"
                            }
                        ],
                        [
                            { text: "\n" },
                            {
                                table: this._headerPdfService.buildPropertyValueTableWithTitle(infoInvoice),
                                layout: "headerLineOnly"
                            }
                        ]
                    ]
                },
                { text: "\n" }, // new line
                { text: pdfDeliveryNotesTexts["document_context_text"] || documentBody["documentContextText"], fontSize: 10 }, // new line
                { text: "\n" }, // new line
                {
                    table: {
                        headerRows: 1,
                        widths: ["5%", "70%", "25%"],
                        body: DeliveryNotePdfFunctions.buildMainTable(tableColumnsObj, this._idocsService.getItemsOfCurrentDocument("pdf"))
                    },
                    layout: "noBorders"
                },
                { text: "\n" }, // new line
                { text: "\n" },
                this._bodyPdfService.buildPropertyValueTableWithoutTitle(infoConditions)
            ],
            footer: [this._footerPdfService.buildLine(), this._footerPdfService.buildCompanyInfoFooter(), this._footerPdfService.buildIBANSFooter()]
        };

        return dd;
    }
}

class InvoicePdfLanguages {
    static en = {
        header: {
            deliveryNoteTitle: "Delivery Note",
            deliveryNoteNumber: "Delivery Note Nr. :",
            invoiceNumber: "Invoice Nr. :",
            ocNumber: "Confirmation Nr. :",
            deliveryNoteDate: "Date :",
            clientNumber: "Client Nr. : ",
            taxID: "Tax ID Number",
            vatID: "VAT ID",
            companyContactPerson: "Contact : ",
            companyContactPersonTelephone: "Telephone : ",
            companyContactPersonEmail: "Email :"
        },
        body: {
            documentContextText: "We thank you for the good cooperation and deliver the following goods as agreed :",
            totalWithoutVAT: "Total without VAT",
            vatFrom: "VAT from ",
            totalWithVAT: "Total with VAT",
            paymentMethod: "Payment Method :",
            paymentConditions: "Payment Conditions :",
            wayOfDelivery: "Delivery Way :",
            deliveryConditions: "Delivery Conditions :",
            ourProjects: "Our Project Numbers : ",
            deliveryDate: "Delivery Date : ",
            clientNumber: ""
        }
    };

    static de = {
        header: {
            deliveryNoteTitle: "Lieferschein",
            deliveryNoteNumber: "Liefer-Nr. :",
            invoiceNumber: "Rechnungs-Nr. :",
            ocNumber: "Auftr.-Nr. :",
            deliveryNoteDate: "Datum : ",
            clientNumber: "Kd.-Nr. :",
            taxID: "ST.-Nr. : ",
            vatID: "USt.-IdNr. : ",
            companyContactPerson: "Bearbeiter : ",
            companyContactPersonTelephone: "Telefon :",
            companyContactPersonEmail: "Email :"
        },
        body: {
            documentContextText: "Wir bedanken uns für die gute Zusammenarbeit und liefern Ihnen wie vereinbart folgende Waren :",
            totalWithoutVAT: "Total exkl. MwSt.",
            vatFrom: "MwSt von",
            totalWithVAT: "Total inkl. MwSt.",
            paymentMethod: "Zahlungsart :",
            paymentConditions: "Zahlungsbedingungen :",
            wayOfDelivery: "Lieferart :",
            deliveryConditions: "Lieferbedingungen :",
            ourProjects: "Unsere Auftrags. -Nr. ",
            deliveryDate: "Liefertermin : ",
            clientNumber: ""
        }
    };

    static es = {
        header: {
            deliveryNoteTitle: "Nota de Entrega",
            deliveryNoteNumber: "Nr. Entrega",
            invoiceNumber: "Factura :",
            ocNumber: "Nr. OT :",
            deliveryNoteDate: "F. Entrega :",
            clientNumber: "Cód. Cliente : ",
            taxID: "Tax ID Number",
            vatID: "VAT number",
            companyContactPerson: "Contacto : ",
            companyContactPersonTelephone: "Teléfono : ",
            companyContactPersonEmail: "Email :"
        },
        body: {
            documentContextText: "Le agradecemos la buena cooperación y entregamos los siguientes productos según lo acordado :",
            totalWithoutVAT: "Total sin IVA",
            vatFrom: "IVA de ",
            totalWithVAT: "Total Con IVA",
            paymentMethod: "Metodo de Pago:",
            paymentConditions: "Condiciones de Pago :",
            wayOfDelivery: "Forma de Entrega :",
            deliveryConditions: "Condiciones de Entrega :",
            ourProjects: "Numero de Proyectos : ",
            deliveryDate: "Fecha de Entrega : ",
            clientNumber: ""
        }
    };
}
