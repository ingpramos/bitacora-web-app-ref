import { BodyPdfService } from "./body-pdf.service";

export class OfferPdfFunctions {
    static buildStyles() {
        return {
            tableHeaderCentered: { italic: true, alignment: "center", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
            tableHeaderLeft: { italic: true, alignment: "left", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
            rowLeft: { italic: true, alignment: "left", fontSize: 9, margin: [0, 3, 0, 3] },
            rowCentered: { italic: true, alignment: "center", fontSize: 9, margin: [0, 3, 0, 3] },
            small: { fontSize: 10 },
            big: { fontSize: 13, bold: true }
        };
    }

    static getClientData(offerObj) {
        let infoClient = [
            // data used to build the right column of the document tableHeader
            offerObj.client_name || "XXXXX-XXXXX",
            offerObj.client_address || "XXXXX-XXXXX",
            offerObj.client_zip_code + " - " + offerObj.client_city || "XXXXX-XXXXX"
        ];
        return infoClient;
    }

    static getContactData(documentHeader, offerObj) {
        let infoContact = [
            // Data used to build the contact column of de document
            { property: documentHeader.clientContactPerson, value: offerObj.contact || "XXXXX-XXXXX" },
            { property: documentHeader.clientContactPersonTelephone, value: offerObj.contact_telephone || "XXXXX-XXXXX" },
            { property: documentHeader.companyContactPerson, value: offerObj.offer_user.user_name + " " + offerObj.offer_user.user_last_name || "XXXXX-XXXXX" },
            { property: documentHeader.companyContactPersonTelephone, value: offerObj.offer_user.user_telephone || "XXXXX-XXXXX" }
        ];
        return infoContact;
    }

    static getOfferData(documentHeader, offerObj, cl) {
        let infoOffer = [
            // Data used to build the left column of de document tableHeader
            { property: documentHeader.offerTitle, value: offerObj.offer_number || "XXXXX-XXXXX" },
            { property: documentHeader.offerNumber, value: offerObj.offer_number || "XXXXX-XXXXX" },
            { property: documentHeader.offerDate, value: BodyPdfService.formatDate(new Date(), cl) },
            { property: documentHeader.expirationDate, value: BodyPdfService.formatDate(offerObj.offer_expiration_date, cl) || "XXXXX-XXXXX" },
            { property: documentHeader.offeredDeliveryDate, value: BodyPdfService.formatDate(offerObj.offered_delivery_date, cl) || "XXXXX-XXXXX" }
        ];
        return infoOffer;
    }

    static getOfferConditionsData(documentConditions, offerObj) {
        let infoOffer = [
            // Data used to build the left column of de document tableHeader
            { property: documentConditions.wayOfDelivery, value: offerObj.delivery_method_name || "XXXXX-XXXXX" },
            { property: documentConditions.deliveryConditions, value: offerObj.delivery_condition_description || "XXXXX-XXXXX" },
            { property: documentConditions.paymentMethod, value: offerObj.payment_method_name || "XXXXX-XXXXX" },
            { property: documentConditions.paymentConditions, value: offerObj.payment_condition_description || "XXXXX-XXXXX" }
        ];
        return infoOffer;
    }

    static buildMainTable(columnsObj, collection) {
        let i = 0;
        let formattedTable = [];
        let tableHeader = [
            { text: columnsObj.item_position, style: "tableHeaderCentered" },
            { text: columnsObj.item_name, style: "tableHeaderLeft" },
            { text: columnsObj.item_amount, style: "tableHeaderCentered" },
            { text: columnsObj.item_sell_price, style: "tableHeaderCentered" },
            { text: columnsObj.item_total_price, style: "tableHeaderCentered" }
        ];

        formattedTable.push(tableHeader);
        collection.forEach(obj => {
            i = i + 1;
            let row = [];
            row.push({ text: i, style: "rowCentered" });
            row.push({ text: buildItemDetails(obj), style: "rowLeft" });
            row.push({ text: obj["item_amount_formatted"] || 0, style: "rowCentered" });
            row.push({ text: obj["item_sell_price_formatted"] || 0, style: "rowCentered" });
            row.push({ text: obj["item_total_price_formatted"] || 0, style: "rowCentered" });
            formattedTable.push(row);
        });

        function buildItemDetails(obj) {
            let itemDetails = "";
            if (obj.item_code) itemDetails += obj.item_code + "\n";
            if (obj.item_name) itemDetails += obj.item_name + "\n";
            if (obj.item_material && obj.item_material != "NONE") itemDetails += obj.item_material + "\n";
            if (obj.item_details && obj.item_details != "NONE") itemDetails += obj.item_details + "\n";
            if (obj.item_in_internal_document_details) itemDetails += obj.item_in_internal_document_details + "\n";
            return itemDetails;
        }

        return formattedTable;
    }
}
