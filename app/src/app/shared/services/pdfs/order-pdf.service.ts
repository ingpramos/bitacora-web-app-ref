import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SharedService } from "../shared.service";
import { OrderPdfFunctions } from "./order-pdf.functions";
import { OrdersService } from "../orders.service";
import { HeaderPdfService } from "./header-pdf.service";
import { FooterPdfService } from "./footer-pdf.service";
import { ColumnsNamesService } from "../columns-names.service";
import * as pdfMake from "pdfmake/build/pdfmake.js";
import * as pdfFonts from "pdfmake/build/vfs_fonts.js";
import { BodyPdfService } from "./body-pdf.service";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
    providedIn: "root"
})

export class OrderPdfService {
    
    pdfMake: any;
    itemsOfCurrentOrder: any;
    styles = this._bodyPdfService.buildStyles(); // styles for pdf document
    sessionInfo;

    constructor(
        private _sharedService: SharedService,
        private _ordersService: OrdersService,
        private _headerPdfService: HeaderPdfService,
        private _bodyPdfService: BodyPdfService,
        private _footerPdfService: FooterPdfService,
        private _columnsNamesService: ColumnsNamesService
    ) {
        this.pdfMake = pdfMake;
        this._sharedService.sessionInfo.subscribe(data => (this.sessionInfo = data)); // holds user and company information
    }

    buildDocument(orderObj) {
        let cl = this._sharedService.user_selected_language;
        let documentHeader = OPLanguages[cl].header;
        let documentBody = OPLanguages[cl].body;
        let infoHeaderLeft = OrderPdfFunctions.getHeaderLeftData(documentHeader, orderObj, cl);
        let providerInfo = OrderPdfFunctions.getProviderData(orderObj);
        let infoContact = OrderPdfFunctions.getContactsData(documentHeader, orderObj);
        let pdfOrdersTexts = this._ordersService.pdfOrdersTexts; // obj with context-text-, disclaimer, etc
        let tableColumnsObj = this._columnsNamesService.getCurrentCustomTableColumnsObject();
        let image = this._sharedService.imgCodeBase64;
        const orderPdfObservable = new Observable(observer => {
            let docDefinition = {
                pageSize: "A4",
                pageMargins: [80, 65, 60, 125],
                styles: this.styles,
                defaultStyle: {
                    columnGap: 25
                },
                header: this._headerPdfService.getPaginator,
                content: [
                    this._headerPdfService.buildDocumentHeader(this.sessionInfo, image),
                    { text: "\n" },
                    {
                        columns: [
                            [
                                { width: "50%", table: this._headerPdfService.buildProviderTable(providerInfo), layout: "noBorders" },
                                { width: "50%", text: "\n" },
                                { table: this._headerPdfService.buildPropertyValueTableWithoutTitle(infoContact), layout: "noBorders" }
                            ],
                            [
                                { width: "50%", text: "\n" },
                                { table: this._headerPdfService.buildPropertyValueTableWithTitle(infoHeaderLeft), layout: "headerLineOnly" }
                            ]
                        ]
                    },
                    { text: "\n" },
                    { text: pdfOrdersTexts["document_context_text"] || documentBody["documentContextText"], fontSize: 10 },
                    { text: "\n" },
                    {
                        table: {
                            headerRows: 1,
                            widths: ["5%", "50%", "12%", "15%", "18%"],
                            body: OrderPdfFunctions.buildMainTable(tableColumnsObj, this._ordersService.getItemsInOrder("pdf"))
                        },
                        layout: "noBorders"
                    },
                    { text: "\n" },
                    this._bodyPdfService.buildTotalTable(documentBody, orderObj),
                    { text: "\n" },
                    { text: "\n" }
                ],
                footer: [this._footerPdfService.buildLine(), this._footerPdfService.buildCompanyInfoFooter(), this._footerPdfService.buildIBANSFooter()]
            };

            observer.next(docDefinition);
        });

        return orderPdfObservable;
    }
}

class OPLanguages {
    static en = {
        header: {
            position: "",
            provider: "Provider : ",
            providerContactPerson: "Contact : ",
            providerContactPersonTelephone: "Telephone : ",
            providerContactPersonEmail: "Email : ",
            orderConfirmedDate: "PO Date : ",
            companyContactPerson: "Asesor : ",
            companyContactPersonTelephone: "Telephone : ",
            orderTitle: "Purchase Order",
            orderNumber: "PO Nr. : ",
            offerNumber: "Offer Nr. : ",
            deliveryDate: "Delivery Date : "
        },
        body: {
            documentContextText: "Hereby we order the following listed positions based on your offer:",
            totalWithoutVAT: "Total without VAT",
            vatFrom: "VAT from ",
            totalWithVAT: "Total with VAT",
            offerNumber: "Offer Number : ",
            ourProjects: "Our Project Numbers : "
        }
    };

    static de = {
        header: {
            position: "",
            provider: "Empfänger : ",
            providerContactPerson: "Durch : ",
            providerContactPersonTelephone: "Telefon : ",
            providerContactPersonEmail: "Email : ",
            orderConfirmedDate: "Best.-Dat. : ",
            companyContactPerson: "Bearbeiter : ",
            companyContactPersonTelephone: "Telefon : ",
            orderTitle: "Bestellung",
            orderNumber: "Best.-Nr. : ",
            offerNumber: "Ang.-Nr. : ",
            deliveryDate: "Liefertermin : "
        },
        body: {
            documentContextText: "Hiermit bestellen wir die folgende Artikel.",
            totalWithoutVAT: "Total exkl. MwSt.",
            vatFrom: "MwSt von",
            totalWithVAT: "Total inkl. MwSt.",
            offerNumber: "Angebotsnummer : ",
            ourProjects: "Unsere Auftrags. -Nr. "
        }
    };

    static es = {
        header: {
            position: "",
            provider: "Proveedor : ",
            providerContactPerson: "Contacto : ",
            providerContactPersonTelephone: "Teléfono : ",
            providerContactPersonEmail: "Email :",
            orderConfirmedDate: "F. Pedido : ",
            companyContactPerson: "Contacto : ",
            companyContactPersonTelephone: "Teléfono : ",
            orderTitle: "Orden de Compra",
            orderNumber: "Nr. Orden : ",
            offerNumber: "Nr. Oferta :",
            deliveryDate: "F. Entrega : "
        },
        body: {
            documentContextText: "Nos permitimos confirmar el pedido de las siguientes articulos acorde con su oferta :",
            totalWithoutVAT: "Total sin IVA",
            vatFrom: "IVA de ",
            totalWithVAT: "Total Con IVA",
            offerNumber: "Número de Oferta : ",
            ourProjects: "Numero de Proyectos : "
        }
    };
}
