import { TestBed, inject } from '@angular/core/testing';

import { HeaderPdfService } from './header-pdf.service';

describe('HeaderPdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeaderPdfService]
    });
  });

  it('should be created', inject([HeaderPdfService], (service: HeaderPdfService) => {
    expect(service).toBeTruthy();
  }));
});
