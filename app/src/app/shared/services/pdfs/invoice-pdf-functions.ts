import { BodyPdfService } from "./body-pdf.service";

export class InvoicePdfFunctions {

	static getClientData(invoiceObj) {
		let infoClient = [ // data used to build the right column of the document tableHeader
			invoiceObj.client_name || "XXXXX-XXXXX",
			(invoiceObj.client_address || "") + " , " + (invoiceObj.client_zip_code || "XXXXX-XXXXX"),
			(invoiceObj.client_city || "XXXXX-XXXXX")  + " - " + (invoiceObj.client_country || "XXXXX-XXXXX")
		];
		return infoClient;
	}

	static getDocumentCreatorData(documentHeader, invoiceObj) {
		let contactInfo = [
			{ property:"", value: ""}, // white space
			{ property: documentHeader.companyContactPerson, value: invoiceObj.user_name + " " + invoiceObj.user_last_name || "XXXXX-XXXXX" },
			{ property: documentHeader.companyContactPersonTelephone, value: invoiceObj.user_telephone || "XXXXX-XXXXX" },
			{ property: documentHeader.companyContactPersonEmail, value: invoiceObj.user_email || "XXXXX-XXXXX" }
		];
		return contactInfo;
	}

	static getInvoiceData(documentHeader, invoiceObj, cl) {
		let infoInvoice = [ // Data used to build the left column of de document tableHeader
			{ property: documentHeader.invoiceTitle, value: "" },
			{ property: documentHeader.invoiceNumber, value: invoiceObj.invoice_number || "XXXXX-XXXXX" },
			{ property: documentHeader.orderConfirmationNumber, value: invoiceObj.order_confirmation_number || "XXXXX-XXXXX" },
			{ property: documentHeader.invoiceDate, value: BodyPdfService.formatDate(invoiceObj.invoice_emition_date, cl) },
			{ property: documentHeader.deliveryDate, value: BodyPdfService.formatDate(invoiceObj.invoice_delivery_date, cl) },
			//{ property: documentHeader.clientNumber, value: invoiceObj.client_number || "XXXXX-XXXXX" },
		];
		return infoInvoice;
	}

	static getInvoiceConditionsData(documentConditions, invoiceObj) {
		let infoOffer = [ // Data used to build the left column of de document tableHeader
			{ property: documentConditions.wayOfDelivery, value: invoiceObj.delivery_method_name || "XXXXX-XXXXX" },
			{ property: documentConditions.deliveryConditions, value: invoiceObj.delivery_condition_description || "XXXXX-XXXXX" },
			{ property: documentConditions.paymentMethod, value: invoiceObj.payment_method_name || "XXXXX-XXXXX" },
			{ property: documentConditions.paymentConditions, value: invoiceObj.payment_condition_description || "XXXXX-XXXXX" },
		];
		return infoOffer;
	}

	static buildMainTable(columnsObj, collection) {
		let i = 0;
		let formattedTable = [];
		let tableHeader = [
			{ text: columnsObj.item_position, style: "tableHeaderCentered" },
			{ text: columnsObj.item_name, style: "tableHeaderLeft" },
			{ text: columnsObj.item_amount, style: "tableHeaderCentered" },
			{ text: columnsObj.item_sell_price, style: "tableHeaderCentered" },
			{ text: columnsObj.item_total_price, style: "tableHeaderCentered" },
		];		

		formattedTable.push(tableHeader);

		collection.forEach((obj) => {
			i = i + 1;
			let row = [];
			row.push({ text: i, style: "rowCentered" });
			row.push({ text: buildItemDetails(obj), style: "rowLeft" });
			row.push({ text: obj["item_amount_formatted"] || 0, style: "rowCentered" });
			row.push({ text: obj["item_sell_price_formatted"] || 0, style: "rowCentered" });
			row.push({ text: obj["item_total_price_formatted"] || 0, style: "rowCentered" });
			formattedTable.push(row);
		});

		function buildItemDetails(obj) {
			let itemDetails = "";
			if (obj.item_code) itemDetails += obj.item_code + "\n";
			if (obj.item_name) itemDetails += obj.item_name + "\n";
			if (obj.item_material && obj.item_material != "NONE") itemDetails += obj.item_material + "\n";
			if (obj.item_details && obj.item_details != "NONE") itemDetails += obj.item_details + "\n";
			if (obj.item_in_internal_document_details) itemDetails += obj.item_in_internal_document_details + "\n";
			return itemDetails;
		}

		return formattedTable;
	}

}
