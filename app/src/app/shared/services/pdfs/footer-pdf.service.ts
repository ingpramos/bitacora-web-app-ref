import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";

@Injectable({
    providedIn: "root"
})
export class FooterPdfService {
    sessionInfo;
    footerFields;
    cl;
    constructor(private _sharedService: SharedService) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.cl = this.sessionInfo.user_selected_language;
            this.footerFields = FooterFieldsLaguages[this.cl];
        });
    }

    buildLine() {
        let obj = { text: "____________________________________________________________________________________", bold: true, margin: [80,1,60, 1] };
        return obj;
    }

    // new data model
    buildFooterFirstColumn(sessionInfo) {
        let column = {};
        column["table"] = {};
        column["table"]["body"] = [
            [{ text: sessionInfo.company_name, fontSize: 7 }],
            [{ text: sessionInfo.company_address, fontSize: 7 }],
            [{ text: sessionInfo.company_zip_code + " - " + sessionInfo.company_city, fontSize: 7 }]
        ];
        column["layout"] = "noBorders";
        return column;
    }

    buildFooterSecondColumn(sessionInfo) {
        let column = {};
        column["table"] = {};
        column["table"]["body"] = [
            [{ text: this.footerFields.companyTelephone + sessionInfo.company_telephone, fontSize: 7 }],
            [{ text: sessionInfo.company_email, fontSize: 7 }],
            [{ text: sessionInfo.company_website, fontSize: 7 }]
        ];
        if (sessionInfo.company_fax) {
            let fax = [{ text: this.footerFields.companyFax + sessionInfo.company_fax, fontSize: 7 }];
            column["table"]["body"].splice(1, 0, fax);
        }
        column["layout"] = "noBorders";
        return column;
    }
    buildFooterThirdColumn(sessionInfo) {
        let column = {};
        column["table"] = {};
        column["table"]["body"] = [
            [
                {
                    text: this.footerFields.businessManager + `${sessionInfo.company_business_manager_name} ${sessionInfo.company_business_manager_last_name}`,
                    fontSize: 7
                }
            ],
            [{ text: this.footerFields.registrationCourt + sessionInfo.company_registration_court, fontSize: 7 }],
            [{ text: this.footerFields.taxIdNumber + sessionInfo.company_tax_identification_number, fontSize: 7 }]
        ];
        if (sessionInfo.company_vat_identification_number) {
            let vat_id = [{ text: this.footerFields.vatIdNumber + sessionInfo.company_vat_identification_number, fontSize: 7 }];
            column["table"]["body"].splice(3, 0, vat_id);
        }
        column["layout"] = "noBorders";
        return column;
    }

    buildCompanyInfoFooter() {
        let obj = {};
        obj["margin"] = [80,1,60,1];
        obj["columns"] = [
            this.buildFooterFirstColumn(this.sessionInfo),
            this.buildFooterSecondColumn(this.sessionInfo),
            this.buildFooterThirdColumn(this.sessionInfo)
        ];
        obj["layout"] = "noBorders";
        return obj;
    }

    buildIBANSDataModel(ibans) {
        let body = {};
        body["banksName"] = [];
        body["ibans"] = [];
        body["bics"] = [];
        if (ibans.length > 0) {
            ibans.forEach(obj => {
                let bank_name = { text: obj["bank_name"], fontSize: 6 };
                let iban = { text: "IBAN: " + obj["iban"], fontSize: 6 };
                let bic = { text: "BIC: " + obj["bic"], fontSize: 6 };
                body["banksName"].push([bank_name]);
                body["ibans"].push([iban]);
                body["bics"].push([bic]);
            });
        } else {
            let bank_name = { text: "Bank Name XXXXXXXX", fontSize: 6 };
            let iban = { text: "IBAN: XXXXXXXXX ", fontSize: 6 };
            let bic = { text: "BIC: XXXXXXXXXXX ", fontSize: 6 };
            body["banksName"].push([bank_name]);
            body["ibans"].push([iban]);
            body["bics"].push([bic]);
        }
        return body;
    }

    buildIBANSFooter() {
        let ibans = this.sessionInfo.ibans || [];
        const data = this.buildIBANSDataModel(ibans);
        let obj = {};
        obj["margin"] = [80,1,60,1];
        obj["columns"] = [data["banksName"], data["ibans"], data["bics"]];
        //obj["layout"] = "noBorders";
        return obj;
    }

    getPaginator(currentPage, pageCount){
        return { text:currentPage +" / "+ pageCount, alignment: "right", style:"small", margin: [80,1,60,1]}
    }

}

class FooterFieldsLaguages {
    static en = {
        companyTelephone: "Telephone: ",
        companyFax: "Fax: ",
        businessManager: "CEO: ",
        registrationCourt: "Registration Court: ",
        taxIdNumber: "TIN: ",
        vatIdNumber: "VAT: "
    };
    static de = {
        companyTelephone: "Telefon: ",
        companyFax: "Fax: ",
        businessManager: "Geschäftsführer: ",
        registrationCourt: "Registergericht: ",
        taxIdNumber: "ST.-Nr.: ",
        vatIdNumber: "USt-IdNr.: "
    };
    static es = {
        companyTelephone: "Teléfono: ",
        companyFax: "Fax: ",
        businessManager: "Gerente: ",
        registrationCourt: "Registro: ",
        taxIdNumber: "NIT: ",
        vatIdNumber: "Exp. ID: "
    };
}
