import { BodyPdfService } from "./body-pdf.service";
export class OrderPdfFunctions {
	constructor() { }
	static buildStyles() {
		return {
			sheetHeaderTableBold: { bold: true, fontSize: 10 },
			sheetHeaderTableInfo: { alignment: "right", fontSize: 10 },
			tableHeader: { italic: true, alignment: "center", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
			row_left: { italic: true, alignment: "left", fontSize: 9, margin: [0, 3, 0, 3] },
			row_centered: { italic: true, alignment: "center", fontSize: 9, margin: [0, 3, 0, 3] },
			small: { fontSize: 10 },
			big: { fontSize: 13, bold: true }
		};
	}
	

	static buildMainTable(columnsObj, collection) {
		let i = 0;
		let formattedTable = [];
		let tableHeader = [
			{ text: columnsObj.item_position, style: "tableHeaderCentered" },
			{ text: columnsObj.item_name, style: "tableHeaderLeft" },
			{ text: columnsObj.item_amount, style: "tableHeaderCentered" },
			{ text: columnsObj.item_buy_price, style: "tableHeaderCentered" },
			{ text: columnsObj.item_total_price, style: "tableHeaderCentered" },
		];
		
		formattedTable.push(tableHeader);
		collection.forEach((obj) => {
			i = i + 1;
			let row = [];
			row.push({ text: i, style: "rowCentered" });
			row.push({ text: buildItemDetails(obj), style: "rowLeft" });
			row.push({ text: obj["item_amount_formatted"] || 0, style: "rowCentered" });
			row.push({ text: obj["item_buy_price_formatted"] || 0, style: "rowCentered" });
			row.push({ text: obj["item_total_price_formatted"] || 0, style: "rowCentered" });
			formattedTable.push(row);
		});

		function buildItemDetails(obj) {
			let itemDetails = "";
			if (obj.item_order_number) itemDetails += obj.item_order_number + "\n";
			if (obj.item_material && obj.item_material != "NONE") itemDetails += obj.item_material + "\n";
			if (obj.item_in_order_details && obj.item_in_order_details != "NONE") itemDetails += obj.item_in_order_details + "\n";
			if (obj.item_details && obj.item_details != "NONE") itemDetails += obj.item_details + "\n";
			itemDetails += obj.item_name + "\n";
			itemDetails += obj.item_code + "\n"
			return itemDetails;
		}

		return formattedTable;
	}

	static buildRightHeader(image, arrayOfValues) {
		let rh = [];
		arrayOfValues.forEach((value) => {
			if (value) {
				let rHRow = [{ text: value, style: "small", alignment: "left" }];
				rh.push(rHRow);
			}
		});
		if (image) {
			rh.unshift([image]);
		}
		else {
			var cn = [{ text: "company name", style: "big" }];
			rh.unshift(cn);
		}
		return rh;
	}

	static buildLeftHeaderRow(bold, value): any {
		let sheetHeaderRow = [];
		let sheetHeaderItemBold = { text: bold, style: "sheetHeaderTableBold" };
		let sheeHeaderItemInfo = { text: value, style: "sheetHeaderTableInfo" };
		sheetHeaderRow.push(sheetHeaderItemBold);
		sheetHeaderRow.push(sheeHeaderItemInfo);
		return sheetHeaderRow;
	}


	// function to process the body of the table, it will join all the formated rows done by buildSheetHeaderRow
	static buildLeftHeader(collection) {
		var sheetHeader = [];
		collection.forEach((obj: any) => {
			sheetHeader.push(this.buildLeftHeaderRow(obj.property, obj.value));
		});
		sheetHeader.splice(0, 1);
		let title = `${collection[0]["property"]} : ${collection[0]["value"]} `;
		let formattedTitle = [{ text: title, alignment: "center", colSpan: 2, bold: true }, {}];
		sheetHeader.unshift(formattedTitle);
		return sheetHeader;
	}

	static getProviderData(orderObj) {
		let infoProvider = [ // data used to build the right column of the document tableHeader
			orderObj.provider_name || "XXXXX-XXXXX",
			orderObj.provider_address || "XXXXX-XXXXX"
		];
		return infoProvider;
	}

	static getContactsData(documentHeader, orderObj) {
		let contactInfo = [
			{ property: documentHeader.providerContactPerson, value: orderObj.contact  || "XXXXX-XXXXX" },
			{ property: documentHeader.providerContactPersonTelephone, value: orderObj.contact_telephone || "XXXXX-XXXXX" },
			{ property: documentHeader.companyContactPerson, value: (orderObj.user_name + " " + orderObj.user_last_name) || "XXXXX-XXXXX" },
			{ property: documentHeader.companyContactPersonTelephone, value: orderObj.user_telephone || "XXXXX-XXXXX" }
		];
		return contactInfo;
	}


	static getHeaderLeftData(documentHeader, orderObj, cl) { // user, company, language and order
		let infoHeaderLeft = [ // Data used to build the left column of de document tableHeader
			{ property: documentHeader.orderNumber, value: orderObj.order_number || "XXXXX-XXXXX" },
			{ property: documentHeader.orderConfirmedDate, value: BodyPdfService.formatDate(new Date(), cl) },
			{ property: documentHeader.deliveryDate, value: BodyPdfService.formatDate(orderObj.order_delivery_date, cl) || "XXXXX-XXXXX" },
		];
		if (typeof orderObj.offer_number == "string" && orderObj.offer_number.length > 0) {
			let offer_number_row = { property: documentHeader.offerNumber, value: orderObj.offer_number || "XXXXX-XXXXX" };
			infoHeaderLeft.splice(1, 0, offer_number_row);
		}
		let leftHeaderTitle = { property: documentHeader.orderTitle, value: "" };
		infoHeaderLeft.unshift(leftHeaderTitle);
		return infoHeaderLeft;
	}

}

