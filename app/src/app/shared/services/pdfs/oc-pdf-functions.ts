import { BodyPdfService } from "./body-pdf.service";

export class OCPdfFunctions {
    constructor() {}

    static getClientData(ocObj) {
        let infoClient = [
            // data used to build the right column of the document tableHeader
            ocObj.client_name || "XXXXX-XXXXX",
            (ocObj.client_address || "") + " , " + (ocObj.client_zip_code || "XXXXX-XXXXX"),
            (ocObj.client_city || "XXXXX-XXXXX") + " - " + (ocObj.client_country || "XXXXX-XXXXX")
        ];
        return infoClient;
    }

    static getContactData(documentHeader, ocObj) {
        // user, company, language and order
        let infoContact = [
            // Data used to build the left column of de document tableHeader
            { property: documentHeader.clientContactPerson, value: ocObj.contact || "XXXXX-XXXXX" },
            { property: documentHeader.clientContactPersonTelephone, value: ocObj.contact_telephone || "XXXXX-XXXXX" },
            {
                property: documentHeader.companyContactPerson,
                value: ocObj.order_confirmation_user.user_name + " " + ocObj.order_confirmation_user.user_last_name || "XXXXX-XXXXX"
            },
            { property: documentHeader.companyContactPersonTelephone, value: ocObj.order_confirmation_user.user_telephone || "XXXXX-XXXXX" }
        ];
        return infoContact;
    }

    static getOCData(documentHeader, ocObj, cl) {
        let infoOC = [
            // Data used to build the left column of de document tableHeader
            { property: documentHeader.ocTitle, value: ocObj.order_confirmation_number || "XXXXX-XXXXX" },
            { property: documentHeader.ocNumber, value: ocObj.order_confirmation_number || "XXXXX-XXXXX" },
            { property: documentHeader.ocDate, value: BodyPdfService.formatDate(new Date(), cl) },
            //{ property: documentHeader.clientNumber, value: ocObj.client_number || 'XXXXX-XXXXX' },
            { property: documentHeader.offeredDeliveryDate, value: BodyPdfService.formatDate(ocObj.offered_delivery_date, cl) || "XXXXX-XXXXX" }
        ];
        let offer_number = ocObj.offer_number;
        if (typeof offer_number == "string" && offer_number.length > 0) {
            const offer_number_row = { property: documentHeader.offerNumber, value: ocObj.offer_number };
            infoOC.splice(2, 0, offer_number_row);
        }
        return infoOC;
    }

    static getOCConditionsData(documentConditions, ocObj) {
        let infoOC = [
            // Data used to build the left column of de document tableHeader
            { property: documentConditions.wayOfDelivery, value: ocObj.delivery_method_name || "XXXXX-XXXXX" },
            { property: documentConditions.deliveryConditions, value: ocObj.delivery_condition_description || "XXXXX-XXXXX" },
            { property: documentConditions.paymentMethod, value: ocObj.payment_method_name || "XXXXX-XXXXX" },
            { property: documentConditions.paymentConditions, value: ocObj.payment_condition_description || "XXXXX-XXXXX" }
        ];
        return infoOC;
    }

    static buildMainTable(columnsObj, collection) {
        let i = 0;
        let formattedTable = [];
        let tableHeader = [
            { text: columnsObj.item_position, style: "tableHeaderCentered" },
            { text: columnsObj.item_code, style: "tableHeaderLeft" },
            { text: columnsObj.item_amount, style: "tableHeaderCentered" },
            { text: columnsObj.item_sell_price, style: "tableHeaderCentered" },
            { text: columnsObj.item_total_price, style: "tableHeaderCentered" }
        ];

        formattedTable.push(tableHeader);

        collection.forEach(obj => {
            i = i + 1;
            let row = [];
            row.push({ text: i, style: "rowCentered" });
            row.push({ text: buildItemDetails(obj), style: "rowLeft" });
            row.push({ text: obj["item_amount_formatted"] || 0, style: "rowCentered" });
            row.push({ text: obj["item_sell_price_formatted"] || 0, style: "rowCentered" });
            row.push({ text: obj["item_total_price_formatted"] || 0, style: "rowCentered" });
            formattedTable.push(row);
        });
        function buildItemDetails(obj) {
            let itemDetails = "";
            if (obj.item_code) itemDetails += obj.item_code + "\n";
            if (obj.item_name) itemDetails += obj.item_name + "\n";
            if (obj.item_material && obj.item_material != "NONE") itemDetails += obj.item_material + "\n";
            if (obj.item_details && obj.item_details != "NONE") itemDetails += obj.item_details + "\n";
            if (obj.item_in_internal_document_details) itemDetails += obj.item_in_internal_document_details + "\n";
            return itemDetails;
        }

        return formattedTable;
    }
}
