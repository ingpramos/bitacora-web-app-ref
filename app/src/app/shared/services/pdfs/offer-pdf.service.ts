import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";
import { OfferPdfFunctions } from "./offer-pdf-functions";
import { OffersService } from "../offers.service";
import { HeaderPdfService } from "./header-pdf.service";
import { FooterPdfService } from "./footer-pdf.service";
import { ColumnsNamesService } from "../columns-names.service";
import { BodyPdfService } from "./body-pdf.service";
import * as pdfMake from "pdfmake/build/pdfmake.js";
import * as pdfFonts from "pdfmake/build/vfs_fonts.js";
import { InternalDocumentsService } from "../internal-documents.service";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

declare var require: any;

@Injectable({
    providedIn: "root"
})
export class OfferPdfService {
    private readonly pdfFonts: any;
    pdfMake: any;
    sessionInfo;
    styles = this._bodyPdfService.buildStyles(); // styles for pdf document
    
    constructor(
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _offersService: OffersService,
        private _headerPdfService: HeaderPdfService,
        private _bodyPdfService: BodyPdfService,
        private _footerPdfService: FooterPdfService,
        private _columnsNamesService: ColumnsNamesService
    ) {
        this.pdfMake = pdfMake;
        this._sharedService.sessionInfo.subscribe(data => (this.sessionInfo = data)); // holds user and company information
    }

    buildDocument(offerObj) {
        let cl = this._sharedService.user_selected_language;
        let documentHeader = OfferPdfLanguages[cl].header;
        let documentBody = OfferPdfLanguages[cl].body;
        let infoClient = OfferPdfFunctions.getClientData(offerObj);
        let infoContact = OfferPdfFunctions.getContactData(documentHeader, offerObj);
        let infoOffer = OfferPdfFunctions.getOfferData(documentHeader, offerObj, cl);
        let pdfOffersTexts = this._offersService.pdfOffersTexts;
        let infoConditions = OfferPdfFunctions.getOfferConditionsData(documentBody, offerObj);
        let image = this._sharedService.imgCodeBase64;
        // building columns for pdf
        let tableColumnsObj = this._columnsNamesService.getCurrentCustomTableColumnsObject();
        let dd = {
            pageSize: "A4",
            pageMargins: [80, 65, 60, 125],
            defaultStyle: {
                columnGap: 25
            },
            styles: this.styles,
            header:this._headerPdfService.getPaginator,
            content: [
                this._headerPdfService.buildDocumentHeader(this.sessionInfo, image),
                { text: "\n" },
                {
                    columns: [
                        [
                            // left part of the document
                            { width: "50%", table: this._headerPdfService.buildClientTable(infoClient), layout: "noBorders" },
                            { width: "50%", text: "\n" },
                            { width: "50%", table: this._headerPdfService.buildPropertyValueTableWithoutTitle(infoContact), layout: "noBorders" }
                        ],
                        [  
                           // right part of the document
                            { width: "50%", text: "\n" },
                            { width: "50%", table: this._headerPdfService.buildPropertyValueTableWithTitle(infoOffer), layout: "headerLineOnly" }
                        ]
                    ]
                },
                { text: "\n" }, // new line
                { text: pdfOffersTexts["document_context_text"] || documentBody["documentContextText"], fontSize: 10 }, // new line
                { text: "\n" }, // new line
                {
                    table: {
                        headerRows: 1,
                        widths: ["5%", "50%", "12%", "15%", "18%"],
                        body: OfferPdfFunctions.buildMainTable(tableColumnsObj, this._idocsService.getItemsOfCurrentDocument("pdf"))
                    },
                    layout: "noBorders"
                },
                { text: "\n" },
                this._bodyPdfService.buildTotalTable(documentBody, offerObj),
                { text: "\n" },
                this._bodyPdfService.buildPropertyValueTableWithoutTitle(infoConditions)
            ],
            footer: [this._footerPdfService.buildLine(), this._footerPdfService.buildCompanyInfoFooter(), this._footerPdfService.buildIBANSFooter()]
        };

        return dd;
    }
}

class OfferPdfLanguages {
    static en = {
        header: {
            position: "",
            client: "Provider : ",
            clientContactPerson: "Contact : ",
            clientContactPersonTelephone: "Telephone : ",
            companyContactPerson: "Asesor : ",
            companyContactPersonTelephone: "Telephone : ",
            companyContactPersonEmail: "E-mail : ",
            offerTitle: "Offer",
            offerNumber: "Number : ",
            offerDate: "Offer date : ",
            clientNumber: "Client : ",
            clientTelephone: "Telephone : ",
            clientEmail: "e-mail : ",
            expirationDate:"Expire at : ",
            offeredDeliveryDate: "Delivery date :",
            taxID: "Tax ID Number",
            vatID: "VAT ID"
        },
        body: {
            documentContextText: "Thank you for your request. In accordance with our terms of delivery and delivery we offer you.",
            totalWithoutVAT: "Total without VAT",
            vatFrom: "VAT from ",
            totalWithVAT: "Total with VAT",
            paymentMethod: "Payment Method :",
            paymentConditions: "Payment Conditions :",
            wayOfDelivery: "Delivery Way :",
            deliveryConditions: "Delivery Conditions :"
        }
    };

    static de = {
        header: {
            position: "",
            client: "Empfänger : ",
            clientContactPerson: "Durch :",
            clientContactPersonTelephone: "Telefon : ",
            companyContactPerson: "Bearbeiter : ",
            companyContactPersonTelephone: "Telefon : ",
            companyContactPersonEmail: "E-mail : ",
            offerTitle: "Angebot",
            offerNumber: "Ang.-Nr. :",
            offerDate: "Angebotsdatum : ",
            clientNumber: "Kunde : ",
            clientTelephone: "Telefon : ",
            clientEmail: "E-mail : ",
            expirationDate:"Gultig bis : ",
            offeredDeliveryDate: "Liefertermin :",
            taxID: "ST.-Nr. : ",
            vatID: "USt.-IdNr. : "
        },
        body: {
            documentContextText: "Vielen Dank für Ihre Anfrage. Gemäss unseren Vekauf- und Lieferbedingungen bieten wir Ihnen an.",
            totalWithoutVAT: "Total exkl. MwSt.",
            vatFrom: "MwSt von",
            totalWithVAT: "Total inkl. MwSt.",
            paymentMethod: "Zahlungsart :",
            paymentConditions: "Zahlungsbedingungen :",
            wayOfDelivery: "Lieferart :",
            deliveryConditions: "Lieferbedingungen :"
        }
    };

    static es = {
        header: {
            position: "",
            client: "Proveedor : ",
            clientContactPerson: "Contacto : ",
            clientContactPersonTelephone: "Teléfono : ",
            companyContactPerson: "Asesor : ",
            companyContactPersonTelephone: "Teléfono : ",
            companyContactPersonEmail: "E-mail : ",
            offerTitle: "Oferta",
            offerNumber: "Número : ",
            offerDate: "Fecha de oferta : ",
            clientNumber: "Cliente : ",
            clientTelephone: "Teléfono : ",
            clientEmail: "e-mail : ",
            expirationDate:"Valida hasta : ",
            offeredDeliveryDate: "F. Entrega :",
            taxID: "Tax ID Number",
            vatID: "VAT number"
        },
        body: {
            documentContextText:
                "Gracias por su solicitud. De acuerdo con nuestros términos de entrega y entrega le ofrecemos los siguientes productos y/o servicios",
            totalWithoutVAT: "Total sin IVA",
            vatFrom: "IVA de ",
            totalWithVAT: "Total con IVA",
            paymentMethod: "Metodo de pago :",
            paymentConditions: "Condiciones de pago :",
            wayOfDelivery: "Forma de entrega :",
            deliveryConditions: "Condiciones de entrega :"
        }
    };
}
