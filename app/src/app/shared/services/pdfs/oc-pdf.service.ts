import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";
import { HeaderPdfService } from "./header-pdf.service";
import { FooterPdfService } from "./footer-pdf.service";
import { ColumnsNamesService } from "../columns-names.service";
import { OCPdfFunctions } from "./oc-pdf-functions";
import * as pdfMake from "pdfmake/build/pdfmake.js";
import * as pdfFonts from "pdfmake/build/vfs_fonts.js";
import { BodyPdfService } from "./body-pdf.service";
import { ConfirmationsService } from "../confirmations.service";
import { InternalDocumentsService } from "../internal-documents.service";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Injectable({
    providedIn: "root"
})
export class OcPdfService {
    private readonly pdfFonts: any;
    pdfMake: any;
    itemsOfCurrentOrder: any;
    codeBase64 = ""; // url64 for image
    styles = this._bodyPdfService.buildStyles();
    sessionInfo;

    constructor(
		private _sharedService: SharedService,
		private _idocsService: InternalDocumentsService,
        private _headerPdfService: HeaderPdfService,
        private _footerPdfService: FooterPdfService,
        private _bodyPdfService: BodyPdfService,
        private _columnsNamesService: ColumnsNamesService,
        private _orderConfirmationsService: ConfirmationsService
    ) {
        this.pdfMake = pdfMake;
        this._sharedService.sessionInfo.subscribe(data => (this.sessionInfo = data)); // holds user and company information
    }

    buildDocument(ocObj) {
        let cl = this._sharedService.user_selected_language;
        let documentHeader = OCPdfLanguages[cl].header;
        let documentBody = OCPdfLanguages[cl].body;
        let infoClient = OCPdfFunctions.getClientData(ocObj);
        let infoContact = OCPdfFunctions.getContactData(documentHeader, ocObj);
        let infoOC = OCPdfFunctions.getOCData(documentHeader, ocObj, cl);
        let infoConditions = OCPdfFunctions.getOCConditionsData(documentBody, ocObj);
        let image = this._sharedService.imgCodeBase64;
        // building columns for pdf
        let pdfOCTexts = this._orderConfirmationsService.pdfOCTexts;
        let tableColumnsObj = this._columnsNamesService.getCurrentCustomTableColumnsObject();
        let dd = {
            pageSize: "A4",
            pageMargins: [80, 65, 60, 125],
            defaultStyle: {
                columnGap: 25
            },
            styles: this.styles,
            header: this._headerPdfService.getPaginator,
            content: [
                this._headerPdfService.buildDocumentHeader(this.sessionInfo, image),
                { text: "\n" },
                {
                    columns: [
                        [
                            { table: this._headerPdfService.buildClientTable(infoClient), layout: "noBorders" },
                            { text: "\n" },
                            { table: this._headerPdfService.buildPropertyValueTableWithoutTitle(infoContact), layout: "noBorders" }
                        ],
                        [{ text: "\n" }, { table: this._headerPdfService.buildPropertyValueTableWithTitle(infoOC), layout: "headerLineOnly" }]
                    ]
                },
                { text: "\n" },
                { text: pdfOCTexts["document_context_text"] || documentBody["documentContextText"], fontSize: 10 },
                { text: "\n" },
                {
                    table: {
                        headerRows: 1,
                        widths: ["5%", "50%", "12%", "15%", "18%"],
                        body: OCPdfFunctions.buildMainTable(tableColumnsObj, this._idocsService.getItemsOfCurrentDocument("pdf"))
                    },
                    layout: "noBorders"
                },
                { text: "\n" },
                this._bodyPdfService.buildTotalTable(documentBody, ocObj),
                { text: "\n" },
                this._bodyPdfService.buildPropertyValueTableWithoutTitle(infoConditions)
            ],
            footer: [this._footerPdfService.buildLine(), this._footerPdfService.buildCompanyInfoFooter(), this._footerPdfService.buildIBANSFooter()]
        };

        return dd;
    }
}

class OCPdfLanguages {
    static en = {
        header: {
            client: "Provider : ",
            clientContactPerson: "Contact : ",
            clientContactPersonTelephone: "Telephone : ",
            clientNumber: "Client : ", // right
            clientTelephone: "Telephone : ",
            clientEmail: "E-mail : ",
            companyContactPerson: "Asesor : ",
            companyContactPersonTelephone: "Telephone : ",
            companyContactPersonEmail: "E-mail : ",
            ocTitle: "Order Confirmation",
            ocNumber: "Confirmation Nr. : ",
            offerNumber: "Offer Number",
            ocDate: "Conf. date : ",
            offeredDeliveryDate: "Delivery date : ",
            taxID: "Tax ID Number",
            vatID: "VAT ID"
        },
        body: {
            documentContextText: "Thank you for your order, we are pleased about your order. For details, see the following overview :",
            totalWithoutVAT: "Total without VAT",
            vatFrom: "VAT from ",
            totalWithVAT: "Total with VAT",
            paymentMethod: "Payment Method : ",
            paymentConditions: "Payment Conditions : ",
            wayOfDelivery: "Delivery Way : ",
            deliveryConditions: "Delivery Conditions : "
        }
    };

    static de = {
        header: {
            client: "Empfänger : ",
            clientContactPerson: "Durch :",
            clientContactPersonTelephone: "Telefon : ",
            clientNumber: "Kunde : ",
            clientTelephone: "Telefon : ",
            clientEmail: "E-mail : ",
            companyContactPerson: "Bearbeiter : ",
            companyContactPersonTelephone: "Telefon : ",
            companyContactPersonEmail: "E-mail : ",
            ocTitle: "Auftragsbestätigung",
            ocNumber: "Auftr.-Nr. : ",
            offerNumber: "Ang.-Nr. : ",
            ocDate: "AB. Datum : ",
            offeredDeliveryDate: "Liefertermin : ",
            taxID: "ST.-Nr. : ",
            vatID: "USt.-IdNr. : "
        },
        body: {
            documentContextText: "Vielen Dank für Ihre Bestellung, wir freuen uns über Ihren Auftrag. Details entnehmen Sie der folgenden übersicht :",
            totalWithoutVAT: "Total exkl. MwSt.",
            vatFrom: "MwSt von",
            totalWithVAT: "Total inkl. MwSt.",
            paymentMethod: "Zahlungsart : ",
            paymentConditions: "Zahlungsbedingungen : ",
            wayOfDelivery: "Lieferart : ",
            deliveryConditions: "Lieferbedingungen : "
        }
    };

    static es = {
        header: {
            position: "",
            client: "Proveedor : ",
            clientContactPerson: "Contacto : ",
            clientContactPersonTelephone: "Teléfono : ",
            clientNumber: "Client : ",
            clientTelephone: "Teléfono : ",
            clientEmail: "E-mail : ",
            companyContactPerson: "Asesor : ",
            companyContactPersonTelephone: "Teléfono : ",
            companyContactPersonEmail: "E-mail : ",
            ocTitle: "Orden de Trabajo",
            ocNumber: "Nr. OT : ",
            offerNumber: "Cotización :",
            ocDate: "Fecha de OT : ",
            offeredDeliveryDate: "F. Entrega :",
            taxID: "Tax ID Number",
            vatID: "VAT number"
        },
        body: {
            documentContextText: "Gracias por su pedido y la confianza en nostros. Para más detalles, consulte la siguiente descripción:",
            totalWithoutVAT: "Total sin IVA",
            vatFrom: "IVA de ",
            totalWithVAT: "Total con IVA",
            paymentMethod: "Metodo de pago :",
            paymentConditions: "Condiciones de pago :",
            wayOfDelivery: "Forma de entrega :",
            deliveryConditions: "Condiciones de entrega :"
        }
    };
}
