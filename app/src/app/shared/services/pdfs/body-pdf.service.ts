import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";
import { CurrencyPipe } from "@angular/common";

@Injectable({
    providedIn: "root"
})
export class BodyPdfService {
    constructor(private _currencyPipe: CurrencyPipe, private _sharedService: SharedService) {}
    currency = this._sharedService.currency;
    language = this._sharedService.user_selected_language;
    buildStyles() {
        return {
            tableHeaderCentered: { italic: true, alignment: "center", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
            tableHeaderLeft: { italic: true, alignment: "left", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
            tableHeaderRight: { italic: true, alignment: "right", fontSize: 10, bold: true, fillColor: "#eeeeee", margin: [0, 2, 0, 2] },
            rowLeft: { italic: true, alignment: "justify", fontSize: 9, margin: [0, 3, 0, 3] },
            rowCentered: { italic: true, alignment: "center", fontSize: 9, margin: [0, 3, 0, 3] },
            small: { fontSize: 10 },
            big: { fontSize: 13, bold: true }
        };
    }

    buildTotalTable(documentBody, valuesObj) {
        let obj = {};
        obj["layout"] = "noBorders";
        obj["table"] = {};
        obj["table"]["headerRows"] = 3;
        obj["table"]["dontBreakRows"] = true;
        obj["table"]["keepWithHeaderRows"] = 3;
        obj["table"]["widths"] = ["5%", "25%", "25%", "12%", "15%", "18%"];
        obj["table"]["body"] = this.buildTotalTableBody(documentBody, valuesObj);
        return obj;
    }

    buildTotalTableBody(textFieldsObj, valuesObj) {
        let firstRow = this.buildFirstRow(textFieldsObj, valuesObj);
        let secondRow = this.buildSecondRow(textFieldsObj, valuesObj);
        let thirdRow = this.buildThirdRow(textFieldsObj, valuesObj);
        let body = [];
        Array.prototype.push.apply(body, [firstRow, secondRow, thirdRow]);
        return body;
    }

    buildFirstRow(textFieldsObj, valuesObj) {
        let totalWithoutVATText = textFieldsObj.totalWithoutVAT;
        let docNetoValue = this._currencyPipe.transform(valuesObj.subTotal, this.currency, "symbol-narrow","", this.language);
        let firstRow = [
            { text: "", style: "tableHeaderCentered" },
            { text: totalWithoutVATText, style: "tableHeaderLeft" },
            { text: "", style: "tableHeaderCentered" },
            { text: "", style: "tableHeaderCentered" },
            { text: this.currency, style: "tableHeaderCentered" },
            { text: docNetoValue, style: "tableHeaderCentered" }
        ];
        return firstRow;
    }
    buildSecondRow(textFieldsObj, valuesObj) {
        let vatPercentage = valuesObj.vat_percentage;
        let vatFromText = vatPercentage + " % " + textFieldsObj.vatFrom;
        let offerNetoValue = this._currencyPipe.transform(valuesObj.subTotal, this.currency, "symbol-narrow","", this.language);
        let vatValue = this._currencyPipe.transform(valuesObj.vat, this.currency, "symbol-narrow","", this.language);
        let secondRow = [
            { text: "", style: "rowCentered" },
            { text: vatFromText, style: "rowLeft" },
            { text: offerNetoValue, style: "rowCentered" },
            { text: "", style: "rowCentered" },
            { text: this.currency, style: "rowCentered" },
            { text: vatValue, style: "rowCentered" }
        ];
        return secondRow;
    }

    buildThirdRow(textFieldsObj, valuesObj) {
        let offerTotalValue = this._currencyPipe.transform(valuesObj.total, this.currency, "symbol-narrow","", this.language);
        let totalWithVATText = textFieldsObj.totalWithVAT;
        let thirdRow = [
            { text: "", style: "tableHeaderCentered" },
            { text: totalWithVATText, style: "tableHeaderLeft" },
            { text: "", style: "tableHeaderCentered" },
            { text: "", style: "tableHeaderCentered" },
            { text: this.currency, style: "tableHeaderCentered" },
            { text: offerTotalValue, style: "tableHeaderCentered" }
        ];
        return thirdRow;
    }

    buildPropertyValueTableWithoutTitle(collection) {
        let obj = {};
        obj["table"] = {};
        obj["table"]["body"] = this.buildPropertyValueTableBody(collection);
        obj["table"]["headerRows"] = 1;
        obj["table"]["keepWithHeaderRows"] = 4;
        //obj['layout'] = 'noBorders';
        return obj;
    }

    buildPropertyValueTableBody(collection) {
        let tableBody = [];
        collection.forEach(object => {
            let array = [
                { text: object["property"], fontSize: 10, border: [false, false, false, false] },
                { text: object["value"], fontSize: 10, border: [false, false, false, false] }
            ];
            tableBody.push(array);
        });
        return tableBody;
    }

    static formatDate(date, cl) {
        let d = new Date(date);
        let options = { year: "numeric", month: "numeric", day: "numeric" };
        return d.toLocaleDateString(cl, options);
    }
}
