import { Injectable } from "@angular/core";
import { SharedService } from "../shared.service";
import { InvoicePdfFunctions } from "./invoice-pdf-functions";
import { InvoicesService } from "../invoices.service";
import { FooterPdfService } from "./footer-pdf.service";
import { HeaderPdfService } from "./header-pdf.service";
import { ColumnsNamesService } from "../columns-names.service";
import { BodyPdfService } from "./body-pdf.service";
import * as pdfMake from "pdfmake/build/pdfmake.js";
import * as pdfFonts from "pdfmake/build/vfs_fonts.js";
import { InternalDocumentsService } from "../internal-documents.service";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

declare var require: any;

@Injectable({
    providedIn: "root"
})
export class InvoicePdfService {
    private readonly pdfFonts: any;
    pdfMake: any;
    sessionInfo;
    styles = this._bodyPdfService.buildStyles(); // styles for pdf document
    constructor(
        private _sharedService: SharedService,
        private _idocsService : InternalDocumentsService,
        private _invoicesService: InvoicesService,
        private _headerPdfService: HeaderPdfService,
        private _bodyPdfService: BodyPdfService,
        private _footerPdfService: FooterPdfService,
        private _columnsNamesService: ColumnsNamesService
    ) {
        this.pdfMake = pdfMake;
        this._sharedService.sessionInfo.subscribe(data => (this.sessionInfo = data));
    }

    buildDocument(invoiceObj) {
        let cl = this._sharedService.user_selected_language;
        let documentHeader = InvoicePdfLanguages[cl].header;
        let documentBody = InvoicePdfLanguages[cl].body;
        let infoInvoice = InvoicePdfFunctions.getInvoiceData(documentHeader, invoiceObj, cl);
        let infoDocumentCreator = InvoicePdfFunctions.getDocumentCreatorData(documentHeader, invoiceObj);
        let infoClient = InvoicePdfFunctions.getClientData(invoiceObj);
        let infoConditions = InvoicePdfFunctions.getInvoiceConditionsData(documentBody, invoiceObj);
        let image = this._sharedService.imgCodeBase64;
        let pdfInvoiceTexts = this._invoicesService.pdfInvoiceTexts;
        let tableColumnsObj = this._columnsNamesService.getCurrentCustomTableColumnsObject();

        let dd = {
            pageSize: "A4",
            pageMargins: [60, 65, 60, 125],
            defaultStyle: {
                columnGap: 25
            },
            styles: this.styles,
            header:this._headerPdfService.getPaginator,
            content: [
                this._headerPdfService.buildDocumentHeader(this.sessionInfo, image),
                { text: "\n" },
                {
                    columns: [
                        [
                            { table: this._headerPdfService.buildClientTable(infoClient), layout: "noBorders" },
                            { text: "\n" },
                            { table: this._headerPdfService.buildPropertyValueTableWithoutTitle(infoDocumentCreator), layout: "noBorders" }
                        ],
                        [{ text: "\n" }, { table: this._headerPdfService.buildPropertyValueTableWithTitle(infoInvoice), layout: "headerLineOnly" }]
                    ]
                },
                { text: "\n" }, // new line
                { text: pdfInvoiceTexts["document_context_text"] || documentBody["documentContextText"], fontSize: 10 }, // new line
                { text: "\n" }, // new line
                {
                    table: {
                        headerRows: 1,
                        widths: ["5%", "50%", "12%", "15%", "18%"],
                        body: InvoicePdfFunctions.buildMainTable(tableColumnsObj, this._idocsService.getItemsOfCurrentDocument("pdf"))
                    },
                    layout: "noBorders"
                },
                { text: "\n" }, // new line
                this._bodyPdfService.buildTotalTable(documentBody, invoiceObj),
                { text: "\n" },
                this._bodyPdfService.buildPropertyValueTableWithoutTitle(infoConditions)
            ],
            footer: [this._footerPdfService.buildLine(), this._footerPdfService.buildCompanyInfoFooter(), this._footerPdfService.buildIBANSFooter()]
        };

        return dd;
    }
}

class InvoicePdfLanguages {
    static en = {
        header: {
            invoiceTitle: "Invoice",
            invoiceNumber: "Invoice Nr : ",
            orderConfirmationNumber: "Confirmation Nr. : ",
            invoiceDate: "Invoice date : ",
            deliveryDate: "Delivery date : ",
            clientNumber: "Client Nr. : ",
            taxID: "Tax ID Number",
            vatID: "VAT ID",
            companyContactPerson: "Contact : ",
            companyContactPersonTelephone: "Telephone : ",
            companyContactPersonEmail: "Email :"
        },
        body: {
            documentContextText: "We hereby invoice you the following items.",
            totalWithoutVAT: "Total without VAT",
            vatFrom: "VAT from ",
            totalWithVAT: "Total with VAT",
            paymentMethod: "Payment Method : ",
            paymentConditions: "Payment Conditions : ",
            wayOfDelivery: "Delivery Way : ",
            deliveryConditions: "Delivery Conditions : ",
            ourProjects: "Our Project Numbers : ",
            deliveryDate: "Delivery Date : ",
            clientNumber: ""
        }
    };

    static de = {
        header: {
            invoiceTitle: "Rechnung",
            invoiceNumber: "Rechnungs-Nr. : ",
            orderConfirmationNumber: "Auftr.-Nr. : ",
            invoiceDate: "Rechnungsdatum : ",
            deliveryDate: "Lieferdatum : ",
            clientNumber: "Kd.-Nr : ",
            taxID: "ST-Nr.: ",
            vatID: "USt-IdNr.: ",
            companyContactPerson: "Bearbeiter :",
            companyContactPersonTelephone: "Telefon : ",
            companyContactPersonEmail: "Email : "
        },
        body: {
            documentContextText: "Hiermit stellen wir Ihnen die folgenden Positionen in Rechnung.",
            totalWithoutVAT: "Total exkl. MwSt.",
            vatFrom: "MwSt von",
            totalWithVAT: "Total inkl. MwSt.",
            paymentMethod: "Zahlungsart :",
            paymentConditions: "Zahlungsbedingungen : ",
            wayOfDelivery: "Lieferart :",
            deliveryConditions: "Lieferbedingungen : ",
            ourProjects: "Unsere Auftrags. -Nr. ",
            deliveryDate: "Liefertermin : ",
            clientNumber: ""
        }
    };

    static es = {
        header: {
            invoiceTitle: "Factura",
            invoiceNumber: "Nr. Factura : ",
            orderConfirmationNumber: "Nr. OT : ",
            invoiceDate: "F. Factura : ",
            deliveryDate: "F. Entrega : ",
            clientNumber: "Nr. Cliente : ",
            orderConfirmedDate: "Fecha de Pedido : ",
            taxID: "Tax ID Number",
            vatID: "VAT number",
            companyContactPerson: "Contacto : ",
            companyContactPersonTelephone: "Teléfono : ",
            companyContactPersonEmail: "Email : "
        },
        body: {
            documentContextText: "Por la presente facturamos los siguientes artículos.",
            totalWithoutVAT: "Total sin IVA",
            vatFrom: "IVA de ",
            totalWithVAT: "Total Con IVA",
            paymentMethod: "Metodo de Pago : ",
            paymentConditions: "Condiciones de Pago : ",
            wayOfDelivery: "Forma de Entrega : ",
            deliveryConditions: "Condiciones de Entrega : ",
            ourProjects: "Numero de Proyectos : ",
            deliveryDate: "Fecha de Entrega : ",
            clientNumber: ""
        }
    };
}
