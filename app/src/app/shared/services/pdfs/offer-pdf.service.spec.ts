import { TestBed, inject } from '@angular/core/testing';


import { OfferPdfService } from './offer-pdf.service';

describe('OfferPdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OfferPdfService]
    });
  });

  it('should be created', inject([OfferPdfService], (service: OfferPdfService) => {
    expect(service).toBeTruthy();
  }));
});
