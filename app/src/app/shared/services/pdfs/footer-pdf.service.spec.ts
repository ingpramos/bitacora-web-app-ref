import { TestBed, inject } from '@angular/core/testing';

import { FooterPdfService } from './footer-pdf.service';

describe('FooterPdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FooterPdfService]
    });
  });

  it('should be created', inject([FooterPdfService], (service: FooterPdfService) => {
    expect(service).toBeTruthy();
  }));
});
