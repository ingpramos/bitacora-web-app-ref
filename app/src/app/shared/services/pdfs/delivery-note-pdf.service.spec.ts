import { TestBed, inject } from '@angular/core/testing';

import { DeliveryNotePdfService } from './delivery-note-pdf.service';

describe('DeliveryNotePdfService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliveryNotePdfService]
    });
  });

  it('should be created', inject([DeliveryNotePdfService], (service: DeliveryNotePdfService) => {
    expect(service).toBeTruthy();
  }));
});
