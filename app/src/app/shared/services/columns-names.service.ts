import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class ColumnsNamesService {
    //Global Info App
    apiUrl: string = this._sharedService.apiUrl;
    company_id;
    user_id;
    current_laguage;

    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.company_id = data.company_id;
            this.user_id = data.user_id;
            this.current_laguage = data.user_selected_language;
        });
    }
    // to hold current obj of columns names
    private customColumnsNames = new BehaviorSubject<any>({});
    newCustomColumnsNamesObs = this.customColumnsNames.asObservable();
    passNewColumnsNamesObj(data: any) {
        this.customColumnsNames.next(data);
    }

    // to hold the collection of custom names from db
    customColumnNamesCollection;
    // to hold the object with custom key, values
    private currentCustomTableColumnsObject: any;
    getCurrentCustomTableColumnsObject() {
        return this.currentCustomTableColumnsObject;
    }

    getCustomColumnsNames(table_name) {
        let table = table_name;
        let url = `${this.apiUrl}/customizedColumns?company_id=${this.company_id}&table=${table}&language=${this.current_laguage}`;
        return this.http.get(url).pipe(
            map(response => {
                this.customColumnNamesCollection = response["data"];
                return response["data"];
            })
        );
    }

    updateCustomColumnsNames(body, table_name) {
        let table = table_name;
        let columnsToUpdateCollection = this.prepareColumnsToUpdate(body, table_name);
        let url = `${this.apiUrl}/customizedColumns?company_id=${this.company_id}&table=${table}`;
        return this.http.put(url, columnsToUpdateCollection).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    findCustomizedName(customizedColumnsCollection, column_name) {
        let cn = customizedColumnsCollection.find((obj: any) => obj["column_name"] === column_name);
        if (cn) return cn["customized_name"];
    }

    buildCustomTableColumnsObject(table, defaultTableColumns) {
        // table = table name in db ; this[table] = ['item_code','item_name....]; standardHeader {item_code: 'Código'}
        let customTableColumns: any = {};
        this.getCustomColumnsNames(table).subscribe(data => {
            this[table].forEach((prop: string) => {
                customTableColumns[prop] = this.findCustomizedName(data, prop) || defaultTableColumns[prop];
            });
            this.currentCustomTableColumnsObject = customTableColumns;
            this.passNewColumnsNamesObj(customTableColumns);
        });
    }

    formatColumnsOfImportTable(defaultTableColumnsObj) {
        Object.keys(defaultTableColumnsObj).forEach(key => {
            if (this.currentCustomTableColumnsObject[key]) defaultTableColumnsObj[key] = this.currentCustomTableColumnsObject[key];
        });
    }

    prepareColumnsToUpdate(newColumnsNamesObj, table_name) {
        let columnsToUpdateCollection = [];
        let columnsToCreate = [];
        Object.keys(newColumnsNamesObj).forEach(key => {
            let value = newColumnsNamesObj[key];
            let match = this.customColumnNamesCollection.find((obj: any) => obj["column_name"] === key);
            if (match) {
                if (match["customized_name"] !== value) {
                    match["customized_name"] = value;
                    columnsToUpdateCollection.push(match);
                }
            } else {
                columnsToCreate.push({ column_name: key, customized_name: value, language: this.current_laguage });
            }
        });
        let customColumns = {};
        customColumns["columnsToCreate"] = columnsToCreate;
        customColumns["columnsToUpdate"] = columnsToUpdateCollection;
        return customColumns;
    }

    formatSuccessData(data) {
        let text = "";
        data.forEach((obj: any) => {
            let t = ` - ${obj["customized_name"]}`;
            if (text === "") {
                text += `${obj["customized_name"]}`;
            } else {
                text += t;
            }
        });
        return text;
    }

    items = ["item_code", "item_name", "item_amount", "total_booked", "total_in_progress", "total_in_use", "item_neto", "total_expected"];
    items_in_assemblies = ["item_position", "item_type", "item_code", "item_name", "item_amount"];
    items_in_invoices = ["item_position", "item_type", "item_code", "item_name", "item_amount", "item_sell_price", "item_total_price"];
    items_in_projects = ["item_position", "item_type", "item_code", "item_name", "item_amount", "item_activity_done_name"];
    items_in_offers = ["item_position", "item_type", "item_code", "item_name", "item_amount", "item_sell_price", "item_total_price"];
    items_in_orders = ["item_position", "item_delivered", "item_code", "item_name", "item_amount", "received_amount", "item_buy_price", "item_total_price"];
}
