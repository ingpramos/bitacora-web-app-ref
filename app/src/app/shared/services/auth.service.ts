import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router, CanActivate } from '@angular/router';
import { SharedService } from './shared.service';
import { catchError, map} from 'rxjs/operators';
import { CompanyService } from './company.service';
import { ConfigService } from './config.service';

@Injectable({
    providedIn: 'root'
})

export class AuthService implements CanActivate {
    // Global Info App
    apiUrl: string = this._sharedService.apiUrl;

    constructor(
        private http: HttpClient,
        private _configService: ConfigService,
        private _sharedService: SharedService,
        private _companyService: CompanyService,
        private router: Router
    ) {

    }

    canActivate() {
        if (this.isLoggedIn()) return true;
        this.router.navigate(['/auth/login']);
        return false;
    }


    login(credentials) {
        let url = `${this.apiUrl}/login`;
        return this.http.post(url, credentials).pipe(map(response => {
            this.setSession(response);
        }));
    }

    setSession(authResult) {
        localStorage.setItem("id_token", authResult['data'].token);
        let expires_at = (this.jwtDecode(authResult['data'].token)['payload'].exp);
        localStorage.setItem("expires_at", JSON.stringify(expires_at));
    }

    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
    }

    public isLoggedIn() {
        return (Date.now() / 1000) < this.getExpiration();
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return expiresAt;
    }

    jwtDecode(t) {
        let token = {};
        token['raw'] = t;
        token['header'] = JSON.parse(window.atob(t.split('.')[0]));
        token['payload'] = JSON.parse(window.atob(t.split('.')[1]));
        return (token)
    }

    getUserId() {
        return this.jwtDecode(localStorage.getItem("id_token"))["payload"].user_id;
    }

    sendResetRequest(body){
        let url = `${this.apiUrl}/forgotPassword`;
        return this.http.post(url,body).pipe(
            catchError(this._configService.handleError)
        )
    }

    resetOwnPassword(credentials){
        let url = `${this.apiUrl}/resetOwnPassword`;
        return this.http.post(url, credentials).pipe(map(response => {
            this.setSession(response);
        }));
    }
    
    activateCompany(credentials){
        let url = `${this.apiUrl}/confirmCompany`;
        return this.http.put(url, credentials);
    }
}
