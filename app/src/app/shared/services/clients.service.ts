import { catchError, map, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from './shared.service';
import { BehaviorSubject } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
	providedIn: 'root'
})

export class ClientsService {
	//Global Info App
	sessionInfo = {}; // this is the source of all ids like company_id or user_id
	company_id = "";
	user_id;
	user_selected_language;
	apiUrl: string = this._sharedService.apiUrl;

	constructor(
		private _configService: ConfigService,
		private _sharedService: SharedService,
		private http: HttpClient
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.sessionInfo = data;
			this.company_id = this.sessionInfo['company_id'];
			this.user_id = this.sessionInfo['user_id'];
			this.user_selected_language = this.sessionInfo['user_selected_language'];
		});
	}

	// hold created or updated client as observable
	private newInfoClient = new BehaviorSubject<any[]>([]);
	clientToPushOrUpdate = this.newInfoClient.asObservable();
	passClientToPushOrUpdate(data: any) {
		this.newInfoClient.next(data);
	}

	// hold clients list as observable
	private clientsList = new BehaviorSubject<any[]>([]);
	currentClientsList = this.clientsList.asObservable();
	updateClientsList(data) {
		this.clientsList.next(data);
	}

	// hold clients contacts list as observable
	private contactsList = new BehaviorSubject<any[]>([]);
	currentContactsList = this.contactsList.asObservable();
	updateContactsList(data) {
		this.contactsList.next(data);
	}

	getClients() {
		let url = `${this.apiUrl}/clients?company_id=${this.company_id}`;
		this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		).subscribe(data => this.updateClientsList(data));
	}

	getClientsByPattern(searchPattern) {
		let url = `${this.apiUrl}/clientsByPattern?company_id=${this.company_id}&search_pattern=${searchPattern}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		).subscribe(data => this.updateClientsList(data));
	}

	getFormattedClients() {
		let clientsUrl = `${this.apiUrl}/clients?company_id=${this.company_id}`;
		return this.http.get(clientsUrl).pipe(
			map(response => this.formatClients(response['data'])),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createClient(body) {
		let createClientUrl = `${this.apiUrl}/clients`;
		body['company_id'] = this.company_id;
		return this.http.post(createClientUrl, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateClient(body) {
		let client_id = body.client_id;
		let url = `${this.apiUrl}/clients?client_id=${client_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteClient(client_id) {
		let url = `${this.apiUrl}/clients?client_id=${client_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	getContacts(client_id) {
		let contactsUrl = `${this.apiUrl}/clientContacts?client_id=${client_id}`;
		this.http.get(contactsUrl).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
			)
			.subscribe(data => this.updateContactsList(data));
	}

	getFormattedContacts(client_id) {
		let contactsUrl = `${this.apiUrl}/clientContacts?client_id=${client_id}`;
		return this.http.get(contactsUrl).pipe(
			map(response => this.formatContacts(response['data'])),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createContact(body) {
		body.company_id = this.company_id;
		let contactsUrl = `${this.apiUrl}/clientContacts`;
		return this.http.post(contactsUrl, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateClientContact(body) {
		const cc_id = body.cc_id;
		let url = `${this.apiUrl}/clientContacts?cc_id=${cc_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	formatClients(collection) { // to make client model to play nice with the select
		let formattedClients = {};
		formattedClients['clients_names'] = [];
		formattedClients['clients_ids'] = [];
		formattedClients['getClient_id'] = (client) => {
			let clientIndex = formattedClients['clients_names'].indexOf(client);
			let client_id = formattedClients['clients_ids'][clientIndex];
			return client_id;
		};
		collection.forEach((obj) => {
			formattedClients['clients_names'].push(obj['client_name']);
			formattedClients['clients_ids'].push(obj['client_id']);
		});
		return formattedClients;
	}

	formatContacts(collection) {
		let formattedContacts = {};
		formattedContacts['contacts_complete_names'] = [];
		formattedContacts['cc_ids'] = [];
		formattedContacts['getCurrentClientContact'] = (contact) => {
			let indexOfConctac = formattedContacts['contacts_complete_names'].indexOf(contact);
			let pc_id = formattedContacts['cc_ids'][indexOfConctac];
			return collection.find((obj) => {
				return obj['cc_id'] === pc_id;
			});
		};
		formattedContacts['getCc_id'] = (contact) => {
			let indexOfConctac = formattedContacts['contacts_complete_names'].indexOf(contact);
			let cc_id = formattedContacts['cc_ids'][indexOfConctac];
			return cc_id;
		};
		collection.forEach((obj) => {
			formattedContacts['contacts_complete_names'].push(obj['contact_name'] + " " + obj['contact_last_name']);
			formattedContacts['cc_ids'].push(obj['cc_id']);
		});

		return formattedContacts;
	}

	deleteContactFromTable(cc_id) {
		let url = `${this.apiUrl}/clientContacts?cc_id=${cc_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

}
