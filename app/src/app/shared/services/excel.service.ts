import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ColumnsNamesService } from "./columns-names.service";
import { SharedService } from "./shared.service";
import * as XLSX from "xlsx";

@Injectable({
    providedIn: "root"
})
export class ExcelService {
    constructor(private _columnsNamesService: ColumnsNamesService, private _sharedService: SharedService) {}

    fileInformation;
    aoa; // array of arrays

    downLoadXlsxFile(data, fileName, sheetName) {
        let arrayOfFields = ["A1", "B1", "C1", "D1", "E1", "F1", "G1", "H1", "I1", "J1", "K1", "L1", "M1"];
        let referenceColumns = this._columnsNamesService.getCurrentCustomTableColumnsObject();
        /* generate worksheet */
        const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);
        let show = this.resumeStandardColumnsNames(arrayOfFields, ws);
        this.buildCustomColumnsNames(show, referenceColumns);
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, sheetName);
        /* save to file */
        XLSX.writeFile(wb, fileName);
    }

    onFileChange(evt: any, context?: string) {
        let filteredData = []; // filter items acoording context mainBoard or internalDocuments
        // is the object that holds the customized columns names
        let referenceColumns = this._columnsNamesService.getCurrentCustomTableColumnsObject();
        const excelObservable = new Observable(observer => {
            /* wire up file reader */
            const target: DataTransfer = <DataTransfer>evt.target;
            if (target.files.length !== 1) throw new Error("Cannot use multiple files");
            this.fileInformation = target.files[0];

            const reader: FileReader = new FileReader();
            reader.onload = (e: any) => {
                /* read workbook */
                const bstr: string = e.target.result;
                const wb: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
                /* grab first sheet */
                const wsname: string = wb.SheetNames[0];
                const ws: XLSX.WorkSheet = wb.Sheets[wsname];
                /* save data, array of arrays */
                this.aoa = XLSX.utils.sheet_to_json(ws, { header: 1, raw: false });
                // let cols = [];
                // for (let i = 0; i < this.aoa[0].length; ++i) {
                // 	cols[i] = { field: this.aoa[0][i] };
                // }
                let proofedArray = this.proofColumns(this.aoa[0], referenceColumns);
                /* generate rest of the data */
                let data = [];
                for (let r = 1; r < this.aoa.length; ++r) {
                    data[r - 1] = {};
                    for (let i = 0; i < this.aoa[r].length; ++i) {
                        if (this.aoa[r][i] === null) {
                            continue;
                        }
                        data[r - 1][proofedArray[i]] = this.aoa[r][i];
                    }
                }

                if (context == "mainBoard") {
                    filteredData = data.filter(obj => obj["item_code"] && obj["item_name"]);
                } else {
                    filteredData = data.filter(obj => obj["item_code"]);
                }
                // prepare data to clasify it in the components
                filteredData.forEach((obj: any) => {
                    obj["is_checked"] = true;
                    obj["item_amount"] = obj["item_amount"] || 0;
                    obj["is_already_in_list"] = false;
                    obj["is_already_in_db"] = false;
                });

                let results = {};
                results["fileInformation"] = this.fileInformation;
                results["items"] = filteredData;
                observer.next(results);
            };
            reader.readAsBinaryString(target.files[0]);
        });
        return excelObservable;
    }
    // function to allow recognize customized columns names
    proofColumns(columnsFromExcel, referenceColumnsObj: any) {
        let proofedArray = [];
        if (columnsFromExcel && columnsFromExcel.length > 0) {
            columnsFromExcel.forEach(currentCol => {
                let match = Object.keys(referenceColumnsObj).find(key => referenceColumnsObj[key] === currentCol);
                if (match) proofedArray.push(match);
                else proofedArray.push(currentCol);
            });
        }
        return proofedArray;
    }

    labelItemsAlreadyInList(itemsAlreadyIn, itemsFromExcel) {
        itemsFromExcel.forEach((ife: any) => {
            itemsAlreadyIn.forEach((iai: any) => {
                // clasify the items from excel that are already in the  target document document
                if (ife["item_code"] === iai["item_code"]) {
                    ife["item_name"] = iai["item_name"];
                    // if the document have a price it will be respected if not will get the price and amount from the document in list
                    if(!ife["item_amount"]) ife["item_amount"] = iai["item_amount"];
                    if(!ife["item_buy_price"]) ife["item_buy_price"] = iai["item_buy_price"];
                    if(!ife["item_sell_price"]) ife["item_sell_price"] = iai["item_sell_price"];
                    ife["is_already_in_list"] = true;
                    ife["is_already_in_db"] = true;
                    ife["is_checked"] = true;
                }
            });
            ife["item_amount_to_show"] = this._sharedService.parseToLocaleString(ife["item_amount"] || 0).trim();
            ife["item_buy_price_to_show"] = this._sharedService.parseToLocaleString(ife["item_buy_price"] || 0).trim();
            ife["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(ife["item_sell_price"] || 0).trim();
        });
    }

    labelItemsAlreadyInDB(itemsAlreadyIn, itemsFromExcel) {
        itemsFromExcel.forEach((ife: any) => {
            itemsAlreadyIn.forEach((iai: any) => {
                if (ife["item_code"] === iai["item_code"]) {
                    ife["item_id"] = iai["item_id"];
                    ife["item_name"] = iai["item_name"] || ife["item_name"];
                    // if the document have a price it will be respected if not will get the price from db
                    if(!ife["item_amount"]) ife["item_amount"] = iai["item_amount"];
                    if(!ife["item_buy_price"]) ife["item_buy_price"] = iai["item_buy_price"];
                    if(!ife["item_sell_price"]) ife["item_sell_price"] = iai["item_sell_price"];
                    ife["is_already_in_db"] = true;
                    ife["is_checked"] = true;
                }
            });
            // this part is really importat for the excel import in main board module
            // numbers come always as string from excel importing library
            if(!ife["item_amount_to_show"]){
                ife["item_amount_to_show"] = this._sharedService.parseToLocaleString(Number(ife["item_amount"]) || 0).trim();                
            } 
            if(!ife["item_buy_price_to_show"]){
                ife["item_buy_price_to_show"] = this._sharedService.parseToLocaleString(Number(ife["item_buy_price"])|| 0).trim();
            } 
            if(!ife["item_sell_price_to_show"]) {
                ife["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(Number(ife["item_sell_price"]) || 0).trim();                
            } 
        });
        
    }

    deleteObjectProperties(collection: any[], arrayOfProperties: any[]) {
        collection.forEach((obj: any) => {
            arrayOfProperties.forEach((property: string) => {
                delete obj[property];
            });
        });
    }

    resumeStandardColumnsNames(arrayOfFields, ws) {
        let columnHeaders = {};
        arrayOfFields.forEach((field: string) => {
            if (ws[field]) columnHeaders[field] = ws[field];
        });
        return columnHeaders;
    }

    buildCustomColumnsNames(columnsHeaderObj, customNamesObj) {
        Object.values(columnsHeaderObj).forEach(value => {
            let ref = value;
            if (customNamesObj[ref["v"]]) ref["v"] = customNamesObj[ref["v"]];
        });
    }
}
