import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { SharedService } from './shared.service';
import { ConfigService } from './config.service';

@Injectable({
	providedIn: 'root'
})

export class ItemsInProjectsService {
	//Global Info App
	apiUrl: string = this._sharedService.apiUrl;
	sessionInfo;
	company_id;
	user_id;
	user_selected_language;

	constructor(
		private _configService: ConfigService,
		private http: HttpClient,
		private _sharedService: SharedService
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.sessionInfo = data;
			this.company_id = this.sessionInfo['company_id'];
			this.user_id = this.sessionInfo['user_id'];
			this.user_selected_language = this.sessionInfo['user_selected_language'];
		});
		this.currentItemsList.subscribe(data => this.itemsInCurrentAssembly = data);
	}

	private itemsInCurrentAssembly: any[];
	getCurrentAssemblyItems() {
		return this.itemsInCurrentAssembly;
	}
	private itemsList = new BehaviorSubject<any>([]);
	currentItemsList = this.itemsList.asObservable();

	updateItemList(data) {
		this.itemsList.next(data);
	}


	getItemsFromAssembly(p_id, aip_id) {
		let url = `${this.apiUrl}/itemsInProject?project_id=${p_id}&aip_id=${aip_id}&language=${this.user_selected_language}`;
		this.http.get(url).pipe(
			map(response => response['data']))
			.subscribe(data => { this.updateItemList(data) });
	}

	insertItemsInProject(collection) {
		let url = `${this.apiUrl}/itemsInProject`;
		return this.http.post(url, collection).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	updateItemInProject(body) {
		let iip_id = body.iip_id;
		let url = `${this.apiUrl}/itemsInProject?iip_id=${iip_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	deleteItemFromProject(iip_id) {
		let url = `${this.apiUrl}/itemsInProject?iip_id=${iip_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	getActivitiesFromItemInProject(iip_id) {
		let url = `${this.apiUrl}/itemInProjectsActivities?iip_id=${iip_id}&company_id=${this.company_id}&language=${this.user_selected_language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	getActivitiesFromAssemblyInProject(iip_id) {
		let url = `${this.apiUrl}/assemblyInProjectsActivities?aip_id=${iip_id}&company_id=${this.company_id}&language=${this.user_selected_language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}
}
