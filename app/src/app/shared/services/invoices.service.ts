import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, tap, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";
import { CurrencyPipe } from "@angular/common";

@Injectable({
    providedIn: "root"
})
export class InvoicesService {
    //Global Info App
    apiUrl: string = this._sharedService.apiUrl;
    sessionInfo;
    company_id;
    user_id;
    user_selected_language;
    currency: string;
    pdfInvoiceTexts = "";
    constructor(
        private cp : CurrencyPipe,
        private _configService: ConfigService,
        private _sharedService: SharedService,
        private http: HttpClient
    ) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
            this.currency = this.sessionInfo["company_currency"];
        });
        // call for context texts of invoice pdf document
        this.getDocumentInvoiceTexts().subscribe(data => this.pdfInvoiceTexts = data);
    }

    // to hold the created or updated oc
    private newInfoInvoice = new BehaviorSubject<any>({});
    invoiceToPushOrUpdate = this.newInfoInvoice.asObservable();
    passOCToPushOrUpdate(data: any) {
        this.newInfoInvoice.next(data);
    }

    // to hold items collection
    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemsList(data) {
        this.itemsList.next(data);
    }
    // to hold invoices collection
    private invoicesList = new BehaviorSubject<any>([]);
    currentInvoicesList = this.invoicesList.asObservable();
    updateInvoicesList(data) {
        this.invoicesList.next(data);
    }
    // // to hold new invoice object
    // private newInvoice = new BehaviorSubject<any>({});
    // newInvoiceToPush = this.newInvoice.asObservable();
    // passNewInvoice(data: any) {  
    //     this.newInvoice.next(data);
    // }

    // // to hold the updated invoice
	// private updatedInvoice = new BehaviorSubject<any>({});
	// updatedInvoiceToReplace = this.updatedInvoice.asObservable();
	// passUpdatedInvoice(data: any) {
	// 	this.updatedInvoice.next(data);
	// }

    getInvoices(state) {
        let url = `${this.apiUrl}/invoices?company_id=${this.company_id}&invoice_state=${state}&language=${this.user_selected_language}`;
        this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        )
            .subscribe(data => this.updateInvoicesList(data));
    }

    getInvoicesByPattern(state, searchPattern) {
        let url = `${this.apiUrl}/invoicesByPattern?company_id=${this.company_id}&invoice_state=${state}&language=${this.user_selected_language}&search_pattern=${searchPattern}`;
        this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        )
            .subscribe(data => this.updateInvoicesList(data));
    }

    getAvailableOrdersConfirmation() {
        let url = `${this.apiUrl}/ordersConfirmations?company_id=${this.company_id}&offer_state=2&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response =>  this.formatOrdersConfirmed(response["data"])),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    createInvoice(body) {
        let url = `${this.apiUrl}/invoices`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateInvoice(body) {
        body.invoice_user_id = this.user_id;
        let url = `${this.apiUrl}/invoices`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteInvoice(invoice_id) {
        let url = `${this.apiUrl}/invoices?internal_document_id=${invoice_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    closeInvoice(invoice_id) {
        let url = `${this.apiUrl}/closeInvoice?internal_document_id=${invoice_id}`;
        return this.http.put(url, {}).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }


    
    getCurrentInvoiceNumber() {
        let url = `${this.apiUrl}/documentNumber?company_id=${this.company_id}&document_name=invoices`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getDocumentInvoiceTexts() {
        let url = `${this.apiUrl}/documentContext?company_id=${this.company_id}&language=${this.user_selected_language}&document_name=invoices`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError))
    }

    formatOrdersConfirmed(collection) {
        // to make order confirmations model to play nice with the select
        let formattedOrdersConfirmed = {};
        formattedOrdersConfirmed["orders_confirmation_numbers"] = [];
        formattedOrdersConfirmed["internal_documents_ids"] = [];
        formattedOrdersConfirmed["get_internal_document_id"] = ocn => {
            let documentIndex = formattedOrdersConfirmed["orders_confirmation_numbers"].indexOf(ocn);
            let internal_document_id =formattedOrdersConfirmed["internal_documents_ids"][documentIndex];
            return internal_document_id;
        };
        collection.forEach(obj => {
            formattedOrdersConfirmed["orders_confirmation_numbers"].push(obj["order_confirmation_number"]);
            formattedOrdersConfirmed["internal_documents_ids"].push(obj["internal_document_id"]);
        });
        formattedOrdersConfirmed["original_data"] = collection;
        return formattedOrdersConfirmed;
    }
}
