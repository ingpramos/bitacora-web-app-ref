import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class GroupsService {
    //Global Info App
    apiUrl: string = this._sharedService.apiUrl;
    sessionInfo;
    company_id;
    user_id;
    user_selected_language;

    constructor(private _configService: ConfigService, private http: HttpClient, private _sharedService: SharedService) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
    }
    private itemsOfCurrentGroup;

    getItemsOfCurrentGroup() {
        return this.itemsOfCurrentGroup;
    }

    // to hold the created or updated group
    private newInfoGroup = new BehaviorSubject<any>({});
    groupToPushOrUpdate = this.newInfoGroup.asObservable();
    passGroupToPushOrUpdate(data: any) {
        this.newInfoGroup.next(data);
    }

    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemList(data) {
        this.itemsList.next(data);
    }

    private groupsList = new BehaviorSubject<any>([]);
    currentGroupsList = this.groupsList.asObservable();
    updateGroupList(data) {
        this.groupsList.next(data);
    }

    getGroups(type) {
        // 1 assembly ; 2 sub assembly
        let url = `${this.apiUrl}/assemblies?company_id=${this.company_id}&assembly_type=${type}&assembly_visible=1`;
        this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateGroupList(data));
    }
    // function to use in the group list component updates grouplist observable
    getGroupsByPattern(type, searchPattern) {
        // type 0 = all ; type 1 = assemblies ; type 2 = sub assemblies
        let url = `${this.apiUrl}/assembliesByPattern?company_id=${this.company_id}&assembly_type=${type}&assembly_visible=1&search_pattern=${searchPattern}`;
        this.http.get(url).pipe(
                map(response => {
                    // return information without formatting if search comming from list component
                    if(type != 0) return response["data"]; 
                    return this.formatAmountAndPriceToShow(response["data"]);
                }),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateGroupList(data));
    }
    // to call data in the insert modal return just the allowed to insert assemblies preventing infinitive loop
    searchAllowedGroupsByPattern(assembly_id, searchPattern) {
        let url = `${this.apiUrl}/allowedAssembliesByPattern?company_id=${this.company_id}&assembly_id=${assembly_id}&search_pattern=${searchPattern}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getGroupsData(type) {
        // 1 assembly ; 2 sub assembly
        let url = `${this.apiUrl}/assemblies?company_id=${this.company_id}&assembly_type=${type}&assembly_visible=1`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getItemsFromAssembly(id) {
        let url = `${this.apiUrl}/itemsInAssemblies?assembly_id=${id}`;
        this.http.get(url).pipe(
                map(response => {
                    this.itemsOfCurrentGroup = response["data"];
                    return response["data"];
                })
            )
            .subscribe(data => this.updateItemList(data));
    }
    // this function is to be use for the tree component
    getSubAssembliesFromAssembly(id) {
        let url = `${this.apiUrl}/subAssembliesFromAssembly?assembly_id=${id}`;
        return this.http.get(url).pipe(
            map(response => {
                return response["data"];
            })
        );
    }

    createAssembly(body) {
        let url = `${this.apiUrl}/assemblies`;
        body.company_id = this.company_id;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateAssembly(body) {
        const assembly_id = body.assembly_id;
        let url = `${this.apiUrl}/assemblies?assembly_id=${assembly_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteAssembly(id) {
        let url = `${this.apiUrl}/assemblies?assembly_id=${id}&company_id=${this.company_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            catchError(this._configService.handleError)
        );
    }

    insertItemsFromExcel(body, assembly_id) {
        let url = `${this.apiUrl}/itemsInAssembliesExcel?company_id=${this.company_id}&assembly_id=${assembly_id}`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertItemsInAssembly(itemsToInsert) {
        let url = `${this.apiUrl}/itemsInAssemblies`;
        return this.http.post(url, itemsToInsert).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertSubAssembliesInAssembly(subAssemblies) {
        let url = `${this.apiUrl}/subAssembliesInAssemblies`;
        return this.http.post(url, subAssemblies).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateItemInAssembly(body) {
        let iia_id = body.iia_id;
        let url = `${this.apiUrl}/itemsInAssemblies?iia_id=${iia_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteItemsFromAssembly(iia_id) {
        let url = `${this.apiUrl}/itemsInAssemblies?iia_id=${iia_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteSubAssemblyFromAssembly(iia_id) {
        let url = `${this.apiUrl}//subAssembliesInAssemblies?iia_id=${iia_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    formatAmountAndPriceToShow(data){
        if(data.length > 0){
            data.forEach(obj => {
            obj["assembly_amount_to_show"] = this._sharedService.parseToLocaleString(obj["assembly_amount"] || 0).trim();            
            obj["assembly_sell_price_to_show"] = this._sharedService.parseToLocaleString(obj["assembly_sell_price"] || 0).trim();
            });
        }
        return data;        
    }
}
