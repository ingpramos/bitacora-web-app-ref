import { catchError, map, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { SharedService } from './shared.service';
import { ConfigService } from './config.service';

@Injectable({ providedIn: 'root' })

export class ProvidersService {

	//Global Info App
	apiUrl: string = this._sharedService.apiUrl;
	sessionInfo;
	company_id;
	user_id;
	user_selected_language;

	constructor(
		private _configService: ConfigService,
		private http: HttpClient,
		private _sharedService: SharedService) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.sessionInfo = data;
			this.company_id = this.sessionInfo['company_id'];
			this.user_id = this.sessionInfo['user_id'];
			this.user_selected_language = this.sessionInfo['user_selected_language'];
		});
	}

	// hold providers list as observable
	private providersList = new BehaviorSubject<any[]>([]);
	currentProviderList = this.providersList.asObservable();
	updateProvidersList(data) {
		this.providersList.next(data);
	}

	// hold providers contacts list as observable
	private contactsList = new BehaviorSubject<any[]>([]);
	currentContactsList = this.contactsList.asObservable();
	updateContactsList(data) {
		this.contactsList.next(data);
	}

	// to hold the created or updated provider as obaservable
	private newInfoProvider = new BehaviorSubject<any>({});
	providerToPushOrUpdate = this.newInfoProvider.asObservable();
	passProviderToPushOrUpdate(data: any) {
		this.newInfoProvider.next(data);
	}


	getProviders() {
		let providersUrl = `${this.apiUrl}/providers?company_id=${this.company_id}`;
		this.http.get(providersUrl).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError))
			.subscribe(data => this.updateProvidersList(data));
	}

	getProvidersByPattern(searchPattern) {
		let providersUrl = `${this.apiUrl}/providersByPattern?company_id=${this.company_id}&search_pattern=${searchPattern}`;
		this.http.get(providersUrl).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError))
			.subscribe(data => this.updateProvidersList(data));
	}

	getFormattedProviders() {
		let providersUrl = `${this.apiUrl}/providers?company_id=${this.company_id}`;
		return this.http.get(providersUrl).pipe(
			map(response => this.formatProviders(response['data'])),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getContacts(id) {
		let contactsUrl = `${this.apiUrl}/providerContacts?provider_id=${id}`;
		this.http.get(contactsUrl).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
			)
			.subscribe(data => this.updateContactsList(data));
	}

	createProvider(body) {
		body.company_id = this.company_id;
		let createProviderUrl = `${this.apiUrl}/providers`;
		return this.http.post(createProviderUrl, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createContact(body) {
		let createContactUrl = `${this.apiUrl}/providerContacts`;
		return this.http.post(createContactUrl, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getFormattedContacts(id) {
		let contactsUrl = `${this.apiUrl}/providerContacts?provider_id=${id}`;
		return this.http.get(contactsUrl).pipe(
			map(response =>  this.formatContacts(response['data'])),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	formatProviders(collection) { // to make provider model to play nice with the select
		let formattedProviders = {};
		formattedProviders['provider_names'] = [];
		formattedProviders['provider_ids'] = [];
		formattedProviders['getProvider_id'] = (provider) => {
			let providerIndex = formattedProviders['provider_names'].indexOf(provider);
			let provider_id = formattedProviders['provider_ids'][providerIndex];
			return provider_id;
		};
		collection.forEach((obj)=> {
			formattedProviders['provider_names'].push(obj['provider_name']);
			formattedProviders['provider_ids'].push(obj['provider_id']);
		});
		return formattedProviders;
	}

	updateProvider(body) {
		let provider_id = body.provider_id;
		let url = `${this.apiUrl}/providers?provider_id=${provider_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteProvider(provider_id) {
		let url = `${this.apiUrl}/providers?provider_id=${provider_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	updateContactProvider(body) {
		const pc_id = body.pc_id;
		let url = `${this.apiUrl}/providerContacts?pc_id=${pc_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteContactProvider(pc_id) {
		let url = `${this.apiUrl}/providerContacts?pc_id=${pc_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	formatContacts(collection) {
		var formattedContacts = {};
		formattedContacts['contacts_complete_names'] = [];
		formattedContacts['pc_ids'] = [];
		formattedContacts['getCurrentProviderContact'] = (contact) => {
			var indexOfConctac = formattedContacts['contacts_complete_names'].indexOf(contact);
			var pc_id = formattedContacts['pc_ids'][indexOfConctac];
			return collection.find((obj) => {
				return obj['pc_id'] === pc_id;
			});
		};
		formattedContacts['getPc_id'] = (contact) => {
			var indexOfConctac = formattedContacts['contacts_complete_names'].indexOf(contact);
			var pc_id = formattedContacts['pc_ids'][indexOfConctac];
			return pc_id;
		};
		collection.forEach((obj) => {
			formattedContacts['contacts_complete_names'].push(obj['contact_name'] + " " + obj['contact_last_name']);
			formattedContacts['pc_ids'].push(obj['pc_id']);
		});

		return formattedContacts;
	}

	deleteContactFromTable(pc_id) {
		let url = `${this.apiUrl}/providerContacts?pc_id=${pc_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}
}
