import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class UsersService {
    //Global Info App
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    user_selected_language;
    apiUrl: string = this._sharedService.apiUrl;

    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
    }
    //Service Properties
    // to hold the created or updated employee as observable
    private newInfoEmployee = new BehaviorSubject<any>({});
    employeeToPushOrUpdate = this.newInfoEmployee.asObservable();
    passEmployeeToPushOrUpdate(data: any) {
        this.newInfoEmployee.next(data);
    }

    // hold employees list as observable
    private usersList = new BehaviorSubject<any[]>([]);
    currentUsersList = this.usersList.asObservable();
    updateUsersList(data) {
        this.usersList.next(data);
    }

    getUsers() {
        let url = `${this.apiUrl}/users?company_id=${this.company_id}`;
        return this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateUsersList(data));
    }

    getUserById() {
        let url = `${this.apiUrl}/users?user_id=${this.user_id}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getUsersByPattern(searchPattern) {
        let providersUrl = `${this.apiUrl}/usersByPattern?company_id=${this.company_id}&search_pattern=${searchPattern}`;
        this.http
            .get(providersUrl)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateUsersList(data));
    }

    createEmployee(body) {
        body.company_id = this.company_id;
        body.user_selected_language = this._sharedService.user_selected_language;
        let createUserUrl = `${this.apiUrl}/users`;
        return this.http.post(createUserUrl, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateEmployee(body) {
        let user_id = body.user_id;
        let url = `${this.apiUrl}/users?user_id=${user_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deActivateUser(user_id) {
        let url = `${this.apiUrl}/deActivateUser?user_id=${user_id}`;
        return this.http.put(url, {}).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updatePassword(body) {
        let url = `${this.apiUrl}/changePassword`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    // change password of other users (Administrator)
    resetPassword(body) {
        body["user_admin_id"] = this.user_id;
        let url = `${this.apiUrl}/resetPassword`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }
}
