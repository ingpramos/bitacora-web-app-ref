import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { SharedService } from './shared.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class LocationsService {

	apiUrl: string = this._sharedService.apiUrl;
	selectedFile: File = null;
	company_id;
	user_id;
	user_selected_language;

	constructor(
		private _configService: ConfigService,
		private _sharedService: SharedService,
		private http: HttpClient
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.company_id = data.company_id;
			this.user_id = data.user_id;
			this.user_selected_language = data.user_selected_language;
		})
	}
	private formatedLocations;
	
	getFormatedLocations(){
		return this.formatedLocations
	}
	// to hold the created or updated location
	private newInfoLocation = new BehaviorSubject<any>({});
	locationToPushOrUpdate = this.newInfoLocation.asObservable();
	passLocationToPushOrUpdate(data: any) {
		this.newInfoLocation.next(data);
	}

	getLocations() {
		let url = `${this.apiUrl}/locations?company_id=${this.company_id}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	callLocations(){
		let url = `${this.apiUrl}/locations?company_id=${this.company_id}`;
		this.http.get(url).pipe(
			map(response => this.formatLocations(response['data'])),
			retry(3),
			catchError(this._configService.handleError)
		).subscribe(data => this.formatedLocations = data);
	}

	createLocation(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/locations`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateLocation(body) {
		let location_id = body.location_id;
		let url = `${this.apiUrl}/locations?location_id=${location_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteLocation(location_id) {
		let url = `${this.apiUrl}/locations?location_id=${location_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	formatLocations(collection) {
		// to make location model to play nice with the select
		let formattedLocations = {};
		formattedLocations['location_names'] = [];
		formattedLocations['location_ids'] = [];
		formattedLocations['get_location_id'] = location => {
			let index = formattedLocations['location_names'].indexOf(location);
			let location_id = formattedLocations['location_ids'][index];
			return location_id;
		};
		collection.forEach(obj => {
			formattedLocations['location_names'].push(obj['location_name']);
			formattedLocations['location_ids'].push(obj['location_id']);
		});
		formattedLocations['original_data'] = collection;
		return formattedLocations;
	}
}
