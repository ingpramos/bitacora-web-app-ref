import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { SharedService } from './shared.service';
import { HttpClient } from '@angular/common/http';
import { catchError, map, retry } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})

export class ActivitiesService {

	apiUrl: string = this._sharedService.apiUrl;
	selectedFile: File = null;
	company_id;
	user_id;
	user_selected_language;

	constructor(
		private _configService: ConfigService,
		private _sharedService: SharedService,
		private http: HttpClient
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.company_id = data.company_id;
			this.user_id = data.user_id;
			this.user_selected_language = data.user_selected_language;
		})
	}

	// to hold the activity to push or update
	private newInfoActivity = new BehaviorSubject<any>({});
	activityToPushOrUpdate = this.newInfoActivity.asObservable();
	passActivityToPushOrUpdate(data: any) {
		this.newInfoActivity.next(data);
	}

	getCompanyActivities() {
		let url = `${this.apiUrl}/activities?company_id=${this.company_id}&language=${this.user_selected_language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	// function to use in the activities list component 
	getActivitiesByPattern(searchPattern) {
		let url = `${this.apiUrl}/activitiesByPattern?company_id=${this.company_id}&language=${this.user_selected_language}&search_pattern=${searchPattern}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	createActivity(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/activities`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateActivity(body) {
		body.company_id = this.company_id;
		let activity_id = body.activity_id;
		let url = `${this.apiUrl}/activities?activity_id=${activity_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteActivity(activity_id) {
		let url = `${this.apiUrl}/activities?activity_id=${activity_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}
}
