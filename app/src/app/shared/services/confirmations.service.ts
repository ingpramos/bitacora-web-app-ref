import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class ConfirmationsService {
    //Global Info App
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    user_selected_language;
    apiUrl: string = this._sharedService.apiUrl;
    pdfOCTexts: any = {};
    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
        this.getDocumentOCTexts().subscribe(data => (this.pdfOCTexts = data));
    }

    // to hold the created or updated oc
    private newInfoOC = new BehaviorSubject<any>({});
    oCToPushOrUpdate = this.newInfoOC.asObservable();
    passOCToPushOrUpdate(data: any) {
        this.newInfoOC.next(data);
    }

    // to hold the order_confirmation list
    private orderConfirmationsList = new BehaviorSubject<any>([]);
    currentOrderConfirmationsList = this.orderConfirmationsList.asObservable();
    updatedOrderConfirmationsList(data: any) {
        this.orderConfirmationsList.next(data);
    }

    getOrdersConfirmations(state) {
        let url = `${this.apiUrl}/ordersConfirmations?company_id=${this.company_id}&offer_state=${state}&language=${this.user_selected_language}`;
        this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updatedOrderConfirmationsList(data));
    }

    getOrdersConfirmationsByPattern(state, searchPattern) {
        let url = `${this.apiUrl}/ordersConfirmationsByPattern?company_id=${this.company_id}&offer_state=${state}&language=${
            this.user_selected_language
        }&search_pattern=${searchPattern}`;
        this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updatedOrderConfirmationsList(data));
    }

    createOrderConfirmation(body) {
        body.order_confirmation_user_id = this.user_id;
        body.company_id = this.company_id;
        let url = `${this.apiUrl}/ordersConfirmations?company_id=${this.company_id}`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateOrderConfirmation(body) {
        body.order_confirmation_user_id = this.user_id;
        let url = `${this.apiUrl}/ordersConfirmations`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteOrderConfirmation(oc_id) {
        let url = `${this.apiUrl}/ordersConfirmations?internal_document_id=${oc_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getCurrentOCNumber() {
        let url = `${this.apiUrl}/documentNumber?company_id=${this.company_id}&document_name=order_confirmations`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getDocumentOCTexts() {
        let url = `${this.apiUrl}/documentContext?company_id=${this.company_id}&language=${this.user_selected_language}&document_name=order_confirmations`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getActivitiesFromItemInOC(iiid_id) {
        let url = `${this.apiUrl}/itemInOCActivities?iiid_id=${iiid_id}&company_id=${this.company_id}&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }
}
