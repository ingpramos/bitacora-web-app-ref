import { TestBed, inject } from '@angular/core/testing';

import { ActionModalsService } from './action-modals.service';

describe('ActionModalsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionModalsService]
    });
  });

  it('should be created', inject([ActionModalsService], (service: ActionModalsService) => {
    expect(service).toBeTruthy();
  }));
});
