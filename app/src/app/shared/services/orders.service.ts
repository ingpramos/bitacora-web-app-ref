import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";
import { CurrencyPipe } from "@angular/common";

@Injectable({
    providedIn: "root"
})
export class OrdersService {
    //Global Info App
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    language;
    apiUrl: string = this._sharedService.apiUrl;
    currency: string;
    pdfOrdersTexts = "";

    constructor(private _currencyPipe: CurrencyPipe, private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.language = this.sessionInfo["user_selected_language"];
            this.currency = this.sessionInfo["company_currency"];
        });
        this.currentItemsList.subscribe(data => (this.itemsInOrder = data));
        // to get the information to fill the pdf
        this.getDocumentOrderTexts().subscribe(data => (this.pdfOrdersTexts = data));
        this.getOrdersStatuses().subscribe(data => (this.statusesCurrentLanguage = data));
    }

    //Service Properties
    private itemsInOrder = [];
    getItemsInOrder(context) {
        // function is called when generating pdf, downloading excel or updating view
        if (context == "view") return this.itemsInOrder;
        let itemsInOrderCopy = JSON.parse(JSON.stringify(this.itemsInOrder));
        if (context == "pdf") {
            itemsInOrderCopy.forEach(obj => {
                obj["item_amount_formatted"] = this._currencyPipe.transform(obj["item_amount"], this.currency, "", "1.1-1", this.language).trim();
                obj["item_buy_price_formatted"] = this._currencyPipe.transform(
                    obj["item_buy_price"],
                    this.currency,
                    "symbol-narrow",
                    "",
                    this.language
                );
                obj["item_total_price_formatted"] = this._currencyPipe.transform(
                    obj["item_total_price"],
                    this.currency,
                    "symbol-narrow",
                    "",
                    this.language
                );
            });
            return itemsInOrderCopy;
        }
        if (context == "excel") {
            itemsInOrderCopy.forEach(obj => {
                let amountAsLocaleString = this._currencyPipe.transform(obj["item_amount"], this.currency, "", "1.1-1", this.language).trim();
                obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(amountAsLocaleString);
                let ibpAsLocaleString = this._currencyPipe.transform(obj["item_buy_price"], this.currency, "", "", this.language).trim();
                obj["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(ibpAsLocaleString);
                let itpAsLocaleString = this._currencyPipe.transform(obj["item_total_price"], this.currency, "", "", this.language).trim();
                obj["item_total_price"] = this._sharedService.parseFormattedStringAsNumber(itpAsLocaleString);
            });
            return itemsInOrderCopy;
        }
    }

    private statusesCurrentLanguage;
    getOrdersStatusesForSelect() {
        return this.statusesCurrentLanguage;
    }

    private currentOrder = {}; // to hold the currently selected order
    setCurrentOrder(order) {
        this.currentOrder = order;
    }

    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemList(data) {
        this.itemsList.next(data);
    }

    private ordersList = new BehaviorSubject<any>([]);
    currentOrdersList = this.ordersList.asObservable();
    updateOrdersList(data) {
        this.ordersList.next(data);
    }
    // to hold the new info order
    private newInfoOrder = new BehaviorSubject<any>({});
    orderToPushOrUpdate = this.newInfoOrder.asObservable();
    passOrderToPushOrUpdate(data: any) {
        this.newInfoOrder.next(data);
    }

    getOrdersStatuses() {
        let url = `${this.apiUrl}/ordersStatuses?language=${this.language}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }
    getOrders(state) {
        let url = `${this.apiUrl}/orders?company_id=${this.company_id}&order_state=${state}&language=${this.language}`;
        return this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateOrdersList(data));
    }

    getOrdersByPattern(state, searchPattern) {
        let url = `${this.apiUrl}/ordersByPattern?company_id=${this.company_id}&order_state=${state}&search_pattern=${searchPattern}`;
        return this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateOrdersList(data));
    }

    getItemsFromOrder(id) {
        let url = `${this.apiUrl}/itemsInOrders?order_id=${id}`;
        this.http
            .get(url)
            .pipe(map(response => response["data"]))
            .subscribe(data => this.updateItemList(data));
    }

    createOrder(body) {
        // to provide language for the created order status
        let url = `${this.apiUrl}/orders?language=${this.language}`;
        body.company_id = this.company_id;
        body.order_state = 1;
        body.user_id = this.user_id;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateOrder(body) {
        let order_id = body.order_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/orders?order_id=${order_id}&language=${this.language}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertItemsInOrder(collection) {
        let url = `${this.apiUrl}/itemsInOrders`;
        return this.http.post(url, collection).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteOrder(order_id) {
        let deleteorderUrl = `${this.apiUrl}/orders?order_id=${order_id}`;
        return this.http.delete(deleteorderUrl).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateItemInOrder(body) {
        let iio_id = body.iio_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/itemsInOrders?iio_id=${iio_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    closeOrder(order_id) {
        let body = {};
        let url = `${this.apiUrl}/closeOrder?order_id=${order_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteItemFromOrder(iio_id) {
        let url = `${this.apiUrl}/itemsInOrders?iio_id=${iio_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getCurrentOrderNumber() {
        let url = `${this.apiUrl}/documentNumber?company_id=${this.company_id}&document_name=orders`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }
    
    getDocumentOrderTexts() {
        let url = `${this.apiUrl}/documentContext?company_id=${this.company_id}&language=${this.language}&document_name=orders`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    // to hold an observable of the items in the offer
    private orderTotalPriceBS = new BehaviorSubject<any>({});
    orderTotalPriceModelObs = this.orderTotalPriceBS.asObservable();

    calculateOrderTotalPriceModel() {
        let documentTotalModel = {};
        let subTotalPrice = this.itemsInOrder.reduce((memo: any, obj: any) => {
            return memo + parseFloat(obj["item_total_price"]);
        }, 0);
        const vat_porcentage = this.currentOrder["vat_percentage"] || 0;
        const vat = ((vat_porcentage / 100) * subTotalPrice).toFixed(2);
        const documentTotalPrice = (1 + vat_porcentage / 100) * subTotalPrice;
        documentTotalModel["subTotal"] = subTotalPrice.toFixed(2);
        documentTotalModel["vat"] = vat;
        documentTotalModel["total"] = documentTotalPrice.toFixed(2);
        this.orderTotalPriceBS.next(documentTotalModel);
    }
}
