import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { SharedService } from './shared.service';
import { ConfigService } from './config.service';

@Injectable({
	providedIn: 'root'
})

export class FiltersService {

	//Global Info App
	apiUrl: string = this._sharedService.apiUrl;
	sessionInfo;
	company_id;
	user_id;
	user_selected_language;
	materials; // to hold the array of materials

	constructor(
		private _configService: ConfigService,
		private _sharedService: SharedService,
		private http: HttpClient
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.sessionInfo = data;
			this.company_id = this.sessionInfo['company_id'];
			this.user_id = this.sessionInfo['user_id'];
			this.user_selected_language = this.sessionInfo['user_selected_language'];
		});
	}

	// to hold the new group
	private newFilterGroup = new BehaviorSubject<any>({});
	newFilterGroupToPush = this.newFilterGroup.asObservable();
	passNewFilterGroup(data: any) {
		this.newFilterGroup.next(data);
	}
	// BeahaviorSubjectForFilterValuesList
	private filterValuesList = new BehaviorSubject<any[]>([]);
	currentFilterValuesList = this.filterValuesList.asObservable();
	updateFilterValuesList(data) {
		this.filterValuesList.next(data);
	}

	private currentFilterGroup = {}; // to hold the currently selected order
	setCurrentFilterGroup(fg) {
		this.currentFilterGroup = fg;
	}

	private filterGroupsList = new BehaviorSubject<any>([]);
	currentFilterGroupsList = this.filterGroupsList.asObservable();
	updateFilterGroupsList(data) {
		this.filterGroupsList.next(data);
	}

	setMaterialsGroup(mg) {
		let materials = [];
		let collection = mg.array_of_filters;
		this.materials = this.formatMaterials(collection);
	}
	getMaterials() {
		return this.materials;
	}

	saveFiltersForItem(collectionOfFilters) {
		let url = `${this.apiUrl}/itemsWithFilters`;
		return this.http.post(url, collectionOfFilters).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	getFiltersGroups() {
		let url = `${this.apiUrl}/filterGroups?company_id=${this.company_id}`;
		this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		).subscribe(data => this.updateFilterGroupsList(data));
	}

	getFilterGroupsByPattern(searchPattern) {
		let url = `${this.apiUrl}/filterGroupsByPattern?company_id=${this.company_id}&search_pattern=${searchPattern}`;
		this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		).subscribe(data => this.updateFilterGroupsList(data));
	}

	getFiltersBySearchPattern(searchPattern) {
		let url = `${this.apiUrl}/filters?search_pattern=${searchPattern}&company_id=${this.company_id}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getFiltersModel() {
		let url = `${this.apiUrl}/filterSModel?company_id=${this.company_id}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createFilterGroup(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/filterGroups`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateFilterGroup(body) {
		let filter_group_id = body.filter_group_id;
		let url = `${this.apiUrl}/filterGroups?filter_group_id=${filter_group_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteFilterGroup(fg_id) {
		let url = `${this.apiUrl}/filterGroups?filter_group_id=${fg_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getFiltersValues(fg_id) {
		let url = `${this.apiUrl}/filterValues?filter_group_id=${fg_id}&company_id=${this.company_id}`;
		this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
			.subscribe(data => this.updateFilterValuesList(data));
	}

	createFilterValue(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/filterValues`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateFilterValue(body) {
		let url = `${this.apiUrl}/filterValues`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteFilterValue(filter_id) {
		let url = `${this.apiUrl}/filterValues?filter_id=${filter_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getItemFilters(item_id) {
		let url = `${this.apiUrl}/itemsWithFilters?item_id=${item_id}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}

	deleteItemFilter(iwf_id) {
		let url = `${this.apiUrl}/itemsWithFilters?iwf_id=${iwf_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		)
	}


	formatMaterials(collection) {
		// to make material model to play nice with the select
		let formattedMaterials = {};
		formattedMaterials['material_names'] = [];
		formattedMaterials['material_ids'] = [];
		formattedMaterials['get_material_id'] = material => {
			let index = formattedMaterials['material_names'].indexOf(material);
			let filter_id = formattedMaterials['material_ids'][index];
			return filter_id;
		};
		collection.forEach(obj => {
			formattedMaterials['material_names'].push(obj['filter_value']);
			formattedMaterials['material_ids'].push(obj['filter_id']);
		});
		formattedMaterials['original_data'] = collection;
		return formattedMaterials;
	}
}
