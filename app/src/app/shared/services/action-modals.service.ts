import { Injectable } from "@angular/core";
import swal from "sweetalert2";
import { SharedService } from "./shared.service";
import { LimitToPipe } from "../pipes/limit-to/limit-to.pipe";

@Injectable({
    providedIn: "root"
})
export class ActionModalsService {
    constructor(private _sharedService: SharedService, private _limitTo: LimitToPipe) {}

    buildConfirmModal(title, text) {
        const cl = this._sharedService.user_selected_language;
        let formattedTitle = this._limitTo.transform(title, [20]);
        return swal({
            position: "center",
            type: "question",
            title: formattedTitle,
            text: text,
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: ModalLanguages[cl].confirmButton,
            cancelButtonText: ModalLanguages[cl].cancelButton
        });
    }

    buildSuccessModal(title, text) {
        let formattedTitle = this._limitTo.transform(title, [20]);
        return swal({
            position: "center",
            type: "success",
            title: formattedTitle,
            text: text
        });
    }

    buildInfoModal(title, text) {
        let formattedTitle = this._limitTo.transform(title, [20]);
        return swal({
            position: "center",
            type: "info",
            title: formattedTitle,
            text: text
        });
    }

    alertError(text) {
        return swal({
            position: "center",
            type: "error",
            text: text
        });
    }
}
class ModalLanguages {
    static en = {
        confirmButton: "Confirm Delete",
        cancelButton: "Cancel"
    };
    static de = {
        confirmButton: "Bestätigen",
        cancelButton: "Zurrück"
    };
    static es = {
        confirmButton: "Confirmar",
        cancelButton: "Cancelar"
    };
}
