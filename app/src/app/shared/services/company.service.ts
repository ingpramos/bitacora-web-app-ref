import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { SharedService } from './shared.service';
import { ConfigService } from './config.service';

@Injectable({
	providedIn: 'root'
})

export class CompanyService {

	apiUrl: string = this._sharedService.apiUrl;
	selectedFile: File = null;
	company_id;
	user_id;
	user_selected_language;

	constructor(
		private _configService: ConfigService,
		private _sharedService: SharedService,
		private http: HttpClient
	) {
		this._sharedService.sessionInfo.subscribe(data => {
			this.company_id = data.company_id;
			this.user_id = data.user_id;
			this.user_selected_language = data.user_selected_language;
		});
	}

	// to hold the created or updated iBAN
	private newInfoIban = new BehaviorSubject<any>({});
	ibanToPushOrUpdate = this.newInfoIban.asObservable();
	passIbanToPushOrUpdate(data: any) {
		this.newInfoIban.next(data);
	}
	
	// to hold the updated order_confirmation
	private updatedCompanyNumbers = new BehaviorSubject<any>({});
	updatedCompanyNumbersToReplace = this.updatedCompanyNumbers.asObservable();
	passUpdatedCompanyNumbers(data: any) {
		this.updatedCompanyNumbers.next(data);
	}
	// to hold the new payment method
	private newInfoPaymentMethod = new BehaviorSubject<any>({});
	paymentMethodToPushOrUpdate = this.newInfoPaymentMethod.asObservable();
	passPaymentMethodToPushOrUpdate(data: any) {
		this.newInfoPaymentMethod.next(data);
	}
	// to hold the new payment condition
	private newInfoPaymentCondition = new BehaviorSubject<any>({});
	paymentConditionToPushOrUpdate = this.newInfoPaymentCondition.asObservable();
	passPaymentCondtionToPushOrUpdate(data: any) {
		this.newInfoPaymentCondition.next(data);
	}
	// to hold the new vat
	private newVat = new BehaviorSubject<any>(0);
	newVatToPush = this.newVat.asObservable();
	passNewVat(data: any) {
		this.newVat.next(data);
	}

	// to hold the new delivery method
	private newInfoDeliveryMethod = new BehaviorSubject<any>({});
	deliveryMethodToPushOrUpdate = this.newInfoDeliveryMethod.asObservable();
	passDeliveryMethodToPushOrUpdate(data: any) {
		this.newInfoDeliveryMethod.next(data);
	}
	// to hold the new delivery condition
	private newInfoDeliveryCondition = new BehaviorSubject<any>({});
	deliveryConditionToPushOrUpdate = this.newInfoDeliveryCondition.asObservable();
	passDeliveryConditionToPushOrUpdate(data: any) {
		this.newInfoDeliveryCondition.next(data);
	}
	// to hold the updated vat
	private updatedVat = new BehaviorSubject<any>([]);
	updatedVatToReplace = this.updatedVat.asObservable();
	passUpdatedVat(data: any) {
		this.updatedVat.next(data);
	}

	generateCompanyID() {
		let text = "";
		let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 10; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}

	getCompanyInfoByUserId(user_id) {
		let url = `${this.apiUrl}/companies?user_id=${user_id}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getSessionInfo(user_id) {
		let url = `${this.apiUrl}/companies?user_id=${user_id}`;
		return this.http.get(url);
	}

	createCompany(body) {
		let url = `${this.apiUrl}/signUp`;
		return this.http.post(url, body).pipe(
			map(response => response),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateCompany(body) {
		let company_id = body.company_id;
		let url = `${this.apiUrl}/companies?company_id=${company_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createIban(body) {
		let url = `${this.apiUrl}/companiesBanks`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateIban(body) {
		let company_iban_id = body.company_iban_id;
		let url = `${this.apiUrl}/companiesBanks?company_iban_id=${company_iban_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteIban(company_iban_id) {
		let url = `${this.apiUrl}/companiesBanks?company_iban_id=${company_iban_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	uploadPhoto(data) {
		let fd = new FormData();
		fd.append('logo', data);
		fd.append('company_id', data.company_id);
		fd.append('company_images', data.company_images);

		let url = `${this.apiUrl}/uploadFile`;;
		return this.http.post(url, fd).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	getPaymentMethods(company_id, language) {
		let url = `${this.apiUrl}/companiesPaymentMethods?company_id=${company_id}&language=${language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}
	createPaymentMethod(body) {
		let url = `${this.apiUrl}/companiesPaymentMethods`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updatePaymentMethod(body) {
		let payment_method_id = body.payment_method_id;
		let url = `${this.apiUrl}/companiesPaymentMethods?payment_method_id=${payment_method_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deletePaymentMethod(pm_id) {
		let url = `${this.apiUrl}/companiesPaymentMethods?payment_method_id=${pm_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	getPaymentConditions(company_id, language) {
		let url = `${this.apiUrl}/companiesPaymentConditions?company_id=${company_id}&language=${language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createPaymentCondition(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/companiesPaymentConditions`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	createVat(body) {
		body['company_id'] = this.company_id;
		let url = `${this.apiUrl}/companiesVats`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateVat(body) {
		body['company_id'] = this.company_id;
		let url = `${this.apiUrl}/companiesVats`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteVat(vat) {
		let url = `${this.apiUrl}/companiesVats?company_id=${this.company_id}&vat_to_delete=${vat}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	updatePaymentCondition(body) {
		let payment_condition_id = body.payment_condition_id;
		let url = `${this.apiUrl}/companiesPaymentConditions?payment_condition_id=${payment_condition_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deletePaymentCondition(pc_id) {
		let url = `${this.apiUrl}/companiesPaymentConditions?payment_condition_id=${pc_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	createDeliveryMethod(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/companiesDeliveryMethods`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateDeliveryMethod(body) {
		let url = `${this.apiUrl}/companiesDeliveryMethods`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteDeliveryMethod(dm_id) {
		let url = `${this.apiUrl}/companiesDeliveryMethods?delivery_method_id=${dm_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}

	createDeliveryCondition(body) {
		body.company_id = this.company_id;
		let url = `${this.apiUrl}/companiesDeliveryConditions`;
		return this.http.post(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateDeliveryCondition(body) {
		let dc_id = body.delivery_condition_id;
		let url = `${this.apiUrl}/companiesDeliveryConditions?delivery_condition_id=${dc_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	deleteDeliveryCondition(delivery_condition_id) {
		let dc_id = delivery_condition_id;
		let url = `${this.apiUrl}/companiesDeliveryConditions?delivery_condition_id=${dc_id}`;
		return this.http.delete(url).pipe(
			map(response => response['data']),
			catchError(this._configService.handleError)
		);
	}


	getDocuments() {
		let url = `${this.apiUrl}/documentsConfiguration?company_id=${this.company_id}&language=${this.user_selected_language}`;
		return this.http.get(url).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	updateDocument(body) {
		let dc_id = body.dc_id;
		body.dct_language = this.user_selected_language;
		let url = `${this.apiUrl}/documentsConfiguration?dc_id=${dc_id}`;
		return this.http.put(url, body).pipe(
			map(response => response['data']),
			retry(3),
			catchError(this._configService.handleError)
		);
	}

	formatPaymentMethods(collection) { // to make method model to play nice with the select
		let formattedPaymentMethods = {};
		formattedPaymentMethods['payment_methods_names'] = [];
		formattedPaymentMethods['payment_methods_ids'] = [];
		formattedPaymentMethods['getPaymentMethod_id'] = (method) => {
			let methodIndex = formattedPaymentMethods['payment_methods_names'].indexOf(method);
			let method_id = formattedPaymentMethods['payment_methods_ids'][methodIndex];
			return method_id;
		};
		collection.forEach((obj) => {
			formattedPaymentMethods['payment_methods_names'].push(obj['payment_method_name']);
			formattedPaymentMethods['payment_methods_ids'].push(obj['payment_method_id']);
		});
		return formattedPaymentMethods;
	}

	formatPaymentConditions(collection) { // to make condition model to play nice with the select
		let formattedPaymentConditions = {};
		formattedPaymentConditions['payment_conditions_descriptions'] = [];
		formattedPaymentConditions['payment_conditions_ids'] = [];
		formattedPaymentConditions['getPaymentCondition_id'] = (condition) => {
			let conditionIndex = formattedPaymentConditions['payment_conditions_descriptions'].indexOf(condition);
			let condition_id = formattedPaymentConditions['payment_conditions_ids'][conditionIndex];
			return condition_id;
		};
		collection.forEach((obj) => {
			formattedPaymentConditions['payment_conditions_descriptions'].push(obj['payment_condition_description']);
			formattedPaymentConditions['payment_conditions_ids'].push(obj['payment_condition_id']);
		});
		return formattedPaymentConditions;
	}

	formatDeliveryMethods(collection) { // to make method model to play nice with the select
		let formattedDeliveryMethods = {};
		formattedDeliveryMethods['delivery_methods_names'] = [];
		formattedDeliveryMethods['delivery_methods_ids'] = [];
		formattedDeliveryMethods['getDeliveryMethod_id'] = (method) => {
			let methodIndex = formattedDeliveryMethods['delivery_methods_names'].indexOf(method);
			let method_id = formattedDeliveryMethods['delivery_methods_ids'][methodIndex];
			return method_id;
		};
		collection.forEach((obj) => {
			formattedDeliveryMethods['delivery_methods_names'].push(obj['delivery_method_name']);
			formattedDeliveryMethods['delivery_methods_ids'].push(obj['delivery_method_id']);
		});
		return formattedDeliveryMethods;
	}

	formatDeliveryConditions(collection) { // to make condition model to play nice with the select
		let formattedDeliveryConditions = {};
		formattedDeliveryConditions['delivery_conditions_descriptions'] = [];
		formattedDeliveryConditions['delivery_conditions_ids'] = [];
		formattedDeliveryConditions['getDeliveryCondition_id'] = (condition) => {
			let conditionIndex = formattedDeliveryConditions['delivery_conditions_descriptions'].indexOf(condition);
			let condition_id = formattedDeliveryConditions['delivery_conditions_ids'][conditionIndex];
			return condition_id;
		};
		collection.forEach((obj) => {
			formattedDeliveryConditions['delivery_conditions_descriptions'].push(obj['delivery_condition_description']);
			formattedDeliveryConditions['delivery_conditions_ids'].push(obj['delivery_condition_id']);
		});
		return formattedDeliveryConditions;
	}

}
