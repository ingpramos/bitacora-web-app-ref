import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class OffersService {
    //Global Info App
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    user_selected_language;
    apiUrl: string = this._sharedService.apiUrl;
    pdfOffersTexts: any = {};
    constructor(private _configService: ConfigService, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
        this.getDocumentOfferTexts().subscribe(data => (this.pdfOffersTexts = data));
    }

    // to hold the created or updated offer
    private newInfoOffer = new BehaviorSubject<any>({});
    offerToPushOrUpdate = this.newInfoOffer.asObservable();
    passOfferToPushOrUpdate(data: any) {
        this.newInfoOffer.next(data);
    }

    // to hold an observable of the items in the offer
    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemsList(data) {
        this.itemsList.next(data);
    }
    // to hold an observable of the current offers
    private offersList = new BehaviorSubject<any>([]);
    currentOffersList = this.offersList.asObservable();
    updateOffersList(data) {
        this.offersList.next(data);
    }

    getOffers(state) {
        let url = `${this.apiUrl}/offers?company_id=${this.company_id}&offer_state=${state}&language=${this.user_selected_language}`;
        this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateOffersList(data));
    }

    getOffersByPattern(state, searchPattern) {
        let url = `${this.apiUrl}/offersByPattern?company_id=${this.company_id}&offer_state=${state}&language=${this.user_selected_language}&search_pattern=${searchPattern}`;
        return this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateOffersList(data));
    }

    getFormattedOffers(state) {
        let url = `${this.apiUrl}/offers?company_id=${this.company_id}&offer_state=${state}&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response => this.formatOpenOffers(response["data"])),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    createOffer(body) {
        let url = `${this.apiUrl}/offers`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateOffer(body) {
        let id_id = body.internal_document_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/offers?internal_document_id=${id_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteOffer(offer_id) {
        let url = `${this.apiUrl}/offers?company_id=${this.company_id}&internal_document_id=${offer_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    closeOffer(offer_id) {
        let body = {};
        let url = `${this.apiUrl}/closeOffer?internal_document_id=${offer_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }


    getCurrentOfferNumber() {
        let url = `${this.apiUrl}/documentNumber?company_id=${this.company_id}&document_name=offers`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getDocumentOfferTexts() {
        let url = `${this.apiUrl}/documentContext?company_id=${this.company_id}&language=${this.user_selected_language}&document_name=offers`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    formatOpenOffers(collection) {
        // to make method model to play nice with the select
        let formattedOpenOffers = {};
        formattedOpenOffers["offer_numbers"] = [];
        formattedOpenOffers["internal_documents_ids"] = [];
        formattedOpenOffers["get_internal_document_id"] = method => {
            let documentIndex = formattedOpenOffers["offer_numbers"].indexOf(method);
            let internal_document_id = formattedOpenOffers["internal_documents_ids"][documentIndex];
            return internal_document_id;
        };
        collection.forEach(obj => {
            formattedOpenOffers["offer_numbers"].push(obj["offer_number"]);
            formattedOpenOffers["internal_documents_ids"].push(obj["internal_document_id"]);
        });
        formattedOpenOffers["original_data"] = collection;
        return formattedOpenOffers;
    }
}
