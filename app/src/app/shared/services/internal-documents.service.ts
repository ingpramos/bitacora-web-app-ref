import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { CurrencyPipe } from "@angular/common";
import { SharedService } from "./shared.service";
import { HttpClient } from "@angular/common/http";
import { catchError, map, retry } from "rxjs/operators";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})

export class InternalDocumentsService {
    sessionInfo = {}; // this is the source of all ids like company_id or user_id
    company_id = "";
    user_id;
    currency: string;
    language: string;
    apiUrl: string = this._sharedService.apiUrl;

    constructor(private _configService: ConfigService, private cp: CurrencyPipe, private _sharedService: SharedService, private http: HttpClient) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.currency = this.sessionInfo["company_currency"];
            this.language = this.sessionInfo["user_selected_language"];
        });
    }
    // to hold an observable of the items in the offer
    private itemsList = new BehaviorSubject<any>([]);
    currentItemsList = this.itemsList.asObservable();
    updateItemsList(data) {
        this.itemsList.next(data);
    }

    getItemsFromInternalDocument(offer_id) {
        let url = `${this.apiUrl}/itemsInOffers?internal_document_id=${offer_id}`;
        this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateItemsList(data));
	}

	insertItemsInInternalDocument(collection) {
        let url = `${this.apiUrl}/itemsInOffers`;
        return this.http.post(url, collection).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertAssembliesInInternalDocument(collection) {
        let url = `${this.apiUrl}/assembliesInOffers`;
        return this.http.post(url, collection).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateItemInInternalDocument(body) {
        let iiid_id = body.item_in_internal_document_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/itemsInOffers?item_in_internal_document_id=${iiid_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateAssemblyInInternalDocument(body) {
        let aiid_id = body.item_in_internal_document_id;
        body.user_id = this.user_id;
        let url = `${this.apiUrl}/assemblyInOffers?aiid_id=${aiid_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }
	
	deleteItemFromInternalDocument(iiid_id) {
        let url = `${this.apiUrl}/itemsInOffers?item_in_internal_document_id=${iiid_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
	}
	
    deleteAssemblyFromInternalDocument(aiid_id) {
        let url = `${this.apiUrl}/assemblyInOffers?aiid_id=${aiid_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    // to calcule current price model for offers, order confirmation and invoices
    private currentDocument = {};
    setCurrentDocument(doc) {
        this.currentDocument = doc;
    }
    private itemsOfCurrentDocument = [];
    setItemsOfCurrentDocument(items) {
        this.itemsOfCurrentDocument = items;
    }

    getItemsOfCurrentDocument(context) {
        if (context == "view") return this.itemsOfCurrentDocument;
        let itemsOfCurrentDocumentCopy = JSON.parse(JSON.stringify(this.itemsOfCurrentDocument));
        if (context == "pdf") {
            itemsOfCurrentDocumentCopy.forEach(obj => {
                obj["item_amount_formatted"] = this.cp.transform(obj["item_amount"], this.currency, "", "1.1-1", this.language);
                obj["item_sell_price_formatted"] = this.cp.transform(obj["item_sell_price"], this.currency, "symbol-narrow", "", this.language);
                obj["item_buy_price_formatted"] = this.cp.transform(obj["item_buy_price"], this.currency, "symbol-narrow", "", this.language);
                obj["item_total_price_formatted"] = this.cp.transform(obj["item_total_price"], this.currency, "symbol-narrow", "", this.language);
            });
            return itemsOfCurrentDocumentCopy;
        }
        if (context == "excel") {
            itemsOfCurrentDocumentCopy.forEach(obj => {
                let amountAsLocaleString = this.cp.transform(obj["item_amount"], this.currency, "", "1.1-1", this.language).trim();
                obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(amountAsLocaleString);
                let ispAsLocaleString = this.cp.transform(obj["item_sell_price"], this.currency, "", "", this.language).trim();
                obj["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(ispAsLocaleString);
                let itpAsLocaleString = this.cp.transform(obj["item_total_price"], this.currency, "", "", this.language).trim();
                obj["item_total_price"] = this._sharedService.parseFormattedStringAsNumber(itpAsLocaleString);
            });
            return itemsOfCurrentDocumentCopy;
        }
    }
    // to hold an observable of the total price model
    private documentTotalPriceBS = new BehaviorSubject<any>({});
    documentTotalPriceModelObs = this.documentTotalPriceBS.asObservable();

    calculateDocumentTotalPriceModel() {
        let documentTotalModel = {};
        let subTotalPrice = this.itemsOfCurrentDocument.reduce((memo: any, obj: any) => {
            return memo + parseFloat(obj["item_total_price"]);
        }, 0);
        const vat_percentage = this.currentDocument["vat_percentage"] || 0;
        const vat = ((vat_percentage / 100) * subTotalPrice).toFixed(2);
        const documentTotalPrice = (1 + vat_percentage / 100) * subTotalPrice;
        documentTotalModel["subTotal"] = subTotalPrice.toFixed(2);
        documentTotalModel["vat"] = vat;
        documentTotalModel["total"] = documentTotalPrice.toFixed(2);
        this.documentTotalPriceBS.next(documentTotalModel);
    }
}
