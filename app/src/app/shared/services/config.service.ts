import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';


@Injectable({
	providedIn: 'root'
})
export class ConfigService {

	constructor() {
	}

	handleError(error: any) {
		if (error.error instanceof ErrorEvent) {
			// A client-side or network error occurred. Handle it accordingly.
			console.error('An error occurred:', error);
		} else {
			// The backend returned an unsuccessfull response code.
			// The response body may contain clues as to what went wrong,
			console.error(error);
		}
		//return an observable with a user-facing error message
		return throwError(error);
	};
}
