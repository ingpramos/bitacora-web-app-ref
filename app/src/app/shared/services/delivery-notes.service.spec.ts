import { TestBed, inject } from '@angular/core/testing';

import { DeliveryNotesService } from './delivery-notes.service';

describe('DeliveryNotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliveryNotesService]
    });
  });

  it('should be created', inject([DeliveryNotesService], (service: DeliveryNotesService) => {
    expect(service).toBeTruthy();
  }));
});
