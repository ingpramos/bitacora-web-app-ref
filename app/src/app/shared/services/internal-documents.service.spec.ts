import { TestBed } from '@angular/core/testing';

import { InternalDocumentsService } from './internal-documents.service';

describe('InternalDocumentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternalDocumentsService = TestBed.get(InternalDocumentsService);
    expect(service).toBeTruthy();
  });
});
