import { TestBed, inject } from '@angular/core/testing';

import { ConfirmationsService } from './confirmations.service';

describe('ConfirmationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfirmationsService]
    });
  });

  it('should be created', inject([ConfirmationsService], (service: ConfirmationsService) => {
    expect(service).toBeTruthy();
  }));
});
