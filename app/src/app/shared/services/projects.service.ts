import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { catchError, map, retry } from "rxjs/operators";
import { SharedService } from "./shared.service";
import { ConfigService } from "./config.service";

@Injectable({
    providedIn: "root"
})
export class ProjectsService {
    //Global Info App
    apiUrl: string = this._sharedService.apiUrl;
    sessionInfo;
    company_id;
    user_id;
    user_selected_language;

    constructor(private _configService: ConfigService, private http: HttpClient, private _sharedService: SharedService) {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.company_id = this.sessionInfo["company_id"];
            this.user_id = this.sessionInfo["user_id"];
            this.user_selected_language = this.sessionInfo["user_selected_language"];
        });
    }

    // to hold the array of projects
    private projectsList = new BehaviorSubject<any[]>([]);
    currentProjectsList = this.projectsList.asObservable();
    updateProjectsList(data: any) {
        this.projectsList.next(data);
    }
    // to hold new project object
    private newInfoProject = new BehaviorSubject<any>({});
    projectToPushOrUpdate = this.newInfoProject.asObservable();
    passProjectToPushOrUpdate(data: any) {
        this.newInfoProject.next(data);
    }

    // to hold the selected project
    private cProject = new BehaviorSubject<any>({});
    currentProject = this.cProject.asObservable();
    setCurrentProject(project) {
        this.cProject.next(project);
    }
    // to hold assemblies in projects
    private cGroups = new BehaviorSubject<any[]>([]);
    currentGroupsList = this.cGroups.asObservable();
    setCurrentGroups(assemblies) {
        this.cGroups.next(assemblies);
    }
    // to hold one new assembly object
    private newGroup = new BehaviorSubject<any>({});
    newGroupToPush = this.newGroup.asObservable();
    passNewGroup(data: any) {
        this.newGroup.next(data);
    }

    // to hold a collection of sub_assemblies inserted in projects
    private collectionOfInsertedAssemblies = new BehaviorSubject<any>([]);
    subAssembliesToPush = this.collectionOfInsertedAssemblies.asObservable();
    passInsertedSubassemblies(data: any) {
        this.collectionOfInsertedAssemblies.next(data);
    }

    getProjects(state) {
        let url = `${this.apiUrl}/projects?company_id=${this.company_id}&project_state=${state}`;
        this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateProjectsList(data));
    }

    getProjectsByPattern(state, searchPattern) {
        let url = `${this.apiUrl}/projectsByPattern?company_id=${this.company_id}&project_state=${state}&search_pattern=${searchPattern}`;
        this.http
            .get(url)
            .pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.updateProjectsList(data));
    }

    getAvailableOrdersConfirmation() {
        let url = `${this.apiUrl}/ordersConfirmations?company_id=${this.company_id}&offer_state=2&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response => this.formatProjects(response["data"])),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getProjectById(id) {
        let url = `${this.apiUrl}/projects?project_id=${id}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    createProject(body) {
        let url = `${this.apiUrl}/projects`;
        body["company_id"] = this.company_id;
        body["project_state"] = 1;
        body["project_creation_date"] = new Date().toISOString;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    updateProject(body) {
        const project_id = body.project_id;
        let url = `${this.apiUrl}/projects?project_id=${project_id}`;
        return this.http.put(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    deleteProject(project_id) {
        let url = `${this.apiUrl}/projects?project_id=${project_id}`;
        return this.http.delete(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    closeProject(project_id) {
        let url = `${this.apiUrl}/closeProject?project_id=${project_id}`;
        return this.http.put(url, {}).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getAssembliesFromProject(project_id) {
        let url = `${this.apiUrl}/assembliesInProjects?project_id=${project_id}`;
        return this.http.get(url).pipe(
                map(response => response["data"]),
                retry(3),
                catchError(this._configService.handleError)
            )
            .subscribe(data => this.setCurrentGroups(data));
    }

    createAssemblyInProject(body, project_id) {
        body.company_id = this.company_id;
        let url = `${this.apiUrl}/assemblyInProject?project_id=${project_id}`;
        return this.http.post(url, body).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertAssembliesInProject(array_of_assembly_ids, project_id) {
        let url = `${this.apiUrl}/assembliesInProjects?project_id=${project_id}&user_id=${this.user_id}`;
        return this.http.post(url, array_of_assembly_ids).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    insertSubAssembliesInProject(array_of_assembly_ids, aip_id) {
        let url = `${this.apiUrl}/subAssembliesInProjects?aip_id=${aip_id}&user_id=${this.user_id}`;
        return this.http.post(url, array_of_assembly_ids).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    getProjectActivitiesCosts(project_id) {
        let url = `${this.apiUrl}/projectActivitiesCosts?project_id=${project_id}&language=${this.user_selected_language}`;
        return this.http.get(url).pipe(
            map(response => response["data"]),
            retry(3),
            catchError(this._configService.handleError)
        );
    }

    calculateTotalActivitiesCostModel(collection) {
        let costModel = {};
        let totalActivitiesCost = collection.reduce((memo: any, obj: any) => {
            return memo + parseFloat(obj["activity_total_cost"]);
        }, 0);
        costModel["totalActivitiesCost"] = totalActivitiesCost.toFixed(2);
        collection.forEach(obj => {
            if (totalActivitiesCost > 0) {
                obj["activity_percentage"] = ((obj.activity_total_cost / totalActivitiesCost) * 100).toFixed(2);
            } else {
                obj["activity_percentage"] = 0;
            }
        });
        costModel["activitiesResume"] = collection;
        return costModel;
    }

    formatProjects(collection) {
        let formattedProjects = {};
        formattedProjects["orders_confirmation_numbers"] = []; // orders confirmation numbers
        formattedProjects["documents_ids"] = []; //
        formattedProjects["getDocuments_id"] = oc_numbers => {
            let selected_projects_ids = [];
            oc_numbers.forEach((oc_number: string) => {
                let index = formattedProjects["orders_confirmation_numbers"].indexOf(oc_number);
                selected_projects_ids.push(formattedProjects["documents_ids"][index]);
            });
            return selected_projects_ids;
        };
        collection.forEach(obj => {
            formattedProjects["orders_confirmation_numbers"].push(obj["order_confirmation_number"]);
            formattedProjects["documents_ids"].push(obj["internal_document_id"]);
        });
        return formattedProjects;
    }
}
