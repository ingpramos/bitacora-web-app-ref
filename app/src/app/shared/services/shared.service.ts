import { Injectable } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { environment } from "../../../environments/environment";
import { CurrencyPipe } from "@angular/common";

@Injectable({
    providedIn: "root"
})

export class SharedService {
    env = environment;
    // Shared Variables
    rootUrl: string = this.env.apiProtocol + this.env.apiHost + ":" + this.env.apiPort;
    apiUrl: string = this.rootUrl + "/api";
    company_id: string;
    user_id: any;
    user_selected_language: string;
    currency: string;
    sessionInfoData: any;
    imgCodeBase64: string;

    // session values
    payment_methods: any[];
    payment_conditions: any[];
    delivery_methods: any[];
    delivery_conditions: any[];
    ibans: any[];

    constructor(private cp: CurrencyPipe) {
        this.sidebarVisible = false;
        this.maTheme = "indigo";
        this.sessionInfo.subscribe(data => {
            this.sessionInfoData = data;
            this.company_id = data.company_id;
            this.user_id = data.user_id;
            this.user_selected_language = data.user_selected_language;
            this.currency = data.company_currency;
            this.payment_methods = data.payment_methods;
            this.payment_conditions = data.payment_conditions;
            this.delivery_methods = data.delivery_methods;
            this.delivery_conditions = data.delivery_conditions;
            this.ibans = data.ibans;
            const imgUrl = data.company_url_image_brand;
            this.getBase64UrlFromImage(imgUrl);
        });
    }

    private sessionInfoBS = new BehaviorSubject<any>({});
    sessionInfo = this.sessionInfoBS.asObservable();
    setSessionInfo(data) {
        this.sessionInfoBS.next(data);
    }

    // Sidebar visibility
    sidebarVisible: boolean;
    sidebarVisibilitySubject: Subject<boolean> = new Subject<boolean>();

    toggleSidebarVisibilty() {
        this.sidebarVisible = !this.sidebarVisible;
        this.sidebarVisibilitySubject.next(this.sidebarVisible);
    }
    // Theming
    maTheme: string;
    maThemeSubject: Subject<string> = new Subject<string>();

    setTheme(color) {
        this.maTheme = color;
        this.maThemeSubject.next(this.maTheme);
    }


    // to hold an observable of the total price model
    private documentTotalPriceBS = new BehaviorSubject<any>({});
    documentTotalPriceModelObs = this.documentTotalPriceBS.asObservable();

    getBase64UrlFromImage(imgUrl) {
        var img = new Image();
        img.setAttribute("crossOrigin", "anonymous");
        let url;
        if (imgUrl && imgUrl != null) {
            url = this.rootUrl + imgUrl;
            img.src = url;
        } else {
            url = "./../../../assets/img/default_logo.png";
            img.src = url;
        }

        img.onload = () => {
            let canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            let ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            let dataURL = canvas.toDataURL();
            this.imgCodeBase64 = dataURL;
        };
    }

    isObjectEquivalent(a, b) {
        // Create arrays of property names
        var aProps = Object.getOwnPropertyNames(a);
        var bProps = Object.getOwnPropertyNames(b);
        // If number of properties is different,
        // objects are not equivalent
        if (aProps.length != bProps.length) {
            return false;
        }
        for (var i = 0; i < aProps.length; i++) {
            var propName = aProps[i];
            // If values of same property are not equal,
            // objects are not equivalent
            if (a[propName] !== b[propName]) {
                return false;
            }
        }
        // If we made it this far, objects
        // are considered equivalent
        return true;
    }

    // to implement spinner functionality
    private _loading: boolean = false;
    loadingStatus: Subject<boolean> = new Subject();

    get loading(): boolean {
        return this._loading;
    }

    set loading(value) {
        this._loading = value;
        this.loadingStatus.next(value);
    }

    startLoading() {
        this.loading = true;
    }

    stopLoading() {
        this.loading = false;
    }

    parseToLocaleString(value) {
        return this.cp.transform(value, this.currency, "", "", this.user_selected_language);
    }

    parseFormattedStringAsNumber(fnum) {
        switch (this.user_selected_language) {
            case "en":
                return this.parseStringAsNumberLocaleEN(fnum);
            case "de":
                return this.parseStringAsNumberLocaleDE(fnum);
            case "es":
                return this.parseStringAsNumberLocaleES(fnum);
            default:
                return 0;
        }
    }

    parseStringAsNumberLocaleEN(numberAsString) {
        let stringWithOutThousandSeparator = numberAsString.replace(/,/g, "");
        let valueASNumber = Number(stringWithOutThousandSeparator);
        return valueASNumber;
    }

    parseStringAsNumberLocaleDE(numberAsString) {
        let stringWithOutThousandSeparator = numberAsString.replace(/\./g, "");
        let replaceCommaWithDot = stringWithOutThousandSeparator.replace(/,/g, ".");
        let valueASNumber = Number(replaceCommaWithDot);
        return valueASNumber;
    }

    parseStringAsNumberLocaleES(numberAsString) {
        let stringWithOutThousandSeparator = numberAsString.replace(/\./g, "");
        let replaceCommaWithDot = stringWithOutThousandSeparator.replace(/,/g, ".");
        let valueASNumber = Number(replaceCommaWithDot);
        return valueASNumber;
    }
}
