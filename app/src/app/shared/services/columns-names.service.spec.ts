import { TestBed, inject } from '@angular/core/testing';

import { ColumnsNamesService } from './columns-names.service';

describe('ColumnsNamesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ColumnsNamesService]
    });
  });

  it('should be created', inject([ColumnsNamesService], (service: ColumnsNamesService) => {
    expect(service).toBeTruthy();
  }));
});
