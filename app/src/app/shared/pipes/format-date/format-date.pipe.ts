import { Pipe, PipeTransform } from "@angular/core";
import { SharedService } from "../../services/shared.service";

@Pipe({
    name: "formatDate"
})
export class FormatDatePipe implements PipeTransform {
    constructor(private _sharedService: SharedService) {}
    cl = this._sharedService.user_selected_language;

    formatDate(date) {
        let d = new Date(date);
        let options = { year: "numeric", month: "numeric", day: "numeric" };
        return d.toLocaleDateString(this.cl, options);      
    }

    transform(value: any, args?: any): any {
        if (value) {
            let d = this.formatDate(value);
            return d;
        } else {
            return;
        }
    }
}
