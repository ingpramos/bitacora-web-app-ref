import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LimitToPipe } from './limit-to/limit-to.pipe';
import { FormatDatePipe } from './format-date/format-date.pipe';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        LimitToPipe,
        FormatDatePipe
    ],
    exports: [
        LimitToPipe,
        FormatDatePipe
    ]

})
export class PipesModule { }
