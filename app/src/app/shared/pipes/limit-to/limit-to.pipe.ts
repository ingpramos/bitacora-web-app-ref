import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'limitTo'
})
export class LimitToPipe implements PipeTransform {
	transform(value: string, args): string {
		if (value) {
			const limit = args[0] ? parseInt(args[0], 10) : 20;
			const trail = args[1] ? args[1] : '...';
			return value.length > limit ? value.substring(0, limit) + trail : value;
		}
		else {
			return 
		}
	}

}
