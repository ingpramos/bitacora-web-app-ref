import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared/services/shared.service';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  loading:boolean = true;

  constructor(
    private _sharedService : SharedService
  ) { }

  ngOnInit() {
    this._sharedService.loadingStatus.subscribe(value => this.loading = value);
  }

}
