import { Component } from '@angular/core';
import { RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError, Router } from '@angular/router';
import { SharedService } from './shared/services/shared.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})

export class AppComponent {
	constructor(
		private router: Router,
		private _sharedService : SharedService
		) {
		this.router.events.subscribe((event: RouterEvent) => {
		  this.navigationInterceptor(event)
		})
	  }
	  // Shows and hides the loading spinner during RouterEvent changes
	  navigationInterceptor(event: RouterEvent): void {
		if (event instanceof NavigationStart) {
		  this._sharedService.startLoading();
		}
		if (event instanceof NavigationEnd) {
			this._sharedService.stopLoading();
		}
	
		// Set loading state to false in both of the below events to hide the spinner in case a request fails
		if (event instanceof NavigationCancel) {
			this._sharedService.stopLoading();
		}
		if (event instanceof NavigationError) {
			this._sharedService.stopLoading();
		}
	  }
}
