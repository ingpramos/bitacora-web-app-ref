import { Routes, RouterModule } from '@angular/router';

const ROUTES: Routes = [
    { path: '', redirectTo:'auth/login', pathMatch: 'full' },
    { path:'auth', loadChildren: './auth/auth.module#AuthModule' },
    { path: 'app', loadChildren: './layout/layout.module#LayoutModule' },
    { path: '**', redirectTo:'auth/login', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(ROUTES);