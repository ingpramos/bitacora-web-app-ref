import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations'
import { SharedService } from "../../shared/services/shared.service";
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthService } from '../../shared/services/auth.service';
import { ProfileComponent } from '../profile/profile.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    animations: [
        trigger('toggleHeight', [
            state('inactive', style({
                height: '0',
                opacity: '0'
            })),
            state('active', style({
                height: '*',
                opacity: '1'
            })),
            transition('inactive => active', animate('200ms ease-in')),
            transition('active => inactive', animate('200ms ease-out'))
        ])
    ]
})

export class NavigationComponent implements OnInit {

    sessionInfo: any = {}; // holds user and company info;
    sidebarVisible: boolean;
    navigationText;
    // Sub menu visibilities
    navigationSubState: any = {
        Company: 'inactive',
        Orders: 'inactive',
        Projects: 'inactive',
        Groups: 'inactive',
        Invoices: 'inactive',
        Offers: 'inactive',
        OrdersConfirmed: 'inactive',
        UserInterface: 'inactive',
        Components: 'inactive'
    };
    //bsModalRef: BsModalRef;
    // Toggle sub menu
    toggleNavigationSub(menu, event) {
        event.preventDefault();
        this.navigationSubState[menu] = (this.navigationSubState[menu] === 'inactive' ? 'active' : 'inactive');
    }

    constructor(
        private _sharedService: SharedService,
        private router: Router,
        private activeRoute: ActivatedRoute,
        private _authService: AuthService,
        public dialog: MatDialog
    ) {
        this._sharedService.sidebarVisibilitySubject.subscribe((value) => {
            this.sidebarVisible = value
        })
        // implementing resolve data before initiating component
        this.activeRoute.data.pipe(map(result => { return result['resolvedData']['data'] })).subscribe(data => {
            this._sharedService.setSessionInfo(data);
            this.sessionInfo = data;
        });
    }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.navigationText = NavigationLanguages[cl];
    }

    isActive(instruction: any[]): boolean {
        // Set the second parameter to true if you want to require an exact match.
        return this.router.isActive(this.router.createUrlTree(instruction), false);
    }

    logout() {
        this._authService.logout();
        this.router.navigate(['/']);
    }

    openProfileModal() {
        const dialogRef = this.dialog.open(ProfileComponent, { width: '900px' });
    }

}

class NavigationLanguages {
    static en = {
        "profile": "Profile",
        "logOut": "Logout",
        "orders": "Orders",
        "openOrders": "Open Orders",
        "closedOrders": "Closed Orders",
        "projects": "Projects",
        "openProjects": "Open Projects",
        "closedProjects": "Closed Projects",
        "groups": "Groups",
        "mainGroups": "Groups",
        "subGroups": "Subgroups",
        "invoices": "Invoices",
        "openInvoices": "Open Invoices",
        "closedInvoices": "Closed Invoices",
        "offers": "Offers",
        "openOffers": "Open Offers",
        "closedOffers": "Closed Offers",
        "ordersConfirmed": "Orders Confirmation",
        "openOrdersConfirmed": "Open OC",
        "closedOrdersConfirmed": "Closed OC",
        "deliveryNotes": "Delivery Notes",
        "mainBoard": "Main Board",
        "company": "Company",
        "companySettings": "Settings",
        "companyInformation": "Information",
        "companyEmployees": "Employees",
        "companyProviders": "Providers",
        "companyClients": "Clients",
        "companyFilters": "Filters",
        "companyProfile": "Profile"
    }

    static de = {
        "profile": "Profil",
        "logOut": "Ausloggen",
        "orders": "Bestellungen",
        "openOrders": "Offene Bestellungen",
        "closedOrders": "Geschlossenen Bestellungen",
        "projects": "Aufträge",
        "openProjects": "Offene Aufträge",
        "closedProjects": "Geschlossene Aufträge",
        "groups": "Gruppen",
        "mainGroups": "Baugruppen",
        "subGroups": "Unterbaugruppen",
        "invoices": "Rechnungen",
        "openInvoices": "Offene Rechnungen",
        "closedInvoices": "Geschlossene Rechnungen",
        "offers": "Agebote",
        "openOffers": "Offene Agebote",
        "closedOffers": "Geschlossene Agebote",
        "ordersConfirmed": "Auftragsbestätigungen",
        "openOrdersConfirmed": "Offene AB",
        "closedOrdersConfirmed": "Geschlossene AB",
        "deliveryNotes": "Lieferscheine",
        "mainBoard": "Lager",
        "companySettings": "Einstellungen",
        "company": "Unternehmen",
        "companyInformation": "Information",
        "companyEmployees": "Mitarbeiter",
        "companyProviders": "Lieferanten",
        "companyClients": "Kunden",
        "companyFilters": "Filters",
        "companyProfile": "Profil"
    }

    static es = {
        "profile": "Perfil",
        "logOut": "Cerrar sesión",
        "orders": "Pedidos",
        "openOrders": "Pedidos Abiertos",
        "closedOrders": "Pedidos Cerrados",
        "projects": "Proyectos",
        "openProjects": "Proyectos Activos",
        "closedProjects": "Proyectos Cerrados",
        "groups": "Grupos",
        "mainGroups": "Groups",
        "subGroups": "Subgrupos",
        "invoices": "Facturas",
        "openInvoices": "Facturas Pedientes",
        "closedInvoices": "Facturas Pagadas",
        "offers": "Cotizaciones",
        "openOffers": "Cotizaciones Abiertas",
        "closedOffers": "Cotizaciones Cerradas",
        "ordersConfirmed": "Pedidos Confirmados",
        "openOrdersConfirmed": "Pedidos Abiertos",
        "closedOrdersConfirmed": "Pedidos Facturados",
        "deliveryNotes": "Notas de Entrega",
        "mainBoard": "Inventario",
        "company": "Empresa",
        "companySettings": "Cofiguraciones",
        "companyInformation": "Información",
        "companyEmployees": "Empleados",
        "companyProviders": "Proveedores",
        "companyClients": "Clientes",
        "companyFilters": "Filtros",
        "companyProfile": "Perfil"
    }
}