import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../shared/services/shared.service";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
    maThemeModel: string = "green";
    headerText: any = {};
    companyName:string = "";
    constructor(private _sharedService: SharedService) {
        this._sharedService.maThemeSubject.subscribe(value => (this.maThemeModel = value));
    }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this._sharedService.sessionInfo.subscribe(data => this.companyName = data.company_name);
        this.headerText = HCLanguages[cl];
    }
    openProfileModal() {}
}

class HCLanguages {
    static en = {
        profile: "Profile",
        logOut: "Logout"
    };

    static de = {
        profile: "Profil",
        logOut: "Ausloggen"
    };

    static es = {
        profile: "Perfil",
        logOut: "Cerrar sesión"
    };
}
