import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthService } from '../shared/services/auth.service';
import { AppResolverService } from '../shared/resolvers/app-resolver.service';

const LAYOUT_ROUTES: Routes = [
    {
        path: '', component: LayoutComponent, resolve: { resolvedData: AppResolverService }, children: [

            //Home
            { path: '', redirectTo: 'main-board', pathMatch: 'full' },
            // Orders
            { path: 'open-orders/:order_id', loadChildren: '../pages/orders/orders.module#OrdersModule', canActivate: [AuthService] },
            { path: 'closed-orders/:order_id', loadChildren: '../pages/orders/orders.module#OrdersModule', canActivate: [AuthService] },

            // Projects
            { path: 'open-projects/:project_id', loadChildren: '../pages/projects/projects.module#ProjectsModule', canActivate: [AuthService] },
            { path: 'closed-projects/:project_id', loadChildren: '../pages/projects/projects.module#ProjectsModule', canActivate: [AuthService] },
            { path: 'open-projects/:project_id/:aip_id', loadChildren: '../pages/project-details/project-details.module#ProjectDetailsModule', canActivate: [AuthService] },
            { path: 'closed-projects/:project_id/:aip_id', loadChildren: '../pages/project-details/project-details.module#ProjectDetailsModule', canActivate: [AuthService] },
            // Groups
            { path: 'main-groups/:assembly_id', loadChildren: '../pages/groups/groups.module#GroupsModule', canActivate: [AuthService] },
            { path: 'sub-groups/:assembly_id', loadChildren: '../pages/groups/groups.module#GroupsModule', canActivate: [AuthService] },
            // Invoices
            { path: 'open-invoices/:invoice_id', loadChildren: '../pages/invoices/invoices.module#InvoicesModule', canActivate: [AuthService] },
            { path: 'closed-invoices/:invoice_id', loadChildren: '../pages/invoices/invoices.module#InvoicesModule', canActivate: [AuthService] },
            // Offers
            { path: 'open-offers/:offer_id', loadChildren: '../pages/offers/offers.module#OffersModule', canActivate: [AuthService] },
            { path: 'closed-offers/:offer_id', loadChildren: '../pages/offers/offers.module#OffersModule', canActivate: [AuthService] },
            // oders confirmations
            { path: 'open-orders-confirmed/:oc_id', loadChildren: '../pages/confirmations/confirmations.module#ConfirmationsModule', canActivate: [AuthService] },
            { path: 'closed-orders-confirmed/:oc_id', loadChildren: '../pages/confirmations/confirmations.module#ConfirmationsModule', canActivate: [AuthService] },
            // delivery notes
            { path: 'delivery-notes/:dn_id', loadChildren: '../pages/delivery-notes/delivery-notes.module#DeliveryNotesModule', canActivate: [AuthService] },
            // main board
            { path: 'main-board', loadChildren: '../pages/main-board/main-board.module#MainBoardModule', canActivate: [AuthService] },
            // companies
            { path: 'company/information', loadChildren: '../pages/information/information.module#InformationModule', canActivate: [AuthService] },
            { path: 'company/settings', loadChildren: '../pages/settings/settings.module#SettingsModule', canActivate: [AuthService] },
            { path: 'company/providers/:provider_id', loadChildren: '../pages/providers/providers.module#ProvidersModule', canActivate: [AuthService] },
            { path: 'company/clients/:client_id', loadChildren: '../pages/clients/clients.module#ClientsModule', canActivate: [AuthService] },
            { path: 'company/filters/:filter_group_id', loadChildren: '../pages/filters/filters.module#FiltersModule', canActivate: [AuthService] }
        ]
    }
];

export const LayoutRouting = RouterModule.forChild(LAYOUT_ROUTES);