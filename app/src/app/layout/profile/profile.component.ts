import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../shared/services/shared.service";
import { UsersService } from "../../shared/services/users.service";
import { ActionModalsService } from "../../shared/services/action-modals.service";
import { MatDialogRef } from "@angular/material";
import { FormatDatePipe } from "../../shared/pipes/format-date/format-date.pipe";

@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
    changePassword: boolean = false;
    currentUser: any = {};
    credentials: any = {};
    profileFormText;
    alerts;
    constructor(
        public dialogRef: MatDialogRef<ProfileComponent>,
        private _sharedService: SharedService,
        private _usersService: UsersService,
        private _actionModalsService: ActionModalsService,
        private _formatDatePipe: FormatDatePipe
    ) {
        let cl = this._sharedService.user_selected_language;
        this.profileFormText = PFLanguage[cl];
        this.alerts = this.profileFormText.alerts;
    }

    ngOnInit() {
        this._usersService.getUserById().subscribe(data => {
            this.currentUser = data;
            this.currentUser["user_creation_date"] = this._formatDatePipe.transform(this.currentUser["user_creation_date"])
        });
    }

    startChangePassword() {
        this.changePassword = true;
    }

    forgetActions() {
        this.changePassword = false;
        this.dialogRef.close();
    }

    updateUser() {
        this._usersService.updateEmployee(this.currentUser).subscribe(data => {
            let title = data.user_name;
            let text = this.alerts.alertUserUpdatedSuccessfully;
            this._actionModalsService.buildSuccessModal(title, text).then(() => window.location.reload());
        });
    }

    updatePassword() {
        this.credentials["user_email"] = this.currentUser["user_email"];
        this._usersService.updatePassword(this.credentials).subscribe(
            data => {
                if (data.token) {
                    let title = "";
                    let text = this.alerts.alertPasswordUpdateSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            error => {
                let text = error.status == 400 ? this.alerts.alertWrongPassword : this.alerts.alertPasswordUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }
}

class PFLanguage {
    static en = {
        alerts: {
            alertUserUpdatedSuccessfully: "User updated successfully",
            alertUserUpdateError: "An error ocurred",
            alertPasswordUpdateSuccess: "Password successfully updated",
            alertPasswordUpdateError: "An error ocurred",
            alertWrongPassword: "Wrong password"
        },
        userName: "User name",
        userLastName: "User last name",
        userEmail: "User email",
        userTelephone: "User telephone",
        userSelectedLanguage: "Selected language",
        userCreationDate: "Registration date",
        userInformation: "user Information",
        changePassword: "Change password",
        currentPassword: "Current password",
        newPassword: "New password",
        confirmNewPassword: "Confirm new password",
        updatePasswordButton: "Update password",
        saveButton: "Save",
        cancelButton: "Cancel",
        languages: [{ name: "English", code: "en" }, { name: "German", code: "de" }, { name: "Spanish", code: "es" }]
    };

    static de = {
        alerts: {
            alertUserUpdatedSuccessfully: "Benutzer erfolgreich aktualiziert",
            alertUserUpdateError: "Es ist ein Fehler aufgetreten",
            alertPasswordUpdateSuccess: "Passwort erfolgreich aktualiziert",
            alertPasswordUpdateError: "Es ist ein Fehler aufgetreten",
            alertWrongPassword: "Falsches Passwort"
        },
        userName: "Vorname",
        userLastName: "Name",
        userEmail: "Email",
        userTelephone: "Telefon",
        userSelectedLanguage: "Sprache",
        userCreationDate: "Registrierungdatum",
        userInformation: "Benutzer Information",
        changePassword: "Passwort ändern",
        currentPassword: "Aktuelles Passwort",
        newPassword: "Neues Passwort",
        confirmNewPassword: "Neues Passwort be­stä­ti­gen",
        updatePasswordButton: "Passwort aktualizieren",
        saveButton: "Speichern",
        cancelButton: "Zurück",
        languages: [{ name: "Englisch", code: "en" }, { name: "Deutsch", code: "de" }, { name: "Spanisch", code: "es" }]
    };
    static es = {
        alerts: {
            alertUserUpdatedSuccessfully: "Usuario actualizado exitosamente",
            alertUserUpdateError: "Ha ocurrido un error",
            alertPasswordUpdateSuccess: "Contraseña actualizada exitosamente",
            alertPasswordUpdateError: "Ha ocurrido un error",
            alertWrongPassword: "Contraseña incorrecta"
        },
        userName: "Nombre",
        userLastName: "Apellido",
        userEmail: "Email",
        userTelephone: "Telefono",
        userSelectedLanguage: "Idioma",
        userCreationDate: "Fecha de Registro",
        userInformation: "Informacion del Usuario",
        changePassword: "Cambiar contraseña",
        currentPassword: "Contraseña actual",
        newPassword: "Nueva contraseña",
        confirmNewPassword: "Confirmar nueva contraseña",
        updatePasswordButton: "Actualizar Contraseña",
        saveButton: "Guardar",
        cancelButton: "Cancelar",
        languages: [{ name: "Inglés", code: "en" }, { name: "Alemán", code: "de" }, { name: "Español", code: "es" }]
    };
}
