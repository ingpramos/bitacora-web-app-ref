import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
// Angular Material
import {
	MatMenuModule, MatInputModule, MatNativeDateModule,
	MatSelectModule, MatFormFieldModule, MatDatepickerModule,
	MatExpansionModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule
} from '@angular/material';

import { CompanyCardComponent } from './company-card/company-card.component';
import { InformationComponent } from './information.component';
import { CeoCardComponent } from './ceo-card/ceo-card.component';
import { EmployeesListComponent } from './employees-list/employees-list.component';
import { EmployeesFormComponent } from './employees-form/employees-form.component';
import { FormatDatePipe } from '../../shared/pipes/format-date/format-date.pipe';
import { ResetEmployeePasswordComponent } from './reset-employee-password/reset-employee-password.component';


const INFORMATION_ROUTES: Routes = [
	{ path: '', component: InformationComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(INFORMATION_ROUTES),
		FormsModule,		
		MatMenuModule,
		MatInputModule,
		MatNativeDateModule,
		MatSelectModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatExpansionModule,
		MatDialogModule,
		MatIconModule
	],
	providers:[
		FormatDatePipe,
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		ResetEmployeePasswordComponent
	],
	declarations: [
		InformationComponent,
		CompanyCardComponent,
		CeoCardComponent,
		EmployeesListComponent,
		EmployeesFormComponent,
		ResetEmployeePasswordComponent
	]
})
export class InformationModule { }
