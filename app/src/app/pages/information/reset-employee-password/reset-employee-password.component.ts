import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsersService } from '../../../shared/services/users.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
    selector: 'app-reset-employee-password',
    templateUrl: './reset-employee-password.component.html',
    styleUrls: ['./reset-employee-password.component.scss']
})

export class ResetEmployeePasswordComponent implements OnInit {

    currentUser = this.modalData.currentUser;
    formText: any;
    alerts: any;
    newPassword;
    confirmedPassword;
    feedbackMsg: string;

    constructor(
        private _sharedService: SharedService,
        private _usersServie: UsersService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<ResetEmployeePasswordComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.formText = REPCLanguages[cl];
        this.alerts = this.formText.alerts;
    }

    ngOnInit() {
    }

    setNewPassword() {
        let body = {};
        body['new_password'] = this.newPassword;
        body['user_password_id'] = this.currentUser.user_id;
        this._usersServie.resetPassword(body).subscribe(data => {
            if (data) {
                this.dialogRef.close();
                let title = this.currentUser.user_name + " " + this.currentUser.user_last_name;
                let text = this.alerts.alertPasswordRessettedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
            }
        }, () => {
            let text = this.alerts.alertResetPasswordError;
            this._actionModalsService.alertError(text);
        });
    }

}

class REPCLanguages {
    static en = {
        "alerts": {
            "alertPasswordRessettedSuccessfully": "Password resetted successfully",
            "alertResetPasswordError": "An error ocurred"
        },
        "title": "Reset password",
        "newPassword": "Enter new password",
        "confirmedPassword": "Confirm new password",
        "resetPasswordText": "Please enter and Confirm the new password",
        "saveButton": "Save",
        "emailNotFoundMsg": "Email not registered",
        "resetLinkSent": "You will recieve a link in your email click it to change your password."
    };
    static de = {
        "alerts": {
            "alertPasswordRessettedSuccessfully": "",
            "alertResetPasswordError": "Es ist ein Fehler aufgetreten."
        },
        "title": "Passwort zurücksetzen",
        "newPassword": "neues Passwort",
        "confirmedPassword": "neues Passwort Bestätigen",
        "resetPasswordText": "Bitte geben Sie ein und bestätigen Sie das neue Passwort.",
        "saveButton": "Speichern",
        "emailNotFoundMsg": "Email nicht registriert",
        "resetLinkSent": "You will recieve a link in your email click it to change your password."
    };
    static es = {
        "alerts": {
            "alertPasswordRessettedSuccessfully": "",
            "alertResetPasswordError": "Ha ocurrido un error"
        },
        "title": "Resetar contraseña",
        "newPassword": "nueva Contraseña",
        "confirmedPassword": "Confirmar nueva contraseña",
        "resetPasswordText": "Por favor ingrese y confirme la nueva contraseña",
        "saveButton": "Guardar",
        "emailNotFoundMsg": "Email no registrado",
        "resetLinkSent": "You will recieve a link in your email click it to change your password."
    };
}
