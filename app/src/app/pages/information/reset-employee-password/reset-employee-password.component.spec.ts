import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetEmployeePasswordComponent } from './reset-employee-password.component';

describe('ResetEmployeePasswordComponent', () => {
  let component: ResetEmployeePasswordComponent;
  let fixture: ComponentFixture<ResetEmployeePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetEmployeePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetEmployeePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
