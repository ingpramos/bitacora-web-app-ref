import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { FormatDatePipe } from "../../../shared/pipes/format-date/format-date.pipe";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

const CURRENCIES = ["USD", "EUR", "JPY", "GBP", "AUD", "CAD", "CHF", "SEK", "NOK", "COP", "CRC", "MXN"];

@Component({
    selector: "app-company-card",
    templateUrl: "./company-card.component.html",
    styleUrls: ["./company-card.component.scss"]
})
export class CompanyCardComponent implements OnInit {
    @Input() sessionInfo;
    companyInfoCopy: any;
    companyFormText: any;
    alerts: any;
    brandImageUrl;
    logoToUpload;
    editCompanyInfo = false;
    editCompanyImage = false;
    cb64;
    prevImg;
    currencies = CURRENCIES;

    constructor(
        private _companyService: CompanyService,
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _formatDatePipe: FormatDatePipe
    ) {}

    ngOnInit() {
        this.prevImg = document.getElementById("pre-image") as HTMLImageElement;
        this.sessionInfo.company_creation_date_to_show = this._formatDatePipe.transform(this.sessionInfo.company_creation_date);
        this.companyInfoCopy = JSON.parse(JSON.stringify(this.sessionInfo));
        this.brandImageUrl = this.sessionInfo.company_url_image_brand;
        let cl = this.sessionInfo.user_selected_language;
        this.companyFormText = CCCLanguage[cl] || {};
        this.alerts = this.companyFormText.alerts;
        this.brandImageUrl !== "NONE" ? this.setCompanyImage() : this.setDefaultImage();
    }

    chooseImage() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    setCompanyImage() {
        this.prevImg.setAttribute("src", this._sharedService.rootUrl + this.brandImageUrl);
    }
    setDefaultImage() {
        this.prevImg.setAttribute("src", "../../assets/img/250x100.png");
    }

    onFileChange(event) {
        let reader: any = new FileReader();
        reader.onload = function(event) {
            let element = document.getElementById("pre-image") as HTMLImageElement;
            element.setAttribute("src", reader.result);
        };
        reader.readAsDataURL(event.target.files[0]);
        this.logoToUpload = event.target.files[0];
        typeof this.logoToUpload == "object" ? (this.editCompanyImage = true) : (this.editCompanyImage = false);
    }

    uploadImage() {
        this.logoToUpload.company_id = this.sessionInfo.company_id;
        this.logoToUpload.company_images = this.sessionInfo.company_images;
        this._companyService.uploadPhoto(this.logoToUpload).subscribe(
            data => {
                if (data) {
                    this.sessionInfo.company_url_image_brand = data.company_url_image_brand;
                    let title = this.logoToUpload.name;
                    let text = this.alerts.alertImageUploadSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this._sharedService.getBase64UrlFromImage(data.company_url_image_brand);
                    this.clearPhoto();
                }
            },
            () => {
                let message = this.alerts.alertImageUploadError;
                this._actionModalsService.alertError(message);
            }
        );
    }

    startToEditCompanyInfo() {
        this.editCompanyInfo = true;
    }

    updateCompany() {
        if (this._sharedService.isObjectEquivalent(this.companyInfoCopy, this.sessionInfo)) {
            this.forgetActions();
            return;
        } else {
            this._companyService.updateCompany(this.sessionInfo).subscribe(
                data => {
                    if (data) {
                        const title = data.company_name;
                        const text = this.alerts.alertCompanyInfoUpdateSuccess;
                        this._actionModalsService.buildSuccessModal(title, text);
                        this.editCompanyInfo = false;
                        // update the session info data
                        let newSessionInfo = Object.assign(this.sessionInfo, data);
                        this._sharedService.setSessionInfo(newSessionInfo);
                    }
                },
                () => {
                    let message = this.alerts.alertCompanyInfoUpdateError;
                    this._actionModalsService.alertError(message);
                }
            );
        }
    }
    // to call when the users decides no to edit or upload
    forgetActions() {
        this.sessionInfo = this.companyInfoCopy;
        this.editCompanyImage = false;
        this.editCompanyInfo = false;
        this.brandImageUrl !== "NONE" ? this.setCompanyImage() : this.setDefaultImage();
        (<HTMLInputElement>document.getElementById("i-input")).value = "";
    }

    clearPhoto() {
        this.editCompanyImage = false;
    }
}

class CCCLanguage {
    static en = {
        alerts: {
            alertImageUploadSuccess: "Image uploaded successfully",
            alertImageUploadError: "An error ocurred",
            alertCompanyInfoUpdateSuccess: "Company information successfully updated",
            alertCompanyInfoUpdateError: "An error ocurred"
        },
        cardTitle: "Company Information",
        companyName: "Company Name",
        companyAddress: "Company Address",
        companyId: "Company Id",
        companyTaxId: "Tax Payer Id",
        companyVAT: "VAT identification number",
        companyCreationDate: "Registry Date",
        companyLogo: "Company Logo",
        companyCountry: "Country",
        companyCity: "City",
        companyZipCode: "Postal Code",
        companyWebsite: "Website",
        companyTelephone: "Telephone",
        companyFax: "Fax",
        companyEmail: "Email",
        companyRegistrationCourt: "Registration Court",
        companyBusinessManagerName: "Business Manager",
        companyBusinessManagerLastName: "Business Manager",
        companyCurrency: "Currency",
        requiredFieldsMessage: "Fields marked * are required fields.",
        updateButton: "Update Company Information",
        uploadImage: "Upload Logo",
        saveButton: "Save",
        forgetButton: "Cancel"
    };

    static de = {
        alerts: {
            alertImageUploadSuccess: "Bild erfolgreich gespeichert",
            alertImageUploadError: "Es ist ein Fehler aufgetreten",
            alertCompanyInfoUpdateSuccess: "Firma information erfolgreich aktualiziert",
            alertCompanyInfoUpdateError: "Es ist ein Fehler aufgetreten"
        },
        cardTitle: "Firma Information",
        companyName: "Unternehmen",
        companyAddress: "Adresse",
        companyId: "Unternehmen Id",
        companyTaxId: "Steuer-ID",
        companyVAT: "Umsatzsteuer-ID",
        companyCreationDate: "Registrierungsdatum",
        companyLogo: "Firmenlogo",
        companyCountry: "Land",
        companyCity: "Stadt",
        companyZipCode: "Postleitzahl",
        companyWebsite: "Website",
        companyTelephone: "Telefon",
        companyFax: "Fax",
        companyEmail: "Email",
        companyRegistrationCourt: "Registergericht",
        companyBusinessManagerName: "Geschäftsführer Vorname",
        companyBusinessManagerLastName: "Geschäftsführer Name",
        companyCurrency: "Währung",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        updateButton: "Firma Information aktualizieren",
        uploadImage: "Firmenlogo hochladen",
        saveButton: "Speichern",
        forgetButton: "Zurück"
    };
    static es = {
        alerts: {
            alertImageUploadSuccess: "Imagen actualizada correctamente",
            alertImageUploadError: "Ha ocurrido un error",
            alertCompanyInfoUpdateSuccess: "Informacion de la empresa actualizada exitosamente",
            alertCompanyInfoUpdateError: "Ha ocurrido un error"
        },
        cardTitle: "Información de la Empresa",
        companyName: "Nombre de la Empresa",
        companyAddress: "Direccion",
        companyId: "Código",
        companyTaxId: "Identificación Tributaria",
        companyVAT: "Impuesto Al valor agregado",
        companyCreationDate: "Fecha de Registro",
        companyLogo: "Logo Empresa",
        companyCountry: "Pais",
        companyCity: "Ciudad",
        companyZipCode: "Código Postal",
        companyWebsite: "Página Web",
        companyTelephone: "Teléfono",
        companyFax: "Fax",
        companyEmail: "Email",
        companyRegistrationCourt: "Oficina de Registro",
        companyBusinessManagerName: "Nombre del Gerente",
        companyBusinessManagerLastName: "Apellido del Gerente",
        companyCurrency: "Moneda",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        updateButton: "Actualizar Información De La Empresa",
        uploadImage: "Subir Logo",
        saveButton: "Guardar",
        forgetButton: "Cancelar"
    };
}
