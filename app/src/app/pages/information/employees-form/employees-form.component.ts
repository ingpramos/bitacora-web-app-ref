import { Component, OnInit, Input, OnChanges, Output, EventEmitter, SimpleChange } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { UsersService } from "../../../shared/services/users.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-employees-form",
    templateUrl: "./employees-form.component.html",
    styleUrls: ["./employees-form.component.scss"]
})
export class EmployeesFormComponent implements OnInit, OnChanges {
    constructor(private _actionModalsService: ActionModalsService, private _sharedService: SharedService, private _usersService: UsersService) {}

    @Input() currentEmployee;
    @Input() editEmployee: boolean;
    @Input() newEmployee: boolean;

    @Output() closeForm = new EventEmitter<boolean>();
    // Init languages setup
    employeeFormText: any;
    alerts: any;
    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.employeeFormText = EFLanguage[cl];
        this.alerts = this.employeeFormText.alerts;
    }

    ngOnChanges(changes) {}

    forgetActions() {
        this.currentEmployee = {};
        this.closeForm.emit(false);
    }

    saveEmployee(employee) {
        this.editEmployee ? this.updateEmployee() : this.createEmployee();
    }

    createEmployee() {
        this._usersService.createEmployee(this.currentEmployee).subscribe(
            data => {
                this._usersService.passEmployeeToPushOrUpdate(data);
                let title = `${this.currentEmployee.user_name} ${this.currentEmployee.user_last_name}`;
                let text = this.alerts.alertEmployeeCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            },
            () => {
                let text = this.alerts.alertEmployeeCreateError;
                this._actionModalsService.alertError(text);
                this.closeForm.emit(true);
            }
        );
    }

    updateEmployee() {
        this._usersService.updateEmployee(this.currentEmployee).subscribe(
            data => {
                this._usersService.passEmployeeToPushOrUpdate(data);
                let title = `${this.currentEmployee.user_name} ${this.currentEmployee.user_last_name}`;
                let text = this.alerts.alertEmployeeUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            },
            () => {
                let text = this.alerts.alertEmployeeUpdateError;
                this._actionModalsService.alertError(text);
                this.closeForm.emit(true);
            }
        );
    }
}

class EFLanguage {
    static en = {
        alerts: {
            alertEmployeeUpdatedSuccessfully: "Employee updated successfully",
            alertEmployeeUpdateError: "An error ocurred",
            alertEmployeeCreatedSuccessfully: "Employee created successfully",
            alertEmployeeCreateError: "An error ocurred"
        },
        employeeName: "Name",
        employeeLastName: "Last Name",
        employeeEmail: "E-mail",
        employeeTelephone: "Telephone",
        editUserInfo: "Edit User Information",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel",
        deleteUser: "Delete User",
        alertDeleteUserTitle: "Do you want to delete this user ?"
    };

    static de = {
        alerts: {
            alertEmployeeUpdatedSuccessfully: "Mitarbeiter erfolgreich aktualiziert",
            alertEmployeeUpdateError: "Es ist ein Fehler aufgetreten",
            alertEmployeeCreatedSuccessfully: "Mitarbeiter erfolgreich erstelt",
            alertEmployeeCreateError: "Es ist ein Fehler aufgetreten"
        },
        employeeName: "Name",
        employeeLastName: "Nachname",
        employeeEmail: "E-mail",
        employeeTelephone: "Telefon",
        editUserInfo: "Benutzer Information bearbeiten",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück",
        deleteUser: "Benutzer löschen",
        alertDeleteUserTitle: "Wollen Sie dieses Benutzer löschen ?"
    };

    static es = {
        alerts: {
            alertEmployeeUpdatedSuccessfully: "Empleado actualizado exitosamente",
            alertEmployeeUpdateError: "An error ocurred",
            alertEmployeeCreatedSuccessfully: "Empleado creado existosamente",
            alertEmployeeCreateError: "Es ist ein Fehler aufgetreten"
        },
        employeeName: "Nombre",
        employeeLastName: "Apellido",
        employeeEmail: "E-mail",
        employeeTelephone: "Teléfono",
        editUserInfo: "Editar información del usuario",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Save",
        cancelButton: "Cancel",
        deleteUser: "Eliminar Usuario",
        alertDeleteUserTitle: "Desea eliminar este usuario ?"
    };
}
