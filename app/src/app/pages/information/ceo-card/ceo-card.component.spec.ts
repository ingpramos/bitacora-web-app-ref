import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CeoCardComponent } from './ceo-card.component';

describe('CeoCardComponent', () => {
  let component: CeoCardComponent;
  let fixture: ComponentFixture<CeoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CeoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CeoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
