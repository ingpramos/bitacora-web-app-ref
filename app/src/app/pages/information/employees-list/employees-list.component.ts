import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { UsersService } from "../../../shared/services/users.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { MatDialog } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { ResetEmployeePasswordComponent } from "../reset-employee-password/reset-employee-password.component";

@Component({
    selector: "app-employees-list",
    templateUrl: "./employees-list.component.html",
    styleUrls: ["./employees-list.component.scss"]
})
export class EmployeesListComponent implements OnInit {
    @Input() employees: any;
    @Input() selectedEmployee;

    employeesListText;
    alerts: any;
    // to show or hide the employee Form
    editEmployee: boolean = false;
    newEmployee: boolean = false;
    employeeToHandle: any = {};

    showSearch: boolean = false;
    private subject: Subject<string> = new Subject();
    searchPattern = "";

    constructor(
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService,
        private _usersService: UsersService,
        public dialog: MatDialog
    ) {
        let cl = this._sharedService.user_selected_language;
        this.employeesListText = ELLanguage[cl];
        this.alerts = this.employeesListText.alerts;
    }

    ngOnInit() {
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForEmployees();
        });
    }

    searchEmployees() {
        this.subject.next(this.searchPattern);
    }

    searchForEmployees() {
        this._usersService.getUsersByPattern(this.searchPattern);
    }

    startResetPassword(user) {
        let initialState = {};
        initialState["currentUser"] = user;
        const dialogRef = this.dialog.open(ResetEmployeePasswordComponent, { width: "350px", data: initialState });
    }

    closeSearch() {
        this.searchPattern = "";
        this._usersService.getUsers();
        this.showSearch = false;
    }

    startEditEmployee(employee) {
        this.employeeToHandle = Object.assign({}, employee);
        this.newEmployee = false;
        this.editEmployee = true;
        // review for possible issues
        window.scroll(0, 0);
    }

    startSearch() {
        this.editEmployee = false;
        this.newEmployee = false;
        this.showSearch = true;
    }

    startNewEmployee() {
        this.editEmployee = false;
        this.newEmployee = true;
        this.employeeToHandle = {};
    }

    forgetActions(value) {
        this.editEmployee = false;
        this.newEmployee = false;
    }

    deActivateEmployee(employee, index) {
        let title = employee.user_name;
        let text = this.alerts.confirmDeleteEmployee;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let user_id = employee.user_id;
                this._usersService.deActivateUser(user_id).subscribe(
                    data => {
                        let text = this.alerts.alertEmployeeDeletedSuccessfully;
                        this._actionModalsService.buildSuccessModal(title, text);
                        this.employees.splice(index, 1);
                    },
                    () => {
                        let text = this.alerts.alertEmployeeDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class ELLanguage {
    static en = {
        alerts: {
            confirmDeleteEmployee: "Are you sure you want to delete this employee ?",
            alertEmployeeDeletedSuccessfully: "Employee successfully created",
            alertEmployeeDeleteError: "An error ocurred"
        },
        title: "Employees",
        newEmployee: "New Employee",
        searchEmployees: "Search Employees",
        filterEmployees: "Search With Filters",
        openEmployees: "Open Employees",
        closedEmployees: "Closed Employees",
        editEmployee: "Edit Employee",
        resetPassword: "Reset Password",
        deleteEmployee: "Delete Employee"
    };

    static de = {
        alerts: {
            confirmDeleteEmployee: "Wollen Sie dieses Mitarbeiter löschen ?",
            alertEmployeeDeletedSuccessfully: "Mitarbeiter erfolgreich gelöscht.",
            alertEmployeeDeleteError: "Es ist ein Fehler aufgetreten."
        },
        title: "Mitarbeiter",
        newEmployee: "Neue Mitarbeiter",
        searchEmployees: "Mitarbeiter suchen",
        filterEmployees: "Suchen mit Filtern",
        openEmployees: "Offene Mitarbeiter",
        closedEmployees: "Geschlossene Mitarbeiter",
        editEmployee: "Mitarbeiter bearbeiten",
        resetPassword: "Password rücksetzen",
        deleteEmployee: "Mitarbeiter löschen"
    };

    static es = {
        alerts: {
            confirmDeleteEmployee: "Esta seguro que deseas eliminar el empleado actual ?",
            alertEmployeeDeletedSuccessfully: "Empleado eliminado exitosamente",
            alertEmployeeDeleteError: "Ha ocurrido un error"
        },
        title: "Empleados",
        newEmployee: "Nuevo Empleado",
        searchEmployees: "Buscar empleados",
        filterEmployees: "Busqueda con Filtros",
        openEmployees: "Pedidos Abiertos",
        closedEmployees: "Pedidos  Cerrados",
        editEmployee: "Editar Empleado",
        resetPassword: "Resetear contraseña",
        deleteEmployee: "Eliminar Empleado"
    };
}
