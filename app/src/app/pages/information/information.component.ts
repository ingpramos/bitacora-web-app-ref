import { Component, OnInit, OnDestroy } from '@angular/core';
import { SharedService } from '../../shared/services/shared.service';
import { UsersService } from '../../shared/services/users.service';

@Component({
	selector: 'app-information',
	templateUrl: './information.component.html',
	styleUrls: ['./information.component.scss']
})

export class InformationComponent implements OnInit, OnDestroy {

	sessionInfo;
	employees = [];
	informationViewText = {};
	// subcriptions
	newInfoEmployee$;
	employeesListSubscription;

	constructor(
		private _sharedService: SharedService,
		private _usersService: UsersService,

	) {
		this.newInfoEmployee$ = this._usersService.employeeToPushOrUpdate.subscribe(data => {
			let user_id = data.user_id;
			if (!user_id) return; // case {} an initialization
			let index = this.employees.findIndex(obj => obj['user_id'] == user_id);
			if (index == -1) {  // user was created 
				this.employees.push(data);
			} else { // user was updated
				this.employees.splice(index, 1, data);
			}
		});
		this.employeesListSubscription = this._usersService.currentUsersList.subscribe(data => this.employees = data);
		this._sharedService.sessionInfo.subscribe(data => {
			this.sessionInfo = data;
			let cl = this._sharedService.user_selected_language;
			this.informationViewText = ICLanguages[cl];
		});
	}

	ngOnInit() {
		this._usersService.getUsers();
	}

	ngOnDestroy() {
		this.newInfoEmployee$.unsubscribe();
		this.employeesListSubscription.unsubscribe();
	}


}

class ICLanguages {
	static en = {
		"ibansListTitle": "IBANS List",
		"paymentMethodsListTitle": "Payment Methods",
		"paymentConditionsListTitle": "Payment Conditions",
		"deliveryMethodsListTitle": "Delivery Methods",
		"deliveryConditionsListTitle": "Delivery Conditions",
		"currentNumbersListTitle": "Current Numbers"
	};
	static de = {
		"ibansListTitle": "IBANS Liste",
		"paymentMethodsListTitle": "Zalungsarten",
		"paymentConditionsListTitle": "Zahlungsbedingungen",
		"deliveryMethodsListTitle": "Lieferarten",
		"deliveryConditionsListTitle": "Lieferbedingungen",
		"currentNumbersListTitle": "Aktuelenummern"
	};
	static es = {
		"ibansListTitle": "IBANS Liste",
		"paymentMethodsListTitle": "Metodos de Pago",
		"paymentConditionsListTitle": "Condiciones de Pago",
		"deliveryMethodsListTitle": "Metodos de Entrega",
		"deliveryConditionsListTitle": "Condiciones de Entrega",
		"currentNumbersListTitle": "Numeros Actuales"
	};
}
