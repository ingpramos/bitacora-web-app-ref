import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ContactFormComponent } from "../contact-form/contact-form.component";
import { MatDialog } from "@angular/material/dialog";
import { ClientsService } from "../../../shared/services/clients.service";

@Component({
    selector: "app-client-header",
    templateUrl: "./client-header.component.html",
    styleUrls: ["./client-header.component.scss"]
})
export class ClientHeaderComponent implements OnInit, OnDestroy {
    @Input() client: any;
    @Input() showMenu: boolean; // to show or hide three points menu

    clientHeaderText: any;
    contactsInClient: number = 0;
    contactsSubscription;
    constructor(private _sharedService: SharedService, private _clientsService: ClientsService, public dialog: MatDialog) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.clientHeaderText = CHLanguages[cl];
        this.contactsSubscription = this._clientsService.currentContactsList.subscribe(data => (this.contactsInClient = data.length));
    }

    ngOnDestroy() {
        this.contactsSubscription.unsubscribe();
    }

    startNewContact() {
        let initialInformation = {};
        initialInformation["client"] = this.client;
        initialInformation["currentContact"] = {};
        const dialogRef = this.dialog.open(ContactFormComponent, { width: "1000px", data: initialInformation });
    }
}

class CHLanguages {
    static en = {
        clientName: "Provider Name",
        clientNumber: "Client Number",
        clientZipCode:"Zip Code",
        clientCountry:"Country",
        clientAddress: "Address",
        clientContacts: "Contacts",
        newContact: "New Contact",
        uploadContactsList: "Upload Contact List"
    };

    static de = {
        clientName: "Lieferant",
        clientNumber: "Kundenummer",
        clientZipCode:"Postleitzahl",
        clientCountry:"Land",
        clientAddress: "Adresse",
        clientContacts: "Kontakte",
        newContact: "Neue Kontakt erstellen",
        uploadContactsList: "Kontaktelist hochladen"
    };

    static es = {
        clientName: "Proveedor",
        clientNumber: "Cliente ID",
        clientZipCode:"Código Postal",
        clientCountry:"País",
        clientAddress: "Dirección",
        clientContacts: "Contactos",
        newContact: "Crear Nuevo Contacto",
        uploadContactsList: "Subir lista de Contactos"
    };
}
