import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-client-form",
    templateUrl: "./client-form.component.html",
    styleUrls: ["./client-form.component.scss"]
})
export class ClientFormComponent implements OnInit {
    @Input() currentClient: any;
    @Input() newClient: boolean;
    @Input() editClient: boolean;

    @Output() closeForm = new EventEmitter<boolean>();

    clientFormText: any;
    alerts: any;

    constructor(private _actionModalsService: ActionModalsService, private _sharedService: SharedService, private _clientService: ClientsService) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.clientFormText = CFLanguages[cl];
        this.alerts = this.clientFormText.alerts;
    }

    forgetActions() {
        this.closeForm.next(true);
    }

    saveClient() {
        this.newClient ? this.createClient(this.currentClient) : this.updateClient(this.currentClient);
    }

    createClient(client) {
        this._clientService.createClient(client).subscribe(
            data => {
                if (data) {
                    this._clientService.passClientToPushOrUpdate(data);
                    let title = this.currentClient.client_name;
                    let text = this.alerts.alertClientCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.next(true);
                }
            },
            e => {
                if (e.status == 403) {
                    // already exist
                    let title = this.currentClient.client_name;
                    const text = this.alerts.alertClientAlreadyExist;
                    this._actionModalsService.buildInfoModal(title, text);
                } else {
                    let text = this.alerts.alertClientCreateError;
                    this._actionModalsService.alertError(text);
                    this.closeForm.next(true);
                }
            }
        );
    }

    updateClient(client) {
        this._clientService.updateClient(client).subscribe(
            data => {
                if (data) {
                    this._clientService.passClientToPushOrUpdate(data);
                    let title = this.currentClient.client_name;
                    let text = this.alerts.alertClientUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.next(true);
                }
            },
            () => {
                let text = this.alerts.alertClientUpdateError;
                this._actionModalsService.alertError(text);
                this.closeForm.next(true);
            }
        );
    }
}

class CFLanguages {
    static en = {
        alerts: {
            alertClientUpdatedSuccessfully: "Client updated successfully",
            alertClientUpdateError: "An error ocurred",
            alertClientCreatedSuccessfully: "Client created successfully",
            alertClientAlreadyExist: "The client you are trying to create already exist",
            alertClientCreateError: "An error ocurred"
        },
        clientName: "Client Name",
        clientAddress: "Client Address",
        clientZipCode: "ZIP Code",
        clientCity: "City",
        clientCountry: "Country",
        clientNumber: "Client Number",
        client: "Client",
        clientTaxID: "Client Tax ID",
        clientVatID: "Client VAT ID",
        clientContact: "Contact",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertClientUpdatedSuccessfully: "Kunde erfolgreich aktualiziert",
            alertClientUpdateError: "Es ist ein Fehler aufgetreten",
            alertClientCreatedSuccessfully: "Kunde erfolgreich erstelt",
            alertClientAlreadyExist: "Der Kunde, den Sie erstellen möchten, ist bereits vorhanden",
            alertClientCreateError: "Es ist ein Fehler aufgetreten"
        },
        clientName: "Kunde",
        clientAddress: "Adresse",
        clientZipCode: "Postleitzahl",
        clientCity: "Stadt",
        clientCountry: "Land",
        clientNumber: "Kundenummer",
        client: "Kunde",
        clientTaxID: "ST-Nr",
        clientVatID: "USt-IdNr",
        clientContact: "Ansprechpartner",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertClientUpdatedSuccessfully: "Cliente actualizado exitosamente",
            alertClientUpdateError: "Ha ocurrido un error",
            alertClientCreatedSuccessfully: "Cliente creado existosamente",
            alertClientAlreadyExist: "El cliente que está intentando crear ya existe",
            alertClientCreateError: "Ha ocurrido un error"
        },
        clientName: "Cliente",
        clientAddress: "Dirección",
        clientZipCode: "Código Postal",
        clientCity: "Ciudad",
        clientCountry: "País",
        clientNumber: "Código Del Cliente",
        client: "Cliente",
        clientTaxID: "NIT",
        clientVatID: "ID Exportador",
        clientContact: "Contacto",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
