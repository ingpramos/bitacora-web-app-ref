import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ClientsService } from '../../shared/services/clients.service';

@Component({
	selector: 'app-clients',
	templateUrl: './clients.component.html',
	styleUrls: ['./clients.component.scss']
})

export class ClientsComponent implements OnInit, OnDestroy {

	client_id: any;
	clients = [];
	selectedClient = {};
	// subscription
	clientsList$;
	clientToPushOrUpdate$;

	constructor(
		private _clientsService: ClientsService,
		private activeRoute: ActivatedRoute,
		private router: Router
	) {
		this.clientToPushOrUpdate$ = this._clientsService.clientToPushOrUpdate.subscribe(data => {
			let client_id = data['client_id'];
			if (!client_id) return; // case {} an initialization
			let index = this.clients.findIndex(obj => obj['client_id'] == client_id);
			if (index == -1) {  // client was created 
				this.clients.push(data);
				this.router.navigate(['app/company/clients', client_id]);
			} else { // client was updated
				this.clients.splice(index, 1, data);
				this.selectedClient = data;
			}
		});
		this.clientsList$ = this._clientsService.currentClientsList.subscribe(data => {
			this.clients = data;
			this.setSelectedClient();
		});
		this.activeRoute.params.pipe(map(params => params['client_id'])).subscribe(client_id => {
			this.client_id = client_id;
			this.setSelectedClient();
		});
	}

	ngOnInit() {
		this._clientsService.getClients();
	}

	ngOnDestroy() {
		this.clientToPushOrUpdate$.unsubscribe();
		this.clientsList$.unsubscribe();
	}

	setSelectedClient() {
		if (this.clients.length > 0) {
			let found = this.clients.find((obj) => obj['client_id'] == this.client_id);
			this.selectedClient = found ? found : this.clients[0];
			this.router.navigate(['app/company/clients', this.selectedClient['client_id']]);
			window.scroll(0, 0);
		} else {
			this.selectedClient = {};
		}
	}
}
