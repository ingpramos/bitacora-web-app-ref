import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
// Angular Material
import {
	MatMenuModule, MatInputModule, MatFormFieldModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule
} from '@angular/material';

import { ClientsComponent } from './clients.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientHeaderComponent } from './client-header/client-header.component';
import { ClientFormComponent } from './client-form/client-form.component';
import { ClientContactsTableComponent } from './client-contacts-table/client-contacts-table.component';
import { ContactFormComponent } from './contact-form/contact-form.component';

const CLIENTS_ROUTES: Routes = [
	{ path: '', component: ClientsComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(CLIENTS_ROUTES),
		FormsModule,
		MatMenuModule,
		MatInputModule,
		MatFormFieldModule,
		MatDialogModule,
		MatIconModule,
		PipesModule
	],
	declarations: [
		ClientsComponent,
		ClientsListComponent,
		ClientHeaderComponent,
		ClientFormComponent,
		ClientContactsTableComponent,
		ContactFormComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		ContactFormComponent
	]

})
export class ClientsModule { }
