import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ClientsService } from '../../../shared/services/clients.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-contact-form',
	templateUrl: './contact-form.component.html',
	styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

	constructor(
		private _sharedService: SharedService,
		private _clientsService: ClientsService,
		private _actionModalsService: ActionModalsService,
		public dialogRef: MatDialogRef<ContactFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
	) { }

	client = this.modalData.client;
	contactFormText;
	alerts;
	currentContact = this.modalData.currentContact;
	editContact = this.modalData.editContact;

	ngOnInit() {
		let cl = this._sharedService.user_selected_language;
		this.contactFormText = CFLanguages[cl];
		this.alerts = this.contactFormText.alerts;
	}

	saveContact() {
		this.editContact ? this.updateContact() : this.createContact();
	}

	updateContact() {
		this._clientsService.updateClientContact(this.currentContact)
			.subscribe(data => {
				this.closeModal();
				let title = `${data.contact_name} ${data.contact_last_name}`;
				let text = this.alerts.alertContactUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
			},()=>{
				let title = this.alerts.alertContactUpdateError;
				this._actionModalsService.alertError(title);
			});
	}

	createContact() {
		let client_id = this.client.client_id;
		this.currentContact.client_id = client_id;
		this._clientsService.createContact(this.currentContact)
			.subscribe(data => {
				this._clientsService.getContacts(client_id);
				let title = `${data.contact_name} ${data.contact_last_name}`;
				let text = this.alerts.alertContactCreatedSuccessfully;
				this.closeModal();
				this._actionModalsService.buildSuccessModal(title, text);
			}, () => {
				let title = this.alerts.alertConctactCreateError;
				this._actionModalsService.alertError(title);
			});
	}

	closeModal() {
		this.currentContact = {};
		this.dialogRef.close();
	}

}

class CFLanguages {
	static en = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Contact created successfully",
			"alertConctactCreateError": "An error ocurred",
			"alertContactUpdatedSuccessfully": "Contact updated successfully",
			"alertContactUpdateError": "An error ocurred"
		},
		"modalTitleCreate":"Create contact",
		"modalTitleUpdate":"Update contact",
		"contactName": "Name",
		"contactLastName": "Last Name",
		"contactEmail": "Email",
		"contactTelephone": "Telephone",
		"contactProvider": "Provider",
		"requiredFieldsMessage": "Fields marked * are required fields.",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};

	static de = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Kontakt erfolgreich erstellt",
			"alertConctactCreateError": "Es ist ein Fehler aufgetreten",
			"alertContactUpdatedSuccessfully": "Kontakt erfolgreich aktualiziert",
			"alertContactUpdateError": "Es ist ein Fehler aufgetreten"
		},
		"modalTitleCreate":"Kontakt  erstellen",
		"modalTitleUpdate":"Kontakt aktualizieren",
		"contactName": "Vorname",
		"contactLastName": "Nachname",
		"contactEmail": "Email",
		"contactTelephone": "Telefon",
		"contactProvider": "Liferant",
		"requiredFieldsMessage": "Mit * gekennzeichnete Felder sind Pflichtfelder.",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};

	static es = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Contacto creado exitosamente",
			"alertConctactCreateError": "Ha ocurrido un error",
			"alertContactUpdatedSuccessfully": "Contacto actualizado exitosamente",
			"alertContactUpdateError": "Ha ocurrido un error"
		},
		"modalTitleCreate":"Crear contacto",
		"modalTitleUpdate":"Actualizar contacto",
		"contactName": "Nombre",
		"contactLastName": "Apellido",
		"contactEmail": "Email",
		"contactTelephone": "Teléfono",
		"contactProvider": "Proveedor",
		"requiredFieldsMessage": "Los campos marcados con * son requeridos.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
