import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { SharedService } from "../../../shared/services/shared.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-clients-list",
    templateUrl: "./clients-list.component.html",
    styleUrls: ["./clients-list.component.scss"]
})
export class ClientsListComponent implements OnInit {
    @Input() clients;

    newClient: boolean = false;
    editClient: boolean = false;
    showSearch: boolean = false;
    clientToHandle = {};
    clientsListText: any;
    alerts: any;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private paramMap$;
    private client_id;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _sharedService: SharedService,
        private _clientsService: ClientsService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.clientsListText = CLLanguage[cl];
        this.alerts = this.clientsListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.client_id = +paramMap.get("client_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForClients();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchClients() {
        this.subject.next(this.searchPattern);
    }

    searchForClients() {
        this._clientsService.getClientsByPattern(this.searchPattern);
    }

    closeSearch() {
        this.searchPattern = "";
        this._clientsService.getClients();
        this.showSearch = false;
    }

    startSearch() {
        this.editClient = false;
        this.newClient = false;
        this.showSearch = true;
    }

    startNewClient() {
        this.clientToHandle = {};
        this.editClient = false;
        this.showSearch = false;
        this.newClient = true;
    }

    startEditClient(client) {
        this.clientToHandle = Object.assign({}, client);
        this.newClient = false;
        this.showSearch = false;
        this.editClient = true;
    }

    forgetActions() {
        this.clientToHandle = {};
        this.editClient = false;
        this.newClient = false;
    }

    deleteClient(currentClient, index) {
        let title = currentClient.client_name;
        let text = this.alerts.confirmDeleteClient;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let client_id = currentClient.client_id;
                this._clientsService.deleteClient(client_id).subscribe(
                    data => {
                        if (data) {
                            const text = this.alerts.alertClientDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.clients.splice(index, 1);
                            this.router.navigate(["app/company/clients", 0]);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let text = this.alerts.alertClientDeleteError;
                            this._actionModalsService.alertError(text);
                        }
                    }
                );
            }
        });
    }
}

class CLLanguage {
    static en = {
        clientsListTitle: "Clients",
        alerts: {
            confirmDeleteClient: "Are you sure you want to delete this client ?",
            alertClientDeletedSuccessfully: "Client deleted successfully",
            alertForeignKeyViolation: "This client is still referenced in one of your documents",
            alertClientDeleteError: "An error ocurred"
        },
        newClient: "New Client",
        searchClients: "Search Clients",
        editClient: "Edit Client",
        deleteClient: "Delete Client"
    };

    static de = {
        clientsListTitle: "Kunden",
        alerts: {
            confirmDeleteClient: "Wollen Sie dieses Kunde löschen ?",
            alertClientDeletedSuccessfully: "Kunde erfolgreich gelöscht",
            alertForeignKeyViolation: "Diese Kunde ist immer noch referenziert in einer Ihrer Dokumenten",
            alertClientDeleteError: "Es ist ein Fehler aufgetreten"
        },
        newClient: "Neue Kunde",
        searchClients: "Kunden suchen",
        editClient: "Kunde bearbeiten",
        deleteClient: "Kunde löschen"
    };

    static es = {
        clientsListTitle: "Clientes",
        alerts: {
            confirmDeleteClient: "Esta seguro que deseas borrar este cliente ?",
            alertClientDeletedSuccessfully: "Cliente borrado exitosamente",
            alertForeignKeyViolation: "Este cliente se encuentra referenciado en uno(s) de sus documentos",
            alertClientDeleteError: "Ha ocurrido un error"
        },
        newClient: "Nuevo Cliente",
        searchClients: "Buscar Cliente",
        editClient: "Editar Cliente",
        deleteClient: "Eliminar Cliente"
    };
}
