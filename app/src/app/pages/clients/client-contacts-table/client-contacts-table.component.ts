import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { ClientsService } from '../../../shared/services/clients.service';
import { SharedService } from '../../../shared/services/shared.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { ContactFormComponent } from '../contact-form/contact-form.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
	selector: 'app-client-contacts-table',
	templateUrl: './client-contacts-table.component.html',
	styleUrls: ['./client-contacts-table.component.scss']
})
export class ClientContactsTableComponent implements OnInit, OnChanges, OnDestroy {

	@Input() client: any;
	clientContactsText: any;
	tableColumns: any;
	contacts = [];
	alerts: any;
	// subcription
	contactsList$: any;

	constructor(
		private _sharedService: SharedService,
		private _clientsService: ClientsService,
		private _actionModalsService: ActionModalsService,
		public dialog: MatDialog
	) {
		this.contactsList$ = this._clientsService.currentContactsList.subscribe(data => this.contacts = data);
	}

	ngOnInit() {
		let cl = this._sharedService.user_selected_language;
		this.clientContactsText = CCTLanguage[cl];
		this.tableColumns = this.clientContactsText.columns;
		this.alerts = this.clientContactsText.alerts;
	}

	ngOnChanges(changes) {
		let client_id = changes.client.currentValue.client_id;
		if (client_id) { this._clientsService.getContacts(client_id); }
	}

	ngOnDestroy() {
		this.contactsList$.unsubscribe();
	}

	editContact(contact) {
		let initialInformation = {};
		initialInformation['client'] = this.client;
		initialInformation['currentContact'] = contact;
		initialInformation['editContact'] = true;
		const dialogRef = this.dialog.open(ContactFormComponent, { width: '1000px', data: initialInformation });
	}

	deleteContactFromTable(contact, index) {
		let title = `${contact.contact_name} ${contact.contact_last_name}`;
		let text = this.alerts.confirmDeleteContact;
		this._actionModalsService.buildConfirmModal(title, text)
			.then(result => {
				if (result.value) {
					let cc_id = contact.cc_id;
					this._clientsService.deleteContactFromTable(cc_id).subscribe(data => {
						if (data) {
							let text = this.alerts.alertContactDeletedSuccess;
							this._actionModalsService.buildSuccessModal(title, text)
							this.contacts.splice(index, 1);
						}
					}, (e) => {
						if (e.error.error.code == '23503') { // handle foreign key constrain
							const text = this.alerts.alertForeignKeyViolation;
							this._actionModalsService.buildInfoModal(title, text);
						}
						else {
							let text = this.alerts.alertContactDeleteError;
							this._actionModalsService.alertError(text);
						}
					});
				}
			});
	}

}

class CCTLanguage {

	static en = {
		"columns": {
			"position": "Position",
			"contact_name": "Name",
			"contact_last_name": "Last Name",
			"contact_telephone": "Telephone",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContact": "Are you sure you want to delete this contact ?",
			"alertContactDeletedSuccess": "Contact deleted successfully",
			"alertForeignKeyViolation": "This contact is still referenced in one of your documents",
			"alertContactDeleteError": "An error ocurred"
		},
		"title": "Client Contacts",
		"options": "Options",
		"filterPlaceholder": "Search Items"
	};

	static de = {
		"columns": {
			"position": "Pos",
			"contact_name": "Vorname",
			"contact_last_name": "Nachname",
			"contact_telephone": "Telefon",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContact": "Möchten Sie diesen Kontakt wirklich löschen?",
			"alertContactDeletedSuccess": "Kontak erfolgreich gelöscht",
			"alertForeignKeyViolation": "Diese Kontakt ist immer noch referenziert in einer Ihrer Dokumenten",
			"alertContactDeleteError": "Es ist ein Fehler aufgetreten",
		},
		"title": "Kundekontakten",
		"options": "Optionen",
		"filterPlaceholder": "Tabelle Suchen"
	};

	static es = {
		"columns": {
			"position": "Posición",
			"contact_name": "Nombre",
			"contact_last_name": "Apellido",
			"contact_telephone": "Telefóno",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContact": "Esta seguro que desea eliminar el contacto actual ?",
			"alertContactDeletedSuccess": "Contacto eliminado exitosamente",
			"alertForeignKeyViolation": "Este contacto está referenciado en una de sus documentos",
			"alertContactDeleteError": "Ha ocurrido un error",
		},
		"title": "Contactos del Cliente",
		"options": "Opciones",
		"filterPlaceholder": "Buscar Items"
	};
}
