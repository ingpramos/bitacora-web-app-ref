import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientContactsTableComponent } from './client-contacts-table.component';

describe('ClientContactsTableComponent', () => {
  let component: ClientContactsTableComponent;
  let fixture: ComponentFixture<ClientContactsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientContactsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientContactsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
