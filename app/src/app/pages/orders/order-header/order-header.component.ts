import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ExcelService } from "../../../shared/services/excel.service";
import { SharedService } from "../../../shared/services/shared.service";
import { OrderPdfService } from "../../../shared/services/pdfs/order-pdf.service";
import { OrdersService } from "../../../shared/services/orders.service";
import { SearchItemsToInsertInOrderComponent } from "../search-items-to-insert-in-order/search-items-to-insert-in-order.component";
import { InsertExcelIntoOrderComponent } from "../insert-excel-into-order/insert-excel-into-order.component";

@Component({
    selector: "app-order-header",
    templateUrl: "./order-header.component.html",
    styleUrls: ["./order-header.component.scss"]
})
export class OrderHeaderComponent implements OnInit, OnDestroy {
    @Input() currentOrder;
    @Input() zeroOrders: boolean; // to enable or disable menu items
    @Input() openOrdersView: boolean;
    @Input() showMenu: boolean; // to show or hide three points menu

    orderHeaderText: any;
    itemsInOrder: number = 0;
    // subscriptions
    itemsList$;
    constructor(
        private _excelService: ExcelService,
        private _sharedService: SharedService,
        private _orderPdfService: OrderPdfService,
        private _ordersService: OrdersService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        // laguages initialitation
        let cl = this._sharedService.user_selected_language;
        this.orderHeaderText = OHCLanguage[cl];
        this.itemsList$ = this._ordersService.currentItemsList.subscribe(data => (this.itemsInOrder = data.length));
    }
    ngOnDestroy() {
        this.itemsList$.unsubscribe();
    }

    searchForItems() {
        let initialState = { currentOrder: this.currentOrder, itemsInOrder: this.itemsInOrder };
        const dialogRef = this.dialog.open(SearchItemsToInsertInOrderComponent, { width: "1200px", data: initialState });
        dialogRef.afterClosed().subscribe(result => {
            (<HTMLInputElement>document.getElementById("i-input")).value = "";
        });
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    generatePdfReport() {
        this._orderPdfService.buildDocument(this.currentOrder).subscribe(doc => {
            let documentName = `${this.currentOrder.order_number} - ${this.currentOrder.provider_name}`;
            this._orderPdfService.pdfMake.createPdf(doc).download(documentName);
        });
    }

    downloadExcelOrder() {
        let documentName = `${this.currentOrder.order_number} - ${this.currentOrder.provider_name}.xlsx`;
        const columnsToDelete = ["iio_id", "order_id", "item_id", "user_name", "user_last_name", "item_delivered", "material_id"];
        let itemsInOrder = this._ordersService.getItemsInOrder("excel");
        this._excelService.deleteObjectProperties(itemsInOrder, columnsToDelete);
        this._excelService.downLoadXlsxFile(itemsInOrder, documentName, this.currentOrder.order_number);
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentOrder"] = this.currentOrder;
            initialState["itemsInOrder"] = this.itemsInOrder;
            const dialogRef = this.dialog.open(InsertExcelIntoOrderComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }
}

class OHCLanguage {
    static en = {
        orderNumber: "Order Number",
        orderProvider: "Provider",
        orderStatus: "Status",
        orderItems: "Items",
        orderProjects: "Projects",
        searchItemsToInsert: "Insert Items",
        downloadExcelFile: "Download Order Excel",
        uploadCsv: "Upload Excel File",
        downloadPdfFile: "Download Order PDF",
        filterTable: "Search Item List"
    };
    static de = {
        orderNumber: "Bestellungsnummer",
        orderProvider: "Lieferant",
        orderStatus: "Status",
        orderItems: "Artikel",
        orderProjects: "Aufträge",
        searchItemsToInsert: "Artikel einfügen",
        downloadExcelFile: "Excel Bestellung herrunterladen",
        uploadCsv: "Excel Datei hochladen",
        downloadPdfFile: "PDF Bestellung herrunterladen",
        filterTable: "Tabelle Suchen"
    };
    static es = {
        orderNumber: "Número de Pedido",
        orderProvider: "Proveedor",
        orderStatus: "Estado",
        orderItems: "Items",
        orderProjects: "Proyectos",
        searchItemsToInsert: "Añadir Items",
        downloadExcelFile: "Descargar Pedido Excel",
        uploadCsv: "Importar Excel con Items",
        downloadPdfFile: "Descargar Pedido PDF",
        filterTable: "Buscar Item"
    };
}
