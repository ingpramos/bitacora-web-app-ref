import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertExcelIntoOrderComponent } from './insert-excel-into-order.component';

describe('InsertExcelIntoOrderComponent', () => {
  let component: InsertExcelIntoOrderComponent;
  let fixture: ComponentFixture<InsertExcelIntoOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertExcelIntoOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertExcelIntoOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
