import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { OrdersService } from "../../../shared/services/orders.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-insert-excel-into-order",
    templateUrl: "./insert-excel-into-order.component.html",
    styleUrls: ["./insert-excel-into-order.component.scss"]
})
export class InsertExcelIntoOrderComponent implements OnInit {
    // modal data
    fileInformation: any = this.modalData.fileInformation;
    currentOrder: any = this.modalData.currentOrder;
    itemsInOrder: number = this.modalData.itemsInOrder;
    // tabs
    selectedTab = 0;
    showJustSelectedItems:boolean = true;
    tableText: any;
    alerts: any;
    tableColumns: any;
    itemsFromExcel: any = this.modalData.itemsFromExcel;
    items: any[] = [];  // property in view
    selectedItems = [];
    itemsAlreadyInList = []; // to hold items already registered in the order
    itemsNotFoundInDB = []; // to hold all items that don't exist in db
    allChecked: boolean = true;
    processingRequest: boolean = false;
    constructor(
        private _excelService: ExcelService,
        private _columnsNamesService: ColumnsNamesService,
        private _sharedService: SharedService,
        private _ordersService: OrdersService,
        private _itemsService: ItemsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<InsertExcelIntoOrderComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.tableText = IEIOLanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
    }

    ngOnInit() {
        let itemsInOrder = this._ordersService.getItemsInOrder("view");
        if (this.itemsFromExcel.length > 0) {
            this._excelService.labelItemsAlreadyInList(itemsInOrder, this.itemsFromExcel);
            this.itemsAlreadyInList = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === true);
            let preSelectedItems: any[] = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === false);
            if(preSelectedItems.length > 0){ // check in db if items exists or not
                let arrayOfCodes = preSelectedItems.map((obj: any) => obj["item_code"]);
                this._itemsService.checkIfItemsExists(arrayOfCodes).subscribe(itemsFound => {
                    this._excelService.labelItemsAlreadyInDB(itemsFound, preSelectedItems);
                    this.itemsNotFoundInDB = preSelectedItems.filter(obj => obj.is_already_in_db === false);
                    this.selectedItems = preSelectedItems.filter(obj => obj.is_already_in_db === true);
                    this.items = this.selectedItems; // init tab 0
                });
            }            
        }
        this._columnsNamesService.formatColumnsOfImportTable(this.tableColumns);
    }

    applyChangesToList(allChecked: boolean) {
        this.itemsFromExcel.forEach((obj: any) => {
            if (obj.is_already_in_list === false && obj.is_already_in_db === true) {
                obj.is_checked = allChecked;
            }
        });
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedItems = this.itemsFromExcel.filter((obj: any) => {
            return obj.is_checked === true && obj.is_already_in_db === true && obj.is_already_in_list === false;
        });
    }

    saveItems() {        
		this.processingRequest = true;
        let order_id = this.currentOrder.order_id;
        let formattedItems = this.formatItemsToSave(this.selectedItems, order_id);
        this._ordersService.insertItemsInOrder(formattedItems).subscribe(
            data => {
                if (data) {
                    this.processingRequest = false;
                    this._ordersService.getItemsFromOrder(order_id);
                    let title = this.currentOrder.order_number;
                    let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeModal();
                }
            },
            () => {
                let text = this.alerts.alertItemsInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    closeModal() {
        this.dialogRef.close();
    }

    formatItemsToSave(collection: any[], id) {
        collection.forEach((obj: any) => {
            obj["order_id"] = id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(obj["item_buy_price_to_show"]);
            obj["user_id"] = this._sharedService.user_id;
        });
        return collection;
    }

    
    showSelectedItems(){
        this.items = this.selectedItems;
        this.showJustSelectedItems = true;
    }
    showItemsAlreadyInList(){
        this.items = this.itemsAlreadyInList;
        this.showJustSelectedItems = false;
    }
    showItemsNotFoundInDB(){
        this.items = this.itemsNotFoundInDB;
        this.showJustSelectedItems = false;
    }

    // Tabs related functions
    updateTab(index){ // update displayed view according the selected tab
        switch (index) {
            case 0 : this.showSelectedItems();
            break;
            case 1 : this.showItemsAlreadyInList();
            break;
            case 2 : this.showItemsNotFoundInDB();
            break;
        }        
    }
}

class IEIOLanguages {
    static en = {
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name",
            item_buy_price: "Price"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        modalTitle: "Import items from excel in order",
        fileName: "File Name",
        itemsFound: "Items in File",
        itemsAlreadyInList: "Items Already in Assembly",
        itemsNotFoundInDB: "Not Registered Items",
        itemsSelected: "Selected Items",
		checkColumnsNames: "No columns where recognized, please check the name of the header columns in your document, the columns marked with * are required.",
		savingProcess:"Saving ...",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name",
            item_buy_price: "Preis"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Bestellung einfügen",
        fileName: "Dateiname",
        itemsFound: "Artikel in Datei",
        itemsAlreadyInList: "bereits vorhandene Artikel",
        itemsNotFoundInDB: "Nicht registrierte Artikel",
        itemsSelected: "ausgewählte Artikel",
        checkColumnsNames:
			"Es wurden keine Spalten erkannt. Bitte überprüfen Sie den Namen der Kopfzeilenspalten in Ihrem Dokument. Die mit * gekennzeichneten Spalten sind erforderlich.",
		savingProcess:"Wird gespeichert ...",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre",
            item_buy_price: "Precio"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Importar Articulos en Orden de Compra",
        fileName: "Nombre del Archivo",
        itemsFound: "Items Encontrados",
        itemsAlreadyInList: "Articulos repetidos",
        itemsNotFoundInDB: "Articulos no registrados",
        itemsSelected: "Items Seleccionados",
        checkColumnsNames:
			"no se reconocieron columnas, verifique el nombre de las columnas del encabezado en su documento, las columnas marcadas con * son obligatorias.",
		savingProcess:"Guardando ...",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
