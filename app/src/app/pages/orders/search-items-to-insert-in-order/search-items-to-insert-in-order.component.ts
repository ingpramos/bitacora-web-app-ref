import { Component, OnInit, Inject } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { OrdersService } from "../../../shared/services/orders.service";
import { ItemsService } from "../../../shared/services/items.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-search-items-to-insert-in-order",
    templateUrl: "./search-items-to-insert-in-order.component.html",
    styleUrls: ["./search-items-to-insert-in-order.component.scss"]
})
export class SearchItemsToInsertInOrderComponent implements OnInit {
    selectedTab = 0;
    currentOrder: any = this.modalData.currentOrder;
    itemsInOrder: number = this.modalData.itemsInOrder;
    tableText: any = {};
    alerts: any;
    tableColumns: any = {};
    // search tab
    searchPattern: string;
    items: any = [];
    itemsAlreadyInList: any[] = [];
    selectedItems: any[] = []; // during a single search
    private subject: Subject<string> = new Subject();
    allChecked:boolean = false; // checkbox for the search scope
    // items confirmed Tab
    itemsToInsert : any[] = []; // to have a reference list to eventually choose from
    itemsConfirmedToInsert :any[] = []; // the ones which finnally will be inserted in the order   
    allConfirmedChecked:boolean = true; // checkbox for the confirm scope
	showJustSelectedItems:boolean = false;
    constructor(
        public _ordersService: OrdersService,
        private _sharedService: SharedService,
        private _itemsService: ItemsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<SearchItemsToInsertInOrderComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {}

    ngOnInit() {
        // language initialization
        let cl = this._sharedService.user_selected_language;
        this.tableText = SITILanguage[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.items = [];
                return;
            } else {
                this.callItems();
            }
        });
    }

    searchItems() {
        this.subject.next(this.searchPattern);
    }

    callItems() {
        this._itemsService.getItemsByNameOrCode(this.searchPattern).subscribe(data => {
            let itemsInOC = this._ordersService.getItemsInOrder("view");
            this._itemsService.labelItemsAlreadyInList(itemsInOC, data);
            this.itemsAlreadyInList = data.filter((obj: any) => obj.is_already_in_list === true);
            this.items = data;
            this.updateSelectedItemsList();
        });
    }

    applyChangesToList(allChecked: boolean) {
        this.items.forEach((obj: any) => {
            if (!obj.is_already_in_list) obj.is_checked = allChecked;
        });
        this.updateSelectedItemsList();
    }    
    // function to be called when using the search tab
    updateSelectedItemsList() {
        this.selectedItems = this.items.filter((obj: any) => obj.is_checked === true && obj.is_already_in_list === false);
    }

    applyChangesToComfirmedList(allConfirmedChecked:boolean){
        this.itemsToInsert.forEach((obj: any) => {
             obj.is_confirmed_to_be_inserted = allConfirmedChecked;
        });
        this.updateItemsToInsertCollection();
    }
    // function to be called when using the confirmed items Tab
    updateItemsToInsertCollection(){
        this.itemsConfirmedToInsert = this.itemsToInsert.filter((obj: any) => obj.is_confirmed_to_be_inserted === true);
    }
	
	showSelectedItems(){
        this.showJustSelectedItems = true;
        this.applyChangesToComfirmedList(true); // to initialize all checkboxes as confirmed
        this.updateItemsToInsertCollection();
		this.items = this.itemsToInsert;
    }
    
	confirmItems(){ 
        Array.prototype.push.apply(this.itemsToInsert, this.selectedItems);
        this.selectedItems = [];
        this.selectedTab = 1;
        this.showSelectedItems();
	}

	showSearchItems(){
        this.showJustSelectedItems = false;
        this.itemsToInsert = this.itemsConfirmedToInsert;
        this.items = [];
        this.searchPattern = "";
        this.itemsAlreadyInList = [];
        this.selectedTab = 0;
	}

    saveItems() {
        let order_id = this.currentOrder.order_id;
        let itemsConfirmedToInsertFormatted = this.formatItemsToInsert(this.itemsConfirmedToInsert,order_id);
        this._ordersService.insertItemsInOrder(itemsConfirmedToInsertFormatted).subscribe(
            data => {
                if (data) {
                    this._ordersService.getItemsFromOrder(order_id);
                    let title = this.currentOrder.order_number;
                    let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeModal();
                }
            },
            () => {
                let text = this.alerts.alertItemsInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatItemsToInsert(collection: any[], id) {
        collection.forEach((obj: any) => {
            obj["order_id"] = id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(obj["item_buy_price_to_show"]);
            obj["user_id"] = this._sharedService.user_id;
        });
        return collection;
    }

    closeModal() {
        this.items = [];
        this.selectedItems = [];
        this.searchPattern = undefined;
        this.dialogRef.close();
    }

    // Tabs related functions
    updateTab(index){ // update displayed view according the selected tab
        switch (index) {
            case 0 : this.showSearchItems();
            break;
            case 1 : this.showSelectedItems();
            break;
        }        
    }
}

class SITILanguage {
    static en = {
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name",
            item_buy_price: "Price"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        modalTitle: "Insert items in purchase order",
        searchItems: "Search Items",
        itemsFound: "Items Found",
        itemsAlreadyInList: "Items Already in Order",
		itemsSelected: "Items Selected",
		itemsConfirmed:"Items Confirmed",		
		confirmSelectedItems:"Confirm Selected Items",
		showSelectedItems:"Show Selected Items",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name",
            item_buy_price: "Preis"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Bestellung einfügen",
        searchItems: "Artikel suchen",
        itemsFound: "Artikel gefunden",
        itemsAlreadyInList: "bereits vorhandene Artikel",
		itemsSelected: "ausgewählte Artikel",
		itemsConfirmed:"Artikel bestätigt",		
		confirmSelectedItems:"Ausgewählte Artikel bestätigen",
		showSelectedItems:"Ausgewählte Artikel anzeigen",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre",
            item_buy_price: "Precio"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar articulos en orden de compra",
        searchItems: "Buscar Items",
        itemsFound: "Items Encontrados",
        itemsAlreadyInList: "Articulos Repetidos",
		itemsSelected: "Items Selecionados",
		itemsConfirmed:"Items Confirmados",		
		confirmSelectedItems:"Confirmar Items Seleccionados",
		showSelectedItems:"Mostrar Items Seleccionados",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
