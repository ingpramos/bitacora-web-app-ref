import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInOrderComponent } from './search-items-to-insert-in-order.component';

describe('SearchItemsToInsertInOrderComponent', () => {
  let component: SearchItemsToInsertInOrderComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
