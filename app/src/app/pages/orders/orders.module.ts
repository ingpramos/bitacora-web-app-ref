import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "../../shared/shared.module";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
// Components
import { OrdersComponent } from './orders.component';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { OrderHeaderComponent } from './order-header/order-header.component';
import { ItemsInOrderTableComponent } from './items-in-order-table/items-in-order-table.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { ItemInOrderFormComponent } from './item-in-order-form/item-in-order-form.component';
import { SearchItemsToInsertInOrderComponent } from './search-items-to-insert-in-order/search-items-to-insert-in-order.component';
import { InsertExcelIntoOrderComponent } from './insert-excel-into-order/insert-excel-into-order.component';
import { ItemsInOrderTableColumnsFormComponent } from './items-in-order-table-columns-form/items-in-order-table-columns-form.component';

// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatButtonModule, MatIconModule,
} from '@angular/material';
import { DirectivesModule } from '../../shared/directives/directives.module';


const ORDERS_ROUTES: Routes = [
	{ path: '', component: OrdersComponent }
];

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		RouterModule.forChild(ORDERS_ROUTES),
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		//MomentDateAdapter,
		MatButtonModule,
		MatIconModule,
		PipesModule,
		DirectivesModule
	],
	declarations: [
		OrdersComponent,
		OrdersListComponent,
		OrderHeaderComponent,
		ItemsInOrderTableComponent,
		OrderFormComponent,
		ItemInOrderFormComponent,
		SearchItemsToInsertInOrderComponent,
		InsertExcelIntoOrderComponent,
		ItemsInOrderTableColumnsFormComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } },
	],
	entryComponents: [
		SearchItemsToInsertInOrderComponent,
		InsertExcelIntoOrderComponent,
		ItemInOrderFormComponent,
		ItemsInOrderTableColumnsFormComponent]
})

export class OrdersModule { }
