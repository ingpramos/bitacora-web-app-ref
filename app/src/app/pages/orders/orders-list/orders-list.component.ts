import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { OrdersService } from "../../../shared/services/orders.service";
import { SharedService } from "../../../shared/services/shared.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-orders-list",
    templateUrl: "./orders-list.component.html",
    styleUrls: ["./orders-list.component.scss"]
})
export class OrdersListComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activatedRoute.snapshot["_urlSegment"].segments[1].path;
    openOrdersView = this.componentUrl === "open-orders" ? true : false;

    @Input() orders;
    @Input() selectedOrder;
    // init languages settings
    ordersListText: any;
    alerts: any;
    // to show or hide the order Form
    editOrder: boolean = false;
    newOrder: boolean = false;
    showSearch: boolean = false;
    orderToHandle: any = {};
    itemsFromSelectedOrder: any;
    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private paramMap$;
    private order_id;
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _ordersService: OrdersService,
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.ordersListText = OLLanguage[cl];
        this.alerts = this.ordersListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.order_id = +paramMap.get("order_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForOrders();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchOrders() {
        this.subject.next(this.searchPattern);
    }

    searchForOrders() {
        let order_state = this.openOrdersView ? 1 : 0;
        this._ordersService.getOrdersByPattern(order_state, this.searchPattern);
    }

    closeSearch() {
        let order_state = this.openOrdersView ? 1 : 0;
        this.searchPattern = "";
        this._ordersService.getOrders(order_state);
        this.showSearch = false;
    }

    startEditOrder(order) {
        this.orderToHandle = JSON.parse(JSON.stringify(order));
        this.newOrder = false;
        this.showSearch = false;
        this.editOrder = true;
        window.scroll(0, 0);
    }

    startNewOrder() {
        this.editOrder = false;
        this.showSearch = false;
        this.newOrder = true;
        this.orderToHandle = {};
    }

    startSearch() {
        this.editOrder = false;
        this.newOrder = false;
        this.showSearch = true;
    }

    forgetActions(value) {
        this.editOrder = false;
        this.newOrder = false;
    }

    deleteOrder(order, index) {
        let title = order.order_number;
        let text = this.alerts.confirmDeleteOrder;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let order_id = order.order_id;
                this._ordersService.deleteOrder(order_id).subscribe(
                    data => {
                        if (data) {
                            let title = order.order_number;
                            let text = this.alerts.alertOrderDeletedSuccessfully;
                            this.orders.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                            this._actionModalsService.buildSuccessModal(title, text);
                        }
                    },
                    () => {
                        let text = this.alerts.alertOrderDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    closeOrder(order) {
        let order_id = order.order_id;
        this._ordersService.closeOrder(order_id).subscribe(data => {
            this.router.navigate(["app/closed-orders", order_id]);
        });
    }
}

class OLLanguage {
    static en = {
        alerts: {
            confirmDeleteOrder: "Are you sure you want to delete this order ?",
            alertOrderDeletedSuccessfully: "Order deleted successfully",
            alertOrderDeleteError: "An error ocurred"
        },
        newOrder: "Create Order",
        searchOrders: "Search Orders",
        openOrders: "Open Orders",
        closedOrders: "Closed Orders",
        editOrder: "Edit Order",
        deleteOrder: "Delete Order",
        closeOrder: "Close Order",
        creationDate: "Created at",
        deliveryDate: "Delivery"
    };

    static de = {
        alerts: {
            confirmDeleteOrder: "Wollen Sie dieses Bestellung löschen ?",
            alertOrderDeletedSuccessfully: "Bestellung erfolgreich gelöscht",
            alertOrderDeleteError: "Es ist ein Fehler aufgetreten"
        },
        newOrder: "Bestellung erstellen",
        searchOrders: "Bestellungen suchen",
        openOrders: "Offene Bestellungen",
        closedOrders: "Geschlossene Bestellungen",
        editOrder: "Bestellung bearbeiten",
        deleteOrder: "Bestellung löschen",
        closeOrder: "Bestellung schließen",
        creationDate: "Erstellt",
        deliveryDate: "Lifertermin"
    };

    static es = {
        alerts: {
            confirmDeleteOrder: "Esta seguro que deseas borrar la siguiente orden ?",
            alertOrderDeletedSuccessfully: "Orden Borrada Exitosamente",
            alertOrderDeleteError: "Ha ocurrido un error"
        },
        newOrder: "Crear Pedido",
        searchOrders: "Buscar Ordenes",
        openOrders: "Pedidos Abiertos",
        closedOrders: "Pedidos  Cerrados",
        editOrder: "Editar Pedido",
        deleteOrder: "Eliminar Pedido",
        closeOrder: "Cerrar Pedido",
        creationDate: "Creación",
        deliveryDate: "Entrega"
    };
}
