import { Component, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from '../../shared/services/orders.service';

@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss']
})

export class OrdersComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activeRoute.snapshot['_urlSegment'].segments[1].path;
    openOrdersView = this.componentUrl === "open-orders" ? true : false;
    zeroOrders: boolean;
    // component subscritions
    ordersList$;
    orderToPushOrUpdate$;
    // component properties
    order_id;
    orders: any = [];
    selectedOrder: any = {};

    constructor(
        private activeRoute: ActivatedRoute,
        private router: Router,
        private _ordersService: OrdersService
    ) {
        this._ordersService.passOrderToPushOrUpdate({}); // to clean the observable when entering in the view       
        this.orderToPushOrUpdate$ = this._ordersService.orderToPushOrUpdate.subscribe(data => {
            let order_id = data.order_id;
            if (!order_id) return; // case {} an initialization
            let index = this.orders.findIndex(obj => obj['order_id'] == order_id);
            if (index == -1) {  // group was created 
                this.orders.push(data);
                this.orders.length > 0 ? this.zeroOrders = false : this.zeroOrders = true;
                this.router.navigate(['app/' + this.componentUrl, order_id]);
            } else { // group was updated
                this.orders.splice(index, 1, data);
                this.selectedOrder = data;
                this._ordersService.setCurrentOrder(this.selectedOrder);
                this._ordersService.calculateOrderTotalPriceModel();
            }
        });
        this.activeRoute.params.pipe(map(params => params['order_id'])).subscribe(order_id => {
            this.order_id = order_id;
            if (this.order_id) this.setSelectedOrder();
        });
        this.ordersList$ = this._ordersService.currentOrdersList.subscribe(data => {
            this.orders = data;
            this.setSelectedOrder();
        });
    }


    ngOnInit() {
        this.openOrdersView ? this._ordersService.getOrders(1) : this._ordersService.getOrders(0);
    }

    ngOnDestroy() {
        this.orderToPushOrUpdate$.unsubscribe();
        this.ordersList$.unsubscribe();
        this._ordersService.updateOrdersList([]);
    }

    setSelectedOrder() {
        this.orders.length > 0 ? this.zeroOrders = false : this.zeroOrders = true;
        if (this.orders.length > 0) {
            let found = this.orders.find((obj) => obj['order_id'] == this.order_id);
            this.selectedOrder = found ? found : this.orders[0];
            this.router.navigate(['app/' + this.componentUrl, this.selectedOrder.order_id]);
            window.scroll(0, 0);
        }
        else {
            this.selectedOrder = {};
        }
        this._ordersService.setCurrentOrder(this.selectedOrder); // to make it avaiable from the service
    }

}
