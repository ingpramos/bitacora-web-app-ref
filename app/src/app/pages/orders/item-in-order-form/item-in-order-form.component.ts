import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { OrdersService } from "../../../shared/services/orders.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { CurrencyPipe } from "@angular/common";

@Component({
    selector: "item-in-order-form",
    templateUrl: "./item-in-order-form.component.html",
    styleUrls: ["./item-in-order-form.component.scss"]
})
export class ItemInOrderFormComponent implements OnInit {
    currentLanguage: string;
    itemFormText: any;
    currentItem: any = this.modalData.currentItem;
    constructor(
        private _actionModalsService: ActionModalsService,
        private _currencyPipe: CurrencyPipe,
        private _sharedService: SharedService,
        private _ordersService: OrdersService,
        public dialogRef: MatDialogRef<ItemInOrderFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemFormText = IIOFCLanguage[this.currentLanguage];
    }

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentItem = JSON.parse(JSON.stringify(this.currentItem));
        // preformat the values to show correct format when opening this component
        let ibp = Number(this.currentItem["item_buy_price"]).toFixed(2);
        let itp = Number(this.currentItem["item_total_price"]).toFixed(2);
        this.currentItem["item_buy_price_to_show"] = this._sharedService.parseToLocaleString(ibp).trim();
        this.currentItem["item_total_price"] = this._sharedService.parseToLocaleString(itp).trim();
    }

    saveItem(item) {
        this.currentItem["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(this.currentItem["item_buy_price_to_show"]);
        this._ordersService.updateItemInOrder(item).subscribe(
            data => {
                if (data) {
                    let order_id = item.order_id;
                    this._ordersService.getItemsFromOrder(order_id);
                    this.closeModal();
                    let title = item.item_code;
                    let text = this.itemFormText.alertItemUpdateSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let message = this.itemFormText.alertItemUpdateError;
                this._actionModalsService.alertError(message);
            }
        );
    }

    closeModal() {
        this.currentItem = {};
        this.dialogRef.close();
    }

    calculateTotalPrice() {
        let stringValue = this.currentItem["item_buy_price_to_show"];
        let priceAsNumber: Number = this._sharedService.parseFormattedStringAsNumber(stringValue);
        let itemTotalPrice = Number(this.currentItem["item_amount"]) * Number(priceAsNumber);
        this.currentItem["item_total_price"] = this._currencyPipe.transform(itemTotalPrice, this._sharedService.currency, "", "", this.currentLanguage);
    }
}

class IIOFCLanguage {
    static en = {
        alertItemUpdateSuccess: "Item updated successfully",
        alertItemUpdateError: "An error ocurred",
        modalTitle: "Update item information",
        itemCode: "Item Code",
        itemOrderNumber: "Order Number",
        itemName: "Item Name",
        itemAmount: "Amount",
        itemBuyPrice: "Price",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Received",
        itemDeliveryDate: "Delivery Date",
        itemLastUser: "Last user",
        itemInOrderDetails: "Details",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alertItemUpdateSuccess: "Artikel erfolgreich aktualiziert",
        alertItemUpdateError: "Es ist ein Fehler aufgetreten",
        modalTitle: "Artikel information aktualizieren",
        itemCode: "Artikel Code",
        itemOrderNumber: "Bestellnummer",
        itemName: "Name",
        itemAmount: "Menge",
        itemBuyPrice: "Preis",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Erhalten",
        itemDeliveryDate: "Lieferdatum",
        itemLastUser: "letzter Benutzer",
        itemInOrderDetails: "Details",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alertItemUpdateSuccess: "Item actualizado exitosamente",
        alertItemUpdateError: "Ha ocurrido un error",
        modalTitle: "Actualizar informacion del articulo",
        itemCode: "Código Del Item",
        itemOrderNumber: "Número de Compra",
        itemName: "Nombre",
        itemAmount: "Cantidad",
        itemBuyPrice: "Precio",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Recibido",
        itemDeliveryDate: "Fecha de Recibido",
        itemLastUser: "Último usuario",
        itemInOrderDetails: "Detalles",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
