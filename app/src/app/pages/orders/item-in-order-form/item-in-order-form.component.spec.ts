import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInOrderFormComponent } from './item-in-order-form.component';

describe('ItemInOrderFormComponent', () => {
  let component: ItemInOrderFormComponent;
  let fixture: ComponentFixture<ItemInOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
