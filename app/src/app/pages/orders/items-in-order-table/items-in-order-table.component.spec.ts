import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOrderTableComponent } from './items-in-order-table.component';

describe('ItemsInOrderTableComponent', () => {
  let component: ItemsInOrderTableComponent;
  let fixture: ComponentFixture<ItemsInOrderTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOrderTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOrderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
