import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { OrdersService } from "../../../shared/services/orders.service";
import { ItemInOrderFormComponent } from "../item-in-order-form/item-in-order-form.component";
import { SharedService } from "../../../shared/services/shared.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ItemsInOrderTableColumnsFormComponent } from "../items-in-order-table-columns-form/items-in-order-table-columns-form.component";

@Component({
    selector: "app-items-in-order-table",
    templateUrl: "./items-in-order-table.component.html",
    styleUrls: ["./items-in-order-table.component.scss"]
})
export class ItemsInOrderTableComponent implements OnInit, OnChanges, OnDestroy {
    // inputs
    @Input() currentOrder: any;
    // compoment properties
    companyCurrency;
    currentLanguage;
    itemsInOrderText: any;
    tableColumns: any = {}; // the column names that will be shown in the view
    defaultTableColumnsObj: any; // obj with default column names
    alerts: any;
    items = [];
    orderTotalFooter: any;
    // subscriptions
    itemsList$;
    columnsNames$;
    totalModel$;
    constructor(
        private _columnsNamesService: ColumnsNamesService,
        private _actionModalsService: ActionModalsService,
        public _sharedService: SharedService,
        public _ordersService: OrdersService,
        public dialog: MatDialog
    ) {
        this.companyCurrency = this._sharedService.currency;        
        // Language Initalitation
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInOrderText = IIOTLanguages[this.currentLanguage];
        this.defaultTableColumnsObj = this.itemsInOrderText.columns;
        this.alerts = this.itemsInOrderText.alerts;
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_orders", this.defaultTableColumnsObj);
    }

    ngOnInit() {
		this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => this.tableColumns = data);
        // to update the items when new ones are inserted
        this.itemsList$ = this._ordersService.currentItemsList.subscribe(data => {
            this.items = data;
            this._ordersService.calculateOrderTotalPriceModel();
		});
		this.totalModel$ = this._ordersService.orderTotalPriceModelObs.subscribe(data => {
            this.orderTotalFooter = data;
            this.currentOrder.subTotal = this.orderTotalFooter.subTotal;
            this.currentOrder.vat = this.orderTotalFooter.vat;
			this.currentOrder.total = this.orderTotalFooter.total;
			this._sharedService.stopLoading();
        });
    }

    ngOnChanges(changes) {
        let order_id = changes.currentOrder.currentValue["order_id"];
        if (order_id) {
			this._ordersService.getItemsFromOrder(order_id);
			this._sharedService.startLoading();
        }
        if (typeof order_id === "undefined") {
            this._ordersService.updateItemList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
        this.totalModel$.unsubscribe();
    }

    editItemInOrder(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemInOrderFormComponent, { width: "1000px", data: initialState });
    }

    editTableColumns() {
        let initialState = { defaultTableColumnsObj: this.defaultTableColumnsObj, currentTableColumns: this.tableColumns };
        const dialogRef = this.dialog.open(ItemsInOrderTableColumnsFormComponent, { width: "1000px", data: initialState });
    }

    deleteItemFromOrder(item, index) {
        let title = item.item_code;
        let text = this.alerts.confirmDeleteItemFromOrder;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let iio_id = item.iio_id;
                this._ordersService.deleteItemFromOrder(iio_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_code;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._ordersService.updateItemList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class IIOTLanguages {
    constructor() {}
    user_selected_language = "en";
    static en = {
        title: "Items in Order",
        totalWithoutVAT: "Total without VAT",
        vatFrom: "VAT from ",
        totalWithVAT: "Total with VAT",
        columns: {
            item_position: "Pos",
            item_delivered: "Status",
            item_code: "Item Code",
            item_order_number: "Item Order Number",
            item_name: "Item Description",
            item_amount: "Amount",
            received_amount: "Delivered",
            item_buy_price: "Price(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOrder: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns Names"
    };

    static de = {
        title: "Artikel in Bestellung",
        totalWithoutVAT: "Total exkl. MwSt.",
        vatFrom: "MwSt von",
        totalWithVAT: "Total inkl. MwSt.",
        columns: {
            item_position: "Pos",
            item_delivered: "Status",
            item_code: "Artikel Code",
            item_order_number: "Bestellnummer",
            item_name: "Artikelbezeichnung",
            item_amount: "Menge",
            received_amount: "Geliefert",
            item_buy_price: "Preis(E)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOrder: "Sind Sie sicher das Sie dieses Artikel löschen möchte ?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Spaltenamen ändern"
    };

    static es = {
        title: "Articulos Pedidos",
        totalWithoutVAT: "Total sin IVA",
        vatFrom: "IVA de ",
        totalWithVAT: "Total Con IVA",
        columns: {
            item_position: "Pos",
            item_delivered: "Estado",
            item_code: "Código del Item",
            item_order_number: "Número de Compra",
            item_name: "Descripción",
            item_amount: "Cantidad",
            received_amount: "Recibido",
            item_buy_price: "Precio(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOrder: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item eliminado exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Nombre de las Columnas"
    };
}
