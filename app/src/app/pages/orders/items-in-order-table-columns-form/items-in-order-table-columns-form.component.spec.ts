import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOrderTableColumnsFormComponent } from './items-in-order-table-columns-form.component';

describe('ItemsInOrderTableColumnsFormComponent', () => {
  let component: ItemsInOrderTableColumnsFormComponent;
  let fixture: ComponentFixture<ItemsInOrderTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOrderTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOrderTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
