import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-items-in-order-table-columns-form",
    templateUrl: "./items-in-order-table-columns-form.component.html",
    styleUrls: ["./items-in-order-table-columns-form.component.scss"]
})
export class ItemsInOrderTableColumnsFormComponent implements OnInit {
    itemsInOrdersTableColumnsText: any;
    defaultTableColumnsObj: any = this.modalData.defaultTableColumnsObj;
    currentTableColumns: any = this.modalData.currentTableColumns; // from the table
    currentColumnsObj: any; // for the form
    arrayOfTableColumns: any;

    constructor(
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        private _actionModalService: ActionModalsService,
        public dialogRef: MatDialogRef<ItemsInOrderTableColumnsFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {}

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentColumnsObj = JSON.parse(JSON.stringify(this.currentTableColumns));
        let cl = this._sharedService.user_selected_language;
        this.itemsInOrdersTableColumnsText = IIOTCFLanguages[cl];
    }

    saveColumns() {
        if (this._sharedService.isObjectEquivalent(this.currentColumnsObj, this.currentTableColumns)) {
            this.closeModal();
            return;
        }
        this._columnsNamesService.updateCustomColumnsNames(this.currentColumnsObj, "items_in_orders").subscribe(data => {
            if (data) {
                let title = this._columnsNamesService.formatSuccessData(data);
                let text = this.itemsInOrdersTableColumnsText.alertColumnsUpdatedSuccessfully;
                this._actionModalService.buildSuccessModal(title, text);
                this._columnsNamesService.buildCustomTableColumnsObject("items_in_orders", this.defaultTableColumnsObj);
                this.closeModal();
            }
        });
    }
    closeModal() {
        this.dialogRef.close();
    }
}

class IIOTCFLanguages {
    static en = {
        modalTitle: "Personalize table columns",
        alertColumnsUpdatedSuccessfully: "Columns updated successfully",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        modalTitle: "Tabellenspalten personalisieren",
        alertColumnsUpdatedSuccessfully: "Reie erfolgreich aktualiziert",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };
    static es = {
        modalTitle: "Personalizar Columnas",
        alertColumnsUpdatedSuccessfully: "Columnas actualizadas",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
