import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from "@angular/core";
import { ProvidersService } from "../../../shared/services/providers.service";
import { OrdersService } from "../../../shared/services/orders.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ProjectsService } from "../../../shared/services/projects.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { DateAdapter } from '@angular/material/core';

@Component({
    selector: "app-order-form",
    templateUrl: "./order-form.component.html",
    styleUrls: ["./order-form.component.scss"]
})

export class OrderFormComponent implements OnInit, OnChanges {
    @Input() currentOrder: any;
    @Input() editOrder: boolean;
    @Input() newOrder: boolean;
    @Output() useForm = new EventEmitter<boolean>();

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _providersService: ProvidersService,
        private _orderService: OrdersService,
        private _projectsService: ProjectsService,
        private adapter: DateAdapter<any>
    ) {}

    // Init languages setup
    sessionInfo: any;
    orderFormText: any;
    alerts: any;
    providers: any = {}; // as formatted Object to play nice with the select
    contacts: any = {}; // as formatted Object to play nice with the select
    statuses: any = this._orderService.getOrdersStatusesForSelect();
    openProjects: any = {}; // as formatted Object to play nice with the select
    vats = [];

    ngOnInit() {
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.vats = this.sessionInfo.company_vats;
            if (this.newOrder) this.callNextOrderNumber();
        });
        let cl = this._sharedService.user_selected_language;
        // change the date picker adapter to current language
        this.adapter.setLocale(cl);
        this.orderFormText = OFLanguage[cl];
        this.alerts = this.orderFormText.alerts;
        this._providersService.getFormattedProviders().subscribe(data => (this.providers = data));
        this._projectsService.getAvailableOrdersConfirmation().subscribe(data => (this.openProjects = data));
        if (this.editOrder) {
            let provider_id = this.currentOrder.provider_id;
            this._providersService.getFormattedContacts(provider_id).subscribe(data => (this.contacts = data));
        }
    }

    ngOnChanges(changes) {
        if (this.newOrder && this.sessionInfo) this.callNextOrderNumber();
    }

    forgetActions() {
        this.currentOrder = {};
        this.useForm.emit(false);
    }

    callProviderContacts(event) {
        let provider = event.value;
        let provider_id = this.providers["getProvider_id"](provider);
        this.currentOrder.provider_id = provider_id;
        this._providersService.getFormattedContacts(provider_id).subscribe(data => {
            this.contacts = data;
        });
    }

    setContactId(event) {
        let contact = event.value;
        this.currentOrder.pc_id = this.contacts["getPc_id"](contact);
    }

    saveOrder() {
        this.editOrder ? this.updateOrder() : this.createOrder();
    }

    createOrder() {
        this.currentOrder.projects_ids = this.openProjects["getDocuments_id"](this.currentOrder.projects_numbers);
        this._orderService.createOrder(this.currentOrder).subscribe(
            data => {
                if (data) {
                    this._orderService.passOrderToPushOrUpdate(data);
                    let title = this.currentOrder.order_number;
                    let text = this.alerts.alertOrderCreatedSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let message = this.alerts.alertOrderCreationError;
                this._actionModalsService.alertError(message);
                this.useForm.emit(false);
            }
        );
    }

    updateOrder() {
        console.log(this.currentOrder.order_delivery_date);
        this.currentOrder.projects_ids = this.openProjects["getDocuments_id"](this.currentOrder.projects_numbers);
        this._orderService.updateOrder(this.currentOrder).subscribe(
            data => {
                if (data) {
                    this._orderService.passOrderToPushOrUpdate(data);
                    let title = this.currentOrder.order_number;
                    let text = this.alerts.alertOrderUpdatedSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this._orderService.calculateOrderTotalPriceModel();
                    this.useForm.emit(false);
                }
            },
            () => {
                let message = this.alerts.alertOrderUpdateError;
                this._actionModalsService.alertError(message);
                this.useForm.emit(false);
            }
        );
    }

    callNextOrderNumber() {
        this._orderService.getCurrentOrderNumber().subscribe(data => {
            this.currentOrder.order_number = `${data.document_prefix}${data.document_current_number + 1}`;
        });
    }
}

class OFLanguage {
    static en = {
        alerts: {
            alertOrderUpdatedSuccess: "Order updated successfully",
            alertOrderCreatedSuccess: "Order created successfully",
            alertOrderUpdateError: "An error ocurred",
            alertOrderCreationError: "An error ocurred"
        },
        orderNumber: "Order Number",
        offerNumber: "Offer Number",
        offerNumberHint:"Offer number from your provider",
        projects: "Projects",
        provider: "Provider",
        providerContact: "Contact",
        orderStatus: "Order Status",
        deliveryDate: "Delivery Date",
        requiredField: "Required Field",
        vats: "VAT values",
        orderStatusPending: "PENDING",
        orderStatusOrdered: "ORDERED",
        orderStatusPaid: "PAID",
        orderStatusDelivered: "DELIVERED",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertOrderUpdatedSuccess: "Bestellung erfolgreich aktualiziert",
            alertOrderCreatedSuccess: "Bestellung erfolgreich erstelt",
            alertOrderUpdateError: "Es ist ein Fehler aufgetreten",
            alertOrderCreationError: "Es ist ein Fehler aufgetreten"
        },
        orderNumber: "Bestellungsnummer",
        offerNumber: "Angebotsnummer",
        offerNumberHint:"ggf. erhaltenes Angebot von Lieferant",
        projects: "Aufträge",
        provider: "Liferant",
        providerContact: "Ansprechpartner",
        orderStatus: "Bestellung Status",
        deliveryDate: "Lifertermin",
        requiredField: "Pflichtfeld",
        vats: "MwSt. Werten",
        orderStatusPending: "ANGEFRAGT",
        orderStatusOrdered: "BESTELLT",
        orderStatusPaid: "BETZAHLT",
        orderStatusDelivered: "GELIEFERT",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertOrderUpdatedSuccess: "Orden actualizada exitosamente",
            alertOrderCreatedSuccess: "Orden creada existosamente",
            alertOrderUpdateError: "Ha ocurrido un Error",
            alertOrderCreationError: "Ha ocurrido un Error"
        },
        orderNumber: "Número de Orden",
        offerNumber: "Número de Oferta",
        offerNumberHint:"Número de oferta de su proveedor",
        projects: "Proyectos",
        provider: "Proveedor",
        providerContact: "Contacto",
        orderStatus: "Estado de la Orden",
        deliveryDate: "Fecha de Entrega",
        requiredField: "Campo Requerido",
        vats: "IVA valores",
        orderStatusPending: "COTIZADO",
        orderStatusOrdered: "CONFIRMADO",
        orderStatusPaid: "PAGADO",
        orderStatusDelivered: "ENTREGADO",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
