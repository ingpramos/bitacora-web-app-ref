import { Component, OnInit, Inject } from '@angular/core';
import { FiltersService } from '../../../shared/services/filters.service';
import { SharedService } from '../../../shared/services/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
@Component({
	selector: 'app-filter-value-form',
	templateUrl: './filter-value-form.component.html',
	styleUrls: ['./filter-value-form.component.scss']
})
export class FilterValueFormComponent implements OnInit {

	currentFilterValue: any = this.modalData.currentFilterValue;
	currentFilterGroup: any = this.modalData.currentFilterGroup;
	editFilterValue: boolean = this.modalData.editFilterValue;
	filterValueFormText: any;
	alerts: any;
	constructor(
		private _filtersService: FiltersService,
		private _sharedService: SharedService,
		private _actionModalsService: ActionModalsService,
		public dialogRef: MatDialogRef<FilterValueFormComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) {
		let cl = this._sharedService.user_selected_language;
		this.filterValueFormText = FVFCLanguages[cl];
		this.alerts = this.filterValueFormText.alerts;
	}

	ngOnInit() {

	}



	saveFilterValue() {
		this.editFilterValue ? this.updateFilterValue() : this.createFilterValue();
	}
	createFilterValue() {
		this.currentFilterValue.filter_group_id = this.currentFilterGroup.filter_group_id;
		this._filtersService.createFilterValue(this.currentFilterValue).subscribe(data => {
			if (data) {
				this.closeModal();
				this._filtersService.getFiltersValues(this.currentFilterValue.filter_group_id);
				let title = data.filter_value;
				let text = this.alerts.alertFilterValueCreatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
			}
		}, () => {
			let text = this.alerts.alertFilterValueCreateError;
			this._actionModalsService.alertError(text);
		})
	}
	updateFilterValue() {
		this._filtersService.updateFilterValue(this.currentFilterValue).subscribe(data => {
			if (data) {
				this.closeModal();
				let title = data.filter_value;
				let text = this.alerts.alertFilterValueUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
			}
		}, () => {
			let text = this.alerts.alertFilterValueUpdateError;
			this._actionModalsService.alertError(text);
		});
	}

	closeModal() {
		this.dialogRef.close();
	}

}

class FVFCLanguages {
	static en = {
		"alerts": {
			"alertFilterValueUpdatedSuccessfully": "Filter updated successfully",
			"alertFilterValueUpdateError": "An error ocurred",
			"alertFilterValueCreatedSuccessfully": "Filter created successfully",
			"alertFilterValueCreateError": "An error ocurred"
		},
		"modalTitleCreate": "Create Filter Value",
		"modalTitleUpdate": "Update Filter Value",
		"filterValue": "Filter Name",
		"filterValueDescription": "Description",
		"errorRequired":"This field is required",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};

	static de = {
		"alerts": {
			"alertFilterValueUpdatedSuccessfully": "Filter erfolgreich aktualiziert",
			"alertFilterValueUpdateError": "Es ist ein Fehler aufgetreten",
			"alertFilterValueCreatedSuccessfully": "Filter erfolgreich erstellt",
			"alertFilterValueCreateError": "Es ist ein Fehler aufgetreten"
		},
		"modalTitleCreate": "Filter erstellen",
		"modalTitleUpdate": "Filter aktualizieren",
		"filterValue": "Filter Name",
		"filterValueDescription": "Beschreibung",
		"errorRequired":"Pflichtfeld",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};

	static es = {
		"alerts": {
			"alertFilterValueUpdatedSuccessfully": "Filtro actualizado exitosamente",
			"alertFilterValueUpdateError": "Ha ocurrido un error",
			"alertFilterValueCreatedSuccessfully": "Filtro creado exitosamente",
			"alertFilterValueCreateError": "Ha ocurrido un error"
		},
		"modalTitleCreate": "Crear Filtro",
		"modalTitleUpdate": "Actualizar filtro",
		"filterValue": "Nombre del Filtro",
		"filterValueDescription": "Descripción",
		"errorRequired":"Campo requerido.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
