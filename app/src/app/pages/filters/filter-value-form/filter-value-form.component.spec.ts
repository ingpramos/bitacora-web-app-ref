import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterValueFormComponent } from './filter-value-form.component';

describe('FilterValueFormComponent', () => {
  let component: FilterValueFormComponent;
  let fixture: ComponentFixture<FilterValueFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterValueFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterValueFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
