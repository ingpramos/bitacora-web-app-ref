import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { PipesModule } from '../../shared/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
// Angular Material
import {
	MatMenuModule, MatInputModule, MatFormFieldModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule
} from '@angular/material';

import { FiltersComponent } from './filters.component';
import { FiltersListComponent } from './filters-list/filters-list.component';
import { FilterGroupFormComponent } from './filter-group-form/filter-group-form.component';
import { FilterValuesTableComponent } from './filter-values-table/filter-values-table.component';
import { FilterGroupHeaderComponent } from './filter-group-header/filter-group-header.component';
import { FilterValueFormComponent } from './filter-value-form/filter-value-form.component';

const FILTERS_ROUTES: Routes = [
	{ path: '', component: FiltersComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(FILTERS_ROUTES),
		PipesModule,
		FormsModule,
		MatMenuModule,
		MatInputModule,
		MatFormFieldModule,
		MatDialogModule,
		MatIconModule
	],
	declarations: [
		FiltersListComponent,
		FiltersComponent,
		FilterGroupFormComponent,
		FilterValuesTableComponent,
		FilterGroupHeaderComponent,
		FilterValueFormComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		FilterValueFormComponent
	]
})
export class FiltersModule { }
