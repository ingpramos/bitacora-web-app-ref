import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterGroupFormComponent } from './filter-group-form.component';

describe('FilterGroupFormComponent', () => {
  let component: FilterGroupFormComponent;
  let fixture: ComponentFixture<FilterGroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterGroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
