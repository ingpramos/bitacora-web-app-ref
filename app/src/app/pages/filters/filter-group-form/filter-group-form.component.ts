import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { FiltersService } from '../../../shared/services/filters.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
	selector: 'app-filter-group-form',
	templateUrl: './filter-group-form.component.html',
	styleUrls: ['./filter-group-form.component.scss']
})

export class FilterGroupFormComponent implements OnInit {
	// component's Inputs
	@Input() editFilterGroup;
	@Input() newFilterGroup;
	@Input() currentFilterGroup: any;
	// component's output
	@Output() useForm = new EventEmitter<boolean>();
	filterGroupFormText: any;
	alerts: any;
	constructor(
		private _sharedService: SharedService,
		private _filtersService: FiltersService,
		private _actionModalsService : ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.filterGroupFormText = FGFCLanguage[cl];
		this.alerts = this.filterGroupFormText.alerts;
	}

	ngOnInit() {

	}

	saveFilterGroup() {
		this.editFilterGroup ? this.updateFilterGroup() : this.createFilterGroup();
	}

	createFilterGroup() {
		this._filtersService.createFilterGroup(this.currentFilterGroup).subscribe(data => {
			if (data) {
				this._filtersService.passNewFilterGroup(data);
				this.useForm.emit(false);
				let title = data.filter_group_name;
				let text = this.alerts.alertFGCreatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title,text);
			}
		},()=>{
			let text = this.alerts.alertFGCreateError;
			this._actionModalsService.alertError(text);
		})
	}

	updateFilterGroup() {
		this._filtersService.updateFilterGroup(this.currentFilterGroup).subscribe(data => {
			if(data){
				this.useForm.emit(false);
				let title = data.filter_group_name;
				let text = this.alerts.alertFGUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title,text);
			}			
		},()=>{
			let text = this.alerts.alertFGUpdateError;
			this._actionModalsService.alertError(text);
		});
	}

	forgetActions() {
		this.useForm.emit();
	}

}

class FGFCLanguage {
	static en = {
		"alerts": {
			"alertFGUpdatedSuccessfully": "Filter group updated successfully",
			"alertFGUpdateError": "An error ocurred",
			"alertFGCreatedSuccessfully": "Filter group created successfully",
			"alertFGCreateError": "An error ocurred"
		},
		"filterGroupName": "Filter Group Name",
		"filterGroupDescription": "Description",
		"errorRequired":"This field is required",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};
	static de = {
		"alerts": {
			"alertFGUpdatedSuccessfully": "Filtergruppe erfolgreich aktualiziert",
			"alertFGUpdateError": "Es ist ein Fehler aufgetreten",
			"alertFGCreatedSuccessfully": "Filtergruppe erfolgreich erstelt",
			"alertFGCreateError": "Es ist ein Fehler aufgetreten"
		},
		"filterGroupName": "Neue Filter Gruppe Name",
		"filterGroupDescription": "Beschreibung",
		"errorRequired":"Pflichtfeld",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};
	static es = {
		"alerts": {
			"alertFGUpdatedSuccessfully": "Grupo de filtros actualizado exitosamente",
			"alertFGUpdateError": "Ha ocurrido un error",
			"alertFGCreatedSuccessfully": "",
			"alertFGCreateError": "Ha ocurrido un error"
		},
		"filterGroupName": "Grupo de Filtros",
		"filterGroupDescription": "Descripción",
		"errorRequired":"Campo requerido.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};

}
