import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { FilterValueFormComponent } from '../filter-value-form/filter-value-form.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'app-filter-group-header',
	templateUrl: './filter-group-header.component.html',
	styleUrls: ['./filter-group-header.component.scss']
})
export class FilterGroupHeaderComponent implements OnInit {

	@Input() currentFilterGroup :any;
	filterGroupHeaderText :any;

	constructor(
		private _sharedService: SharedService,
		public dialog: MatDialog
	) {
		let cl = this._sharedService.user_selected_language;
		this.filterGroupHeaderText = FGHCLanguage[cl];
	}

	ngOnInit() {
	}

	createFilterValue(){
		let initialState = { currentFilterValue: {}, currentFilterGroup: this.currentFilterGroup};
		const dialogRef = this.dialog.open(FilterValueFormComponent, { width: '500px', data: initialState});
	}


}

class FGHCLanguage {
	static en = {
		"filterGroupName": "Filter Group",
		"addNewFilterValue":"Add New Filter Value"
	};
	static de = {
		"filterGroupName": "Filter Gruppe",
		"addNewFilterValue":"Neues Filter erstellen"
	};
	static es = {
		"filterGroupName": "Grupo de Filtro",
		"addNewFilterValue":"Añadir Nuevo Valor"
	};
}
