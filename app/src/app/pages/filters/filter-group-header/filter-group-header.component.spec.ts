import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterGroupHeaderComponent } from './filter-group-header.component';

describe('FilterGroupHeaderComponent', () => {
  let component: FilterGroupHeaderComponent;
  let fixture: ComponentFixture<FilterGroupHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterGroupHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterGroupHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
