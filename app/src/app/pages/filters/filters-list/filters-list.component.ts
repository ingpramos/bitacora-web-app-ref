import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { FiltersService } from "../../../shared/services/filters.service";

@Component({
    selector: "app-filters-list",
    templateUrl: "./filters-list.component.html",
    styleUrls: ["./filters-list.component.scss"]
})
export class FiltersListComponent implements OnInit {
    @Input() filterGroups;

    sessionInfo: any = {};
    filterGroupsListText: any;
    alerts: any;
    editFilterGroup: boolean = false;
    newFilterGroup: boolean = false;
    filterGroupToHandle: any = {};
    private paramMapSubscription: any;
    filter_group_id: any;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private _sharedServices: SharedService,
        private _actionModalsService: ActionModalsService,
        private _filtersService: FiltersService
    ) {
        let cl = this._sharedServices.user_selected_language;
        this.filterGroupsListText = FLCLanguages[cl];
        this.alerts = this.filterGroupsListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMapSubscription = this.activatedRoute.paramMap.subscribe(paramMap => (this.filter_group_id = +paramMap.get("filter_group_id")));
        this._sharedServices.sessionInfo.subscribe(data => (this.sessionInfo = data));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForFilterGroups();
        });
    }

    searchOrdersConfirmed() {
        this.subject.next(this.searchPattern);
    }

    searchForFilterGroups() {
        this._filtersService.getFilterGroupsByPattern(this.searchPattern);
    }

    startSearch() {
        this.editFilterGroup = false;
        this.newFilterGroup = false;
        this.showSearch = true;
    }

    closeSearch() {
        this.searchPattern = "";
        this._filtersService.getFiltersGroups();
        this.showSearch = false;
    }

    startEditFilterGroup(offer) {
        this.newFilterGroup = false;
        this.filterGroupToHandle = offer;
        this.editFilterGroup = true;
    }

    startNewFilterGroup() {
        this.editFilterGroup = false;
        this.filterGroupToHandle = {};
        this.newFilterGroup = true;
    }

    deleteFilterGroup(fg, index) {
        let title = fg.filter_group_name;
        let text = this.alerts.confirmDeleteFG;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let fg_id = fg.filter_group_id;
                this._filtersService.deleteFilterGroup(fg_id).subscribe(
                    data => {
                        if (data) {
                            this.router.navigate(["app/company/filters", 0]);
                            let text = this.alerts.alertFGDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.filterGroups.splice(index, 1);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let text = this.alerts.alertFGDeleteError;
                            this._actionModalsService.alertError(text);
                        }
                    }
                );
            }
        });
    }

    forgetActions(value) {
        this.editFilterGroup = false;
        this.newFilterGroup = false;
    }
}

class FLCLanguages {
    static en = {
        alerts: {
            confirmDeleteFG: "Are you sure you want to delete this filter group ?",
            alertFGDeletedSuccessfully: "Filter group deleted successfully",
            alertForeignKeyViolation: "This filter group is still referenced in one of your items",
            alertFGDeleteError: "An error ocurred"
        },
        title: "Filters",
        newFilterGroup: "New Filter Group",
        searchFilterGroup: "Search Filter Group",
        editFilterGroup: "Edit Filter Group",
        deleteFilterGroup: "Delete Filter Group"
    };
    static de = {
        alerts: {
            confirmDeleteFG: "Wollen Sie dieses Filterngruppe löschen ?",
            alertFGDeletedSuccessfully: "Filterngruppe erfolgreich gelöscht",
            alertForeignKeyViolation: "Dieser Filterngruppe ist immer noch referenziert in einer Ihrer Artikel",
            alertFGDeleteError: "Es ist ein Fehler aufgetreten"
        },
        title: "Filtern",
        newFilterGroup: "Neue Filterngruppe erstellen",
        searchFilterGroup: "Filtergruppe suchen",
        editFilterGroup: "Filtern Bearbeiten",
        deleteFilterGroup: "Filtern Löschen"
    };
    static es = {
        alerts: {
            confirmDeleteFG: "Estas seguro que deseas borrar este grupo de filtros ?",
            alertFGDeletedSuccessfully: "Grupo de filtros eliminidado exitosamente",
            alertForeignKeyViolation: "Este grupo de filtros está referenciado en uno de sus articulos",
            alertFGDeleteError: "Ha ocurrido un error"
        },
        title: "Filtros",
        newFilterGroup: "Nuevo grupo de filtros",
        searchFilterGroup: "Buscar grupo de filtros",
        editFilterGroup: "Editar Grupo de Filtros",
        deleteFilterGroup: "Eliminar Grupo de Filtros"
    };
}
