import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { FiltersService } from '../../../shared/services/filters.service';
import { SharedService } from '../../../shared/services/shared.service';
import { FilterValueFormComponent } from '../filter-value-form/filter-value-form.component';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'app-filter-values-table',
	templateUrl: './filter-values-table.component.html',
	styleUrls: ['./filter-values-table.component.scss']
})

export class FilterValuesTableComponent implements OnInit, OnChanges, OnDestroy {

	@Input() currentFilterGroup;
	filterValuesTableText: any;
	alerts;
	filterValues: any = [];
	tableColumns: any = {};
	// subscription
	filterValuesList$;
	constructor(
		private _sharedService: SharedService,
		private _filtersService: FiltersService,
		private _actionModalsService : ActionModalsService,
		public dialog: MatDialog
	) {
		let cl = this._sharedService.user_selected_language;
		this.filterValuesTableText = FVTLanguage[cl];
		this.alerts = this.filterValuesTableText.alerts;
		this.tableColumns = this.filterValuesTableText.header;
		this.filterValuesList$ = this._filtersService.currentFilterValuesList.subscribe(data => this.filterValues = data);
	}

	ngOnInit() {
	}
	
	ngOnDestroy(){
		this.filterValuesList$.unsubscribe();
	}

	ngOnChanges(changes) {
		let fg_id = changes.currentFilterGroup.currentValue.filter_group_id;
		if (fg_id) { this._filtersService.getFiltersValues(fg_id); }
		if (!fg_id) { this._filtersService.updateFilterValuesList([]); }
	}

	editFilterValue(filterValue) {
		let initialState = { currentFilterValue: filterValue, currentFilterGroup: this.currentFilterGroup, editFilterValue: true };
		const dialogRef = this.dialog.open(FilterValueFormComponent, { width: '500px', data: initialState});
	}

	deleteFilterValue(fv,index) {
		let title = fv.filter_value;
		let text = this.alerts.confirmDeleteFV;
		this._actionModalsService.buildConfirmModal(title, text)
			.then(result => {
				if (result.value) {
					let filter_id = fv.filter_id;
					this._filtersService.deleteFilterValue(filter_id).subscribe(data => {
						if (data) {
							let title = fv.filter_value;
							let text = this.alerts.alertFilterDeletedSuccessfully;
							this._actionModalsService.buildSuccessModal(title, text)
							this.filterValues.splice(index, 1);
						}
					}, (e) => {
						if (e.error.error.code == '23503') { // handle foreign key constrain
							const text = this.alerts.alertForeignKeyViolation;
							this._actionModalsService.buildInfoModal(title, text);
						} else {
						let text = this.alerts.alertFilterDeleteError;
						this._actionModalsService.alertError(text);
						}
					});
				}
			});
	}

}

class FVTLanguage {

	static en = {
		"title": "Filters",
		"header": {
			"position": "Position",
			"filter_value": "Name"
		},
		"alerts": {
			"confirmDeleteFV": "Are you sure you want to delete this filter ?",
			"alertFilterDeletedSuccessfully": "Filter Deleted Successfully",
			"alertForeignKeyViolation": "This filter is still referenced in one of your items",
			"alertFilterDeleteError": "An error ocurred"
		},
		"options": "Options",
		"filterPlaceholder": "Search Items"
	};

	static de = {
		"title": "Filtern",
		"header": {
			"position": "Pos",
			"filter_value": "Filter"
		},
		"alerts": {
			"confirmDeleteFV": "Sind Sie sicher das Sie dieses Filter löschen möchte ?",
			"alertFilterDeletedSuccessfully": "Filter erfolgreich gelöscht",
			"alertForeignKeyViolation": "Dieser Filter ist immer noch referenziert in einer Ihrer Artikel",
			"alertFilterDeleteError": "Es ist ein Fehler aufgetreten"
		},
		"options": "Optionen",
		"filterPlaceholder": "Tabelle Suchen"
	};

	static es = {
		"title": "Filtros",
		"header": {
			"position": "Posición",
			"filter_value": "Filtro"
		},
		"alerts": {
			"confirmDeleteFV": "Esta seguro que desea eliminar el filtro actual ?",
			"alertFilterDeletedSuccessfully": "Filtro Eliminado Exitosamente",
			"alertForeignKeyViolation": "Este filtro está referenciado en uno de sus articulos",
			"alertFilterDeleteError": "Ha ocurrido un error"
		},
		"options": "Opciones",
		"filterPlaceholder": "Buscar Items"
	};
}
