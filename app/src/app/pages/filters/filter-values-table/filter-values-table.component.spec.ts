import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterValuesTableComponent } from './filter-values-table.component';

describe('FilterValuesTableComponent', () => {
  let component: FilterValuesTableComponent;
  let fixture: ComponentFixture<FilterValuesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterValuesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterValuesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
