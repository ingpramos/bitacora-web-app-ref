import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { FiltersService } from '../../shared/services/filters.service';

@Component({
	selector: 'app-filters',
	templateUrl: './filters.component.html',
	styleUrls: ['./filters.component.scss']
})

export class FiltersComponent implements OnInit, OnDestroy {

	filterGroups: any = [];
	selectedFilterGroup = {};
	filter_group_id;
	newFGSubscription;
	filterGroupsSubscription;
	constructor(
		private _filtersService: FiltersService,
		private activeRoute: ActivatedRoute,
		private router: Router,
	) {
		this.newFGSubscription = this._filtersService.newFilterGroupToPush.subscribe(data => {
			let filter_group_id = data.filter_group_id;
			if (filter_group_id) {
				this.filterGroups.push(data);
				this.router.navigate(['app/company/filters', filter_group_id]);
			}
		});
		this.activeRoute.params.pipe(map(params => params['filter_group_id']))
			.subscribe(filter_group_id => {
				this.filter_group_id = filter_group_id;
				this.setSelectedFilterGroup();
			});
		this.filterGroupsSubscription = this._filtersService.currentFilterGroupsList.subscribe(data => {
			this.filterGroups = data;
			this.setSelectedFilterGroup();
		});
	}

	ngOnInit() {
		this._filtersService.getFiltersGroups();
	}

	setSelectedFilterGroup() {
		if (this.filterGroups.length > 0) {
			let found = this.filterGroups.find((obj) => obj['filter_group_id'] == this.filter_group_id);
			this.selectedFilterGroup = found ? found : this.filterGroups[0];
			this.router.navigate(['app/company/filters', this.selectedFilterGroup['filter_group_id']]);
			window.scroll(0,0);
		}
		else {
			this.selectedFilterGroup = {};
		}
		this._filtersService.setCurrentFilterGroup(this.selectedFilterGroup); // to make it avaiable from the service
	}

	ngOnDestroy() {
		this.newFGSubscription.unsubscribe();
	}
}
