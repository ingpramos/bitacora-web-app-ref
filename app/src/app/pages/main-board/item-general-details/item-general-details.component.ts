import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsService } from "../../../shared/services/items.service";
import { Router } from "@angular/router";

@Component({
    selector: "app-item-general-details",
    templateUrl: "./item-general-details.component.html",
    styleUrls: ["./item-general-details.component.scss"]
})
export class ItemGeneralDetailsComponent implements OnInit {
    currentLanguage:string;
    currentItem = this.modalData.currentItem;
    projectsRegisters: any[] = []; // projects rows -- registers
    ordersRegisters: any[] = []; // orders rows -- registers
    oCRegisters: any[] = []; // order confirmations rows -- registers
    modalText;
    itemHeaderText;
    projectsTableColumns;
    oCTableColumns;
    ordersTableColumns;
    constructor(
        private router: Router,
        private _sharedService: SharedService,
        private _itemsService: ItemsService,
        public dialogRef: MatDialogRef<ItemGeneralDetailsComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.modalText = IGDCLanguages[this.currentLanguage];
        this.itemHeaderText = this.modalText.itemHeaderText;
        this.projectsTableColumns = this.modalText.projectsTableColumns;
        this.oCTableColumns = this.modalText.oCTableColumns;
        this.ordersTableColumns = this.modalText.ordersTableColumns;
    }

    ngOnInit() {
        const item_id = this.currentItem.item_id;
        this._itemsService.getItemGeneralCurrentStatus(item_id).subscribe(data => {
            this.projectsRegisters = data.item_in_projects || [];
            this.oCRegisters = data.item_in_internal_documents || [];
            this.ordersRegisters = data.item_in_orders || [];
        });
    }

    goToOrder(order) {
        this.router.navigate([order.order_url, order.order_id]);
        this.dialogRef.close();
    }
}

class IGDCLanguages {
    static en = {
        modalTitle: "Item general details",
        itemHeaderText: {
            itemName: "Item Name",
            itemCode: "Item Code",
            amountStock: "In Stock",
            amountRequired: "Booked",
            amountInUse: "In use",
            amountNeto: "Neto"
        },
        projectTableTitle: "Item in projects",
        projectsTableColumns: {
            position: "Pos",
            assembly_name: "Group Name",
            assembly_number: "Group Number",
            required_amount: "Required Amount",
            activity_done_name: "Status",
            project_name: "Project Name",
            project_number: "Project Number",
            client_name: "Client",
            deadline: "Deadline"
        },
        oCTableTitle: "Item in Confirmed Orders",
        oCTableColumns: {
            position: "Pos",
			item_amount: "Amount",
			activity_done_name: "Status",
            order_confirmation_number: "OC Number",
            client_name: "Client",
            offered_delivery_date: "Deadline"
        },
        ordersTableTitle: "Item in purchase orders",
        ordersTableColumns: {
            position: "Pos",
            order_number: "Order number",
            provider_name: "Provider",
            item_ordered_amount: "Ordered Amount",
            received_amount: "Received Amount",
            order_delivery_date: "Delivery Date"
        }
    };

    static de = {
        modalTitle: "Artikeldetails",
        itemHeaderText: {
            itemName: "Artikelname",
            itemCode: "Code",
            amountStock: "Vorrat",
            amountRequired: "Gebucht",
            amountInUse: "im Einsatz",
            amountNeto: "Netto"
        },
        projectTableTitle: "Artikel im Projekten",
        projectsTableColumns: {
            position: "Pos",
            assembly_name: "Baugruppename",
            assembly_number: "Baugruppenummer",
            required_amount: "Menge",
            activity_done_name: "Status",
            project_name: "Auftragsname",
            project_number: "Auftragsnummer",
            client_name: "Kunde",
            deadline: "Liefertermin"
        },
        oCTableTitle: "Artikel in Auftragsbestätigungen",
        oCTableColumns: {
            position: "Pos",
			item_amount: "Menge",
			activity_done_name: "Status",
            order_confirmation_number: "Auftragsnummer",
            client_name: "Kunde",
            offered_delivery_date: "Liefertermin"
        },
        ordersTableTitle: "Artikel im Bestellungen",
        ordersTableColumns: {
            position: "Pos",
            order_number: "Bestellungsnummer",
            provider_name: "Lieferant",
            item_ordered_amount: "Menge",
            received_amount: "Erhalten",
            order_delivery_date: "Liefertermin"
        }
    };

    static es = {
        modalTitle: "Detalles de Articulo",
        itemHeaderText: {
            itemName: "Nombre del item",
            itemCode: "Código",
            amountStock: "Stock",
            amountRequired: "Requeridos",
            amountInUse: "En uso",
            amountNeto: "Neto"
        },
        projectTableTitle: "Item en proyectos",
        projectsTableColumns: {
            position: "Pos",
            assembly_name: "Nombre de Grupo",
            assembly_number: "Nr. de Grupo",
            required_amount: "Cantidad",
            activity_done_name: "Estatus",
            project_name: "Nombre de proyecto",
            project_number: "Nr. de proyecto",
            client_name: "Cliente",
            deadline: "Fecha de entrega"
        },
        oCTableTitle: "Item en ordenes confirmadas",
        oCTableColumns: {
            position: "Pos",
			item_amount: "Cantidad",
			activity_done_name: "Estatus",
            order_confirmation_number: "Nr OT",
            client_name: "Cliente",
            offered_delivery_date: "Fecha de Entrega"
        },
        ordersTableTitle: "Item en ordenes de compra",
        ordersTableColumns: {
            position: "Pos",
            order_number: "Orden de Compra",
            provider_name: "Proveedor",
            item_ordered_amount: "Cantidad",
            received_amount: "Recibido",
            order_delivery_date: "Fecha de Entrega"
        }
    };
}
