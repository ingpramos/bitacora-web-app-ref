import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemGeneralDetailsComponent } from './item-general-details.component';

describe('ItemGeneralDetailsComponent', () => {
  let component: ItemGeneralDetailsComponent;
  let fixture: ComponentFixture<ItemGeneralDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemGeneralDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemGeneralDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
