import { Component, OnInit, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ItemFormComponent } from "../item-form/item-form.component";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ColumnsFormComponent } from "../columns-form/columns-form.component";
import { FiltersTableComponent } from "../filters-table/filters-table.component";
import { ItemGeneralDetailsComponent } from "../item-general-details/item-general-details.component";
import { ItemProcessDetailsModalComponent } from "../item-process-details-modal/item-process-details-modal.component";

@Component({
    selector: "app-main-board-table",
    templateUrl: "./main-board-table.component.html",
    styleUrls: ["./main-board-table.component.scss"]
})
export class MainBoardTableComponent implements OnInit, OnDestroy {
    tableColumns: any = {};
    items = [];
    defaultTableColumnsObj: any;
    currentLanguage:string;
    alerts: any;
    itemsInStockText: any;
    itemsMenuText: any;
    // subscriptions
    itemsList$;
    columnsNames$;
    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        private _itemsService: ItemsService,
        public dialog: MatDialog
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.defaultTableColumnsObj = MBTLanguages[this.currentLanguage].columns; // this object have the language defaults values
        this.alerts = MBTLanguages[this.currentLanguage].alerts;
        this.itemsInStockText = MBTLanguages[this.currentLanguage];
        this.itemsMenuText = MBTLanguages[this.currentLanguage].itemsMenu;
        this.itemsList$ = this._itemsService.itemsCurrentList.subscribe(data => (this.items = data));
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => (this.tableColumns = data));
        this._columnsNamesService.buildCustomTableColumnsObject("items", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        this._sharedService.startLoading();
        this._itemsService.getItems().subscribe(data => {
            this.items = data;
            this._sharedService.stopLoading();
        });
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
    }


    editItem(item) {
        let initialState = { currentItem: item, editItem: true };
        const dialogRef = this.dialog.open(ItemFormComponent, { width: "1000px", data: initialState });
    }

    searchForLabels(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(FiltersTableComponent, { width: "1000px", data: initialState });
    }

    deleteItem(item, index) {
        let title = item.item_code;
        let text = this.alerts.confirmDeleteItemFromStockTitle;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let item_id = item.item_id;
                this._itemsService.deleteItem(item_id).subscribe(
                    data => {
                        let title = item.item_code;
                        let text = this.alerts.alertItemDeletedSuccessfullyFromStock;
                        this._actionModalsService.buildSuccessModal(title, text);
                        this.items.splice(index, 1);
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertItemDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }

    editTableColumns() {
        let initialState = { defaultTableColumnsObj: this.defaultTableColumnsObj, currentTableColumns: this.tableColumns };
        const dialogRef = this.dialog.open(ColumnsFormComponent, { width: "1000px", data: initialState });
    }
    openItemDetails(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemGeneralDetailsComponent, { width: "1400px", data: initialState });
    }

    showItemDetailedCosts(item) {
        let initialInformation = {};
        initialInformation["currentItem"] = item;
        const dialogRef = this.dialog.open(ItemProcessDetailsModalComponent, { width: "1200px", data: initialInformation });
    }
}

class MBTLanguages {
    static en = {
        columns: {
            item_code: "Item Code",
            item_name: "Name",
            item_amount: "Stock",
            total_in_use: "In Use",
            total_booked: "Pending",
            total_in_progress: "In Process",
            item_neto: "Neto",
            total_expected: "Ordered"
        },
        alerts: {
            confirmDeleteItemFromStockTitle: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfullyFromStock: "Item deleted successfully",
            alertForeignKeyViolation: "This item is still referenced in one of your documents",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Opt",
        itemsMenu: {
            editItem: "Edit Item",
            itemDetails: "See Details",
            itemActivitiesCosts: "See Costs details",
            addTagsToItem: "Add Filter Tag",
            deleteItem: "Delete Item"
        },
        title: "Stock Overview",
        editTableColumns: "Edit Table Columns"
    };

    static de = {
        columns: {
            item_code: "Artikel Code",
            item_name: "Name",
            item_amount: "Vorrat",
            total_in_use: "Eingebaut",
            total_booked: "Gebucht",
            total_in_progress: "in Arbeit",
            item_neto: "Netto",
            total_expected: "Bestellt"
        },
        alerts: {
            confirmDeleteItemFromStockTitle: "Sind Sie sicher das sie dieses Artikel löschen möchte ?",
            alertItemDeletedSuccessfullyFromStock: "Artikel erfolgreich gelöscht",
            alertForeignKeyViolation: "Dieser Artikel ist immer noch referenziert in einer Ihrer Dokumenten",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Opt",
        itemsMenu: {
            editItem: "Artikel bearbeiten",
            itemDetails: "Artikel Details",
            itemActivitiesCosts: "Kosten Details",
            addTagsToItem: "Label einfügen",
            deleteItem: "Artikel löschen"
        },
        title: "Lager überblick",
        editTableColumns: "Spaltenamen ändern"
    };

    static es = {
        columns: {
            item_code: "Código Del Item",
            item_name: "Nombre",
            item_amount: "Existencias",
            total_in_use: "Montado",
            total_booked: "Reservado",
            total_in_progress: "En proceso",
            item_neto: "Neto",
            total_expected: "Pedidos"
        },
        alerts: {
            confirmDeleteItemFromStockTitle: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfullyFromStock: "Item eliminidado exitosamente",
            alertForeignKeyViolation: "Este item está referenciado en uno de sus documentos",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        itemsMenu: {
            editItem: "Editar Articulo",
            itemDetails: "Detalles del Articulo",
            itemActivitiesCosts: "Detalles de costos",
            addTagsToItem: "Etiquetas",
            deleteItem: "Eliminar Articulo"
        },
        title: "Inventario",
        editTableColumns: "Editar Nombre de las Columnas"
    };
}
