import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainBoardTableComponent } from './main-board-table.component';

describe('MainBoardTableComponent', () => {
  let component: MainBoardTableComponent;
  let fixture: ComponentFixture<MainBoardTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainBoardTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainBoardTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
