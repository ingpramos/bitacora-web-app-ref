import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { FiltersService } from "../../../shared/services/filters.service";
import { LocationsService } from "../../../shared/services/locations.service";
import { FormatDatePipe } from "../../../shared/pipes/format-date/format-date.pipe";

@Component({
    selector: "app-item-form",
    templateUrl: "./item-form.component.html",
    styleUrls: ["./item-form.component.scss"]
})
export class ItemFormComponent implements OnInit {

    newItem: any = this.modalData.newItem;
    editItem: any = this.modalData.editItem;
    currentItem: any = this.modalData.currentItem;
    currentItemCopy = JSON.parse(JSON.stringify(this.currentItem));
    itemFormText: any;
    alerts;
    itemTypes: any[];
    materials = { material_names: [] };
    locations: any = { location_names: [] };
    filterGroups: any = {};

    constructor(
        private _formatDatePipe: FormatDatePipe,
        private _actionModalsService: ActionModalsService,
        private _itemsServices: ItemsService,
        private _sharedService: SharedService,
        private _filtersService: FiltersService,
        private _locationsService: LocationsService,
        public dialogRef: MatDialogRef<ItemFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.itemFormText = IFLanguages[cl];
        this.alerts = this.itemFormText.alerts;
        this.itemTypes = this.itemFormText.itemTypes;
        this.materials = this._filtersService.getMaterials();
        this.locations = this._locationsService.getFormatedLocations();
    }

    ngOnInit() {
        // preformat the values to show correct format when opening this component
        let ibp = Number(this.currentItem["item_buy_price"]).toFixed(2);
        let isp = Number(this.currentItem["item_sell_price"]).toFixed(2);
        this.currentItem["item_buy_price_to_show"] = this._sharedService.parseToLocaleString(ibp).trim();
        this.currentItem["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(isp).trim();
        this.currentItem["last_update_to_show"] = this._formatDatePipe.transform(this.currentItem["last_update"]);
    }

    setMaterialId(event) {
        const material_name = event.value;
        const material_id = this.materials["get_material_id"](material_name);
        if (material_id != this.currentItemCopy.material_id) {
            this.currentItem["material_id"] = material_id;
            this.currentItem["new_material_id"] = material_id;
            this.currentItem["old_material_id"] = this.currentItemCopy.material_id || 0;
        }
    }

    setLocationId(event) {
        const location_name = event.value;
        const location_id = this.locations["get_location_id"](location_name);
        if (location_id != this.currentItemCopy.location_id) {
            this.currentItem["location_id"] = location_id;
            this.currentItem["new_location_id"] = location_id;
            this.currentItem["old_location_id"] = this.currentItemCopy.location_id || 0;
        }
    }

    closeModal() {
        this.dialogRef.close();
    }

    saveItem(item) {
        this.editItem ? this.updateItem() : this.createItem();
    }

    createItem() {
        this._itemsServices.createItem(this.currentItem).subscribe(
            data => {
                if (data) {
                    this.closeModal();
                    let title = this.currentItem["item_code"];
                    let text = this.alerts.alertItemCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this._itemsServices.getItemsByNameOrCode(title).subscribe(data => this._itemsServices.updateCurrentList(data));
                }
            },
            error => {
                if (error.status === 403) {
                    this.closeModal();
                    let title = this.currentItem["item_code"];
                    let text = this.alerts.alertItemAlreadyExistsTitle;
                    this._actionModalsService.buildSuccessModal(title, text);
                } else {
                    let text = this.alerts.alertItemCreateError;
                    this._actionModalsService.alertError(text);
                }
            }
        );
    }

    updateItem() {
        this.currentItem["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(this.currentItem["item_buy_price_to_show"]);
        this.currentItem["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(this.currentItem["item_sell_price_to_show"]);
        this._itemsServices.updateItem(this.currentItem).subscribe(
            data => {
                this.closeModal();
                let title = this.currentItem["item_code"];
                let text = this.alerts.alertItemUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertItemUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }
}

class IFLanguages {
    static en = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item updated successfully",
            alertItemUpdateError: "An error ocurred",
            alertItemCreatedSuccessfully: "Item created successfully",
            alertItemCreateError: "An error ocurred",
            alertItemAlreadyExistsTitle: "Item already exists"
        },
        itemTypes: ["Manufacturing part", "Purchased part", "Service", "Project"],
        modalTitleCreate: "Create new Item",
        modalTitleUpdate: "Update Item",
        itemCode: "Item Code",
        itemName: "Item Name",
        itemType: "Item Type",
        itemAmount: "Stock",
        itemOrderNumber: "Order Number",
        itemBarcode: "Barcode",
        itemBrand: "Item Brand",
        itemProvider: "Provider",
        itemBuyPrice: "Buy price",
        itemSellPrice: "Sell price",
        itemAssemblyName: "Assembly",
        itemDetails: "Details",
        itemLocation: "Location",
        itemMaterial: "Material",
        itemRawMaterialProfil: "Material Profil",
        itemRawMaterialDimensions: "Dimensions",
        itemLastUpdate: "Last Time",
        itemLastPerson: "Last Person",
        lastPersonAmount: "Last Amount",
        chipsPlaceholder: "Add new Filter",
        requiredField: "Required Field",
        filterCategorie: "Filter Categorie",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertItemUpdatedSuccessfully: "Artikel erfolgreich aktualiziert",
            alertItemUpdateError: "Es ist ein Fehler aufgetreten",
            alertItemCreatedSuccessfully: "Artikel erfolgreich erstellt",
            alertItemCreateError: "Es ist ein Fehler aufgetreten",
            alertItemAlreadyExistsTitle: "Artikel ist bereits vorhanden"
        },
        itemTypes: ["Fertigungsteil", "Kaufteil", "Dienstleistung", "Projekt/Baugruppe"],
        modalTitleCreate: "Neues Artikel erstellen",
        modalTitleUpdate: "Artikel aktualizieren",
        itemCode: "Artikel Code",
        itemName: "Name",
        itemType: "Artikel Typ",
        itemAmount: "Vorrat",
        itemOrderNumber: "Bestellungsnummer",
        itemBarcode: "Barcode",
        itemBrand: "Marke",
        itemProvider: "Liferant",
        itemBuyPrice: "Kaufspreis",
        itemSellPrice: "Verkaufspreis",
        itemAssemblyName: "Baugruppe",
        itemDetails: "Details",
        itemLocation: "Lage",
        itemMaterial: "Material",
        itemRawMaterialProfil: "Profil",
        itemRawMaterialDimensions: "Massen",
        itemLastUpdate: "Letztes Mal",
        itemLastPerson: "Letztes Person",
        lastPersonAmount: "Menge",
        chipsPlaceholder: "Neuen Filter hinzufügen",
        requiredField: "Pflichtfeld",
        filterCategorie: "Gruppe",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item actualizado exitosamente",
            alertItemUpdateError: "Ha ocurrido un error",
            alertItemCreatedSuccessfully: "Item creado exitosamente",
            alertItemCreateError: "Ha ocurrido un error",
            alertItemAlreadyExistsTitle: "El item ya se encuentra registrado"
        },
        itemTypes: ["Manufacturada", "Comprada", "Servicio", "Proyecto"],
        modalTitleCreate: "Crear Nuevo Articulo",
        modalTitleUpdate: "Actualizar Articulo",
        itemCode: "Código Del Item",
        itemName: "Nombre",
        itemType: "Tipo",
        itemBrand: "Marca",
        itemAmount: "Existencias",
        itemOrderNumber: "Código de Compra",
        itemBarcode: "Código de Barras",
        itemProvider: "Proveedor",
        itemBuyPrice: "Precio de Compra",
        itemSellPrice: "Precio de Venta",
        itemAssemblyName: "Ensamble",
        itemDetails: "Detalles",
        itemLocation: "Ubicación",
        itemMaterial: "Material",
        itemRawMaterialProfil: "Perfil",
        itemRawMaterialDimensions: "Dimensiones",
        itemLastUpdate: "Ultima vez",
        itemLastPerson: "Ultima Persona",
        lastPersonAmount: "Ultima Cantidad",
        chipsPlaceholder: "Añadir nuevo filtro",
        requiredField: "Campo Requerido",
        filterCategorie: "Categoria",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
