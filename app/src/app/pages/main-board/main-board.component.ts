import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../../shared/services/locations.service';

@Component({
    selector: 'app-main-board',
    templateUrl: './main-board.component.html',
    styleUrls: ['./main-board.component.scss']
})
export class MainBoardComponent implements OnInit {

    constructor(private _locationsServices: LocationsService) {
        this._locationsServices.callLocations();
    }

    ngOnInit() {

    }

}
