import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainBoardHeaderComponent } from './main-board-header.component';

describe('MainBoardHeaderComponent', () => {
  let component: MainBoardHeaderComponent;
  let fixture: ComponentFixture<MainBoardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainBoardHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainBoardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
