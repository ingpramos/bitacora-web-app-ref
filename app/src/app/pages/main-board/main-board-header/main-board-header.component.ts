import { Component, OnInit } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ItemFormComponent } from "../item-form/item-form.component";
import { ImportExcelComponent } from "../import-excel/import-excel.component";
import { FiltersService } from "../../../shared/services/filters.service";
import { FormatDatePipe } from "../../../shared/pipes/format-date/format-date.pipe";

@Component({
    selector: "app-main-board-header",
    templateUrl: "./main-board-header.component.html",
    styleUrls: ["./main-board-header.component.scss"]
})
export class MainBoardHeaderComponent implements OnInit {
    constructor(
        private _sharedService: SharedService,
        private _excelService: ExcelService,
        private _itemsService: ItemsService,
        private _filtersService: FiltersService,
        private _formatDatePipe: FormatDatePipe,
        public dialog: MatDialog
    ) {
        this.currentFilterGroup["array_of_filters"] = [];
        this._filtersService.getFiltersModel().subscribe(data => {
            this.filterGroups = data;
            this._filtersService.setMaterialsGroup(this.filterGroups[0]);
        });
    }

    private subject: Subject<string> = new Subject();
    searchPattern: string;
    mainBoardHeaderText: any;
    filterGroups = [];
    currentFilterGroup: any = {};
    currentFilterValue:any;

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.mainBoardHeaderText = MBHLanguages[cl];
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this._itemsService.getItems().subscribe(data => {
                    this._itemsService.updateCurrentList(data);
                });
            } else {
                this.callItems();
            }
        });
    }

    searchItems() {
        this.subject.next(this.searchPattern);
    }

    callItems() {
        this._itemsService.getItemsByNameOrCode(this.searchPattern).subscribe(data => {
            this.currentFilterGroup = {};
            this._itemsService.updateCurrentList(data);
        });
    }

    getItemsByFilter() {
        let filter_id = this.currentFilterValue.filter_id;
        if(filter_id == null || !filter_id) return;
        this._itemsService.getItemsByFilter(filter_id).subscribe(data => this._itemsService.updateCurrentList(data));
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input-board");
        element.click();
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt, "mainBoard").subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            const dialogRef = this.dialog.open(ImportExcelComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input-board")).value = "";
            });
        });
    }

    setCollectionOfFilters() {
        this.currentFilterValue = "";
        this.searchPattern = "";
    }

    downloadExcelReport() {
        const name = this._formatDatePipe.transform(Date.now());
        let documentName = `${name}.xlsx`;
        this._itemsService.getPendigsItems().subscribe(data => {
            const columnsToDelete = ["iip_id", "item_id"];
            this._excelService.deleteObjectProperties(data, columnsToDelete);
            this._excelService.downLoadXlsxFile(data, documentName, name);
        });
    }

    createItem() {
        let initialState = { currentItem: {} };
        const dialogRef = this.dialog.open(ItemFormComponent, { width: "1000px", data: initialState });
    }
}

class MBHLanguages {
    static en = {
        search: "Find all items you have in stock",
        filterBy: "Filter By",
        select: "Select a",
        createItem: "Create New Item",
        createNewAssembly: "Create a New Assembly",
        createNewOrder: "Create a New Order",
        downloadExcelPendings: "Download Excel Pending Items",
        uploadExcelFile: "Upload Excel Items",
        languages: "Languages",
        languagesName: {
            english: "English",
            german: "German",
            spanish: "Spanish"
        }
    };

    static de = {
        search: "Finden Sie jeder ware in Lager",
        filterBy: "Filtern nach",
        select: "Wählen",
        createItem: "Neues Item erstellen",
        createNewAssembly: "Neue Baugruppe erstellen",
        createNewOrder: "Neue Bestellung erstellen",
        downloadExcelPendings: "Herrunterladen Excel Fehlende Artikel",
        uploadExcelFile: "Excel Items hochladen",
        languages: "Sprachen",
        languagesName: {
            english: "Englisch",
            german: "Deutsch",
            spanish: "Spanisch"
        }
    };

    static es = {
        search: "Encuentra cualquier articulo en inventario",
        filterBy: "Filtrar por",
        select: "Selecciona un ",
        createItem: "Crear un nuevo Articulo",
        createNewAssembly: "Crear un nuevo Conjunto",
        createNewOrder: "Crear nueva Orden",
        downloadExcelPendings: "Descargar Excel Items Requeridos",
        uploadExcelFile: "Importar Excel con Items",
        languages: "Idiomas",
        languagesName: {
            english: "Inglés",
            german: "Alemán",
            spanish: "Español"
        }
    };
}
