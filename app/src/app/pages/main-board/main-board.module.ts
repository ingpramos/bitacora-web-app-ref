import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainBoardComponent } from './main-board.component';
import { MainBoardHeaderComponent } from './main-board-header/main-board-header.component';
import { Routes, RouterModule } from '@angular/router';
import { MainBoardTableComponent } from './main-board-table/main-board-table.component';
import { ItemFormComponent } from './item-form/item-form.component';
import { ImportExcelComponent } from './import-excel/import-excel.component';
import { ColumnsFormComponent } from './columns-form/columns-form.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FiltersTableComponent } from './filters-table/filters-table.component';
import { FormatDatePipe } from '../../shared/pipes/format-date/format-date.pipe';
import { ItemGeneralDetailsComponent } from './item-general-details/item-general-details.component';
import { SharedModule } from '../../shared/shared.module';
import { ItemProcessDetailsModalComponent } from './item-process-details-modal/item-process-details-modal.component';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { MatInputModule, MatTabsModule, MatSelectModule, MatChipsModule, MatMenuModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule} from '@angular/material';
import { DirectivesModule } from '../../shared/directives/directives.module';

const MAINBOARD_ROUTES: Routes = [
    { path: '', component: MainBoardComponent }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MAINBOARD_ROUTES),
        PipesModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatTabsModule,
        MatSelectModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatMenuModule,
        MatDialogModule,
        MatIconModule,
        SharedModule,
        DirectivesModule
    ],
    declarations: [
        MainBoardComponent,
        MainBoardHeaderComponent,
        MainBoardTableComponent,
        ItemFormComponent,
        ImportExcelComponent,
        ColumnsFormComponent,
        FiltersTableComponent,
        ItemGeneralDetailsComponent,
        ItemProcessDetailsModalComponent,
        ItemHeaderComponent
    ],
    providers: [
        FormatDatePipe,
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
        ItemFormComponent,
        ImportExcelComponent,
        ColumnsFormComponent,
        FiltersTableComponent,
        ItemGeneralDetailsComponent,
        ItemProcessDetailsModalComponent
	]
})
export class MainBoardModule { }
