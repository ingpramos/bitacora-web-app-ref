import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";

@Component({
    selector: "app-item-header",
    templateUrl: "./item-header.component.html",
    styleUrls: ["./item-header.component.scss"]
})
export class ItemHeaderComponent implements OnInit {
    @Input() currentItem;
	itemHeaderText;
    currentLanguage:string;
    
    constructor(private _sharedService: SharedService) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemHeaderText = IHCLanguages[this.currentLanguage];
    }

    ngOnInit() {}
}

class IHCLanguages {
    static en = {
        itemName: "Item Name",
        itemCode: "Item Code",
        amountStock: "In Stock",
		amountRequired: "Booked",
		amountInProgress:"In Progress",
		amountInUse: "In use",		
        amountExpected: "Ordered",
        amountNeto: "Neto"
    };

    static de = {
        itemName: "Artikelname",
        itemCode: "Code",
        amountStock: "Vorrat",
		amountRequired: "Gebucht",
		amountInProgress:"In Arbeit",
		amountInUse: "Eingebaut", // TODO: prove if it is the right word to use		
        amountExpected: "Bestellt",
        amountNeto: "Netto"
    };

    static es = {
        itemName: "Nombre del item",
        itemCode: "Código",
        amountStock: "Stock",
		amountRequired: "Requeridos",
		amountInProgress:"En Proceso",
		amountInUse: "En uso",		
        amountExpected: "Pedidos",
        amountNeto: "Neto"
    };
}
