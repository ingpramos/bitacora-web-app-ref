import { Component, OnInit, Inject } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { FiltersService } from "../../../shared/services/filters.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-filters-table",
    templateUrl: "./filters-table.component.html",
    styleUrls: ["./filters-table.component.scss"]
})
export class FiltersTableComponent implements OnInit {
    filtersTableText;
    alerts;
    tableColumns;
    itemHeader;
    searchPattern;
    currentItem = this.modalData.currentItem;
    itemFilters = [];
    filters = [];
    selectedFilters = [];
    private subject: Subject<string> = new Subject();
    constructor(
        private _sharedService: SharedService,
        private _filtersService: FiltersService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<FiltersTableComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.filters = [];
                return;
            } else {
                this.callFilters();
            }
        });
    }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.filtersTableText = FTCLanguage[cl];
        this.alerts = this.filtersTableText.alerts;
        this.tableColumns = this.filtersTableText.columns;
        this.itemHeader = this.filtersTableText.itemHeader;
        const item_id = this.currentItem.item_id;
        this._filtersService.getItemFilters(item_id).subscribe(data => (this.itemFilters = data));
    }
    searchFilters() {
        this.subject.next(this.searchPattern);
    }

    applyChangesToList(allChecked) {
        this.filters.forEach((obj: any) => (obj.is_checked = allChecked));
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedFilters = this.filters.filter((obj: any) => obj.is_checked === true);
    }

    callFilters() {
        this._filtersService.getFiltersBySearchPattern(this.searchPattern).subscribe(data => (this.filters = data));
    }

    saveFilters() {
        this.selectedFilters.forEach(obj => (obj["item_id"] = this.currentItem.item_id));
        this._filtersService.saveFiltersForItem(this.selectedFilters).subscribe(
            data => {
                if (data) {
                    this.closeModal();
                    let title = this.currentItem["item_code"];
                    let text = this.alerts.alertFiltersInsertedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertFiltersInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    closeModal() {
        this.dialogRef.close();
    }

    deleteFilterFromItem(filter, index) {
        let title = filter.filter_value;
        let text = this.alerts.alertConfirmDeleteFilter;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                const iwf_id = filter.iwf_id;
                this._filtersService.deleteItemFilter(iwf_id).subscribe(
                    data => {
                        if (data) {
                            let title = filter.filter_value;
                            let text = this.alerts.alertFilterDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.itemFilters.splice(index, 1);
                        }
                    },
                    () => {
                        let text = this.alerts.alertFilterDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class FTCLanguage {
    static en = {
        alerts: {
            alertFiltersInsertedSuccessfully: "Filter added successfully",
            alertConfirmDeleteFilter: "Are you sure you want to delete this filter ?",
            alertFilterDeletedSuccessfully: "Filter deleted successfully",
            alertFiltersInsertError: "An error ocurred",
            alertFilterDeleteError: "An error ocurred"
        },
        searchFilters: "Search Filters",
        columns: {
            filter_position: "Position",
            filter_group: "Group",
            filter_value: "Filter"
        },
        itemHeader: {
            itemCode: "Item Code",
            itemName: "Item Name",
            itemAmount: "Amount"
        },
        modalTitle: "Add filters to item",
        filtersFound: "Filters Found",
        filtersSelected: "Filters Selected",
        saveButton: "Add Filters",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertFiltersInsertedSuccessfully: "Filters erfolgreich eingefügt",
            alertConfirmDeleteFilter: "Wollen Sie dieses Filter löschen ?",
            alertFilterDeletedSuccessfully: "Filter erfolgreich gelöscht",
            alertFiltersInsertError: "Es ist ein Fehler aufgetreten",
            alertFilterDeleteError: "Es ist ein Fehler aufgetreten"
        },
        searchFilters: "Labels suchen",
        columns: {
            filter_position: "Position",
            filter_group: "Gruppe",
            filter_value: "Label"
        },
        itemHeader: {
            itemCode: "Artikel Code",
            itemName: "Artikel Name",
            itemAmount: "Menge"
        },
        modalTitle: "Filtern zum Artikel addieren",
        filtersFound: "Filters gefunden",
        filtersSelected: "ausgewählte Filters",
        saveButton: "Filters einfügen",
        cancelButton: "Zürruck"
    };

    static es = {
        alerts: {
            alertFiltersInsertedSuccessfully: "Filtros insertados exitosamente",
            alertConfirmDeleteFilter: "Esta seguro que desea borrar esta filtro ?",
            alertFilterDeletedSuccessfully: "Filtro eliminado exitosamente",
            alertFiltersInsertError: "Ha ocurrido un error",
            alertFilterDeleteError: "Ha ocurrido un error"
        },
        searchFilters: "Buscar Filtros",
        columns: {
            filter_position: "Posición",
            filter_group: "Grupo",
            filter_value: "Filtro"
        },
        itemHeader: {
            itemCode: "Código",
            itemName: "Nombre",
            itemAmount: "Cantidad"
        },
        modalTitle: "Añadir filtros a Items",
        filtersFound: "Filtros Encontrados",
        filtersSelected: "Filters Selecionados",
        saveButton: "Añadir Filtros",
        cancelButton: "Cancelar"
    };
}
