import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ExcelService } from "../../../shared/services/excel.service";

@Component({
    selector: "app-import-excel",
    templateUrl: "./import-excel.component.html",
    styleUrls: ["./import-excel.component.scss"]
})
export class ImportExcelComponent implements OnInit {
    company_id: string;
    fileInformation: any = this.modalData.fileInformation;
    tableText: any;
    private alerts: any;
    tableColumns: any;
    itemsFromExcel: any = this.modalData.itemsFromExcel;
    items: any[] = [];
    showJustSelectedItems = true;
    itemsAlreadyInDB: any[] = [];
    selectedItems: any[] = [];
    allChecked: boolean = true;
    processingRequest: boolean = false;

    constructor(
        private _excelService: ExcelService,
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        private _itemsService: ItemsService,
        public dialogRef: MatDialogRef<ImportExcelComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {}

    ngOnInit() {
        this.company_id = this._sharedService.company_id;
        let cl = this._sharedService.user_selected_language;
        this.tableText = IELanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = IELanguages[cl].columns;
        if (this.itemsFromExcel.length > 0) {
            let arrayOfCodes = this.itemsFromExcel.map((obj: any) => obj["item_code"]);
            this._itemsService.checkIfItemsExists(arrayOfCodes).subscribe(itemsFound => {
                this._excelService.labelItemsAlreadyInDB(itemsFound, this.itemsFromExcel);
                this.itemsAlreadyInDB = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_db === true);
                this.selectedItems = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_db === false);
                this.items = this.selectedItems;
            });
        }
        this._columnsNamesService.formatColumnsOfImportTable(this.tableColumns);
    }

    applyChangesToList(allChecked: boolean) {
        this.itemsFromExcel.forEach((obj: any) => {
            obj.is_checked = allChecked;
            if (obj.is_already_in_db === false) obj.is_checked = allChecked;
        });
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedItems = this.itemsFromExcel.filter((obj: any) => obj.is_checked === true && obj.is_already_in_db === false);
    }

    closeModal() {
        this.dialogRef.close();
    }

    saveItems() {
        this.processingRequest = true;
        let formattedItems = this.formatItemsToInsert(this.selectedItems, this.company_id);
        this._itemsService.insertItemsFromExcel(formattedItems).subscribe(data => {
            if (data) {
                this.processingRequest = false;
                this.closeModal();
                this._itemsService.updateCurrentList(data); // TODO: eventually search fo a better transition from save items to sweet alert
                let title = data.length;
                let text = this.alerts.alertItemsInsertedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
            }
        });
    }

    formatItemsToInsert(collection, id) {
        collection.forEach(obj => {
            obj["company_id"] = id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["item_buy_price"] = this._sharedService.parseFormattedStringAsNumber(obj["item_buy_price_to_show"]);
            obj["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(obj["item_sell_price_to_show"]);
        });
        return collection;
    }

    showSelectedItems() {
        this.items = this.selectedItems;
        this.showJustSelectedItems = true;
    }
    showItemsAlreadyInDB() {
        this.items = this.itemsAlreadyInDB;
        this.showJustSelectedItems = false;
    }

    // Tabs related functions
    updateTab(index) {
        // update displayed view according the selected tab
        switch (index) {
            case 0:
                this.showSelectedItems();
                break;
            case 1:
                this.showItemsAlreadyInDB();
                break;
        }
    }
}

class IELanguages {
    static en = {
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name",
            item_buy_price: "Buy Price",
            item_sell_price: "Sell Price"
        },
        modalTitle: "Insert items from document in your stock",
        fileName: "File Name",
        itemsFound: "Items in File",
        itemsAlreadyInDB: "Items Already In Database",
        itemsSelected: "Selected Items",
        checkColumnsNames: "No columns where recognized, please check the name of the header columns in your document, the columns marked with * are required.",
        savingProcess: "Saving ...",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name",
            item_buy_price: "Kaufpreis",
            item_sell_price: "Verkaufspreis"
        },
        modalTitle: "Artikel ins Lager einfügen",
        fileName: "Dateiname",
        itemsFound: "Artikel in Datei",
        itemsAlreadyInDB: "bereits vorhandene Artikel",
        itemsSelected: "ausgewählte Artikel",
        checkColumnsNames:
            "Es wurden keine Spalten erkannt. Bitte überprüfen Sie den Namen der Kopfzeilenspalten in Ihrem Dokument. Die mit * gekennzeichneten Spalten sind erforderlich.",
        savingProcess: "Wird gespeichert ...",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre",
            item_buy_price: "Precio de Compra",
            item_sell_price: "Precio de Venta"
        },
        modalTitle: "Insertar items desde documento en la base de datos",
        fileName: "Nombre del Archivo",
        itemsFound: "Items en Archivo",
        itemsAlreadyInDB: "Items ya registrados",
        itemsSelected: "Items Seleccionados",
        checkColumnsNames:
            "no se reconocieron columnas, verifique el nombre de las columnas del encabezado en su documento, las columnas marcadas con * son obligatorias.",
        savingProcess: "Guardando ...",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
