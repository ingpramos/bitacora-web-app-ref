import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ColumnsNamesService } from '../../../shared/services/columns-names.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-columns-form',
	templateUrl: './columns-form.component.html',
	styleUrls: ['./columns-form.component.scss']
})
export class ColumnsFormComponent implements OnInit {

	itemsInOrdersTableColumnsText :any;
	defaultTableColumnsObj:any = this.modalData.defaultTableColumnsObj;
	currentTableColumns:any = this.modalData.currentTableColumns; // from the table
	currentColumnsObj:any; // for the form

	constructor(
		private _sharedService: SharedService,
		private _columnsNamesService: ColumnsNamesService,
		private _actionModalService: ActionModalsService,
		public dialogRef: MatDialogRef<ColumnsFormComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) { }

	
	ngOnInit() {
		// to brake the bond with the object displayed at the view
		this.currentColumnsObj = JSON.parse(JSON.stringify(this.currentTableColumns));
		let cl = this._sharedService.user_selected_language;
		this.itemsInOrdersTableColumnsText = CFCLanguages[cl];
	}

	saveColumns() {
		this._columnsNamesService.updateCustomColumnsNames(this.currentColumnsObj, "items").subscribe(data => {
			if (data) {
				this.closeModal();
				let title = this._columnsNamesService.formatSuccessData(data);
				let text = this.itemsInOrdersTableColumnsText.alertColumnsUpdatedSuccessfully;
				this._actionModalService.buildSuccessModal(title,text);	
				this._columnsNamesService.buildCustomTableColumnsObject("items",this.defaultTableColumnsObj);			
			}
		})
	}
	
	closeModal() {
		this.dialogRef.close();
	}

}

class CFCLanguages {
	static en = {
		"modalTitle": "Personalize table columns",
		"alertColumnsUpdatedSuccessfully": "Columns updated successfully",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};
	static de = {
		"modalTitle": "Personalize table columns",
		"alertColumnsUpdatedSuccessfully": "Reie erfolgreich aktualiziert",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};
	static es = {
		"modalTitle": "Personalizar Columnas",
		"alertColumnsUpdatedSuccessfully": "Columnas actualizadas existosamente",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
