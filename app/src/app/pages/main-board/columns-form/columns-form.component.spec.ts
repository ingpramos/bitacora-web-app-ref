import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnsFormComponent } from './columns-form.component';

describe('ColumnsFormComponent', () => {
  let component: ColumnsFormComponent;
  let fixture: ComponentFixture<ColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
