import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemProcessDetailsModalComponent } from './item-process-details-modal.component';

describe('ItemProcessDetailsModalComponent', () => {
  let component: ItemProcessDetailsModalComponent;
  let fixture: ComponentFixture<ItemProcessDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemProcessDetailsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemProcessDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
