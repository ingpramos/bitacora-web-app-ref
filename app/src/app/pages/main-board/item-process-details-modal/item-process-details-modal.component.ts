import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemsService } from '../../../shared/services/items.service';

@Component({
    selector: 'app-item-process-details-modal',
    templateUrl: './item-process-details-modal.component.html',
    styleUrls: ['./item-process-details-modal.component.scss']
})
export class ItemProcessDetailsModalComponent implements OnInit {

    currentItem;
	modalText;
	activitiesTableColumns;
	itemActivitiesResume: any[] = [];
	totalActivitiesCost = 0;
	companyCurrency : string = '';

    constructor(
        private _sharedService: SharedService,
        private _itemsService : ItemsService,
		public dialogRef: MatDialogRef<ItemProcessDetailsModalComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
    ) { }

    ngOnInit() {
        this.currentItem = this.modalData.currentItem;
		this.companyCurrency = this._sharedService.currency;
		let cl = this._sharedService.user_selected_language;
		this.modalText = IPDMCLanguages[cl];
		this.activitiesTableColumns = this.modalText.activitiesTableColumns;
		const item_id = this.currentItem.item_id;
		this._itemsService.getItemAverageCost(item_id).subscribe(data => {
            let costModel = this._itemsService.calculateItemTotalActivitiesCostModel(data);
			this.itemActivitiesResume = costModel['activitiesResume'];
			this.totalActivitiesCost = costModel['totalActivitiesCost'];
        });
    }

}

class IPDMCLanguages {
    static en = {
        "modalTitle": "Costs Details",
        "activitiesTableTitle": "Activities Details",
        "activitiesTableColumns": {
            "position": "Position",
            "activity_name": "Activity Name",
            "activity_hours_per_unit": "Hr/U",
            "activity_hour_price": "Cost/Hr",
            "activity_cost_per_unit": "Total Cost",
            "activity_percentage": "Percentage"
        },
        "activitiesTotalCost": "Total",
        "ordersTableTitle": "Item in purchase orders",
        "ordersTableColumns": {
            "position": "Position",
            "order_number": "Order number",
            "provider_name": "Provider",
            "item_ordered_amount": "Ordered Amount",
            "received_amount": "Received Amount",
            "order_delivery_date": "Delivery Date"
        }
    };

    static de = {
        "modalTitle": "Kosten Details",
        "activitiesTableTitle": "Aktivitäten Details",
        "activitiesTableColumns": {
            "position": "Position",
            "activity_name": "Aktivitätsname",
            "activity_hours_per_unit": "Std/E",
            "activity_hour_price": "Preis/Std",
            "activity_cost_per_unit": "Koste/E",
            "activity_percentage": "Prozente"
        },
        "activitiesTotalCost": "Total",
        "ordersTableTitle": "Artikel im Bestellungen",
        "ordersTableColumns": {
            "position": "Position",
            "order_number": "Bestellungsnummer",
            "provider_name": "Lieferant",
            "item_ordered_amount": "Menge",
            "received_amount": "Erhalten",
            "order_delivery_date": "Liefertermin"
        }
    };

    static es = {
        "modalTitle": "Detalles de Costos",
        "activitiesTableTitle": "Detalle de Actividades",
        "activitiesTableColumns": {
            "position": "Posición",
            "activity_name": "Actividad",
            "activity_hours_per_unit": "Hr/U",
            "activity_hour_price": "Costo/Hr",
            "activity_cost_per_unit": "Costo/U",
            "activity_percentage": "Porcentage"
        },
        "activitiesTotalCost": "Total",
        "ordersTableTitle": "Item en ordenes de compra",
        "ordersTableColumns": {
            "position": "Position",
            "order_number": "Orden de Compra",
            "provider_name": "Proveedor",
            "item_ordered_amount": "Cantidad",
            "received_amount": "Recivido",
            "order_delivery_date": "Fecha de Entrega"
        }
    };
}

