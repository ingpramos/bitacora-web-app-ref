import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-group-form",
    templateUrl: "./group-form.component.html",
    styleUrls: ["./group-form.component.scss"]
})
export class GroupFormComponent implements OnInit {
    @Input() currentGroup: any;
    @Input() editGroup;
    @Input() newGroup;
    @Input() mainGroupsView :boolean;
    @Output() closeForm = new EventEmitter<boolean>();

    groupFormText: any;
    alerts: any;

    constructor(private _actionModalsService: ActionModalsService, private _sharedService: SharedService, private _groupService: GroupsService) {
        let cl = this._sharedService.user_selected_language;
        this.groupFormText = GFLanguages[cl];
        this.alerts = this.groupFormText.alerts;
    }

    ngOnInit() {}

    saveGroup() {
        this.newGroup ? this.createGroup(this.currentGroup) : this.updateGroup(this.currentGroup);
    }

    createGroup(body) {
        this.mainGroupsView ? (body.assembly_type = 1) : (body.assembly_type = 2);
        body.assembly_visible = 1;
        this._groupService.createAssembly(body).subscribe(
            data => {
                if (data) {
                    this._groupService.passGroupToPushOrUpdate(data);
                    let title = this.currentGroup.assembly_number;
                    let text = this.alerts.alertGroupCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            e => {
                if (e.status == 403) {
                    // already exist
                    let title = this.currentGroup.assembly_number;
                    const text = this.alerts.alertGroupNumberAlreadyExist;
                    this._actionModalsService.buildInfoModal(title, text);
                } else {
                    let text = this.alerts.alertGroupCreateError;
                    this._actionModalsService.alertError(text);
                    this.closeForm.emit(true);
                }
            }
        );
    }

    updateGroup(body) {
        this._groupService.updateAssembly(body).subscribe(
            data => {
                if (data) {
                    this._groupService.passGroupToPushOrUpdate(data);
                    let title = this.currentGroup.assembly_number;
                    let text = this.alerts.alertGroupUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            () => {
                let text = this.alerts.alertGroupUpdateError;
                this._actionModalsService.alertError(text);
                this.closeForm.emit(true);
            }
        );
    }

    forgetActions() {
        this.closeForm.emit(true);
    }
}

class GFLanguages {
    static en = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Group updated successfully",
            alertGroupUpdateError: "An error ocurred",
            alertGroupCreatedSuccessfully: "Group created successfully",
            alertGroupNumberAlreadyExist: "Group already exist",
            alertGroupCreateError: "An error ocurred"
        },
        assemblyName: "Group Name",
        subAssemblyName: "Subgroup Name",
        assemblyNumber: "Group Number",
        subAssemblyNumber: "Subgroup Number",
		assemblyBuyPrice:"Buy Price",
		assemblySellPrice:"Sell Price",
		assemblyDetails:"Details",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Baugruppe erfolgreich aktualiziert",
            alertGroupUpdateError: "Es ist ein Fehler aufgetreten",
            alertGroupCreatedSuccessfully: "Baugruppe erfolgreich erstelt",
            alertGroupNumberAlreadyExist: "Baugruppe existiert bereits",
            alertGroupCreateError: "Es ist ein Fehler aufgetreten"
        },
        assemblyName: "Baugruppe Name",
        subAssemblyName: "Unterbaugruppe Name",
        assemblyNumber: "Baugruppe Nummer",
        subAssemblyNumber: "Unterbaugruppe Nummer",
		assemblyBuyPrice:"Kaufspreis",
		assemblySellPrice:"Verkaufspreis",
		assemblyDetails:"Details",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Grupo actualizado exitosamente",
            alertGroupUpdateError: "Ha ocurrido un error",
            alertGroupCreatedSuccessfully: "Grupo creado existosamente",
            alertGroupNumberAlreadyExist: "Este grupo ya  se encuentra registrado",
            alertGroupCreateError: "Ha ocurrido un error"
        },
        assemblyName: "Nombre del grupo",
        subAssemblyName: "Nombre del grupo",
        assemblyNumber: "Número del grupo",
        subAssemblyNumber: "Número del grupo",
		assemblyBuyPrice:"Precio de Compra",
		assemblySellPrice:"Precio de Venta",
		assemblyDetails:"Detalles",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
