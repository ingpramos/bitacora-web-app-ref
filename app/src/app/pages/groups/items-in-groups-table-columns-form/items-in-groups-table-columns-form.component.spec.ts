import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInGroupsTableColumnsFormComponent } from './items-in-groups-table-columns-form.component';

describe('ItemsInGroupsTableColumnsFormComponent', () => {
  let component: ItemsInGroupsTableColumnsFormComponent;
  let fixture: ComponentFixture<ItemsInGroupsTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInGroupsTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInGroupsTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
