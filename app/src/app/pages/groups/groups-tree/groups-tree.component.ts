import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Input } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { DynamicDatabase, DynamicFlatNode, DynamicDataSource } from './dinamic-data-source';
import { GroupsService } from '../../../shared/services/groups.service';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-groups-tree',
	templateUrl: 'groups-tree.component.html',
	styleUrls: ['groups-tree.component.scss'],
	providers: [DynamicDatabase]
})

export class GroupsTreeComponent {

	@Input() groups;
	@Input() mainGroupsView;

	groupToHandle = {};
	editGroup: boolean = false;
	newGroup: boolean = false;
	showSearch: boolean = false;
	private subject: Subject<string> = new Subject();
	searchPattern = '';
	groupTreeText;
	
	constructor(
		database: DynamicDatabase,
		private _sharedService: SharedService,
		private _groupsService: GroupsService
	) {
		this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
		this.dataSource = new DynamicDataSource(this.treeControl, database, this._groupsService);
		this._groupsService.currentGroupsList.subscribe(data => {
			if (data.length > 0) {		
				this.dataSource.data = database.initialData(data);
			}
		})

		let cl = this._sharedService.user_selected_language;
		this.groupTreeText = GTCLanguages[cl];
	}

	treeControl: FlatTreeControl<DynamicFlatNode>;

	dataSource: DynamicDataSource;

	getLevel = (node: DynamicFlatNode) => node.level;

	isExpandable = (node: DynamicFlatNode) => node.expandable;

	hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;


	searchGroups() {
		this.subject.next(this.searchPattern);
	}

	searchForGroups() {
		let assembly_type = this.mainGroupsView ? 1 : 2;
		this._groupsService.getGroupsByPattern(assembly_type, this.searchPattern);
	}

	closeSearch() {
		let assembly_type = this.mainGroupsView ?  1 : 2;
		this.searchPattern = '';
		this._groupsService.getGroups(assembly_type);
		this.showSearch = false;
	}

	startSearch(){
		this.editGroup = false;
		this.newGroup = false;
		this.showSearch = true;
	}

	startNewGroup() {
		this.groupToHandle = {};
		this.editGroup = false;
		this.showSearch = false;
		this.newGroup = true;
	}

	startEditGroup(group) {
		this.groupToHandle = group;
		this.newGroup = false;
		this.showSearch = false;
		this.editGroup = true;
	}

	forgetActions() {
		this.groupToHandle = {};
		this.editGroup = false;
		this.newGroup = false;
	}

}

class GTCLanguages {
	static en = {
		"alerts": {
			"confirmDeleteAssembly": "Are you sure you want to delete this Assembly ?",
			"alertAssemblyDeletedSuccessfully": "Assembly deleted successfully",
			"alertAssemblyDeleteError": "An error ocurred"
		},
		"mainGroups": "Assemblies",
		"subGroups": "Subassemblies",
		"newGroup": "Create Assembly",
		"searchGroups": "Search Assemblies",
		"editGroup": "Edit Assembly",
		"deleteGroup": "Delete Assembly"
	}

	static de = {
		"alerts": {
			"confirmDeleteAssembly": "Wollen Sie dieses Baugruppe löschen ?",
			"alertAssemblyDeletedSuccessfully": "Baugruppe erfolgreich gelöscht",
			"alertAssemblyDeleteError": "Es ist ein Fehler aufgetreten"
		},
		"mainGroups": "Baugruppen",
		"subGroups": "UnterBaugruppen",
		"newGroup": "Baugruppe erstellen",
		"searchGroups": "Baugruppen suchen",
		"editGroup": "Baugruppe Bearbeiten",
		"deleteGroup": "Baugruppe Löschen"
	}

	static es = {
		"alerts": {
			"confirmDeleteAssembly": "Estas seguro que deseas borrar el siguiente Ensamble ?",
			"alertAssemblyDeletedSuccessfully": "Ensamble eliminidado exitosamente",
			"alertAssemblyDeleteError": "Ha ocurrido un error"
		},
		"mainGroups": "Emsambles",
		"subGroups": "Subemsables",
		"newGroup": "Crear grupo",
		"searchGroups": "Buscar Grupos",
		"editGroup": "Editar Ensamble",
		"deleteGroup": "Eliminar Ensamble",
	}
}
