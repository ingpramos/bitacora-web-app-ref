

export const DataReference = [
    {
        "assembly_id": "70",
        "items_in_assembly": 3,
        "assembly_name": "GROUP ONE 1111111111111",
        "assembly_number": "GG11",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 1,
        "sub_assemblies_in_assembly": 2,
        "sub_assemblies_ids": ['73', '74']
    },
    {
        "assembly_id": "71",
        "items_in_assembly": 0,
        "assembly_name": "GRUPO DOS",
        "assembly_number": "GP222333",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 1,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": ['75', '76']
    },
    {
        "assembly_id": "78",
        "items_in_assembly": 0,
        "assembly_name": "HOY TEST Edit",
        "assembly_number": "HT123",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 1,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": ['73', '76']
    },
    {
        "assembly_id": "73",
        "items_in_assembly": 0,
        "assembly_name": "SUB ONE",
        "assembly_number": "SA01",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 1,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "74",
        "items_in_assembly": 0,
        "assembly_name": "SUB GROUP TWO",
        "assembly_number": "SG2",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": ['79', '80']
    },
    {
        "assembly_id": "75",
        "items_in_assembly": 0,
        "assembly_name": "TERCER SUB ensamble",
        "assembly_number": "TER SUB 33",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": ['81', '82']
    },
    {
        "assembly_id": "76",
        "items_in_assembly": 0,
        "assembly_name": "SUB CUARTO",
        "assembly_number": "SG44444",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "79",
        "items_in_assembly": 3,
        "assembly_name": "GROUP ONE 1111111111111",
        "assembly_number": "SUB 5",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 2,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "80",
        "items_in_assembly": 0,
        "assembly_name": "GRUPO DOS",
        "assembly_number": "SUB 6",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "81",
        "items_in_assembly": 0,
        "assembly_name": "HOY TEST Edit",
        "assembly_number": "SUB 7",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "82",
        "items_in_assembly": 0,
        "assembly_name": "SUB 8",
        "assembly_number": "SA01",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 1,
        "sub_assemblies_ids": []
    },
    {
        "assembly_id": "83",
        "items_in_assembly": 0,
        "assembly_name": "SUB GROUP TWO",
        "assembly_number": "SUB 9",
        "company_id": "3UGMSKKJBY",
        "assembly_type": 2,
        "sub_assemblies_in_assembly": 0,
        "sub_assemblies_ids": []
    }
];

function formatData(collection: any[]) {
    let dataToMap = [];
    let rootLevelData = collection.filter(obj => obj['assembly_type'] === 1).map(obj => obj['assembly_id']);
    collection.forEach(obj => {
        if (obj['sub_assemblies_ids'].length > 0) {
            let arr = [obj['assembly_id'], obj['sub_assemblies_ids']];
            dataToMap.push(arr);
        }
    });
    return { dataToMap: dataToMap, rootLevelData: rootLevelData, originalData:collection };
}

export function getData() {
    return formatData(DataReference);
}