import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { GroupsService } from "../../../shared/services/groups.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";

@Component({
    selector: "app-insert-sub-groups",
    templateUrl: "./insert-sub-groups.component.html",
    styleUrls: ["./insert-sub-groups.component.scss"]
})
export class InsertSubGroupsComponent implements OnInit {
    allChecked: boolean;
    groupsTableText: any;
    groups = [];
    selectedGroups = [];
    currentGroup: any = this.modalData.currentGroup;
    groupHeaderText: any = this.modalData.groupHeaderText;
    mainGroupsView:boolean = this.modalData.mainGroupsView;
    alerts;
    // search related variables
    searchPattern: string;
    private subject: Subject<string> = new Subject();
    constructor(
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<InsertSubGroupsComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.groupsTableText = ISGCLanguages[cl];
        this.alerts = this.groupsTableText.alerts;
    }

    ngOnInit() {
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.groups = [];
                this.selectedGroups = [];
                return;
            } else {
                const assembly_id = this.currentGroup.assembly_id;
                this._groupsService.searchAllowedGroupsByPattern(assembly_id, this.searchPattern).subscribe(data => (this.groups = data));
            }
        });
    }

    searchGroups() {
        this.subject.next(this.searchPattern);
    }

    closeModal() {
        this.dialogRef.close();
    }

    applyChangesToList(allChecked) {
        this.groups.forEach((obj: any) => (obj.is_checked = allChecked));
        this.getSelectedGroups();
    }

    getSelectedGroups() {
        this.selectedGroups = this.groups.filter((obj: any) => obj.is_checked === true);
    }

    saveGroups() {
        let assembly_id = this.currentGroup.assembly_id;
        this.addAssemblyId(this.selectedGroups, assembly_id);
        this._groupsService.insertSubAssembliesInAssembly(this.selectedGroups).subscribe(
            data => {
                this._groupsService.getItemsFromAssembly(assembly_id);
                this.closeModal();
                let title = this.currentGroup.assembly_number;
                let text = `${data.length.toString()} ${this.alerts.alertAssembliesInsertedSuccessfully}`;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertAssembliesInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    addAssemblyId(collection, assembly_id) {
        collection.forEach(obj => {
            obj["sub_assembly_id"] = obj["assembly_id"];
            obj["assembly_id"] = assembly_id;
        });
        return collection;
    }
}

class ISGCLanguages {
    static en = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Groups inserted successfully",
            alertAssembliesInsertError: "An error ocurred"
        },
        modalTitle: "Insert subgroups in groups",
        assembly_number: "Group number",
        assembly_name: "Group name",
        items_in_assembly: "Items",
        sub_assemblies_in_assembly: "Subassemblies",
        selected: "Selected",
        searchSubAssemblies: " Search groups",
        assembliesFound: "Groups found",
        itemsSelected: "Selected groups",
        saveButton: "Insert groups",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertAssembliesInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Unterbaugruppe in Baugruppe einfügen",
        assembly_number: "Baugruppe Nummer",
        assembly_name: "Baugruppe Name",
        items_in_assembly: "Teile",
        sub_assemblies_in_assembly: "Unterbaugruppen",
        selected: "Ausgewählt",
        searchSubAssemblies: " Baugruppen Suchen",
        assembliesFound: "Baugruppen gefunden",
        itemsSelected: "ausgewählte Baugruppen",
        saveButton: "Baugruppen einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Grupos insertados exitosamente",
            alertAssembliesInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar subgrupo en grupo",
        assembly_number: "Número del grupo",
        assembly_name: "Nombre del grupo",
        items_in_assembly: "Piezas",
        sub_assemblies_in_assembly: "subgrupos",
        selected: "Seleccionados",
        searchSubAssemblies: " Buscar grupos",
        assembliesFound: "Grupos econtrados",
        itemsSelected: "Grupos seleccionados",
        saveButton: "Insertar Grupos",
        cancelButton: "Cancelar"
    };
}
