import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertSubGroupsComponent } from './insert-sub-groups.component';

describe('InsertSubGroupsComponent', () => {
  let component: InsertSubGroupsComponent;
  let fixture: ComponentFixture<InsertSubGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsertSubGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertSubGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
