import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInGroupsTableComponent } from './items-in-groups-table.component';

describe('ItemsInGroupsTableComponent', () => {
  let component: ItemsInGroupsTableComponent;
  let fixture: ComponentFixture<ItemsInGroupsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInGroupsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInGroupsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
