import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ItemInGroupFormComponent } from "../item-in-group-form/item-in-group-form.component";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ItemsInGroupsTableColumnsFormComponent } from "../items-in-groups-table-columns-form/items-in-groups-table-columns-form.component";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";

@Component({
    selector: "app-items-in-groups-table",
    templateUrl: "./items-in-groups-table.component.html",
    styleUrls: ["./items-in-groups-table.component.scss"]
})
export class ItemsInGroupsTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() currentGroup: any;
    items:any[] = [];
    currentLanguage:string;
    itemsInGroupText: any;
    tableColumns: any = {};
    defaultTableColumnsObj: any;
    alerts: any;
    // subscriptions
    itemsList$;
    columnsNames$;
    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInGroupText = IIGTLanguages[this.currentLanguage];
        this.alerts = this.itemsInGroupText.alerts;
        this.defaultTableColumnsObj = this.itemsInGroupText.columns;
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_assemblies", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        this.itemsList$ = this._groupsService.currentItemsList.subscribe(data => {
            this.items = data;
			this._sharedService.stopLoading();
		});
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => (this.tableColumns = data));
    }

    ngOnChanges(changes) {
        if (changes.currentGroup && changes.currentGroup != {}) {
            let assembly_id = changes.currentGroup.currentValue.assembly_id;
            if (assembly_id) {
                this._groupsService.getItemsFromAssembly(assembly_id);
                this._sharedService.startLoading();
            }
            if (!assembly_id) this._groupsService.updateItemList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
    }

    editItem(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemInGroupFormComponent, { width: "1000px", data: initialState });
    }

    editTableColumns() {
        let initialState = { defaultTableColumnsObj: this.defaultTableColumnsObj, currentTableColumns: this.tableColumns };
        const dialogRef = this.dialog.open(ItemsInGroupsTableColumnsFormComponent, { width: "1000px", data: initialState });
    }

    deleteRow(item, index) {
        let title = item.item_code;
        let text = this.alerts.confirmDeleteItem;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                item.item_type === "item" ? this.deleteItem(item, index) : this.deleteSubassembly(item, index);
            }
        });
    }
    deleteItem(item, index) {
        let iia_id = item.iia_id;
        this._groupsService.deleteItemsFromAssembly(iia_id).subscribe(
            data => {
                if (data) {
                    let title = item.item_code;
                    let text = this.alerts.alertItemDeletedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.items.splice(index, 1);
                    this._groupsService.updateItemList(this.items);
                }
            },
            () => {
                let text = this.alerts.alertItemDeleteError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    deleteSubassembly(item, index) {
        let iia_id = item.iia_id;
        this._groupsService.deleteSubAssemblyFromAssembly(iia_id).subscribe(
            data => {
                if (data) {
                    let title = item.item_code;
                    let text = this.alerts.alertItemDeletedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.items.splice(index, 1);
                }
            },
            () => {
                let text = this.alerts.alertItemDeleteError;
                this._actionModalsService.alertError(text);
            }
        );
    }
}

class IIGTLanguages {
    static en = {
        title: "Items In Group",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Item Code",
            item_name: "Item Name",
            item_amount: "Amount"
        },
        alerts: {
            confirmDeleteItem: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns Names"
    };

    static de = {
        title: "Gruppe Details",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Artikel Code",
            item_name: "Name",
            item_amount: "Menge"
        },
        alerts: {
            confirmDeleteItem: "Sind Sie sicher das Sie dieses Artikel löschen möchte?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Spaltennamen bearbeiten"
    };

    static es = {
        title: "Items En el Grupo",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Código del Item",
            item_name: "Nombre",
            item_amount: "Cantidad"
        },
        alerts: {
            confirmDeleteItem: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item eliminado exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar nombres de las columnas"
    };
}
