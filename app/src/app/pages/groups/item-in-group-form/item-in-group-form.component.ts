import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef , MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';
import { GroupsService } from '../../../shared/services/groups.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
	selector: 'app-item-in-group-form',
	templateUrl: './item-in-group-form.component.html',
	styleUrls: ['./item-in-group-form.component.scss']
})

export class ItemInGroupFormComponent implements OnInit {

	constructor(
		private _groupsService: GroupsService,
		private _sharedService: SharedService,
		private _actionModalsService: ActionModalsService,
		public dialogRef: MatDialogRef <ItemInGroupFormComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) { }

	itemFormText: any;
	currentItem: any = this.modalData.currentItem;

	ngOnInit() {
		let cl = this._sharedService.user_selected_language;
		this.itemFormText = IIGLanguages[cl];
	}

	saveItem(item) {
		this._groupsService.updateItemInAssembly(item).subscribe(data => {
			if (data) {
				let title = item.item_name;
				let text = this.itemFormText.alertUpdatedItem;
				this._actionModalsService.buildSuccessModal(title, text);
				this.closeModal()
			}
		}, () => {
			let text = this.itemFormText.alertUpdateError;
			this._actionModalsService.alertError(text);
		})
	}

	closeModal() {
		this.currentItem = {};
		this.dialogRef.close();
	}

}

class IIGLanguages {
	static en = {
		"alertUpdatedItem": "Item updated successfully",
		"alertUpdateError": "An error ocurred",
		"modalTitle":"Update item information",
		"itemCode": "Item Code",
		"itemName": "Item Name",
		"itemAmount": "Amount",
		"subAssemblyNumber": "Subassembly Number",
		"subAssemblyName": "Subassembly Name",
		"requiredFieldsMessage": "Fields marked * are required fields.",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};

	static de = {
		"alertUpdatedItem": "Artikel erfolgreich aktualiziert",
		"alertUpdateError": "Es ist ein Fehler aufgetreten",
		"modalTitle":"Artikel information aktualizieren",
		"itemCode": "Artikel Code",
		"itemName": "Name",
		"itemAmount": "Menge",
		"subAssemblyNumber": "Unterbaugruppe Nummer",
		"subAssemblyName": "Unterbaugruppe Name",
		"requiredFieldsMessage": "Mit * gekennzeichnete Felder sind Pflichtfelder.",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};

	static es = {
		"alertUpdatedItem": "Item actualizado exitosamente",
		"alertUpdateError": "Ha ocurrido un error",
		"modalTitle":"Actualizar informacion del articulo",
		"itemCode": "Código Del Item",
		"itemName": "Nombre",
		"itemAmount": "Cantidad",
		"subAssemblyNumber": "Número del subensamble",
		"subAssemblyName": "Nombre del subensamble",
		"requiredFieldsMessage": "Los campos marcados con * son requeridos.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
