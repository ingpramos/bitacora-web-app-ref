import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInGroupFormComponent } from './item-in-group-form.component';

describe('ItemInGroupFormComponent', () => {
  let component: ItemInGroupFormComponent;
  let fixture: ComponentFixture<ItemInGroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInGroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
