import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-groups-list",
    templateUrl: "./groups-list.component.html",
    styleUrls: ["./groups-list.component.scss"]
})
export class GroupsListComponent implements OnInit {
    componentUrl = this.activeRoute.snapshot["_urlSegment"].segments[1].path;

    @Input() groups;
    @Input() mainGroupsView:boolean;

    groupsListText;
    alerts: any;
    groupToHandle = {};
    editGroup: boolean = false;
    newGroup: boolean = false;

    showSearch: boolean = false;
    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private activatedRoute: ActivatedRoute;
    private paramMap$;
    private assembly_id;

    constructor(
        activatedRoute: ActivatedRoute,
        private router: Router,
        private activeRoute: ActivatedRoute,
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.groupsListText = GLLanguages[cl];
        this.alerts = this.groupsListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.assembly_id = +paramMap.get("assembly_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForGroups();
        });
    }
    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchGroups() {
        this.subject.next(this.searchPattern);
    }

    searchForGroups() {
        let assembly_type = this.mainGroupsView ? 1 : 2;
        this._groupsService.getGroupsByPattern(assembly_type, this.searchPattern);
    }

    closeSearch() {
        let assembly_type = this.mainGroupsView ? 1 : 2;
        this.searchPattern = "";
        this._groupsService.getGroups(assembly_type);
        this.showSearch = false;
    }

    startSearch() {
        this.editGroup = false;
        this.newGroup = false;
        this.showSearch = true;
    }

    startNewGroup() {
        this.groupToHandle = {};
        this.editGroup = false;
        this.showSearch = false;
        this.newGroup = true;
    }

    startEditGroup(group) {
        this.groupToHandle = Object.assign({}, group);
        this.newGroup = false;
        this.showSearch = false;
        this.editGroup = true;
    }

    forgetActions() {
        this.groupToHandle = {};
        this.editGroup = false;
        this.newGroup = false;
    }

    deleteGroup(group, index) {
        let title = group.assembly_name;
        let text = this.alerts.confirmDeleteAssembly;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let assembly_id = group.assembly_id;
                this._groupsService.deleteAssembly(assembly_id).subscribe(
                    data => {
                        if (data) {
                            let text = this.alerts.alertAssemblyDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.groups.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let text = this.alerts.alertAssemblyDeleteError;
                            this._actionModalsService.alertError(text);
                        }
                    }
                );
            }
        });
    }
}

class GLLanguages {
    static en = {
        alerts: {
            confirmDeleteAssembly: "Are you sure you want to delete this group ?",
            alertAssemblyDeletedSuccessfully: "Group deleted successfully",
            alertForeignKeyViolation: "This group is still referenced in one of your projects",
            alertAssemblyDeleteError: "An error ocurred"
        },
        mainGroups: "Groups",
        subGroups: "Subgroups",
        newGroup: "Create Group",
        searchGroups: "Search Groups",
        editGroup: "Edit Group",
        deleteGroup: "Delete Group"
    };

    static de = {
        alerts: {
            confirmDeleteAssembly: "Wollen Sie dieses Baugruppe löschen ?",
            alertAssemblyDeletedSuccessfully: "Baugruppe erfolgreich gelöscht",
            alertForeignKeyViolation: "Dieser Baugruppe ist immer noch referenziert in einer Ihrer Projekten",
            alertAssemblyDeleteError: "Es ist ein Fehler aufgetreten"
        },
        mainGroups: "Baugruppen",
        subGroups: "UnterBaugruppen",
        newGroup: "Baugruppe erstellen",
        searchGroups: "Baugruppen suchen",
        editGroup: "Baugruppe bearbeiten",
        deleteGroup: "Baugruppe löschen"
    };

    static es = {
        alerts: {
            confirmDeleteAssembly: "Estas seguro que deseas borrar este Grupo ?",
            alertAssemblyDeletedSuccessfully: "Grupo eliminidado exitosamente",
            alertForeignKeyViolation: "Este grupo está referenciado en una de sus proyectos",
            alertAssemblyDeleteError: "Ha ocurrido un error"
        },
        mainGroups: "Grupos",
        subGroups: "Subgrupos",
        newGroup: "Crear Grupo",
        searchGroups: "Buscar Grupos",
        editGroup: "Editar Grupo",
        deleteGroup: "Eliminar Grupo"
    };
}
