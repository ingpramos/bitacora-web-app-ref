import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { PipesModule } from '../../shared/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { GroupsComponent } from './groups.component';
import { GroupsListComponent } from './groups-list/groups-list.component';
import { GroupHeaderComponent } from './group-header/group-header.component';
import { ItemsInGroupsTableComponent } from './items-in-groups-table/items-in-groups-table.component';
import { SearchItemsToInsertInGroupComponent } from './search-items-to-insert-in-group/search-items-to-insert-in-group.component';
import { ImportExcelIntoGroupComponent } from './import-excel-into-group/import-excel-into-group.component';
import { GroupFormComponent } from './group-form/group-form.component';
import { ItemInGroupFormComponent } from './item-in-group-form/item-in-group-form.component';
import { ItemsInGroupsTableColumnsFormComponent } from './items-in-groups-table-columns-form/items-in-groups-table-columns-form.component';
import { InsertSubGroupsComponent } from './insert-sub-groups/insert-sub-groups.component';
import { GroupsTreeComponent } from './groups-tree/groups-tree.component';
import { DirectivesModule } from '../../shared/directives/directives.module';
// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatSelectModule,MatTreeModule,MatButtonModule,MatProgressBarModule,
	MatFormFieldModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule
} from '@angular/material';


const GROUPS_ROUTES: Routes = [
	{ path: '', component: GroupsComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(GROUPS_ROUTES),
		PipesModule,
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatSelectModule,
		MatFormFieldModule,
		MatDialogModule,
		MatTreeModule,
		MatButtonModule,
		MatProgressBarModule,
		MatIconModule,
		DirectivesModule
	],
	declarations: [
		GroupsComponent,
		GroupsListComponent,
		GroupHeaderComponent,
		ItemsInGroupsTableComponent,
		SearchItemsToInsertInGroupComponent,
		ImportExcelIntoGroupComponent,
		GroupFormComponent,
		ItemInGroupFormComponent,
		ItemsInGroupsTableColumnsFormComponent,
		InsertSubGroupsComponent,
		GroupsTreeComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		SearchItemsToInsertInGroupComponent,
		ImportExcelIntoGroupComponent,
		ItemInGroupFormComponent,
		ItemsInGroupsTableColumnsFormComponent,
		InsertSubGroupsComponent
	]
})
export class GroupsModule { }
