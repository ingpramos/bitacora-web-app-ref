import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExcelIntoGroupComponent } from './import-excel-into-group.component';

describe('ImportExcelIntoGroupComponent', () => {
  let component: ImportExcelIntoGroupComponent;
  let fixture: ComponentFixture<ImportExcelIntoGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportExcelIntoGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExcelIntoGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
