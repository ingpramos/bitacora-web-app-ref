import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-import-excel-into-group",
    templateUrl: "./import-excel-into-group.component.html",
    styleUrls: ["./import-excel-into-group.component.scss"]
})
export class ImportExcelIntoGroupComponent implements OnInit {

    groupHeaderText: any = this.modalData.groupHeaderText;
    currentGroup: any = this.modalData.currentGroup;
    fileInformation: any = this.modalData.fileInformation;
    mainGroupsView = this.modalData.mainGroupsView;
    // tabs
    selectedTab = 0;
    showJustSelectedItems:boolean = true;
    tableText: any;
    alerts: any;
    tableColumns: any;
    itemsFromExcel: any = this.modalData.itemsFromExcel;
    items:any[] = [];
    selectedItems = [];
    itemsAlreadyInList = [];
    itemsNotFoundInDB = [];
    allChecked = true;

    constructor(
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _columnsNamesService: ColumnsNamesService,
        private _excelService: ExcelService,
        private _itemsService: ItemsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<ImportExcelIntoGroupComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.tableText = IEIGLanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
    }

    ngOnInit() {
        if (this.itemsFromExcel.length > 0) {
            this._excelService.labelItemsAlreadyInList(this._groupsService.getItemsOfCurrentGroup(), this.itemsFromExcel);
            this.itemsAlreadyInList = this.itemsFromExcel.filter(obj => obj.is_already_in_list === true);
            let preSelectedItems: any[] = this.itemsFromExcel.filter(obj => obj.is_already_in_list === false);
            if (preSelectedItems.length > 0) {
                // check in db if items exists or not
                let arrayOfCodes = preSelectedItems.map((obj: any) => obj["item_code"]);
                this._itemsService.checkIfItemsExists(arrayOfCodes).subscribe(itemsFound => {
                    this._excelService.labelItemsAlreadyInDB(itemsFound, preSelectedItems);
                    this.itemsNotFoundInDB = preSelectedItems.filter(obj => obj.is_already_in_db === false);
                    this.selectedItems = preSelectedItems.filter(obj => obj.is_already_in_db === true);
                    this.items = this.selectedItems;
                });
            }
        }
        this._columnsNamesService.formatColumnsOfImportTable(this.tableColumns);
    }

    applyChangesToList(allChecked) {
        this.itemsFromExcel.forEach((obj: any) => {
            if (obj.is_already_in_list === false && obj.is_already_in_db === true) obj.is_checked = allChecked;
        });
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedItems = this.itemsFromExcel.filter((obj: any) => {
            return obj.is_checked === true && obj.is_already_in_db === true && obj.is_already_in_list === false;
        });
    }

    saveItems() {
        let assembly_id = this.currentGroup.assembly_id;
        let formattedItems = this.formatItemsToInsert(this.selectedItems ,assembly_id);
        this._groupsService.insertItemsInAssembly(formattedItems).subscribe(
            data => {
                if (data) {
                    this._groupsService.getItemsFromAssembly(assembly_id);
                    this.closeModal();
                    let title = this.currentGroup.assembly_number;
                    let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertItemsInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatItemsToInsert(collection: any[], id) {
        collection.forEach((obj: any) => {
            obj["assembly_id"] = id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["user_id"] = this._sharedService.user_id;
        });
        return collection;
    }

    closeModal() {
        this.dialogRef.close();
    }

    showSelectedItems(){
        this.items = this.selectedItems;
        this.showJustSelectedItems = true;
    }
    showItemsAlreadyInList(){
        this.items = this.itemsAlreadyInList;
        this.showJustSelectedItems = false;
    }
    showItemsNotFoundInDB(){
        this.items = this.itemsNotFoundInDB;
        this.showJustSelectedItems = false;
    }
    // Tabs related functions
    updateTab(index){ // update displayed view according the selected tab
        switch (index) {
            case 0 : this.showSelectedItems();
            break;
            case 1 : this.showItemsAlreadyInList();
            break;
            case 2 : this.showItemsNotFoundInDB();
            break;
        }        
    }
}

class IEIGLanguages {
    static en = {
        fileName: "File Name",
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        modalTitle: "Insert items in group",
        itemsFound: "Items Found",
        itemsAlreadyInList: "Items Already in Assembly",
        itemsNotFoundInDB: "Not Registered Items",
        itemsSelected: "Selected Items",
        checkColumnsNames: "No columns where recognized, please check the name of the header columns in your document, the columns marked with * are required.",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        fileName: "Dateiname",
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Baugruppe einfügen",
        itemsFound: "Artikel gefunden",
        itemsAlreadyInList: "bereits vorhandene Artikel",
        itemsNotFoundInDB: "Nicht registrierte Artikel",
        itemsSelected: "ausgewählte Artikel",
        checkColumnsNames:
            "Es wurden keine Spalten erkannt. Bitte überprüfen Sie den Namen der Kopfzeilenspalten in Ihrem Dokument. Die mit * gekennzeichneten Spalten sind erforderlich.",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        fileName: "Nombre del Archivo",
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar articulos en grupo",
        itemsFound: "Items Encontrados",
        itemsAlreadyInList: "Articulos Repetidos",
        itemsNotFoundInDB: "Articulos no registrados",
        itemsSelected: "Items Seleccionados",
        checkColumnsNames:
            "no se reconocieron columnas, verifique el nombre de las columnas del encabezado en su documento, las columnas marcadas con * son obligatorias.",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
