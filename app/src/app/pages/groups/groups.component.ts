
import { map } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { GroupsService } from '../../shared/services/groups.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-groups',
	templateUrl: './groups.component.html',
	styleUrls: ['./groups.component.scss']
})

export class GroupsComponent implements OnInit, OnDestroy {
	// check component url
	componentUrl:string = this.activeRoute.snapshot['_urlSegment'].segments[1].path;
	mainGroupsView : boolean = this.componentUrl === "main-groups" ? true : false;
	groups = [];
	zeroGroups: boolean;
	selectedGroup = {};
	assembly_id;
	// subscriptions
	groupsList$;
	groupToPushOrUpdate$;

	constructor(
		private _groupsService: GroupsService,
		private router: Router,
		private activeRoute: ActivatedRoute
	) {
		this.activeRoute.params.pipe(map(params => params['assembly_id'])).subscribe(assembly_id => {
			this.assembly_id = assembly_id;
			this.setSelectedGroup();
		});
		// to add a new group to the collection of groups
		this.groupToPushOrUpdate$ = this._groupsService.groupToPushOrUpdate.subscribe(data => {
			let assembly_id = data.assembly_id;
			if (!assembly_id) return; // case {} an initialization
			let index = this.groups.findIndex(obj => obj['assembly_id'] == assembly_id);
			if (index == -1) {  // group was created 
				this.groups.push(data);
				this.router.navigate(['app/' + this.componentUrl, assembly_id]);
			} else { // group was updated
				this.groups.splice(index, 1, data);
				this.selectedGroup = data;
			}
		});
		this.groupsList$ = this._groupsService.currentGroupsList.subscribe(data => {
			this.groups = data;
			this.setSelectedGroup();
		});
	}

	ngOnInit() {
		this.mainGroupsView ? this._groupsService.getGroups(1) : this._groupsService.getGroups(2);
	}

	ngOnDestroy() {
		this.groupToPushOrUpdate$.unsubscribe();
		this.groupsList$.unsubscribe();
		this._groupsService.updateGroupList([]); // clean group list before leaving
	}

	setSelectedGroup() {
		if (this.groups.length > 0) {
			let found = this.groups.find((obj) => obj['assembly_id'] == this.assembly_id);
			this.selectedGroup = found ? found : this.groups[0];
			this.router.navigate(['app/' + this.componentUrl, this.selectedGroup['assembly_id']]);
			window.scroll(0, 0);
		} else {
			this.selectedGroup = {};
		}
	}

}
