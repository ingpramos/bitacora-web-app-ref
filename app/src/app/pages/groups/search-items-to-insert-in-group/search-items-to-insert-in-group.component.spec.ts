import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInGroupComponent } from './search-items-to-insert-in-group.component';

describe('SearchItemsToInsertInGroupComponent', () => {
  let component: SearchItemsToInsertInGroupComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
