import { Component, OnInit, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { SearchItemsToInsertInGroupComponent } from "../search-items-to-insert-in-group/search-items-to-insert-in-group.component";
import { ImportExcelIntoGroupComponent } from "../import-excel-into-group/import-excel-into-group.component";
import { InsertSubGroupsComponent } from "../insert-sub-groups/insert-sub-groups.component";
import { GroupsService } from "../../../shared/services/groups.service";

@Component({
    selector: "app-group-header",
    templateUrl: "./group-header.component.html",
    styleUrls: ["./group-header.component.scss"]
})
export class GroupHeaderComponent implements OnInit {
    @Input() currentGroup;
    @Input() mainGroupsView: boolean; // to decide wich text to show
    @Input() showMenu: boolean; // to show or hide three points menu
    groupHeaderText: any;
    itemsInGroup = 0;
    // subscriptions
    itemsList$;

    constructor(
                private _sharedService: SharedService, 
                private _excelService: ExcelService,
                public dialog: MatDialog,
                private _groupsService: GroupsService
                ) {
        let cl = this._sharedService.user_selected_language;
        this.groupHeaderText = GHLanguages[cl];
    }

    ngOnInit() {
        // update number of items in header
        this.itemsList$ = this._groupsService.currentItemsList.subscribe(data => (this.itemsInGroup = data.length));
    }

    searchForItems() {
        let initialInformation = {};
        initialInformation["currentGroup"] = this.currentGroup;
        initialInformation["groupHeaderText"] = this.groupHeaderText;
        initialInformation["mainGroupsView"] = this.mainGroupsView;
        const dialogRef = this.dialog.open(SearchItemsToInsertInGroupComponent, { width: "1200px", data: initialInformation });
    }

    searchForSubAssemblies() {
        let initialInformation = {};
        initialInformation["currentGroup"] = this.currentGroup;
        initialInformation["groupHeaderText"] = this.groupHeaderText;
        initialInformation["mainGroupsView"] = this.mainGroupsView;
        const dialogRef = this.dialog.open(InsertSubGroupsComponent, { width: "1200px", data: initialInformation });
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentGroup"] = this.currentGroup;
            initialState["groupHeaderText"] = this.groupHeaderText;
            const dialogRef = this.dialog.open(ImportExcelIntoGroupComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }
}

class GHLanguages {
    static en = {
        assemblyName: "Group Name",
        subAssemblyName: "Subgroup Name",
        assemblyNumber: "Group Number",
        subAssemblyNumber: "Subgroup Number",
        components: "Pieces",        
        subAssemblies: "Subgroups",
        insertItems: "Insert Items",
        insertSubAssemblies: "Insert Subgroups",
        uploadExcel: "Upload Excel File",
        filterTable: "Search Item List"
    };

    static de = {
        assemblyName: "Baugruppe Name",
        subAssemblyName: "Unterbaugruppe Name",
        assemblyNumber: "Baugruppe Nummer",
        subAssemblyNumber: "Unterbaugruppe Number",
        components: "Teile",
        subAssemblies: "Unterbaugruppen",
        insertItems: "Artikel einfügen",
        insertSubAssemblies: "Unterbaugruppe einfügen",
        uploadExcel: "Excel Items hochladen",
        filterTable: "Tabelle Suchen"
    };

    static es = {
        assemblyName: "Nombre del Grupo",
        subAssemblyName: "Nombre del subgrupo",
        assemblyNumber: "Número del Grupo",
        subAssemblyNumber: "Número del subgrupo",
        components: "Piezas",
        subAssemblies: "Subgrupos",
        insertItems: "Insertar Piezas",
        insertSubAssemblies: "Insertar Subgrupos",
        uploadExcel: "Importar Excel con Items",
        filterTable: "Buscar Item"
    };
}
