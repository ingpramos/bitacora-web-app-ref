import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { DeliveryNotesService } from "../../../shared/services/delivery-notes.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { CompanyService } from "../../../shared/services/company.service";
import { DateAdapter } from "@angular/material/core";

@Component({
    selector: "app-delivery-note-form",
    templateUrl: "./delivery-note-form.component.html",
    styleUrls: ["./delivery-note-form.component.scss"]
})

export class DeliveryNoteFormComponent implements OnInit {
    @Input() currentDeliveryNote;
    @Input() editDeliveryNote: boolean;
    @Input() newDeliveryNote: boolean;

    @Output() useForm = new EventEmitter<boolean>();

    sessionInfo: any;
    currentDeliveryNoteCopy;
    showCurrentInvoiceWhileEdit: boolean = false;
    invoices: any = {};
    deliveryMethods: any = {};
    deliveryConditions: any = {};
    deliveryNoteFormText: any;
    alerts: any;

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _deliveryNotesService: DeliveryNotesService,
        private _companyService: CompanyService,
        private adapter: DateAdapter<any>
    ) { }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        // change the date picker adapter to current language
        this.adapter.setLocale(cl);
        this.deliveryNoteFormText = DNFCLanguages[cl];
        this.alerts = this.deliveryNoteFormText.alerts;

        if (this.editDeliveryNote) this.showCurrentInvoiceWhileEdit = true;
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            if (this.newDeliveryNote) this.callNextDeliveryNoteNumber();
            if (this.editDeliveryNote) this.currentDeliveryNoteCopy = JSON.parse(JSON.stringify(this.currentDeliveryNote));
            this.callInvoices();
            this.deliveryMethods = this._companyService.formatDeliveryMethods(this.sessionInfo.delivery_methods);
            this.deliveryConditions = this._companyService.formatDeliveryConditions(this.sessionInfo.delivery_conditions);
        });
    }

    ngOnChanges(changes) {
        if (this.newDeliveryNote && this.sessionInfo) this.callNextDeliveryNoteNumber();
    }

    callNextDeliveryNoteNumber() {
        this._deliveryNotesService.getCurrentDeliveryNoteNumber().subscribe(data => {
            this.currentDeliveryNote.delivery_note_number = `${data.document_prefix}${data.document_current_number + 1}`;
        });
    }

    callInvoices() {
        this._deliveryNotesService.getAvailableInvoices().subscribe(response => (this.invoices = response));
    }

    setDocument(event) {
        let docId = this.invoices["get_internal_document_id"](event.value);
        let internalDocument = this.invoices["original_data"].find(obj => obj["internal_document_id"] == docId);
        this.setProperties(internalDocument);
    }

    setProperties(internalDocument) {
        this.currentDeliveryNote["internal_document_id"] = internalDocument["internal_document_id"];
        this.currentDeliveryNote["client_name"] = internalDocument["client_name"];
        this.currentDeliveryNote["payment_method_name"] = internalDocument["payment_method_name"];
        this.currentDeliveryNote["payment_condition_description"] = internalDocument["payment_condition_description"];
        this.currentDeliveryNote["delivery_method_name"] = internalDocument["delivery_method_name"];
        this.currentDeliveryNote["delivery_condition_description"] = internalDocument["delivery_condition_description"];
    }

    saveDeliveryNote() {
        this.editDeliveryNote ? this.updateDeliveryNote() : this.createDeliveryNoteFromInvoice();
    }

    createDeliveryNoteFromInvoice() {
        this._deliveryNotesService.createDeliveryNote(this.currentDeliveryNote).subscribe(
            data => {
                if (data) {
                    this._deliveryNotesService.passNewDeliveryNote(data);
                    this.useForm.emit(false);
                    let title = this.currentDeliveryNote.delivery_note_number;
                    let text = this.alerts.alertDeliveryNoteCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertDeliveryNoteCreateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    updateDeliveryNote() {
        this.currentDeliveryNote.prev_doc_id = this.currentDeliveryNoteCopy.internal_document_id;
        this.currentDeliveryNote.new_doc_id = this.currentDeliveryNote.internal_document_id;
        this._deliveryNotesService.updateDeliveryNote(this.currentDeliveryNote).subscribe(
            data => {
                if (data) {
                    this.useForm.emit(false);
                    let title = this.currentDeliveryNote.delivery_note_number;
                    let text = this.alerts.alertDeliveryNoteUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertDeliveryNoteUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    forgetActions() {
        if (this.editDeliveryNote) {
            this.currentDeliveryNote.order_confirmation_number = this.currentDeliveryNoteCopy.order_confirmation_number;
            this.setProperties(this.currentDeliveryNoteCopy);
        } else {
            this.currentDeliveryNote = {};
        }
        this.useForm.emit(false);
    }
}

class DNFCLanguages {
    static en = {
        alerts: {
            alertDeliveryNoteUpdatedSuccessfully: "Invoice updated successfully",
            alertDeliveryNoteUpdateError: "An error ocurred",
            alertDeliveryNoteCreatedSuccessfully: "Invoice created successfully",
            alertDeliveryNoteCreateError: "An error ocurred"
        },
        deliveryNoteNumber: "Delivery Note Number",
        currentDeliveryNoteNumber: "Current Invoice Number",
        invoiceNumber: "Invoice Number",
        client: "Client",
        paymentMethod: "Payment Method",
        paymentConditions: "Payment Conditions",
        deliveryMethod: "Delivery Method",
        deliveryConditions: "Delivery Conditions",
        invoiceEmissionDate: "Invoice Date",
        deliveryNoteDate: "Delivery Date",
        companyContact: "Contact",
        invoiceStatus: "Order Status",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertDeliveryNoteUpdatedSuccessfully: "Rechnung erfolgreich aktualiziert",
            alertDeliveryNoteUpdateError: "Es ist ein Fehler aufgetreten",
            alertDeliveryNoteCreatedSuccessfully: "Rechnung erfolgreich erstelt",
            alertDeliveryNoteCreateError: "Es ist ein Fehler aufgetreten"
        },
        deliveryNoteNumber: "Lieferscheinnummer",
        currentDeliveryNoteNumber: "Aktuell Rechnungsnummer",
        invoiceNumber: "Rechnungsnummer",
        client: "Kunde",
        paymentMethod: "Zahlungsart",
        paymentConditions: "Zahlungsbedingungen",
        deliveryMethod: "Liefertart",
        deliveryConditions: "Liefertbedigungen",
        invoiceEmissionDate: "Rechnungsdatum",
        deliveryNoteDate: "Lifertdatum",
        companyContact: "Ansprechpartner",
        invoiceStatus: "Bestellung Status",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertDeliveryNoteUpdatedSuccessfully: "Factura actualizada exitosamente",
            alertDeliveryNoteUpdateError: "Ha ocurrido un error",
            alertDeliveryNoteCreatedSuccessfully: "Factura creada existosamente",
            alertDeliveryNoteCreateError: "Ha ocurrido un error"
        },
        deliveryNoteNumber: "Número de Nota de Entrega",
        currentDeliveryNoteNumber: "Número de Pedido Actual",
        invoiceNumber: "Factura",
        client: "Cliente",
        paymentMethod: "Método de Pago",
        paymentConditions: "Condiciones de Pago:",
        deliveryMethod: "Método de Pago",
        deliveryConditions: "Condiciones de Entrega",
        invoiceEmissionDate: "Fecha De Facturación",
        deliveryNoteDate: "Fecha de Entrega",
        companyContact: "Contacto",
        invoiceStatus: "Estado de la Orden",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
