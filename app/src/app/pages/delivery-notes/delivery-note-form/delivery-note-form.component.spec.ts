import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryNoteFormComponent } from './delivery-note-form.component';

describe('DeliveryNoteFormComponent', () => {
  let component: DeliveryNoteFormComponent;
  let fixture: ComponentFixture<DeliveryNoteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryNoteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryNoteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
