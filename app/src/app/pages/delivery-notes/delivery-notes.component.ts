import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { map } from "rxjs/operators";
import { DeliveryNotesService } from "../../shared/services/delivery-notes.service";
import { InternalDocumentsService } from "../../shared/services/internal-documents.service";

@Component({
    selector: "app-delivery-notes",
    templateUrl: "./delivery-notes.component.html",
    styleUrls: ["./delivery-notes.component.scss"]
})
export class DeliveryNotesComponent implements OnInit, OnDestroy {
    // component properties
    deliveryNotes: any = [];
    delivery_note_id: any;
    selectedDeliveryNote = {};
    // subscriptions
    deliveryNotesList$;
    newDeliveryNote$;

    constructor(
        private _deliveryNotesService: DeliveryNotesService,
        private _idocsService: InternalDocumentsService,
        private activeRoute: ActivatedRoute,
        private router: Router
    ) {
        this._deliveryNotesService.passNewDeliveryNote({});
        this.activeRoute.params.pipe(map(params => params["dn_id"])).subscribe(dn_id => {
            this.delivery_note_id = dn_id;
            this.setSelectedDeliveryNote();
        });
        this.newDeliveryNote$ = this._deliveryNotesService.newDeliveryNoteToPush.subscribe(data => {
            let delivery_note_id = data.internal_document_id;
            if (delivery_note_id) {
                this.deliveryNotes.push(data);
                this.router.navigate(["app/delivery-notes", delivery_note_id]);
                window.scroll(0, 0);
            }
        });
        this.deliveryNotesList$ = this._deliveryNotesService.currentDeliveryNotesList.subscribe(data => {
            this.deliveryNotes = data;
            this.setSelectedDeliveryNote();
        });
    }

    ngOnInit() {
        this._deliveryNotesService.getDeliveryNotes();
    }
    
    ngOnDestroy() {
        this.newDeliveryNote$.unsubscribe();
        this.deliveryNotesList$.unsubscribe();
    }

    setSelectedDeliveryNote() {
        if (this.deliveryNotes.length > 0) {
            let found = this.deliveryNotes.find(obj => obj["internal_document_id"] == this.delivery_note_id);
            this.selectedDeliveryNote = found ? found : this.deliveryNotes[0];
            this.router.navigate(["app/delivery-notes", this.selectedDeliveryNote["internal_document_id"]]);
            this._idocsService.setCurrentDocument(this.selectedDeliveryNote);
        } else {
            this.selectedDeliveryNote = {};
            this._idocsService.setCurrentDocument(this.selectedDeliveryNote);
        }
    }
}
