import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInDeliveryNoteTableComponent } from './items-in-delivery-note-table.component';

describe('ItemsInDeliveryNoteTableComponent', () => {
  let component: ItemsInDeliveryNoteTableComponent;
  let fixture: ComponentFixture<ItemsInDeliveryNoteTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInDeliveryNoteTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInDeliveryNoteTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
