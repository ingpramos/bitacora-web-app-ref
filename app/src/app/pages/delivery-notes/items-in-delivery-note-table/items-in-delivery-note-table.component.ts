import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { MatDialog } from "@angular/material";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-items-in-delivery-note-table",
    templateUrl: "./items-in-delivery-note-table.component.html",
    styleUrls: ["./items-in-delivery-note-table.component.scss"]
})

export class ItemsInDeliveryNoteTableComponent implements OnInit {
    @Input() currentDeliveryNote;
    companyCurrency: string = "";
    currentLanguage:string;
    items = [];
    itemsInDNText: any;
    tableColumns: any = {};
    defaultTableColumnsObj: any;
    alerts: any;
    invoiceTotalFooter: any = {};
    // subscription
    itemsList$;
    columnsNames$;
    constructor(
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _actionModalsService: ActionModalsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.companyCurrency = this._sharedService.currency;
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInDNText = IIDNTCLanguages[this.currentLanguage];
        this.alerts = this.itemsInDNText.alerts;
        this.defaultTableColumnsObj = this.itemsInDNText.columns;
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => (this.tableColumns = data));
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_offers", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        // to update the items when new ones are inserted
        this.itemsList$ = this._idocsService.currentItemsList.subscribe(data => {
            this.items = data;
            if (this.items) {
                this._idocsService.setItemsOfCurrentDocument(this.items);
                this._idocsService.calculateDocumentTotalPriceModel();
            }
        });
        this._idocsService.documentTotalPriceModelObs.subscribe(data => {
            this.invoiceTotalFooter = data;
            this.currentDeliveryNote.subTotal = this.invoiceTotalFooter.subTotal;
            this.currentDeliveryNote.vat = this.invoiceTotalFooter.vat;
            this.currentDeliveryNote.total = this.invoiceTotalFooter.total;
        });
    }

    ngOnChanges(changes) {
        let id_id = changes.currentDeliveryNote.currentValue.internal_document_id;
        if (id_id) {
            this._idocsService.getItemsFromInternalDocument(id_id);
        }
        if (!id_id) {
            this._idocsService.updateItemsList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
    }

    deleteItem(item, index) {
        item["item_type"] == "item" ? this.deleteItemFromDeliveryNote(item, index) : this.deleteAssemblyFromDeliveryNote(item, index);
    }

    deleteItemFromDeliveryNote(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromDN;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                const iiinvoice_id = item.item_in_internal_document_id;
                this._idocsService.deleteItemFromInternalDocument (iiinvoice_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.calculateDocumentTotalPriceModel();
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    deleteAssemblyFromDeliveryNote(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromDN;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let aiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteAssemblyFromInternalDocument(aiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class IIDNTCLanguages {

    static en = {
        title: "Delivery Notes Details",
        totalWithoutVAT: "Total without VAT",
        vatFrom: "VAT from ",
        totalWithVAT: "Total with VAT",
        columns: {
            item_position: "Pos",
            item_type:"Type",
            item_code: "Item Code",
            item_name: "Item Description",
            item_amount: "Amount",
            item_sell_price: "Price(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromDN: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted Successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns Names"
    };

    static de = {
        title: "Lieferschein Details",
        totalWithoutVAT: "Total exkl. MwSt.",
        vatFrom: "MwSt von",
        totalWithVAT: "Total inkl. MwSt.",
        columns: {
            item_position: "Pos",
            item_type:"Typ",
            item_code: "Artikel Code",
            item_name: "Artikelbezeichnung",
            item_amount: "Menge",
            item_sell_price: "Preis(E)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromDN: "Sind Sie sicher das Sie dieses Artikel löschen möchte?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Sapltennamen Bearbeiten"
    };

    static es = {
        title: "Detalles de la Nota de Entrega",
        totalWithoutVAT: "Total sin IVA",
        vatFrom: "IVA de ",
        totalWithVAT: "Total Con IVA",
        columns: {
            item_position: "Pos",
            item_type:"Tipo",
            item_code: "Código del Item",
            item_name: "Descripción",
            item_amount: "Cantidad",
            item_sell_price: "Precio(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromDN: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item Eliminado Exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Nombre de las Columnas"
    };
}
