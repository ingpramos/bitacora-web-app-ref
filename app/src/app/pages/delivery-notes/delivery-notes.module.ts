import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { DeliveryNotesComponent } from './delivery-notes.component';
import { DeliveryNotesListComponent } from './delivery-notes-list/delivery-notes-list.component';
import { DeliveryNoteHeaderComponent } from './delivery-note-header/delivery-note-header.component';
import { ItemsInDeliveryNoteTableComponent } from './items-in-delivery-note-table/items-in-delivery-note-table.component';
import { DeliveryNoteFormComponent } from './delivery-note-form/delivery-note-form.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
// Angular Material
import {
  MatMenuModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
  MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatIconModule
} from '@angular/material';

const DELIVERYNOTES_ROUTES: Routes = [
  { path: '', component: DeliveryNotesComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DELIVERYNOTES_ROUTES),
    PipesModule,
    FormsModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule
  ],
  declarations: [
    DeliveryNotesComponent,
    DeliveryNotesListComponent,
    DeliveryNoteHeaderComponent,
    ItemsInDeliveryNoteTableComponent,
    DeliveryNoteFormComponent
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
  ]
})
export class DeliveryNotesModule { }
