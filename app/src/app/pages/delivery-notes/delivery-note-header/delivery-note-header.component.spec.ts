import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryNoteHeaderComponent } from './delivery-note-header.component';

describe('DeliveryNoteHeaderComponent', () => {
  let component: DeliveryNoteHeaderComponent;
  let fixture: ComponentFixture<DeliveryNoteHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryNoteHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryNoteHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
