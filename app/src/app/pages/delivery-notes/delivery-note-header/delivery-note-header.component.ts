import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { DeliveryNotePdfService } from "../../../shared/services/pdfs/delivery-note-pdf.service";

@Component({
    selector: "app-delivery-note-header",
    templateUrl: "./delivery-note-header.component.html",
    styleUrls: ["./delivery-note-header.component.scss"]
})
export class DeliveryNoteHeaderComponent implements OnInit {
    @Input() currentDeliveryNote: any;

    dnHeaderText: any;
    itemsInDeliveryNote = 0;
    itemsSubscription;
    constructor(private _sharedService: SharedService, private _deliveryNotePdfService: DeliveryNotePdfService) {
        let cl = this._sharedService.user_selected_language;
        this.dnHeaderText = DNHCLanguage[cl];
    }

    ngOnInit() {}

    ngOnDestroy() {}

    generatePdfDeliveryNote() {
        let document = this._deliveryNotePdfService.buildDocument(this.currentDeliveryNote);
        let documentName = `${this.currentDeliveryNote.delivery_note_number} - ${this.currentDeliveryNote.client_name}`;
        this._deliveryNotePdfService.pdfMake.createPdf(document).download(documentName);
    }
}

class DNHCLanguage {
    user_selected_language = "en";

    static en = {
        deliveryNoteNumber: "DeliveryNote Number",
        invoiceNumber: "Invoice Number",
        ocNumber: "Order Number",
        deliveryNoteClient: "Client",
        deliveryNoteDate: "Date",
        deliveryNoteItems: "Items",
        deliveryNoteProject: "Projects",
        insertItems: "Insert Items",
        insertProjects: "Insert Projects",
        downloadExcelFile: "Download DeliveryNote Excel",
        uploadExcel: "Upload Excel File",
        downloadPdfFile: "Download DeliveryNote PDF",
        filterTable: "Search Item List",
        downloadPdfNote: "Download Delivery Note"
    };

    static de = {
        deliveryNoteNumber: "Lieferscheinnummer",
        invoiceNumber: "Invoice Number",
        ocNumber: "Order Number",
        deliveryNoteClient: "Kunde",
        deliveryNoteDate: "Datum",
        deliveryNoteItems: "Artikel",
        deliveryNoteProject: "Aufträge",
        insertItems: "Teile Einfügen",
        insertProjects: "Projekt Einfügen",
        downloadExcelFile: "Excel Rechnung Herrunterladen",
        uploadExcel: "Excel Datei hochladen",
        downloadPdfFile: "PDF Rechnung Herrunterladen",
        filterTable: "Tabelle Suchen",
        downloadPdfNote: "PDF Lieferschein"
    };
    static es = {
        deliveryNoteNumber: "Nr. Nota de entrega",
        invoiceNumber: "Nr. de Factura",
        ocNumber: "Nr. de Orden",
        deliveryNoteClient: "Cliente",
        deliveryNoteDate: "Fecha",
        deliveryNoteItems: "Articulos",
        deliveryNoteProject: "Proyectos",
        insertItems: "Facturar Piezas",
        insertProjects: "Facturar Proyecto",
        downloadExcelFile: "Descargar Factura Excel",
        uploadExcel: "Importar Excel con Items",
        downloadPdfFile: "Descargar Factura PDF",
        filterTable: "Buscar Item",
        downloadPdfNote: "Descargar Nota De Entrega"
    };
}
