import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { DeliveryNotesService } from "../../../shared/services/delivery-notes.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-delivery-notes-list",
    templateUrl: "./delivery-notes-list.component.html",
    styleUrls: ["./delivery-notes-list.component.scss"]
})
export class DeliveryNotesListComponent implements OnInit {
    // check component url
    componentUrl = this.activatedRoute.snapshot["_urlSegment"].segments[1].path;

    @Input() deliveryNotes;
    deliveryNotesListText: any;
    alerts: any;
    deliveryNoteToHandle = {};
    editDeliveryNote: boolean = false;
    newDeliveryNote: boolean = false;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private paramMap$;
    private dn_id;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _sharedService: SharedService,
        private _deliveryNotesService: DeliveryNotesService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.deliveryNotesListText = DNLCLanguages[cl];
        this.alerts = this.deliveryNotesListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.dn_id = +paramMap.get("dn_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForDelivereyNotes();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    startSearch() {
        this.editDeliveryNote = false;
        this.newDeliveryNote = false;
        this.showSearch = true;
    }

    searchDeliveryNotes() {
        this.subject.next(this.searchPattern);
    }

    searchForDelivereyNotes() {
        this._deliveryNotesService.getDeliveryNotesByPattern(this.searchPattern);
    }

    closeSearch() {
        this.searchPattern = "";
        this.showSearch = false;
        this._deliveryNotesService.getDeliveryNotes();
    }

    startNewDeliveryNote() {
        this.editDeliveryNote = false;
        this.deliveryNoteToHandle = {};
        this.newDeliveryNote = true;
    }

    startEditDeliveryNote(dn) {
        this.newDeliveryNote = false;
        this.editDeliveryNote = true;
        this.deliveryNoteToHandle = dn;
    }

    forgetActions() {
        this.editDeliveryNote = false;
        this.newDeliveryNote = false;
        this.deliveryNoteToHandle = {};
    }

    deleteDeliveryNote(deliveryNote, index) {
        let title = deliveryNote.delivery_note_number;
        let text = this.alerts.confirmDeleteDeliveryNote;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let deliveryNote_id = deliveryNote.internal_document_id;
                this._deliveryNotesService.deleteDeliveryNote(deliveryNote_id).subscribe(
                    data => {
                        if (data) {
                            let title = deliveryNote.delivery_note_number;
                            let text = this.alerts.alertDeliveryNoteDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.deliveryNotes.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    () => {
                        let text = this.alerts.alertDeliveryNoteDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class DNLCLanguages {
    static en = {
        alerts: {
            confirmDeleteDeliveryNote: "Are You Sure You Want To Delete This DeliveryNote ?",
            alertDeliveryNoteDeletedSuccessfully: "Order Confirmation Deleted Successfully",
            alertDeliveryNoteDeleteError: "An error ocurred",
            alertCloseDeliveryNoteError: "An error ocurred"
        },
        newDeliveryNote: "New Delivery Note",
        searchDeliveryNotes: "Search Delivery Notes",
        filterDeliveryNotes: "Search With Filters",
        deliveryNotes: "Delivery Notes",
        editDeliveryNote: "Edit Delivery Note",
        deleteDeliveryNote: "Delete Delivery Note",
        closeDeliveryNote: "Close DeliveryNote",
        creationDate: "Created at",
        deliveryDate: "Delivery"
    };
    static de = {
        alerts: {
            confirmDeleteDeliveryNote: "Wollen Sie dieses Lieferschein Löschen ?",
            alertDeliveryNoteDeletedSuccessfully: "Auftragbestätigung gelöscht",
            alertDeliveryNoteDeleteError: "Es ist ein Fehler aufgetreten",
            alertCloseDeliveryNoteError: "Es ist ein Fehler aufgetreten"
        },
        newDeliveryNote: "Neuer Lieferschein",
        searchDeliveryNotes: "Suche Lieferscheine",
        filterDeliveryNotes: "Suchen mit Filtern",
        deliveryNotes: "Lieferscheine",
        editDeliveryNote: "Lieferschein Bearbeiten",
        deleteDeliveryNote: "Lieferschein Löschen",
        closeDeliveryNote: "Lieferschein schließen",
        creationDate: "Erstellt",
        deliveryDate: "Lifertdatum"
    };
    static es = {
        alerts: {
            confirmDeleteDeliveryNote: "Esta seguro que deseas borrar la factura ?",
            alertDeliveryNoteDeletedSuccessfully: "Nota de Entrega eliminada exitosamente",
            alertDeliveryNoteDeleteError: "Ha ocurrido un error",
            alertCloseDeliveryNoteError: "Ha ocurrido un error"
        },
        newDeliveryNote: "Nueva Nota de Entrega",
        searchDeliveryNotes: "Buscar Notas de Entrega",
        filterDeliveryNotes: "Busqueda con Filtros",
        deliveryNotes: "Notas de Entrega",
        editDeliveryNote: "Editar Nota de Entrega",
        deleteDeliveryNote: "Eliminar Nota de Entrega",
        closeDeliveryNote: "Cerrar Nota de Entrega",
        creationDate: "Creación",
        deliveryDate: "Entrega"
    };
}
