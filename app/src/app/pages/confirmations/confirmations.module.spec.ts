import { ConfirmationsModule } from './confirmations.module';

describe('ConfirmationsModule', () => {
  let confirmationsModule: ConfirmationsModule;

  beforeEach(() => {
    confirmationsModule = new ConfirmationsModule();
  });

  it('should create an instance', () => {
    expect(confirmationsModule).toBeTruthy();
  });
});
