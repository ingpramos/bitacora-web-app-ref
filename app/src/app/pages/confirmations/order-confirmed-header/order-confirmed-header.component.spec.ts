import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderConfirmedHeaderComponent } from './order-confirmed-header.component';

describe('OrderConfirmedHeaderComponent', () => {
  let component: OrderConfirmedHeaderComponent;
  let fixture: ComponentFixture<OrderConfirmedHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderConfirmedHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderConfirmedHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
