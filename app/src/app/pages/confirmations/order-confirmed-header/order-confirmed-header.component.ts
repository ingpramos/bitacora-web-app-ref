import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { SearchItemsToInsertInOcComponent } from "../search-items-to-insert-in-oc/search-items-to-insert-in-oc.component";
import { OcPdfService } from "../../../shared/services/pdfs/oc-pdf.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ImportExcelIntoOcComponent } from "../import-excel-into-oc/import-excel-into-oc.component";
import { OffersService } from "../../../shared/services/offers.service";
import { MatDialog } from "@angular/material/dialog";
import { SearchGroupsToInsertInOcComponent } from "../search-groups-to-insert-in-oc/search-groups-to-insert-in-oc.component";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-order-confirmed-header",
    templateUrl: "./order-confirmed-header.component.html",
    styleUrls: ["./order-confirmed-header.component.scss"]
})
export class OrderConfirmedHeaderComponent implements OnInit {
    @Input() currentOrderConfirmation;
    @Input() zeroOC: boolean; // to enable or disable menu items
    @Input() openOrdersConfirmedView: boolean; // to enable or disable menu items
    @Input() showMenu: boolean; // to show or hide three points menu

    itemsInOC: number = 0;
    orderConfirmedHeaderText: any = {};
    itemsList$;

    constructor(
        private _sharedService: SharedService,
        private _ocPdfService: OcPdfService,
        private _excelService: ExcelService,
        private _idocsService: InternalDocumentsService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.orderConfirmedHeaderText = OCHCLanguage[cl];
        this.itemsList$ = this._idocsService.currentItemsList.subscribe(items => (this.itemsInOC = items.length));
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
    }

    searchForItems() {
        let initialState = {};
        initialState["currentOrderConfirmation"] = this.currentOrderConfirmation;
        initialState["itemsInOC"] = this.itemsInOC;
        const dialogRef = this.dialog.open(SearchItemsToInsertInOcComponent, { width: "1200px", data: initialState });
    }

    searchForAssemblies() {
        let initialState = {};
        initialState["currentOrderConfirmation"] = this.currentOrderConfirmation;
        const dialogRef = this.dialog.open(SearchGroupsToInsertInOcComponent, { width: "1200px", data: initialState });
    }

    downloadExcelOC() {
        let documentName = `${this.currentOrderConfirmation.order_confirmation_number} - ${this.currentOrderConfirmation.client_name}.xlsx`;
        let columnsToDelete = ["item_in_internal_document_id","internal_document_id","item_id","company_id","item_raw_material_dimensions","item_type","user_id"];
        let itemsInOC = this._idocsService.getItemsOfCurrentDocument("excel");
        this._excelService.deleteObjectProperties(itemsInOC, columnsToDelete);
        this._excelService.downLoadXlsxFile(itemsInOC, documentName, this.currentOrderConfirmation.order_confirmation_number);
    }

    generatePdfOC() {
        let document = this._ocPdfService.buildDocument(this.currentOrderConfirmation);
        let documentName = `${this.currentOrderConfirmation.order_confirmation_number} - ${this.currentOrderConfirmation.client_name}`;
        this._ocPdfService.pdfMake.createPdf(document).download(documentName);
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentOrderConfirmation"] = this.currentOrderConfirmation;
            initialState["itemsInOC"] = this.itemsInOC;
            const dialogRef = this.dialog.open(ImportExcelIntoOcComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }
}

class OCHCLanguage {
    static en = {
        orderConfirmationNumber: "Order Number",
        offerNumber: "Offer Number",
        offerClient: "Client",
        offerContact: "Contact",
        contactTelephone: "Telephone",
        contactEmail: "Email",
        ocItems: "Items",
        offerCreationDate: "Offered at",
        insertItems: "Insert Items",
        insertAssemblies: "Insert Groups",
        downloadExcelFile: "Download Order Confirmation Excel",
        uploadCsv: "Upload Excel File",
        downloadPdfFile: "Download Order Confirmation PDF",
        filterTable: "Search Item List"
    };
    static de = {
        orderConfirmationNumber: "AB nummer",
        offerClient: "Kunde",
        offerContact: "Kontact",
        offerNumber: "Angebotsnummer",
        contactTelephone: "Telefon",
        contactEmail: "Email",
        ocItems: "Artikel",
        offerCreationDate: "Datum",
        insertItems: "Artikel einfügen",
        insertAssemblies: "Baugruppen einfügen",
        downloadExcelFile: "AB als Excel herrunterladen",
        uploadCsv: "Excel Datei hochladen",
        downloadPdfFile: "AB als PDF herrunterladen",
        filterTable: "Tabelle Suchen"
    };
    static es = {
        orderConfirmationNumber: "Número de pedido",
        offerNumber: "Número de Oferta",
        offerClient: "Cliente",
        offerContact: "Contacto",
        contactTelephone: "Teléfono",
        contactEmail: "Email",
        ocItems: "Articulos",
        offerCreationDate: "Fecha",
        insertItems: "Añadir Items",
        insertAssemblies: "Añadir Grupos",
        downloadExcelFile: "Descargar Pedido Excel",
        uploadCsv: "Importar Excel con Items",
        downloadPdfFile: "Descargar Confirmación PDF",
        filterTable: "Buscar Item"
    };
}
