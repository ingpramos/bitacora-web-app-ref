import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOcTableColumnsFormComponent } from './items-in-oc-table-columns-form.component';

describe('ItemsInOcTableColumnsFormComponent', () => {
  let component: ItemsInOcTableColumnsFormComponent;
  let fixture: ComponentFixture<ItemsInOcTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOcTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOcTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
