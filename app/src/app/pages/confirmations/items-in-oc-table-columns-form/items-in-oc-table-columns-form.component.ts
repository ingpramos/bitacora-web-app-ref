import { Component, OnInit, Inject } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
    selector: "app-items-in-oc-table-columns-form",
    templateUrl: "./items-in-oc-table-columns-form.component.html",
    styleUrls: ["./items-in-oc-table-columns-form.component.scss"]
})
export class ItemsInOcTableColumnsFormComponent implements OnInit {
    itemsInOCTableColumnsText: any;
    defaultTableColumnsObj: any = this.modalData.defaultTableColumnsObj;
    currentTableColumns: any = this.modalData.currentTableColumns; // from the table
    currentColumnsObj: any = {}; // for the form

    constructor(
        private _sharedService: SharedService,
        private _actionModalService: ActionModalsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialogRef: MatDialogRef<ItemsInOcTableColumnsFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.itemsInOCTableColumnsText = IIOCTCFLanguages[cl];
    }

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentColumnsObj = JSON.parse(JSON.stringify(this.currentTableColumns));
    }

    saveColumns() {
        if (this._sharedService.isObjectEquivalent(this.currentColumnsObj, this.currentTableColumns)) {
            this.closeModal();
            return;
        }
        this._columnsNamesService.updateCustomColumnsNames(this.currentColumnsObj, "items_in_offers").subscribe(data => {
            if (data) {
                let title = this._columnsNamesService.formatSuccessData(data);
                let text = this.itemsInOCTableColumnsText.alertColumnsUpdatedSuccessfully;
                this._actionModalService.buildSuccessModal(title, text);
                this._columnsNamesService.buildCustomTableColumnsObject("items_in_offers", this.defaultTableColumnsObj);
                this.closeModal();
            }
        });
    }
    closeModal() {
        this.dialogRef.close();
    }
}

class IIOCTCFLanguages {
    static en = {
        modalTitle: "Personalize table columns",
        alertColumnsUpdatedSuccessfully: "Columns updated successfully",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        modalTitle: "Tabellenspalten personalisieren",
        alertColumnsUpdatedSuccessfully: "Reie erfolgreich aktualiziert",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };
    static es = {
        modalTitle: "Personalizar Columnas",
        alertColumnsUpdatedSuccessfully: "Columnas actualizadas",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
