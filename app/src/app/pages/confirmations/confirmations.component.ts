import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";
import { ConfirmationsService } from "../../shared/services/confirmations.service";
import { InternalDocumentsService } from "../../shared/services/internal-documents.service";

@Component({
    selector: "app-confirmations",
    templateUrl: "./confirmations.component.html",
    styleUrls: ["./confirmations.component.scss"]
})
export class ConfirmationsComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activeRoute.snapshot["_urlSegment"].segments[1].path;
    openOrdersConfirmedView = this.componentUrl === "open-orders-confirmed" ? true : false;
    zeroOC: boolean; // use to desable header menu

    ordersConfirmed = [];
    selectedOrderConfirmation = {};
    oc_id;
    // subsriptions
    orderConfirmationsList$;
    oCToPushOrUpdate$;

    constructor(
        private activeRoute: ActivatedRoute,
        private _idocsService: InternalDocumentsService,
        private router: Router,
        private _ordersConfirmationsService: ConfirmationsService
    ) {
        this.activeRoute.params.pipe(map(params => params["oc_id"])).subscribe(oc_id => {
            this.oc_id = oc_id;
            this.setSelectedOrderConfirmation();
        });
        this.orderConfirmationsList$ = this._ordersConfirmationsService.currentOrderConfirmationsList.subscribe(data => {
            this.ordersConfirmed = data;
            this.setSelectedOrderConfirmation();
        });
        this.oCToPushOrUpdate$ = this._ordersConfirmationsService.oCToPushOrUpdate.subscribe(data => {
            let order_confirmation_number = data.order_confirmation_number;
            if (!order_confirmation_number) return; // case data = {} an initialization
            // to find inex no use id because in this case the document_id changes and just the oc number will be added
            let index = this.ordersConfirmed.findIndex(obj => obj["order_confirmation_number"] == order_confirmation_number);
            if (index == -1) {
                // oc was created
                const oc_id = data.internal_document_id;
                this.ordersConfirmed.push(data);
                this.router.navigate(["app/" + this.componentUrl, oc_id]);
            } else {
                // oc was updated
                this.ordersConfirmed.splice(index, 1, data);
                this.selectedOrderConfirmation = data;
                this._idocsService.setCurrentDocument(this.selectedOrderConfirmation); // to update total cost model
                this._idocsService.calculateDocumentTotalPriceModel();
            }
        });
    }

    ngOnInit() {
        this.openOrdersConfirmedView ? this._ordersConfirmationsService.getOrdersConfirmations(2) : this._ordersConfirmationsService.getOrdersConfirmations(3);
    }

    ngOnDestroy() {
        this.orderConfirmationsList$.unsubscribe();
        this.oCToPushOrUpdate$.unsubscribe();
        this._ordersConfirmationsService.updatedOrderConfirmationsList([]);
    }

    setSelectedOrderConfirmation() {
        this.ordersConfirmed.length > 0 ? (this.zeroOC = false) : (this.zeroOC = true);
        if (this.ordersConfirmed.length > 0) {
            let found = this.ordersConfirmed.find(obj => obj["internal_document_id"] == this.oc_id);
            this.selectedOrderConfirmation = found ? found : this.ordersConfirmed[0];
            this.router.navigate(["app/" + this.componentUrl, this.selectedOrderConfirmation["internal_document_id"]]);
            this._idocsService.setCurrentDocument(this.selectedOrderConfirmation);
            window.scroll(0, 0);
        } else {
            this.selectedOrderConfirmation = {};
            this._idocsService.setCurrentDocument(this.selectedOrderConfirmation);
        }
    }
}
