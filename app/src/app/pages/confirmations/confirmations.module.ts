import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { ConfirmationsComponent } from './confirmations.component';
import { OrdersConfirmationListComponent } from './orders-confirmation-list/orders-confirmation-list.component';
import { OrderConfirmedHeaderComponent } from './order-confirmed-header/order-confirmed-header.component';
import { ItemsInOrderConfirmationTableComponent } from './items-in-order-confirmation-table/items-in-order-confirmation-table.component';
import { OrderConfirmationFormComponent } from './order-confirmation-form/order-confirmation-form.component';
import { SearchItemsToInsertInOcComponent } from './search-items-to-insert-in-oc/search-items-to-insert-in-oc.component';
import { ItemsInOcFormComponent } from './items-in-oc-form/items-in-oc-form.component';
import { ItemsInOcTableColumnsFormComponent } from './items-in-oc-table-columns-form/items-in-oc-table-columns-form.component';
import { ImportExcelIntoOcComponent } from './import-excel-into-oc/import-excel-into-oc.component';
import { ItemInOcActivitiesDetailsModalComponent } from './item-in-oc-activities-details-modal/item-in-oc-activities-details-modal.component';
import { SearchGroupsToInsertInOcComponent } from './search-groups-to-insert-in-oc/search-groups-to-insert-in-oc.component';
// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatIconModule
} from '@angular/material';
import { DirectivesModule } from '../../shared/directives/directives.module';

const CONFIRMATIONS_ROUTES: Routes = [
	{ path: '', component: ConfirmationsComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(CONFIRMATIONS_ROUTES),
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatIconModule,
		PipesModule,
		DirectivesModule
	],
	declarations: [
		ConfirmationsComponent,
		OrdersConfirmationListComponent,
		OrderConfirmedHeaderComponent,
		ItemsInOrderConfirmationTableComponent,
		OrderConfirmationFormComponent,
		SearchItemsToInsertInOcComponent,
		ItemsInOcFormComponent,
		ItemsInOcTableColumnsFormComponent,
		ImportExcelIntoOcComponent,
		ItemInOcActivitiesDetailsModalComponent,
		SearchGroupsToInsertInOcComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		SearchItemsToInsertInOcComponent,
		ItemsInOcFormComponent,
		ItemsInOcTableColumnsFormComponent,
		ImportExcelIntoOcComponent,
		ItemInOcActivitiesDetailsModalComponent,
		SearchGroupsToInsertInOcComponent
	]
})
export class ConfirmationsModule { }
