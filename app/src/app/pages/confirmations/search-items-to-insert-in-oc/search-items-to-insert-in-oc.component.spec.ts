import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInOcComponent } from './search-items-to-insert-in-oc.component';

describe('SearchItemsToInsertInOcComponent', () => {
  let component: SearchItemsToInsertInOcComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInOcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInOcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInOcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
