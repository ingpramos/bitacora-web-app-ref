import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchGroupsToInsertInOcComponent } from './search-groups-to-insert-in-oc.component';

describe('SearchGroupsToInsertInOcComponent', () => {
  let component: SearchGroupsToInsertInOcComponent;
  let fixture: ComponentFixture<SearchGroupsToInsertInOcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchGroupsToInsertInOcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchGroupsToInsertInOcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
