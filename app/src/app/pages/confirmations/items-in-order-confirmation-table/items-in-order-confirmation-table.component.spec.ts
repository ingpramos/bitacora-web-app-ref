import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOrderConfirmationTableComponent } from './items-in-order-confirmation-table.component';

describe('ItemsInOrderConfirmationTableComponent', () => {
  let component: ItemsInOrderConfirmationTableComponent;
  let fixture: ComponentFixture<ItemsInOrderConfirmationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOrderConfirmationTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOrderConfirmationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
