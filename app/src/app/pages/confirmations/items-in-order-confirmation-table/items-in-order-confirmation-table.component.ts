import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ItemsInOcFormComponent } from "../items-in-oc-form/items-in-oc-form.component";
import { ItemsInOcTableColumnsFormComponent } from "../items-in-oc-table-columns-form/items-in-oc-table-columns-form.component";
import { MatDialog } from "@angular/material/dialog";
import { ItemInOcActivitiesDetailsModalComponent } from "../item-in-oc-activities-details-modal/item-in-oc-activities-details-modal.component";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-items-in-order-confirmation-table",
    templateUrl: "./items-in-order-confirmation-table.component.html",
    styleUrls: ["./items-in-order-confirmation-table.component.scss"]
})

export class ItemsInOrderConfirmationTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() currentOrderConfirmation: any;
    companyCurrency: string = "";
    currentLanguage:string;
    items;
    itemsInOrderConfirmationText: any;
    alerts: any;
    tableColumns: any = {};
    defaultTableColumnsObj: any = {};
    ocTotalFooter: any;
    // subscriptions
    itemsList$;
    columnsNames$;
    totalModel$;

    constructor(
        public _idocsService: InternalDocumentsService,
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.companyCurrency = this._sharedService.currency;
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInOrderConfirmationText = IIOCTLanguages[this.currentLanguage];
        this.alerts = IIOCTLanguages[this.currentLanguage].alerts;
        this.defaultTableColumnsObj = this.itemsInOrderConfirmationText.columns;
        
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_offers", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => this.tableColumns = data);
        this.itemsList$ = this._idocsService.currentItemsList.subscribe(data => {
            this.items = data;
            this._idocsService.setItemsOfCurrentDocument(this.items);
            this._idocsService.calculateDocumentTotalPriceModel();
        });
        this.totalModel$ = this._idocsService.documentTotalPriceModelObs.subscribe(data => {
            this.ocTotalFooter = data;
            this.currentOrderConfirmation.subTotal = this.ocTotalFooter.subTotal;
            this.currentOrderConfirmation.vat = this.ocTotalFooter.vat;
            this.currentOrderConfirmation.total = this.ocTotalFooter.total;
            this._sharedService.stopLoading();
        });
    }

    ngOnChanges(changes) {
        let oc_id = changes.currentOrderConfirmation.currentValue.internal_document_id;
        if (oc_id) {
            this._idocsService.getItemsFromInternalDocument(oc_id);
            this._sharedService.startLoading();
        }
        if (!oc_id) {
            this._idocsService.updateItemsList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
        this.totalModel$.unsubscribe();
    }

    editItemInOC(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemsInOcFormComponent, { width: "1000px", data: initialState });
    }

    editTableColumns() {
        let initialState = {
            defaultTableColumnsObj: this.defaultTableColumnsObj,
            currentTableColumns: this.tableColumns
        };
        const dialogRef = this.dialog.open(ItemsInOcTableColumnsFormComponent, { width: "1000px", data: initialState });
    }

    openItemDetails(item) {
        let initialState = { currentItem: item, currentOrderConfirmation: this.currentOrderConfirmation };
        const dialogRef = this.dialog.open(ItemInOcActivitiesDetailsModalComponent, { width: "1200px", data: initialState });
    }

    deleteItem(item, index) {
        item["item_type"] == "item" ? this.deleteItemFromOC(item, index) : this.deleteAssemblyFromOC(item, index);
    }

    deleteItemFromOC(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromOC;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let iiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteItemFromInternalDocument(iiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    deleteAssemblyFromOC(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromOC;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let aiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteAssemblyFromInternalDocument(aiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class IIOCTLanguages {
    constructor() {}
    user_selected_language = "en";

    static en = {
        title: "Order Confirmation Details",
        totalWithoutVAT: "Total without VAT",
        vatFrom: "VAT from ",
        totalWithVAT: "Total with VAT",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Item Code",
            item_name: "Item Description",
            item_amount: "Amount",
            item_sell_price: "Price(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOC: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns"
    };

    static de = {
        title: "Auftragsbestätigungsdetails",
        totalWithoutVAT: "Total exkl. MwSt.",
        vatFrom: "MwSt von",
        totalWithVAT: "Total inkl. MwSt.",
        columns: {
            item_position: "Pos",
            item_type: "Typ",
            item_code: "Artikel Code",
            item_name: "Artikelbezeichnung",
            item_amount: "Menge",
            item_sell_price: "Preis(E)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOC: "Sind Sie sicher das Sie dieses Artikel löschen möchte?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle suchen",
        editTableColumns: "Spaltennamen bearbeiten"
    };

    static es = {
        title: "Detalles de Oferta",
        totalWithoutVAT: "Total sin IVA",
        vatFrom: "IVA de ",
        totalWithVAT: "Total Con IVA",
        columns: {
            item_position: "Pos",
            item_type: "Tipo",
            item_code: "Código del Item",
            item_name: "Description",
            item_amount: "Cantidad",
            item_sell_price: "Precio(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOC: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item eliminado exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Columnas"
    };
}
