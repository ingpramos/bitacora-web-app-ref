import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExcelIntoOcComponent } from './import-excel-into-oc.component';

describe('ImportExcelIntoOcComponent', () => {
  let component: ImportExcelIntoOcComponent;
  let fixture: ComponentFixture<ImportExcelIntoOcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportExcelIntoOcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExcelIntoOcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
