import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { OffersService } from "../../../shared/services/offers.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-import-excel-into-oc",
    templateUrl: "./import-excel-into-oc.component.html",
    styleUrls: ["./import-excel-into-oc.component.scss"]
})
export class ImportExcelIntoOcComponent implements OnInit {

    oCHeaderText: any = this.modalData.oCHeaderText;
    currentOrderConfirmation: any = this.modalData.currentOrderConfirmation;
    itemsInOC: number = this.modalData.itemsInOC;
    // tabs
    selectedTab = 0;
    showJustSelectedItems: boolean = true;
    tableText: any;
    alerts: any;
    tableColumns: any;
    fileInformation: any = this.modalData.fileInformation;
    itemsFromExcel: any = this.modalData.itemsFromExcel;
    items:any = [];
    selectedItems: any = [];
    itemsAlreadyInList: any = [];
    itemsNotFoundInDB: any = [];
    allChecked: boolean = true;
    processingRequest: boolean = false;
    user_id;
    constructor(
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _excelService: ExcelService,
        private _itemsService: ItemsService,
        private _columnsNamesService: ColumnsNamesService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<ImportExcelIntoOcComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this._sharedService.sessionInfo.subscribe(data => (this.user_id = data.user_id));
    }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.tableText = IEIOCLanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
        if (this.itemsFromExcel.length > 0) {
            this._excelService.labelItemsAlreadyInList(this._idocsService.getItemsOfCurrentDocument("view"), this.itemsFromExcel);
            this.itemsAlreadyInList = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === true);
            let preSelectedItems: any[] = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === false);
            if (preSelectedItems.length > 0) { // check in db if items exists or not
                let arrayOfCodes = preSelectedItems.map((obj: any) => obj["item_code"]);
                this._itemsService.checkIfItemsExists(arrayOfCodes).subscribe(itemsFound => {
                    this._excelService.labelItemsAlreadyInDB(itemsFound, preSelectedItems);
                    this.itemsNotFoundInDB = preSelectedItems.filter((obj: any) => obj.is_already_in_db === false);
                    this.selectedItems = preSelectedItems.filter((obj: any) => obj.is_already_in_db === true);
                    this.items = this.selectedItems;
                });
            }
        }

        this._columnsNamesService.formatColumnsOfImportTable(this.tableColumns);
    }

    applyChangesToList(allChecked) {
        this.itemsFromExcel.forEach((obj: any) => {
            if (obj.is_already_in_list === false && obj.is_already_in_db === true) {
                obj.is_checked = allChecked;
            }
        });
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedItems = this.itemsFromExcel.filter((obj: any) => {
            return obj.is_checked === true && obj.is_already_in_db === true && obj.is_already_in_list === false;
        });
    }

    saveItems() {
        this.processingRequest = true;
        let offer_id = this.currentOrderConfirmation.internal_document_id;
        let formattedItems = this.formatItemsToInsert(this.selectedItems, offer_id);
        this._idocsService.insertItemsInInternalDocument(formattedItems).subscribe(
            data => {
                if (data) {
                    this.processingRequest = false;
                    this._idocsService.getItemsFromInternalDocument(offer_id);
                    this.closeModal();
                    let title = this.currentOrderConfirmation.order_confirmation_number;
                    let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertItemInsertedError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatItemsToInsert(collection, id) {
        collection.forEach((obj: any) => {
            obj["internal_document_id"] = id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(obj["item_sell_price_to_show"]);
            obj["user_id"] = this.user_id;
        });
        return collection;
    }

    closeModal() {
        this.dialogRef.close();
    }
    showSelectedItems() {
        this.items = this.selectedItems;
        this.showJustSelectedItems = true;
    }
    showItemsAlreadyInList() {
        this.items = this.itemsAlreadyInList;
        this.showJustSelectedItems = false;
    }
    showItemsNotFoundInDB() {
        this.items = this.itemsNotFoundInDB;
        this.showJustSelectedItems = false;
    }
    // Tabs related functions
    updateTab(index) {
        // update displayed view according the selected tab
        switch (index) {
            case 0:
                this.showSelectedItems();
                break;
            case 1:
                this.showItemsAlreadyInList();
                break;
            case 2:
                this.showItemsNotFoundInDB();
                break;
        }
    }
}

class IEIOCLanguages {
    static en = {
        fileName: "File Name",
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name",
            item_sell_price: "Price"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemInsertedError: "An error ocurred"
        },
        modalTitle: "Insert items in order confirmation",
        itemsFound: "Items Found",
        itemsAlreadyInList: "Items Already in Offer",
        itemsSelected: "Selected Items",
        checkColumnsNames: "No columns where recognized, please check the name of the header columns in your document, the columns marked with * are required.",
        savingProcess: "Saving ...",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        fileName: "Dateiname",
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name",
            item_sell_price: "Preis"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemInsertedError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Auftragsbestätigung einfügen",
        itemsFound: "Artikel gefunden",
        itemsAlreadyInList: "bereits vorhandene Artikel",
        itemsSelected: "ausgewählte Artikel",
        checkColumnsNames:
            "Es wurden keine Spalten erkannt. Bitte überprüfen Sie den Namen der Kopfzeilenspalten in Ihrem Dokument. Die mit * gekennzeichneten Spalten sind erforderlich.",
        savingProcess: "Wird gespeichert ...",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        fileName: "Nombre del Archivo",
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre",
            item_sell_price: "Precio"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemInsertedError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar items en orden de trabajo",
        itemsFound: "Items Encontrados",
        itemsAlreadyInList: "Articulos Repetidos",
        itemsSelected: "Items Seleccionados",
        checkColumnsNames:
            "no se reconocieron columnas, verifique el nombre de las columnas del encabezado en su documento, las columnas marcadas con * son obligatorias.",
        savingProcess: "Guardando ...",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
