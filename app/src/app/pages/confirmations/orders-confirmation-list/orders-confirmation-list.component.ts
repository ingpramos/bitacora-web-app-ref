import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ConfirmationsService } from "../../../shared/services/confirmations.service";

@Component({
    selector: "app-orders-confirmation-list",
    templateUrl: "./orders-confirmation-list.component.html",
    styleUrls: ["./orders-confirmation-list.component.scss"]
})
export class OrdersConfirmationListComponent implements OnInit {
    // check component url
    componentUrl = this.activeRoute.snapshot["_urlSegment"].segments[1].path;
    openOrdersConfirmedView = this.componentUrl === "open-orders-confirmed" ? true : false;

    @Input() ordersConfirmed;

    orderConfirmationToHandle = {};
    ordersConfirmedListText: any;
    alerts: any;
    newOrderConfirmation: boolean = false;
    editOrderConfirmation: boolean = false;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private activatedRoute: ActivatedRoute;
    private paramMap$;
    private oc_id;

    constructor(
        activatedRoute: ActivatedRoute,
        private router: Router,
        private _ordersConfirmationsService: ConfirmationsService,
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService,
        private activeRoute: ActivatedRoute
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.ordersConfirmedListText = OCLCLanguage[cl];
        this.alerts = this.ordersConfirmedListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.oc_id = +paramMap.get("oc_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForOrdersConfirmed();
        });
    }

    ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchOrdersConfirmed() {
        this.subject.next(this.searchPattern);
    }

    searchForOrdersConfirmed() {
        let offer_state = this.openOrdersConfirmedView ? 2 : 3;
        this._ordersConfirmationsService.getOrdersConfirmationsByPattern(offer_state, this.searchPattern);
    }

    closeSearch() {
        let offer_state = this.openOrdersConfirmedView ? 2 : 3;
        this.searchPattern = "";
        this._ordersConfirmationsService.getOrdersConfirmations(offer_state);
        this.showSearch = false;
    }

    startSearch() {
        this.editOrderConfirmation = false;
        this.newOrderConfirmation = false;
        this.showSearch = true;
    }

    startNewOrderConfirmation() {
        this.editOrderConfirmation = false;
        this.newOrderConfirmation = true;
        this.orderConfirmationToHandle = {};
    }

    startEditOrderConfirmation(orderConfirmation) {
        this.orderConfirmationToHandle = JSON.parse(JSON.stringify(orderConfirmation));
        this.newOrderConfirmation = false;
        this.editOrderConfirmation = true;
    }

    deleteOrderConfirmation(oc, index) {
        let title = oc.order_confirmation_number;
        let text = this.alerts.confirmDeleteOC;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let oc_id = oc.internal_document_id;
                this._ordersConfirmationsService.deleteOrderConfirmation(oc_id).subscribe(
                    data => {
                        if (data) {
                            let title = oc.order_confirmation_number;
                            let text = this.alerts.alertOCDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.ordersConfirmed.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    () => {
                        let text = this.alerts.alertOCDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editOrderConfirmation = false;
        this.newOrderConfirmation = false;
        this.orderConfirmationToHandle = {};
    }
}

class OCLCLanguage {
    static en = {
        alerts: {
            confirmDeleteOC: "Are you sure you want to delete this order confirmation ?",
            alertOCDeletedSuccessfully: "Order confirmation deleted successfully",
            alertOCDeleteError: "An error ocurred"
        },
        newOrderConfirmation: "New Order Confirmation",
        searchOrdersConfirmed: "Search Orders Confirmed",
        openOrdersConfirmed: "Confirmed Orders",
        closedOrdersConfirmed: "Closed Confirmed Orders",
        editOrderConfirmation: "Edit Order Confirmation",
        deleteOrderConfirmation: "Delete Order Confirmation",
        creationDate: "Created at",
        deliveryDate: "Delivery"
    };

    static de = {
        alerts: {
            confirmDeleteOC: "Wollen Sie dieses Auftragsbestätigung löschen ?",
            alertOCDeletedSuccessfully: "Auftragsbestätigung gelöscht",
            alertOCDeleteError: "Es ist ein Fehler aufgetreten"
        },
        newOrderConfirmation: "New Auftragsbestätigung",
        searchOrdersConfirmed: "Auftragsbestätigungen suchen",
        openOrdersConfirmed: "Auftragsbestätigungen",
        closedOrdersConfirmed: "Geschlossene AB",
        editOrderConfirmation: "AB bearbeiten",
        deleteOrderConfirmation: "AB löschen",
        creationDate: "Erstellt",
        deliveryDate: "Lifertermin"
    };

    static es = {
        alerts: {
            confirmDeleteOC: "Esta seguro que deseas borrar la siguiente Confirmación de Orden?",
            alertOCDeletedSuccessfully: "Confirmacion de Orden eliminada exitosamente",
            alertOCDeleteError: "Ha ocurrido un error"
        },

        newOrderConfirmation: "Nueva Confirmación De Pedido",
        searchOrdersConfirmed: "Buscar Pedido",
        openOrdersConfirmed: "Pedidos Abiertos",
        closedOrdersConfirmed: "Pedios Facturados",
        editOrderConfirmation: "Editar Confirmacion",
        deleteOrderConfirmation: "Borrar Confirmacion",
        creationDate: "Creación",
        deliveryDate: "Entrega"
    };
}
