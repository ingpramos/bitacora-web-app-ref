import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersConfirmationListComponent } from './orders-confirmation-list.component';

describe('OrdersConfirmationListComponent', () => {
  let component: OrdersConfirmationListComponent;
  let fixture: ComponentFixture<OrdersConfirmationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersConfirmationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersConfirmationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
