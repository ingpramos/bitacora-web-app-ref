import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOcFormComponent } from './items-in-oc-form.component';

describe('ItemsInOcFormComponent', () => {
  let component: ItemsInOcFormComponent;
  let fixture: ComponentFixture<ItemsInOcFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOcFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOcFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
