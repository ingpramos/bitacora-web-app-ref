import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';
import { ConfirmationsService } from '../../../shared/services/confirmations.service';
import { ProjectsService } from '../../../shared/services/projects.service';
@Component({
	selector: 'app-item-in-oc-activities-details-modal',
	templateUrl: './item-in-oc-activities-details-modal.component.html',
	styleUrls: ['./item-in-oc-activities-details-modal.component.scss']
})
export class ItemInOcActivitiesDetailsModalComponent implements OnInit {

	modalText;
	tableHeaderText;
	currentOrderConfirmation = this.modalData.currentOrderConfirmation;
	currentItem = this.modalData.currentItem;
	activitiesText;
	activitiesResume = [];
	totalActivitiesCost;
	companyCurrency : string = '';

	constructor(
		private _sharedService: SharedService,
		private _ordersConfirmationsService: ConfirmationsService,
		private _projectsService : ProjectsService,
		public dialogRef : MatDialogRef<ItemInOcActivitiesDetailsModalComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) { }

	ngOnInit() {
		this.companyCurrency = this._sharedService.currency;
		let cl = this._sharedService.user_selected_language;
		this.modalText = IIOCDMCLanguages[cl];
		this.tableHeaderText = IIOCDMCLanguages[cl].header;
		let iiid_id = this .currentItem.item_in_internal_document_id;
		this._ordersConfirmationsService.getActivitiesFromItemInOC(iiid_id).subscribe(data => {
			let costModel = this._projectsService.calculateTotalActivitiesCostModel(data);
			this.activitiesResume = costModel['activitiesResume'];
			this.totalActivitiesCost = costModel['totalActivitiesCost'];
		});
	}

}

class IIOCDMCLanguages {
	static en = {
		"modalTitleItem": "Item details",
		"itemName": "Item name",
		"assemblyName": "Group name",
		"itemCode": "Item Code",
		"assemblyNumber": "Group number",
		"itemAmount": "Amount",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Position",
			"activity_name": "Activity name",
			"activity_duration": "Duration",
			"hour_price": "Hour price",
			"activity_total_cost": "Cost",
			"activity_percentage": "Percentage",
			"activity_date": "Date",
			"activity_person": "Employee"
		},
		"activities": {
			"book": "Book",
			"finish": "Finish",
			"assembly": "Assembly"
		}
	};

	static de = {
		"modalTitleItem": "Artikeldetails",
		"itemName": "Artikelname",
		"assemblyName": "Baugruppe name",
		"itemCode": "Artikelcode",
		"assemblyNumber": "Baugruppe nummer",
		"itemAmount": "Menge",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Position",
			"activity_name": "Aktivität",
			"activity_duration": "Zeitdauer",
			"hour_price": "Stundenpreis",
			"activity_total_cost": "kosten",
			"activity_percentage": "Prozente",
			"activity_date": "Datum",
			"activity_person": "Mitarbeiter"
		},
		"activities": {
			"book": "Buchen",
			"finish": "Fertigen",
			"assembly": "Montieren"
		}
	};

	static es = {
		"modalTitleItem": "Detalles de Articulo",
		"itemName": "Nombre",
		"assemblyName": "Nombre del grupo",
		"itemCode": "Código",
		"assemblyNumber": "Nr. grupo",
		"itemAmount": "Cantidad",
		"assemblyItems": "Partes ensambladas",
		"completedPorcentage": "Avance",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Posición",
			"activity_name": "Actividad",
			"activity_duration": "Duración",
			"hour_price": "Precio hora",
			"activity_total_cost": "Costo",
			"activity_percentage": "Porcentage",
			"activity_date": "Fecha",
			"activity_person": "Empleado"
		},
		"activities": {
			"book": "Reservar",
			"finish": "Terminar",
			"assembly": "Ensamblar"
		}
	};
}
