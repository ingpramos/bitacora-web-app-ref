import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInOcActivitiesDetailsModalComponent } from './item-in-oc-activities-details-modal.component';

describe('ItemInOcActivitiesDetailsModalComponent', () => {
  let component: ItemInOcActivitiesDetailsModalComponent;
  let fixture: ComponentFixture<ItemInOcActivitiesDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInOcActivitiesDetailsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInOcActivitiesDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
