import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderConfirmationFormComponent } from './order-confirmation-form.component';

describe('OrderConfirmationFormComponent', () => {
  let component: OrderConfirmationFormComponent;
  let fixture: ComponentFixture<OrderConfirmationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderConfirmationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderConfirmationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
