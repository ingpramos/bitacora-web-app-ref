import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { OffersService } from "../../../shared/services/offers.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ConfirmationsService } from "../../../shared/services/confirmations.service";
import { DateAdapter } from "@angular/material/core";

@Component({
    selector: "app-order-confirmation-form",
    templateUrl: "./order-confirmation-form.component.html",
    styleUrls: ["./order-confirmation-form.component.scss"]
})

export class OrderConfirmationFormComponent implements OnInit {
    // component's Inputs
    @Input() editOrderConfirmation;
    @Input() newOrderConfirmation;
    @Input() currentOrderConfirmation: any;
    // component's output
    @Output() useForm = new EventEmitter<boolean>();
    // component's Properties
    sessionInfo: any;
    orderConfirmationFormText: any;
    alerts: any;
    currentOrderConfirmationCopy;
    openOffers: any = {};
    readOnlyInput: boolean = false;
    showCurrentOfferNumberWhileEdit: boolean = false;
    clients: any = {}; // a formatted object to play nice with the select
    clientContacts: any = {}; // a formatted object to play nice with the select
    paymentMethods: any = {};
    paymentConditions: any = {};
    deliveryMethods: any = {};
    deliveryConditions: any = {};
    vats = [];

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _clientsService: ClientsService,
        private _confirmationsService: ConfirmationsService,
        private _offersService: OffersService,
        private _companyService: CompanyService,
        private adapter: DateAdapter<any>
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        // change the date picker adapter to current language
        this.adapter.setLocale(cl);
        this.orderConfirmationFormText = OCFCLanguage[cl];
        this.alerts = this.orderConfirmationFormText.alerts;
        if (this.editOrderConfirmation) {
            this.showCurrentOfferNumberWhileEdit = true;
            this.currentOrderConfirmationCopy = JSON.parse(JSON.stringify(this.currentOrderConfirmation));
        }
        this.callOpenOffers();
        this._clientsService.getFormattedClients().subscribe(data => {
            this.clients = data;
            if (this.editOrderConfirmation) this.callClientContacts();
            if (this.newOrderConfirmation) this.currentOrderConfirmation.vat_porcentage = 19;
        });
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.vats = this.sessionInfo.company_vats;
            if (this.newOrderConfirmation) this.callNextOrderConfirmationNumber();
            this.paymentMethods = this._companyService.formatPaymentMethods(this.sessionInfo.payment_methods);
            this.paymentConditions = this._companyService.formatPaymentConditions(this.sessionInfo.payment_conditions);
            this.deliveryMethods = this._companyService.formatDeliveryMethods(this.sessionInfo.delivery_methods);
            this.deliveryConditions = this._companyService.formatDeliveryConditions(this.sessionInfo.delivery_conditions);
        });
    }

    saveOrderConfirmation() {
        this.editOrderConfirmation ? this.updateOrderConfirmation() : this.createOrderConfirmation();
    }

    createOrderConfirmation() {
        this.currentOrderConfirmation["offer_state"] = 2;
        this.findIds();
        this._confirmationsService.createOrderConfirmation(this.currentOrderConfirmation).subscribe(
            data => {
                if (data) {
                    this._confirmationsService.passOCToPushOrUpdate(data);
                    let title = this.currentOrderConfirmation.order_confirmation_number;
                    let text = this.alerts.alertOCCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let text = this.alerts.alertOCCreateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    updateOrderConfirmation() {
        this.findIds();
        this.currentOrderConfirmation["previous_id"] = this.currentOrderConfirmationCopy.internal_document_id;
        this.currentOrderConfirmation["new_id"] = this.currentOrderConfirmation.internal_document_id;
        this._confirmationsService.updateOrderConfirmation(this.currentOrderConfirmation).subscribe(
            data => {
                if (data) {
                    this._confirmationsService.passOCToPushOrUpdate(data);
                    let title = this.currentOrderConfirmation.order_confirmation_number;
                    let text = this.alerts.alertOCUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let text = this.alerts.alertOCUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    callClientContacts() {
        let client = this.currentOrderConfirmation.client_name;
        let client_id = this.clients["getClient_id"](client);
        this.currentOrderConfirmation.client_id = client_id;
        this._clientsService.getFormattedContacts(client_id).subscribe(data => {
            this.clientContacts = data;
        });
    }

    callNextOrderConfirmationNumber() {
        this._confirmationsService.getCurrentOCNumber().subscribe(data => {
            this.currentOrderConfirmation.order_confirmation_number = `${data.document_prefix}${data.document_current_number + 1}`;
        });
    }

    callOpenOffers() {
        this._offersService.getFormattedOffers(1).subscribe(data => {
            this.openOffers = data;
        });
    }

    setDocument(event) {
        this.showCurrentOfferNumberWhileEdit = false;
        let docId = this.openOffers["get_internal_document_id"](event.value);
        let internalDocument = this.openOffers["original_data"].find(obj => obj["internal_document_id"] == docId);
        this.setProperties(internalDocument);
    }

    setProperties(internalDocument) {
        this.currentOrderConfirmation["internal_document_id"] = internalDocument["internal_document_id"];
        this.currentOrderConfirmation["client_name"] = internalDocument["client_name"];
        this.currentOrderConfirmation["contact"] = internalDocument["contact"];
        this.currentOrderConfirmation["payment_method_name"] = internalDocument["payment_method_name"];
        this.currentOrderConfirmation["payment_condition_description"] = internalDocument["payment_condition_description"];
        this.currentOrderConfirmation["delivery_method_name"] = internalDocument["delivery_method_name"];
        this.currentOrderConfirmation["delivery_condition_description"] = internalDocument["delivery_condition_description"];
        this.currentOrderConfirmation["offered_delivery_date"] = internalDocument["offered_delivery_date"];
        this.currentOrderConfirmation["vat_percentage"] = internalDocument["vat_percentage"];
        this.readOnlyInput = true;
    }

    findIds() {
        this.currentOrderConfirmation.client_contact_id = this.clientContacts["getCc_id"](this.currentOrderConfirmation.contact);
        this.currentOrderConfirmation.payment_method_id = this.paymentMethods["getPaymentMethod_id"](this.currentOrderConfirmation.payment_method_name);
        this.currentOrderConfirmation.payment_condition_id = this.paymentConditions["getPaymentCondition_id"](
            this.currentOrderConfirmation.payment_condition_description
        );
        this.currentOrderConfirmation.delivery_method_id = this.deliveryMethods["getDeliveryMethod_id"](this.currentOrderConfirmation.delivery_method_name);
        this.currentOrderConfirmation.delivery_condition_id = this.deliveryConditions["getDeliveryCondition_id"](
            this.currentOrderConfirmation.delivery_condition_description
        );
    }

    forgetActions() {
        if (this.editOrderConfirmation) {
            this.currentOrderConfirmation["offer_number"] = this.currentOrderConfirmationCopy["offer_number"];
            this.setProperties(this.currentOrderConfirmationCopy);
        } else {
            this.currentOrderConfirmation = {};
        }
        this.useForm.emit(false);
    }
}

class OCFCLanguage {
    static en = {
        alerts: {
            alertOCUpdatedSuccessfully: "Order Confirmation updated successfully",
            alertOCUpdateError: "An error ocurred",
            alertOCCreatedSuccessfully: "Order Confirmation created successfully",
            alertOCCreateError: "An error ocurred"
        },
        orderConfirmationNumber: "Order Confirmation Number",
        currentOfferNumber: "Current Offer Number",
        offerNumber: "Offer Number",
        client: "Client",
        clientContact: "Contact",
        paymentConditions: "Payment Conditions",
        paymentMethod: "Payment Method",
        deliveryMethod: "Way of delivery",
        deliveryConditions: "Delivery Conditions",
        offerVATValue: "VAT %",
        offerStatus: "Order Status",
        deliveryDate: "Delivery Date",
        requiredField: "Required Field",
        offerStatusPending: "PENDING",
        offerStatusOrdered: "ORDERED",
        offerStatusPaid: "PAID",
        offerStatusDelivered: "DELIVERED",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertOCUpdatedSuccessfully: "Auftragsbestätigung erfolgreich aktualiziert",
            alertOCUpdateError: "Es ist ein Fehler aufgetreten",
            alertOCCreatedSuccessfully: "Auftragsbestätigung erfolgreich erstelt",
            alertOCCreateError: "Es ist ein Fehler aufgetreten"
        },
        orderConfirmationNumber: "AB Nummer",
        currentOfferNumber: "Aktuelle Angebotsnummer",
        offerNumber: "Angebotsnummer",
        client: "Kunde",
        clientContact: "Kontak",
        paymentConditions: "Zahlungsbedingungen",
        paymentMethod: "Zahlungsart",
        deliveryMethod: "Liefertart",
        deliveryConditions: "Lieferbedingungen",
        offerVATValue: "MwSt. %",
        offerStatus: "Bestellung Status",
        deliveryDate: "Lifertermin",
        requiredField: "Pflichtfeld",
        offerStatusPending: "ANGEFRAGT",
        offerStatusOrdered: "BESTELLT",
        offerStatusPaid: "BETZAHLT",
        offerStatusDelivered: "GELIEFERT",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertOCUpdatedSuccessfully: "Orden de Trabajo actualizada exitosamente",
            alertOCUpdateError: "Ha ocurrido un error",
            alertOCCreatedSuccessfully: "Orden de trabajo creada existosamente",
            alertOCCreateError: "Ha ocurrido un error"
        },
        orderConfirmationNumber: "Número de Pedido",
        currentOfferNumber: "Número de cotización Actual",
        offerNumber: "Número de Cotización",
        client: "Cliente",
        clientContact: "Contacto",
        paymentConditions: "Condiciones de Pago",
        paymentMethod: "Metodo de Pago",
        deliveryMethod: "Método de Entrega",
        deliveryConditions: "Condiciones de entrega",
        offerVATValue: "IVA %",
        offerStatus: "Estado de la Orden",
        deliveryDate: "Fecha de Entrega",
        requiredField: "Campo Requerido",
        offerStatusPending: "COTIZADO",
        offerStatusOrdered: "CONFIRMADO",
        offerStatusPaid: "PAGADO",
        offerStatusDelivered: "ENTREGADO",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
