import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchGroupsToInsertInInvoiceComponent } from './search-groups-to-insert-in-invoice.component';

describe('SearchGroupsToInsertInInvoiceComponent', () => {
  let component: SearchGroupsToInsertInInvoiceComponent;
  let fixture: ComponentFixture<SearchGroupsToInsertInInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchGroupsToInsertInInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchGroupsToInsertInInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
