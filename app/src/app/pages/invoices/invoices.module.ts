import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { InvoicesComponent } from './invoices.component';
import { InvoicesListComponent } from './invoices-list/invoices-list.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoiceHeaderComponent } from './invoice-header/invoice-header.component';
import { ItemsInInvoiceTableComponent } from './items-in-invoice-table/items-in-invoice-table.component';
import { ItemInInvoiceFormComponent } from './item-in-invoice-form/item-in-invoice-form.component';
import { SearchItemsToInsertInInvoiceComponent } from './search-items-to-insert-in-invoice/search-items-to-insert-in-invoice.component';
import { ItemsInInvoiceTableColumnsFormComponent } from './items-in-invoice-table-columns-form/items-in-invoice-table-columns-form.component';
import { ImportExcelIntoInvoiceComponent } from './import-excel-into-invoice/import-excel-into-invoice.component';
import { SearchGroupsToInsertInInvoiceComponent } from './search-groups-to-insert-in-invoice/search-groups-to-insert-in-invoice.component';
// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatIconModule
} from '@angular/material';
import { DirectivesModule } from '../../shared/directives/directives.module';

const INVOICES_ROUTES: Routes = [
	{ path: '', component: InvoicesComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(INVOICES_ROUTES),
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatIconModule,
		PipesModule,
		DirectivesModule
	],
	declarations: [
		InvoicesComponent,
		InvoicesListComponent,
		InvoiceFormComponent,
		InvoiceHeaderComponent,
		ItemsInInvoiceTableComponent,
		ItemInInvoiceFormComponent,
		SearchItemsToInsertInInvoiceComponent,
		ItemsInInvoiceTableColumnsFormComponent,
		ImportExcelIntoInvoiceComponent,
		SearchGroupsToInsertInInvoiceComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		ItemInInvoiceFormComponent,
		SearchItemsToInsertInInvoiceComponent,
		ItemsInInvoiceTableColumnsFormComponent,
		ImportExcelIntoInvoiceComponent,
		ItemsInInvoiceTableColumnsFormComponent,
		SearchGroupsToInsertInInvoiceComponent
	]
})
export class InvoicesModule { }
