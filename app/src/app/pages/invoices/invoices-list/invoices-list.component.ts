import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { InvoicesService } from "../../../shared/services/invoices.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-invoices-list",
    templateUrl: "./invoices-list.component.html",
    styleUrls: ["./invoices-list.component.scss"]
})
export class InvoicesListComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activatedRoute.snapshot["_urlSegment"].segments[1].path;
    openInvoicesView = this.componentUrl === "open-invoices" ? true : false;

    @Input() invoices;
    invoicesListText: any;
    alerts: any;
    invoiceToHandle = {};
    newInvoice = false;
    editInvoice = false;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private paramMap$;
    private invoice_id;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _sharedService: SharedService,
        private _invoicesService: InvoicesService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.invoicesListText = ILCLanguages[cl];
        this.alerts = this.invoicesListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.invoice_id = +paramMap.get("invoice_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForInvoices();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchInvoices() {
        this.subject.next(this.searchPattern);
    }

    searchForInvoices() {
        let invoice_state = this.openInvoicesView ? 1 : 0;
        this._invoicesService.getInvoicesByPattern(invoice_state, this.searchPattern);
    }

    closeSearch() {
        let invoice_state = this.openInvoicesView ? 1 : 0;
        this.searchPattern = "";
        this._invoicesService.getInvoices(invoice_state);
        this.showSearch = false;
    }

    startSearch() {
        this.editInvoice = false;
        this.newInvoice = false;
        this.showSearch = true;
    }

    startEditInvoice(invoice) {
        this.newInvoice = false;
        this.invoiceToHandle = invoice;
        this.editInvoice = true;
    }

    startNewInvoice() {
        this.editInvoice = false;
        this.invoiceToHandle = {};
        this.newInvoice = true;
    }

    forgetActions() {
        this.editInvoice = false;
        this.newInvoice = false;
        this.invoiceToHandle = {};
    }

    deleteInvoice(invoice, index) {
        let title = invoice.invoice_number;
        let text = this.alerts.confirmDeleteInvoice;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let invoice_id = invoice.internal_document_id;
                this._invoicesService.deleteInvoice(invoice_id).subscribe(
                    data => {
                        if (data) {
                            let title = invoice.invoice_number;
                            let text = this.alerts.alertInvoiceDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.invoices.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    () => {
                        let text = this.alerts.alertInvoiceDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    closeInvoice(invoice) {
        let invoice_id = invoice.internal_document_id;
        this._invoicesService.closeInvoice(invoice_id).subscribe(
            data => {
                if (data) this.router.navigate(["app/closed-invoices", invoice_id]);
            },
            () => {
                let text = this.alerts.alertCloseInvoiceError;
                this._actionModalsService.alertError(text);
            }
        );
    }
}

class ILCLanguages {
    static en = {
        alerts: {
            confirmDeleteInvoice: "Are you sure you want to delete this invoice ?",
            alertInvoiceDeletedSuccessfully: "Invoice deleted successfully",
            alertInvoiceDeleteError: "An error ocurred",
            alertCloseInvoiceError: "An error ocurred"
        },
        newInvoice: "New invoice",
        searchInvoices: "Search invoices",
        filterInvoices: "Search With Filters",
        openInvoices: "Open Invoices",
        closedInvoices: "Closed Invoices",
        editInvoice: "Edit Invoice",
        deleteInvoice: "Delete Invoice",
        closeInvoice: "Close Invoice",
        invoiceEmissionDate: "Created at",
        invoiceDeliveryDate: "Delivery Date",
    };
    static de = {
        alerts: {
            confirmDeleteInvoice: "wollen Sie dieses Rechnung löschen ?",
            alertInvoiceDeletedSuccessfully: "Rechnung gelöscht",
            alertInvoiceDeleteError: "Es ist ein Fehler aufgetreten",
            alertCloseInvoiceError: "Es ist ein Fehler aufgetreten"
        },
        newInvoice: "Neue Rechnung",
        searchInvoices: "Rechnungen suchen",
        filterInvoices: "Suchen mit Filtern",
        openInvoices: "Offene Rechnungen",
        closedInvoices: "Geschlossene Rechnungen",
        editInvoice: "Rechnung bearbeiten",
        deleteInvoice: "Rechnung löschen",
        closeInvoice: "Rechnung schließen",
        invoiceEmissionDate: "Erstellt am",
        invoiceDeliveryDate: "Lieferdatum",
    };
    static es = {
        alerts: {
            confirmDeleteInvoice: "Esta seguro que desea borrar esta factura ?",
            alertInvoiceDeletedSuccessfully: "Factura eliminada exitosamente",
            alertInvoiceDeleteError: "Ha ocurrido un error",
            alertCloseInvoiceError: "Ha ocurrido un error"
        },
        newInvoice: "Nueva Factura",
        searchInvoices: "Buscar Facturas",
        filterInvoices: "Busqueda con Filtros",
        openInvoices: "Facturas Abiertas",
        closedInvoices: "Facturas  Pagadas",
        editInvoice: "Editar Factura",
        deleteInvoice: "Eliminar Factura",
        closeInvoice: "Cerrar Factura",
        invoiceEmissionDate: "Creada",
        invoiceDeliveryDate: "Fecha de entrega",
    };
}
