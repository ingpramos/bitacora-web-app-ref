import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { SearchItemsToInsertInInvoiceComponent } from "../search-items-to-insert-in-invoice/search-items-to-insert-in-invoice.component";
import { InvoicePdfService } from "../../../shared/services/pdfs/invoice-pdf.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ImportExcelIntoInvoiceComponent } from "../import-excel-into-invoice/import-excel-into-invoice.component";
import { DeliveryNotePdfService } from "../../../shared/services/pdfs/delivery-note-pdf.service";
import { SearchGroupsToInsertInInvoiceComponent } from "../search-groups-to-insert-in-invoice/search-groups-to-insert-in-invoice.component";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-invoice-header",
    templateUrl: "./invoice-header.component.html",
    styleUrls: ["./invoice-header.component.scss"]
})
export class InvoiceHeaderComponent implements OnInit, OnDestroy {
    @Input() currentInvoice: any;
    @Input() zeroInvoices: boolean; // to enable or disable menu items
    @Input() openInvoicesView: boolean; // to enable or disable menu items
    @Input() showMenu: boolean; // to show or hide three points menu

    itemsInInvoice: number = 0;
    invoiceHeaderText: any;
    itemsSubscription;

    constructor(
        private _invoicePdfService: InvoicePdfService,
        private _sharedService: SharedService,
        private _excelService: ExcelService,
        private _idocsService: InternalDocumentsService,
        private _DeliveryNotePdfService: DeliveryNotePdfService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.invoiceHeaderText = IHCLanguage[cl];
        this.itemsSubscription = this._idocsService.currentItemsList.subscribe(data => (this.itemsInInvoice = data.length));
    }

    ngOnDestroy() {
        this.itemsSubscription.unsubscribe();
    }

    searchForItems() {
        let initialState = {};
        initialState["currentInvoice"] = this.currentInvoice;
        initialState["itemsInInvoice"] = this.itemsInInvoice;
        const dialogRef = this.dialog.open(SearchItemsToInsertInInvoiceComponent, { width: "1200px", data: initialState });
    }

    searchForAssemblies() {
        let initialState = {};
        initialState["currentInvoice"] = this.currentInvoice;
        const dialogRef = this.dialog.open(SearchGroupsToInsertInInvoiceComponent, { width: "1200px", data: initialState });
    }

    generatePdfReport() {
        let document = this._invoicePdfService.buildDocument(this.currentInvoice);
        let documentName = `${this.currentInvoice.invoice_number} - ${this.currentInvoice.client_name}`;
        this._invoicePdfService.pdfMake.createPdf(document).download(documentName);
    }

    generatePdfDeliveryNote() {
        let document = this._DeliveryNotePdfService.buildDocument(this.currentInvoice);
        let documentName = `${this.currentInvoice.invoice_number} - ${this.currentInvoice.client_name}`;
        this._DeliveryNotePdfService.pdfMake.createPdf(document).download(documentName);
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    downloadExcelInvoice() {
        let documentName = `${this.currentInvoice.invoice_number} - ${this.currentInvoice.client_name}.xlsx`;
        let columnsToDelete = ["item_in_internal_document_id","internal_document_id","item_id","company_id","item_raw_material_dimensions","item_type","user_id"];
        let itemsInOffer = this._idocsService.getItemsOfCurrentDocument("excel");
        this._excelService.deleteObjectProperties(itemsInOffer, columnsToDelete);
        this._excelService.downLoadXlsxFile(itemsInOffer, documentName, this.currentInvoice.invoice_number);
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentInvoice"] = this.currentInvoice;
            initialState["itemsInInvoice"] = this.itemsInInvoice; // amount of items @ 1 der 34
            const dialogRef = this.dialog.open(ImportExcelIntoInvoiceComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }
}

class IHCLanguage {
    static en = {
        invoiceNumber: "Invoice Number",
        ocNumber: "Order Number",
        invoiceClient: "Client",
        invoiceStatus: "Status",
        statusPaid: "Paid",
        statusNotPaid: "Waiting for payment",
        invoiceItems: "Items",
        invoiceProject: "Project",
        insertItems: "Insert Items",
        insertAssemblies: "Insert Groups",
        insertProjects: "Insert Projects",
        downloadExcelFile: "Download Invoice Excel",
        uploadExcel: "Upload Excel File",
        downloadPdfFile: "Download Invoice PDF",
        filterTable: "Search Item List",
        downloadPdfNote: "Download Delivery Note"
    };

    static de = {
        invoiceNumber: "Rechnungsnummer",
        ocNumber: "AB Nummer ",
        invoiceClient: "Kunde",
        invoiceStatus: "Status",
        statusPaid: "bezahlt",
        statusNotPaid: "nicht bezahlt",
        invoiceItems: "Artikel",
        invoiceProject: "Auftrag",
        insertItems: "Artikel einfügen",
        insertAssemblies: "Baugruppen einfügen",
        insertProjects: "Projekt einfügen",
        downloadExcelFile: "Excel Rechnung herrunterladen",
        uploadExcel: "Excel Datei hochladen",
        downloadPdfFile: "PDF Rechnung herrunterladen",
        filterTable: "Tabelle Suchen",
        downloadPdfNote: "PDF Lieferschein"
    };
    static es = {
        invoiceNumber: "Número de Factura",
        ocNumber: "Número de Orden",
        invoiceClient: "Cliente",
        invoiceStatus: "Estado",
        statusPaid: "Pagada",
        statusNotPaid: "No pagada",
        invoiceItems: "Articulos",
        invoiceProject: "Proyecto",
        insertItems: "Añadir Items",
        insertAssemblies: "Añadir Grupos",
        insertProjects: "Facturar Proyecto",
        downloadExcelFile: "Descargar Factura Excel",
        uploadExcel: "Importar Excel con Items",
        downloadPdfFile: "Descargar Factura PDF",
        filterTable: "Buscar Item",
        downloadPdfNote: "Descargar Nota De Entrega"
    };
}
