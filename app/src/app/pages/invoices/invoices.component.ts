import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { map } from "rxjs/operators";
import { InvoicesService } from "../../shared/services/invoices.service";
import { InternalDocumentsService } from "../../shared/services/internal-documents.service";

@Component({
    selector: "app-invoices",
    templateUrl: "./invoices.component.html",
    styleUrls: ["./invoices.component.scss"]
})
export class InvoicesComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activeRoute.snapshot["_urlSegment"].segments[1].path;
    openInvoicesView = this.componentUrl === "open-invoices" ? true : false;
    zeroInvoices: boolean;
    // component properties
    invoices: any = [];
    invoice_id: any;
    selectedInvoice = {};
    // subscriptions
    invoicesList$;
    invoiceToPushOrUpdate$;
    
    constructor(
		private _invoiceService: InvoicesService,
		private _idocsService: InternalDocumentsService,
		private activeRoute: ActivatedRoute,
		private router: Router
		) {
        //this._invoiceService.passNewInvoice({});
        this.activeRoute.params.pipe(map(params => params["invoice_id"])).subscribe(invoice_id => {
            this.invoice_id = invoice_id;
            this.setSelectedInvoice();
        });
        this.invoiceToPushOrUpdate$ =  this._invoiceService.invoiceToPushOrUpdate.subscribe(data =>{
            let invoice_number = data.invoice_number;
            if (!invoice_number) return; // case data = {} an initialization
            // to find inex no use id because in this case the document_id changes and just the oc number will be added
            let index = this.invoices.findIndex(obj => obj["invoice_number"] == invoice_number);
            if (index == -1) {
                // invoice was created
                const invoice_id = data.internal_document_id;
                this.invoices.push(data);
                this.router.navigate(["app/" + this.componentUrl, invoice_id]);
            } else {
                // oc was updated
                this.invoices.splice(index, 1, data);
                this.selectedInvoice = data;
                this._idocsService.setCurrentDocument(this.selectedInvoice); // to update total cost model
                this._idocsService.calculateDocumentTotalPriceModel();
            }
        });

        this.invoicesList$ = this._invoiceService.currentInvoicesList.subscribe(data => {
            this.invoices = data;
            this.setSelectedInvoice();
        });
    }

    ngOnInit() {
        this.openInvoicesView ? this._invoiceService.getInvoices(1) : this._invoiceService.getInvoices(0);
    }

    ngOnDestroy() {
        this.invoiceToPushOrUpdate$.unsubscribe();
        this.invoicesList$.unsubscribe();
        this._invoiceService.updateInvoicesList([]);
    }

    setSelectedInvoice() {
        this.invoices.length > 0 ? (this.zeroInvoices = false) : (this.zeroInvoices = true);
        if (this.invoices.length > 0) {
            let found = this.invoices.find(obj => obj["internal_document_id"] == this.invoice_id);
            this.selectedInvoice = found ? found : this.invoices[0];
            this.router.navigate(["app/" + this.componentUrl, this.selectedInvoice["internal_document_id"]]);
            this._idocsService.setCurrentDocument(this.selectedInvoice);
            window.scroll(0, 0);
        } else {
            this.selectedInvoice = {};
            this._idocsService.setCurrentDocument(this.selectedInvoice);
        }
    }
}
