import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { InvoicesService } from "../../../shared/services/invoices.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { CurrencyPipe } from "@angular/common";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-item-in-invoice-form",
    templateUrl: "./item-in-invoice-form.component.html",
    styleUrls: ["./item-in-invoice-form.component.scss"]
})
export class ItemInInvoiceFormComponent implements OnInit {

	itemFormText: any;
	alerts:any;
    currentItem: any = this.modalData.currentItem;
    currentLanguage:string;
    constructor(
        private _currencyPipe: CurrencyPipe,
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService,
        private _invoicesService: InvoicesService,
        private _idocsService: InternalDocumentsService,
        public dialogRef: MatDialogRef<ItemInInvoiceFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
		this.itemFormText = IIIFCLanguage[this.currentLanguage];
		this.alerts = this.itemFormText.alerts;
    }

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentItem = JSON.parse(JSON.stringify(this.currentItem));
         // preformat the values to show correct format when opening this component
         let isp = Number(this.currentItem["item_sell_price"]).toFixed(2);
         let itp = Number(this.currentItem["item_total_price"]).toFixed(2);
         this.currentItem["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(isp).trim();
         this.currentItem["item_total_price"] = this._sharedService.parseToLocaleString(itp).trim();
    }

    closeModal() {
        this.currentItem = {};
        this.dialogRef.close();
    }

	save(item) {
        // format the string as Number before saving
        item["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(item["item_sell_price_to_show"]);
        item["item_type"] == "item" ? this.saveItem(item) : this.saveAssembly(item);
    }

    saveItem(item) {
        this._idocsService.updateItemInInternalDocument(item).subscribe(
            data => {
                if (data) {
                    let invoice_id = item.internal_document_id;
                    this._idocsService.getItemsFromInternalDocument(invoice_id);
                    let title = item.item_code;
                    let text = this.alerts.alertItemUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeModal();
                }
            },
            () => {
                let text = this.alerts.alertItemUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    saveAssembly(assembly) {
        this.renameProperties(assembly);
        this._idocsService.updateAssemblyInInternalDocument(assembly).subscribe(
            data => {
                if (data) {
                    let invoice_id = assembly.internal_document_id;
                    this._idocsService.getItemsFromInternalDocument(invoice_id);
                    this.closeModal();
                    let title = assembly.item_name;
                    let text = this.alerts.alertItemUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertItemUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    renameProperties(assembly) {
        assembly["aiid_id"] = assembly["item_in_internal_document_id"];
        assembly["assembly_amount"] = assembly["item_amount"];
        assembly["assembly_sell_price"] = assembly["item_sell_price"];
        assembly["assembly_in_internal_document_details"] = assembly["item_in_internal_document_details"];
    }

    calculateTotalPrice() {
        let stringValue = this.currentItem["item_sell_price_to_show"];
        let priceAsNumber: Number = this._sharedService.parseFormattedStringAsNumber(stringValue);
        let itemTotalPrice = Number(this.currentItem["item_amount"]) * Number(priceAsNumber);
        this.currentItem["item_total_price"] = this._currencyPipe.transform(itemTotalPrice, this._sharedService.currency, "", "", this.currentLanguage);
    }
}

class IIIFCLanguage {
    static en = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item updated successfully",
            alertItemUpdateError: "An error ocurred"
        },
        modalTitle: "Update item information",
        itemCode: "Item Code",
        itemOrderNumber: "Order Number",
        itemName: "Item Name",
        itemAmount: "Amount",
        itemBuyPrice: "Price",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Received",
        itemLastPerson: "Last Person",
        itemDeliveryDate: "Delivery Date",
        itemInInternalDocumentDetails: "Details",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertItemUpdatedSuccessfully: "Artikel erfolgreich aktualiziert",
            alertItemUpdateError: "An error ocurred"
        },
        modalTitle: "Artikel information aktualizieren",
        itemCode: "Artikel Code",
        itemOrderNumber: "Bestellnummer",
        itemName: "Name",
        itemAmount: "Menge",
        itemBuyPrice: "Preis",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Erhalten",
        itemLastPerson: "Letztes Person",
        itemDeliveryDate: "Lieferdatum",
        itemInInternalDocumentDetails: "Details",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item actualizado exitosamente",
            alertItemUpdateError: "An error ocurred"
        },
        modalTitle: "Actualizar informacion del articulo",
        itemCode: "Código Del Item",
        itemOrderNumber: "Numero de Compra",
        itemName: "Nombre",
        itemAmount: "Cantidad",
        itemBuyPrice: "Precio",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Recibido",
        itemLastPerson: "Ultima Persona",
        itemDeliveryDate: "Fecha de Recibido",
        itemInInternalDocumentDetails: "Detalles",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
