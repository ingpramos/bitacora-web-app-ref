import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInInvoiceFormComponent } from './item-in-invoice-form.component';

describe('ItemInInvoiceFormComponent', () => {
  let component: ItemInInvoiceFormComponent;
  let fixture: ComponentFixture<ItemInInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
