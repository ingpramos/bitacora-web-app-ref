import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { InvoicesService } from "../../../shared/services/invoices.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { CompanyService } from "../../../shared/services/company.service";
import { DateAdapter } from "@angular/material/core";

@Component({
    selector: "app-invoice-form",
    templateUrl: "./invoice-form.component.html",
    styleUrls: ["./invoice-form.component.scss"]
})
export class InvoiceFormComponent implements OnInit, OnChanges {
    @Input() currentInvoice;
    @Input() editInvoice: boolean;
    @Input() newInvoice: boolean;

    @Output() useForm = new EventEmitter<boolean>();

    sessionInfo: any;
    showCurrentOrderConfirmationWhileEdit: boolean = false;
    currentInvoiceCopy;
    ordersConfirmed: any = {};
    clients: any = {};
    paymentMethods: any = {};
    paymentConditions: any = {};
    deliveryMethods: any = {};
    deliveryConditions: any = {};
    invoiceFormText: any;
    alerts: any;

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _invoicesService: InvoicesService,
        private _clientsServices: ClientsService,
        private _companyService: CompanyService,
        private adapter: DateAdapter<any>
    ) { }

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        // change the date picker adapter to current language
        this.adapter.setLocale(cl);
        this.invoiceFormText = IFCLanguages[cl];
        this.alerts = this.invoiceFormText.alerts;
        
        if (this.editInvoice) this.showCurrentOrderConfirmationWhileEdit = true;
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            if (this.newInvoice) this.callNextInvoiceNumber();
            if (this.editInvoice) this.currentInvoiceCopy = JSON.parse(JSON.stringify(this.currentInvoice));
            this.callOrdersConfirmation();
            this.paymentMethods = this._companyService.formatPaymentMethods(this.sessionInfo.payment_methods);
            this.paymentConditions = this._companyService.formatPaymentConditions(this.sessionInfo.payment_conditions);
            this.deliveryMethods = this._companyService.formatDeliveryMethods(this.sessionInfo.delivery_methods);
            this.deliveryConditions = this._companyService.formatDeliveryConditions(this.sessionInfo.delivery_conditions);
        });
        this._clientsServices.getFormattedClients().subscribe(data => (this.clients = data));
    }

    ngOnChanges(changes) {
        if (this.newInvoice && this.sessionInfo) this.callNextInvoiceNumber();
    }

    saveInvoice() {
        this.editInvoice ? this.updateInvoice() : this.createInvoiceFromOrderConfirmed();
    }

    createInvoiceFromOrderConfirmed() {
        this.currentInvoice["invoice_user_id"] = this.sessionInfo.user_id;
        this.currentInvoice["invoice_state"] = 1;
        this.currentInvoice["offer_state"] = 3;
        this._invoicesService.createInvoice(this.currentInvoice).subscribe(
            data => {
                if (data) {
                    this._invoicesService.passOCToPushOrUpdate(data);
                    this.useForm.emit(false);
                    let title = this.currentInvoice.invoice_number;
                    let text = this.alerts.alertInvoiceCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertInvoiceCreateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    updateInvoice() {
        this.currentInvoice["invoice_user_id"] = this.sessionInfo.user_id;
        this.currentInvoice["invoice_state"] = 1;
        this.currentInvoice["offer_state"] = 3;
        this.currentInvoice.prev_doc_id = this.currentInvoiceCopy.internal_document_id;
        this.currentInvoice.new_doc_id = this.currentInvoice.internal_document_id;
        this._invoicesService.updateInvoice(this.currentInvoice).subscribe(
            data => {
                if (data) {
                    this.useForm.emit(false);
                    this._invoicesService.passOCToPushOrUpdate(data);
                    let title = this.currentInvoice.invoice_number;
                    let text = this.alerts.alertInvoiceUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertInvoiceUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    callNextInvoiceNumber() {
        this._invoicesService.getCurrentInvoiceNumber().subscribe(data => {
            this.currentInvoice.invoice_number = `${data.document_prefix}${data.document_current_number + 1}`;
        });
    }

    callOrdersConfirmation() {
        this._invoicesService.getAvailableOrdersConfirmation().subscribe(response => {
            this.ordersConfirmed = response;
        });
    }

    setDocument(event) {
        let docId = this.ordersConfirmed["get_internal_document_id"](event.value);
        let internalDcoument = this.ordersConfirmed["original_data"].find(obj => obj["internal_document_id"] == docId);
        this.setProperties(internalDcoument);
    }

    setProperties(internalDcoument) {
        this.currentInvoice["internal_document_id"] = internalDcoument["internal_document_id"];
        this.currentInvoice["client_name"] = internalDcoument["client_name"];
        this.currentInvoice["payment_method_name"] = internalDcoument["payment_method_name"];
        this.currentInvoice["payment_condition_description"] = internalDcoument["payment_condition_description"];
        this.currentInvoice["delivery_method_name"] = internalDcoument["delivery_method_name"];
        this.currentInvoice["delivery_condition_description"] = internalDcoument["delivery_condition_description"];
        this.currentInvoice["invoice_emition_date"] = undefined; // clean date picker
        this.currentInvoice["invoice_delivery_date"] = undefined; // clean date picker
    }

    forgetActions() {
        if (this.editInvoice) {
            this.currentInvoice.order_confirmation_number = this.currentInvoiceCopy.order_confirmation_number;
            this.setProperties(this.currentInvoiceCopy);
        } else {
            this.currentInvoice = {};
        }
        this.useForm.emit(false);
    }
}

class IFCLanguages {
    static en = {
        alerts: {
            alertInvoiceUpdatedSuccessfully: "Invoice updated successfully",
            alertInvoiceUpdateError: "An error ocurred",
            alertInvoiceCreatedSuccessfully: "Invoice created successfully",
            alertInvoiceCreateError: "An error ocurred"
        },
        invoiceNumber: "Invoice Number",
        currentOrderConfirmationNumber: "Current Order Confirmation Number",
        orderConfirmation: "Order Confirmation",
        client: "Client",
        paymentMethod: "Payment Method",
        paymentConditions: "Payment Conditions",
        deliveryMethod: "Delivery Method",
        deliveryConditions: "Delivery Conditions",
        invoiceEmissionDate: "Invoice Date",
        invoiceDeliveryDate: "Delivery Date",
        companyContact: "Contact",
        invoiceStatus: "Order Status",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertInvoiceUpdatedSuccessfully: "Rechnung erfolgreich aktualiziert",
            alertInvoiceUpdateError: "Es ist ein Fehler aufgetreten",
            alertInvoiceCreatedSuccessfully: "Rechnung erfolgreich erstelt",
            alertInvoiceCreateError: "Es ist ein Fehler aufgetreten"
        },
        invoiceNumber: "Rechnungsnummer",
        currentOrderConfirmationNumber: "Aktuell AB Nummer",
        orderConfirmation: "Auftragbestätigung",
        client: "Kunde",
        paymentMethod: "Zahlungsart",
        paymentConditions: "Zahlungsbedingungen",
        deliveryMethod: "Liefertart",
        deliveryConditions: "Liefertbedigungen",
        invoiceEmissionDate: "Rechnungsdatum",
        invoiceDeliveryDate: "Lieferdatum",
        companyContact: "Ansprechpartner",
        invoiceStatus: "Bestellung Status",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertInvoiceUpdatedSuccessfully: "Factura actualizada exitosamente",
            alertInvoiceUpdateError: "Ha ocurrido un error",
            alertInvoiceCreatedSuccessfully: "Factura creada existosamente",
            alertInvoiceCreateError: "Ha ocurrido un error"
        },
        invoiceNumber: "Número de Factura",
        currentOrderConfirmationNumber: "Número de Pedido Actual",
        orderConfirmation: "Pedido",
        client: "Cliente",
        paymentMethod: "Método de pago",
        paymentConditions: "Condiciones de pago",
        deliveryMethod: "Método de entrega",
        deliveryConditions: "Condiciones de entrega",
        invoiceEmissionDate: "Fecha de facturación",
        invoiceDeliveryDate: "Fecha de entrega",
        companyContact: "Contacto",
        invoiceStatus: "Estado de la orden",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
