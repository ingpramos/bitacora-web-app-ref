import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInInvoiceTableColumnsFormComponent } from './items-in-invoice-table-columns-form.component';

describe('ItemsInInvoiceTableColumnsFormComponent', () => {
  let component: ItemsInInvoiceTableColumnsFormComponent;
  let fixture: ComponentFixture<ItemsInInvoiceTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInInvoiceTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInInvoiceTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
