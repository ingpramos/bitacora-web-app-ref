import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExcelIntoInvoiceComponent } from './import-excel-into-invoice.component';

describe('ImportExcelIntoInvoiceComponent', () => {
  let component: ImportExcelIntoInvoiceComponent;
  let fixture: ComponentFixture<ImportExcelIntoInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportExcelIntoInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExcelIntoInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
