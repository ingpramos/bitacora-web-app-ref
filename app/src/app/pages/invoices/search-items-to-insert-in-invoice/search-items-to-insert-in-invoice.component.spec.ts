import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInInvoiceComponent } from './search-items-to-insert-in-invoice.component';

describe('SearchItemsToInsertInInvoiceComponent', () => {
  let component: SearchItemsToInsertInInvoiceComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
