import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInInvoiceTableComponent } from './items-in-invoice-table.component';

describe('ItemsInInvoiceTableComponent', () => {
  let component: ItemsInInvoiceTableComponent;
  let fixture: ComponentFixture<ItemsInInvoiceTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInInvoiceTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInInvoiceTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
