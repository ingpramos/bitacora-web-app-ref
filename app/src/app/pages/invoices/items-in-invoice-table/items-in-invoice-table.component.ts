import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemInInvoiceFormComponent } from "../item-in-invoice-form/item-in-invoice-form.component";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ItemsInInvoiceTableColumnsFormComponent } from "../items-in-invoice-table-columns-form/items-in-invoice-table-columns-form.component";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-items-in-invoice-table",
    templateUrl: "./items-in-invoice-table.component.html",
    styleUrls: ["./items-in-invoice-table.component.scss"]
})

export class ItemsInInvoiceTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() currentInvoice;
    companyCurrency: string = "";
    currentLanguage:string;
    items = [];
    itemsInInvoiceText: any;
    tableColumns: any = {};
    defaultTableColumnsObj: any;
    alerts: any;
    invoiceTotalFooter: any;
    // subscription
    itemsList$;
    columnsNames$;
    totalModel$;
    constructor(
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _actionModalsService: ActionModalsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.companyCurrency = this._sharedService.currency;
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInInvoiceText = IIITCLanguages[this.currentLanguage];
        this.alerts = this.itemsInInvoiceText.alerts;
        this.defaultTableColumnsObj = this.itemsInInvoiceText.columns;
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_invoices", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => this.tableColumns = data);
        // to update the items when new ones are inserted
        this.itemsList$ = this._idocsService.currentItemsList.subscribe(data => {
            this.items = data;
            if (this.items) {
                this._idocsService.setItemsOfCurrentDocument(this.items);
                this._idocsService.calculateDocumentTotalPriceModel();
            }
        });
        this.totalModel$ = this._idocsService.documentTotalPriceModelObs.subscribe(data => {
            this.invoiceTotalFooter = data;
            this.currentInvoice.subTotal = this.invoiceTotalFooter.subTotal;
            this.currentInvoice.vat = this.invoiceTotalFooter.vat;
            this.currentInvoice.total = this.invoiceTotalFooter.total;
            this._sharedService.stopLoading();
        });
    }

    ngOnChanges(changes) {
        let id_id = changes.currentInvoice.currentValue.internal_document_id;
        if (id_id) {
            this._idocsService.getItemsFromInternalDocument(id_id);
            this._sharedService.startLoading();
        }
        if (!id_id) {
            this._idocsService.updateItemsList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
        this.totalModel$.unsubscribe();
    }

    editTableColumns() {
        let initialState = {
            defaultTableColumnsObj: this.defaultTableColumnsObj,
            currentTableColumns: this.tableColumns
        };
        const dialogRef = this.dialog.open(ItemsInInvoiceTableColumnsFormComponent, { width: "1000px", data: initialState });
    }

    editItemInInvoice(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemInInvoiceFormComponent, { width: "1000px", data: initialState });
    }

    deleteItem(item, index) {
        item["item_type"] == "item" ? this.deleteItemFromInvoice(item, index) : this.deleteAssemblyFromInvoice(item, index);
    }

    deleteItemFromInvoice(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromInvoice;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                const iiinvoice_id = item.item_in_internal_document_id;
                this._idocsService.deleteItemFromInternalDocument(iiinvoice_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
    deleteAssemblyFromInvoice(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromInvoice;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let aiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteAssemblyFromInternalDocument(aiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class IIITCLanguages {
    static en = {
        title: "Invoice Details",
        totalWithoutVAT: "Total without VAT",
        vatFrom: "VAT from ",
        totalWithVAT: "Total with VAT",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Item Code",
            item_name: "Item Desciption",
            item_amount: "Amount",
            item_sell_price: "Price(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromInvoice: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns Names"
    };

    static de = {
        title: "Rechnungsdetails",
        totalWithoutVAT: "Total exkl. MwSt.",
        vatFrom: "MwSt von",
        totalWithVAT: "Total inkl. MwSt.",
        columns: {
            item_position: "Pos",
            item_type: "Typ",
            item_code: "Artikel Code",
            item_name: "Name",
            item_amount: "Menge",
            item_sell_price: "Preis(E)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromInvoice: "Sind Sie sicher das Sie dieses Artikel löschen möchte?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Sapltennamen bearbeiten"
    };

    static es = {
        title: "Detalles de la Factura",
        totalWithoutVAT: "Total sin IVA",
        vatFrom: "IVA de ",
        totalWithVAT: "Total Con IVA",
        columns: {
            item_position: "Pos",
            item_type: "Tipo",
            item_code: "Código del Item",
            item_name: "Descripción",
            item_amount: "Cantidad",
            item_sell_price: "Precio(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromInvoice: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item eliminado exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Nombre de las Columnas"
    };
}
