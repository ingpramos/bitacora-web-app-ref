import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';

// Angular Material
import {
	MatMenuModule, MatInputModule, MatFormFieldModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatIconModule
} from '@angular/material';

import { PipesModule } from '../../shared/pipes/pipes.module';

import { ProvidersComponent } from './providers.component';
import { ProvidersListComponent } from './providers-list/providers-list.component';
import { ProviderHeaderComponent } from './provider-header/provider-header.component';
import { ProviderContactsTableComponent } from './provider-contacts-table/provider-contacts-table.component';
import { ProviderFormComponent } from './provider-form/provider-form.component';
import { ContactFormComponent } from './contact-form/contact-form.component';

const PROVIDERS_ROUTES: Routes = [
	{ path: '', component: ProvidersComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(PROVIDERS_ROUTES),
		FormsModule,
		MatMenuModule,
		MatInputModule,
		MatFormFieldModule,
		MatDialogModule,
		MatIconModule,
		PipesModule
	],
	declarations: [
		ProvidersComponent,
		ProvidersListComponent,
		ProviderHeaderComponent,
		ProviderContactsTableComponent, ProviderFormComponent, ContactFormComponent],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		ContactFormComponent
	]
})
export class ProvidersModule { }
