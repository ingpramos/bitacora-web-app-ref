import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ContactFormComponent } from "../contact-form/contact-form.component";
import { ProvidersService } from "../../../shared/services/providers.service";

@Component({
    selector: "app-provider-header",
    templateUrl: "./provider-header.component.html",
    styleUrls: ["./provider-header.component.scss"]
})
export class ProviderHeaderComponent implements OnInit, OnDestroy {
    // languge initialization
    providerHeaderText: any;
    @Input() provider: any = {};
    @Input() showMenu: boolean; // to show or hide three points menu

    contactsInProvider: number = 0;
    contactsSubscription;

    constructor(private _sharedService: SharedService, private _providersService: ProvidersService, public dialog: MatDialog) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.providerHeaderText = PHLanguages[cl];
        this.contactsSubscription = this._providersService.currentContactsList.subscribe(data => (this.contactsInProvider = data.length));
    }

    ngOnDestroy() {
        this.contactsSubscription.unsubscribe();
    }

    startNewContact() {
        let initialInformation = {};
        initialInformation["provider"] = this.provider;
        initialInformation["currentContact"] = {};
        const dialogRef = this.dialog.open(ContactFormComponent, { width: "1000px", data: initialInformation });
    }
}

class PHLanguages {
    static en = {
        providerName: "Provider Name",
        providerTelephone: "Telephone",
        providerAddress: "Address",
        providerContacts: "Contacts",
        newContact: "New Contact",
        uploadContactsList: "Upload Contact List"
    };

    static de = {
        providerName: "Lieferant",
        providerTelephone: "Telefon",
        providerAddress: "Adresse",
        providerContacts: "Kontakte",
        newContact: "Neue Kontakt erstellen",
        uploadContactsList: "Kontaktelist hochladen"
    };

    static es = {
        providerName: "Proveedor",
        providerTelephone: "Telefono",
        providerAddress: "Dirección",
        providerContacts: "Contactos",
        newContact: "Crear Nuevo Contacto",
        uploadContactsList: "Subir lista de Contactos"
    };
}
