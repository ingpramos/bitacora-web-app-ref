import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { ProvidersService } from '../../../shared/services/providers.service';
import { ContactFormComponent } from '../contact-form/contact-form.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
	selector: 'app-provider-contacts-table',
	templateUrl: './provider-contacts-table.component.html',
	styleUrls: ['./provider-contacts-table.component.scss']
})

export class ProviderContactsTableComponent implements OnInit, OnChanges, OnDestroy {

	@Input() provider;
	providerContactsText: any;
	tableColumns: any;
	contacts = [];
	alerts: any;
	// subscription
	contactsList$: any;

	constructor(
		private _sharedService: SharedService,
		private _providersService: ProvidersService,
		private _actionModalsService: ActionModalsService,
		public dialog: MatDialog
	) {
		this.contactsList$ = this._providersService.currentContactsList.subscribe(data => this.contacts = data);
	}

	ngOnInit() {
		let cl = this._sharedService.user_selected_language;
		this.providerContactsText = PCTLanguage[cl];
		this.tableColumns = PCTLanguage[cl].columns;
		this.alerts = PCTLanguage[cl].alerts;
	}
	ngOnDestroy() {
		this.contactsList$.unsubscribe();
	}

	ngOnChanges(changes) {
		let provider_id = changes.provider.currentValue.provider_id;
		if (provider_id) { this._providersService.getContacts(provider_id); }
	}

	editContact(contact) {
		let initialInformation = {};
		initialInformation['provider'] = this.provider;
		initialInformation['currentContact'] = contact;
		initialInformation['editContact'] = true;
		const dialogRef = this.dialog.open(ContactFormComponent, { width: '1000px', data: initialInformation });
	}

	deleteContactFromTable(contact, index) {
		let title = `${contact.contact_name} ${contact.contact_last_name}`;
		let text = this.alerts.confirmDeleteContactFromProvider;
		this._actionModalsService.buildConfirmModal(title, text)
			.then(result => {
				if (result.value) {
					let pc_id = contact.pc_id;
					this._providersService.deleteContactFromTable(pc_id).subscribe(data => {
						if (data) {
							let text = this.alerts.alertContactProviderDeletedSuccessfully;
							this._actionModalsService.buildSuccessModal(title, text)
							this.contacts.splice(index, 1);
						}
					}, (e) => {
						if (e.error.error.code == '23503') { // handle foreign key constrain
							const text = this.alerts.alertForeignKeyViolation;
							this._actionModalsService.buildInfoModal(title, text);
						}
						else {
							let titel = this.alerts.alertContactProviderDeletedError;
							this._actionModalsService.alertError(titel);
						}
					});
				}
			});
	}

}

class PCTLanguage {

	static en = {
		"columns": {
			"position": "Pos",
			"contact_name": "Name",
			"contact_last_name": "Last Name",
			"contact_telephone": "Telephone",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContactFromProvider": "Are you sure you want to delete this contact ?",
			"alertContactProviderDeletedSuccessfully": "Item Deleted Successfully",
			"alertForeignKeyViolation": "This contact is still referenced in one of your orders",
			"alertContactProviderDeletedError": "An error ocurred"
		},
		"title": "Provider Contacts",
		"options": "Options",
		"filterPlaceholder": "Search Items"
	};

	static de = {
		"columns": {
			"position": "Pos",
			"contact_name": "Vorname",
			"contact_last_name": "Nachname",
			"contact_telephone": "Telefon",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContactFromProvider": "Sind Sie sicher dass Sie dieses Artikel löschen möchte ?",
			"alertContactProviderDeletedSuccessfully": "Artikel erfolgreich gelöscht",
			"alertForeignKeyViolation": "Dieser Kontakt ist immer noch referenziert in einer Ihrer Bestellungen",
			"alertContactProviderDeletedError": "Es ist ein Fehler aufgetreten"
		},
		"title": "Lieferant Kontanten",
		"options": "Optionen",
		"filterPlaceholder": "Tabelle Suchen"
	};

	static es = {
		"columns": {
			"position": "Pos",
			"contact_name": "Nombre",
			"contact_last_name": "Apellido",
			"contact_telephone": "Telefóno",
			"contact_email": "Email"
		},
		"alerts": {
			"confirmDeleteContactFromProvider": "Esta seguro que desea eliminar el contacto actual ?",
			"alertContactProviderDeletedSuccessfully": "Contacto eliminado exitosamente",
			"alertForeignKeyViolation": "Este contacto está referenciado en una de sus ordenes",
			"alertContactProviderDeletedError": "Ha ocurrido un error"
		},
		"title": "Contactos de Proveedor",
		"options": "Opciones",
		"filterPlaceholder": "Buscar Items"
	};
}
