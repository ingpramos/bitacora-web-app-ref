import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderContactsTableComponent } from './provider-contacts-table.component';

describe('ProviderContactsTableComponent', () => {
  let component: ProviderContactsTableComponent;
  let fixture: ComponentFixture<ProviderContactsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderContactsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderContactsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
