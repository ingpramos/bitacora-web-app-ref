import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ProvidersService } from "../../../shared/services/providers.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-provider-form",
    templateUrl: "./provider-form.component.html",
    styleUrls: ["./provider-form.component.scss"]
})
export class ProviderFormComponent implements OnInit {
    // component's Inputs
    providerFormText: any;
    alerts: any;
    @Input() currentProvider: any;
    @Input() editProvider;
    @Input() newProvider;
    // component's output
    @Output() closeForm = new EventEmitter<any>();
    /* @Output() useForm = new EventEmitter<boolean>(); */

    constructor(private _actionModalsService: ActionModalsService, private _sharedService: SharedService, private _providersService: ProvidersService) {
        let cl = this._sharedService.user_selected_language;
        this.providerFormText = PFLanguages[cl];
        this.alerts = this.providerFormText.alerts;
    }

    ngOnInit() {}

    forgetActions() {
        this.currentProvider = {};
        this.closeForm.emit(true);
    }

    saveProvider() {
        this.editProvider ? this.updateProvider() : this.createProvider();
    }

    createProvider() {
        this._providersService.createProvider(this.currentProvider).subscribe(
            data => {
                this._providersService.passProviderToPushOrUpdate(data);
                let title = this.currentProvider.provider_name;
                let text = this.alerts.alertProviderCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            },
            e => {
                if (e.status == 403) {
                    // already exist
                    let title = this.currentProvider.provider_name;
                    const text = this.alerts.alertProviderAlreadyExist;
                    this._actionModalsService.buildInfoModal(title, text);
                } else {
                    let title = this.alerts.alertProviderCreateError;
                    this._actionModalsService.alertError(title);
                    this.closeForm.emit(true);
                }
            }
        );
    }

    updateProvider() {
        this._providersService.updateProvider(this.currentProvider).subscribe(
            data => {
                this._providersService.passProviderToPushOrUpdate(data);
                let title = this.currentProvider.provider_name;
                let text = this.alerts.alertProviderUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(false);
            },
            () => {
                let title = this.alerts.alertProviderUpdateError;
                this._actionModalsService.alertError(title);
                this.closeForm.emit(true);
            }
        );
    }
}

class PFLanguages {
    static en = {
        alerts: {
            alertProviderUpdatedSuccessfully: "Provider updated successfully",
            alertProviderUpdateError: "An error ocurred",
            alertProviderCreatedSuccessfully: "Provider created successfully",
            alertProviderAlreadyExist: "The provider you are trying to create already exist",
            alertProviderCreateError: "An error ocurred"
        },
        providerName: "Provider Name",
        providerAddress: "Provider Address",
        providerTelephone: "Telephone",
        provider: "Provider",
        providerContact: "Contact",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertProviderUpdatedSuccessfully: "Lieferant erfolgreich aktualiziert",
            alertProviderUpdateError: "Es ist ein Fehler aufgetreten",
            alertProviderCreatedSuccessfully: "Lieferant erfolgreich erstellt",
            alertProviderAlreadyExist: "Der Lieferant, den Sie erstellen möchten, ist bereits vorhanden",
            alertProviderCreateError: "Es ist ein Fehler aufgetreten"
        },
        providerName: "Lieferant",
        providerAddress: "Adresse",
        providerTelephone: "Telefon",
        provider: "Liferant",
        providerContact: "Ansprechpartner",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertProviderUpdatedSuccessfully: "Proveedor actualizado correctamente",
            alertProviderUpdateError: "Ha ocurrido un error",
            alertProviderCreatedSuccessfully: "Proveedor creado exitosamente",
            alertProviderAlreadyExist: "El proveedor que está intentando crear ya existe",
            alertProviderCreateError: "Ha ocurrido un error"
        },
        providerName: "Proveedor",
        providerAddress: "Dirección",
        providerTelephone: "Teléfono",
        provider: "Proveedor",
        providerContact: "Contacto",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
