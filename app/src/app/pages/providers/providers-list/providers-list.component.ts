import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { SharedService } from "../../../shared/services/shared.service";
import { ProvidersService } from "../../../shared/services/providers.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-providers-list",
    templateUrl: "./providers-list.component.html",
    styleUrls: ["./providers-list.component.scss"]
})
export class ProvidersListComponent implements OnInit, OnDestroy {
    // laguage initialization;
    providersListText: any;
    alerts: any;
    editProvider: boolean = false;
    newProvider: boolean = false;
    showSearch: boolean = false;
    providerToHandle = {};

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private activatedRoute: ActivatedRoute;
    private paramMap$;
    private provider_id;

    @Input() providers: any;

    constructor(
        activatedRoute: ActivatedRoute,
        private router: Router,
        private _sharedService: SharedService,
        private _providersService: ProvidersService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.providersListText = PLLanguage[cl];
        this.alerts = this.providersListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.provider_id = +paramMap.get("provider_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForProviders();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchProviders() {
        this.subject.next(this.searchPattern);
    }

    searchForProviders() {
        this._providersService.getProvidersByPattern(this.searchPattern);
    }

    closeSearch() {
        this.searchPattern = "";
        this._providersService.getProviders();
        this.showSearch = false;
    }

    startSearch() {
        this.editProvider = false;
        this.newProvider = false;
        this.showSearch = true;
    }

    startEditProvider(provider) {
        this.providerToHandle = Object.assign({}, provider);
        this.editProvider = true;
    }

    startNewProvider() {
        this.providerToHandle = {};
        this.editProvider = false;
        this.newProvider = true;
    }

    forgetActions(value) {
        this.editProvider = false;
        this.newProvider = false;
    }

    deleteProvider(currentProvider, index) {
        let title = currentProvider.provider_name;
        let text = this.alerts.confirmDeleteProviderText;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let provider_id = currentProvider.provider_id;
                this._providersService.deleteProvider(provider_id).subscribe(
                    data => {
                        if (data) {
                            let text = this.alerts.alertProviderDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.providers.splice(index, 1);
                            this.router.navigate(["app/company/providers", 0]);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertProviderDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }
}

class PLLanguage {
    static en = {
        alerts: {
            confirmDeleteProviderText: "Are you sure you want to delete this Provider ?",
            alertProviderDeletedSuccessfully: "Provider deleted successfully",
            alertForeignKeyViolation: "This provider is still referenced in one of your orders",
            alertProviderDeleteError: "An error ocurred"
        },
        title: "Providers",
        newProvider: "New Provider",
        searchProviders: "Search Providers",
        editProvider: "Edit Provider",
        deleteProvider: "Delete Provider"
    };

    static de = {
        alerts: {
            confirmDeleteProviderText: "Wollen Sie dieses Lieferant löschen ?",
            alertProviderDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertForeignKeyViolation: "Dieser Lieferant ist immer noch referenziert in einer Ihrer Bestellungen",
            alertProviderDeleteError: "Es ist ein Fehler aufgetreten"
        },
        title: "Lieferanten",
        newProvider: "Neue Lieferant",
        searchProviders: "Lieferanten suchen",
        editProvider: "Lieferant bearbeiten",
        deleteProvider: "Lieferant löschen"
    };

    static es = {
        alerts: {
            confirmDeleteProviderText: "Esta seguro que deseas borrar este proveedor ?",
            alertProviderDeletedSuccessfully: "Proveedor eliminado exitosamente",
            alertForeignKeyViolation: "Este proveedor está referenciado en una de sus ordenes",
            alertProviderDeleteError: "Ha ocurrido un error"
        },
        title: "Proveedores",
        newProvider: "Nuevo Proveedor",
        searchProviders: "Buscar Proveedores",
        editProvider: "Editar Proveedor",
        deleteProvider: "Eliminar Provedor"
    };
}
