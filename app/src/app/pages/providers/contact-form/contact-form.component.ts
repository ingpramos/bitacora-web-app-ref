import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';
import { ProvidersService } from '../../../shared/services/providers.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
	selector: 'app-contact-form',
	templateUrl: './contact-form.component.html',
	styleUrls: ['./contact-form.component.scss']
})

export class ContactFormComponent implements OnInit {

	contactFormText;
	alerts: any;
	provider: any = this.modalData.provider;
	currentContact: any = this.modalData.currentContact;
	editContact: boolean = this.modalData.editContact;

	constructor(
		private _sharedService: SharedService,
		private _providersService: ProvidersService,
		private _actionModalsService: ActionModalsService,
		public dialogRef: MatDialogRef<ContactFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
	) { }

	ngOnInit() {
		let cl = this._sharedService.user_selected_language;
		this.contactFormText = CFLanguages[cl];
		this.alerts = this.contactFormText.alerts;
	}

	createContact() {
		this.currentContact['provider_id'] = this.provider.provider_id;
		this._providersService.createContact(this.currentContact).subscribe(data => {
			this._providersService.getContacts(this.provider.provider_id);
			this.closeModal();
			let title = `${data.contact_name} ${data.contact_last_name}`;
			let text = this.alerts.alertContactCreatedSuccessfully;
			this._actionModalsService.buildSuccessModal(title, text);
		}, () => {
			let titel = this.alerts.alertContactCreateError;
			this._actionModalsService.alertError(titel);
		})
	}

	closeModal() {
		this.currentContact = {};
		this.dialogRef.close();
	}

	updateContact() {
		this._providersService.updateContactProvider(this.currentContact)
			.subscribe(data => {
				this.closeModal();
				let title = `${data.contact_name} ${data.contact_last_name}`;
				let text = this.alerts.alertContactUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
			}, () => {
				let titel = this.alerts.alertContactUpdateError;
				this._actionModalsService.alertError(titel);
			})
	}

	saveContact() {
		this.editContact ? this.updateContact() : this.createContact();
	}

}

class CFLanguages {
	static en = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Contact created successfully",
			"alertContactCreateError": "An error ocurred",
			"alertContactUpdatedSuccessfully": "Contact updated successfully",
			"alertContactUpdateError": "An error ocurred"
		},
		"modalTitleCreate":"Create contact ",
		"modalTitleUpdate":"Update contact",
		"contactName": "Name",
		"contactLastName": "Last Name",
		"contactEmail": "Email",
		"contactTelephone": "Telephone",
		"contactProvider": "Provider",
		"requiredFieldsMessage": "Fields marked * are required fields.",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};

	static de = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Kontakt erfolgreich erstellt",
			"alertContactCreateError": "Es ist ein Fehler aufgetreten",
			"alertContactUpdatedSuccessfully": "Kontakt erfolgreich aktualiziert",
			"alertContactUpdateError": "Es ist ein Fehler aufgetreten",
		},
		"modalTitleCreate":"Kontakt erstellen",
		"modalTitleUpdate":"Kontakt aktualizieren",
		"contactName": "Vorname",
		"contactLastName": "Nachname",
		"contactEmail": "Email",
		"contactTelephone": "Telefon",
		"contactProvider": "Liferant",
		"requiredFieldsMessage": "Mit * gekennzeichnete Felder sind Pflichtfelder.",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};

	static es = {
		"alerts": {
			"alertContactCreatedSuccessfully": "Contacto creado exitosamente",
			"alertContactCreateError": "Ha ocurrido un error",
			"alertContactUpdatedSuccessfully": "Contacto actualizado exitosamente",
			"alertContactUpdateError": "Ha ocurido un error"
		},
		"modalTitleCreate":"Crear contacto",
		"modalTitleUpdate":"Actualizar contacto",
		"contactName": "Nombre",
		"contactLastName": "Apellido",
		"contactEmail": "Email",
		"contactTelephone": "Teléfono",
		"contactProvider": "Proveedor",
		"requiredFieldsMessage": "Los campos marcados con * son requeridos.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
