import { Component, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { ProvidersService } from '../../shared/services/providers.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-providers',
	templateUrl: './providers.component.html',
	styleUrls: ['./providers.component.scss']
})

export class ProvidersComponent implements OnInit, OnDestroy {

	providers: any = [];
	selectedProvider: any = {};
	provider_id;
	// subscriptions
	providersList$;
	providerToPushOrUpdate$;
	constructor(
		private _providersService: ProvidersService,
		private activeRoute: ActivatedRoute,
		private router: Router,
	) {
		this.providerToPushOrUpdate$ = this._providersService.providerToPushOrUpdate.subscribe(data => {
			let provider_id = data.provider_id;
			if (!provider_id) return; // case {} an initialization
			let index = this.providers.findIndex(obj => obj['provider_id'] == provider_id);
			if (index == -1) {  // provider was created 
				this.providers.push(data);
				this.router.navigate(['app/company/providers', provider_id]);
			} else { // provider was updated
				this.providers.splice(index, 1, data);
				this.selectedProvider = data;
			}
		});
		this.activeRoute.params.pipe(map(params => params['provider_id'])).subscribe(provider_id => {
			this.provider_id = provider_id;
			this.setSelectedProvider();
		});
		this.providersList$ = this._providersService.currentProviderList.subscribe(data => {
			this.providers = data;
			this.setSelectedProvider();
		});
	}

	ngOnInit() {
		this._providersService.getProviders();
	}

	setSelectedProvider() {
		if (this.providers.length > 0) {
			let found = this.providers.find((obj) => obj['provider_id'] == this.provider_id);
			this.selectedProvider = found ? found : this.providers[0];
			this.router.navigate(['app/company/providers', this.selectedProvider['provider_id']]);
			window.scroll(0, 0);
		} else {
			this.selectedProvider = {};
		}
	}

	ngOnDestroy() {
		this.providersList$.unsubscribe();
		this.providerToPushOrUpdate$.unsubscribe();
	}
}
