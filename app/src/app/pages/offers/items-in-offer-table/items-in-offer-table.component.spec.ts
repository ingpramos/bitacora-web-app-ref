import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOfferTableComponent } from './items-in-offer-table.component';

describe('ItemsInOfferTableComponent', () => {
  let component: ItemsInOfferTableComponent;
  let fixture: ComponentFixture<ItemsInOfferTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOfferTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOfferTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
