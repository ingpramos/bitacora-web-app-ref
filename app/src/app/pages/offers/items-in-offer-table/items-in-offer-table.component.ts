import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { ItemInOfferFormComponent } from "../item-in-offer-form/item-in-offer-form.component";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsInOfferTableColumnsFormComponent } from "../items-in-offer-table-columns-form/items-in-offer-table-columns-form.component";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-items-in-offer-table",
    templateUrl: "./items-in-offer-table.component.html",
    styleUrls: ["./items-in-offer-table.component.scss"]
})

export class ItemsInOfferTableComponent implements OnInit, OnChanges, OnDestroy {
    // to get Information about the current order
    @Input() currentOffer: any;
    // Component properties
    companyCurrency: string = "";
    currentLanguage:string;
    items = [];
    itemsInOfferText: any;
    tableColumns: any = {};
    defaultTableColumnsObj: any;
    offerTotalFooter: any = {};
    alerts: any;
    // subscriptions
    itemsList$;
    columnsNames$;
    totalModel$;

    constructor(
        private _idocsService : InternalDocumentsService,
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.companyCurrency = this._sharedService.currency;
        this.currentLanguage = this._sharedService.user_selected_language; // Language Initalitation
        this.itemsInOfferText = IIOFFERTLanguages[this.currentLanguage];
        this.alerts = this.itemsInOfferText.alerts;
        this.defaultTableColumnsObj = this.itemsInOfferText.columns;               
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_offers", this.defaultTableColumnsObj);
    }

    ngOnInit() {
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => this.tableColumns = data);
        //to update the items when new ones are inserted
        this.itemsList$ = this._idocsService.currentItemsList.subscribe(data => {
            this.items = data;
            this._idocsService.setItemsOfCurrentDocument(this.items); // to update data for total price model
            this._idocsService.calculateDocumentTotalPriceModel();
        });
        this.totalModel$ = this._idocsService.documentTotalPriceModelObs.subscribe(data => {
            this.offerTotalFooter = data;
            this.currentOffer.subTotal = this.offerTotalFooter.subTotal;
            this.currentOffer.vat = this.offerTotalFooter.vat;
            this.currentOffer.total = this.offerTotalFooter.total;
            this._sharedService.stopLoading();
        });
    }

    ngOnChanges(changes) {
        let offer_id = changes.currentOffer.currentValue.internal_document_id;
        if (offer_id) {
            this._idocsService.getItemsFromInternalDocument(offer_id);
            this._sharedService.startLoading();
        }
        if (!offer_id) {
            this._idocsService.updateItemsList([]);
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
        this.totalModel$.unsubscribe();
    }

    editItemInOffer(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(ItemInOfferFormComponent, { width: "1000px", data: initialState });
    }

    editTableColumns() {
        let initialState = {
            defaultTableColumnsObj: this.defaultTableColumnsObj,
            currentTableColumns: this.tableColumns
        };
        const dialogRef = this.dialog.open(ItemsInOfferTableColumnsFormComponent, { width: "1000px", data: initialState });
	}
	
	deleteItem(item, index){
		item["item_type"] == "item" ? this.deleteItemFromOffer(item,index) : this.deleteAssemblyFromOffer(item,index);
	}
	
    deleteItemFromOffer(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromOffer;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let iiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteItemFromInternalDocument(iiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
	}
	
	deleteAssemblyFromOffer(item, index) {
        let title = item.item_name;
        let text = this.alerts.confirmDeleteItemFromOffer;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let aiid_id = item.item_in_internal_document_id;
                this._idocsService.deleteAssemblyFromInternalDocument(aiid_id).subscribe(
                    data => {
                        if (data) {
                            let title = item.item_name;
                            let text = this.alerts.alertItemDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.items.splice(index, 1);
                            this._idocsService.updateItemsList(this.items);
                        }
                    },
                    () => {
                        let text = this.alerts.alertItemDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }
}

class IIOFFERTLanguages {
    static en = {
        title: "Offer Details",
        totalWithoutVAT: "Total without VAT",
        vatFrom: "VAT from ",
        totalWithVAT: "Total with VAT",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Item Code",
            item_name: "Item Description",
            item_amount: "Amount",
            item_sell_price: "Price(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOffer: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfully: "Item deleted successfully",
            alertItemDeleteError: "An error ocurred"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns"
    };

    static de = {
        title: "Angebot Details",
        totalWithoutVAT: "Total exkl. MwSt.",
        vatFrom: "MwSt von",
        totalWithVAT: "Total inkl. MwSt.",
        columns: {
            item_position: "Pos",
            item_type: "Typ",
            item_code: "Artikel Code",
            item_name: "Artikelbezeichnung",
            item_amount: "Menge",
            item_sell_price: "Preis(E)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOffer: "Sind Sie sicher das Sie dieses Artikel löschen möchte?",
            alertItemDeletedSuccessfully: "Artikel erfolgreich gelöscht",
            alertItemDeleteError: "Es ist ein Fehler aufgetreten"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Spaltennamen bearbeiten"
    };

    static es = {
        title: "Detalles de Oferta",
        totalWithoutVAT: "Total sin IVA",
        vatFrom: "IVA de ",
        totalWithVAT: "Total Con IVA",
        columns: {
            item_position: "Pos",
            item_type: "Tipo",
            item_code: "Código del Item",
            item_name: "Descripción",
            item_amount: "Cantidad",
            item_sell_price: "Precio(U)",
            item_total_price: "Total"
        },
        alerts: {
            confirmDeleteItemFromOffer: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfully: "Item eliminado exitosamente",
            alertItemDeleteError: "Ha ocurrido un error"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Columnas"
    };
}
