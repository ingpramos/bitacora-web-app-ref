import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportExcelIntoOfferComponent } from './import-excel-into-offer.component';

describe('ImportExcelIntoOfferComponent', () => {
  let component: ImportExcelIntoOfferComponent;
  let fixture: ComponentFixture<ImportExcelIntoOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportExcelIntoOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportExcelIntoOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
