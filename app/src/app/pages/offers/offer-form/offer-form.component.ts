import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { OffersService } from "../../../shared/services/offers.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { CompanyService } from "../../../shared/services/company.service";
import { DateAdapter } from '@angular/material/core';

@Component({
    selector: "app-offer-form",
    templateUrl: "./offer-form.component.html",
    styleUrls: ["./offer-form.component.scss"]
})
export class OfferFormComponent implements OnInit, OnChanges {
    // component's Inputs
    @Input() editOffer;
    @Input() newOffer;
    @Input() currentOffer: any;
    // component's output
    @Output() useForm = new EventEmitter<boolean>();
    // component's Properties
    sessionInfo: any;
    offerFormText: any;
    alerts: any;
    clients: any = {}; // a formatted object to play nice with the select
    client_contacts: any = {}; // a formatted object to play nice with the select
    paymentMethods: any = {};
    paymentConditions: any = {};
    deliveryMethods: any = {};
    deliveryConditions: any = {};
    vats = [];
    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _clientsService: ClientsService,
        private _offersService: OffersService,
        private _companyService: CompanyService,
        private adapter: DateAdapter<any>
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        // change the date picker adapter to current language
        this.adapter.setLocale(cl);
        this.offerFormText = OFFERFCLanguage[cl];
        this.alerts = this.offerFormText.alerts;
        this._clientsService.getFormattedClients().subscribe(data => {
            this.clients = data;
            if (this.editOffer) this.callClientContacts();
            if (this.newOffer) this.currentOffer.offer_vat_porcentage = 19;
        });
        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.vats = this.sessionInfo.company_vats;
            if (this.newOffer) this.callNextOfferNumber();
            this.paymentMethods = this._companyService.formatPaymentMethods(this.sessionInfo.payment_methods);
            this.paymentConditions = this._companyService.formatPaymentConditions(this.sessionInfo.payment_conditions);
            this.deliveryMethods = this._companyService.formatDeliveryMethods(this.sessionInfo.delivery_methods);
            this.deliveryConditions = this._companyService.formatDeliveryConditions(this.sessionInfo.delivery_conditions);
        });
    }
    ngOnChanges(changes) {
        if (this.newOffer && this.sessionInfo) {
            this.callNextOfferNumber();
        }
    }

    saveOffer() {
        this.editOffer ? this.updateOffer() : this.createOffer();
    }

    createOffer() {
        this.currentOffer["offer_state"] = 1;
        this.findIds();
        this._offersService.createOffer(this.currentOffer).subscribe(
            data => {
                if (data) {
                    this._offersService.passOfferToPushOrUpdate(data);
                    let title = this.currentOffer.offer_number;
                    let text = this.alerts.alertOfferCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let title = this.alerts.alertOfferCreationError;
                this._actionModalsService.alertError(title);
            }
        );
    }

    updateOffer() {
        this.findIds();
        this._offersService.updateOffer(this.currentOffer).subscribe(
            data => {
                if (data) {
                    this._offersService.passOfferToPushOrUpdate(data);
                    let title = this.currentOffer.offer_number;
                    let text = this.alerts.alertOfferUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let text = this.alerts.alertOfferUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    callClientContacts() {
        let client = this.currentOffer.client_name;
        let client_id = this.clients["getClient_id"](client);
        this.currentOffer.client_id = client_id;
        this._clientsService.getFormattedContacts(client_id).subscribe(data => {
            this.client_contacts = data;
        });
    }

    findIds() {
        this.currentOffer.offer_user_id = this.sessionInfo.user_id;
        this.currentOffer.company_id = this.sessionInfo.company_id;
        this.currentOffer.client_contact_id = this.client_contacts["getCc_id"](this.currentOffer.contact);
        this.currentOffer.payment_method_id = this.paymentMethods["getPaymentMethod_id"](this.currentOffer.payment_method_name);
        this.currentOffer.payment_condition_id = this.paymentConditions["getPaymentCondition_id"](this.currentOffer.payment_condition_description);
        this.currentOffer.delivery_method_id = this.deliveryMethods["getDeliveryMethod_id"](this.currentOffer.delivery_method_name);
        this.currentOffer.delivery_condition_id = this.deliveryConditions["getDeliveryCondition_id"](this.currentOffer.delivery_condition_description);
    }

    callNextOfferNumber() {
        this._offersService.getCurrentOfferNumber().subscribe(data => {
            this.currentOffer.offer_number = `${data.document_prefix}${data.document_current_number + 1}`;
        });
    }

    forgetActions() {
        this.currentOffer = {};
        this.useForm.emit(false);
    }
}

class OFFERFCLanguage {
    static en = {
        alerts: {
            alertOfferUpdatedSuccessfully: "Offer updated successfully",
            alertOfferUpdateError: "Offer could not be updated",
            alertOfferCreatedSuccessfully: "Offer created successfully",
            alertOfferCreationError: "Offer could not be created"
        },
        offerNumber: "Offer Number",
        client: "Client",
        clientContact: "Contact",
        paymentConditions: "Payment Conditions",
        paymentMethod: "Payment Method",
        wayOfDelivery: "Way of delivery",
        deliveryConditions: "Delivery Conditions",
        offerVATValue: "VAT %",
        expirationDate:"Expire at",
        deliveryDate: "Date of delivery",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertOfferUpdatedSuccessfully: "Angebot erfolgreich aktualiziert",
            alertOfferUpdateError: "Es ist ein Fehler aufgetreten",
            alertOfferCreatedSuccessfully: "Angebot erfolgreich erstelt",
            alertOfferCreationError: "Es ist ein Fehler aufgetreten"
        },
        offerNumber: "Angebotsnummer",
        client: "Kunde",
        clientContact: "Kontak",
        paymentConditions: "Zahlungsbedingungen",
        paymentMethod: "Zahlungsart",
        wayOfDelivery: "Liefertart",
        deliveryConditions: "Lieferbedingungen",
        offerVATValue: "MwSt. %",
        expirationDate:"Gultig bis",
        deliveryDate: "Lifertermin",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertOfferUpdatedSuccessfully: "Oferta actualizada exitosamente",
            alertOfferUpdateError: "La offerta no pudo ser actualizada",
            alertOfferCreatedSuccessfully: "Oferta creada existosamente",
            alertOfferCreationError: "La oferta no pudo ser creada"
        },
        offerNumber: "Número de Oferta",
        client: "Cliente",
        clientContact: "Contacto",
        paymentConditions: "Condiciones de Pago",
        paymentMethod: "Metodo de Pago",
        wayOfDelivery: "Método de Entrega",
        deliveryConditions: "Condiciones de entrega",
        offerVATValue: "IVA %",
        expirationDate:"Valida hasta",
        deliveryDate: "Fecha de Entrega",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
