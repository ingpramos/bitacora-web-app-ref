import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { OffersService } from "../../../shared/services/offers.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: "app-offers-list",
    templateUrl: "./offers-list.component.html",
    styleUrls: ["./offers-list.component.scss"]
})
export class OffersListComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activatedRoute.snapshot["_urlSegment"].segments[1].path;
    openOffersView = this.componentUrl === "open-offers" ? true : false;
    @Input() offers;

    offersListText: any;
    alerts: any;
    offerToHandle = {};
    editOffer: boolean = false;
    newOffer: boolean = false;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";

    private paramMap$;
    private id_id;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _offersService: OffersService,
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.offersListText = OFFERSLLanguage[cl];
        this.alerts = this.offersListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.id_id = +paramMap.get("offer_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForOffers();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchOffers() {
        this.subject.next(this.searchPattern);
    }

    searchForOffers() {
        let offer_state = this.openOffersView ? 1 : 0;
        this._offersService.getOffersByPattern(offer_state, this.searchPattern);
    }

    closeSearch() {
        let offer_state = this.openOffersView ? 1 : 0;
        this.searchPattern = "";
        this._offersService.getOffers(offer_state);
        this.showSearch = false;
    }

    startSearch() {
        this.editOffer = false;
        this.newOffer = false;
        this.showSearch = true;
    }

    startEditOffer(offer) {
        this.offerToHandle = JSON.parse(JSON.stringify(offer));
        this.newOffer = false;
        this.editOffer = true;
    }

    startNewOffer() {
        this.editOffer = false;
        this.offerToHandle = {};
        this.newOffer = true;
    }

    closeOffer(offer) {
        let offer_id = offer.internal_document_id;
        this._offersService.closeOffer(offer_id).subscribe(data => {
            this.router.navigate(["app/closed-offers", offer_id]);
        });
    }

    deleteOffer(offer, index) {
        let title = offer.offer_number;
        let text = this.alerts.confirmDeleteOffer;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let offer_id = offer.internal_document_id;
                this._offersService.deleteOffer(offer_id).subscribe(
                    data => {
                        if (data) {
                            let title = offer.offer_number;
                            let text = this.alerts.alertOfferDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.offers.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    () => {
                        let text = this.alerts.alertOfferDeleteError;
                        this._actionModalsService.alertError(text);
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editOffer = false;
        this.newOffer = false;
        this.offerToHandle = {};
    }
}

class OFFERSLLanguage {
    static en = {
        alerts: {
            confirmDeleteOffer: "Are you sure you want to delete this Offer ?",
            alertOfferDeletedSuccessfully: "Offer deleted successfully",
            alertOfferDeleteError: "An error ocurred"
        },
        newOffer: "New Offer",
        searchOffers: "Search Offers",
        newOrderConfirmation: "New Order Confirmation",
        openOffers: "Open Offers",
        closedOffers: "Closed Offers",
        ordersConfirmed: "Confirmed Orders",
        editOffer: "Edit Offer",
        deleteOffer: "Delete Offer",
        closeOffer: "Close Offer",
        editOrderConfirmation: "Edit Order Confirmation",
        deleteOrderConfirmation: "Delete Order Confirmation",
        creationDate: "Created at",
        expirationDate:"Expire at",
        deliveryDate: "Delivery"
    };

    static de = {
        alerts: {
            confirmDeleteOffer: "Wollen Sie dieses Angebot löschen ?",
            alertOfferDeletedSuccessfully: "Angebot erfolgreich gelöscht",
            alertOfferDeleteError: "Es ist ein Fehler aufgetreten"
        },
        newOffer: "Neu Angebot",
        searchOffers: "Angeboten suchen",
        newOrderConfirmation: "New Auftragbestätigung",
        openOffers: "Offene Angeboten",
        closedOffers: "Geschlossene Angeboten",
        ordersConfirmed: "Auftragsbestätigungen",
        editOffer: "Angebot bearbeiten",
        deleteOffer: "Angebot löschen",
        closeOffer: "Angebot schließen",
        editOrderConfirmation: "AB Bearbeiten",
        deleteOrderConfirmation: "AB Löschen",
        creationDate: "Erstellt",
        expirationDate:"Gultig bis",
        deliveryDate: "Lifertermin"
    };

    static es = {
        alerts: {
            confirmDeleteOffer: "Esta seguro que deseas borrar la siguiente oferta ?",
            alertOfferDeletedSuccessfully: "Oferta eliminada exitosamente",
            alertOfferDeleteError: "Ha ocurrido un error"
        },
        newOffer: "Nueva Cotización",
        searchOffers: "Buscar Cotización",
        newOrderConfirmation: "Nueva Confirmación De Pedido",
        openOffers: "Cotizaciones Abiertas",
        closedOffers: "Cotizaciones  Cerradas",
        ordersConfirmed: "Pedidos Confirmados",
        editOffer: "Editar Cotización",
        deleteOffer: "Eliminar Cotización",
        closeOffer: "Cerrar Oferta",
        editOrderConfirmation: "Editar Confirmacion",
        deleteOrderConfirmation: "Borrar Confirmacion",
        creationDate: "Creada",
        expirationDate:"Valida hasta",
        deliveryDate: "Entrega"
    };
}
