import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { map } from "rxjs/operators";
import { OffersService } from "../../shared/services/offers.service";
import { InternalDocumentsService } from "../../shared/services/internal-documents.service";

@Component({
    selector: "app-offers",
    templateUrl: "./offers.component.html",
    styleUrls: ["./offers.component.scss"]
})
export class OffersComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activeRoute.snapshot["_urlSegment"].segments[1].path;
    openOffersView = this.componentUrl === "open-offers" ? true : false;
    zeroOffers: boolean = true; // use to desable header menu
    // component porperties
    offers = [];
    offer_id;
    selectedOffer = {};
    // subscriptions
    offerToPushOrUpdate$;
    offersList$;
    constructor(
        private _idocsService: InternalDocumentsService, // this service is shared by offers, ocs, invocies
        private _offersService: OffersService,
        private activeRoute: ActivatedRoute,
        private router: Router
    ) {
        // to clean any data when the component initialize
        this._offersService.passOfferToPushOrUpdate({});
        this.activeRoute.params.pipe(map(params => params["offer_id"])).subscribe(offer_id => {
            this.offer_id = offer_id;
            this.setSelectedOffer();
        });
        this.offerToPushOrUpdate$ = this._offersService.offerToPushOrUpdate.subscribe(data => {
            let offer_id = data.internal_document_id;
            if (!offer_id) return; // case {} an initialization
            let index = this.offers.findIndex(obj => obj["internal_document_id"] == offer_id);
            if (index == -1) {
                // offer was created
                this.offers.push(data);
                this.router.navigate(["app/" + this.componentUrl, offer_id]);
            } else {
                // offer was updated
                this.offers.splice(index, 1, data);
                this.router.navigate(["app/" + this.componentUrl, offer_id]);
                this.selectedOffer = data;
                this._idocsService.setCurrentDocument(this.selectedOffer); // to update total cost model
                this._idocsService.calculateDocumentTotalPriceModel(); // to update total cost model
            }
        });
        this.offersList$ = this._offersService.currentOffersList.subscribe(data => {
            this.offers = data;
            this.setSelectedOffer();
        });
    }

    ngOnInit() {
        this.openOffersView ? this._offersService.getOffers(1) : this._offersService.getOffers(0);
    }

    ngOnDestroy() {
        this.offerToPushOrUpdate$.unsubscribe();
        this.offersList$.unsubscribe();
        this._offersService.updateOffersList([]);
    }

    setSelectedOffer() {
        this.offers.length > 0 ? (this.zeroOffers = false) : (this.zeroOffers = true);
        if (this.offers.length > 0) {
            let found = this.offers.find(obj => obj["internal_document_id"] == this.offer_id);
            this.selectedOffer = found ? found : this.offers[0];
            this.router.navigate(["app/" + this.componentUrl, this.selectedOffer["internal_document_id"]]);
            this._idocsService.setCurrentDocument(this.selectedOffer); // to use info to update total price model
            window.scroll(0, 0);
        } else {
            this.selectedOffer = {};
            this._idocsService.setCurrentDocument(this.selectedOffer);
            this.offers.length > 0 ? (this.zeroOffers = false) : (this.zeroOffers = true);
        }
    }
}
