import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemInOfferFormComponent } from './item-in-offer-form.component';

describe('ItemInOfferFormComponent', () => {
  let component: ItemInOfferFormComponent;
  let fixture: ComponentFixture<ItemInOfferFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInOfferFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInOfferFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
