import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { CurrencyPipe } from "@angular/common";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-item-in-offer-form",
    templateUrl: "./item-in-offer-form.component.html",
    styleUrls: ["./item-in-offer-form.component.scss"]
})
export class ItemInOfferFormComponent implements OnInit {
    // component's Properties
    itemFormText: any;
    currentLanguage:string;
    alerts: any;
    currentItem: any = this.modalData.currentItem;
    constructor(
        private _currencyPipe: CurrencyPipe,
        private _sharedService: SharedService,
        private _idocsService: InternalDocumentsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<ItemInOfferFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemFormText = IIOFFERFCLanguage[this.currentLanguage];
        this.alerts = this.itemFormText.alerts;
    }

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentItem = JSON.parse(JSON.stringify(this.currentItem));
        // preformat the values to show correct format when opening this component
        let isp = Number(this.currentItem["item_sell_price"]).toFixed(2);
        let itp = Number(this.currentItem["item_total_price"]).toFixed(2);
        this.currentItem["item_sell_price_to_show"] = this._sharedService.parseToLocaleString(isp).trim();
        this.currentItem["item_total_price"] = this._sharedService.parseToLocaleString(itp).trim();
    }

    closeModal() {
        this.currentItem = {};
        this.dialogRef.close();
    }

    save(item) {
        // format the string as Number before saving
        item["item_sell_price"] = this._sharedService.parseFormattedStringAsNumber(item["item_sell_price_to_show"]);
        item["item_type"] == "item" ? this.saveItem(item) : this.saveAssembly(item);
    }

    saveItem(item) {
        this._idocsService.updateItemInInternalDocument(item).subscribe(
            data => {
                if (data) {
                    let offer_id = item.internal_document_id;
                    this._idocsService.getItemsFromInternalDocument(offer_id);
                    this.closeModal();
                    let title = item.item_name;
                    let text = this.alerts.alertItemUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertItemUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    saveAssembly(assembly) {
        this.renameProperties(assembly);
        this._idocsService.updateAssemblyInInternalDocument(assembly).subscribe(
            data => {
                if (data) {
                    let offer_id = assembly.internal_document_id;
                    this._idocsService.getItemsFromInternalDocument(offer_id);
                    this.closeModal();
                    let title = assembly.item_name;
                    let text = this.alerts.alertItemUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertItemUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }
    // to send the right property names to the rest api
    renameProperties(assembly) {
        assembly["aiid_id"] = assembly["item_in_internal_document_id"];
        assembly["assembly_amount"] = assembly["item_amount"];
        assembly["assembly_sell_price"] = assembly["item_sell_price"];
        assembly["assembly_in_internal_document_details"] = assembly["item_in_internal_document_details"];
    }

    calculateTotalPrice() {
        let stringValue = this.currentItem["item_sell_price_to_show"];
        let priceAsNumber: Number = this._sharedService.parseFormattedStringAsNumber(stringValue);
        let itemTotalPrice = Number(this.currentItem["item_amount"]) * Number(priceAsNumber);
        this.currentItem["item_total_price"] = this._currencyPipe.transform(itemTotalPrice, this._sharedService.currency, "", "", this.currentLanguage);
    }
}

class IIOFFERFCLanguage {
    static en = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item updated successfully",
            alertItemUpdateError: "An error ocurred"
        },
        modalTitle: "Update item information",
        itemCode: "Item Number",
        itemOrderNumber: "Order Number",
        itemName: "Item Name",
        itemAmount: "Amount",
        itemOfferedPrice: "Price",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Received",
        itemLastPerson: "Last Person",
        itemDeliveryDate: "Delivery Date",
        itemInInternalDocumentDetails: "Details",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertItemUpdatedSuccessfully: "Artikel erfolgreich aktualiziert",
            alertItemUpdateError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel information aktualizieren",
        itemCode: "Artikel Code",
        itemOrderNumber: "Bestellnummer",
        itemName: "Name",
        itemAmount: "Menge",
        itemOfferedPrice: "Preis",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Erhalten",
        itemLastPerson: "Letztes Person",
        itemDeliveryDate: "Lieferdatum",
        itemInInternalDocumentDetails: "Details",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertItemUpdatedSuccessfully: "Item actualizado exitosamente",
            alertItemUpdateError: "Ha ocurrido un error"
        },
        modalTitle: "Actualizar informacion del articulo",
        itemCode: "Código Del Item",
        itemOrderNumber: "Numero de Compra",
        itemName: "Nombre",
        itemAmount: "Cantidad",
        itemOfferedPrice: "Precio",
        itemsTotalPrice: "Total",
        itemReceivedAmount: "Recibido",
        itemLastPerson: "Ultima Persona",
        itemDeliveryDate: "Fecha de Recibido",
        itemInInternalDocumentDetails: "Detalles",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
