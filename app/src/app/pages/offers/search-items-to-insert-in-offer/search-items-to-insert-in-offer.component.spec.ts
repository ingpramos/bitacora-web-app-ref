import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInOfferComponent } from './search-items-to-insert-in-offer.component';

describe('SearchItemsToInsertInOfferComponent', () => {
  let component: SearchItemsToInsertInOfferComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
