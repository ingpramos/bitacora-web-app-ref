import { Component, OnInit, Inject, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { Subject } from "rxjs";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { debounceTime } from "rxjs/operators";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-search-groups-to-insert-in-offer",
    templateUrl: "./search-groups-to-insert-in-offer.component.html",
    styleUrls: ["./search-groups-to-insert-in-offer.component.scss"]
})
export class SearchGroupsToInsertInOfferComponent implements OnInit, OnDestroy {
    user_id;
    allChecked: boolean;
    groupsTableText: any;
    groups = [];
    selectedGroups = [];
    currentOffer: any = this.modalData.currentOffer;
    groupHeaderText: any = this.modalData.groupHeaderText;
    alerts;
    // search related variables
    searchPattern: string;
    private subject: Subject<string> = new Subject();
    // subscriptions
    groupsList$;
    constructor(
        public _idocsService: InternalDocumentsService,
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<SearchGroupsToInsertInOfferComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.groupsTableText = SGTIIOLanguages[cl];
        this.alerts = this.groupsTableText.alerts;
        this.user_id = this._sharedService.user_id;
    }

    ngOnInit() {
        this.groupsList$ = this._groupsService.currentGroupsList.subscribe(data => {
            this.groups = data;
        });
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.groups = [];
                this.selectedGroups = [];
                return;
            } else {
                this._groupsService.getGroupsByPattern(0, this.searchPattern);
            }
        });
    }

    ngOnDestroy() {
        this.groupsList$.unsubscribe();
        this._groupsService.updateGroupList([]);
    }

    searchGroups() {
        this.subject.next(this.searchPattern);
    }

    closeModal() {
        this.dialogRef.close();
    }

    applyChangesToList(allChecked) {
        this.groups.forEach((obj: any) => (obj.is_checked = allChecked));
        this.getSelectedGroups();
    }

    getSelectedGroups() {
        this.selectedGroups = this.groups.filter((obj: any) => obj.is_checked === true);
    }

    saveGroups() {
        let offer_id = this.currentOffer.internal_document_id;
        let formattedGroups = this.formatGroupsToInsert(this.selectedGroups, offer_id);
        this._idocsService.insertAssembliesInInternalDocument(formattedGroups).subscribe(
            data => {
                this._idocsService.getItemsFromInternalDocument(offer_id);
                this.closeModal();
                let title = this.currentOffer.offer_number;
                let text = `${data.length.toString()} ${this.alerts.alertAssembliesInsertedSuccessfully}`;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertAssembliesInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatGroupsToInsert(collection, offer_id) {
        collection.forEach(obj => {
            obj["internal_document_id"] = offer_id;
            obj["assembly_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["assembly_amount_to_show"]);
            obj["assembly_sell_price"] = this._sharedService.parseFormattedStringAsNumber(obj["assembly_sell_price_to_show"]);
            obj["user_id"] = this.user_id;
        });
        return collection;
    }
}

class SGTIIOLanguages {
    static en = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Groups inserted successfully",
            alertAssembliesInsertError: "An error ocurred"
        },
        modalTitle: "Insert groups in offer",
        assembly_position: "Pos",
        assembly_number: "Group number",
        assembly_name: "Group name",
        assembly_amount: "Menge",
        assembly_sell_price: "Price",
        selected: "Selected",
        searchSubAssemblies: " Search groups",
        assembliesFound: "Groups Found",
        assembliesSelected: "Selected Groups",
        saveButton: "Insert groups",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertAssembliesInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Baugruppe in Angebot einfügen",
        assembly_position: "Pos",
        assembly_number: "Baugruppe Nummer",
        assembly_name: "Baugruppe Name",
        assembly_amount: "Menge",
        assembly_sell_price: "Preis",
        selected: "Ausgewählt",
        searchSubAssemblies: " Baugruppen suchen",
        assembliesFound: "Baugruppen gefunden",
        assembliesSelected: "ausgewählte Baugruppen",
        saveButton: "Baugruppen einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertAssembliesInsertedSuccessfully: "Grupos insertados exitosamente",
            alertAssembliesInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar grupos en oferta",
        assembly_position: "Pos",
        assembly_number: "Número del grupo",
        assembly_name: "Nombre del grupo",
        assembly_amount: "Cantidad",
        assembly_sell_price: "Precio",
        selected: "Seleccionados",
        searchSubAssemblies: " Buscar grupos",
        assembliesFound: "Grupos econtrados",
        assembliesSelected: "Grupos seleccionados",
        saveButton: "Insertar Grupos",
        cancelButton: "Cancelar"
    };
}
