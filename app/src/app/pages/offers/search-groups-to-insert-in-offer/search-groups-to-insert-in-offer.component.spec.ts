import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchGroupsToInsertInOfferComponent } from './search-groups-to-insert-in-offer.component';

describe('SearchGroupsToInsertInOfferComponent', () => {
  let component: SearchGroupsToInsertInOfferComponent;
  let fixture: ComponentFixture<SearchGroupsToInsertInOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchGroupsToInsertInOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchGroupsToInsertInOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
