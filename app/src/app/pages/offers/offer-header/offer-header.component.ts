import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { SearchItemsToInsertInOfferComponent } from "../search-items-to-insert-in-offer/search-items-to-insert-in-offer.component";
import { ImportExcelIntoOfferComponent } from "../import-excel-into-offer/import-excel-into-offer.component";
import { ExcelService } from "../../../shared/services/excel.service";
import { OfferPdfService } from "../../../shared/services/pdfs/offer-pdf.service";
import { SearchGroupsToInsertInOfferComponent } from "../search-groups-to-insert-in-offer/search-groups-to-insert-in-offer.component";
import { InternalDocumentsService } from "../../../shared/services/internal-documents.service";

@Component({
    selector: "app-offer-header",
    templateUrl: "./offer-header.component.html",
    styleUrls: ["./offer-header.component.scss"]
})
export class OfferHeaderComponent implements OnInit, OnDestroy {
    @Input() currentOffer;
    @Input() zeroOffers: boolean; // to enable or disable menu items
    @Input() openOffersView: boolean; // to enable or disable menu items
    @Input() showMenu: boolean; // to show or hide three points menu

    itemsInOffer: number = 0;
    offerHeaderText;
    itemsSubscription;

    constructor(
        private _offerPdfService: OfferPdfService,
        private _idocsService: InternalDocumentsService,
        private _excelService: ExcelService,
        private _sharedService: SharedService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.offerHeaderText = OFFERHCLanguage[cl];
        this.itemsSubscription = this._idocsService.currentItemsList.subscribe(items => (this.itemsInOffer = items.length));
    }

    ngOnDestroy() {
        this.itemsSubscription.unsubscribe();
    }

    searchForItems() {
        let initialState = {};
        initialState["currentOffer"] = this.currentOffer;
        initialState["itemsInOffer"] = this.itemsInOffer;
        const dialogRef = this.dialog.open(SearchItemsToInsertInOfferComponent, { width: "1200px", data: initialState });
    }

    searchForAssemblies() {
        let initialState = {};
        initialState["currentOffer"] = this.currentOffer;
        initialState["itemsInOffer"] = this.itemsInOffer;
        const dialogRef = this.dialog.open(SearchGroupsToInsertInOfferComponent, { width: "1200px", data: initialState });
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentOffer"] = this.currentOffer;
            initialState["itemsInOffer"] = this.itemsInOffer;
            const dialogRef = this.dialog.open(ImportExcelIntoOfferComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }

    generatePdfOffer() {
        let document = this._offerPdfService.buildDocument(this.currentOffer);
        let documentName = `${this.currentOffer.offer_number} - ${this.currentOffer.client_name}`;
        this._offerPdfService.pdfMake.createPdf(document).download(documentName);
    }

    downloadExcelOffer() {
        let documentName = `${this.currentOffer.offer_number} - ${this.currentOffer.client_name}.xlsx`;
        let columnsToDelete = ["item_in_internal_document_id","internal_document_id","item_id","company_id","item_raw_material_dimensions","item_type","user_id"];
        let itemsInOffer = this._idocsService.getItemsOfCurrentDocument("excel");
        this._excelService.deleteObjectProperties(itemsInOffer, columnsToDelete);
        this._excelService.downLoadXlsxFile(itemsInOffer, documentName, this.currentOffer.offer_number);
    }
}

class OFFERHCLanguage {
    user_selected_language = "en";
    static en = {
        orderConfirmationNumber: "Order Number",
        offerNumber: "Offer Number",
        offerClient: "Client",
        offerContact: "Contact",
        contactTelephone: "Telephone",
        contactEmail: "Email",
        offerItems: "Items",
        offerCreationDate: "Offered at",
        insertItems: "Insert Items",
        insertAssemblies: "Insert Groups",
        downloadExcelFile: "Download Offer Excel",
        uploadCsv: "Upload Excel File",
        downloadPdfFile: "Download Offer PDF",
        filterTable: "Search Item List"
    };
    static de = {
        orderConfirmationNumber: "AB nummer",
        offerNumber: "Angebotsnummer",
        offerClient: "Kunde",
        offerContact: "Kontact",
        contactTelephone: "Telefon",
        contactEmail: "Email",
        offerItems: "Artikel",
        offerCreationDate: "Datum",
        insertItems: "Artikel einfügen",
        insertAssemblies: "Baugruppen einfügen",
        downloadExcelFile: "Excel Angebot herrunterladen",
        uploadCsv: "Excel Datei hochladen",
        downloadPdfFile: "PDF Angebot herrunterladen",
        filterTable: "Tabelle Suchen"
    };
    static es = {
        orderConfirmationNumber: "Nr. de pedido",
        offerNumber: "Nr. de Oferta",
        offerClient: "Cliente",
        offerContact: "Contacto",
        contactTelephone: "Teléfono",
        contactEmail: "Email",
        offerItems: "Articulos",
        offerCreationDate: "Fecha",
        insertItems: "Añadir Items",
        insertAssemblies: "Añadir Grupos",
        downloadExcelFile: "Descargar Pedido Excel",
        uploadCsv: "Importar Excel con Items",
        downloadPdfFile: "Descargar Cotización PDF",
        filterTable: "Buscar Item"
    };
}
