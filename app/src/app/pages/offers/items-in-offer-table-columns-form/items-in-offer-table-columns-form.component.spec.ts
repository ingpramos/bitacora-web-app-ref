import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsInOfferTableColumnsFormComponent } from './items-in-offer-table-columns-form.component';

describe('ItemsInOfferTableColumnsFormComponent', () => {
  let component: ItemsInOfferTableColumnsFormComponent;
  let fixture: ComponentFixture<ItemsInOfferTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsInOfferTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsInOfferTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
