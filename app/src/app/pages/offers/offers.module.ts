import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { OffersComponent } from './offers.component';
import { OffersListComponent } from './offers-list/offers-list.component';
import { OfferHeaderComponent } from './offer-header/offer-header.component';
import { OfferFormComponent } from './offer-form/offer-form.component';
import { ItemsInOfferTableComponent } from './items-in-offer-table/items-in-offer-table.component';
import { ItemInOfferFormComponent } from './item-in-offer-form/item-in-offer-form.component';
import { SearchItemsToInsertInOfferComponent } from './search-items-to-insert-in-offer/search-items-to-insert-in-offer.component';
import { ImportExcelIntoOfferComponent } from './import-excel-into-offer/import-excel-into-offer.component';
import { ItemsInOfferTableColumnsFormComponent } from './items-in-offer-table-columns-form/items-in-offer-table-columns-form.component';
import { SearchGroupsToInsertInOfferComponent } from './search-groups-to-insert-in-offer/search-groups-to-insert-in-offer.component';
// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatIconModule
} from '@angular/material';
import { DirectivesModule } from '../../shared/directives/directives.module';

const OFFERS_ROUTES: Routes = [
	{ path: '', component: OffersComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(OFFERS_ROUTES),
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatIconModule,
		PipesModule,
		DirectivesModule
	],
	declarations: [
		OffersComponent,
		OffersListComponent,
		OfferHeaderComponent,
		OfferFormComponent,
		ItemsInOfferTableComponent,
		ItemInOfferFormComponent,
		SearchItemsToInsertInOfferComponent,
		ImportExcelIntoOfferComponent,
		ItemsInOfferTableColumnsFormComponent,
		SearchGroupsToInsertInOfferComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		ItemInOfferFormComponent,
		SearchItemsToInsertInOfferComponent, 
		ImportExcelIntoOfferComponent,
		ItemsInOfferTableColumnsFormComponent,
		SearchGroupsToInsertInOfferComponent 
	]
})
export class OffersModule { }
