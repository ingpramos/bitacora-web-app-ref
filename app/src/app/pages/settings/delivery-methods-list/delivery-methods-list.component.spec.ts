import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryMethodsListComponent } from './delivery-methods-list.component';

describe('DeliveryMethodsListComponent', () => {
  let component: DeliveryMethodsListComponent;
  let fixture: ComponentFixture<DeliveryMethodsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryMethodsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryMethodsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
