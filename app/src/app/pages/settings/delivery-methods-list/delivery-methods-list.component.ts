import { Component, OnInit, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-delivery-methods-list",
    templateUrl: "./delivery-methods-list.component.html",
    styleUrls: ["./delivery-methods-list.component.scss"]
})
export class DeliveryMethodsListComponent implements OnInit, OnDestroy {
    deliveryMethods: any;
    deliveryMethodsListText: any = {};
    alerts: any;
    newDeliveryMethod: boolean = false;
    editDeliveryMethod: boolean = false;
    deliveryMethodToHandle: any = {};
    //subscriptions
    deliveryMethodToPushOrUpdate$: any;
    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.deliveryMethodsListText = DMLCLanguages[cl];
        this.alerts = this.deliveryMethodsListText.alerts;
    }

    ngOnInit() {
        this.deliveryMethods = this._sharedService.delivery_methods || [];
        this.deliveryMethodToPushOrUpdate$ = this._companyService.deliveryMethodToPushOrUpdate.subscribe(data => {
            let delivery_method_id = data.delivery_method_id;
            if (!delivery_method_id) return; // case {} an initialization
            let index = this.deliveryMethods.findIndex(obj => obj["delivery_method_id"] == delivery_method_id);
            if (index == -1) {
                // delivery method was created
                this.deliveryMethods.push(data);
            } else {
                // delivery method was updated
                this.deliveryMethods.splice(index, 1, data);
            }
            this._sharedService.delivery_methods = this.deliveryMethods;
        });
    }

    ngOnDestroy() {
        this.deliveryMethodToPushOrUpdate$.unsubscribe();
    }

    startEditDeliveryMethod(deliveryMethod) {
        this.deliveryMethodToHandle = Object.assign({}, deliveryMethod);
        this.newDeliveryMethod = false;
        this.editDeliveryMethod = true;
        window.scroll(0, 0);
    }

    startNewDeliveryMethod() {
        this.editDeliveryMethod = false;
        this.newDeliveryMethod = true;
        this.deliveryMethodToHandle = {};
    }

    deleteDeliveryMethod(dm, index) {
        let title = dm.delivery_method_name;
        let text = this.alerts.confirmDeleteDeliveryMethod;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let dm_id = dm.delivery_method_id;
                this._companyService.deleteDeliveryMethod(dm_id).subscribe(
                    data => {
                        if (data) {
                            this.deliveryMethods.splice(index, 1);
                            this._sharedService.delivery_methods = this.deliveryMethods;
                            let text = this.alerts.alertDMDeletedSucessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertDMDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editDeliveryMethod = false;
        this.newDeliveryMethod = false;
    }
}

class DMLCLanguages {
    static en = {
        alerts: {
            confirmDeleteDeliveryMethod: "Are you sure you want to delete this delivery method",
            alertDMDeletedSucessfully: "Delivery method successfully deleted",
            alertForeignKeyViolation: "This delivery method is still referenced in one of your documents",
            alertDMDeleteError: "An error ocurred"
        },
        deliveryMethodsTitle: "Delivery Methods",
        editDeliveryMethod: "Edit Delivery Method",
        deleteDeliveryMethod: "Delete Delivery Method"
    };
    static de = {
        alerts: {
            confirmDeleteDeliveryMethod: "Wollen Sie dieses Liefertart löschen?",
            alertDMDeletedSucessfully: "Liefertart erfolgreich gelöscht",
            alertForeignKeyViolation: "Diese Liefertart ist immer noch referenziert in einer Ihrer Dokumenten",
            alertDMDeleteError: "Es ist ein Fehler aufgetreten"
        },
        deliveryMethodsTitle: "Lifertarten",
        editDeliveryMethod: "Liefertart bearbeiten",
        deleteDeliveryMethod: "Liefetrart löschen"
    };
    static es = {
        alerts: {
            confirmDeleteDeliveryMethod: "Esta seguro que desea borrar este método de entrega ?",
            alertDMDeletedSucessfully: "Método de entrega eliminado exitosamente",
            alertForeignKeyViolation: "Este método de entrega está referenciado en alguno de sus documentos",
            alertDMDeleteError: "Ha ocurrido un error"
        },
        deliveryMethodsTitle: "Métodos De Entrega",
        editDeliveryMethod: "Editar Método de Entrega",
        deleteDeliveryMethod: "Eliminar Método de Entrega"
    };
}
