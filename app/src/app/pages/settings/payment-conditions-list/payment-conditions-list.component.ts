import { Component, OnInit, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-payment-conditions-list",
    templateUrl: "./payment-conditions-list.component.html",
    styleUrls: ["./payment-conditions-list.component.scss"]
})
export class PaymentConditionsListComponent implements OnInit, OnDestroy {
    paymentConditions;
    newPaymentCondition: boolean = false;
    editPaymentCondition: boolean = false;
    paymentConditionToHandle = {};
    paymentConditionsListText: any;
    alerts: any;
    // subscription
    paymentConditionToPushOrUpdate$;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.paymentConditionsListText = PCLCLanguages[cl];
        this.alerts = this.paymentConditionsListText.alerts;
    }

    ngOnInit() {
        this.paymentConditions = this._sharedService.payment_conditions;
        this.paymentConditionToPushOrUpdate$ = this._companyService.paymentConditionToPushOrUpdate.subscribe(data => {
            let payment_condition_id = data.payment_condition_id;
            if (!payment_condition_id) return; // case {} an initialization
            let index = this.paymentConditions.findIndex(obj => obj["payment_condition_id"] == payment_condition_id);
            if (index == -1) {
                // payment condition was created
                this.paymentConditions.push(data);
            } else {
                // payment condition was updated
                this.paymentConditions.splice(index, 1, data);
			}
			this._sharedService.payment_conditions = this.paymentConditions;
        });
    }

    ngOnDestroy() {
        this.paymentConditionToPushOrUpdate$.unsubscribe();
    }

    startEditPaymentCondition(paymentCondition) {
        this.paymentConditionToHandle = Object.assign({}, paymentCondition);
        this.newPaymentCondition = false;
        this.editPaymentCondition = true;
        window.scroll(0, 0);
    }

    startNewPaymentCondition() {
        this.editPaymentCondition = false;
        this.newPaymentCondition = true;
        this.paymentConditionToHandle = {};
    }

    deletePaymentCondition(pc, index) {
        let title = pc.payment_condition_description;
        let text = this.alerts.confirmDeletePaymentCondition;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let pc_id = pc.payment_condition_id;
                this._companyService.deletePaymentCondition(pc_id).subscribe(
                    data => {
                        if (data) {                           
							this.paymentConditions.splice(index, 1);
							this._sharedService.payment_conditions = this.paymentConditions;
							let text = this.alerts.alertPaymentConditionDeletedSuccessfully;							
							this._actionModalsService.buildSuccessModal(title, text);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertPaymentConditionDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editPaymentCondition = false;
        this.newPaymentCondition = false;
    }
}

class PCLCLanguages {
    static en = {
        alerts: {
            confirmDeletePaymentCondition: "Are you sure you want to delete this payment condition?",
            alertPaymentConditionDeletedSuccessfully: "Payment condition deleted successfully.",
            alertForeignKeyViolation: "This payment condition is still referenced in one of your documents",
            alertPaymentConditionDeleteError: "An error ocurred"
        },
        title: "Payment Conditions",
        editPaymentCondition: "Edit Payment Condition",
        deletePaymentCondition: "Delete Payment Condition"
    };
    static de = {
        alerts: {
            confirmDeletePaymentCondition: "Wollen Sie dieses Zahlungsbedinung Löschen ?",
            alertPaymentConditionDeletedSuccessfully: "Zahlungsbedinung erfolgreich gelöscht",
            alertForeignKeyViolation: "Diese Zahlungsbedinung ist immer noch referenziert in einer Ihrer Dokumenten",
            alertPaymentConditionDeleteError: "Es ist ein Fehler aufgetreten"
        },
        title: "Zahlungsbediungen",
        editPaymentCondition: "Zahlungsbedinung bearbeiten",
        deletePaymentCondition: "Zahlungsbedinung löschen"
    };
    static es = {
        alerts: {
            confirmDeletePaymentCondition: "Esta seguro que desea borrar esta condicion pago ?",
            alertPaymentConditionDeletedSuccessfully: "Condición de pago eliminado exitosamente",
            alertForeignKeyViolation: "Esta condición de pago está referenciada en alguno de sus documentos",
            alertPaymentConditionDeleteError: "Ha ocurrido un error"
        },
        title: "Condiciones de Pago",
        editPaymentCondition: "Editar Condicion de Pago",
        deletePaymentCondition: "Eliminar Condicion de Pago"
    };
}
