import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { ActivitiesService } from '../../../shared/services/activities.service';

@Component({
	selector: 'app-activity-form',
	templateUrl: './activity-form.component.html',
	styleUrls: ['./activity-form.component.scss']
})
export class ActivityFormComponent implements OnInit {

	@Input() editActivity;
	@Input() newActivity;
	@Input() currentActivity;
	@Output() closeForm = new EventEmitter<boolean>();

	activityFormText:any;
	alerts:any;
	
	constructor(
		private _sharedService: SharedService,
		private _activitiesService: ActivitiesService, 
		private _actionModalsService : ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.activityFormText = AFCLanguage[cl];
		this.alerts = this.activityFormText.alerts;
	}

	ngOnInit() {

	}

	saveActivity() {
		this.newActivity ? this.createActivity() : this.updateActivity();
	}

	createActivity() {
		this._activitiesService.createActivity(this.currentActivity).subscribe(data => {
			if (data) {
				this._activitiesService.passActivityToPushOrUpdate(data);
				let title = data.activity_name;
				let text = this.alerts.alertActivityCreatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title,text);
				this.closeForm.emit(false);
			}
		},()=>{
			let text = this.alerts.alertActivityCreateError;
			this._actionModalsService.alertError(text);
			this.closeForm.emit(false);
		});
	}

	updateActivity() {
		this._activitiesService.updateActivity(this.currentActivity).subscribe(data => {
			if (data) {
				this._activitiesService.passActivityToPushOrUpdate(data);
				let title = this.currentActivity.activity_name;
				let text = this.alerts.alertActivityUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title,text);
				this.closeForm.emit(false);
			}
		},()=>{
			let text = this.alerts.alertActivityCreateError;
			this._actionModalsService.alertError(text);
			this.closeForm.emit(false);
		});
	}

	forgetActions() {
		this.currentActivity = {};
		this.closeForm.emit(false);
	}

}

class AFCLanguage {
	static en = {
		"alerts": {
			"alertActivityCreatedSuccessfully": "Activity created successfully",
			"alertActivityCreateError": "An error ocurred",
			"alertActivityUpdatedSuccessfully": "Activity updated successfully",
			"alertActivityUpdateError": "An error ocurred"
		},
		"activityName": "Activity Name",
		"activityDoneName": "Activity done Name",
		"activityDescription": "Activity Description",
		"activityPrice":"Hourly Price",
		"errorRequired":"This field is required",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	};

	static de = {
		"alerts": {
			"alertActivityCreatedSuccessfully": "Aktivität erfolgreich erstelt",
			"alertActivityCreateError": "Es ist ein Fehler aufgetreten",
			"alertActivityUpdatedSuccessfully": "Aktivität erfolgreich aktualiziert",
			"alertActivityUpdateError": "Es ist ein Fehler aufgetreten"
		},
		"activityName": "Aktivität Name",
		"activityDoneName": "Activity done Name",
		"activityDescription": "Beschreibung",
		"activityPrice":"Stundenpreis",
		"errorRequired":"Pflichtfeld",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	};

	static es = {
		"alerts": {
			"alertActivityCreatedSuccessfully": "Actividad creada exitosamente",
			"alertActivityCreateError": "Ha ocurrido un error",
			"alertActivityUpdatedSuccessfully": "Actividad actualizada exitosamente",
			"alertActivityUpdateError": "Ha ocurrido un error"
		},
		"activityName": "Nombre de Actividad",
		"activityDoneName": "Nombre preterito",
		"activityDescription": "Descripción",
		"activityPrice":"Precio por Hora",
		"errorRequired":"Campo requerido.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	};
}
