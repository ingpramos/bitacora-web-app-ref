import { Component, OnInit, OnDestroy, Input } from "@angular/core";
import { CompanyService } from "../../../shared/services/company.service";
import { SharedService } from "../../../shared/services/shared.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-ibans-list",
    templateUrl: "./ibans-list.component.html",
    styleUrls: ["./ibans-list.component.scss"]
})
export class IbansListComponent implements OnInit, OnDestroy {
    ibans;
    ibansListText: any;
    alerts: any;
    ibanToHandle = {};
    editIban: boolean = false;
    newIban: boolean = false;

    sessionInfo$;
    ibanToPushOrUpdate$;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.ibansListText = ILCLanguages[cl];
        this.alerts = this.ibansListText.alerts;
    }

    ngOnInit() {
		this.ibans = this._sharedService.ibans || [];
        this.ibanToPushOrUpdate$ = this._companyService.ibanToPushOrUpdate.subscribe(data => {
            let company_iban_id = data.company_iban_id;
            if (!company_iban_id) return; // case {} an initialization
            let index = this.ibans.findIndex(obj => obj["company_iban_id"] == company_iban_id);
            if (index == -1) {
                // iban was created
                this.ibans.push(data);
            } else {
                // iban was updated
                this.ibans.splice(index, 1, data);
			}
			this._sharedService.ibans = this.ibans; // make changes available to all the app
        });
    }
    ngOnDestroy() {
        this.ibanToPushOrUpdate$.unsubscribe();
    }

    startEditIban(iban) {
        this.ibanToHandle = Object.assign({}, iban);
        this.newIban = false;
        this.editIban = true;
        window.scroll(0, 0);
    }

    startNewIban() {
        this.editIban = false;
        this.newIban = true;
        this.ibanToHandle = {};
    }

    deleteIban(iban, index) {
        let title = iban.bank_name;
        let text = this.alerts.confirmDeleteIBAN;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let company_iban_id = iban.company_iban_id;
                this._companyService.deleteIban(company_iban_id).subscribe(
                    data => {
                        if (data) {
							this.ibans.splice(index, 1);
							this._sharedService.ibans = this.ibans; // make changes available to all the app
                            let title = data.bank_name;
                            let text = this.alerts.IBANDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.forgetActions();
                        }
                    },
                    () => {
                        let title = this.alerts.IBANDeleteError;
                        this._actionModalsService.alertError(title);
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editIban = false;
        this.newIban = false;
    }
}

class ILCLanguages {
    static en = {
        alerts: {
            confirmDeleteIBAN: "Are you sure you want to delete this IBAN",
            IBANDeletedSuccessfully: "IBAN successfully deleted",
            IBANDeleteError: "An error ocurred"
        },
        ibansListTitle: "IBANS List",
        editIban: "Edit IBAN",
        deleteIban: "Delete IBAN"
    };
    static de = {
        alerts: {
            confirmDeleteIBAN: "Wollen Sie dieses IBAN löschen",
            IBANDeletedSuccessfully: "Zahlungsart erfolgreich gelöscht",
            IBANDeleteError: "Es ist ein Fehler aufgetreten"
        },
        ibansListTitle: "IBANS Liste",
        editIban: "IBAN bearbeiten",
        deleteIban: "IBAN löschen"
    };
    static es = {
        alerts: {
            confirmDeleteIBAN: "Esta seguro que desea borrar este IBAN?",
            IBANDeletedSuccessfully: "IBAN eliminado exitosamente",
            IBANDeleteError: "Ha ocurrido un error"
        },
        ibansListTitle: "Lista De IBANS",
        editIban: "Editar IBAN",
        deleteIban: "Eliminar IBAN"
    };
}
