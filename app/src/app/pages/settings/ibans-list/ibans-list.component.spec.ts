import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IbansListComponent } from './ibans-list.component';

describe('IbansListComponent', () => {
  let component: IbansListComponent;
  let fixture: ComponentFixture<IbansListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IbansListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IbansListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
