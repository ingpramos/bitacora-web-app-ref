import { Component, OnInit, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-payment-methods-list",
    templateUrl: "./payment-methods-list.component.html",
    styleUrls: ["./payment-methods-list.component.scss"]
})
export class PaymentMethodsListComponent implements OnInit, OnDestroy {
    paymentMethods: any;
    paymentMethodsListText: any = {};
    alerts: any;
    newPaymentMethod: boolean = false;
    editPaymentMethod: boolean = false;
    paymentMethodToHandle: any = {};
    // subscriptions
    paymentMethodToPushOrUpdate$: any;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.paymentMethodsListText = PMLCLanguages[cl];
        this.alerts = this.paymentMethodsListText.alerts;
    }

    ngOnInit() {
        this.paymentMethods = this._sharedService.payment_methods;
        this.paymentMethodToPushOrUpdate$ = this._companyService.paymentMethodToPushOrUpdate.subscribe(data => {
            let payment_method_id = data.payment_method_id;
            if (!payment_method_id) return; // case {} an initialization
            let index = this.paymentMethods.findIndex(obj => obj["payment_method_id"] == payment_method_id);
            if (index == -1) {
                // payment method was created
                this.paymentMethods.push(data);
            } else {
                // payment method was updated
                this.paymentMethods.splice(index, 1, data);
            }
            this._sharedService.payment_methods = this.paymentMethods;
        });
    }
    ngOnDestroy() {
        this.paymentMethodToPushOrUpdate$.unsubscribe();
    }

    startEditPaymentMethod(paymentMethod) {
        this.paymentMethodToHandle = Object.assign({}, paymentMethod);
        this.newPaymentMethod = false;
        this.editPaymentMethod = true;
        window.scroll(0, 0);
    }

    startNewPaymentMethod() {
        this.editPaymentMethod = false;
        this.newPaymentMethod = true;
        this.paymentMethodToHandle = {};
    }

    deletePaymentMethod(pm, index) {
        let title = pm.payment_method_name;
        let text = this.alerts.comfirmDeletePM;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let pm_id = pm.payment_method_id;
                this._companyService.deletePaymentMethod(pm_id).subscribe(
                    data => {
                        if (data) {
                            this.paymentMethods.splice(index, 1);
                            this._sharedService.payment_methods = this.paymentMethods;
                            let text = this.alerts.alertPMDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertPMDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }

    forgetActions(value) {
        this.editPaymentMethod = false;
        this.newPaymentMethod = false;
    }
}

class PMLCLanguages {
    static en = {
        alerts: {
            comfirmDeletePM: "Are you sure you want to delete this payment method ?",
            alertPMDeletedSuccessfully: "Payment method successfully deleted",
            alertForeignKeyViolation: "This payment method is still referenced in one of your documents",
            alertPMDeleteError: "An error ocurred"
        },
        paymentMethodsTitle: "Payment Methods",
        editPaymentMethod: "Edit Payment Method",
        deletePaymentMethod: "Delete Payment Method"
    };
    static de = {
        alerts: {
            comfirmDeletePM: "Wollen Sie dieses Zahlungsart löschen ?",
            alertPMDeletedSuccessfully: "Zahlungsart erfolgreich gelöscht",
            alertForeignKeyViolation: "Diese Zahlungsart ist immer noch referenziert in einer Ihrer Dokumenten",
            alertPMDeleteError: "Es ist ein Fehler aufgetreten"
        },
        paymentMethodsTitle: "Zahlungsarten",
        editPaymentMethod: "Zahlungsart bearbeiten",
        deletePaymentMethod: "Zahlungsart löschen"
    };
    static es = {
        alerts: {
            comfirmDeletePM: "Esta seguro que desea borrar este metodo de pago? ",
            alertPMDeletedSuccessfully: "Metodo de pago eliminado exitosamente",
            alertForeignKeyViolation: "Este método de entrega está referenciado en alguno de sus documentos",
            alertPMDeleteError: "Ha ocurrido un error"
        },
        paymentMethodsTitle: "Metodos De Pago",
        editPaymentMethod: "Editar Método de Pago",
        deletePaymentMethod: "Eliminar Método de Pago"
    };
}
