import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatsFormComponent } from './vats-form.component';

describe('VatsFormComponent', () => {
  let component: VatsFormComponent;
  let fixture: ComponentFixture<VatsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
