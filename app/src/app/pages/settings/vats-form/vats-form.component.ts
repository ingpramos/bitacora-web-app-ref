import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { CompanyService } from '../../../shared/services/company.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
    selector: 'app-vats-form',
    templateUrl: './vats-form.component.html',
    styleUrls: ['./vats-form.component.scss']
})
export class VatsFormComponent implements OnInit {

    @Input() currentVat;
    @Input() editVat;
    @Input() newVat;
    @Input() index;
    @Output() useForm = new EventEmitter<boolean>();

    old_vat;
    vatsFormText: any;
    alerts: any;

    constructor(
        private _sharedService: SharedService,
        private _companyService: CompanyService,
        private _actionModalsService: ActionModalsService
    ) {
        let cl = this._sharedService.user_selected_language;
        this.vatsFormText = VFCLanguages[cl];
        this.alerts = this.vatsFormText.alerts;
    }

    ngOnInit() {
        if (this.editVat) this.old_vat = this.currentVat;
    }

    saveVat() {
        this.newVat ? this.createVat() : this.updateVat(this.index);
    }

    createVat() {
        let body = {};
        body['new_vat'] = this.currentVat;
        this._companyService.createVat(body).subscribe(data => {
            if (data) {
                this._companyService.passNewVat(this.currentVat);
                let title = data.company_vats;
                let text = this.alerts.alertVFCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.forgetActions();
            }
        }, () => {
            let text = this.alerts.alertVFCreateError;
            this._actionModalsService.alertError(text);
        })
    }

    updateVat(index) {
        let body = {};
        body['new_vat'] = this.currentVat;
        body['vat_to_replace'] = this.old_vat;
        this._companyService.updateVat(body).subscribe(data => {
            if (data) {
                let title = this.currentVat;
                this._companyService.passUpdatedVat([index,this.currentVat]);
                let text = this.alerts.alertVFUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.forgetActions();
            }
        }, () => {
            let text = this.alerts.alertVFCreateError;
            this._actionModalsService.alertError(text);
        })
    }

    forgetActions() {
        this.currentVat = {};
        this.useForm.emit(false);
    }

}

class VFCLanguages {

    static en = {
        "alerts": {
            "alertVFUpdatedSuccessfully": "Vat updated successfully",
            "alertVFUpdateError": "An error ocurred",
            "alertVFCreatedSuccessfully": "Vat created successfully",
            "alertVFCreateError": "An error ocurred"
        },
        "vatNumber": {
            "placeholder": "Plus value added tax",
			"hint":"Write a value between 0 and 100",
			"errorRequired":"This field is required"
        },
        "saveButton": "Save",
        "cancelButton": "Cancel"
    }
    static de = {
        "alerts": {
            "alertVFUpdatedSuccessfully": "Mwst erfolgreich aktualiziert",
            "alertVFUpdateError": "Es ist ein Fehler aufgetreten",
            "alertVFCreatedSuccessfully": "Mwst erfolgreich erstelt",
            "alertVFCreateError": "Es ist ein Fehler aufgetreten"
        },
        "vatNumber": {
            "placeholder": "Mehrwertsteuer",
			"hint":"Schreiben Sie einen Wert zwischen 0 und 100",
			"errorRequired":"Pflichtfeld"
        },
        "saveButton": "Speichern",
        "cancelButton": "Zurück"
    }

    static es = {
        "alerts": {
            "alertVFUpdatedSuccessfully": "Procentage de impuesto actualizado exitosamente",
            "alertVFUpdateError": "Ha ocurrido un error",
            "alertVFCreatedSuccessfully": "Procentage de impuesto creado existosamente",
            "alertVFCreateError": "Ha ocurrido un error"
        },
        "vatNumber": {
            "placeholder": "Impuesto al valor agregado",
			"hint":"Por favor ingrese un valor entre 0 y 100.",
			"errorRequired":"Campo requerido."
		} ,
        "saveButton": "Guardar",
        "cancelButton": "Cancelar"
    }

}
