import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
// Angular Material
import {
	MatMenuModule,
	MatInputModule,
	MatNativeDateModule,
	MatSelectModule,
	MatFormFieldModule,
	MatIconModule
} from '@angular/material';

import { SettingsComponent } from './settings.component';
import { IbansListComponent } from './ibans-list/ibans-list.component';
import { IbanFormComponent } from './iban-form/iban-form.component';
import { PaymentMethodsListComponent } from './payment-methods-list/payment-methods-list.component';
import { PaymentMethodFormComponent } from './payment-method-form/payment-method-form.component';
import { PaymentConditionsListComponent } from './payment-conditions-list/payment-conditions-list.component';
import { PaymentConditionFormComponent } from './payment-condition-form/payment-condition-form.component';
import { DeliveryMethodsListComponent } from './delivery-methods-list/delivery-methods-list.component';
import { DeliveryMethodFormComponent } from './delivery-method-form/delivery-method-form.component';
import { DeliveryConditionsListComponent } from './delivery-conditions-list/delivery-conditions-list.component';
import { DeliveryConditionFormComponent } from './delivery-condition-form/delivery-condition-form.component';
import { ActvitiesListComponent } from './actvities-list/actvities-list.component';
import { ActivityFormComponent } from './activity-form/activity-form.component';
import { DocumentsConfigurationComponent } from './documents-configuration/documents-configuration.component';
import { DocumentFormComponent } from './document-form/document-form.component';
import { VatsFormComponent } from './vats-form/vats-form.component';
import { VatsListComponent } from './vats-list/vats-list.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { LocationsListComponent } from './locations-list/locations-list.component';
import { LocationFormComponent } from './location-form/location-form.component';


const SETTINGS_ROUTES: Routes = [{ path: '', component: SettingsComponent } ];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule.forChild(SETTINGS_ROUTES),
		MatMenuModule,
		MatInputModule,
		MatNativeDateModule,
		MatSelectModule,
		MatFormFieldModule,
		MatIconModule,
		PipesModule
	],
	declarations: [
		SettingsComponent,
		IbansListComponent,
		IbanFormComponent,
		PaymentMethodsListComponent,
		PaymentMethodFormComponent,
		PaymentConditionsListComponent,
		PaymentConditionFormComponent,
		DeliveryMethodsListComponent,
		DeliveryMethodFormComponent,
		DeliveryConditionsListComponent,
		DeliveryConditionFormComponent,
		ActvitiesListComponent,
		ActivityFormComponent,
		DocumentsConfigurationComponent,
		DocumentFormComponent,
		VatsFormComponent,
		VatsListComponent,
		LocationsListComponent,
		LocationFormComponent
	]
})
export class SettingsModule {}
