export class IBFCLanguages {

	static en = {
		"alerts": {
			"alertBankUpdateSuccess": "Bank updated successfully",
			"alertBankUpdateError": "An error ocurred",
			"alertBankCreationSuccess": "Bank created successfully",
			"alertBankCreateError": "An error ocurred"
		},
		"bankName": {
			"placeholder": "Bank Name",
			"hint":"Please write the name of your bank.",
			"errorRequired":"This field is required"
		} ,
		"iban": {
			"placeholder":  "IBAN (International Bank Account Number)",
			"hint":"IBAN requires between 16 and 34 characters.",
			"errorRequired":"This field is required.",
			"errorMinLength":"Your IBAN should have at least 16 characters.",
			"errorMaxLength":"Your IBAN should have maximum 34 characters."
		},
		"bic":{
			"placeholder":"BIC (Bank Identifier Code)",
			"hint":"BIC requires between 8 and 11 characters.",
			"errorRequired":"This field is required.",
			"errorMinLength":"Your BIC should have at least 8 characters.",
			"errorMaxLength":"Your IBAN should have maximum 11 characters."
		},
		"saveButton": "Save",
		"cancelButton": "Cancel"
	}
	static de = {
		"alerts": {
			"alertBankUpdateSuccess": "Bank erfolgreich aktualiziert",
			"alertBankUpdateError": "Ha ocurrido un error",
			"alertBankCreationSuccess": "Bank erfolgreich erstelt",
			"alertBankCreateError": "Ha ocurrido un error"
		},
		"bankName":{
			"placeholder": "Bankname",
			"hint":"Bitte geben Sie den Namen Ihrer Bank an.",
			"errorRequired":"Pflichtfeld"
		},
		"iban": {
			"placeholder": "IBAN (International Bank Account Number)",
			"hint":"IBAN erfordert zwischen 16 und 34 Zeichen.",
			"errorRequired":"Pflichtfeld",
			"errorMinLength":"Ihre IBAN sollte aus mindestens 16 Zeichen bestehen.",
			"errorMaxLength":"Ihre IBAN sollte aus maximal 34 Zeichen bestehen."
		} ,
		"bic": {
			"placeholder": "BIC (Bank Identifier Code)",
			"hint":"IBAN erfordert zwischen 8 und 11 Zeichen",
			"errorRequired":"Pflichtfeld",
			"errorMinLength":"Ihre IBAN sollte aus mindestens 8 Zeichen bestehen.",
			"errorMaxLength":"Ihre BIC sollte aus maximal 11 Zeichen bestehen."
		},
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	}

	static es = {
		"alerts": {
			"alertBankUpdateSuccess": "Banco actualizado exitosamente",
			"alertBankUpdateError": "Ha ocurrido un error",
			"alertBankCreationSuccess": "Banco creado existosamente",
			"alertBankCreateError": "Ha ocurrido un error"
		},
		"bankName":{
			"placeholder": "Nombre del banco",
			"hint":"Por favor ingrese el nombre de su banco.",
			"errorRequired":"Campo requerido."
		} ,
		"iban":{
			"placeholder": "IBAN (International Bank Account Number)",
			"hint":"IBAN tiene entre 16 y 34 carácteres.",
			"errorRequired":"Campo requerido.",
			"errorMinLength":"El IBAN tiene al menos 16 carácteres.",
			"errorMaxLength":"El IBAN tiene máximo 34 carácteres."
		},
		"bic": {
			"placeholder": "BIC (Bank Identifier Code)",
			"hint":"El BIC tiene entre 8 y 11 carácteres.",
			"errorRequired":"Campo requerido.",
			"errorMinLength":"El BIC tiene al menos 16 carácteres.",
			"errorMaxLength":"El BIC tiene máximo 34 carácteres."
		},
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	}

}