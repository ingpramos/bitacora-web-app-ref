import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { CompanyService } from '../../../shared/services/company.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { IBFCLanguages } from './iban-form.languages';

@Component({
	selector: 'app-iban-form',
	templateUrl: './iban-form.component.html',
	styleUrls: ['./iban-form.component.scss']
})

export class IbanFormComponent implements OnInit {

	@Input() currentIban : any;
	@Input() editIban;
	@Input() newIban;
	@Output() closeForm = new EventEmitter<boolean>();

	ibanFormText: any;
	alerts: any;

	constructor(
		private _sharedService: SharedService,
		private _companyService: CompanyService,
		private _actionModalsService: ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.ibanFormText = IBFCLanguages[cl];
		this.alerts = this.ibanFormText.alerts;
	}

	ngOnInit() {

	}

	saveIban() {
		this.editIban ? this.updateIban() : this.createIban();
	}

	updateIban() {
		this._companyService.updateIban(this.currentIban).subscribe(data => {
			if (data) {
				this._companyService.passIbanToPushOrUpdate(data);
				let title = data.bank_name;
				let text = this.alerts.alertBankUpdateSuccess;
				this._actionModalsService.buildSuccessModal(title, text);
				this.closeForm.emit(true);
			}
		}, () => {
			let title = this.alerts.alertBankUpdateError;
			this._actionModalsService.alertError(title);
			this.closeForm.emit(true);
		})
	}

	createIban() {
		this.currentIban.company_id = this._sharedService.company_id;
		this._companyService.createIban(this.currentIban).subscribe(data => {
			if (data) {
				this._companyService.passIbanToPushOrUpdate(data);
				let title = data.bank_name;
				let text = this.alerts.alertBankCreationSuccess;
				this._actionModalsService.buildSuccessModal(title, text);
				this.closeForm.emit(true);
			}
		}, () => {
			let title = this.alerts.alertBankCreateError;
			this._actionModalsService.alertError(title);
			this.closeForm.emit(true);
		});
	}

	forgetActions() {
		this.currentIban = {};
		this.closeForm.emit(true);
	}

}


