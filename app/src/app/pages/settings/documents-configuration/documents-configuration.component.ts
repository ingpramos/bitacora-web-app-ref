import { Component, OnInit } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { OrdersService } from "../../../shared/services/orders.service";
import { OffersService } from "../../../shared/services/offers.service";
import { InvoicesService } from "../../../shared/services/invoices.service";
import { ConfirmationsService } from "../../../shared/services/confirmations.service";
import { DeliveryNotesService } from "../../../shared/services/delivery-notes.service";

@Component({
    selector: "app-documents-configuration",
    templateUrl: "./documents-configuration.component.html",
    styleUrls: ["./documents-configuration.component.scss"]
})
export class DocumentsConfigurationComponent implements OnInit {
    documents: any;
    documentsListText: any;
    alerts: any;
    documentToHandle = {};
    editDocument: boolean = false;

    constructor(
        private _sharedService: SharedService,
        private _companyService: CompanyService,
        private _ordersService: OrdersService,
        private _offersService: OffersService,
        private _invoicesService: InvoicesService,
        private _orderConfirmationsService: ConfirmationsService,
        private _deliveryNotesService: DeliveryNotesService,
        private _actionModalsService: ActionModalsService
    ) {
        let cl = this._sharedService.user_selected_language;
        this.documentsListText = ILCLanguages[cl];
        this.alerts = this.documentsListText.alerts;
    }

    ngOnInit() {
        this._companyService.getDocuments().subscribe(data => {
            this.documents = data;
        });
    }

    startEditDocument(doc) {
        this.editDocument = true;
        this.documentToHandle = Object.assign({}, doc);
        window.scroll(0, 0);
    }

    updateDocument(doc: any) {
        if (doc == false) return (this.editDocument = false); // close the form when canceled
        this._companyService.updateDocument(doc).subscribe(
            data => {
                if (data) {
                    const index = this.documents.findIndex(obj => obj["dc_id"] == data["dc_id"]);
                    this.documents.splice(index, 1, data);
                    let title = this.documentsListText[doc.document_name];
                    let text = this.alerts.alertDocumentUpdateSuccess;
                    this._actionModalsService.buildSuccessModal(title, text);
                    let service = data.document_name;
                    this.updateContextTextInService(service);
                    this.editDocument = false;
                }
            },
            () => {
                let title = this.alerts.alertDocumentUpdateError;
                this._actionModalsService.alertError(title);
            }
        );
    }

    updateContextTextInService(service) {
        switch (service) {
            case "orders":
                this._ordersService.getDocumentOrderTexts().subscribe(data => (this._ordersService.pdfOrdersTexts = data));
                break;
            case "invoices":
                this._invoicesService.getDocumentInvoiceTexts().subscribe(data => (this._invoicesService.pdfInvoiceTexts = data));
                break;
            case "offers":
                this._offersService.getDocumentOfferTexts().subscribe(data => (this._offersService.pdfOffersTexts = data));
                break;
            case "order_confirmations":
                this._orderConfirmationsService.getDocumentOCTexts().subscribe(data => (this._orderConfirmationsService.pdfOCTexts = data));
                break;
            case "delivery_notes":
                this._deliveryNotesService.getDocumentDeliveryNoteTexts().subscribe(data => (this._deliveryNotesService.pdfDeliveryNotesTexts = data));
        }
    }
}

class ILCLanguages {
    static en = {
        alerts: {
            alertDocumentUpdateSuccess: "Document updated successfully",
            alertDocumentUpdateError: "An error ocurred"
        },
        documentsListTitle: "Documents List",
        editDocument: "Edit Document",
        deleteDocument: "Delete Document",
        orders: "Orders",
        invoices: "Invoices",
        offers: "Offers",
        delivery_notes: "Delivery Notes",
        order_confirmations: "Order Confirmations"
    };
    static de = {
        alerts: {
            alertDocumentUpdateSuccess: "Dokument erfolgreich aktualiziert",
            alertDocumentUpdateError: "Es ist ein Fehler aufgetreten"
        },
        documentsListTitle: "Dokument Liste",
        editDocument: "Document bearbeiten",
        deleteDocument: "Document löschen",
        orders: "Bestellungen",
        invoices: "Rechungen",
        offers: "Angeboten",
        delivery_notes: "Lieferschein",
        order_confirmations: "Auftragsbestätigungen"
    };
    static es = {
        alerts: {
            alertDocumentUpdateSuccess: "Documento actualizado exitosamente",
            alertDocumentUpdateError: "Ha ocurrido un error"
        },
        documentsListTitle: "Lista De Documentos",
        editDocument: "Editar Documento",
        deleteDocument: "Eliminar Documento",
        orders: "Ordenes de Compra",
        invoices: "Facturas",
        offers: "Cotizaciones",
        delivery_notes: "Notas de entrega",
        order_confirmations: "Pedidos"
    };
}
