import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentsConfigurationComponent } from './documents-configuration.component';

describe('DocumentsConfigurationComponent', () => {
  let component: DocumentsConfigurationComponent;
  let fixture: ComponentFixture<DocumentsConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentsConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentsConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
