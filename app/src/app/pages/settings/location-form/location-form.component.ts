import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { CompanyService } from '../../../shared/services/company.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { LocationsService } from '../../../shared/services/locations.service';

@Component({
    selector: 'app-location-form',
    templateUrl: './location-form.component.html',
    styleUrls: ['./location-form.component.scss']
})
export class LocationFormComponent implements OnInit {

    @Input() currentLocation;
    @Input() editLocation;
    @Input() newLocation;
    @Output() closeForm = new EventEmitter<boolean>();
    locationFormText: any;
    alerts: any;

    constructor(
        private _sharedService: SharedService,
        private _locationsService: LocationsService,
        private _actionModalsService: ActionModalsService
    ) {
        let cl = this._sharedService.user_selected_language;
        this.locationFormText = LFCLanguages[cl];
        this.alerts = this.locationFormText.alerts;
    }

    ngOnInit() {

    }

    saveLocation() {
        this.newLocation ? this.createLocation() : this.updateLocation();
    }

    createLocation() {
        this._locationsService.createLocation(this.currentLocation).subscribe(data => {
            if (data) {
                this._locationsService.passLocationToPushOrUpdate(data);
                let title = data.location_name;
                let text = this.alerts.alertLocationCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            }
        }, () => {
            let text = this.alerts.alertLocationCreateError;
            this._actionModalsService.alertError(text);
            this.closeForm.emit(true);
        });
    }

    updateLocation() {
        this._locationsService.updateLocation(this.currentLocation).subscribe(data => {
            if (data) {
                this._locationsService.passLocationToPushOrUpdate(data);
                let title = data.location_name;
                let text = this.alerts.alertLocationUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            }
        }, () => {
            let text = this.alerts.alertLocationUpdateError;
            this._actionModalsService.alertError(text);
            this.closeForm.emit(true);
        });
    }

    forgetActions() {
        this.currentLocation = {};
        this.closeForm.emit(true);
    }

}

class LFCLanguages {

    static en = {
        "alerts": {
            "alertLocationUpdatedSuccessfully": "Location updated successfully",
            "alertLocationUpdateError": "An error ocurred",
            "alertLocationCreatedSuccessfully": "Location created successfully",
            "alertLocationCreateError": "An error ocurred"
        },
        "pcName": "Payment Method",
        "locationName": "Location",
        "errorRequired": "This field is required",
        "saveButton": "Save",
        "cancelButton": "Cancel"
    }
    static de = {
        "alerts": {
            "alertLocationUpdatedSuccessfully": "Lagerort erfolgreich aktualiziert",
            "alertLocationUpdateError": "Es ist ein Fehler aufgetreten",
            "alertLocationCreatedSuccessfully": "Lagerort erfolgreich erstelt",
            "alertLocationCreateError": "Es ist ein Fehler aufgetreten"
        },
        "pcName": "Zahlungsart",
        "locationName": "Lagerort",
        "errorRequired": "Pflichtfeld",
        "saveButton": "Speichern",
        "cancelButton": "Zurück"
    }

    static es = {
        "alerts": {
            "alertLocationUpdatedSuccessfully": "Lugar actualizado exitosamente",
            "alertLocationUpdateError": "Ha ocurrido un error",
            "alertLocationCreatedSuccessfully": "Lugar creado existosamente",
            "alertLocationCreateError": "Ha ocurrido un error"
        },
        "pcName": "Método de pago",
        "locationName": "Lugar",
        "errorRequired": "Campo requerido.",
        "saveButton": "Guardar",
        "cancelButton": "Cancelar"
    }

}