import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-payment-method-form",
    templateUrl: "./payment-method-form.component.html",
    styleUrls: ["./payment-method-form.component.scss"]
})
export class PaymentMethodFormComponent implements OnInit {
    @Input() currentPaymentMethod;
    @Input() editPaymentMethod;
    @Input() newPaymentMethod;
    @Output() closeForm = new EventEmitter<boolean>();
    paymentMethodFormText: any;
    alerts: any;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.paymentMethodFormText = PMFCLanguages[cl];
        this.alerts = this.paymentMethodFormText.alerts;
    }

    ngOnInit() {}

    savePaymentMethod() {
        this.editPaymentMethod ? this.updatePaymentMethod() : this.createPaymentMethod();
    }

    updatePaymentMethod() {
        this._companyService.updatePaymentMethod(this.currentPaymentMethod).subscribe(
            data => {
                if (data) {
                    this._companyService.passPaymentMethodToPushOrUpdate(data);
                    let title = data.payment_method_name;
                    let text = this.alerts.alertPMUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            () => {
                let text = this.alerts.alertPMUpdateError;
                this._actionModalsService.alertError(text);
                this.closeForm.emit(true);
            }
        );
    }

    createPaymentMethod() {
        this.currentPaymentMethod.company_id = this._sharedService.company_id;
        this._companyService.createPaymentMethod(this.currentPaymentMethod).subscribe(
            data => {
                if (data) {
                    this._companyService.passPaymentMethodToPushOrUpdate(data);
                    this.forgetActions();
                    let title = data.payment_method_name;
                    let text = this.alerts.alertPMCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            () => {
                let text = this.alerts.alertPMCreateError;
                this._actionModalsService.alertError(text);
                this.closeForm.emit(true);
            }
        );
    }

    forgetActions() {
        this.currentPaymentMethod = {};
        this.closeForm.emit(true);
    }
}

class PMFCLanguages {
    static en = {
        alerts: {
            alertPMUpdatedSuccessfully: "Payment method updated successfully",
            alertPMUpdateError: "An error ocurred",
            alertPMCreatedSuccessfully: "Payment method created successfully",
            alertPMCreateError: "An error ocurred"
        },
        pmName: "Payment Method",
        pmDescription: "Description",
        errorRequired: "This field is required",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        alerts: {
            alertPMUpdatedSuccessfully: "Zahlungsart erfolgreich aktualiziert",
            alertPMUpdateError: "Es ist ein Fehler aufgetreten",
            alertPMCreatedSuccessfully: "Zahlungsart erfolgreich erstelt",
            alertPMCreateError: "Es ist ein Fehler aufgetreten"
        },
        pmName: "Zahlungsart",
        pmDescription: "Beschreibung",
        errorRequired: "Pflichtfeld",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertPMUpdatedSuccessfully: "Metodo de pago actualizado exitosamente",
            alertPMUpdateError: "Es ist ein Fehler aufgetreten",
            alertPMCreatedSuccessfully: "Metodo de pago creado existosamente",
            alertPMCreateError: "Es ist ein Fehler aufgetreten"
        },
        pmName: "Método De Pago",
        pmDescription: "Descripción",
        errorRequired: "Campo requerido.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
