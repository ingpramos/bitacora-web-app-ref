import { Component, OnInit } from '@angular/core';
import { CompanyService } from '../../../shared/services/company.service';
import { SharedService } from '../../../shared/services/shared.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { LocationsService } from '../../../shared/services/locations.service';

@Component({
	selector: 'app-locations-list',
	templateUrl: './locations-list.component.html',
	styleUrls: ['./locations-list.component.scss']
})
export class LocationsListComponent implements OnInit {

	locationsListText: any;
	alerts: any;
	locations = [];
	editLocation = false;
	newLocation = false;
	locationToHandle = {};
	// subscriptions
	locationsList$
	locationToPushOrUpdate$;

	constructor(
		private _locationsService: LocationsService,
		private _sharedService: SharedService,
		private _actionModalsService: ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.locationsListText = LLClanguages[cl];
		this.alerts = this.locationsListText.alerts;
		this.locationsList$ = this._locationsService.getLocations().subscribe(data => this.locations = data);
		this.locationToPushOrUpdate$ = this._locationsService.locationToPushOrUpdate.subscribe(data => {
			let location_id = data.location_id;
			if (!location_id) return; // case {} an initialization
			let index = this.locations.findIndex(obj => obj['location_id'] == location_id);
			if (index == -1) {  // group was created 
				this.locations.push(data);
			} else { // group was updated
				this.locations.splice(index, 1, data);

			}
		});
	}
	ngOnInit() {

	}

	ngOnDestroy() {
		this.locationToPushOrUpdate$.unsubscribe();
	}

	startNewLocation() {
		this.editLocation = false;
		this.newLocation = true;
		this.locationToHandle = {};
	}

	startEditLocation(location) {
		this.locationToHandle = Object.assign({}, location);
		this.newLocation = false;
		this.editLocation = true;
	}

	forgetActions(value) {
		this.newLocation = false;
		this.editLocation = false;
		this.locationToHandle = {};
	}

	deleteLocation(location, index) {
		let title = location.location_name;
		let text = this.alerts.confirmDeleteLocation;
		this._actionModalsService.buildConfirmModal(title, text)
			.then(result => {
				if (result.value) {
					let location_id = location.location_id;
					this._locationsService.deleteLocation(location_id).subscribe(data => {
						if (data) {
							let text = this.alerts.alertLocationDeletedSuccessfully;
							this._actionModalsService.buildSuccessModal(title, text);
							this.locations.splice(index, 1);
						}
					}, (e) => {
						if (e.error.error.code == '23503') { // handle foreign key constrain
							const text = this.alerts.alertForeignKeyViolation;
							this._actionModalsService.buildInfoModal(title, text);
						}
						else {
							let title = this.alerts.alertDMDeleteError
							this._actionModalsService.alertError(title);
						}
					});
				}
			});
	}

}

class LLClanguages {
	static en = {
		"alerts": {
			"confirmDeleteLocation": "Are you sure you want to delete this location ?",
			"alertLocationDeletedSuccessfully": "Location successfully deleted",
			"alertForeignKeyViolation": "Some of your artikels still referenced with this location",
			"alertLocationDeleteError": "An error ocurred"
		},
		"locationsListTitle": "Locations",
		"editLocation": "Edit location",
		"deleteLocation": "Delete location"
	};
	static de = {
		"alerts": {
			"confirmDeleteLocation": "Wollen Sie dieses lagerort löschen ?",
			"alertLocationDeletedSuccessfully": "Lagerort erfolgreich gelöscht",
			"alertForeignKeyViolation": "Auf diesen Standort wird immer noch in einem Ihrer Artikel verwiesen",
			"alertLocationDeleteError": "Es ist ein Fehler aufgetreten"
		},
		"locationsListTitle": "Lagerorten",
		"editLocation": "Lagerort bearbeiten",
		"deleteLocation": "Lagerort löschen"
	};
	static es = {
		"alerts": {
			"confirmDeleteLocation": "Esta seguro que deseas eliminar el lugar actual ?",
			"alertLocationDeletedSuccessfully": "lugar eliminada exitosamente",
			"alertForeignKeyViolation": "Este lugar esta referenciado en alguno de sus articulos",
			"alertLocationDeleteError": "Ha ocurrido un error"
		},
		"locationsListTitle": "Lugares",
		"editLocation": "Editar lugar",
		"deleteLocation": "Eliminar lugar"
	};
}


