import { Component, OnInit, OnDestroy } from '@angular/core';
import { SharedService } from '../../shared/services/shared.service';
import { CompanyService } from '../../shared/services/company.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit, OnDestroy {
    
    sessionInfo;
    vats = [];
    deliveryConditions = [];
    settingsViewText;
    currentDocumentsNumbers = {};
    // subscriptions
    dCSubscription;
    numbersSubscription;
    vSubscription;
    updatedVatSubscription;


    constructor(
        private _sharedService: SharedService,
        private _companyService: CompanyService
    ) {

    }

    ngOnInit() {

        this._sharedService.sessionInfo.subscribe(data => {
            this.sessionInfo = data;
            this.vats = this.sessionInfo.company_vats;
        });
        this.vSubscription = this._companyService.newVatToPush.subscribe(data => {
            if (data && data > 0) this.vats.push(data);
        });
        this.numbersSubscription = this._companyService.updatedCompanyNumbersToReplace.subscribe(data => this.currentDocumentsNumbers = data);
        this.updatedVatSubscription = this._companyService.updatedVatToReplace.subscribe(data => this.vats[data[0]] = data[1]);

    }

    ngOnDestroy() {
        this.numbersSubscription.unsubscribe();
        this.vSubscription.unsubscribe();
        this.updatedVatSubscription.unsubscribe();
    }
}
