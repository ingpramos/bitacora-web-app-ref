import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentConditionFormComponent } from './payment-condition-form.component';

describe('PaymentConditionFormComponent', () => {
  let component: PaymentConditionFormComponent;
  let fixture: ComponentFixture<PaymentConditionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentConditionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentConditionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
