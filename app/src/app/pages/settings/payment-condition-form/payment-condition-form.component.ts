import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-payment-condition-form",
    templateUrl: "./payment-condition-form.component.html",
    styleUrls: ["./payment-condition-form.component.scss"]
})
export class PaymentConditionFormComponent implements OnInit {
    @Input() currentPaymentCondition;
    @Input() editPaymentCondition;
    @Input() newPaymentCondition;
    @Output() closeForm = new EventEmitter<boolean>();

    paymentConditionFormText: any;
    alerts: any;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.paymentConditionFormText = PCFCLanguages[cl];
        this.alerts = this.paymentConditionFormText.alerts;
    }

    ngOnInit() {}

    savePaymentCondition() {
        this.newPaymentCondition ? this.createPaymentCondition() : this.updatePaymentCondition();
    }

    createPaymentCondition() {
        this._companyService.createPaymentCondition(this.currentPaymentCondition).subscribe(
            data => {
                if (data) {
                    this._companyService.passPaymentCondtionToPushOrUpdate(data);
                    let title = data.payment_condition_description;
                    let text = this.alerts.alertPCCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            () => {
                let text = this.alerts.alertPCCreateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    updatePaymentCondition() {
        this._companyService.updatePaymentCondition(this.currentPaymentCondition).subscribe(
            data => {
                if (data) {
                    this._companyService.passPaymentCondtionToPushOrUpdate(data);
                    let title = data.payment_condition_description;
                    let text = this.alerts.alertPCUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeForm.emit(true);
                }
            },
            () => {
                let text = this.alerts.alertPCUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    forgetActions() {
        this.currentPaymentCondition = {};
        this.closeForm.emit(true);
    }
}

class PCFCLanguages {
    static en = {
        alerts: {
            alertPCUpdatedSuccessfully: "Payment condition updated successfully",
            alertPCUpdateError: "An error ocurred",
            alertPCCreatedSuccessfully: "Payment condition created successfully",
            alertPCCreateError: "An error ocurred"
        },
        pcName: "Payment Method",
        pcDescription: "Description",
        errorRequired: "This field is required",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        alerts: {
            alertPCUpdatedSuccessfully: "Zahlungsbedinung erfolgreich aktualiziert",
            alertPCUpdateError: "Es ist ein Fehler aufgetreten",
            alertPCCreatedSuccessfully: "Zahlungsbedinung erfolgreich erstelt",
            alertPCCreateError: "Es ist ein Fehler aufgetreten"
        },
        pcName: "Zahlungsart",
        pcDescription: "Beschreibung",
        errorRequired: "Pflichtfeld",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertPCUpdatedSuccessfully: "Condición de pago actualizada exitosamente",
            alertPCUpdateError: "Ha ocurrido un error",
            alertPCCreatedSuccessfully: "Condición de pago creada existosamente",
            alertPCCreateError: "Ha ocurrido un error"
        },
        pcName: "Metodo De Pago",
        pcDescription: "Descripción",
        errorRequired: "Campo requerido.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
