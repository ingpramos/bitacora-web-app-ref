import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryConditionsListComponent } from './delivery-conditions-list.component';

describe('DeliveryConditionsListComponent', () => {
  let component: DeliveryConditionsListComponent;
  let fixture: ComponentFixture<DeliveryConditionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryConditionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryConditionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
