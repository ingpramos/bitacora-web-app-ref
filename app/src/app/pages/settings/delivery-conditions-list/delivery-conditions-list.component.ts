import { Component, OnInit, OnDestroy } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-delivery-conditions-list",
    templateUrl: "./delivery-conditions-list.component.html",
    styleUrls: ["./delivery-conditions-list.component.scss"]
})
export class DeliveryConditionsListComponent implements OnInit, OnDestroy {
    deliveryConditions: any;
    deliveryConditionsListText: any = {};
    alerts: any;
    newDeliveryCondition = false;
    editDeliveryCondition = false;
    deliveryMethodToHandle = {};
    // subscription
    deliveryConditionToPushOrUpdate$: any;
    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.deliveryConditionsListText = DCLCLanguages[cl];
        this.alerts = this.deliveryConditionsListText.alerts;
    }

    ngOnInit() {
        this.deliveryConditions = this._sharedService.delivery_conditions || [];
        this.deliveryConditionToPushOrUpdate$ = this._companyService.deliveryConditionToPushOrUpdate.subscribe(data => {
            let delivery_condition_id = data.delivery_condition_id;
            if (!delivery_condition_id) return; // case {} an initialization
            let index = this.deliveryConditions.findIndex(obj => obj["delivery_condition_id"] == delivery_condition_id);
            if (index == -1) {
                // delivery condition was created
                this.deliveryConditions.push(data);
            } else {
                // delivery condition was updated
                this.deliveryConditions.splice(index, 1, data);
            }
            this._sharedService.delivery_conditions = this.deliveryConditions;
        });
    }
    ngOnDestroy() {
        this.deliveryConditionToPushOrUpdate$.unsubscribe();
    }

    startEditDeliveryCondition(deliveryMethod) {
        this.deliveryMethodToHandle = Object.assign({}, deliveryMethod);
        this.newDeliveryCondition = false;
        this.editDeliveryCondition = true;
        window.scroll(0, 0);
    }

    startNewDeliveryCondition() {
        this.editDeliveryCondition = false;
        this.newDeliveryCondition = true;
        this.deliveryMethodToHandle = {};
    }

    deleteDeliveryCondition(dc, index) {
        let title = dc.delivery_condition_description;
        let text = this.alerts.confirmDeleteDeliveryCondition;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let dc_id = dc.delivery_condition_id;
                this._companyService.deleteDeliveryCondition(dc_id).subscribe(
                    data => {
                        if (data) {
                            let text = this.alerts.alertDCDeletedSuccessfully;
                            this.deliveryConditions.splice(index, 1);
                            this._sharedService.delivery_conditions = this.deliveryConditions;
                            this._actionModalsService.buildSuccessModal(title, text);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let title = this.alerts.alertDCDeleteError;
                            this._actionModalsService.alertError(title);
                        }
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editDeliveryCondition = false;
        this.newDeliveryCondition = false;
    }
}

class DCLCLanguages {
    static en = {
        alerts: {
            confirmDeleteDeliveryCondition: "Are you sure you want to delete this delivery condition?",
            alertDCDeletedSuccessfully: "Delivery Condition successfully deleted",
            alertForeignKeyViolation: "This delivery condition is still referenced in one of your documents",
            alertDCDeleteError: "An error ocurred"
        },
        title: "Delivery Conditions",
        editDeliveryCondition: "Edit Delivery condition",
        deleteDeliveryCondition: "Delete Delivery condition"
    };
    static de = {
        alerts: {
            confirmDeleteDeliveryCondition: "Wollen Sie dieses Lieferungsbedingung Löschen?",
            alertDCDeletedSuccessfully: "Lieferungsbedingung erfolgreich gelöscht",
            alertForeignKeyViolation: "Diese Lieferungsbedingung ist immer noch referenziert in einer Ihrer Dokumenten",
            alertDCDeleteError: "Es ist ein Fehler aufgetreten"
        },
        title: "Lieferungsbedingungen",
        editDeliveryCondition: "Lieferungsbedingung bearbeiten",
        deleteDeliveryCondition: "Lieferungsbedingung löschen"
    };
    static es = {
        alerts: {
            confirmDeleteDeliveryCondition: "Esta seguro que desea borrar esta condición entrega ?",
            alertDCDeletedSuccessfully: "Condición de entrega eliminado exitosamente",
            alertForeignKeyViolation: "Esta condición de entrega está referenciada en alguno de sus documentos",
            alertDCDeleteError: "Ha ocurrido un error"
        },
        title: "Codiciones de Entrega",
        editDeliveryCondition: "Editar Condición de Entrega",
        deleteDeliveryCondition: "Eliminar Condición de Entrega"
    };
}
