import { Component, OnInit, OnDestroy } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';
import { ActivitiesService } from '../../../shared/services/activities.service';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
	selector: 'app-actvities-list',
	templateUrl: './actvities-list.component.html',
	styleUrls: ['./actvities-list.component.scss']
})
export class ActvitiesListComponent implements OnInit, OnDestroy {

	activitiesListText: any;
	alerts: any;
	activities = [];
	editActivity = false;
	newActivity = false;
	activityToHandle = {};
	// search functionality
	showSearch: boolean = false;
	private subject: Subject<string> = new Subject();
	searchPattern = '';
	// subscriptions
	activitiesList$: any;
	activityToPushOrUpdate$: any;

	constructor(
		private _activitiesService: ActivitiesService,
		private _sharedService: SharedService,
		private _actionModalsService: ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.activitiesListText = ALClanguages[cl];
		this.alerts = this.activitiesListText.alerts;
		this.activitiesList$ = this._activitiesService.getCompanyActivities().subscribe(data => this.activities = data);
		this.activityToPushOrUpdate$ = this._activitiesService.activityToPushOrUpdate.subscribe(data => {
			let activity_id = data.activity_id;
			if (!activity_id) return; // case {} an initialization
			let index = this.activities.findIndex(obj => obj['activity_id'] == activity_id);
			if (index == -1) {  // provider was created 
				this.activities.push(data);
			} else { // activity was updated
				this.activities.splice(index, 1, data);
			}
		});
	}
	ngOnInit() {
		this.subject.pipe(debounceTime(500)).subscribe(done => {
			(this.searchPattern.length < 1 || this.searchPattern === undefined) ? this.closeSearch() : this.searchForActivities();
		});
	}

	searchForActivities() {
		this._activitiesService.getActivitiesByPattern(this.searchPattern).subscribe(data => this.activities = data);
	}

	searchActivities() {
		this.subject.next(this.searchPattern);
	}

	ngOnDestroy() {
		this.activityToPushOrUpdate$.unsubscribe();
	}

	startNewActivity() {
		this.editActivity = false;
		this.newActivity = true;
		this.activityToHandle = {};
	}

	startEditActivity(activity) {
		this.activityToHandle = Object.assign({}, activity);
		this.newActivity = false;
		this.editActivity = true;
	}

	forgetActions(value) {
		this.newActivity = false;
		this.editActivity = false;
		this.activityToHandle = {};
	}

	startSearch() {
		this.editActivity = false;
		this.newActivity = false;
		this.showSearch = true;
	}

	closeSearch() {
		this.searchPattern = '';
		this._activitiesService.getCompanyActivities().subscribe(data => this.activities = data);
		this.showSearch = false;
	}

	deleteActivity(activity, index) {
		let title = activity.activity_name;
		let text = this.alerts.confirmDeleteActivity;
		this._actionModalsService.buildConfirmModal(title, text)
			.then(result => {
				if (result.value) {
					let activity_id = activity.activity_id;
					this._activitiesService.deleteActivity(activity_id)
						.subscribe(data => {
							if (data) {
								let text = this.alerts.alertActivityDeletedSuccessfully;
								this._actionModalsService.buildSuccessModal(title, text);
								this.activities.splice(index, 1);
							}
						}, (e) => {
							if (e.error.error.code == '23503') { // handle foreign key constrain
								const text = this.alerts.alertForeignKeyViolation;
								this._actionModalsService.buildInfoModal(title, text);
							}
							else {
								let text = this.alerts.alertActivityDeleteError;
								this._actionModalsService.alertError(text);
							}
						});
				}
			});
	}
}

class ALClanguages {
	static en = {
		"alerts": {
			"confirmDeleteActivity": "Are you sure you want to delete this activity ?",
			"alertActivityDeletedSuccessfully": "Activity successfully deleted",
			"alertForeignKeyViolation": "This activity is still referenced in one of your projects",
			"alertActivityDeleteError": "An error ocurred"
		},
		"activitiesListTitle": "Activities",
		"newActivity":"New Activity",
		"searchActivities":"Search Activities",
		"editActivity": "Edit Activity",
		"deleteActivity": "Delete Activity"
	};
	static de = {
		"alerts": {
			"confirmDeleteActivity": "Wollen Sie dieses Aktivität löschen ?",
			"alertActivityDeletedSuccessfully": "Aktivität erfolgreich gelöscht",
			"alertForeignKeyViolation": "Diese Aktivität wird noch in einem Ihrer Projekte referenziert",
			"alertActivityDeleteError": "Es ist ein Fehler aufgetreten"
		},
		"activitiesListTitle": "Aktivitäten",
		"newActivity":"Neu Aktivität",
		"searchActivities":"Aktivitäten suchen",
		"editActivity": "Aktivität bearbeiten",
		"deleteActivity": "Aktivität löschen"
	};
	static es = {
		"alerts": {
			"confirmDeleteActivity": "Esta seguro que deseas eliminar la actividad actual ?",
			"alertActivityDeletedSuccessfully": "Actividad eliminada exitosamente",
			"alertForeignKeyViolation": "Esta actividad se encuentra referenciada en uno de sus proyectos",
			"alertActivityDeleteError": "Ha ocurrido un error"
		},
		"activitiesListTitle": "Actividades",
		"newActivity":"Nueva Actividad",
		"searchActivities":"Buscar Actividades",
		"editActivity": "Editar Actividad",
		"deleteActivity": "Eliminar Actividad"
	};
}
