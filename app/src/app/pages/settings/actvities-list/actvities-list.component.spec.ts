import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActvitiesListComponent } from './actvities-list.component';

describe('ActvitiesListComponent', () => {
  let component: ActvitiesListComponent;
  let fixture: ComponentFixture<ActvitiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActvitiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActvitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
