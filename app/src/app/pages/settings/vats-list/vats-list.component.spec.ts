import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VatsListComponent } from './vats-list.component';

describe('VatsListComponent', () => {
  let component: VatsListComponent;
  let fixture: ComponentFixture<VatsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VatsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VatsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
