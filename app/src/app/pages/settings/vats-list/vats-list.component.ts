import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { CompanyService } from "../../../shared/services/company.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-vats-list",
    templateUrl: "./vats-list.component.html",
    styleUrls: ["./vats-list.component.scss"]
})
export class VatsListComponent implements OnInit {
    @Input() vats;
    index; // to be able to update view value after an edit
    editVat: boolean = false;
    newVat: boolean = false;
    vatToHandle: any;
    vatsListText: any;
    alerts: any;

    constructor(private _sharedService: SharedService, private _companyService: CompanyService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.vatsListText = VLCLanguages[cl];
        this.alerts = this.vatsListText.alerts;
    }

    ngOnInit() {}

    startNewVat() {
        this.editVat = false;
        this.newVat = true;
        this.vatToHandle = {};
    }

    startEditVat(vat, index) {
        this.index = index;
        this.editVat = true;
        this.newVat = false;
        this.vatToHandle = vat;
    }

    deleteVat(vat, index) {
        let title = vat;
        let text = this.alerts.confirmDeleteVat;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                this._companyService.deleteVat(vat).subscribe(
                    data => {
                        if (data) {
                            let title = vat;
                            let text = this.alerts.alertVatDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.vats.splice(index, 1);
                        }
                    },
                    () => {
                        let title = this.alerts.alertVatDeleteError;
                        this._actionModalsService.alertError(title);
                    }
                );
            }
        });
    }

    forgetActions() {
        this.editVat = false;
        this.newVat = false;
    }
}

class VLCLanguages {
    static en = {
        alerts: {
            confirmDeleteVat: "Are you sure you want to delete this tax value?",
            alertVatDeletedSuccessfully: "tax percentage deleted successfully.",
            alertVatDeleteError: "An error ocurred"
        },
        title: "Vats",
        editVat: "Edit Vat",
        deleteVat: "Delete Vat"
    };
    static de = {
        alerts: {
            confirmDeleteVat: "Wollen Sie dieses MwSt Werte löschen ?",
            alertVatDeletedSuccessfully: "MwSt Werte erfolgreich gelöscht",
            alertVatDeleteError: "Es ist ein Fehler aufgetreten"
        },
        title: "MwSt",
        editVat: "MwSt bearbeiten",
        deleteVat: "MwSt löschen"
    };
    static es = {
        alerts: {
            confirmDeleteVat: "Esta seguro que desea borrar este porcentage de impuestos?",
            alertVatDeletedSuccessfully: "Porcentage de impuestos eliminado exitosamente",
            alertVatDeleteError: "Ha ocurrido un error"
        },
        title: "Impuesto IVA",
        editVat: "Editar IVA",
        deleteVat: "Eliminar IVA"
    };
}
