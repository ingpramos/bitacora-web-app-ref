import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryConditionFormComponent } from './delivery-condition-form.component';

describe('DeliveryConditionFormComponent', () => {
  let component: DeliveryConditionFormComponent;
  let fixture: ComponentFixture<DeliveryConditionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryConditionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryConditionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
