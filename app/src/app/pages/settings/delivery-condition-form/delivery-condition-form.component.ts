import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { CompanyService } from '../../../shared/services/company.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
	selector: 'app-delivery-condition-form',
	templateUrl: './delivery-condition-form.component.html',
	styleUrls: ['./delivery-condition-form.component.scss']
})

export class DeliveryConditionFormComponent implements OnInit {

	@Input() currentDeliveryCondition;
	@Input() editDeliveryCondition;
	@Input() newDeliveryCondition;
	@Output() closeForm = new EventEmitter<boolean>();

	deliveryConditionFormText: any;
	alerts: any;

	constructor(
		private _sharedService: SharedService,
		private _companyService: CompanyService,
		private _actionModalsService: ActionModalsService
	) {
		let cl = this._sharedService.user_selected_language;
		this.deliveryConditionFormText = DCFCLanguages[cl];
		this.alerts = this.deliveryConditionFormText.alerts;
	}

	ngOnInit() {

	}

	saveDeliveryCondition() {
		this.newDeliveryCondition ? this.createDeliveryCondition() : this.updateDeliveryCondition();
	}

	createDeliveryCondition() {
		this._companyService.createDeliveryCondition(this.currentDeliveryCondition).subscribe(data => {
			if (data) {
				this._companyService.passDeliveryConditionToPushOrUpdate(data);
				let title = data.delivery_condition_description;
				let text = this.alerts.alertDCCreatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
				this.closeForm.emit(true);
			}
		}, () => {
			let text = this.alerts.alertDCCreateError;
			this._actionModalsService.alertError(text);
			this.closeForm.emit(true);
		});
	}

	updateDeliveryCondition() {
		this._companyService.updateDeliveryCondition(this.currentDeliveryCondition).subscribe(data => {
			if (data) {
				this._companyService.passDeliveryConditionToPushOrUpdate(data);
				let title = data.delivery_condition_description;
				let text = this.alerts.alertDCUpdatedSuccessfully;
				this._actionModalsService.buildSuccessModal(title, text);
				this.closeForm.emit(true);
			}
		}, () => {
			let text = this.alerts.alertDCUpdateError;
			this._actionModalsService.alertError(text);
			this.closeForm.emit(true);
		});
	}

	forgetActions() {
		this.closeForm.emit(true);
	}

}

class DCFCLanguages {

	static en = {
		"alerts": {
			"alertDCUpdatedSuccessfully": "Delivery Condition updated successfully",
			"alertDCUpdateError": "An error ocurred",
			"alertDCCreatedSuccessfully": "Delivery Condition created successfully",
			"alertDCCreateError": "An error ocurred"
		},
		"dcName": "Payment Method",
		"dcDescription": "Description",
		"errorRequired": "This field is required",
		"saveButton": "Save",
		"cancelButton": "Cancel"
	}
	
	static de = {
		"alerts": {
			"alertDCUpdatedSuccessfully": "Zahlungsbedinung erfolgreich aktualiziert",
			"alertDCUpdateError": "Es ist ein Fehler aufgetreten",
			"alertDCCreatedSuccessfully": "Zahlungsbedinung erfolgreich erstelt",
			"alertDCCreateError": "Es ist ein Fehler aufgetreten"
		},
		"dcName": "Zahlungsbedinung",
		"dcDescription": "Beschreibung",
		"errorRequired": "Pflichtfeld",
		"saveButton": "Speichern",
		"cancelButton": "Zurück"
	}

	static es = {
		"alerts": {
			"alertDCUpdatedSuccessfully": "Condicion de entrega actualizada exitosamente",
			"alertDCUpdateError": "Ha ocurrido un error",
			"alertDCCreatedSuccessfully": "Condición de entrega creada existosamente",
			"alertDCCreateError": "Ha ocurrido un error",
		},
		"dcName": "Método De Pago",
		"dcDescription": "Descripción",
		"errorRequired": "Campo requerido.",
		"saveButton": "Guardar",
		"cancelButton": "Cancelar"
	}

}

