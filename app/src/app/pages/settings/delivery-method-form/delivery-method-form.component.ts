import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { CompanyService } from '../../../shared/services/company.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
    selector: 'app-delivery-method-form',
    templateUrl: './delivery-method-form.component.html',
    styleUrls: ['./delivery-method-form.component.scss']
})

export class DeliveryMethodFormComponent implements OnInit {

    @Input() currentDeliveryMethod: any;
    @Input() editDeliveryMethod: boolean;
    @Input() newDeliveryMethod: boolean;
    @Output() closeForm = new EventEmitter<boolean>();

    deliveryMethodFormText: any;
    alerts: any;
    constructor(
        private _sharedService: SharedService,
        private _companyService: CompanyService,
        private _actionModalsService : ActionModalsService
    ) {
        let cl = this._sharedService.user_selected_language;
        this.deliveryMethodFormText = DMFCLanguages[cl];
        this.alerts = this.deliveryMethodFormText.alerts;
    }

    ngOnInit() {
 
    }

    saveDeliveryMethod() {
        this.newDeliveryMethod ? this.createDeliveryMethod() : this.updateDeliveryMethod();
    }

    createDeliveryMethod() {
        this._companyService.createDeliveryMethod(this.currentDeliveryMethod).subscribe(data => {
            if (data) {
                this._companyService.passDeliveryMethodToPushOrUpdate(data);
                let title = data.delivery_method_name;
                let text = this.alerts.alertDMCreatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            }
        }, () => {
            let text = this.alerts.alertDMCreateError;
            this._actionModalsService.alertError(text);
            this.closeForm.emit(true);
        });
    }

    updateDeliveryMethod(){
        this._companyService.updateDeliveryMethod(this.currentDeliveryMethod).subscribe(data => {
            if (data) {
                this._companyService.passDeliveryMethodToPushOrUpdate(data);
                let title = data.delivery_method_name;
                let text = this.alerts.alertDMUpdatedSuccessfully;
                this._actionModalsService.buildSuccessModal(title, text);
                this.closeForm.emit(true);
            }
        }, () => {
            let text = this.alerts.alertDMUpdateError;
            this._actionModalsService.alertError(text);
            this.closeForm.emit(true);
        });
    }

    forgetActions() {
        this.closeForm.emit(true);
    }

}

class DMFCLanguages {
    static en = {
        "alerts": {
            "alertDMUpdatedSuccessfully": "Delivery method updated successfully",
            "alertDMUpdateError": "An error ocurred",
            "alertDMCreatedSuccessfully": "Delivery method created successfully",
            "alertDMCreateError": "An error ocurred"
        },
        "dmName": "Payment Method",
        "errorRequired":"This field is required",
        "dmDescription": "Description",
        "saveButton": "Save",
        "cancelButton": "Cancel"
    }
    static de = {
        "alerts": {
            "alertDMUpdatedSuccessfully": "Zahlungsart erfolgreich aktualiziert",
            "alertDMUpdateError": "Es ist ein Fehler aufgetreten",
            "alertDMCreatedSuccessfully": "Zahlungsart erfolgreich erstelt",
            "alertDMCreateError": "Es ist ein Fehler aufgetreten"
        },
        "dmName": "Zahlungsart",
        "errorRequired":"Pflichtfeld",
        "dmDescription": "Beschreibung",
        "saveButton": "Speichern",
        "cancelButton": "Zurück"
    }
    static es = {
        "alerts": {
            "alertDMUpdatedSuccessfully": "Método de entrega actualizado exitosamente",
            "alertDMUpdateError": "Ha ocurrido un error",
            "alertDMCreatedSuccessfully": "Método de entrega creado existosamente",
            "alertDMCreateError": "Ha ocurrido un error"
        },
        "dmName": "Metodo de entrega",
        "errorRequired":"Campo requerido.",
        "dmDescription": "Descripción",
        "saveButton": "Guardar",
        "cancelButton": "Cancelar"
    }

}
