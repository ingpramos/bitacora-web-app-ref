import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';

@Component({
    selector: 'app-document-form',
    templateUrl: './document-form.component.html',
    styleUrls: ['./document-form.component.scss']
})

export class DocumentFormComponent implements OnInit {

    @Input() currentDocument;
    @Output() updatedDocument = new EventEmitter<any>();
    documentFormText: any;
    alerts: any;

    constructor(
        private _sharedService: SharedService,
    ) {
        let cl = this._sharedService.user_selected_language;
        this.documentFormText = IBFCLanguages[cl];
        this.alerts = this.documentFormText.alerts;
    }

    ngOnInit() {
    }

    updateDocument() {
        this.updatedDocument.emit(this.currentDocument);
        this.currentDocument = {};
    }

    forgetActions() {
        this.currentDocument = {};
        this.updatedDocument.emit(false);
    }

}

class IBFCLanguages {

    static en = {
        "orders":"Orders",
        "invoices":"Invoices",
        "offers":"Offers",
        "delivery_notes":"Delivery Notes",
        "order_confirmations":"Order Confirmations",
        "documentPrefix": "Document prefix",
        "documentCurrentNumber": "Current number",
        "documentContextText": "Document text",
        "errorRequired":"This field is required",
        "saveButton": "Save",
        "cancelButton": "Cancel"
    }
    static de = {
        "orders":"Bestellungen",
        "invoices":"Rechungen",
        "offers":"Angeboten",
        "delivery_notes":"Lieferschein",
        "order_confirmations":"Auftragsbestätigungen",
        "documentPrefix": "Dokument prefix",
        "documentCurrentNumber": "Aktuellenummer",
        "documentContextText": "Text",
        "errorRequired":"Pflichtfeld",
        "saveButton": "Speichern",
        "cancelButton": "Zurück"
    }

    static es = {
        "orders":"Ordenes de Compra",
        "invoices":"Facturas",
        "offers":"Ofertas",
        "delivery_notes":"Notas de entrega",
        "order_confirmations":"Pedidos",
        "documentPrefix": "Prefijo",
        "documentCurrentNumber": "Número actual",
        "documentContextText": "Texto",
        "errorRequired":"Campo requerido.",
        "saveButton": "Guardar",
        "cancelButton": "Cancelar"
    }

}
