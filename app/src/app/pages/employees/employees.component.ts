import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../shared/services/users.service';

@Component({
    selector: 'app-employees',
    templateUrl: './employees.component.html',
    styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

    employees: any = [];

    constructor(
        private _usersService: UsersService
    ) {

    }

    ngOnInit() {
        this.callEmployees();
    }

    callEmployees() {
        //this._usersService.getUsers();
    }

}
