import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { EmployeesComponent } from './employees.component';
import { EmployeeProfilComponent } from './employee-profil/employee-profil.component';

import { FormsModule } from '@angular/forms';
// Angular Material
import {
	MatMenuModule, MatInputModule, MatSelectModule,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule
} from '@angular/material';





const EMPLOYEES_ROUTES:Routes = [
  { path: '', component: EmployeesComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EMPLOYEES_ROUTES),
    MatMenuModule,
		MatInputModule,
		MatSelectModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
    FormsModule
  ],
  declarations: [EmployeesComponent, EmployeeProfilComponent]
})
export class EmployeesModule { }
