import { Component, OnInit, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { PdImportExcelComponent } from "../pd-import-excel/pd-import-excel.component";
import { SearchItemsToInsertInProjectsComponent } from "../search-items-to-insert-in-projects/search-items-to-insert-in-projects.component";
import { MatDialog } from "@angular/material/dialog";
import { PdInsertGroupsInProjectComponent } from "../pd-insert-groups-in-project/pd-insert-groups-in-project.component";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";

@Component({
    selector: "app-pd-group-header",
    templateUrl: "./pd-group-header.component.html",
    styleUrls: ["./pd-group-header.component.scss"]
})
export class PdGroupHeaderComponent implements OnInit {
    @Input() currentGroup: any = {};
    @Input() showMenu: boolean; // to show or hide three points menu

    groupHeaderText: any;
    itemsInAssembly: number = 0;
    subAssembliesInAssembly: number = 0;
    itemsSubscription;
    constructor(
        private _sharedService: SharedService,
        private _excelService: ExcelService,
        private _itemsInProjectsService: ItemsInProjectsService,
        public dialog: MatDialog
    ) {}

    ngOnInit() {
        let cl = this._sharedService.user_selected_language;
        this.groupHeaderText = PDGHLanguages[cl];
        this.itemsSubscription = this._itemsInProjectsService.currentItemsList.subscribe(data => {
            this.itemsInAssembly = data.filter(obj => obj["item_type"] === "item").length;
            this.subAssembliesInAssembly = data.filter(obj => obj["item_type"] === "sub_assembly").length;
        });
    }

    searchForItems() {
        let initialInformation = {};
        initialInformation["currentGroup"] = this.currentGroup;
        initialInformation["groupHeaderText"] = this.groupHeaderText;
        const dialogRef = this.dialog.open(SearchItemsToInsertInProjectsComponent, { width: "1200px", data: initialInformation });
    }
    searchForSubAssemblies() {
        let initialInformation = {};
        initialInformation["currentGroup"] = this.currentGroup;
        initialInformation["groupHeaderText"] = this.groupHeaderText;
        const dialogRef = this.dialog.open(PdInsertGroupsInProjectComponent, { width: "1200px", data: initialInformation });
    }

    openFileBrowser() {
        let element: HTMLElement = document.getElementById("i-input");
        element.click();
    }

    onFileChange(evt: any) {
        this._sharedService.startLoading();
        this._excelService.onFileChange(evt).subscribe(data => {
            this._sharedService.stopLoading();
            let initialState = { fileInformation: data["fileInformation"], itemsFromExcel: data["items"], selectedItems: data["items"] };
            initialState["currentGroup"] = this.currentGroup;
            initialState["groupHeaderText"] = this.groupHeaderText;
            const dialogRef = this.dialog.open(PdImportExcelComponent, { width: "1200px", data: initialState });
            dialogRef.afterClosed().subscribe(result => {
                (<HTMLInputElement>document.getElementById("i-input")).value = "";
            });
        });
    }
}

class PDGHLanguages {
    static en = {
        assemblyName: "Group Name",
        assemblyNumber: "Group Number",
        assemblyItems: "Items",
        subAssembliesInAssemblies: "Subgroups",
        assembledItems: "Assembled",
        completedPorcentage: "Completed",
        insertItems: "Insert Items",
        insertSubAssemblies: "Insert subgroups",
        uploadExcel: "Upload Excel Items",
        filterTable: "Search Item List"
    };

    static de = {
        assemblyName: "Baugruppe",
        assemblyNumber: "Nummer",
        assemblyItems: "Teile",
        subAssembliesInAssemblies: "Unterbaugruppen",
        assembledItems: "Eingebaut",
        completedPorcentage: "Fortschritt",
        insertItems: "Artikel einfügen",
        insertSubAssemblies: "Unterbaugruppe hinzufügen",
        uploadExcel: "Excel Datei hochladen",
        filterTable: "Tabelle Suchen"
    };

    static es = {
        assemblyName: "Grupo",
        assemblyNumber: "Número",
        assemblyItems: "Partes",
        subAssembliesInAssemblies: "Subgrupos",
        assembledItems: "Partes Ensambladas",
        completedPorcentage: "Avance",
        insertItems: "Insertar Piezas",
        insertSubAssemblies: "Insertar subgrupos",
        uploadExcel: "Importar Excel con Items",
        filterTable: "Buscar Item"
    };
}
