import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdGroupHeaderComponent } from './pd-group-header.component';

describe('PdGroupHeaderComponent', () => {
  let component: PdGroupHeaderComponent;
  let fixture: ComponentFixture<PdGroupHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdGroupHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdGroupHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
