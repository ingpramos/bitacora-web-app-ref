import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdItemFormComponent } from './pd-item-form.component';

describe('PdItemFormComponent', () => {
  let component: PdItemFormComponent;
  let fixture: ComponentFixture<PdItemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdItemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
