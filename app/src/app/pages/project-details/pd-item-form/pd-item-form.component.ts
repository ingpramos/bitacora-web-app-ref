import { Component, OnInit, Inject } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-pd-item-form",
    templateUrl: "./pd-item-form.component.html",
    styleUrls: ["./pd-item-form.component.scss"]
})
export class PdItemFormComponent implements OnInit {
    itemFormText: any;
    currentItem: any = this.modalData.currentItem;

    constructor(
		private _sharedServices: SharedService,
		private _actionModalsService: ActionModalsService,
        private _itemsInProjects: ItemsInProjectsService,
        public dialogRef: MatDialogRef<PdItemFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedServices.user_selected_language;
        this.itemFormText = PDIFLanguages[cl];
    }

    ngOnInit() {}

    updateItem() {
        this._itemsInProjects.updateItemInProject(this.currentItem).subscribe(data => {
            if (data) {
                this.currentItem["item_amount"] = data.item_amount;
				this.closeModal();
				let title = this.currentItem.item_code;
                let text = this.itemFormText.alertItemUpdateSuccess;
                this._actionModalsService.buildSuccessModal(title, text);
            }
		},
		() => {
			let message = this.itemFormText.alertItemUpdateError;
			this._actionModalsService.alertError(message);
		}
		);
    }

    closeModal() {
        this.dialogRef.close();
    }
}

class PDIFLanguages {
    static en = {
		alertItemUpdateSuccess: "Item updated successfully",
		alertItemUpdateError: "An error ocurred",
        modalTitle: "Update item information",
        itemCode: "Item Code",
        itemName: "Item Name",
        itemAmount: "Amount",
        subAssemblyNumber: "Subassembly Number",
        subAssemblyName: "Subassembly Name",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
		alertItemUpdateSuccess: "Artikel erfolgreich aktualiziert",
		alertItemUpdateError: "Es ist ein Fehler aufgetreten",
        modalTitle: "Artikel information aktualizieren",
        itemCode: "Artikel Code",
        itemName: "Name",
        itemAmount: "Menge",
        subAssemblyNumber: "Unterbaugruppe Nummer",
        subAssemblyName: "Unterbaugruppe Name",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
		alertItemUpdateSuccess: "Item actualizado exitosamente",
		alertItemUpdateError: "Ha ocurrido un error",
        modalTitle: "Actualizar información del articulo",
        itemCode: "Código Del Item",
        itemName: "Nombre",
        itemAmount: "Cantidad",
        subAssemblyNumber: "Número Del Subensamble",
        subAssemblyName: "Nombre Del Subensamble",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
