import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectsService } from '../../shared/services/projects.service';

@Component({
	selector: 'app-project-details',
	templateUrl: './project-details.component.html',
	styleUrls: ['./project-details.component.scss']
})

export class ProjectDetailsComponent implements OnInit, OnDestroy {

	// subscriptions
	currentGroupsSubscription;
	groupsToPushSubscription;
	aip_id;
	project_id
	groups = [];
	selectedGroup;
	currentProject = {};
	constructor(
		private activeRoute: ActivatedRoute,
		private _projectsService: ProjectsService
	) {
		this.activeRoute.params.subscribe(params => {
			this.aip_id = params.aip_id
			this.project_id = params.project_id;
			this.setSelectedGroup();
		})
		//this._projectsService.currentProject.subscribe(data => this.currentProject = data);
		this.groupsToPushSubscription = this._projectsService.subAssembliesToPush.subscribe(data => {
			Array.prototype.push.apply(this.groups, data);
		});
		this.currentGroupsSubscription = this._projectsService.currentGroupsList.subscribe(data => {
			this.groups = data;
			if (data) { this.setSelectedGroup(); }
		});
	}

	ngOnInit() {
		if (!Object.keys(this.currentProject).length) {
			this._projectsService.getProjectById(this.project_id).subscribe(data => this.currentProject = data);
			this._projectsService.getAssembliesFromProject(this.project_id);
		}
		this.setSelectedGroup();
	}

	ngOnDestroy(){
		this.currentGroupsSubscription.unsubscribe();
		this.groupsToPushSubscription.unsubscribe();
	}

	setSelectedGroup() {
		if (this.groups.length > 0) {
			let found = this.groups.find((obj) => obj['aip_id'] == this.aip_id);
			this.selectedGroup = found ? found : this.groups[0];
		}
		else {
			this.selectedGroup = {};
		}
	}

}
