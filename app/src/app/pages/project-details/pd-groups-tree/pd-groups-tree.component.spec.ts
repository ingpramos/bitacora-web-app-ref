import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdGroupsTreeComponent } from './pd-groups-tree.component';

describe('PdGroupsTreeComponent', () => {
  let component: PdGroupsTreeComponent;
  let fixture: ComponentFixture<PdGroupsTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdGroupsTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdGroupsTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
