import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { DynamicDatabase, DynamicFlatNode, DynamicDataSource } from './dinamic-data-source';

@Component({
	selector: 'app-pd-groups-tree',
	templateUrl: './pd-groups-tree.component.html',
	styleUrls: ['./pd-groups-tree.component.scss'],
	providers: [DynamicDatabase]
})

export class PdGroupsTreeComponent implements OnInit, OnChanges {

	@Input() groups;
	@Input() currentProject: any;

	pdGroupsListText;
	project_id;
	aip_id;
	groupToHandle = {};
	editGroup = false;

	constructor(
		private database: DynamicDatabase,
		private _sharedService: SharedService
	) {
		this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
		this.dataSource = new DynamicDataSource(this.treeControl, this.database);
		let cl = this._sharedService.user_selected_language;
		this.pdGroupsListText = PDGLLanguages[cl];
	}

	treeControl: FlatTreeControl<DynamicFlatNode>;

	dataSource: DynamicDataSource;

	getLevel = (node: DynamicFlatNode) => node.level;

	isExpandable = (node: DynamicFlatNode) => node.expandable;

	hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;

	ngOnInit() {
		this.dataSource.data = this.database.initialData(this.groups);
	}

	ngOnChanges(){
		// to update the (tree-view) data
		this.dataSource.data = this.database.initialData(this.groups);
	}

	startEditGroup(group) {
		this.groupToHandle = group;
		this.editGroup = true;
	}
	forgetActions() {
		this.editGroup = false;
	}
}


class PDGLLanguages {
	static en = {
		"editGroupInProject": "Edit Group",
		"deleteGroupFromProject": "Delete Group"

	}

	static de = {
		"editGroupInProject": "Baugruppe bearbeiten",
		"deleteGroupFromProject": "Baugruppe löschen"
	}

	static es = {
		"editGroupInProject": "Editar Grupo",
		"deleteGroupFromProject": "Eliminar Grupo"

	}
}