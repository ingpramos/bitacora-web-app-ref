import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';


export class DynamicFlatNode {
	constructor(
		public item: any,
		public level = 1,
		public expandable = false,
		public isLoading = false
	) { }
}

export class DynamicDatabase {
	dataCollection;
	/** Initial data from database */
	initialData(dataCollection): DynamicFlatNode[] {
		this.dataCollection = dataCollection;
		return dataCollection.filter(obj => obj['assembly_type'] === 1).map(obj => {
			if (obj['sub_assemblies_in_assembly'].length > 0) {
				return new DynamicFlatNode(obj, 0, true);
			} else {
				return new DynamicFlatNode(obj, 0, false);
			}
		});
	}
}

@Injectable()
export class DynamicDataSource {

	dataChange = new BehaviorSubject<DynamicFlatNode[]>([]);

	get data(): DynamicFlatNode[] { return this.dataChange.value; }
	set data(value: DynamicFlatNode[]) {
		this.treeControl.dataNodes = value;
		this.dataChange.next(value);
	}

	constructor(
		private treeControl: FlatTreeControl<DynamicFlatNode>,
		private database: DynamicDatabase
	) { }

	connect(collectionViewer: CollectionViewer): Observable<DynamicFlatNode[]> {
		this.treeControl.expansionModel.onChange.subscribe(change => {
			if ((change as SelectionChange<DynamicFlatNode>).added ||
				(change as SelectionChange<DynamicFlatNode>).removed) {
				this.handleTreeControl(change as SelectionChange<DynamicFlatNode>);
			}
		});

		return merge(collectionViewer.viewChange, this.dataChange).pipe(map(() => this.data));
	}

	/** Handle expand/collapse behaviors */
	handleTreeControl(change: SelectionChange<DynamicFlatNode>) {
		if (change.added) {
			change.added.forEach(node => this.toggleNode(node, true));
		}
		if (change.removed) {
			change.removed.slice().reverse().forEach(node => this.toggleNode(node, false));
		}
	}

	/**
	 * Toggle the node, remove from display list
	 */
	toggleNode(node: DynamicFlatNode, expand: boolean) {
		node.isLoading = true;
		const children = node.item.sub_assemblies_in_assembly.map(id => {
			return this.database.dataCollection.find(obj => obj['aip_id'] === id);
		});
		const index = this.data.indexOf(node);
		if (!children) { // If no children, or cannot find the node, no op
			return;
		}
		if (expand) { // aqui haz que llamar los datos de la db y quizas  anadir una propiedad que diga tiene sub emsables
			const nodes = children.map(obj => {
				if (obj['sub_assemblies_in_assembly'].length > 0) {
					return new DynamicFlatNode(obj, node.level + 1, true);
				} else {
					return new DynamicFlatNode(obj, node.level + 1, false);
				}
			});
			this.data.splice(index + 1, 0, ...nodes);
		} else {
			let count = 0;
			for (let i = index + 1; i < this.data.length
				&& this.data[i].level > node.level; i++ , count++) { }
			this.data.splice(index + 1, count);
		}

		// notify the change
		this.dataChange.next(this.data);
		node.isLoading = false;

	}
}