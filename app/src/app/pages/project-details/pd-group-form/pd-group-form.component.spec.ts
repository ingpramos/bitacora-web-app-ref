import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdGroupFormComponent } from './pd-group-form.component';

describe('PdGroupFormComponent', () => {
  let component: PdGroupFormComponent;
  let fixture: ComponentFixture<PdGroupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdGroupFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdGroupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
