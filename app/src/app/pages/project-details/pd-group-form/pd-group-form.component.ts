import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-pd-group-form",
    templateUrl: "./pd-group-form.component.html",
    styleUrls: ["./pd-group-form.component.scss"]
})
export class PdGroupFormComponent implements OnInit {
    @Input() editGroup;
    @Input() currentGroup: any;
    @Output() useForm = new EventEmitter<boolean>();
    pdGroupFormText: any;
    constructor(private _sharedService: SharedService, private _groupsService: GroupsService, private _actionModalsService: ActionModalsService) {
        let cl = this._sharedService.user_selected_language;
        this.pdGroupFormText = PDGFCLanguages[cl];
    }

    ngOnInit() {}

    saveGroup() {
        this._groupsService.updateAssembly(this.currentGroup).subscribe(data => {
            if (data) {
                this.useForm.emit(false);
                let title = this.currentGroup.assembly_number;
                let text = this.pdGroupFormText.alertGroupUpdatedSuccessfullyTitle;
                this._actionModalsService.buildSuccessModal(title, text);
            }
        });
    }

    forgetActions() {
        this.currentGroup = {};
        this.useForm.emit(false);
    }
}

class PDGFCLanguages {
    static en = {
        alertGroupUpdatedSuccessfullyTitle: "Group updated successfully",
        alertGroupCreatedSuccessfullyTitle: "Group created successfully",
        assemblyName: "Assembly Name",
        assemblyNumber: "Assembly Number",
        assemblyType: "Assembly Type",
        requiredField: "Required Field",
        assemblyTypeAssembly: "ASSEMBLY",
        assemblyTypeSubAssembly: "SUBASSEMBLY",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        alertGroupUpdatedSuccessfullyTitle: "Baugruppe erfolgreich aktualiziert",
        alertGroupCreatedSuccessfullyTitle: "Baugruppe erfolgreich erstelt",
        assemblyName: "Baugruppe Name",
        assemblyNumber: "Baugruppe Nummer",
        assemblyType: "Baugruppe Typ",
        requiredField: "Pflichtfeld",
        assemblyTypeAssembly: "BAUGRUPPE",
        assemblyTypeSubAssembly: "UNTERBAUGRUPPE",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alertGroupUpdatedSuccessfullyTitle: "Grupo actualizado exitosamente",
        alertGroupCreatedSuccessfullyTitle: "Grupo creado existosamente",
        assemblyName: "Nombre del ensamble",
        assemblyNumber: "Número del ensamble",
        assemblyType: "Tipo de ensamble",
        requiredField: "Campo requerido",
        assemblyTypeAssembly: "EMSAMBLE",
        assemblyTypeSubAssembly: "SUBENSAMBLE",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
