import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdItemsTableColumnsFormComponent } from './pd-items-table-columns-form.component';

describe('PdItemsTableColumnsFormComponent', () => {
  let component: PdItemsTableColumnsFormComponent;
  let fixture: ComponentFixture<PdItemsTableColumnsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdItemsTableColumnsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdItemsTableColumnsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
