import { Component, OnInit, Inject } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
    selector: "app-pd-items-table-columns-form",
    templateUrl: "./pd-items-table-columns-form.component.html",
    styleUrls: ["./pd-items-table-columns-form.component.scss"]
})
export class PdItemsTableColumnsFormComponent implements OnInit {
    constructor(
        private _sharedService: SharedService,
        private _columnsNamesService: ColumnsNamesService,
        private _actionModalService: ActionModalsService,
        public dialogRef: MatDialogRef<PdItemsTableColumnsFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {}

    itemsInProjectsTableColumnsText: any;
    defaultTableColumnsObj: any = this.modalData.defaultTableColumnsObj;
    currentTableColumns: any = this.modalData.currentTableColumns; // from the table
    currentColumnsObj: any; // for the form

    ngOnInit() {
        // to brake the bond with the object displayed at the view
        this.currentColumnsObj = JSON.parse(JSON.stringify(this.currentTableColumns));
        let cl = this._sharedService.user_selected_language;
        this.itemsInProjectsTableColumnsText = PDITCLanguages[cl];
    }

    saveColumns() {
        if (this._sharedService.isObjectEquivalent(this.currentColumnsObj, this.currentTableColumns)) {
            this.closeModal();
            return;
        }
        this._columnsNamesService.updateCustomColumnsNames(this.currentColumnsObj, "items_in_projects").subscribe(data => {
            if (data) {
                let title = this._columnsNamesService.formatSuccessData(data);
                let text = this.itemsInProjectsTableColumnsText.alertColumnsUpdatedSuccessfully;
                this._actionModalService.buildSuccessModal(title, text);
                this._columnsNamesService.buildCustomTableColumnsObject("items_in_projects", this.defaultTableColumnsObj);
                this.closeModal();
            }
        });
    }
    closeModal() {
        this.dialogRef.close();
    }
}

class PDITCLanguages {
    static en = {
        modalTitle: "Personalize table columns",
        alertColumnsUpdatedSuccessfully: "Columns updated successfully",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        modalTitle: "Tabellenspalten personalisieren",
        alertColumnsUpdatedSuccessfully: "Reie erfolgreich aktualiziert",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };
    static es = {
        modalTitle: "Personalizar Columnas",
        alertColumnsUpdatedSuccessfully: "Columnas actualizadas",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
