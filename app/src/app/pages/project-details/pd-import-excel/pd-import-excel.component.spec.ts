import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdImportExcelComponent } from './pd-import-excel.component';

describe('PdImportExcelComponent', () => {
  let component: PdImportExcelComponent;
  let fixture: ComponentFixture<PdImportExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdImportExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdImportExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
