import { Component, OnInit, Inject } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";
import { ExcelService } from "../../../shared/services/excel.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-pd-import-excel",
    templateUrl: "./pd-import-excel.component.html",
    styleUrls: ["./pd-import-excel.component.scss"]
})
export class PdImportExcelComponent implements OnInit {
    // modal data
    fileInformation: any = this.modalData.fileInformation;
    groupHeaderText: any = this.modalData.groupHeaderText;
    currentGroup: any = this.modalData.currentGroup;
    // tabs
    selectedTab = 0;
    showJustSelectedItems:boolean = true;
    tableText: any;
    alerts: any;
    tableColumns: any;
    items: any = [];
    selectedItems = [];
    allChecked = true;
    itemsFromExcel = this.modalData.itemsFromExcel;
    itemsAlreadyInList = [];
    itemsNotFoundInDB = [];
    user_id;

    constructor(
        private _itemsInProjectsService: ItemsInProjectsService,
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService,
        private _excelService: ExcelService,
        private _itemsService: ItemsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialogRef: MatDialogRef<PdImportExcelComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.tableText = PDIELanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
        this.user_id = this._sharedService.user_id;
    }

    ngOnInit() {
        if (this.itemsFromExcel.length > 0) {
            let itemsInCurrentAssembly = this._itemsInProjectsService.getCurrentAssemblyItems();
            this._excelService.labelItemsAlreadyInList(itemsInCurrentAssembly, this.itemsFromExcel);
            this.itemsAlreadyInList = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === true);
            let preSelectedItems: any[] = this.itemsFromExcel.filter((obj: any) => obj.is_already_in_list === false);
            if (preSelectedItems.length > 0) { // check in db if items exists or not
                let arrayOfCodes = preSelectedItems.map((obj: any) => obj["item_code"]);
                this._itemsService.checkIfItemsExists(arrayOfCodes).subscribe(itemsFound => {
                    this._excelService.labelItemsAlreadyInDB(itemsFound, preSelectedItems);
                    this.itemsNotFoundInDB = preSelectedItems.filter((obj: any) => obj.is_already_in_db === false);
                    this.selectedItems = preSelectedItems.filter((obj: any) => obj.is_already_in_db === true);
                    this.items = this.selectedItems;
                });
            }
        }
        this._columnsNamesService.formatColumnsOfImportTable(this.tableColumns);
    }

    applyChangesToList(allChecked) {
        this.itemsFromExcel.forEach((obj: any) => {
            if (obj.is_already_in_list === false && obj.is_already_in_db === true) {
                obj.is_checked = allChecked;
            }
        });
        this.getSelectedItems();
    }

    getSelectedItems() {
        this.selectedItems = this.itemsFromExcel.filter((obj: any) => {
            return obj.is_checked === true && obj.is_already_in_db === true && obj.is_already_in_list === false;
        });
    }

    saveItems() {
        const { project_id, assembly_id, aip_id } = this.currentGroup;
        let formattedItems = this.formatItems(this.selectedItems, project_id, assembly_id, aip_id);
        this._itemsInProjectsService.insertItemsInProject(formattedItems).subscribe(
            data => {
                this._itemsInProjectsService.getItemsFromAssembly(project_id, aip_id);
                this.closeModal();
                let title = this.currentGroup.assembly_number;
                let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertItemsInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatItems(collection, project_id, assembly_id, aip_id) {
        collection.forEach((obj: any) => {
            obj["project_id"] = project_id;
            obj["aip_id"] = aip_id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["user_id"] = this.user_id;
        });
        return collection;
    }

    closeModal() {
        this.dialogRef.close();
    }

    showSelectedItems(){
        this.items = this.selectedItems;
        this.showJustSelectedItems = true;
    }
    showItemsAlreadyInList(){
        this.items = this.itemsAlreadyInList;
        this.showJustSelectedItems = false;
    }
    showItemsNotFoundInDB(){
        this.items = this.itemsNotFoundInDB;
        this.showJustSelectedItems = false;
    }

    // Tabs related functions
    updateTab(index){ // update displayed view according the selected tab
        switch (index) {
            case 0 : this.showSelectedItems();
            break;
            case 1 : this.showItemsAlreadyInList();
            break;
            case 2 : this.showItemsNotFoundInDB();
            break;
        }        
    }
}

class PDIELanguages {
    static en = {
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        modalTitle: "Insert items in group",
        fileName: "File Name",
        itemsFound: "Items Found",
        itemsAlreadyInList: "Items already in group",
        itemsNotFoundInDB: "Not registered items",
        itemsSelected: "Selected items",
        checkColumnsNames: "No columns where recognized, please check the name of the header columns in your document, the columns marked with * are required.",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Baugruppe einfügen",
        fileName: "Dateiname",
        itemsFound: "Artikel gefunden",
        itemsAlreadyInList: "bereits vorhandene Artikel",
        itemsNotFoundInDB: "Nicht Registrierte Artikel",
        itemsSelected: "ausgewählte Artikel",
        checkColumnsNames:
            "Es wurden keine Spalten erkannt. Bitte überprüfen Sie den Namen der Kopfzeilenspalten in Ihrem Dokument. Die mit * gekennzeichneten Spalten sind erforderlich.",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar articulos en grupo",
        fileName: "Nombre del Archivo",
        itemsFound: "Items Encontrados",
        itemsAlreadyInList: "Articulos Repetidos",
        itemsNotFoundInDB: "Articulos no registrados",
        itemsSelected: "Items Seleccionados",
        checkColumnsNames:
            "no se reconocieron columnas, verifique el nombre de las columnas del encabezado en su documento, las columnas marcadas con * son obligatorias.",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
