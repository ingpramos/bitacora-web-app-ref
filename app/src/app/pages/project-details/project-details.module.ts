import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { PipesModule } from '../../shared/pipes/pipes.module';
import { FormsModule } from '@angular/forms';
import { PdGroupsListComponent } from './pd-groups-list/pd-groups-list.component';
import { ProjectDetailsComponent } from './project-details.component';
import { PdGroupHeaderComponent } from './pd-group-header/pd-group-header.component';
import { PdItemsTableComponent } from './pd-items-table/pd-items-table.component';
import { SearchItemsToInsertInProjectsComponent } from './search-items-to-insert-in-projects/search-items-to-insert-in-projects.component';
import { PdImportExcelComponent } from './pd-import-excel/pd-import-excel.component';
import { PdItemFormComponent } from './pd-item-form/pd-item-form.component';
import { PdGroupFormComponent } from './pd-group-form/pd-group-form.component';
import { PdItemsDetailsModalComponent } from './pd-items-details-modal/pd-items-details-modal.component';
import { PdItemsTableColumnsFormComponent } from './pd-items-table-columns-form/pd-items-table-columns-form.component';
import { PdInsertGroupsInProjectComponent } from './pd-insert-groups-in-project/pd-insert-groups-in-project.component';
import { PdGroupsTreeComponent } from './pd-groups-tree/pd-groups-tree.component';
import { DirectivesModule } from '../../shared/directives/directives.module';
// Angular Material
import {
	MatMenuModule, MatTabsModule, MatInputModule, MatFormFieldModule, MatDialogModule,
	 MAT_DIALOG_DEFAULT_OPTIONS,MatTreeModule,MatButtonModule,MatProgressBarModule,
} from '@angular/material';



const PROJECTS_DETAILS_ROUTES: Routes = [
	{ path: '', component: ProjectDetailsComponent }
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(PROJECTS_DETAILS_ROUTES),
		PipesModule,
		FormsModule,
		MatMenuModule,
		MatTabsModule,
		MatInputModule,
		MatFormFieldModule,
		MatDialogModule,
		MatTreeModule,
		MatButtonModule,
		MatProgressBarModule,
		DirectivesModule
	],
	declarations: [
		PdGroupsListComponent,
		ProjectDetailsComponent,
		PdGroupHeaderComponent,
		PdItemsTableComponent,
		SearchItemsToInsertInProjectsComponent,
		PdImportExcelComponent,
		PdItemFormComponent,
		PdGroupFormComponent,
		PdItemsDetailsModalComponent, 
		PdItemsTableColumnsFormComponent, 
		PdInsertGroupsInProjectComponent, 
		PdGroupsTreeComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents: [
		SearchItemsToInsertInProjectsComponent,
		PdImportExcelComponent,
		PdItemFormComponent,
		PdItemsDetailsModalComponent,
		PdItemsTableColumnsFormComponent,
		PdInsertGroupsInProjectComponent
	]
})
export class ProjectDetailsModule { }
