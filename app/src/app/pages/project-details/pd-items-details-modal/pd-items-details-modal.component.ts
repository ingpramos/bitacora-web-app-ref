import { Component, OnInit, Inject } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';
import { ItemsInProjectsService } from '../../../shared/services/items-in-projects.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectsService } from '../../../shared/services/projects.service';

@Component({
	selector: 'app-pd-items-details-modal',
	templateUrl: './pd-items-details-modal.component.html',
	styleUrls: ['./pd-items-details-modal.component.scss']
})
export class PdItemsDetailsModalComponent implements OnInit {

	modalText;
	tableHeaderText;
	currentItem = this.modalData.currentItem;
	activitiesText;
	companyCurrency : string = '';

	activitiesResume = [];
	totalActivitiesCost;
	constructor(
		private _sharedService: SharedService,
		private _itemsInProjectsService: ItemsInProjectsService,
		private _projectsService : ProjectsService,
		public dialogRef: MatDialogRef<PdItemsDetailsModalComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData

	) { 
		
	}

	ngOnInit() {
		this.companyCurrency = this._sharedService.currency;
		let cl = this._sharedService.user_selected_language;
		this.modalText = PDDMLanguages[cl];
		this.tableHeaderText = PDDMLanguages[cl].header;
		this.activitiesText = this.modalText.activities;
		let iip_id = this.currentItem.iip_id;
		if (this.currentItem.item_type === 'item') {
			this._itemsInProjectsService.getActivitiesFromItemInProject(iip_id).subscribe(data => {
				let costModel = this._projectsService.calculateTotalActivitiesCostModel(data);
				this.activitiesResume = costModel['activitiesResume'];
				this.totalActivitiesCost = costModel['totalActivitiesCost'];
			});
		}
		if (this.currentItem.item_type === 'assembly') {
			this._itemsInProjectsService.getActivitiesFromAssemblyInProject(iip_id).subscribe(data => {
				let costModel = this._projectsService.calculateTotalActivitiesCostModel(data);
				this.activitiesResume = costModel['activitiesResume'];
				this.totalActivitiesCost = costModel['totalActivitiesCost'];
			});
		}
	}


}

class PDDMLanguages {
	static en = {
		"modalTitleItem": "Item details",
		"modalTitleSubAssembly": "Group details",
		"itemName": "Item name",
		"assemblyName": "Group name",
		"itemCode": "Item Code",
		"assemblyNumber": "Group number",
		"itemAmount": "Amount",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Position",
			"activity_name": "Activity name",
			"activity_duration": "Duration",
			"hour_price": "Hour price",
			"activity_total_cost": "Cost",
			"activity_percentage": "Percentage",
			"activity_date": "Date",
			"activity_person": "Employee"
		},
		"activities": {
			"book": "Book",
			"finish": "Finish",
			"assembly": "Assembly"
		}
	};

	static de = {
		"modalTitleItem": "Artikeldetails",
		"modalTitleSubAssembly": "Baugruppe details",
		"itemName": "Artikelname",
		"assemblyName": "Baugruppe name",
		"itemCode": "Artikelcode",
		"assemblyNumber": "Baugruppe nummer",
		"itemAmount": "Menge",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Position",
			"activity_name": "Aktivität",
			"activity_duration": "Zeitdauer",
			"hour_price": "Stundenpreis",
			"activity_total_cost": "kosten",
			"activity_percentage": "Prozente",
			"activity_date": "Datum",
			"activity_person": "Mitarbeiter"
		},
		"activities": {
			"book": "Buchen",
			"finish": "Fertigen",
			"assembly": "Montieren"
		}
	};

	static es = {
		"modalTitleItem": "Detalles de Articulo",
		"modalTitleSubAssembly": "Detalles del grupo",
		"itemName": "Nombre",
		"assemblyName": "Nombre del grupo",
		"itemCode": "Código",
		"assemblyNumber": "Nr. grupo",
		"itemAmount": "Cantidad",
		"assemblyItems": "Partes ensambladas",
		"completedPorcentage": "Avance",
		"activitiesTotalCost": "Total",
		"header": {
			"activity_position": "Posición",
			"activity_name": "Actividad",
			"activity_duration": "Duración",
			"hour_price": "Precio hora",
			"activity_total_cost": "Costo",
			"activity_percentage": "Porcentage",
			"activity_date": "Fecha",
			"activity_person": "Empleado"
		},
		"activities": {
			"book": "Reservar",
			"finish": "Terminar",
			"assembly": "Ensamblar"
		}
	};
}
