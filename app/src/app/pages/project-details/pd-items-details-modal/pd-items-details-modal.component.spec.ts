import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdItemsDetailsModalComponent } from './pd-items-details-modal.component';

describe('PdItemsDetailsModalComponent', () => {
  let component: PdItemsDetailsModalComponent;
  let fixture: ComponentFixture<PdItemsDetailsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdItemsDetailsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdItemsDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
