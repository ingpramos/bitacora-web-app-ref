import { Component, OnInit, Inject } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ProjectsService } from "../../../shared/services/projects.service";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-pd-insert-groups-in-project",
    templateUrl: "./pd-insert-groups-in-project.component.html",
    styleUrls: ["./pd-insert-groups-in-project.component.scss"]
})
export class PdInsertGroupsInProjectComponent implements OnInit {
    allChecked: boolean;
    groupsTableText: any;
    groups = [];
    selectedGroups = [];
    currentGroup: any = this.modalData.currentGroup;
    groupHeaderText: any = this.modalData.groupHeaderText;
    alerts: any;
    // search related variables
    searchPattern: string;
    private subject: Subject<string> = new Subject();

    constructor(
        private _sharedService: SharedService,
        private _groupsService: GroupsService,
        private _projectsService: ProjectsService,
        private _itemsInProjectsService: ItemsInProjectsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<PdInsertGroupsInProjectComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.groupsTableText = PDIGIPCLanguages[cl];
        this.alerts = this.groupsTableText.alerts;
    }

    ngOnInit() {
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.groups = [];
                this.selectedGroups = [];
                return;
            } else {
                const assembly_id = this.currentGroup.assembly_id;
                this._groupsService.searchAllowedGroupsByPattern(assembly_id, this.searchPattern).subscribe(data => (this.groups = data));
            }
        });
    }

    searchGroups() {
        this.subject.next(this.searchPattern);
    }

    closeModal() {
        this.dialogRef.close();
    }

    applyChangesToList(allChecked) {
        this.groups.forEach((obj: any) => (obj.is_checked = allChecked));
        this.getSelectedGroups();
    }

    getSelectedGroups() {
        this.selectedGroups = this.groups.filter((obj: any) => obj.is_checked === true);
    }

    saveGroups() {
        let aip_id = this.currentGroup.aip_id;
        let array_of_assembly_ids = this.selectedGroups.map(obj => obj.assembly_id);
        this._projectsService.insertSubAssembliesInProject(array_of_assembly_ids, aip_id).subscribe(
            data => {
                let project_id = this.currentGroup.project_id;
                // to update the assemblies in projects collection (tree-view)
                this._projectsService.getAssembliesFromProject(project_id);
                this._itemsInProjectsService.getItemsFromAssembly(project_id, aip_id);
                this.closeModal();
                let title = this.currentGroup.assembly_number;
                let text = `${array_of_assembly_ids.length.toString()} ${this.alerts.alertAssembliesInsertedSuccessfully}`;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertAssembliesInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }
}

class PDIGIPCLanguages {
    static en = {
        modalTitle: "Insert subgroups in groups",
        assembly_number: "Group Number",
        assembly_name: "Group Name",
        items_in_assembly: "Items",
        sub_assemblies_in_assembly: "Subgroups",
        selected: "Selected",
        searchSubAssemblies: "Search groups",
        assembliesFound: "Groups found",
        itemsSelected: "Selected groups",
        saveButton: "Insert groups",
        cancelButton: "Cancel",
        alerts: {
            alertAssembliesInsertedSuccessfully: "Subgroups inserted successfully",
            alertAssembliesInsertError: "An error ocurred"
        }
    };

    static de = {
        modalTitle: "Unterbaugruppe in Baugruppe einfügen",
        assembly_number: "Baugruppe Nummer",
        assembly_name: "Baugruppe Name",
        items_in_assembly: "Teile",
        sub_assemblies_in_assembly: "Unterbaugruppen",
        selected: "Ausgewählt",
        searchSubAssemblies: "Baugruppen Suchen",
        assembliesFound: "Baugruppen gefunden",
        itemsSelected: "ausgewählte Baugruppen",
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück",
        alerts: {
            alertAssembliesInsertedSuccessfully: " Unterbaugruppe erfolgreich eingefügt",
            alertAssembliesInsertError: "Es ist ein Fehler aufgetreten"
        }
    };

    static es = {
        modalTitle: "Insertar subgrupo en grupo",
        assembly_number: "Número del grupo",
        assembly_name: "Nombre del grupo",
        items_in_assembly: "Piezas",
        sub_assemblies_in_assembly: "Subgrupos",
        selected: "Seleccionados",
        searchSubAssemblies: "Bucar grupos",
        assembliesFound: "Grupos econtrados",
        itemsSelected: "Grupos seleccionados",
        saveButton: "Insertar grupos",
        cancelButton: "Cancelar",
        alerts: {
            alertAssembliesInsertedSuccessfully: "Subensambles insertados exitosamente",
            alertAssembliesInsertError: "Ha ocurrido un error"
        }
    };
}
