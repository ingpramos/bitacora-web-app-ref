import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdInsertGroupsInProjectComponent } from './pd-insert-groups-in-project.component';

describe('PdInsertGroupsInProjectComponent', () => {
  let component: PdInsertGroupsInProjectComponent;
  let fixture: ComponentFixture<PdInsertGroupsInProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdInsertGroupsInProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdInsertGroupsInProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
