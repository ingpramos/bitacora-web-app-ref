import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdGroupsListComponent } from './pd-groups-list.component';

describe('PdGroupsListComponent', () => {
  let component: PdGroupsListComponent;
  let fixture: ComponentFixture<PdGroupsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdGroupsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdGroupsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
