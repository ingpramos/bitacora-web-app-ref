import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service';

@Component({
	selector: 'app-pd-groups-list',
	templateUrl: './pd-groups-list.component.html',
	styleUrls: ['./pd-groups-list.component.scss']
})
export class PdGroupsListComponent implements OnInit{

	@Input() groups;
	@Input() currentProject: any;
	
	pdGroupsListText;
	project_id;
	aip_id;
	groupToHandle = {};
	editGroup = false;
	
	constructor(
		private _sharedService : SharedService
		
	) {
		let cl = this._sharedService.user_selected_language;
		this.pdGroupsListText = PDGLLanguages[cl];
	}

	ngOnInit() {

	}

	startEditGroup(group){
		this.groupToHandle = group;
		this.editGroup = true;
	}
	forgetActions(){
		this.editGroup = false;
	}
}

class PDGLLanguages {
	static en = {
		"editGroupInProject":"Edit Group",
		"deleteGroupFromProject":"Delete Group"

	}

	static de = {
		"editGroupInProject":"Baugruppe Bearbeiten",
		"deleteGroupFromProject":"Baugruppe Löschen"
	}

	static es = {
		"editGroupInProject":"Editar Grupo",
		"deleteGroupFromProject":"Eliminar Grupo"

	}
}
