import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdItemsTableComponent } from './pd-items-table.component';

describe('PdItemsTableComponent', () => {
  let component: PdItemsTableComponent;
  let fixture: ComponentFixture<PdItemsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdItemsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdItemsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
