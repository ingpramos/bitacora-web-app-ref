import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { MatDialog } from "@angular/material/dialog";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";
import { PdItemFormComponent } from "../pd-item-form/pd-item-form.component";
import { PdItemsDetailsModalComponent } from "../pd-items-details-modal/pd-items-details-modal.component";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ColumnsNamesService } from "../../../shared/services/columns-names.service";
import { PdItemsTableColumnsFormComponent } from "../pd-items-table-columns-form/pd-items-table-columns-form.component";

@Component({
    selector: "app-pd-items-table",
    templateUrl: "./pd-items-table.component.html",
    styleUrls: ["./pd-items-table.component.scss"]
})
export class PdItemsTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() currentGroup: any;
    itemsInProjectText: any = {};
    currentLanguage:string;
    tableColumns: any = {};
    defaultTableColumnsObj: any = {};
    modalText: any = {};
    items = [];
    // subscriptions
    itemsList$;
    columnsNames$;

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _itemsInprojectsService: ItemsInProjectsService,
        private _columnsNamesService: ColumnsNamesService,
        public dialog: MatDialog
    ) {
        this.currentLanguage = this._sharedService.user_selected_language;
        this.itemsInProjectText = PDITLanguages[this.currentLanguage];
        this.modalText = this.itemsInProjectText.modal;
        this.defaultTableColumnsObj = this.itemsInProjectText.columns;
    }

    ngOnInit() {
        this.columnsNames$ = this._columnsNamesService.newCustomColumnsNamesObs.subscribe(data => this.tableColumns = data);        
        this.itemsList$ = this._itemsInprojectsService.currentItemsList.subscribe(data =>{
            this.items = data;
            this._sharedService.stopLoading();
        });        
        this._columnsNamesService.buildCustomTableColumnsObject("items_in_projects", this.defaultTableColumnsObj);
    }

    ngOnChanges(changes) {
        this._itemsInprojectsService.updateItemList([]);
        if (changes.currentGroup && changes.currentGroup != {}) {
            let aip_id = changes.currentGroup.currentValue.aip_id;
            let project_id = changes.currentGroup.currentValue.project_id;
            if (project_id && aip_id) {
                this._itemsInprojectsService.getItemsFromAssembly(project_id, aip_id);
                this._sharedService.startLoading();
            }
        }
    }

    ngOnDestroy() {
        this.itemsList$.unsubscribe();
        this.columnsNames$.unsubscribe();
    }

    editTableColumns() {
        let initialState = {
            defaultTableColumnsObj: this.defaultTableColumnsObj,
            currentTableColumns: this.tableColumns
        };
        const dialogRef = this.dialog.open(PdItemsTableColumnsFormComponent, { width: "1000px", data: initialState });
    }

    editItem(item) {
        let initialState = { currentItem: item };
        const dialogRef = this.dialog.open(PdItemFormComponent, { width: "1000px", data: initialState });
    }

    openItemDetails(item) {
        let initialState = { currentItem: item, currentGroup: this.currentGroup };
        const dialogRef = this.dialog.open(PdItemsDetailsModalComponent, { width: "1000px", data: initialState });
    }

    deleteItem(item, index){
        item["item_type"] == "item" ? this.deleteItemFromProject(item, index) : this.deleteAssemblyFromProject(item, index);
    }

    deleteItemFromProject(item, index) {
        let iip_id = item.iip_id;
        let title = item.item_code;
        let text = this.modalText.confirmDeleteItemFromProjectTitle;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let item_id = item.item_id;
                this._itemsInprojectsService.deleteItemFromProject(iip_id).subscribe(data => {
                    if (data) {
                        let title = item.item_code;
                        let text = this.modalText.alertItemDeletedSuccessfullyFromProject;
                        this.items.splice(index, 1);
                        this._actionModalsService.buildSuccessModal(title, text);
                    }
                });
            }
        });
    }

    deleteAssemblyFromProject(item, index) {
        let iip_id = item.iip_id;
        let title = item.item_code;
        let text = this.modalText.confirmDeleteItemFromProjectTitle;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let item_id = item.item_id;
                this._itemsInprojectsService.deleteItemFromProject(iip_id).subscribe(data => {
                    if (data) {
                        let title = item.item_code;
                        let text = this.modalText.alertItemDeletedSuccessfullyFromProject;
                        this.items.splice(index, 1);
                        this._actionModalsService.buildSuccessModal(title, text);
                    }
                });
            }
        });
    }
}

class PDITLanguages {
    static en = {
        title: "Items In Projects",
        columns: {
            item_position: "Pos",
            item_type: "Type",
            item_code: "Item Code",
            item_name: "Item Name",
            item_amount: "Amount",
            item_activity_done_name: "State"
        },
        modal: {
            confirmDeleteItemFromProjectTitle: "Are you sure you want to delete this item ?",
            alertItemDeletedSuccessfullyFromProject: "Item deleted successfully"
        },
        options: "Options",
        filterPlaceholder: "Search Items",
        editTableColumns: "Edit Columns",
        itemBooked: "Booked",
        itemFinished: "Finished",
        itemAssembled: "Assembled"
    };

    static de = {
        title: "Artikel In Projekt",
        columns: {
            item_position: "Pos",
            item_type: "Typ",
            item_code: "Artikel Code",
            item_name: "Name",
            item_amount: "Menge",
            item_activity_done_name: "Zustand"
        },
        modal: {
            confirmDeleteItemFromProjectTitle: "Sind Sie sicher das Sie dieses Artikel löschen möchte",
            alertItemDeletedSuccessfullyFromProject: "Artikel erfolgreich gelöscht"
        },
        options: "Optionen",
        filterPlaceholder: "Tabelle Suchen",
        editTableColumns: "Spaltennamen Bearbeiten",
        itemBooked: "Gebucht",
        itemFinished: "Fertig",
        itemAssembled: "Montiert"
    };

    static es = {
        title: "Items en Proyecto",
        columns: {
            item_position: "Pos",
            item_type: "Tipo",
            item_code: "Código del Item",
            item_name: "Nombre",
            item_amount: "Cantidad",
            item_activity_done_name: "Estado"
        },
        modal: {
            confirmDeleteItemFromProjectTitle: "Esta seguro que desea eliminar el item actual ?",
            alertItemDeletedSuccessfullyFromProject: "Item eliminado exitosamente"
        },
        options: "Opciones",
        filterPlaceholder: "Buscar Items",
        editTableColumns: "Editar Columnas",
        itemBooked: "Reservado",
        itemFinished: "Terminado",
        itemAssembled: "Ensamblado"
    };
}
