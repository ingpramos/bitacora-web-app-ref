import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemsToInsertInProjectsComponent } from './search-items-to-insert-in-projects.component';

describe('SearchItemsToInsertInProjectsComponent', () => {
  let component: SearchItemsToInsertInProjectsComponent;
  let fixture: ComponentFixture<SearchItemsToInsertInProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchItemsToInsertInProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemsToInsertInProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
