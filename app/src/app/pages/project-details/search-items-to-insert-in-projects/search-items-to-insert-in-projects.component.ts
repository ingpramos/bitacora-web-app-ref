import { debounceTime } from "rxjs/operators";
import { Subject } from "rxjs";
import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ItemsService } from "../../../shared/services/items.service";
import { ItemsInProjectsService } from "../../../shared/services/items-in-projects.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";

@Component({
    selector: "app-search-items-to-insert-in-projects",
    templateUrl: "./search-items-to-insert-in-projects.component.html",
    styleUrls: ["./search-items-to-insert-in-projects.component.scss"]
})
export class SearchItemsToInsertInProjectsComponent implements OnInit {
	selectedTab = 0;
	project_id;
    assembly_id;
    aip_id;
	user_id;
    currentGroup: any = this.modalData.currentGroup;
    groupHeaderText: any = this.modalData.groupHeaderText;
    tableText;
    alerts;
	tableColumns = {};
	// search tab
    private subject: Subject<string> = new Subject();
    searchPattern;
    items = [];
    itemsAlreadyInList: any[] = [];
    selectedItems = [];    
	// items confirmed Tab
	itemsToInsert : any[] = []; // to have a reference list to eventually choose from
	itemsConfirmedToInsert :any[] = []; // the ones which finnally will be inserted in the order   
	allConfirmedChecked:boolean = true; // checkbox for the confirm scope
	showJustSelectedItems:boolean = false;

    constructor(
        private _sharedService: SharedService,
        private _actionModalsService: ActionModalsService,
        private _itemsService: ItemsService,
        private _itemsInProjectsService: ItemsInProjectsService,
        public dialogRef: MatDialogRef<SearchItemsToInsertInProjectsComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        // language initialization
        let cl = this._sharedService.user_selected_language;
        this.tableText = SITIIPLanguages[cl];
        this.alerts = this.tableText.alerts;
        this.tableColumns = this.tableText.columns;
        this.user_id = this._sharedService.user_id;
    }

    ngOnInit() {
        this.project_id = this.currentGroup.project_id;
        this.assembly_id = this.currentGroup.assembly_id;
        this.aip_id = this.currentGroup.aip_id;

        this.subject.pipe(debounceTime(500)).subscribe(done => {
            if (this.searchPattern.length < 1 || this.searchPattern === undefined) {
                this.items = [];
                this.selectedItems = [];
                return;
            } else {
                this.callItems();
            }
        });
    }

    searchItems() {
        this.subject.next(this.searchPattern);
    }

    callItems() {
        this._itemsService.getItemsByNameOrCode(this.searchPattern).subscribe(data => {
            let itemsInProject = this._itemsInProjectsService.getCurrentAssemblyItems();
            this._itemsService.labelItemsAlreadyInList(itemsInProject, data);
            this.itemsAlreadyInList = data.filter((obj: any) => obj.is_already_in_list === true);
            this.items = data;
            this.updateSelectedItemsList();
        });
    }

    applyChangesToList(allChecked) {
        this.items.forEach((obj: any) => {
            if (!obj.is_already_in_list) {
                obj.is_checked = allChecked;
            }
        });
        this.updateSelectedItemsList();
    }

    updateSelectedItemsList() {
        this.selectedItems = this.items.filter((obj: any) => {
            return obj.is_checked === true && obj.is_already_in_list === false;
        });
	}
	
	applyChangesToComfirmedList(allConfirmedChecked:boolean){
        this.itemsToInsert.forEach((obj: any) => {
             obj.is_confirmed_to_be_inserted = allConfirmedChecked;
        });
        this.updateItemsToInsertCollection();
    }
    // function to be called when using the confirmed items Tab
    updateItemsToInsertCollection(){
        this.itemsConfirmedToInsert = this.itemsToInsert.filter((obj: any) => obj.is_confirmed_to_be_inserted === true);
    }
	
	showSelectedItems(){
        this.showJustSelectedItems = true;
        this.applyChangesToComfirmedList(true); // to initialize all checkboxes as confirmed
        this.updateItemsToInsertCollection();
		this.items = this.itemsToInsert;
    }
    
	confirmItems(){ 
        Array.prototype.push.apply(this.itemsToInsert, this.selectedItems);
        this.selectedItems = [];
        this.selectedTab = 1;
        this.showSelectedItems();
	}

	showSearchItems(){
        this.showJustSelectedItems = false;
        this.itemsToInsert = this.itemsConfirmedToInsert;
        this.items = [];
        this.searchPattern = "";
        this.itemsAlreadyInList = [];
        this.selectedTab = 0;
	}

    saveItems() {
        let itemsConfirmedToInsertFormatted = this.formatItems(this.itemsConfirmedToInsert);
        this._itemsInProjectsService.insertItemsInProject(itemsConfirmedToInsertFormatted).subscribe(
            data => {
                this.closeModal();
                this._itemsInProjectsService.getItemsFromAssembly(this.project_id, this.aip_id);
                let title = this.currentGroup.assembly_number;
                let text = `${data.length.toString()} ${this.alerts.alertItemsInsertedSuccessfully}`;
                this._actionModalsService.buildSuccessModal(title, text);
            },
            () => {
                let text = this.alerts.alertItemsInsertError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    formatItems(collection) {
        collection.forEach((obj: any) => {
            obj["project_id"] = this.project_id;
            obj["aip_id"] = this.aip_id;
            obj["item_amount"] = this._sharedService.parseFormattedStringAsNumber(obj["item_amount_to_show"]);
            obj["user_id"] = this.user_id;
        });
        return collection;
    }

    closeModal() {
        this.items = [];
        this.selectedItems = [];
        this.searchPattern = undefined;
        this.dialogRef.close();
	}
	    // Tabs related functions
		updateTab(index){ // update displayed view according the selected tab
			switch (index) {
				case 0 : this.showSearchItems();
				break;
				case 1 : this.showSelectedItems();
				break;
			}        
		}
}

class SITIIPLanguages {
    static en = {
        searchItems: "Search Items",
        columns: {
            item_position: "Position",
            item_code: "Item Code",
            item_amount: "Amount",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items inserted successfully",
            alertItemsInsertError: "An error ocurred"
        },
        modalTitle: "Insert items in purchase order",
		itemsFound: "Items Found",
		itemsAlreadyInList: "Items Already in Assembly",
        itemsSelected: "Items Selected",
		itemsConfirmed:"Items Confirmed",		
		confirmSelectedItems:"Confirm Selected Items",
		showSelectedItems:"Show Selected Items",
        saveButton: "Insert Items",
        cancelButton: "Cancel"
    };

    static de = {
        searchItems: "Artikel suchen",
        columns: {
            item_position: "Position",
            item_code: "Artikel Code",
            item_amount: "Menge",
            item_name: "Name"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Artikel erfolgreich eingefügt",
            alertItemsInsertError: "Es ist ein Fehler aufgetreten"
        },
        modalTitle: "Artikel in Projekt einfügen",
		itemsFound: "Artikel Gefunden",
		itemsAlreadyInList: "bereits vorhandene Artikel",
		itemsSelected: "ausgewählte Artikel",
		itemsConfirmed:"Artikel bestätigt",		
		confirmSelectedItems:"Ausgewählte Artikel bestätigen",
		showSelectedItems:"Ausgewählte Artikel anzeigen",        
        saveButton: "Artikel einfügen",
        cancelButton: "Zurück"
    };

    static es = {
        searchItems: "Buscar Items",
        columns: {
            item_position: "Posición",
            item_code: "Código del Item",
            item_amount: "Cantidad",
            item_name: "Nombre"
        },
        alerts: {
            alertItemsInsertedSuccessfully: "Items insertados exitosamente",
            alertItemsInsertError: "Ha ocurrido un error"
        },
        modalTitle: "Insertar articulos en proyecto",
		itemsFound: "Items Encontrados",
		itemsAlreadyInList: "Articulos Repetidos",
        itemsSelected: "Items Selecionados",
		itemsConfirmed:"Items Confirmados",		
		confirmSelectedItems:"Confirmar Items Seleccionados",
		showSelectedItems:"Mostrar Items Seleccionados",
        saveButton: "Insertar Items",
        cancelButton: "Cancelar"
    };
}
