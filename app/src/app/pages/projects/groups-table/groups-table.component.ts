import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';
import { GroupsService } from '../../../shared/services/groups.service';
import { ProjectsService } from '../../../shared/services/projects.service';
import { ActionModalsService } from '../../../shared/services/action-modals.service';

@Component({
	selector: 'app-groups-table',
	templateUrl: './groups-table.component.html',
	styleUrls: ['./groups-table.component.scss']
})
export class GroupsTableComponent implements OnInit {
	
	currentProject: any = this.modalData.currentProject;
	allChecked: boolean;
	groupTableText: any;
	alerts: any;
	groups = [];
	selectedGroups = [];
	
	constructor(
		private _actionModalsService: ActionModalsService,
		private _sharedService: SharedService,
		private _groupsServices: GroupsService,
		private _projectsService: ProjectsService,
		public dialogRef: MatDialogRef<GroupsTableComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) {
		let cl = this._sharedService.user_selected_language;
		this.groupTableText = GTLanguages[cl];
		this.alerts = this.groupTableText.alerts;
	}

	ngOnInit() {
		this.callAssemblies();
	}

	callAssemblies() {
		this._groupsServices.getGroupsData(1).subscribe(data => {
			this.groups = data;
		})
	}

	applyChangesToList(allChecked) {
		this.groups.forEach((obj: any) => {
			obj.is_checked = allChecked;
		});
		this.getSelectedGroups();
	}

	getSelectedGroups() {
		this.selectedGroups = this.groups.filter((obj: any)=> {
			return obj.is_checked === true;
		}, this);
	}

	saveGroups() {
		let project_id = this.currentProject.project_id;
		let array_of_assembly_ids = this.selectedGroups.map( a=>  a['assembly_id']);
		this._projectsService.insertAssembliesInProject(array_of_assembly_ids, project_id)
			.subscribe(data => {
				this._projectsService.getAssembliesFromProject(project_id);
				this.closeModal();
				let title = this.currentProject.order_confirmation_number;
				let text = `${data.length.toString()} ${this.alerts.alertAssembliesInsertedSuccessfully}`;
				this._actionModalsService.buildSuccessModal(title,text);
			},()=>{
				let text = this.alerts.alertInsertAssembliesError;
				this._actionModalsService.alertError(text);
			});
	}

	closeModal() {
		this.dialogRef.close();
	}

}

class GTLanguages {
	static en = {
		"alerts": {
			"alertAssembliesInsertedSuccessfully": "Groups inserted successfully",
			"alertInsertAssembliesError": "An error ocurred"
		},
		"modalTitle": "Insert Groups in project",
		"assembly_number": "Group Number",
		"assembly_name": "Group Name",
		"items_in_assembly": "Items",
		"sub_assemblies_in_assembly": "Subgroups",
		"selected": "Selected",
		"filterPlaceholder": "Search Items",
		"assembliesFound": "Groups Found",
		"itemsSelected": "Selected groups",
		"saveButton": "Insert groups",
		"cancelButton": "Cancel"
	};

	static de = {
		"alerts": {
			"alertAssembliesInsertedSuccessfully": "Baugruppen erfolgreich eingefügt",
			"alertInsertAssembliesError": "Es ist ein Fehler aufgetreten"
		},
		"modalTitle": "Baugruppe in Projekt einfügen",
		"assembly_number": "Baugruppe Nummer",
		"assembly_name": "Baugruppe Name",
		"items_in_assembly": "Teile",
		"sub_assemblies_in_assembly": "Unterbaugruppen",
		"selected": "Ausgewählt",
		"filterPlaceholder": "Tabelle Suchen",
		"assembliesFound": "Baugruppen gefunden",
		"itemsSelected": "ausgewählte Baugruppen",
		"saveButton": "Artikel einfügen",
		"cancelButton": "Zürruck"
	};

	static es = {
		"alerts": {
			"alertAssembliesInsertedSuccessfully": "Grupos insertados exitosamente",
			"alertInsertAssembliesError": "Ha ocurrido un error"
		},
		"modalTitle": "Insertar grupo en proyecto",
		"assembly_number": "Nr. de grupo",
		"assembly_name": "Nombre del grupo",
		"items_in_assembly": "Piezas",
		"sub_assemblies_in_assembly": "Subgrupos",
		"selected": "Seleccionados",
		"filterPlaceholder": "Bucar grupos",
		"assembliesFound": "Grupos econtrados",
		"itemsSelected": "Grupos seleccionados",
		"saveButton": "Insertar grupos",
		"cancelButton": "Cancelar"
	};

}
