import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from "@angular/router";
import { PipesModule } from '../../shared/pipes/pipes.module';
// Angular Material
import {
	MatMenuModule, MatInputModule, MatSelectModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS,
	MatFormFieldModule, MatDatepickerModule, MatNativeDateModule, MatIconModule
} from '@angular/material';
// Components 
import { ProjectsComponent } from './projects.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectFormComponent } from './project-form/project-form.component';
import { ProjectHeaderComponent } from './project-header/project-header.component';
import { ProjectGroupsListComponent } from './project-groups-list/project-groups-list.component';
import { GroupsTableComponent } from './groups-table/groups-table.component';
import { NewGroupFormComponent } from './new-group-form/new-group-form.component';
import { ProjectCostsModalComponent } from './project-costs-modal/project-costs-modal.component';



const PROJECTS_ROUTES: Routes = [
	{ path: '', component: ProjectsComponent }
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule.forChild(PROJECTS_ROUTES),
		PipesModule,
		MatMenuModule,
		MatInputModule,
		MatSelectModule,
		MatDialogModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatIconModule	
	],
	declarations: [
		ProjectsComponent,
		ProjectsListComponent,
		ProjectFormComponent,
		ProjectHeaderComponent,
		ProjectGroupsListComponent,
		GroupsTableComponent,
		NewGroupFormComponent,
		ProjectCostsModalComponent
	],
	providers: [
		{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
	],
	entryComponents:[
		GroupsTableComponent, 
		NewGroupFormComponent,
		ProjectCostsModalComponent
	]
})
export class ProjectsModule { }
