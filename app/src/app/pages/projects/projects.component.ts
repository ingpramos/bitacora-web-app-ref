import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ProjectsService } from '../../shared/services/projects.service';

@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
	styleUrls: ['./projects.component.scss']
})

export class ProjectsComponent implements OnInit, OnDestroy {
	// check component url
	componentUrl = this.activeRoute.snapshot['_urlSegment'].segments[1].path;
	openProjectsView = this.componentUrl === "open-projects" ? true : false;
	zeroProjects: boolean;
	// subscriptions
	projectsList$;
	projectToPushOrUpdate$;

	projects: any = [];
	project_id;
	selectedProject: any = {};

	constructor(
		private router: Router,
		private _projectsService: ProjectsService,
		private activeRoute: ActivatedRoute
	) {
		this.activeRoute.params.pipe(map(params => params['project_id'])).subscribe(project_id => {
			this.project_id = project_id;
			if (this.project_id) this.setSelectedProject();
		});
	}

	ngOnInit() {
		this.projectToPushOrUpdate$ = this._projectsService.projectToPushOrUpdate.subscribe(data => {
			let project_id = data.project_id;
            if (!project_id) return; // case {} an initialization
            let index = this.projects.findIndex(obj => obj['project_id'] == project_id);
            if (index == -1) {  // group was created 
                this.projects.push(data);
                this.router.navigate(['app/' + this.componentUrl, project_id]);
            } else { // group was updated
                this.projects.splice(index, 1, data);
                this.selectedProject = data;
            }
		});
		this.projectsList$ = this._projectsService.currentProjectsList.subscribe(data => {
			this.projects = data;
			this.setSelectedProject();
		});
		this.openProjectsView ? this._projectsService.getProjects(1) : this._projectsService.getProjects(0);
	}


	setSelectedProject() {
		this.projects.length > 0 ? this.zeroProjects = false : this.zeroProjects = true;
		if (this.projects && this.projects.length > 0) {
			let found = this.projects.find((obj) => obj['project_id'] == this.project_id);
			this.selectedProject = found ? found : this.projects[0];
			this.router.navigate(['app/' + this.componentUrl, this.selectedProject['project_id']]);
			this._projectsService.setCurrentProject(this.selectedProject);
			window.scroll(0, 0);
		}
	}
	ngOnDestroy() {
		this.projectToPushOrUpdate$.unsubscribe();
		this.projectsList$.unsubscribe();
		this._projectsService.updateProjectsList([]);
	}

}
