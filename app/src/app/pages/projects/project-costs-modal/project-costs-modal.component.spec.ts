import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectCostsModalComponent } from './project-costs-modal.component';

describe('ProjectCostsModalComponent', () => {
  let component: ProjectCostsModalComponent;
  let fixture: ComponentFixture<ProjectCostsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectCostsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectCostsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
