import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../../../shared/services/shared.service';
import { ProjectsService } from '../../../shared/services/projects.service';

@Component({
	selector: 'app-project-costs-modal',
	templateUrl: './project-costs-modal.component.html',
	styleUrls: ['./project-costs-modal.component.scss']
})
export class ProjectCostsModalComponent implements OnInit {

	currentProject;
	modalText;
	activitiesTableColumns;
	projectActivitiesResume: any[] = [];
	totalActivitiesCost = 0;
	companyCurrency : string = '';
	constructor(
		private _sharedService: SharedService,
		private _projectsService: ProjectsService,
		public dialogRef: MatDialogRef<ProjectCostsModalComponent>,
		@Inject(MAT_DIALOG_DATA) public modalData
	) {
		
	}

	ngOnInit() {
		this.currentProject = this.modalData.currentProject;
		this.companyCurrency = this._sharedService.currency;
		let cl = this._sharedService.user_selected_language;
		this.modalText = PCMCLanguages[cl];
		this.activitiesTableColumns = this.modalText.activitiesTableColumns;
		const project_id = this.currentProject.project_id;
		this._projectsService.getProjectActivitiesCosts(project_id).subscribe(data => {
			let costModel = this._projectsService.calculateTotalActivitiesCostModel(data);
			this.projectActivitiesResume = costModel['activitiesResume'];
			this.totalActivitiesCost = costModel['totalActivitiesCost'];
		});
	}

}

class PCMCLanguages {
	static en = {
		"modalTitle": "Costs Details",
		"activitiesTableTitle": "Activities Details",
		"activitiesTableColumns": {
			"position": "Position",
			"activity_name": "Activity Name",
			"activity_total_time": "Total time",
			"activity_price": "Price",
			"activity_total_price": "Total Cost",
			"activity_percentage": "Percentage"
		},
		"activitiesTotalCost": "Total",
		"ordersTableTitle": "Item in purchase orders",
		"ordersTableColumns": {
			"position": "Position",
			"order_number": "Order number",
			"provider_name": "Provider",
			"item_ordered_amount": "Ordered Amount",
			"received_amount": "Received Amount",
			"order_delivery_date": "Delivery Date"
		}
	};

	static de = {
		"modalTitle": "Kosten Details",
		"activitiesTableTitle": "Aktivitäten im Projekt",
		"activitiesTableColumns": {
			"position": "Position",
			"activity_name": "Aktivitätsname",
			"activity_total_time": "Gesamtzeit",
			"activity_price": "Preis",
			"activity_total_price": "Total",
			"activity_percentage": "Prozente"
		},
		"activitiesTotalCost": "Total",
		"ordersTableTitle": "Artikel im Bestellungen",
		"ordersTableColumns": {
			"position": "Position",
			"order_number": "Bestellungsnummer",
			"provider_name": "Lieferant",
			"item_ordered_amount": "Menge",
			"received_amount": "Erhalten",
			"order_delivery_date": "Liefertermin"
		}
	};

	static es = {
		"modalTitle": "Detalles de Costos",
		"activitiesTableTitle": "Actividades en Proyecto",
		"activitiesTableColumns": {
			"position": "Posición",
			"activity_name": "Actividad",
			"activity_total_time": "Tiempo Total",
			"activity_price": "Precio",
			"activity_total_price": "Total",
			"activity_percentage": "Porcentage"
		},
		"activitiesTotalCost": "Total",
		"ordersTableTitle": "Item en ordenes de compra",
		"ordersTableColumns": {
			"position": "Position",
			"order_number": "Orden de Compra",
			"provider_name": "Proveedor",
			"item_ordered_amount": "Cantidad",
			"received_amount": "Recivido",
			"order_delivery_date": "Fecha de Entrega"
		}
	};
}
