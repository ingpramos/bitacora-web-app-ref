import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SharedService } from "../../../shared/services/shared.service";
import { ProjectsService } from "../../../shared/services/projects.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ClientsService } from "../../../shared/services/clients.service";
import { InvoicesService } from "../../../shared/services/invoices.service";

@Component({
    selector: "app-project-form",
    templateUrl: "./project-form.component.html",
    styleUrls: ["./project-form.component.scss"]
})
export class ProjectFormComponent implements OnInit {
    @Input() editProject: boolean;
    @Input() newProject: boolean;
    @Input() currentProject: any;
    @Output() useForm = new EventEmitter<boolean>();

    showCurrentProjectNumberWhileEdit = false;
    readOnlyInput = false;
    projectFormText: any;
    alerts: any;
    clients: any = {};
    ordersConfirmed: any = {};

    constructor(
        private _actionModalsService: ActionModalsService,
        private _sharedService: SharedService,
        private _projectService: ProjectsService,
        private _clientsServices: ClientsService,
        private _invoicesService: InvoicesService
    ) {
        let cl = _sharedService.user_selected_language;
        this.projectFormText = PFCLanguage[cl];
        this.alerts = this.projectFormText.alerts;
        this._clientsServices.getFormattedClients().subscribe(data => (this.clients = data));
    }

    ngOnInit() {
        this.callOrdersConfirmation();
    }

    saveProject() {
        this.editProject ? this.updateProject() : this.createNewProject();
    }

    createNewProject() {
        this.currentProject["project_state"] = 1;
        this._projectService.createProject(this.currentProject).subscribe(
            data => {
                if (data) {
                    this._projectService.passProjectToPushOrUpdate(data);
                    let title = this.currentProject.project_name;
                    let text = this.alerts.alertProjectCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let text = this.alerts.alertProjectCreateError;
                this._actionModalsService.alertError(text);
                this.useForm.emit(false);
            }
        );
    }

    updateProject() {
        this._projectService.updateProject(this.currentProject).subscribe(
            data => {
                if (data) {
                    this._projectService.passProjectToPushOrUpdate(data);
                    let title = this.currentProject.project_name;
                    let text = this.alerts.alertProjectUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.useForm.emit(false);
                }
            },
            () => {
                let text = this.alerts.alertProjectUpdateError;
                this._actionModalsService.alertError(text);
                this.useForm.emit(false);
            }
        );
    }

    callOrdersConfirmation() {
        this._invoicesService.getAvailableOrdersConfirmation().subscribe(response => {
            this.ordersConfirmed = response;
        });
    }

    setDocument(event) {
        let docId = this.ordersConfirmed["get_internal_document_id"](event.value);
        let internalDocument = this.ordersConfirmed["original_data"].find(obj => {
            return obj["internal_document_id"] == docId;
        });
        this.setProperties(internalDocument);
    }

    setProperties(internalDocument) {
        this.currentProject["project_internal_document_id"] = internalDocument["internal_document_id"];
        this.currentProject["client_name"] = internalDocument["client_name"];
        this.readOnlyInput = true;
    }

    forgetActions() {
        this.currentProject = {};
        this.useForm.emit(false);
    }
}

class PFCLanguage {
    static en = {
        alerts: {
            alertProjectUpdatedSuccessfully: "Project updated successfully",
            alertProjectUpdateError: "An error ocurred",
            alertProjectCreatedSuccessfully: "Project created successfully",
            alertProjectCreateError: "An error ocurred"
        },
        projectName: "Project Name",
        projectNumber: "Project Number",
        projectType: "Type",
        client: "Client",
        deadLine: "Deadline",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };

    static de = {
        alerts: {
            alertProjectUpdatedSuccessfully: "Auftrag erfolgreich aktualiziert",
            alertProjectUpdateError: "Es ist ein Fehler aufgetreten",
            alertProjectCreatedSuccessfully: "Auftrag erfolgreich erstelt",
            alertProjectCreateError: "Es ist ein Fehler aufgetreten"
        },
        projectName: "Projektname",
        projectNumber: "Auftragsnummer",
        projectType: "Typ",
        client: "Kunde",
        deadLine: "Lifertermin",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertProjectUpdatedSuccessfully: "Proyecto actualizado exitosamente",
            alertProjectUpdateError: "Ha ocurrido un error",
            alertProjectCreatedSuccessfully: "Proyecto creado existosamente",
            alertProjectCreateError: "Ha ocurrido un error"
        },
        projectName: "Nombre del Proyecto",
        projectNumber: "Número del Proyecto",
        projectType: "Tipo",
        client: "Cliente",
        deadLine: "Fecha de Entrega",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
