import { Component, OnInit, OnChanges, OnDestroy, Input } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ProjectsService } from "../../../shared/services/projects.service";
import { NewGroupFormComponent } from "../new-group-form/new-group-form.component";

@Component({
    selector: "app-project-groups-list",
    templateUrl: "./project-groups-list.component.html",
    styleUrls: ["./project-groups-list.component.scss"]
})
export class ProjectGroupsListComponent implements OnInit, OnChanges, OnDestroy {
    @Input() project;
    groupsListText;
    assemblies: any = [];
    newAssembly$;
    groupsList$;

    constructor(private _sharedService: SharedService, private _projectsService: ProjectsService, public dialog: MatDialog) {}

    ngOnInit() {
        this.groupsList$ = this._projectsService.currentGroupsList.subscribe(data => {
            this.assemblies = data;
            this._sharedService.stopLoading();
        });
        this.newAssembly$ = this._projectsService.newGroupToPush.subscribe(data => {
            if (this.assemblies) this.assemblies.push(data);            
        });
        let cl = this._sharedService.user_selected_language;
        this.groupsListText = GLLanguages[cl];
    }

    ngOnChanges(changes) {
        let project_id = changes.project.currentValue.project_id;
        if (project_id) {
            this._projectsService.getAssembliesFromProject(project_id);
            this._sharedService.startLoading();
        }
        if (!project_id) {
            this._projectsService.setCurrentGroups([]);
        }
    }

    ngOnDestroy() {
        this.groupsList$.unsubscribe();
        this.newAssembly$.unsubscribe();
    }

    startEditGroup(assembly) {
        let initialInformation = {};
        initialInformation["currentProject"] = this.project;
        initialInformation["currentGroup"] = assembly;
        initialInformation["editGroup"] = true;
        const dialogRef = this.dialog.open(NewGroupFormComponent, { width: "1000px", data: initialInformation });
    }
}

class GLLanguages {
    static en = {
        assemblyName: "Group Name",
        assemblyNumber: "Group Number",
        assemblyItems: "Items",
        assembledItems: "Assembled",
        completedPorcentage: "Completed",
        editGroup: "Edit group",
        deleteGroup: "Delete group"
    };

    static de = {
        assemblyName: "Baugruppe",
        assemblyNumber: "Nummer",
        assemblyItems: "Teile",
        assembledItems: "Eingebaut",
        completedPorcentage: "komplett",
        editGroup: "Gruppe aktualizieren",
        deleteGroup: "Gruppe löschen"
    };

    static es = {
        assemblyName: "Grupo",
        assemblyNumber: "Número",
        assemblyItems: "Partes",
        assembledItems: "Partes Ensambladas",
        completedPorcentage: "Avance",
        editGroup: "Editar grupo",
        deleteGroup: "Eliminar grupo"
    };
}
