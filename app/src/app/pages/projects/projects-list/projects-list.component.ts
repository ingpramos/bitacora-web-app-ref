import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { ProjectsService } from "../../../shared/services/projects.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { ProjectCostsModalComponent } from "../project-costs-modal/project-costs-modal.component";

@Component({
    selector: "app-projects-list",
    templateUrl: "./projects-list.component.html",
    styleUrls: ["./projects-list.component.scss"]
})
export class ProjectsListComponent implements OnInit, OnDestroy {
    // check component url
    componentUrl = this.activatedRoute.snapshot["_urlSegment"].segments[1].path;
    openProjectsView = this.componentUrl === "open-projects" ? true : false;

    @Input() projects;

    projectsListText: any;
    alerts: any;
    projectToHandle;
    newProject: boolean = false;
    editProject: boolean = false;
    showSearch: boolean = false;

    private subject: Subject<string> = new Subject();
    searchPattern = "";
    private paramMap$;
    private project_id;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private _sharedService: SharedService,
        private _projectsService: ProjectsService,
        private _actionModalsService: ActionModalsService,
        public dialog: MatDialog
    ) {
        this.activatedRoute = activatedRoute;
        let cl = this._sharedService.user_selected_language;
        this.projectsListText = PLCLanguage[cl];
        this.alerts = this.projectsListText.alerts;
    }

    ngOnInit() {
        // to force routerLinkActive
        this.paramMap$ = this.activatedRoute.paramMap.subscribe(paramMap => (this.project_id = +paramMap.get("project_id")));
        this.subject.pipe(debounceTime(500)).subscribe(done => {
            this.searchPattern.length < 1 || this.searchPattern === undefined ? this.closeSearch() : this.searchForProjects();
        });
    }

    public ngOnDestroy() {
        this.paramMap$ && this.paramMap$.unsubscribe();
    }

    searchProjects() {
        this.subject.next(this.searchPattern);
    }

    searchForProjects() {
        let project_state = this.openProjectsView ? 1 : 0;
        this._projectsService.getProjectsByPattern(project_state, this.searchPattern);
    }

    closeSearch() {
        let project_state = this.openProjectsView ? 1 : 0;
        this.searchPattern = "";
        this._projectsService.getProjects(project_state);
        this.showSearch = false;
    }

    startSearch() {
        this.editProject = false;
        this.newProject = false;
        this.showSearch = true;
    }

    startNewProject() {
        this.projectToHandle = {};
        this.showSearch = false;
        this.editProject = false;
        this.newProject = true;
    }

    startEditProject(project) {
        this.projectToHandle = JSON.parse(JSON.stringify(project));
        this.showSearch = false;
        this.newProject = false;
        this.editProject = true;
    }

    forgetActions(event) {
        this.editProject = false;
        this.newProject = false;
    }

    startDeleteProject(project, index) {
        let title = project.project_name;
        let text = this.alerts.confirmDeleteProject;
        this._actionModalsService.buildConfirmModal(title, text).then(result => {
            if (result.value) {
                let project_id = project.project_id;
                this._projectsService.deleteProject(project_id).subscribe(
                    data => {
                        if (data) {
                            let text = this.alerts.alertProjectDeletedSuccessfully;
                            this._actionModalsService.buildSuccessModal(title, text);
                            this.projects.splice(index, 1);
                            this.router.navigate(["app/" + this.componentUrl, 0]);
                        }
                    },
                    e => {
                        if (e.error.error.code == "23503") {
                            // handle foreign key constrain
                            const text = this.alerts.alertForeignKeyViolation;
                            this._actionModalsService.buildInfoModal(title, text);
                        } else {
                            let text = this.alerts.alertProjectDeleteError;
                            this._actionModalsService.alertError(text);
                        }
                    }
                );
            }
        });
    }

    closeProject(project) {
        let project_id = project.project_id;
        this._projectsService.closeProject(project_id).subscribe(
            data => {
                this.router.navigate(["app/closed-projects", project_id]);
            },
            () => {
                let text = this.alerts.alertCloseProjectError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    showProjectCosts(project) {
        let initialInformation = {};
        initialInformation["currentProject"] = project;
        const dialogRef = this.dialog.open(ProjectCostsModalComponent, { width: "1000px", data: initialInformation });
    }
}

class PLCLanguage {
    static en = {
        alerts: {
            confirmDeleteProject: "Are you sure you want to delete this Project ?",
            alertProjectDeletedSuccessfully: "Project deleted successfully",
            alertProjectDeleteError: "An error ocurred",
            alertForeignKeyViolation: "This project is still referenced in one of your orders",
            alertCloseProjectError: "An error ocurred"
        },
        openProjectsTitle: "Open Projects",
        closedProjectsTitle: "Closed projects",
        newProject: "Create Project",
        searchProjects: "Search Projects",
        editProject: "Edit Project",
        deleteProject: "Delete Project",
        closeProject: "Close Project",
        projectCosts: "Check Project Costs",
        creationDate: "Created at",
        deadline: "Deadline"
    };

    static de = {
        alerts: {
            confirmDeleteProject: "Wollen Sie dieses Projekt löschen ?",
            alertProjectDeletedSuccessfully: "Projekt erfolgreich gelöscht",
            alertProjectDeleteError: "Es ist ein Fehler aufgetreten",
            alertForeignKeyViolation: "Dieser Projekt ist immer noch referenziert in einer Ihrer Bestellungen",
            alertCloseProjectError: "Es ist ein Fehler aufgetreten"
        },
        openProjectsTitle: "Offene Projekte",
        closedProjectsTitle: "Geschlosene Projekte",
        newProject: "Projekt erstellen",
        searchProjects: "Projekte suchen",
        editProject: "Projekt bearbeiten",
        deleteProject: "Projekt löschen",
        closeProject: "Projekt schließen",
        projectCosts: "Projektkosten anzeigen",
        creationDate: "Erstellt",
        deadline: "Lifertermin"
    };

    static es = {
        alerts: {
            confirmDeleteProject: "Esta seguro que deseas borrar este proyecto ?",
            alertProjectDeletedSuccessfully: "Proyecto eliminado exitosamente",
            alertProjectDeleteError: "Ha ocurrido un error",
            alertForeignKeyViolation: "Este proyecto está referenciado en una de sus ordenes de compra",
            alertCloseProjectError: "Ha ocurrido un error"
        },
        openProjectsTitle: "Proyectos Abiertos",
        closedProjectsTitle: "Proyectos Terminados",
        newProject: "Crear Proyecto",
        searchProjects: "Buscar Proyectos",
        editProject: "Editar Proyecto",
        deleteProject: "Borrar Proyecto",
        closeProject: "Cerrar Proyecto",
        projectCosts: "Mostrar costos del proyecto",
        creationDate: "Creación",
        deadline: "Entrega"
    };
}
