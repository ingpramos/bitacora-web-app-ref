import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ProjectsService } from "../../../shared/services/projects.service";
import { ActionModalsService } from "../../../shared/services/action-modals.service";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsService } from "../../../shared/services/groups.service";

@Component({
    selector: "app-new-group-form",
    templateUrl: "./new-group-form.component.html",
    styleUrls: ["./new-group-form.component.scss"]
})
export class NewGroupFormComponent implements OnInit {
    editGroup: boolean = this.modalData.editGroup;
    newGroup: boolean = this.modalData.newGroup;
    currentProject: any = this.modalData.currentProject;
    currentGroup: any = {};
    groupFormText: any;
    alerts: any;
    constructor(
        private _sharedService: SharedService,
        private _projectsService: ProjectsService,
        private _groupsService: GroupsService,
        private _actionModalsService: ActionModalsService,
        public dialogRef: MatDialogRef<NewGroupFormComponent>,
        @Inject(MAT_DIALOG_DATA) public modalData
    ) {
        let cl = this._sharedService.user_selected_language;
        this.groupFormText = GFLanguages[cl];
        this.alerts = this.groupFormText.alerts;
        if (this.editGroup) this.currentGroup = this.modalData.currentGroup;
    }

    ngOnInit() {}

    saveGroup() {
        this.editGroup ? this.updateGroup() : this.createGroup();
    }

    createGroup() {
        let project_id = this.currentProject.project_id;
        this.currentGroup.assembly_type = 1;
        this.currentGroup.assembly_visible = 0;
        this._projectsService.createAssemblyInProject(this.currentGroup, project_id).subscribe(
            data => {
                if (data) {
                    this._projectsService.passNewGroup(data);
                    this.closeModal();
                    let title = data.assembly_name;
                    let text = this.alerts.alertGroupCreatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                }
            },
            () => {
                let text = this.alerts.alertGroupCreateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    updateGroup() {
        this._groupsService.updateAssembly(this.currentGroup).subscribe(
            data => {
                if (data) {
                    let title = this.currentGroup.assembly_number;
                    let text = this.alerts.alertGroupUpdatedSuccessfully;
                    this._actionModalsService.buildSuccessModal(title, text);
                    this.closeModal();
                }
            },
            () => {
                let text = this.alerts.alertGroupUpdateError;
                this._actionModalsService.alertError(text);
            }
        );
    }

    closeModal() {
        this.dialogRef.close();
    }
}

class GFLanguages {
    static en = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Group updated successfully",
            alertGroupUpdateError: "An error ocurred",
            alertGroupCreatedSuccessfully: "Group created successfully",
            alertGroupCreateError: "An error ocurred"
        },
        modalTitleCreate: "Create group in project",
        modalTitleUpdate: "Update group in project",
        assemblyName: "Assembly Name",
        assemblyNumber: "Assembly Number",
        assemblyType: "Assembly Type",
        requiredField: "Required Field",
        requiredFieldsMessage: "Fields marked * are required fields.",
        saveButton: "Save",
        cancelButton: "Cancel"
    };
    static de = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Gruppe erfolgreich aktualiziert",
            alertGroupUpdateError: "Es ist ein Fehler aufgetreten",
            alertGroupCreatedSuccessfully: "Gruppe erfolgreich erstelt",
            alertGroupCreateError: "Es ist ein Fehler aufgetreten"
        },
        modalTitleCreate: "Baugruppe in Projekt erstellen",
        modalTitleUpdate: "Baugruppe in Projekt aktualizieren",
        assemblyName: "Baugruppe Name",
        assemblyNumber: "Baugruppe Nummer",
        assemblyType: "Baugruppe Typ",
        requiredField: "Pflichtfeld",
        requiredFieldsMessage: "Mit * gekennzeichnete Felder sind Pflichtfelder.",
        saveButton: "Speichern",
        cancelButton: "Zurück"
    };

    static es = {
        alerts: {
            alertGroupUpdatedSuccessfully: "Grupo actualizado Exitosamente",
            alertGroupUpdateError: "Ha ocurrido un error",
            alertGroupCreatedSuccessfully: "Grupo creado existosamente",
            alertGroupCreateError: "Ha ocurrido un error"
        },
        modalTitleCreate: "Crear grupo en proyecto",
        modalTitleUpdate: "Actualizar grupo en proyecto",
        assemblyName: "Nombre del grupo",
        assemblyNumber: "Número del grupo",
        assemblyType: "Tipo de Ensamble",
        requiredField: "Campo Requerido",
        requiredFieldsMessage: "Los campos marcados con * son requeridos.",
        saveButton: "Guardar",
        cancelButton: "Cancelar"
    };
}
