import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { SharedService } from "../../../shared/services/shared.service";
import { GroupsTableComponent } from "../groups-table/groups-table.component";
import { NewGroupFormComponent } from "../new-group-form/new-group-form.component";

@Component({
    selector: "app-project-header",
    templateUrl: "./project-header.component.html",
    styleUrls: ["./project-header.component.scss"]
})
export class ProjectHeaderComponent implements OnInit, OnChanges {
    @Input() project;
    @Input() zeroProjects: boolean; // to enable or disable menu items
    @Input() openProjectsView: boolean;
    @Input() showMenu: boolean; // to show or hide three points menu

    projectHeaderText: any;

    constructor(private _sharedService: SharedService, public dialog: MatDialog) {
        let cl = this._sharedService.user_selected_language;
        this.projectHeaderText = PHLanguage[cl];
    }

    ngOnInit() {}

    searchAssemblies() {
        let initialInformation = {};
        initialInformation["currentProject"] = this.project;
        initialInformation["projectHeaderText"] = this.projectHeaderText;
        const dialogRef = this.dialog.open(GroupsTableComponent, { width: "1000px", data: initialInformation });
    }
    createNewAssembly() {
        let initialInformation = {};
        initialInformation["newGroup"] = true;
        initialInformation["currentProject"] = this.project;
        const dialogRef = this.dialog.open(NewGroupFormComponent, { width: "1000px", data: initialInformation });
    }
    ngOnChanges() {}
}

class PHLanguage {
    static en = {
        projectName: "Project Name",
        projectNumber: "Project Number",
        projectAssemblies: "Assemblies",
        projectType: "Type",
        projectClient: "Client",
        completedPercentage: "Progress",
        projectCreation:"Start",
        projectDeadline: "Deadline",
        insertAssembly: "Insert assemblies",
        insertNewAssembly: "Insert new assembly"
    };

    static de = {
        projectName: "Projektname",
        projectNumber: "Auftragsnummer",
        projectAssemblies: "Baugruppen",
        projectType: "Typ",
        projectClient: "Kunde",
        completedPercentage: "Fortschritt",
        projectCreation:"Erstellt",
        projectDeadline: "Termin",
        insertAssembly: "Baugruppen hinzufügen",
        insertNewAssembly: "Baugruppe erstellen"
    };

    static es = {
        projectName: "Nombre del proyecto",
        projectNumber: "Número",
        projectAssemblies: "Ensambles",
        projectType: "Tipo",
        projectClient: "Cliente",
        completedPercentage: "Avance",
        projectCreation:"Inicio",
        projectDeadline: "Entrega",
        insertAssembly: "Insertar Ensambles",
        insertNewAssembly: "Crear Ensamble"
    };
}
