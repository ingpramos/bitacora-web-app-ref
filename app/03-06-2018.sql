--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3 (Ubuntu 10.3-1)
-- Dumped by pg_dump version 10.3 (Ubuntu 10.3-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: assemblies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assemblies (
    assembly_id integer NOT NULL,
    company_id text NOT NULL,
    assembly_name text NOT NULL,
    assembly_number text NOT NULL,
    assembly_type integer,
    assembly_creation_date timestamp(0) without time zone DEFAULT now()
);


ALTER TABLE public.assemblies OWNER TO postgres;

--
-- Name: assemblies_assembly_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assemblies_assembly_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assemblies_assembly_id_seq OWNER TO postgres;

--
-- Name: assemblies_assembly_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assemblies_assembly_id_seq OWNED BY public.assemblies.assembly_id;


--
-- Name: assemblies_in_projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assemblies_in_projects (
    aip_id integer NOT NULL,
    project_id integer NOT NULL,
    assembly_id integer NOT NULL
);


ALTER TABLE public.assemblies_in_projects OWNER TO postgres;

--
-- Name: assemblies_in_projects_aip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assemblies_in_projects_aip_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assemblies_in_projects_aip_id_seq OWNER TO postgres;

--
-- Name: assemblies_in_projects_aip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assemblies_in_projects_aip_id_seq OWNED BY public.assemblies_in_projects.aip_id;


--
-- Name: client_contacts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_contacts (
    cc_id integer NOT NULL,
    client_id integer NOT NULL,
    contact_name character varying(150),
    contact_last_name character varying(150),
    contact_telephone character varying(150),
    contact_email character varying(150)
);


ALTER TABLE public.client_contacts OWNER TO postgres;

--
-- Name: client_contacts_cc_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_contacts_cc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_contacts_cc_id_seq OWNER TO postgres;

--
-- Name: client_contacts_cc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_contacts_cc_id_seq OWNED BY public.client_contacts.cc_id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clients (
    client_id integer NOT NULL,
    company_id text NOT NULL,
    client_name character varying(100) NOT NULL,
    client_address character varying(100) NOT NULL,
    client_number character varying(20)
);


ALTER TABLE public.clients OWNER TO postgres;

--
-- Name: clients_client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clients_client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_client_id_seq OWNER TO postgres;

--
-- Name: clients_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clients_client_id_seq OWNED BY public.clients.client_id;


--
-- Name: columns_names; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.columns_names (
    column_id integer NOT NULL,
    column_name character varying,
    column_table character varying
);


ALTER TABLE public.columns_names OWNER TO postgres;

--
-- Name: columns_names_column_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.columns_names_column_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.columns_names_column_id_seq OWNER TO postgres;

--
-- Name: columns_names_column_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.columns_names_column_id_seq OWNED BY public.columns_names.column_id;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id integer NOT NULL,
    company_id text NOT NULL,
    company_tax_payer_id text NOT NULL,
    company_name text NOT NULL,
    company_address text,
    company_zip_code integer NOT NULL,
    company_country text,
    company_state integer DEFAULT 1,
    company_creation_date timestamp(0) without time zone DEFAULT now(),
    company_url_logo character varying(200) DEFAULT 'NONE'::character varying,
    company_url_image_brand character varying(200) DEFAULT 'NONE'::character varying,
    company_images text,
    company_website text,
    company_city text,
    company_telephone character varying(30),
    company_fax character varying(30),
    company_email character varying(150)
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.companies_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.companies_id_seq OWNER TO postgres;

--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.companies_id_seq OWNED BY public.companies.id;


--
-- Name: customized_columns_names; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customized_columns_names (
    ccn_id integer NOT NULL,
    company_id text NOT NULL,
    column_id integer,
    customized_name character varying
);


ALTER TABLE public.customized_columns_names OWNER TO postgres;

--
-- Name: customized_columns_names_ccn_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customized_columns_names_ccn_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customized_columns_names_ccn_id_seq OWNER TO postgres;

--
-- Name: customized_columns_names_ccn_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customized_columns_names_ccn_id_seq OWNED BY public.customized_columns_names.ccn_id;


--
-- Name: filter_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filter_groups (
    filter_group_id integer NOT NULL,
    company_id text NOT NULL,
    filter_group_name text
);


ALTER TABLE public.filter_groups OWNER TO postgres;

--
-- Name: filter_groups_filter_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filter_groups_filter_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filter_groups_filter_group_id_seq OWNER TO postgres;

--
-- Name: filter_groups_filter_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filter_groups_filter_group_id_seq OWNED BY public.filter_groups.filter_group_id;


--
-- Name: filter_values; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.filter_values (
    filter_id integer NOT NULL,
    filter_group_id integer NOT NULL,
    company_id text NOT NULL,
    filter_value text
);


ALTER TABLE public.filter_values OWNER TO postgres;

--
-- Name: filter_values_filter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.filter_values_filter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filter_values_filter_id_seq OWNER TO postgres;

--
-- Name: filter_values_filter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.filter_values_filter_id_seq OWNED BY public.filter_values.filter_id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items (
    item_id integer NOT NULL,
    company_id text NOT NULL,
    item_code text NOT NULL,
    item_name text NOT NULL,
    item_raw_material_dimensions text DEFAULT 'NONE'::text,
    item_creation_date timestamp(0) without time zone DEFAULT now(),
    item_order_number character varying(50),
    item_barcode character varying(50)
);


ALTER TABLE public.items OWNER TO postgres;

--
-- Name: items_in_assemblies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_assemblies (
    iia_id integer NOT NULL,
    assembly_id integer NOT NULL,
    item_id integer NOT NULL,
    item_amount numeric(5,1) DEFAULT 1 NOT NULL,
    CONSTRAINT items_in_assemblies_item_amount_check CHECK ((item_amount >= (0)::numeric))
);


ALTER TABLE public.items_in_assemblies OWNER TO postgres;

--
-- Name: items_in_assemblies_iia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_assemblies_iia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_assemblies_iia_id_seq OWNER TO postgres;

--
-- Name: items_in_assemblies_iia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_assemblies_iia_id_seq OWNED BY public.items_in_assemblies.iia_id;


--
-- Name: items_in_offers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_offers (
    iioffer_id integer NOT NULL,
    offer_id integer NOT NULL,
    item_id integer NOT NULL,
    item_amount numeric(5,1) DEFAULT 1 NOT NULL,
    item_offered_price numeric(8,2),
    user_id integer,
    CONSTRAINT items_in_offers_item_amount_check CHECK ((item_amount >= (0)::numeric))
);


ALTER TABLE public.items_in_offers OWNER TO postgres;

--
-- Name: items_in_offers_iioffer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_offers_iioffer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_offers_iioffer_id_seq OWNER TO postgres;

--
-- Name: items_in_offers_iioffer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_offers_iioffer_id_seq OWNED BY public.items_in_offers.iioffer_id;


--
-- Name: items_in_orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_orders (
    iio_id integer NOT NULL,
    item_id integer NOT NULL,
    order_id integer NOT NULL,
    item_amount numeric(5,1) DEFAULT 1,
    received_amount numeric(5,1) DEFAULT 0 NOT NULL,
    last_amount_added numeric(5,1) DEFAULT 0 NOT NULL,
    item_buy_price numeric(8,2),
    user_id integer,
    item_received_date timestamp(0) without time zone DEFAULT now(),
    item_delivered boolean DEFAULT false,
    CONSTRAINT items_in_orders_item_amount_check CHECK ((item_amount >= (0)::numeric)),
    CONSTRAINT items_in_orders_last_amount_added_check CHECK ((last_amount_added >= (0)::numeric)),
    CONSTRAINT items_in_orders_received_amount_check CHECK ((received_amount >= (0)::numeric))
);


ALTER TABLE public.items_in_orders OWNER TO postgres;

--
-- Name: items_in_orders_iio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_orders_iio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_orders_iio_id_seq OWNER TO postgres;

--
-- Name: items_in_orders_iio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_orders_iio_id_seq OWNED BY public.items_in_orders.iio_id;


--
-- Name: items_in_projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_projects (
    iip_id integer NOT NULL,
    project_id integer NOT NULL,
    item_id integer NOT NULL,
    item_amount numeric(5,1) DEFAULT 1 NOT NULL,
    assembly_id integer,
    aip_id integer,
    saip_id integer,
    state_id integer DEFAULT 1,
    state_date timestamp(0) without time zone DEFAULT now(),
    user_id integer,
    CONSTRAINT items_in_projects_item_amount_check CHECK ((item_amount >= (0)::numeric))
);


ALTER TABLE public.items_in_projects OWNER TO postgres;

--
-- Name: items_in_projects_iip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_projects_iip_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_projects_iip_id_seq OWNER TO postgres;

--
-- Name: items_in_projects_iip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_projects_iip_id_seq OWNED BY public.items_in_projects.iip_id;


--
-- Name: items_in_projects_states; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_projects_states (
    iips_id integer NOT NULL,
    iip_id integer,
    state_id integer,
    state_date timestamp(0) without time zone DEFAULT now(),
    user_id integer
);


ALTER TABLE public.items_in_projects_states OWNER TO postgres;

--
-- Name: items_in_projects_states_iips_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_projects_states_iips_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_projects_states_iips_id_seq OWNER TO postgres;

--
-- Name: items_in_projects_states_iips_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_projects_states_iips_id_seq OWNED BY public.items_in_projects_states.iips_id;


--
-- Name: items_in_stock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_in_stock (
    iis_id integer NOT NULL,
    item_id integer,
    user_id integer,
    item_amount numeric(5,1) DEFAULT 0 NOT NULL,
    last_amount_added numeric(5,1) DEFAULT 0 NOT NULL,
    last_update timestamp(0) without time zone,
    item_location text DEFAULT 'NONE'::text,
    CONSTRAINT items_in_stock_item_amount_check CHECK ((item_amount >= (0)::numeric))
);


ALTER TABLE public.items_in_stock OWNER TO postgres;

--
-- Name: items_in_stock_iis_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_in_stock_iis_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_in_stock_iis_id_seq OWNER TO postgres;

--
-- Name: items_in_stock_iis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_in_stock_iis_id_seq OWNED BY public.items_in_stock.iis_id;


--
-- Name: items_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_item_id_seq OWNER TO postgres;

--
-- Name: items_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_item_id_seq OWNED BY public.items.item_id;


--
-- Name: items_processes_times; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_processes_times (
    ipt_id integer NOT NULL,
    company_id text NOT NULL,
    item_id integer NOT NULL,
    state_id integer NOT NULL,
    user_id integer NOT NULL,
    processed_amount numeric(5,1) DEFAULT 1 NOT NULL,
    process_time numeric(4,1) DEFAULT 1 NOT NULL,
    process_time_date timestamp(0) without time zone DEFAULT now(),
    project_numbers text[],
    CONSTRAINT items_processes_times_processed_amount_check CHECK ((processed_amount >= (0)::numeric)),
    CONSTRAINT items_processes_times_processed_amount_check1 CHECK ((processed_amount >= (0)::numeric))
);


ALTER TABLE public.items_processes_times OWNER TO postgres;

--
-- Name: items_processes_times_ipt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_processes_times_ipt_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_processes_times_ipt_id_seq OWNER TO postgres;

--
-- Name: items_processes_times_ipt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_processes_times_ipt_id_seq OWNED BY public.items_processes_times.ipt_id;


--
-- Name: items_with_filters; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items_with_filters (
    iwf_id integer NOT NULL,
    item_id integer NOT NULL,
    filter_id integer NOT NULL,
    filter_group_id integer NOT NULL
);


ALTER TABLE public.items_with_filters OWNER TO postgres;

--
-- Name: items_with_filters_iwf_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_with_filters_iwf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_with_filters_iwf_id_seq OWNER TO postgres;

--
-- Name: items_with_filters_iwf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_with_filters_iwf_id_seq OWNED BY public.items_with_filters.iwf_id;


--
-- Name: offers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offers (
    offer_id integer NOT NULL,
    offer_number character varying(20) NOT NULL,
    company_id text NOT NULL,
    client_id integer NOT NULL,
    client_contact_id integer NOT NULL,
    inquiry_id integer,
    offer_creation_date timestamp(0) without time zone DEFAULT now(),
    offer_expiration_date timestamp without time zone,
    payment_method character varying,
    payment_conditions character varying,
    user_id integer
);


ALTER TABLE public.offers OWNER TO postgres;

--
-- Name: offers_offer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.offers_offer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offers_offer_id_seq OWNER TO postgres;

--
-- Name: offers_offer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.offers_offer_id_seq OWNED BY public.offers.offer_id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    order_id integer NOT NULL,
    company_id text,
    order_number text NOT NULL,
    project_numbers text[],
    order_creation_date timestamp(0) without time zone,
    provider_id integer,
    order_state integer,
    order_status integer,
    user_id integer,
    order_delivery_date timestamp(0) without time zone DEFAULT now(),
    pc_id integer,
    offer_number text
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: orders_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_order_id_seq OWNER TO postgres;

--
-- Name: orders_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_order_id_seq OWNED BY public.orders.order_id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.projects (
    project_id integer NOT NULL,
    company_id text NOT NULL,
    project_name text NOT NULL,
    project_number text NOT NULL,
    project_state integer,
    project_type text,
    project_client text,
    project_creation_date timestamp(0) without time zone DEFAULT now(),
    project_deadline timestamp(0) without time zone,
    user_id integer
);


ALTER TABLE public.projects OWNER TO postgres;

--
-- Name: projects_project_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.projects_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_project_id_seq OWNER TO postgres;

--
-- Name: projects_project_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.projects_project_id_seq OWNED BY public.projects.project_id;


--
-- Name: provider_contacts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.provider_contacts (
    pc_id integer NOT NULL,
    provider_id integer NOT NULL,
    contact_name character varying(150),
    contact_last_name character varying(150),
    contact_telephone character varying(150),
    contact_email character varying(150)
);


ALTER TABLE public.provider_contacts OWNER TO postgres;

--
-- Name: provider_contacts_pc_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.provider_contacts_pc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.provider_contacts_pc_id_seq OWNER TO postgres;

--
-- Name: provider_contacts_pc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.provider_contacts_pc_id_seq OWNED BY public.provider_contacts.pc_id;


--
-- Name: providers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.providers (
    provider_id integer NOT NULL,
    company_id character varying NOT NULL,
    provider_name character varying NOT NULL,
    provider_address character varying,
    provider_telephone character varying
);


ALTER TABLE public.providers OWNER TO postgres;

--
-- Name: providers_provider_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.providers_provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.providers_provider_id_seq OWNER TO postgres;

--
-- Name: providers_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.providers_provider_id_seq OWNED BY public.providers.provider_id;


--
-- Name: states; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.states (
    state_id integer NOT NULL,
    company_id text NOT NULL,
    state_name text NOT NULL,
    state_description text
);


ALTER TABLE public.states OWNER TO postgres;

--
-- Name: states_state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.states_state_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.states_state_id_seq OWNER TO postgres;

--
-- Name: states_state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.states_state_id_seq OWNED BY public.states.state_id;


--
-- Name: sub_assemblies_in_assemblies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sub_assemblies_in_assemblies (
    saia_id integer NOT NULL,
    sub_assembly_id integer NOT NULL,
    assembly_id integer NOT NULL
);


ALTER TABLE public.sub_assemblies_in_assemblies OWNER TO postgres;

--
-- Name: sub_assemblies_in_assemblies_saia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sub_assemblies_in_assemblies_saia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sub_assemblies_in_assemblies_saia_id_seq OWNER TO postgres;

--
-- Name: sub_assemblies_in_assemblies_saia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sub_assemblies_in_assemblies_saia_id_seq OWNED BY public.sub_assemblies_in_assemblies.saia_id;


--
-- Name: sub_assemblies_in_projects; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sub_assemblies_in_projects (
    saip_id integer NOT NULL,
    project_id integer NOT NULL,
    sub_assembly_id integer NOT NULL,
    assembly_id integer NOT NULL,
    aip_id integer,
    state_id integer DEFAULT 1,
    state_date timestamp(0) without time zone DEFAULT now(),
    user_id integer
);


ALTER TABLE public.sub_assemblies_in_projects OWNER TO postgres;

--
-- Name: sub_assemblies_in_projects_saip_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sub_assemblies_in_projects_saip_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sub_assemblies_in_projects_saip_id_seq OWNER TO postgres;

--
-- Name: sub_assemblies_in_projects_saip_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sub_assemblies_in_projects_saip_id_seq OWNED BY public.sub_assemblies_in_projects.saip_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    user_id integer NOT NULL,
    company_id text NOT NULL,
    user_name text NOT NULL,
    user_last_name text NOT NULL,
    user_email text NOT NULL,
    user_selected_language text,
    user_creation_date timestamp(0) without time zone DEFAULT now(),
    user_active boolean DEFAULT true,
    salt text NOT NULL,
    hash text NOT NULL,
    user_telephone text
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;


--
-- Name: assemblies assembly_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies ALTER COLUMN assembly_id SET DEFAULT nextval('public.assemblies_assembly_id_seq'::regclass);


--
-- Name: assemblies_in_projects aip_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies_in_projects ALTER COLUMN aip_id SET DEFAULT nextval('public.assemblies_in_projects_aip_id_seq'::regclass);


--
-- Name: client_contacts cc_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_contacts ALTER COLUMN cc_id SET DEFAULT nextval('public.client_contacts_cc_id_seq'::regclass);


--
-- Name: clients client_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients ALTER COLUMN client_id SET DEFAULT nextval('public.clients_client_id_seq'::regclass);


--
-- Name: columns_names column_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.columns_names ALTER COLUMN column_id SET DEFAULT nextval('public.columns_names_column_id_seq'::regclass);


--
-- Name: companies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies ALTER COLUMN id SET DEFAULT nextval('public.companies_id_seq'::regclass);


--
-- Name: customized_columns_names ccn_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customized_columns_names ALTER COLUMN ccn_id SET DEFAULT nextval('public.customized_columns_names_ccn_id_seq'::regclass);


--
-- Name: filter_groups filter_group_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_groups ALTER COLUMN filter_group_id SET DEFAULT nextval('public.filter_groups_filter_group_id_seq'::regclass);


--
-- Name: filter_values filter_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_values ALTER COLUMN filter_id SET DEFAULT nextval('public.filter_values_filter_id_seq'::regclass);


--
-- Name: items item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items ALTER COLUMN item_id SET DEFAULT nextval('public.items_item_id_seq'::regclass);


--
-- Name: items_in_assemblies iia_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_assemblies ALTER COLUMN iia_id SET DEFAULT nextval('public.items_in_assemblies_iia_id_seq'::regclass);


--
-- Name: items_in_offers iioffer_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_offers ALTER COLUMN iioffer_id SET DEFAULT nextval('public.items_in_offers_iioffer_id_seq'::regclass);


--
-- Name: items_in_orders iio_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_orders ALTER COLUMN iio_id SET DEFAULT nextval('public.items_in_orders_iio_id_seq'::regclass);


--
-- Name: items_in_projects iip_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects ALTER COLUMN iip_id SET DEFAULT nextval('public.items_in_projects_iip_id_seq'::regclass);


--
-- Name: items_in_projects_states iips_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects_states ALTER COLUMN iips_id SET DEFAULT nextval('public.items_in_projects_states_iips_id_seq'::regclass);


--
-- Name: items_in_stock iis_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_stock ALTER COLUMN iis_id SET DEFAULT nextval('public.items_in_stock_iis_id_seq'::regclass);


--
-- Name: items_processes_times ipt_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times ALTER COLUMN ipt_id SET DEFAULT nextval('public.items_processes_times_ipt_id_seq'::regclass);


--
-- Name: items_with_filters iwf_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_with_filters ALTER COLUMN iwf_id SET DEFAULT nextval('public.items_with_filters_iwf_id_seq'::regclass);


--
-- Name: offers offer_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers ALTER COLUMN offer_id SET DEFAULT nextval('public.offers_offer_id_seq'::regclass);


--
-- Name: orders order_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN order_id SET DEFAULT nextval('public.orders_order_id_seq'::regclass);


--
-- Name: projects project_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects ALTER COLUMN project_id SET DEFAULT nextval('public.projects_project_id_seq'::regclass);


--
-- Name: provider_contacts pc_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_contacts ALTER COLUMN pc_id SET DEFAULT nextval('public.provider_contacts_pc_id_seq'::regclass);


--
-- Name: providers provider_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.providers ALTER COLUMN provider_id SET DEFAULT nextval('public.providers_provider_id_seq'::regclass);


--
-- Name: states state_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states ALTER COLUMN state_id SET DEFAULT nextval('public.states_state_id_seq'::regclass);


--
-- Name: sub_assemblies_in_assemblies saia_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_assemblies ALTER COLUMN saia_id SET DEFAULT nextval('public.sub_assemblies_in_assemblies_saia_id_seq'::regclass);


--
-- Name: sub_assemblies_in_projects saip_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects ALTER COLUMN saip_id SET DEFAULT nextval('public.sub_assemblies_in_projects_saip_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Data for Name: assemblies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assemblies (assembly_id, company_id, assembly_name, assembly_number, assembly_type, assembly_creation_date) FROM stdin;
1	LaBitacora	FREE PIECES	000000	1	2018-02-25 08:53:32
2	3UGMSKKJBY	FIRST ASSEMBLY	FS01111	1	2018-02-25 09:01:03
3	3UGMSKKJBY	SECOND ASSEMBLY	SA022222	1	2018-02-25 09:01:33
4	3UGMSKKJBY	PRIMER SUBASSEMBLY	PSA0111	2	2018-02-25 09:03:58
5	3UGMSKKJBY	ZWEITE BAUGRUPPE	ZWB02222	2	2018-02-25 09:05:02
6	3UGMSKKJBY	FGHDHDSHSDHJ	23456	2	2018-02-25 13:45:13
21	3UGMSKKJBY	cinco	5555555555555	1	2018-04-30 09:15:24
22	3UGMSKKJBY	numero 6	666666	1	2018-04-30 20:09:23
23	3UGMSKKJBY	sub test uno	sss1111	2	2018-05-01 16:48:26
24	3UGMSKKJBY	Group test 111	GT111	1	2018-05-01 16:49:06
25	3UGMSKKJBY	miercoles	mi123456	1	2018-05-02 19:15:20
26	3UGMSKKJBY	NUEvo test uno	NT123456	1	2018-05-25 18:57:36
\.


--
-- Data for Name: assemblies_in_projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assemblies_in_projects (aip_id, project_id, assembly_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	2	1
5	3	1
6	4	1
7	5	1
8	6	1
9	7	1
10	8	1
11	9	1
12	10	1
13	11	1
14	12	1
27	2	21
28	2	22
29	3	3
30	3	21
31	4	25
32	6	2
33	6	3
34	6	21
35	6	22
36	6	24
37	6	25
38	13	1
39	14	1
\.


--
-- Data for Name: client_contacts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client_contacts (cc_id, client_id, contact_name, contact_last_name, contact_telephone, contact_email) FROM stdin;
1	2	uno	dos	111112222222222	uno@dos.com
2	2	dos	tres	22222222333333333	dos@tres.com
4	2	cinco	seis	55556666	cinco@seis.com
5	1	test one	test two	1212121	tre@qwer.com
6	2	Marcus	Rashford	333344444	tres@cuantro.com
7	2	Antony	Martial	333344444	tres@cuantro.com
8	2	Cristano	Ronaldo	333344444	tres@cuantro.com
3	2	Federico 	Macheda	333344444	tres@cuantro.com
9	3	Rio	Brasil	33333333333	rio@brasil.com
\.


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clients (client_id, company_id, client_name, client_address, client_number) FROM stdin;
1	3UGMSKKJBY	ICE MAN GMBH	La puta mierda	+48789452
2	3UGMSKKJBY	TU PERRA MADRE GMBH	La puta mierda	+48789452
3	3UGMSKKJBY	Inix	Mexico	111111111
4	3UGMSKKJBY	manchester	united	12345678
5	3UGMSKKJBY	Malta	Joda	Loco
\.


--
-- Data for Name: columns_names; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.columns_names (column_id, column_name, column_table) FROM stdin;
1	item_code	items
2	item_name	items
3	item_raw_material_dimensions	items
5	item_barcode	items
4	item_amount	items
6	total_assembled	items
7	total_booked	items
8	neto	items
9	total_ordered	items
10	item_order_number	items
11	item_delivered	items_in_orders
12	item_code	items_in_orders
13	item_order_number	items_in_orders
14	item_name	items_in_orders
15	item_amount	items_in_orders
16	received_amount	items_in_orders
17	item_buy_price	items_in_orders
18	item_total_price	items_in_orders
\.


--
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.companies (id, company_id, company_tax_payer_id, company_name, company_address, company_zip_code, company_country, company_state, company_creation_date, company_url_logo, company_url_image_brand, company_images, company_website, company_city, company_telephone, company_fax, company_email) FROM stdin;
1	LaBitacora	LaBitacora	BITACORA	NO LOCATION	23701	\N	1	2018-02-25 08:53:31	NONE	NONE	\N	\N	\N	\N	0049 4521 779977	info@rohde-maschinenbau.de
2	3UGMSKKJBY	de1234567890	Rohde Maschinenbau Gmbh	mk strasse 31	23701	Alemania	1	2018-02-25 07:55:04	NONE	/db/images/1519545303793/images.png	1519545303793	http://www.rohde-maschinenbau.de	eutin	0045679123	0049 4521 779977	info@rohde-maschinenbau.de
\.


--
-- Data for Name: customized_columns_names; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customized_columns_names (ccn_id, company_id, column_id, customized_name) FROM stdin;
2	3UGMSKKJBY	2	Bennenung
4	3UGMSKKJBY	5	QR_CODE
5	3UGMSKKJBY	3	Row Material
1	3UGMSKKJBY	1	Artlikelnummer
3	3UGMSKKJBY	4	Menge
6	3UGMSKKJBY	6	Montiert
7	3UGMSKKJBY	7	Gebucht
8	3UGMSKKJBY	8	Netto
9	3UGMSKKJBY	9	Bestellt
11	3UGMSKKJBY	10	Bestelltungsnummer
12	3UGMSKKJBY	11	Geliefert
14	3UGMSKKJBY	13	Bestellungsnummer
19	3UGMSKKJBY	18	Total
17	3UGMSKKJBY	16	Erhalten
16	3UGMSKKJBY	15	Menge
15	3UGMSKKJBY	14	Gallo 2
18	3UGMSKKJBY	17	fd
13	3UGMSKKJBY	12	Nuevo
\.


--
-- Data for Name: filter_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filter_groups (filter_group_id, company_id, filter_group_name) FROM stdin;
1	LaBitacora	MATERIAL
2	LaBitacora	PROVIDER
3	LaBitacora	TYPE
4	3UGMSKKJBY	OBERFLÄCHE
\.


--
-- Data for Name: filter_values; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.filter_values (filter_id, filter_group_id, company_id, filter_value) FROM stdin;
3	2	3UGMSKKJBY	juan
5	2	3UGMSKKJBY	AMM
6	2	3UGMSKKJBY	ABB
7	2	3UGMSKKJBY	SMC
8	4	3UGMSKKJBY	pintado
9	4	3UGMSKKJBY	cromado
10	4	3UGMSKKJBY	zincado
\.


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items (item_id, company_id, item_code, item_name, item_raw_material_dimensions, item_creation_date, item_order_number, item_barcode) FROM stdin;
3	3UGMSKKJBY	22.403.00.00.02-B	ECKENVERKLEIDUNG-8-FACH DURCHFLUSS	NONE	2018-02-25 07:59:57	\N	\N
4	3UGMSKKJBY	22.403.00.00.04-A	ECKE-FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
5	3UGMSKKJBY	24.400.00.00.12	VOR-UND RÜCKLAUFHALTER	NONE	2018-02-25 07:59:57	\N	\N
6	3UGMSKKJBY	22.400.00.00.27-C	HINTERES SEITENBLECH-AUSLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
7	3UGMSKKJBY	22.403.01.01.01	STREBE FORMSTATIONTÜR-UNTEN	NONE	2018-02-25 07:59:57	\N	\N
8	3UGMSKKJBY	22.403.01.01.02	STREBE FORMSTATIONTÜR-OBEN	NONE	2018-02-25 07:59:57	\N	\N
9	3UGMSKKJBY	KK4S-10E	S-KUPPLUNG BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
10	3UGMSKKJBY	22.403.00.00.20	EXCENTER FÜR TÜR	NONE	2018-02-25 07:59:57	\N	\N
11	3UGMSKKJBY	22.403.01.02.02-A	STREBE TÜR-OBEN	NONE	2018-02-25 07:59:57	\N	\N
12	3UGMSKKJBY	22.403.00.00.21	UNTERLEGSCHEIBE-TÜR	NONE	2018-02-25 07:59:57	\N	\N
13	3UGMSKKJBY	22.403.02.00.02	ANSCHLAG, FLÜGELTÜR HEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
14	3UGMSKKJBY	22.403.01.02.01-A	STREBE TÜR, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
15	3UGMSKKJBY	218-9103	SEITE	NONE	2018-02-25 07:59:57	\N	\N
16	3UGMSKKJBY	22.400.00.00.10-C	KLAPPE, AUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
17	3UGMSKKJBY	22.400.00.00.09	RAHMEN, EINLAUF/AUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
18	3UGMSKKJBY	22.400.00.00.29	SCHEIBE EINLAUF/AUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
19	3UGMSKKJBY	GN 666.1-30-500-EL	ROHRGRIFF-500MM	NONE	2018-02-25 07:59:57	\N	\N
20	3UGMSKKJBY	22.403.00.00.06-B	RAHMEN, EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
21	3UGMSKKJBY	22.403.00.00.18-A	RAHMEN 1880X470X1	NONE	2018-02-25 07:59:57	\N	\N
22	3UGMSKKJBY	22.403.00.00.15-C	VERKLEIDUNGSBLECH, EINLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
23	3UGMSKKJBY	22.403.00.00.13-B	VERKLEIDUNGSBLECH EINLAUF, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
24	3UGMSKKJBY	22.403.02.00.03	HALTELASCHE UNTEN, WARTUNGSTÜR	NONE	2018-02-25 07:59:57	\N	\N
25	3UGMSKKJBY	22.403.02.00.04	BEFESTIGUNGSWINKEL,	NONE	2018-02-25 07:59:57	\N	\N
26	3UGMSKKJBY	22.403.00.00.14	WARTUNGSKLAPPE EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
27	3UGMSKKJBY	22.403.02.00.05	HALTELASCHE OBEN, WARTUNGSTÜR	NONE	2018-02-25 07:59:57	\N	\N
28	3UGMSKKJBY	22.403.00.00.05-B	RAHMEN, BEDIENPULT	NONE	2018-02-25 07:59:57	\N	\N
29	3UGMSKKJBY	22.403.00.00.12	VERKLEIDUNGSBLECH 1	NONE	2018-02-25 07:59:57	\N	\N
30	3UGMSKKJBY	22.403.00.00.29	SCHEIBE, BEDIENPULT	NONE	2018-02-25 07:59:57	\N	\N
31	3UGMSKKJBY	HLS	HALTER LICHTSCHRANKE	NONE	2018-02-25 07:59:57	\N	\N
32	3UGMSKKJBY	Z81-9-R1_8	ANSCHLUSSNIPPEL MIT FREIEM	NONE	2018-02-25 07:59:57	\N	\N
33	3UGMSKKJBY	AR20-F02H-A	DRUCKREGLER	NONE	2018-02-25 07:59:57	\N	\N
34	3UGMSKKJBY	AR40-F04H-A	DRUCKREGLER	NONE	2018-02-25 07:59:57	\N	\N
35	3UGMSKKJBY	KP8-10-50	PANELMANOMETER	NONE	2018-02-25 07:59:57	\N	\N
36	3UGMSKKJBY	KP8-10-40	PANELMANOMETER	NONE	2018-02-25 07:59:57	\N	\N
37	3UGMSKKJBY	KQ2LF06-01A	WINKEL-STECKVERSCHRAUBUNG	NONE	2018-02-25 07:59:57	\N	\N
38	3UGMSKKJBY	KQ2R06-10A	STECKVERBINDUNG, REDUZIERT	NONE	2018-02-25 07:59:57	\N	\N
39	3UGMSKKJBY	22.400.00.08.03	HALTELEISTE, FOLIENAUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
40	3UGMSKKJBY	22.400.00.00.34-A	OBERES FÜHRUNGSBLECH, AUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
41	3UGMSKKJBY	22.400.00.08.01	BLECH, FOLIENAUSLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
43	3UGMSKKJBY	7350024479287	ABB-MAGNE 2B-EVA	NONE	2018-02-25 07:59:57	\N	\N
44	3UGMSKKJBY	7350024452013	2TLA020046R0000-EVA	NONE	2018-02-25 07:59:57	\N	\N
45	3UGMSKKJBY	AW40-F04G-A	FILTERREGLER	NONE	2018-02-25 07:59:57	\N	\N
46	3UGMSKKJBY	22.403.02.00.08	HALTER,MAGNE 2B, BEDIENSEITE	NONE	2018-02-25 07:59:57	\N	\N
47	3UGMSKKJBY	Y400-A	ZWISCHENSTÜCK	NONE	2018-02-25 07:59:57	\N	\N
48	3UGMSKKJBY	Y400T-A	ZWISCHENSTÜCK MIT	NONE	2018-02-25 07:59:57	\N	\N
49	3UGMSKKJBY	VHS40-F04A-S	3-2-WEGE HANDABSPERRVENTIL	NONE	2018-02-25 07:59:57	\N	\N
51	3UGMSKKJBY	AN40-04	SCHALLDÄMPFER	NONE	2018-02-25 07:59:57	\N	\N
52	3UGMSKKJBY	EAV4000-F04-5YZ-Q	SOFT-STARTVENTIL	NONE	2018-02-25 07:59:57	\N	\N
53	3UGMSKKJBY	22.403.02.00.09	HALTER, MAGNE 2B, HEIZUNGSSEITE	NONE	2018-02-25 07:59:57	\N	\N
54	3UGMSKKJBY	22.403.02.00.10	DISTANZBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
55	3UGMSKKJBY	OGP280	REFLEKTORLICHTSCHRANKE	NONE	2018-02-25 07:59:57	\N	\N
56	3UGMSKKJBY	GN 6336.4-ST-32-M6-60	STERNGRIFFSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
57	3UGMSKKJBY	22.403.01.01.03	TÜR FORMSTATION BEDIEN-SEITE	NONE	2018-02-25 07:59:57	\N	\N
58	3UGMSKKJBY	DIN 912 M8 X 16	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
59	3UGMSKKJBY	DIN 912 M6 X 12	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
60	3UGMSKKJBY	DIN 912 M4 X 16	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
61	3UGMSKKJBY	22.403.00.03.05	FENSTERSCHEIBE, TÜR	NONE	2018-02-25 07:59:57	\N	\N
62	3UGMSKKJBY	00.105.13.07.06-A	TÜRBLECH	NONE	2018-02-25 07:59:57	\N	\N
63	3UGMSKKJBY	22.403.01.02.03	TÜR FORMSTATION HEIZUNG SEITE	NONE	2018-02-25 07:59:57	\N	\N
64	3UGMSKKJBY	24.403.00.00.35	GEWINDESTANGE,VERKLEIDUNG	NONE	2018-02-25 07:59:57	\N	\N
65	3UGMSKKJBY	22.403.01.03.01	TÜR HEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
66	3UGMSKKJBY	22.403.00.00.34	VERKLEIDUNG FSSE	NONE	2018-02-25 07:59:57	\N	\N
67	3UGMSKKJBY	00.110.00.00.02-A	RÜCKWAND	NONE	2018-02-25 07:59:57	\N	\N
68	3UGMSKKJBY	00.110.00.00.05	MOTORPLATTE	NONE	2018-02-25 07:59:57	\N	\N
69	3UGMSKKJBY	00.110.00.00.01	HEIZWALZE	NONE	2018-02-25 07:59:57	\N	\N
70	3UGMSKKJBY	00.110.00.00.08	ABTRIEBSRAD	NONE	2018-02-25 07:59:57	\N	\N
71	3UGMSKKJBY	00.110.00.00.20	DISTANZRING	NONE	2018-02-25 07:59:57	\N	\N
72	3UGMSKKJBY	00.110.00.00.09	ANTRIEBSKETTENRAD	NONE	2018-02-25 07:59:57	\N	\N
73	3UGMSKKJBY	00.110.00.00.21	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
74	3UGMSKKJBY	00.110.00.00.22	TRÄGERLEISTE SCHALTSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
75	3UGMSKKJBY	00.110.00.00.23	VERSCHLUSSWINKEL GEHﾎUSE	NONE	2018-02-25 07:59:57	\N	\N
76	3UGMSKKJBY	00.110.00.00.24	HALTERUNG LICHTSCHRANKEN	NONE	2018-02-25 07:59:57	\N	\N
77	3UGMSKKJBY	00.110.00.00.25	UNTERLEGPLATTE ZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
78	3UGMSKKJBY	00.110.00.01.00	ABSTREIFBÜGEL	NONE	2018-02-25 07:59:57	\N	\N
79	3UGMSKKJBY	00.110.01.00.01	FLANSCHPLATTE	NONE	2018-02-25 07:59:57	\N	\N
80	3UGMSKKJBY	00.110.01.00.02	FLANSCHPLATTE VORNE	NONE	2018-02-25 07:59:57	\N	\N
81	3UGMSKKJBY	00.110.01.00.03	FLANSCH	NONE	2018-02-25 07:59:57	\N	\N
82	3UGMSKKJBY	00.110.01.00.04	FLANSCH VORNE	NONE	2018-02-25 07:59:57	\N	\N
83	3UGMSKKJBY	00.110.01.00.05	ROHR	NONE	2018-02-25 07:59:57	\N	\N
84	3UGMSKKJBY	00.110.01.00.06	LAGERSITZ	NONE	2018-02-25 07:59:57	\N	\N
85	3UGMSKKJBY	00.110.01.00.07	FLANSCHPLATTE	NONE	2018-02-25 07:59:57	\N	\N
86	3UGMSKKJBY	00.110.01.00.08	DISTANZBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
87	3UGMSKKJBY	00.110.01.00.09	BEFESTIGUNGSFLANSCH FÜR	NONE	2018-02-25 07:59:57	\N	\N
88	3UGMSKKJBY	00.110.01.00.10	BEFESTIGUNG BÜRSTENHALTER	NONE	2018-02-25 07:59:57	\N	\N
89	3UGMSKKJBY	00.110.01.00.11	ANSCHLU-BLOCK	NONE	2018-02-25 07:59:57	\N	\N
90	3UGMSKKJBY	00.110.01.00.12	ABDECKPLATTE	NONE	2018-02-25 07:59:57	\N	\N
91	3UGMSKKJBY	00.110.01.00.14	STROMSCHIENE	NONE	2018-02-25 07:59:57	\N	\N
92	3UGMSKKJBY	00.110.01.00.15	BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
93	3UGMSKKJBY	00.110.06.00.01	ARM FÜR SCHWINGE	NONE	2018-02-25 07:59:57	\N	\N
94	3UGMSKKJBY	00.110.01.00.16	SCHUTZSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
95	3UGMSKKJBY	00.110.06.00.02	HALTEWINKEL, SCHWINGE VORNE	NONE	2018-02-25 07:59:57	\N	\N
96	3UGMSKKJBY	00.110.06.00.03	HALTEWINKEL, SCHWINGE HINTEN	NONE	2018-02-25 07:59:57	\N	\N
97	3UGMSKKJBY	00.110.06.00.04	ROLLE, 600MM	NONE	2018-02-25 07:59:57	\N	\N
98	3UGMSKKJBY	00.110.06.00.05	ANLAGEROLLE	NONE	2018-02-25 07:59:57	\N	\N
99	3UGMSKKJBY	00.110.06.00.06	GEWINDESTANGE M12, LﾎNGE:	NONE	2018-02-25 07:59:57	\N	\N
100	3UGMSKKJBY	00.110.07.00.02-A	BEGRENZUNGSROLLE	NONE	2018-02-25 07:59:57	\N	\N
101	3UGMSKKJBY	00.110.07.00.03	ROLLE	NONE	2018-02-25 07:59:57	\N	\N
102	3UGMSKKJBY	00.110.07.00.01	WINKEL, FOLIENEINLAUF	NONE	2018-02-25 07:59:57	\N	\N
103	3UGMSKKJBY	00.110.07.00.04	POM-STOPFEN	NONE	2018-02-25 07:59:57	\N	\N
104	3UGMSKKJBY	00.110.08.00.01	WINKEL, AUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
105	3UGMSKKJBY	00.110.08.00.02	ROLLE	NONE	2018-02-25 07:59:57	\N	\N
106	3UGMSKKJBY	00.110.09.00.01	HALTEWINKEL	NONE	2018-02-25 07:59:57	\N	\N
107	3UGMSKKJBY	00.110.09.00.02	HALTEWINKEL	NONE	2018-02-25 07:59:57	\N	\N
108	3UGMSKKJBY	00.110.09.00.03	STANGE	NONE	2018-02-25 07:59:57	\N	\N
109	3UGMSKKJBY	00.110.09.00.04	STREBE	NONE	2018-02-25 07:59:57	\N	\N
110	3UGMSKKJBY	00.110.09.00.05	GEWINDESTANGE	NONE	2018-02-25 07:59:57	\N	\N
111	3UGMSKKJBY	00.110.09.00.06	HALTERUNG	NONE	2018-02-25 07:59:57	\N	\N
112	3UGMSKKJBY	00.110.09.00.07	STANGE HALTERUNG	NONE	2018-02-25 07:59:57	\N	\N
113	3UGMSKKJBY	00.110.09.00.08	GEWINDESTANGE M12, LﾎNGE =	NONE	2018-02-25 07:59:57	\N	\N
114	3UGMSKKJBY	00.110.09.00.09	ROLLE	NONE	2018-02-25 07:59:57	\N	\N
115	3UGMSKKJBY	00.110.09.00.10	ROLLENHALTERUNG	NONE	2018-02-25 07:59:57	\N	\N
116	3UGMSKKJBY	00.110.09.00.11	HALTERUNG STANGE	NONE	2018-02-25 07:59:57	\N	\N
117	3UGMSKKJBY	00.110.09.00.12	HALTESTANGE ZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
118	3UGMSKKJBY	00.110.10.01.01	KLAPPE, UNTERTEIL	NONE	2018-02-25 07:59:57	\N	\N
119	3UGMSKKJBY	00.110.09.00.13	VERLﾎNGERUNGSSTAB ZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
120	3UGMSKKJBY	00.110.10.01.03	DECKBLECH, KLAPPE	NONE	2018-02-25 07:59:57	\N	\N
121	3UGMSKKJBY	00.110.10.01.04	HALTEWINKEL WﾎRMETUNNEL	NONE	2018-02-25 07:59:57	\N	\N
122	3UGMSKKJBY	00.110.10.01.05	STABILISIERUNGSWINKEL	NONE	2018-02-25 07:59:57	\N	\N
123	3UGMSKKJBY	DIN 125-A 17	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
124	3UGMSKKJBY	DIN 705-A 30	STELLRING	NONE	2018-02-25 07:59:57	\N	\N
125	3UGMSKKJBY	DIN 912 M4 X 6 -	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
126	3UGMSKKJBY	00.110.10.01.02	DECKBLECH, VORNE	NONE	2018-02-25 07:59:57	\N	\N
127	3UGMSKKJBY	DIN 914 - M12 X 25	GEWINDESTIFT MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
128	3UGMSKKJBY	DIN 914 - M16 X 25	GEWINDESTIFT MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
129	3UGMSKKJBY	DIN EN ISO 7042 -M6	SICHERUNGSMUTTER	NONE	2018-02-25 07:59:57	\N	\N
130	3UGMSKKJBY	DIN 7984 - M10 X 30	ZYLINDERSCHRAUBE, NIEDRIGER KOPF	NONE	2018-02-25 07:59:57	\N	\N
131	3UGMSKKJBY	DIN 7991 - M4 X 40	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
132	3UGMSKKJBY	DIN 7991 - M6 X 16	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
133	3UGMSKKJBY	DIN 7991 - M6 X 20	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
134	3UGMSKKJBY	O-RING 30 X 2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
135	3UGMSKKJBY	O-RING 42 X 5 - VITON	O-RING	NONE	2018-02-25 07:59:57	\N	\N
136	3UGMSKKJBY	12B-1 - 44 GLIEDER	ROLLENKETTE	NONE	2018-02-25 07:59:57	\N	\N
137	3UGMSKKJBY	KK30X30	KABELKANAL	NONE	2018-02-25 07:59:57	\N	\N
138	3UGMSKKJBY	KABELSCHUH	KABELSCHUH	NONE	2018-02-25 07:59:57	\N	\N
139	3UGMSKKJBY	3017762	CONDUCTIX WAMPLER,	NONE	2018-02-25 07:59:57	\N	\N
140	3UGMSKKJBY	126908-HLP-S-12.5	HEIZPATRONE-800W-400V G250	NONE	2018-02-25 07:59:57	\N	\N
141	3UGMSKKJBY	T-28-511	5-2-WEGE SCHIEBERVENTIL	NONE	2018-02-25 07:59:57	\N	\N
142	3UGMSKKJBY	18-T-23-013	DREHSCHALTER	NONE	2018-02-25 07:59:57	\N	\N
143	3UGMSKKJBY	CM 5111.500	KOMPAKT SYSTEMSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
144	3UGMSKKJBY	OJ5191	REFLEXLICHTSCHRANKE	NONE	2018-02-25 07:59:57	\N	\N
145	3UGMSKKJBY	3212.23	KÜHLGERÄT	NONE	2018-02-25 07:59:57	\N	\N
146	3UGMSKKJBY	S37DRS71S4	SCHNECKENGETRIEBEMOTOR	NONE	2018-02-25 07:59:57	\N	\N
147	3UGMSKKJBY	CP96SB32-50	NORMZYLINDER-ISO 15552	NONE	2018-02-25 07:59:57	\N	\N
148	3UGMSKKJBY	INNENRAUM	WﾎRMESCHRANK	NONE	2018-02-25 07:59:57	\N	\N
149	3UGMSKKJBY	25.401.00.00.01	ACHSE, 910MM	NONE	2018-02-25 07:59:57	\N	\N
150	3UGMSKKJBY	25.401.00.00.04	WINKEL	NONE	2018-02-25 07:59:57	\N	\N
151	3UGMSKKJBY	25.401.00.00.07	ROLLE, 1160MM	NONE	2018-02-25 07:59:57	\N	\N
152	3UGMSKKJBY	25.401.00.00.05	WINKEL, EINSTELLBAR	NONE	2018-02-25 07:59:57	\N	\N
153	3UGMSKKJBY	25.401.00.00.06	ACHSE, 1183MM	NONE	2018-02-25 07:59:57	\N	\N
154	3UGMSKKJBY	AE 1090.500	KOMPAKT SYSTEMSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
155	3UGMSKKJBY	25.401.00.01.00	STREBE MASCHINENRAHMEN, KURZ	NONE	2018-02-25 07:59:57	\N	\N
156	3UGMSKKJBY	25.401.00.00.02	ROLLE, 600MM	NONE	2018-02-25 07:59:57	\N	\N
157	3UGMSKKJBY	25.401.00.02.00	STREBE MASCHINENRAHMEN, LANG	NONE	2018-02-25 07:59:57	\N	\N
158	3UGMSKKJBY	25.401.00.00.03	LAGERSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
159	3UGMSKKJBY	25.401.00.03.01	STREBE, VERKLEIDUNG	NONE	2018-02-25 07:59:57	\N	\N
160	3UGMSKKJBY	25.401.00.03.02	DISTANZBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
161	3UGMSKKJBY	25.401.00.03.03	ACHSE-D20MMX640MM	NONE	2018-02-25 07:59:57	\N	\N
162	3UGMSKKJBY	23.400.00.00.08	MUTTER	NONE	2018-02-25 07:59:57	\N	\N
163	3UGMSKKJBY	23.400.00.00.09	BRÜCKE FÜR SPINDELLAGER	NONE	2018-02-25 07:59:57	\N	\N
164	3UGMSKKJBY	23.400.00.00.12-A	SPINDEL	NONE	2018-02-25 07:59:57	\N	\N
165	3UGMSKKJBY	23.400.00.00.10-A	LAGERSCHILD, BRÜCKE	NONE	2018-02-25 07:59:57	\N	\N
166	3UGMSKKJBY	23.400.00.00.29	TRÄGERLEISTE ABDECKHAUBE	NONE	2018-02-25 07:59:57	\N	\N
167	3UGMSKKJBY	23.400.00.00.30	SCHUTZBLENDE	NONE	2018-02-25 07:59:57	\N	\N
168	3UGMSKKJBY	23.400.00.00.26	ABDECKHAUBE, STAPELUNG	NONE	2018-02-25 07:59:57	\N	\N
169	3UGMSKKJBY	23.400.00.01.01	GRUNDPLATTE	NONE	2018-02-25 07:59:57	\N	\N
170	3UGMSKKJBY	DIN 705-A 16	STELLRING	NONE	2018-02-25 07:59:57	\N	\N
171	3UGMSKKJBY	O-RING 90 X 5 - VITON	O-RING	NONE	2018-02-25 07:59:57	\N	\N
172	3UGMSKKJBY	GN 237-ZD-50-50-A	SCHARNIER SCHWARZ	NONE	2018-02-25 07:59:57	\N	\N
173	3UGMSKKJBY	23.400.00.01.04-A	FÜHRUNGSKLOTZ	NONE	2018-02-25 07:59:57	\N	\N
174	3UGMSKKJBY	23.400.00.01.02-A	"ZYLINDERBEFESTIGUNG	NONE	2018-02-25 07:59:57	\N	\N
175	3UGMSKKJBY	23.400.00.00.27	VERSTEIFUNG FÜR HAUBE	NONE	2018-02-25 07:59:57	\N	\N
176	3UGMSKKJBY	23.400.00.01.06	BEFESTIGUNGSLEISTE	NONE	2018-02-25 07:59:57	\N	\N
177	3UGMSKKJBY	23.400.00.01.05-A	FÜHRUNGSBLOCK	NONE	2018-02-25 07:59:57	\N	\N
178	3UGMSKKJBY	23.400.00.01.07	ZENTRIERUNG FÜR STAPELVORRICHTUNG	NONE	2018-02-25 07:59:57	\N	\N
179	3UGMSKKJBY	23.400.00.03.01	ABSTÜTZUNG HINTEN	NONE	2018-02-25 07:59:57	\N	\N
180	3UGMSKKJBY	23.400.00.03.02	LEISTE	NONE	2018-02-25 07:59:57	\N	\N
181	3UGMSKKJBY	23.400.00.03.03	ZYLINDERWINKEL	NONE	2018-02-25 07:59:57	\N	\N
182	3UGMSKKJBY	23.401.00.00.01	BEFESTIGUNGSWINKEL FÜR SPANNER	NONE	2018-02-25 07:59:57	\N	\N
183	3UGMSKKJBY	23.405.00.00.01	FÜHRUNGSHALTER, VORNE	NONE	2018-02-25 07:59:57	\N	\N
184	3UGMSKKJBY	23.405.00.00.04	ZENTRIERDORN	NONE	2018-02-25 07:59:57	\N	\N
185	3UGMSKKJBY	23.400.00.03.04	DISTANZBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
186	3UGMSKKJBY	23.405.00.00.07	PRÜFSTÜCK, HANDKURBEL	NONE	2018-02-25 07:59:57	\N	\N
187	3UGMSKKJBY	23.400.00.03.05	DISTANZHÜLSE	NONE	2018-02-25 07:59:57	\N	\N
188	3UGMSKKJBY	23.405.00.00.03	FÜHRUNGSSTANGE	NONE	2018-02-25 07:59:57	\N	\N
189	3UGMSKKJBY	23.405.00.00.02	FÜHRUNGSHALTER, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
190	3UGMSKKJBY	23.405.00.00.05	HALTERBEFESTIGUNG	NONE	2018-02-25 07:59:57	\N	\N
191	3UGMSKKJBY	23.401.00.00.02	HALTERUNG FÜR STAPELWAGENANBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
192	3UGMSKKJBY	23.405.01.01.00	GESTELL, STAPELWAGEN	NONE	2018-02-25 07:59:57	\N	\N
193	3UGMSKKJBY	19.402.00.00.02	MOTORAUFNAHME	NONE	2018-02-25 07:59:57	\N	\N
194	3UGMSKKJBY	23.400.00.01.03	FÜHRUNGSSTANGE	NONE	2018-02-25 07:59:57	\N	\N
195	3UGMSKKJBY	19.402.00.00.01	GRUNDPLATTE	NONE	2018-02-25 07:59:57	\N	\N
196	3UGMSKKJBY	19.402.00.00.03	SCHRAUBPLATTE	NONE	2018-02-25 07:59:57	\N	\N
197	3UGMSKKJBY	19.402.00.00.04	KUGELUMLAUFSPINDEL	NONE	2018-02-25 07:59:57	\N	\N
198	3UGMSKKJBY	23.405.00.00.06	HALTER, VERRIEGELUNG	NONE	2018-02-25 07:59:57	\N	\N
199	3UGMSKKJBY	19.402.00.00.05	FÜHRUNGSSÄULE	NONE	2018-02-25 07:59:57	\N	\N
200	3UGMSKKJBY	19.402.00.00.06	ABFANGBUCHSE, BEDIENSEITE	NONE	2018-02-25 07:59:57	\N	\N
201	3UGMSKKJBY	19.402.00.00.07	ZAHNRIEMENSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
202	3UGMSKKJBY	19.402.00.00.08	ZAHNRIEMENSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
203	3UGMSKKJBY	19.402.00.00.09	ANSCHLUSSPLATTE	NONE	2018-02-25 07:59:57	\N	\N
204	3UGMSKKJBY	19.402.00.00.11	DISTANZLEISTE	NONE	2018-02-25 07:59:57	\N	\N
205	3UGMSKKJBY	19.402.00.00.12	ABFANGBUCHSE, RÜCKSEITE	NONE	2018-02-25 07:59:57	\N	\N
206	3UGMSKKJBY	19.402.00.00.13-A	HALTER FÜR SENSOR	NONE	2018-02-25 07:59:57	\N	\N
207	3UGMSKKJBY	19.402.00.00.10	FÜHRUNGSPLATTE	NONE	2018-02-25 07:59:57	\N	\N
208	3UGMSKKJBY	19.402.00.00.14	BETÄTIGUNGSWINKEL	NONE	2018-02-25 07:59:57	\N	\N
209	3UGMSKKJBY	19.402.00.00.15-A	SCHALTANSCHLAGSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
210	3UGMSKKJBY	19.402.00.00.16	ANSCHLAGSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
211	3UGMSKKJBY	19.402.00.00.17-A	HALTER, SENSOR ENDPUNKTE	NONE	2018-02-25 07:59:57	\N	\N
212	3UGMSKKJBY	19.402.00.00.20	ELASTOMERFEDER	NONE	2018-02-25 07:59:57	\N	\N
213	3UGMSKKJBY	19.402.00.00.22-A	SCHEIBE, ZAHNRIEMENSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
214	3UGMSKKJBY	19.402.00.00.18	ENDLAGENDÄMPFUNG, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
215	3UGMSKKJBY	19.402.00.00.26	ROTGUSSFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
216	3UGMSKKJBY	19.402.01.00.01	LAGERGEHÄUSE	NONE	2018-02-25 07:59:57	\N	\N
217	3UGMSKKJBY	19.402.01.00.02-A	GLOCKE MOTORBEFESTIGUNG-UNTEN (OHNE BREMSE)	NONE	2018-02-25 07:59:57	\N	\N
218	3UGMSKKJBY	19.402.01.00.05-A	WELLE	NONE	2018-02-25 07:59:57	\N	\N
219	3UGMSKKJBY	19.402.00.00.23	VORLEGESCHEIBE KUGELSPINDEL	NONE	2018-02-25 07:59:57	\N	\N
220	3UGMSKKJBY	19.402.00.00.19	ENDLAGENDÄMPFUNG, OBEN	NONE	2018-02-25 07:59:57	\N	\N
221	3UGMSKKJBY	18.400.00.00.02	BEFESTIGUNGSLASCHE	NONE	2018-02-25 07:59:57	\N	\N
222	3UGMSKKJBY	18.400.00.00.03-B	ABSCHIRMBLECH	NONE	2018-02-25 07:59:57	\N	\N
223	3UGMSKKJBY	18.400.00.00.06-A	VERFAHRWINKEL HEIZUNG, EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
224	3UGMSKKJBY	18.400.00.00.05-A	VERFAHRWINKEL HEIZUNG, SEITE FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
225	3UGMSKKJBY	18.400.00.00.08-A	BEFESTIGUNGSWINKEL, KABELSCHLEPP	NONE	2018-02-25 07:59:57	\N	\N
226	3UGMSKKJBY	18.400.00.00.09	ABSTANDHALTER, ABSCHIRMBLECH	NONE	2018-02-25 07:59:57	\N	\N
227	3UGMSKKJBY	19.402.01.00.03-B	GLOCKE MOTORBEFESTIGUNG, OBEN	NONE	2018-02-25 07:59:57	\N	\N
228	3UGMSKKJBY	18.400.00.00.10	KLEMMLEISTE, HEIZUNG OBEN	NONE	2018-02-25 07:59:57	\N	\N
229	3UGMSKKJBY	18.400.00.00.01	BEFESTIGUNGSWINKEL	NONE	2018-02-25 07:59:57	\N	\N
230	3UGMSKKJBY	18.400.00.00.12	GEWINDESTANGE M10, L=210MM	NONE	2018-02-25 07:59:57	\N	\N
231	3UGMSKKJBY	18.400.00.00.15	DISTANZRÖHRCHEN, HEIZUNGSRAHMEN	NONE	2018-02-25 07:59:57	\N	\N
232	3UGMSKKJBY	18.400.00.00.14	KABELSCHLEPPANSCHLAG	NONE	2018-02-25 07:59:57	\N	\N
233	3UGMSKKJBY	18.400.00.00.16	GEWINDESTANGE M10, L=200MM	NONE	2018-02-25 07:59:57	\N	\N
234	3UGMSKKJBY	18.400.00.00.13	ABSTANDHALTER	NONE	2018-02-25 07:59:57	\N	\N
235	3UGMSKKJBY	18.400.00.00.17	HALTER FÜR PANZERSCHLAUCH	NONE	2018-02-25 07:59:57	\N	\N
236	3UGMSKKJBY	18.400.00.00.18	KLEMMLEISTE, HEIZUNG UNTEN	NONE	2018-02-25 07:59:57	\N	\N
237	3UGMSKKJBY	18.400.00.00.19	BEFESTIGUNGSLEISTE, HEIZUNG UNTEN	NONE	2018-02-25 07:59:57	\N	\N
238	3UGMSKKJBY	18.400.00.01.01-A	S235 JR FL 60X20 X 680	NONE	2018-02-25 07:59:57	\N	\N
239	3UGMSKKJBY	18.400.00.01.02-C	S235 JR FL 60X20 X 697	NONE	2018-02-25 07:59:57	\N	\N
240	3UGMSKKJBY	18.400.00.01.03-B	S235 JR 120X20 X 620	NONE	2018-02-25 07:59:57	\N	\N
241	3UGMSKKJBY	18.400.00.01.04-B	WINKEL, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
242	3UGMSKKJBY	18.400.00.01.05-B	S235 JR VKT 30X30 X 960	NONE	2018-02-25 07:59:57	\N	\N
243	3UGMSKKJBY	18.400.00.01.06	WINKEL 1	NONE	2018-02-25 07:59:57	\N	\N
244	3UGMSKKJBY	18.400.01.00.01	STRAHLERBLECH, QUER	NONE	2018-02-25 07:59:57	\N	\N
245	3UGMSKKJBY	18.400.00.00.11	BEFESTIGUNGSLEISTE, HEIZUNG OBEN	NONE	2018-02-25 07:59:57	\N	\N
246	3UGMSKKJBY	18.400.01.00.02	STRAHLERBLECH, LÄNGS	NONE	2018-02-25 07:59:57	\N	\N
247	3UGMSKKJBY	18.400.01.00.04	STROMSCHIENE, LÄNGS	NONE	2018-02-25 07:59:57	\N	\N
248	3UGMSKKJBY	18.400.01.00.03	BEFESTIGUNG FÜR STROMSCHIENENHALTER	NONE	2018-02-25 07:59:57	\N	\N
249	3UGMSKKJBY	18.400.01.00.05	HALTER FÜR KLEMMEN	NONE	2018-02-25 07:59:57	\N	\N
250	3UGMSKKJBY	18.400.01.00.07	HAUBE, OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
251	3UGMSKKJBY	18.400.01.00.06	STROMSCHIENE	NONE	2018-02-25 07:59:57	\N	\N
252	3UGMSKKJBY	18.400.01.00.10	HAUBENHÄLFTE, EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
253	3UGMSKKJBY	18.400.01.00.08	ABSCHLUSSBLECH HAUBE	NONE	2018-02-25 07:59:57	\N	\N
254	3UGMSKKJBY	18.400.01.00.11	HAUBENHÄLFTE, FORMSTATIONSEITE	NONE	2018-02-25 07:59:57	\N	\N
255	3UGMSKKJBY	18.400.01.00.09-A	ANSCHLUSSWINKEL, UNTERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
256	3UGMSKKJBY	18.400.01.00.12-A	DECKBLECH F. HEIZUNGSHAUBE, OBEN	NONE	2018-02-25 07:59:57	\N	\N
257	3UGMSKKJBY	18.400.01.00.13	DECKBLECH F. HEIZUNGSHAUBE, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
258	3UGMSKKJBY	18.400.01.00.17	NULLEITER QUERREGELUNG	NONE	2018-02-25 07:59:57	\N	\N
259	3UGMSKKJBY	18.400.01.01.01-C	WINKEL, LANG	NONE	2018-02-25 07:59:57	\N	\N
260	3UGMSKKJBY	18.400.01.00.15	KLEMMSTÜCK F. HEIZUNGSHAUBEN	NONE	2018-02-25 07:59:57	\N	\N
261	3UGMSKKJBY	18.400.01.01.02	WINKEL	NONE	2018-02-25 07:59:57	\N	\N
262	3UGMSKKJBY	18.400.01.01.03	WINKEL, MITTE	NONE	2018-02-25 07:59:57	\N	\N
263	3UGMSKKJBY	18.400.03.00.01	STRAHLERBLECH, OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
264	3UGMSKKJBY	18.400.03.00.04	NULLLEITER	NONE	2018-02-25 07:59:57	\N	\N
265	3UGMSKKJBY	18.400.03.00.03-A	STROMSCHIENE FÜR OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
266	3UGMSKKJBY	18.400.03.00.02-A	KLEMMENHALTER	NONE	2018-02-25 07:59:57	\N	\N
267	3UGMSKKJBY	18.400.03.00.06	ANSCHLUSSWINKEL, OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
268	3UGMSKKJBY	18.400.03.01.01-A	WINKEL, LANG	NONE	2018-02-25 07:59:57	\N	\N
269	3UGMSKKJBY	18.400.03.00.05	DISTANZHÜLSE NULLLEITER OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
270	3UGMSKKJBY	18.400.03.01.03	WINKEL, LINKS	NONE	2018-02-25 07:59:57	\N	\N
271	3UGMSKKJBY	18.400.01.01.04	WINKEL	NONE	2018-02-25 07:59:57	\N	\N
272	3UGMSKKJBY	18.400.04.00.01	TRÄGERLEISTE SCHALTSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
273	3UGMSKKJBY	18.400.00.01.07	WINKEL 2	NONE	2018-02-25 07:59:57	\N	\N
274	3UGMSKKJBY	18.400.03.01.04	WINKEL, KURZ	NONE	2018-02-25 07:59:57	\N	\N
275	3UGMSKKJBY	18.400.04.00.03	ABSTANDHALTER	NONE	2018-02-25 07:59:57	\N	\N
276	3UGMSKKJBY	24.400.00.01.01	KÜHLPLATTE, SCHALTSCHRANK HEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
277	3UGMSKKJBY	24.400.00.01.02	AUFSCHRAUBPLATTE FÜR SOLID STATE RELAIS	NONE	2018-02-25 07:59:57	\N	\N
278	3UGMSKKJBY	00.003.00.00.04-A	KNIEHEBELWELLE	NONE	2018-02-25 07:59:57	\N	\N
279	3UGMSKKJBY	00.003.00.00.03	KNIEHEBEL	NONE	2018-02-25 07:59:57	\N	\N
280	3UGMSKKJBY	00.003.00.00.08	SPREIZLEISTE 1	NONE	2018-02-25 07:59:57	\N	\N
281	3UGMSKKJBY	00.003.00.00.13-A	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
282	3UGMSKKJBY	00.003.00.00.18	MITNEHMER	NONE	2018-02-25 07:59:57	\N	\N
283	3UGMSKKJBY	00.003.00.00.09	SPREIZLEISTE 2	NONE	2018-02-25 07:59:57	\N	\N
284	3UGMSKKJBY	00.003.00.00.24	BOLZEN, KNIEHEBEL	NONE	2018-02-25 07:59:57	\N	\N
285	3UGMSKKJBY	00.003.00.01.02	WICKELROHR	NONE	2018-02-25 07:59:57	\N	\N
286	3UGMSKKJBY	17.400.00.00.04	"RITZEL, Z=16, M=2	NONE	2018-02-25 07:59:57	\N	\N
287	3UGMSKKJBY	17.402.00.00.01-B	STREBE, WICKELWELLE	NONE	2018-02-25 07:59:57	\N	\N
288	3UGMSKKJBY	17.402.00.00.03	"ZAHNRAD, Z=47, M=2	NONE	2018-02-25 07:59:57	\N	\N
289	3UGMSKKJBY	17.400.00.00.03	SCHEIBE, AUFWICKLUNG	NONE	2018-02-25 07:59:57	\N	\N
290	3UGMSKKJBY	17.402.00.00.02-A	GETRIEBEABDECKUNG, AUSSEN	NONE	2018-02-25 07:59:57	\N	\N
291	3UGMSKKJBY	17.402.00.00.05	DISTANZRING	NONE	2018-02-25 07:59:57	\N	\N
292	3UGMSKKJBY	17.402.00.00.08	DISTANZRING	NONE	2018-02-25 07:59:57	\N	\N
293	3UGMSKKJBY	17.402.00.00.07	ZWISCHENWELLE	NONE	2018-02-25 07:59:57	\N	\N
294	3UGMSKKJBY	17.402.00.00.09	GETRIEBEABDECKUNG, INNEN	NONE	2018-02-25 07:59:57	\N	\N
295	3UGMSKKJBY	17.402.00.01.02-A	STREBE, GESTELL	NONE	2018-02-25 07:59:57	\N	\N
296	3UGMSKKJBY	17.402.00.01.01-A	HALTESTREBE, GESTELL	NONE	2018-02-25 07:59:57	\N	\N
297	3UGMSKKJBY	17.402.00.01.03-A	BODENPLATTE, GESTELL	NONE	2018-02-25 07:59:57	\N	\N
298	3UGMSKKJBY	17.402.01.01.01	FLANSCH WICKELWELLE	NONE	2018-02-25 07:59:57	\N	\N
299	3UGMSKKJBY	16.402.00.00.01	HALTELEISTE FÜR LICHTSCHRANKE	NONE	2018-02-25 07:59:57	\N	\N
300	3UGMSKKJBY	16.402.01.01.03	SCHWINGE, ACHSE	NONE	2018-02-25 07:59:57	\N	\N
301	3UGMSKKJBY	16.402.01.01.01	BOCK	NONE	2018-02-25 07:59:57	\N	\N
302	3UGMSKKJBY	16.402.01.01.04	LAGERUNG ABROLLBOCK	NONE	2018-02-25 07:59:57	\N	\N
303	3UGMSKKJBY	16.402.01.01.05	GEWINDEEINSATZ M20/M12	NONE	2018-02-25 07:59:57	\N	\N
304	3UGMSKKJBY	16.402.01.01.06	ACHSE FÜR UMLENKROLLE, 910MM LANG	NONE	2018-02-25 07:59:57	\N	\N
305	3UGMSKKJBY	16.402.01.01.13	LUFTVERTEILER	NONE	2018-02-25 07:59:57	\N	\N
306	3UGMSKKJBY	16.402.01.01.11	ABWICKELWELLE	NONE	2018-02-25 07:59:57	\N	\N
307	3UGMSKKJBY	16.402.01.01.16	KLEMMRING	NONE	2018-02-25 07:59:57	\N	\N
308	3UGMSKKJBY	16.402.01.01.07	EINLAUFROLLE	NONE	2018-02-25 07:59:57	\N	\N
309	3UGMSKKJBY	16.402.02.00.01	GRUNDPLATTE, ABZUG	NONE	2018-02-25 07:59:57	\N	\N
310	3UGMSKKJBY	16.402.01.01.17	KONUS Ø75	NONE	2018-02-25 07:59:57	\N	\N
311	3UGMSKKJBY	16.402.01.01.18	STANDFUSS	NONE	2018-02-25 07:59:57	\N	\N
312	3UGMSKKJBY	16.402.02.00.02	SEITENWAND, ABZUG	NONE	2018-02-25 07:59:57	\N	\N
313	3UGMSKKJBY	16.402.02.00.04	EINLAUF-ABDECKUNG, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
314	3UGMSKKJBY	16.402.02.00.07	DECKBLECH	NONE	2018-02-25 07:59:57	\N	\N
315	3UGMSKKJBY	16.402.02.00.08	ABDECKHAUBE FÜR KUPPLUNG	NONE	2018-02-25 07:59:57	\N	\N
316	3UGMSKKJBY	16.402.02.00.10	WALZENWELLE	NONE	2018-02-25 07:59:57	\N	\N
317	3UGMSKKJBY	16.402.02.00.09	WALZENACHSE	NONE	2018-02-25 07:59:57	\N	\N
318	3UGMSKKJBY	16.402.02.00.06	ZYLINDERHALTER	NONE	2018-02-25 07:59:57	\N	\N
319	3UGMSKKJBY	16.402.01.01.10	ACHSE FÜR UMLENKROLLE, 703MM LANG	NONE	2018-02-25 07:59:57	\N	\N
320	3UGMSKKJBY	16.402.02.01.01	WALZE, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
321	3UGMSKKJBY	16.402.02.01.02	SPANNSATZSITZ	NONE	2018-02-25 07:59:57	\N	\N
322	3UGMSKKJBY	16.402.02.01.03	BELAG, GUMMI	NONE	2018-02-25 07:59:57	\N	\N
323	3UGMSKKJBY	16.402.02.02.02	LAGERSITZ, WALZE OBEN	NONE	2018-02-25 07:59:57	\N	\N
324	3UGMSKKJBY	16.402.03.02.01	LEISTE	NONE	2018-02-25 07:59:57	\N	\N
325	3UGMSKKJBY	16.402.02.02.01	WALZE, OBEN	NONE	2018-02-25 07:59:57	\N	\N
326	3UGMSKKJBY	16.402.03.02.02	GEWINDESTANGE M12, L=300MM	NONE	2018-02-25 07:59:57	\N	\N
327	3UGMSKKJBY	16.402.03.02.03	ROLLE	NONE	2018-02-25 07:59:57	\N	\N
328	3UGMSKKJBY	16.402.03.02.04	GEWINDESTANGE M12, L=750MM	NONE	2018-02-25 07:59:57	\N	\N
329	3UGMSKKJBY	21.400.00.02.01	TRÄGERPLATTE FÜR HEBELVENTILE ABROLLBOCK	NONE	2018-02-25 07:59:57	\N	\N
330	3UGMSKKJBY	00.105.05.05.01	EINLAUFTISCH	NONE	2018-02-25 07:59:57	\N	\N
331	3UGMSKKJBY	10.401.00.00.17	SEITENANSCHLAG	NONE	2018-02-25 07:59:57	\N	\N
332	3UGMSKKJBY	00.105.05.05.02	HALTELEISTE FÜR FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
333	3UGMSKKJBY	14.102.00.00.39	DISTANZROHR	NONE	2018-02-25 07:59:57	\N	\N
334	3UGMSKKJBY	14.200.00.00.19	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
335	3UGMSKKJBY	14.102.00.00.33	SCHEIBE, EINLAUF	NONE	2018-02-25 07:59:57	\N	\N
337	3UGMSKKJBY	14.200.00.00.18	HÜLSE	NONE	2018-02-25 07:59:57	\N	\N
338	3UGMSKKJBY	14.400.00.00.02	"DUPLEX KETTENRAD DIN 8192 - ISO 08 B - 2 (1/2"" X 5/16""), 13 ZÄHNE"	NONE	2018-02-25 07:59:57	\N	\N
339	3UGMSKKJBY	14.400.00.00.03	ENTSTACHELSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
340	3UGMSKKJBY	14.400.00.00.05	"DUPLEX KETTENRAD, DIN 8192 - A, ISO 08B-2 (1/2"" X 5/16""), 19 ZÄHNE"	NONE	2018-02-25 07:59:57	\N	\N
341	3UGMSKKJBY	14.400.00.00.06	LAGERBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
342	3UGMSKKJBY	14.400.00.00.09	GLEITSCHIENE UNTEN, VORNE	NONE	2018-02-25 07:59:57	\N	\N
343	3UGMSKKJBY	14.400.00.00.10	GLEITSCHIENE UNTEN, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
344	3UGMSKKJBY	14.400.00.00.11	UMLENKSTÜCK, VORNE	NONE	2018-02-25 07:59:57	\N	\N
345	3UGMSKKJBY	14.400.00.00.12	UMLENKSTÜCK, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
346	3UGMSKKJBY	14.400.00.00.13	GLEITFLACHSTAHL	NONE	2018-02-25 07:59:57	\N	\N
347	3UGMSKKJBY	14.400.00.00.15-A	OBERES DECKBLECH,AUSLAUF HINTEN	NONE	2018-02-25 07:59:57	\N	\N
348	3UGMSKKJBY	14.400.00.00.17-A	OBERES DECKBLECH, EINLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
349	3UGMSKKJBY	14.400.00.00.18-A	OBERES DECKBLECH, EINLAUF HINTEN	NONE	2018-02-25 07:59:57	\N	\N
350	3UGMSKKJBY	14.400.00.00.20	UNTERES DECKBLECH, EINLAUF	NONE	2018-02-25 07:59:57	\N	\N
351	3UGMSKKJBY	14.400.00.00.14-A	OBERES DECKBLECH, AUSLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
352	3UGMSKKJBY	14.400.00.00.22	ABDECKUNG, AUSLAUF HINTEN	NONE	2018-02-25 07:59:57	\N	\N
353	3UGMSKKJBY	14.400.00.00.21	ABDECKUNG, AUSLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
354	3UGMSKKJBY	14.400.00.00.23	KETTENSPANNBLECH	NONE	2018-02-25 07:59:57	\N	\N
355	3UGMSKKJBY	14.400.00.00.24-D	FÜHRUNGSPLATTE	NONE	2018-02-25 07:59:57	\N	\N
356	3UGMSKKJBY	14.400.00.00.25	ZYLINDERPLATTE	NONE	2018-02-25 07:59:57	\N	\N
357	3UGMSKKJBY	14.400.00.00.29-B	AUFLAGESTÜCK FÜR FOLIENTRANSPORT	NONE	2018-02-25 07:59:57	\N	\N
358	3UGMSKKJBY	14.400.00.00.28	JUSTIERBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
359	3UGMSKKJBY	14.400.00.00.31	HÜLSE FÜR KETTENRAD MIT NABE	NONE	2018-02-25 07:59:57	\N	\N
360	3UGMSKKJBY	14.400.00.00.35	EINLAUFROLLE	NONE	2018-02-25 07:59:57	\N	\N
361	3UGMSKKJBY	14.400.00.00.32-A	SPINDEL, FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
362	3UGMSKKJBY	14.400.00.00.33-A	SPINDEL, EINLAUF	NONE	2018-02-25 07:59:57	\N	\N
363	3UGMSKKJBY	14.400.00.00.36	EINLAUFACHSE	NONE	2018-02-25 07:59:57	\N	\N
364	3UGMSKKJBY	14.400.00.00.40	EINSTACHELROLLE	NONE	2018-02-25 07:59:57	\N	\N
365	3UGMSKKJBY	14.400.00.00.41	MUTTER M20, LINKS	NONE	2018-02-25 07:59:57	\N	\N
366	3UGMSKKJBY	14.400.00.00.42	MUTTER M20, RECHTS	NONE	2018-02-25 07:59:57	\N	\N
367	3UGMSKKJBY	14.400.00.00.47-A	SPREIZPLATTE	NONE	2018-02-25 07:59:57	\N	\N
368	3UGMSKKJBY	14.400.00.00.43	BRÜCKE, SPREIZHUBVERSTELLUNG	NONE	2018-02-25 07:59:57	\N	\N
369	3UGMSKKJBY	14.400.00.04.01	VIERKANTROHR	NONE	2018-02-25 07:59:57	\N	\N
370	3UGMSKKJBY	14.400.00.04.02	ANSCHLUSSSTÜCK	NONE	2018-02-25 07:59:57	\N	\N
371	3UGMSKKJBY	14.400.00.04.03	ENDSTÜCK	NONE	2018-02-25 07:59:57	\N	\N
372	3UGMSKKJBY	14.400.00.04.04	ROHR	NONE	2018-02-25 07:59:57	\N	\N
373	3UGMSKKJBY	14.400.00.05.01-A	FLACHSTAHL	NONE	2018-02-25 07:59:57	\N	\N
374	3UGMSKKJBY	14.400.01.00.02-A	DECKLEISTE, FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
375	3UGMSKKJBY	14.400.01.00.04	ROLLE	NONE	2018-02-25 07:59:57	\N	\N
376	3UGMSKKJBY	14.400.01.00.05	M20 MUTTER, RECHTS	NONE	2018-02-25 07:59:57	\N	\N
377	3UGMSKKJBY	14.400.00.06.01-B	FLACHSTAHL	NONE	2018-02-25 07:59:57	\N	\N
378	3UGMSKKJBY	14.400.00.06.02-B	FLACHSTAHL	NONE	2018-02-25 07:59:57	\N	\N
379	3UGMSKKJBY	14.400.01.00.06	M20 MUTTER, LINKS	NONE	2018-02-25 07:59:57	\N	\N
380	3UGMSKKJBY	14.400.01.00.07	LAGERKLOTZ	NONE	2018-02-25 07:59:57	\N	\N
381	3UGMSKKJBY	14.400.00.00.04	LAGERBOLZEN F. KETTENRAD MIT NABE	NONE	2018-02-25 07:59:57	\N	\N
382	3UGMSKKJBY	14.400.01.00.08	SPINDEL	NONE	2018-02-25 07:59:57	\N	\N
383	3UGMSKKJBY	14.400.00.05.02-A	VIERKANT	NONE	2018-02-25 07:59:57	\N	\N
384	3UGMSKKJBY	14.401.00.00.02	OBERE GLEITSCHIENE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
385	3UGMSKKJBY	14.401.00.00.01	OBERE GLEITSCHIENE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
386	3UGMSKKJBY	14.401.00.00.05	UNTERES DECKBLECH, MITTE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
387	3UGMSKKJBY	14.401.00.00.03	OBERES DECKBLECH, MITTE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
388	3UGMSKKJBY	14.401.00.00.04	OBERES DECKBLECH, MITTE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
389	3UGMSKKJBY	14.401.00.00.06	UNTERES DECKBLECH, MITTE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
390	3UGMSKKJBY	14.401.00.00.08	KETTENRAD, VERSTELLSPINDEL	NONE	2018-02-25 07:59:57	\N	\N
391	3UGMSKKJBY	14.401.00.00.07	KETTENRAD, EINLAUFSPINDEL	NONE	2018-02-25 07:59:57	\N	\N
392	3UGMSKKJBY	14.401.00.00.09	DISTANZSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
393	3UGMSKKJBY	14.401.00.00.11	KETTENSPANNER, FOLIENBREITENVERSTELLUNG	NONE	2018-02-25 07:59:57	\N	\N
394	3UGMSKKJBY	14.400.01.00.01-A	LEISTE, FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
395	3UGMSKKJBY	14.401.00.01.01	BLECH	NONE	2018-02-25 07:59:57	\N	\N
396	3UGMSKKJBY	14.401.00.00.10	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
397	3UGMSKKJBY	14.401.00.01.02	STIRNSEITENBLECH	NONE	2018-02-25 07:59:57	\N	\N
398	3UGMSKKJBY	14.401.00.01.03	BLECH, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
399	3UGMSKKJBY	14.401.00.02.01	WANGE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
400	3UGMSKKJBY	14.400.00.05.03-A	STÜTZE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
401	3UGMSKKJBY	14.401.00.02.03	BLECH, UNTEN	NONE	2018-02-25 07:59:57	\N	\N
402	3UGMSKKJBY	15.400.00.00.01	KEILWELLE	NONE	2018-02-25 07:59:57	\N	\N
403	3UGMSKKJBY	15.400.00.00.03	DISTANZSTÜCK FÜR STEHLAGER	NONE	2018-02-25 07:59:57	\N	\N
404	3UGMSKKJBY	14.401.00.02.02	STIRNSEITENBLECH	NONE	2018-02-25 07:59:57	\N	\N
405	3UGMSKKJBY	15.400.00.00.05-C	MOTORFLANSCHPLATTE	NONE	2018-02-25 07:59:57	\N	\N
406	3UGMSKKJBY	15.400.00.00.02	LAGERFLANSCH	NONE	2018-02-25 07:59:57	\N	\N
407	3UGMSKKJBY	15.400.00.00.06	ABDECKBLECH	NONE	2018-02-25 07:59:57	\N	\N
408	3UGMSKKJBY	15.400.00.00.08-A	ZAHNRIEMENSCHEIBE, KEILWELLE	NONE	2018-02-25 07:59:57	\N	\N
409	3UGMSKKJBY	15.400.00.00.11	"DUPLEX KETTENRAD, DIN 8192 - A, ISO 08B-2 (1/2"" X 5/16""),19 ZÄHNE"	NONE	2018-02-25 07:59:57	\N	\N
410	3UGMSKKJBY	15.400.00.00.12	KEILNABE	NONE	2018-02-25 07:59:57	\N	\N
411	3UGMSKKJBY	15.401.00.00.01	BLECHKONSOLE	NONE	2018-02-25 07:59:57	\N	\N
412	3UGMSKKJBY	15.401.01.00.01	ABSTANDSTRÄGER	NONE	2018-02-25 07:59:57	\N	\N
413	3UGMSKKJBY	15.401.01.00.02	ABSTANDSTRÄGER SEITEN	NONE	2018-02-25 07:59:57	\N	\N
414	3UGMSKKJBY	11.400.00.00.07-B	FÜHRUNGSSTÜCK, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
415	3UGMSKKJBY	11.400.00.00.08-B	FÜHRUNGSSTÜCK, VORNE	NONE	2018-02-25 07:59:57	\N	\N
416	3UGMSKKJBY	11.400.00.00.10-A	FLANSCH FÜR FÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
417	3UGMSKKJBY	11.400.00.00.09-B	FÜHRUNGSWELLE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
418	3UGMSKKJBY	11.400.00.00.12	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
419	3UGMSKKJBY	11.400.00.00.15	ACHSBOLZEN, SCHWINGE	NONE	2018-02-25 07:59:57	\N	\N
420	3UGMSKKJBY	11.400.00.00.22-A	TISCHGERADFÜHRUNG, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
421	3UGMSKKJBY	11.400.00.00.23-A	TISCHGERADFÜHRUNG-VORNE	NONE	2018-02-25 07:59:57	\N	\N
422	3UGMSKKJBY	11.400.00.00.24-B	TISCHANLENKUNG, VORDERTEIL	NONE	2018-02-25 07:59:57	\N	\N
423	3UGMSKKJBY	11.400.00.00.25	BOLZEN, TISCHANLENKUNG	NONE	2018-02-25 07:59:57	\N	\N
424	3UGMSKKJBY	11.400.00.00.26	FLANSCH FÜR TISCHANLENKUNG	NONE	2018-02-25 07:59:57	\N	\N
425	3UGMSKKJBY	11.400.00.00.27	ABDECKUNG FLANSCH	NONE	2018-02-25 07:59:57	\N	\N
426	3UGMSKKJBY	11.400.00.00.28	KNIEHEBELBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
427	3UGMSKKJBY	11.400.00.00.36-B	FÜHRUNGSWELLE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
428	3UGMSKKJBY	11.400.00.00.29	ACHSHALTER	NONE	2018-02-25 07:59:57	\N	\N
429	3UGMSKKJBY	11.400.00.00.41-A	FÜHRUNGSSTANGE FÜR FORMTISCH	NONE	2018-02-25 07:59:57	\N	\N
430	3UGMSKKJBY	11.400.00.00.43	BEGRENZUNGSSCHEIBE, STÜTZROLLE	NONE	2018-02-25 07:59:57	\N	\N
431	3UGMSKKJBY	11.400.00.00.49-A	DISTANZHÜLSE FÜR SCHWINGENFLANSCH	NONE	2018-02-25 07:59:57	\N	\N
432	3UGMSKKJBY	11.400.00.00.37-A	TISCHANLENKUNG, HINTERTEIL	NONE	2018-02-25 07:59:57	\N	\N
433	3UGMSKKJBY	11.400.00.00.44	DICHTSCHEIBE, KNIEHEBELLAGER	NONE	2018-02-25 07:59:57	\N	\N
434	3UGMSKKJBY	11.400.00.00.51	BEGRENZUNGSSCHEIBE, TISCHANLENKUNG	NONE	2018-02-25 07:59:57	\N	\N
435	3UGMSKKJBY	11.400.00.00.53	EINSTELLKEIL, TISCHGERADFÜHRUNG VORNE	NONE	2018-02-25 07:59:57	\N	\N
436	3UGMSKKJBY	11.400.00.00.54-B	VERSTELLSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
437	3UGMSKKJBY	11.400.00.00.52	EINSTELLKEIL, TISCHGERADFÜHRUNG HINTEN	NONE	2018-02-25 07:59:57	\N	\N
438	3UGMSKKJBY	11.400.00.00.55	HALTEPLATTE	NONE	2018-02-25 07:59:57	\N	\N
439	3UGMSKKJBY	11.400.00.00.58	AUSWERFERPLATTE, SCHWIMMENDE LAGERUNG	NONE	2018-02-25 07:59:57	\N	\N
440	3UGMSKKJBY	11.400.00.00.57-A	HALTEBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
441	3UGMSKKJBY	11.400.00.00.60	INIHALTER	NONE	2018-02-25 07:59:57	\N	\N
442	3UGMSKKJBY	11.400.00.02.01	ZAPFEN	NONE	2018-02-25 07:59:57	\N	\N
443	3UGMSKKJBY	11.400.00.02.02-A	VORDERE SEITENWAND FORMTISCH	NONE	2018-02-25 07:59:57	\N	\N
444	3UGMSKKJBY	11.400.00.02.03-A	HINTERE SEITENWAND FORMTISCH	NONE	2018-02-25 07:59:57	\N	\N
445	3UGMSKKJBY	11.400.00.02.05	STREBE	NONE	2018-02-25 07:59:57	\N	\N
446	3UGMSKKJBY	11.400.00.02.04	PLATTE, INNEN	NONE	2018-02-25 07:59:57	\N	\N
447	3UGMSKKJBY	11.400.00.02.07-C	PLATTE, OBEN	NONE	2018-02-25 07:59:57	\N	\N
448	3UGMSKKJBY	11.400.00.03.00-E	AUSWERFERKONSOLE-ZYLINDER Ø63-HUB 150MM	NONE	2018-02-25 07:59:57	\N	\N
449	3UGMSKKJBY	11.401.00.00.01-A	ANTRIEBSWELLE	NONE	2018-02-25 07:59:57	\N	\N
450	3UGMSKKJBY	11.401.00.00.02-A	KURBEL	NONE	2018-02-25 07:59:57	\N	\N
451	3UGMSKKJBY	11.401.00.00.03	KURBELBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
452	3UGMSKKJBY	11.401.00.00.05	KOPPEL, VORNE	NONE	2018-02-25 07:59:57	\N	\N
453	3UGMSKKJBY	11.401.00.00.06	KOPPEL, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
454	3UGMSKKJBY	11.401.00.00.08	ACHSBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
455	3UGMSKKJBY	11.401.00.00.09-B	KNIEHEBEL, VORNE	NONE	2018-02-25 07:59:57	\N	\N
456	3UGMSKKJBY	11.401.00.00.10-B	KNIEHEBEL, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
457	3UGMSKKJBY	11.401.00.00.15	REFLEKTOR	NONE	2018-02-25 07:59:57	\N	\N
458	3UGMSKKJBY	11.401.01.00.01	SEITENWAND, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
459	3UGMSKKJBY	11.401.01.00.02	SEITENWAND, VORNE	NONE	2018-02-25 07:59:57	\N	\N
460	3UGMSKKJBY	11.401.01.00.03	BODENPLATTE FORMSTATION, STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
461	3UGMSKKJBY	11.401.01.01.01	TRÄGERPLATTE ANTRIEB, STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
462	3UGMSKKJBY	11.401.01.00.07	INITIATORHALTER	NONE	2018-02-25 07:59:57	\N	\N
463	3UGMSKKJBY	11.401.01.01.02	STREBE, STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
464	3UGMSKKJBY	11.401.01.00.04	PLATTE, STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
465	3UGMSKKJBY	11.401.01.00.05	GEGENLAGER FÜR EINSTELLKEIL	NONE	2018-02-25 07:59:57	\N	\N
466	3UGMSKKJBY	11.401.02.00.01-A	SCHNECKENWELLE FÜR HANDVERSTELLUNG	NONE	2018-02-25 07:59:57	\N	\N
467	3UGMSKKJBY	11.401.02.00.02	POM-BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
468	3UGMSKKJBY	11.401.02.01.02	LAGERPLATTE, VORNE	NONE	2018-02-25 07:59:57	\N	\N
469	3UGMSKKJBY	11.401.02.00.04	SCHNECKENRAD	NONE	2018-02-25 07:59:57	\N	\N
470	3UGMSKKJBY	11.401.02.01.01	TRÄGERBLOCK,HANDKURBEL	NONE	2018-02-25 07:59:57	\N	\N
471	3UGMSKKJBY	11.401.02.01.04	GRIFF FÜR FIXIERUNGSSTIFTE	NONE	2018-02-25 07:59:57	\N	\N
472	3UGMSKKJBY	11.401.02.01.03	LAGERPLATTE, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
473	3UGMSKKJBY	11.401.02.01.05	HALTEWINKEL FÜR NÄHERUNGSSCHALTER (M12)	NONE	2018-02-25 07:59:57	\N	\N
474	3UGMSKKJBY	11.400.00.02.06-C	TRÄGER, INNEN	NONE	2018-02-25 07:59:57	\N	\N
475	3UGMSKKJBY	12.402.01.00.02	GLOCKE FORMSTATION, UNTEN-REXROTH MOTOR - STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
476	3UGMSKKJBY	21.400.00.01.01	FORMLUFTVERTEILER	NONE	2018-02-25 07:59:57	\N	\N
477	3UGMSKKJBY	24.400.00.00.06	KÜHLWASSERVERTEILER, UNTERWERKZEUG	NONE	2018-02-25 07:59:57	\N	\N
478	3UGMSKKJBY	24.400.00.00.07	ANSCHLUSS, DREHDURCHFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
479	3UGMSKKJBY	24.400.00.00.08	KÜHLWASSERVERTEILER FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
480	3UGMSKKJBY	24.400.00.00.09	DISTANZBUCHSE FÜR KÜHLWASSERVERTEILER	NONE	2018-02-25 07:59:57	\N	\N
481	3UGMSKKJBY	24.400.00.00.10	DISTANZBUCHSE FÜR KÜHLWASSERVERTEILER, KURZ	NONE	2018-02-25 07:59:57	\N	\N
482	3UGMSKKJBY	11.401.00.00.13	FLANSCH FÜR SCHWINGE	NONE	2018-02-25 07:59:57	\N	\N
483	3UGMSKKJBY	12.402.01.00.03	GLOCKE FORMSTATION, OBEN-REXROTH MOTOR - STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
484	3UGMSKKJBY	11.300.02.00.01-A	DECKEL	NONE	2018-02-25 07:59:57	\N	\N
485	3UGMSKKJBY	11.300.02.00.03	STELLRING	NONE	2018-02-25 07:59:57	\N	\N
486	3UGMSKKJBY	11.300.02.00.02-A	FÜHRUNGSFLANSCH	NONE	2018-02-25 07:59:57	\N	\N
487	3UGMSKKJBY	11.300.02.00.04-B	ACHSE ZUR VERSTELLUNG, LANG	NONE	2018-02-25 07:59:57	\N	\N
488	3UGMSKKJBY	11.300.02.00.05-A	ACHSE ZUR VERSTELLUNG, KURZ	NONE	2018-02-25 07:59:57	\N	\N
489	3UGMSKKJBY	11.300.02.00.07	GERADSTIRNRAD, MODUL 2, 47 ZÄHNE	NONE	2018-02-25 07:59:57	\N	\N
490	3UGMSKKJBY	11.300.02.00.06-A	"KETTENRAD DIN 8192 - 13 ZÄHNE, 1/2 X 3/16"""	NONE	2018-02-25 07:59:57	\N	\N
491	3UGMSKKJBY	11.300.02.00.10	ACHSE	NONE	2018-02-25 07:59:57	\N	\N
492	3UGMSKKJBY	11.300.02.00.08	GERADSTIRNRAD, MODUL 2, 20 ZÄHNE	NONE	2018-02-25 07:59:57	\N	\N
493	3UGMSKKJBY	11.300.02.00.09	KOLBEN	NONE	2018-02-25 07:59:57	\N	\N
494	3UGMSKKJBY	11.300.02.00.13	KETTENSPANNVORRICHTUNG	NONE	2018-02-25 07:59:57	\N	\N
495	3UGMSKKJBY	11.300.02.00.11-A	"KETTENRAD DIN 8192 - 9 ZÄHNE, ISO 083-1, 1/2 X 3/16"""	NONE	2018-02-25 07:59:57	\N	\N
496	3UGMSKKJBY	11.300.02.00.12	SPINDELLAGERUNG	NONE	2018-02-25 07:59:57	\N	\N
497	3UGMSKKJBY	11.300.02.00.14	ACHSE FÜR KETTENSPANNER	NONE	2018-02-25 07:59:57	\N	\N
498	3UGMSKKJBY	11.300.02.00.15	UNTERLEGSCHEIBE, ANTRIEBSKETTENRAD	NONE	2018-02-25 07:59:57	\N	\N
499	3UGMSKKJBY	11.400.00.00.39	RING FÜR FÜHRUNGSSÄULE	NONE	2018-02-25 07:59:57	\N	\N
500	3UGMSKKJBY	11.300.02.00.16	UNTERLEGSCHEIBE, UMLENKRAD	NONE	2018-02-25 07:59:57	\N	\N
501	3UGMSKKJBY	11.400.00.04.01-A	WERKZEUGWECHSELFÜHRUNG, SCHIENE VORNE	NONE	2018-02-25 07:59:57	\N	\N
502	3UGMSKKJBY	11.400.00.04.02-A	WERKZEUGWECHSELFÜHRUNG, SCHIENE HINTEN	NONE	2018-02-25 07:59:57	\N	\N
503	3UGMSKKJBY	11.400.00.04.04	WERKZEUGWECHSELFÜHRUNG, BOLZEN	NONE	2018-02-25 07:59:57	\N	\N
504	3UGMSKKJBY	11.400.00.04.05	WERKZEUGWECHSELFÜHRUNG, SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
505	3UGMSKKJBY	11.400.00.04.06	WERKZEUGWECHSELFÜHRUNG, KUNSTSTOFFSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
506	3UGMSKKJBY	11.400.00.04.03	WERKZEUGWECHSELFÜHRUNG, HALTERUNG	NONE	2018-02-25 07:59:57	\N	\N
507	3UGMSKKJBY	11.400.00.07.01-A	WERKZEUGHALTER, HALTER	NONE	2018-02-25 07:59:57	\N	\N
508	3UGMSKKJBY	11.400.00.07.02-B	WERKZEUGHALTER, SCHIENE	NONE	2018-02-25 07:59:57	\N	\N
509	3UGMSKKJBY	11.401.00.01.00	OBERJOCH	NONE	2018-02-25 07:59:57	\N	\N
510	3UGMSKKJBY	10.401.00.00.01	TRÄGER, LINKS	NONE	2018-02-25 07:59:57	\N	\N
511	3UGMSKKJBY	10.401.00.00.02	TRÄGER, RECHTS	NONE	2018-02-25 07:59:57	\N	\N
512	3UGMSKKJBY	10.401.00.00.03	STREBE	NONE	2018-02-25 07:59:57	\N	\N
513	3UGMSKKJBY	10.401.00.00.04	MITTELSTREBE	NONE	2018-02-25 07:59:57	\N	\N
514	3UGMSKKJBY	10.401.00.00.05	ABSTANDSHALTER FÜR ROLLBAHN	NONE	2018-02-25 07:59:57	\N	\N
515	3UGMSKKJBY	10.401.00.00.06	SPURTRÄGER, RECHTS	NONE	2018-02-25 07:59:57	\N	\N
516	3UGMSKKJBY	10.401.00.00.07	SPURTRÄGER, LINKS	NONE	2018-02-25 07:59:57	\N	\N
517	3UGMSKKJBY	10.401.00.00.08	DISTANZHÜLSE FÜR MASCHINENFÜSSE	NONE	2018-02-25 07:59:57	\N	\N
518	3UGMSKKJBY	11.401.01.00.06-A	FÜHRUNGSSÄULE, LANGE VERSION	NONE	2018-02-25 07:59:57	\N	\N
519	3UGMSKKJBY	10.401.00.00.10	ANSCHLAG, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
520	3UGMSKKJBY	10.401.00.00.12	HALTERUNG KABELKANAL	NONE	2018-02-25 07:59:57	\N	\N
521	3UGMSKKJBY	10.401.00.00.14	ABSTANDSHALTER FÜR ROLLBAHN	NONE	2018-02-25 07:59:57	\N	\N
522	3UGMSKKJBY	10.401.00.00.16	MONTAGEFÜSSE	NONE	2018-02-25 07:59:57	\N	\N
523	3UGMSKKJBY	10.401.00.00.13	ABSTANDSHALTER FÜR ROLLBAHN	NONE	2018-02-25 07:59:57	\N	\N
524	3UGMSKKJBY	10.401.01.00.02	TRÄGER OBERWAGEN, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
525	3UGMSKKJBY	10.401.00.00.15	ANSCHRAUBPLATTE FÜR STECKDOSE	NONE	2018-02-25 07:59:57	\N	\N
526	3UGMSKKJBY	10.401.01.00.03	TRÄGER OBERWAGEN, LINKS	NONE	2018-02-25 07:59:57	\N	\N
527	3UGMSKKJBY	10.401.01.00.05	TRÄGER OBERWAGEN, VORNE	NONE	2018-02-25 07:59:57	\N	\N
528	3UGMSKKJBY	10.401.01.00.08	ROLLENBEFESTIGUNG	NONE	2018-02-25 07:59:57	\N	\N
529	3UGMSKKJBY	10.401.01.00.06	STÜTZROLLE	NONE	2018-02-25 07:59:57	\N	\N
530	3UGMSKKJBY	10.401.01.00.07	ABSTANDSBUCHSE STÜTZROLLE	NONE	2018-02-25 07:59:57	\N	\N
531	3UGMSKKJBY	10.401.01.00.04	TRÄGER OBERWAGEN, RECHTS	NONE	2018-02-25 07:59:57	\N	\N
532	3UGMSKKJBY	10.401.01.00.09	ACHSBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
533	3UGMSKKJBY	10.401.00.00.09	EINSTELLSCHRAUBE FÜR MASCHINENFÜSSE	NONE	2018-02-25 07:59:57	\N	\N
534	3UGMSKKJBY	10.401.01.00.10	POM-ROLLE	NONE	2018-02-25 07:59:57	\N	\N
535	3UGMSKKJBY	10.401.02.00.01	TRÄGERLEISTE SCHALTSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
536	3UGMSKKJBY	10.401.02.00.02	ABSTANDHALTER, LANG	NONE	2018-02-25 07:59:57	\N	\N
537	3UGMSKKJBY	10.401.02.00.03	ABSTANDHALTER, KURZ	NONE	2018-02-25 07:59:57	\N	\N
538	3UGMSKKJBY	10.401.00.00.11	ANSCHLAG, VORNE	NONE	2018-02-25 07:59:57	\N	\N
539	3UGMSKKJBY	24.400.00.00.11	KÜHLWASSERVERTEILER (FOLIENTRANSPORT, SCHALTSCHRÄNKE)	NONE	2018-02-25 07:59:57	\N	\N
540	3UGMSKKJBY	11.400.00.04.08	WERKZEUGWECHSELFÜHRUNG, HALTEWINKEL	NONE	2018-02-25 07:59:57	\N	\N
541	3UGMSKKJBY	10.401.01.00.01	BLECH FÜR RAHMEN	NONE	2018-02-25 07:59:57	\N	\N
542	3UGMSKKJBY	11.400.00.00.06-A	KNIEHEBEL, VORNE OBEN	NONE	2018-02-25 07:59:57	\N	\N
543	3UGMSKKJBY	11.400.00.00.05-A	KNIEHEBEL, HINTEN OBEN	NONE	2018-02-25 07:59:57	\N	\N
544	3UGMSKKJBY	11.400.00.00.11	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
545	3UGMSKKJBY	11.401.00.00.04	DISTANZSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
546	3UGMSKKJBY	11.400.00.00.46	DISTANZACHSE	NONE	2018-02-25 07:59:57	\N	\N
547	3UGMSKKJBY	11.401.00.00.07	ACHSHALTER	NONE	2018-02-25 07:59:57	\N	\N
548	3UGMSKKJBY	11.401.00.00.12	DISTANZSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
549	3UGMSKKJBY	12.402.01.00.01	GETRIEBEFLANSCH	NONE	2018-02-25 07:59:57	\N	\N
550	3UGMSKKJBY	11.401.01.01.03-A	LAGERPLATTE	NONE	2018-02-25 07:59:57	\N	\N
551	3UGMSKKJBY	16.402.00.00.02	HALTER FÜR IFM-LICHTSCHRANKE	NONE	2018-02-25 07:59:57	\N	\N
552	3UGMSKKJBY	16.402.01.01.08	WINKEL, VORNE FÜR UMLENKROLLE	NONE	2018-02-25 07:59:57	\N	\N
553	3UGMSKKJBY	16.402.01.01.09	WINKEL, HINTEN FÜR UMLENKROLLE	NONE	2018-02-25 07:59:57	\N	\N
554	3UGMSKKJBY	16.402.02.00.05	EINLAU-ABDECKUNG, OBEN	NONE	2018-02-25 07:59:57	\N	\N
555	3UGMSKKJBY	00.003.00.01.01	KAPPE, WICKELWELLE	NONE	2018-02-25 07:59:57	\N	\N
556	3UGMSKKJBY	14.400.01.01.01	STANGE FÜR WÄRMEKLAPPE	NONE	2018-02-25 07:59:57	\N	\N
557	3UGMSKKJBY	10.401.00.00.18	DISTANZHÜLSE FÜR KABELKANAL	NONE	2018-02-25 07:59:57	\N	\N
558	3UGMSKKJBY	17.402.00.00.04	SCHEIBE, WICKELWELLE	NONE	2018-02-25 07:59:57	\N	\N
559	3UGMSKKJBY	DIN 912 M3X12	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
560	3UGMSKKJBY	DIN 912 M4X10-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
561	3UGMSKKJBY	DIN 912 M4X20-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
562	3UGMSKKJBY	DIN 912 M4X30-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
563	3UGMSKKJBY	DIN 912 M4X25-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
564	3UGMSKKJBY	DIN 912 M4X40-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
565	3UGMSKKJBY	DIN 912 M6X10-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
566	3UGMSKKJBY	DIN 912 M4X6-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
567	3UGMSKKJBY	DIN 912 M6X12-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
568	3UGMSKKJBY	DIN 912 M6X16-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
569	3UGMSKKJBY	DIN 912 M6X20-V2A	ZYLINDERSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
570	3UGMSKKJBY	ISO 7380 M6X12-V2A	RUNDKOPFSCHRAUBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
571	3UGMSKKJBY	DIN 7991 M6X16	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
572	3UGMSKKJBY	DIN 7991 M8X20	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
573	3UGMSKKJBY	DIN 912 M3X25	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
574	3UGMSKKJBY	DIN 912 M6X10	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
575	3UGMSKKJBY	DIN 912 M6X12	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
576	3UGMSKKJBY	DIN 912 M6X16	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
577	3UGMSKKJBY	DIN 912 M6X20	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
578	3UGMSKKJBY	DIN 912 M6X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
579	3UGMSKKJBY	DIN 912 M6X30	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
580	3UGMSKKJBY	DIN 912 M6X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
581	3UGMSKKJBY	DIN 912 M6X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
582	3UGMSKKJBY	DIN 912 M6X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
583	3UGMSKKJBY	DIN 912 M6X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
584	3UGMSKKJBY	DIN 912 M6X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
585	3UGMSKKJBY	DIN 912 M6X70	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
586	3UGMSKKJBY	DIN 912 M6X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
587	3UGMSKKJBY	DIN 912 M6X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
588	3UGMSKKJBY	DIN 912 M6X120	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
589	3UGMSKKJBY	DIN 912 M8X10	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
590	3UGMSKKJBY	DIN 912 M8X12	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
591	3UGMSKKJBY	DIN 912 M8X16	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
592	3UGMSKKJBY	DIN 912 M8X20	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
593	3UGMSKKJBY	DIN 912 M8X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
594	3UGMSKKJBY	DIN 912 M8X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
595	3UGMSKKJBY	DIN 912 M8X30	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
596	3UGMSKKJBY	DIN 912 M8X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
597	3UGMSKKJBY	DIN 912 M8X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
598	3UGMSKKJBY	DIN 912 M8X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
599	3UGMSKKJBY	DIN 912 M8X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
600	3UGMSKKJBY	DIN 912 M8X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
601	3UGMSKKJBY	DIN 912 M8X70	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
602	3UGMSKKJBY	DIN 912 M8X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
603	3UGMSKKJBY	DIN 912 M8X90	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
604	3UGMSKKJBY	DIN 912 M8X140	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
605	3UGMSKKJBY	DIN 912 M10X16	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
606	3UGMSKKJBY	DIN 912 M10X20	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
607	3UGMSKKJBY	DIN 912 M10X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
608	3UGMSKKJBY	DIN 912 M10X30	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
609	3UGMSKKJBY	DIN 912 M10X30 VA	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
610	3UGMSKKJBY	DIN 912 M10X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
611	3UGMSKKJBY	DIN 912 M10X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
612	3UGMSKKJBY	DIN 912 M10X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
613	3UGMSKKJBY	DIN 912 M10X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
614	3UGMSKKJBY	DIN 912 M10X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
615	3UGMSKKJBY	DIN 912 M10X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
616	3UGMSKKJBY	DIN 912 M10X70	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
617	3UGMSKKJBY	DIN 912 M10X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
618	3UGMSKKJBY	DIN 912 M10X90	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
619	3UGMSKKJBY	DIN 912 M10X100	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
620	3UGMSKKJBY	DIN 912 M10X120	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
621	3UGMSKKJBY	DIN 912 M10X140	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
622	3UGMSKKJBY	DIN 912 M12X20	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
623	3UGMSKKJBY	DIN 912 M12X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
624	3UGMSKKJBY	DIN 912 M12X30	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
625	3UGMSKKJBY	DIN 912 M12X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
626	3UGMSKKJBY	DIN 912 M12X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
627	3UGMSKKJBY	DIN 912 M12X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
628	3UGMSKKJBY	DIN 912 M12X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
629	3UGMSKKJBY	DIN 912 M12X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
630	3UGMSKKJBY	DIN 912 M12X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
631	3UGMSKKJBY	DIN 912 M12X70	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
632	3UGMSKKJBY	DIN 912 M12X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
633	3UGMSKKJBY	DIN 912 M12X100	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
634	3UGMSKKJBY	DIN 912 M12X110	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
635	3UGMSKKJBY	DIN 912 M12X120	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
636	3UGMSKKJBY	DIN 912 M16X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
637	3UGMSKKJBY	DIN 912 M10X12	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
638	3UGMSKKJBY	DIN 912 M20X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
639	3UGMSKKJBY	DIN 912 M20X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
640	3UGMSKKJBY	DIN 912 M16X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
641	3UGMSKKJBY	DIN 912 M20X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
642	3UGMSKKJBY	DIN 912 M20X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
643	3UGMSKKJBY	DIN 912 M20X90	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
644	3UGMSKKJBY	DIN 912 M16X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
645	3UGMSKKJBY	DIN 912 M24X65	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
646	3UGMSKKJBY	DIN 912 M24X100	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
647	3UGMSKKJBY	DIN 912 M14X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
648	3UGMSKKJBY	DIN 7380 M6X8	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
649	3UGMSKKJBY	DIN 7380 M6X12	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
650	3UGMSKKJBY	DIN 7380 M6X16	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
651	3UGMSKKJBY	DIN 7380 M6X20	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
652	3UGMSKKJBY	DIN 7380 M6X30	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
653	3UGMSKKJBY	DIN 7991 M5X10	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
654	3UGMSKKJBY	DIN 7991 M5X80	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
655	3UGMSKKJBY	DIN 7991 M6X10	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
656	3UGMSKKJBY	DIN 7991 M6X20	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
657	3UGMSKKJBY	DIN 7991 M6X12	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
658	3UGMSKKJBY	DIN 7991 M6X25	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
659	3UGMSKKJBY	DIN 7991 M6X30	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
660	3UGMSKKJBY	DIN 7991 M8X25	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
661	3UGMSKKJBY	DIN 7991 M8X60	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
662	3UGMSKKJBY	DIN 7380 M8X12	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
663	3UGMSKKJBY	DIN 6912 M6X12	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
664	3UGMSKKJBY	DIN 6912 M6X30	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
665	3UGMSKKJBY	DIN 6912 M10X20	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
666	3UGMSKKJBY	DIN 6912 M8X20	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
667	3UGMSKKJBY	DIN 6912 M10X25	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
668	3UGMSKKJBY	DIN 6912 M10X30	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
669	3UGMSKKJBY	DIN 7991 M5X16	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
670	3UGMSKKJBY	DIN 7991 M4X30	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
671	3UGMSKKJBY	DIN 7991 M8X30	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
672	3UGMSKKJBY	DIN 7991 M10X25	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
673	3UGMSKKJBY	DIN 7991 M12X20	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
674	3UGMSKKJBY	DIN 7991 M12X25	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
675	3UGMSKKJBY	DIN 912 M4X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
676	3UGMSKKJBY	DIN 912 M4X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
677	3UGMSKKJBY	DIN 912 M4X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
678	3UGMSKKJBY	DIN 912 M4X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
679	3UGMSKKJBY	DIN 912 M4X90	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
680	3UGMSKKJBY	DIN 912 M4X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
681	3UGMSKKJBY	DIN 912 M5X10	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
682	3UGMSKKJBY	DIN 912 M5X12	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
683	3UGMSKKJBY	DIN 912 M5X16	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
684	3UGMSKKJBY	DIN 912 M5X25	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
685	3UGMSKKJBY	DIN 912 M5X30	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
686	3UGMSKKJBY	DIN 912 M5X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
687	3UGMSKKJBY	DIN 912 M5X40	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
688	3UGMSKKJBY	DIN 912 M5X35	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
689	3UGMSKKJBY	DIN 912 M5X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
690	3UGMSKKJBY	DIN 912 M5X60	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
691	3UGMSKKJBY	DIN 912 M5X60 VA	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
692	3UGMSKKJBY	DIN 912 M5X70	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
693	3UGMSKKJBY	DIN 912 M5X130	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
694	3UGMSKKJBY	14.401.08.00.04	GEWINDESTANGE M8,LÄNGE=140MM	NONE	2018-02-25 07:59:57	\N	\N
695	3UGMSKKJBY	D-M9PL	ELEKTRONISCHER SIGNALGEBER	NONE	2018-02-25 07:59:57	\N	\N
696	3UGMSKKJBY	CDQ2A40TF-25DZ	KOMPAKTZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
697	3UGMSKKJBY	SY5320-5DZD-01F-Q	5-3-WEGE VENTIL	NONE	2018-02-25 07:59:57	\N	\N
698	3UGMSKKJBY	AS2201F-02-06SA	DROSSELRÜCKSCHLAGVENTIL	NONE	2018-02-25 07:59:57	\N	\N
699	3UGMSKKJBY	AS3201F-03-10SA	DROSSELRÜCKSCHLAGVENTIL, EINSCHRAUBBAR	NONE	2018-02-25 07:59:57	\N	\N
700	3UGMSKKJBY	AR20-F02G-A	DRUCKREGLER	NONE	2018-02-25 07:59:57	\N	\N
701	3UGMSKKJBY	AS2002F-06	DROSSELRÜCKSCHLAGVENTIL	NONE	2018-02-25 07:59:57	\N	\N
702	3UGMSKKJBY	KQ2L06-03AS	WINKELSTECKVERSCHRAUBUNG	NONE	2018-02-25 07:59:57	\N	\N
703	3UGMSKKJBY	VHS20-F02A-S	3-WEGE-HANDABSPERRVENTIL	NONE	2018-02-25 07:59:57	\N	\N
704	3UGMSKKJBY	Y200T-A	ZWISCHENSTÜCK MIT BEFESTIGUNGSELEMENT	NONE	2018-02-25 07:59:57	\N	\N
705	3UGMSKKJBY	AN20-02	SCHALLDÄMPFER	NONE	2018-02-25 07:59:57	\N	\N
706	3UGMSKKJBY	ASP630F-F04-10S	DROSSELRÜCKSCHLAGVENTIL, ENTSPERRBAR	NONE	2018-02-25 07:59:57	\N	\N
707	3UGMSKKJBY	CP96SDB100-400C	ISO-ZYLINDER (ISO 15552)	NONE	2018-02-25 07:59:57	\N	\N
708	3UGMSKKJBY	CD85N25-10-B	ISO-ZYLINDER (ISO 6432)	NONE	2018-02-25 07:59:57	\N	\N
709	3UGMSKKJBY	CS5100	BEFESTIGUNGSELEMENT	NONE	2018-02-25 07:59:57	\N	\N
710	3UGMSKKJBY	GKM20-40	GABELGELENK	NONE	2018-02-25 07:59:57	\N	\N
711	3UGMSKKJBY	KQ2L08-01AS	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
712	3UGMSKKJBY	KQ2L08-02AS	WINKEL-STECKVERSCHRAUBUNG	NONE	2018-02-25 07:59:57	\N	\N
713	3UGMSKKJBY	KQ2T08-00A	T-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
714	3UGMSKKJBY	KQ2L10-02AS	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
715	3UGMSKKJBY	CDQMA50TF-10	KOMPAKTZYLINDER MIT FÜHRUNGEN	NONE	2018-02-25 07:59:57	\N	\N
716	3UGMSKKJBY	KQ2L06-01AS	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
717	3UGMSKKJBY	KQ2L06-02AS	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
718	3UGMSKKJBY	ASV510F-02-10S	DROSSEL-SCHNELLENTLÜFTUNGSVENTIL	NONE	2018-02-25 07:59:57	\N	\N
719	3UGMSKKJBY	ANB1-12	HOCHLEISTUNGSSCHALLDÄMPFER	NONE	2018-02-25 07:59:57	\N	\N
720	3UGMSKKJBY	KQ2L10-01AS	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
721	3UGMSKKJBY	KQ2S06-01AS	STECKVERSCHRAUBUNG MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
722	3UGMSKKJBY	KK6P-12H	S-KUPPLUNG, STECKER	NONE	2018-02-25 07:59:57	\N	\N
723	3UGMSKKJBY	KQ2S10-01AS	STECKVERSCHRAUBUNG MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
724	3UGMSKKJBY	KK6S-04MS	S-KUPPLUNG BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
725	3UGMSKKJBY	MGPM63TF-140Z-XB10	KOMPAKTZYLINDER MIT FÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
726	3UGMSKKJBY	VP3165-105DZA1-Q	3-2-WEGE VENTIL	NONE	2018-02-25 07:59:57	\N	\N
727	3UGMSKKJBY	GA36-10-01	MANOMETER	NONE	2018-02-25 07:59:57	\N	\N
728	3UGMSKKJBY	KQ2VF06-01AS	EINSCHRAUBWINKEL MIT INNENGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
729	3UGMSKKJBY	VHK3-06F-06FL	3_2-WEGE HANDABSPERRVENTIL	NONE	2018-02-25 07:59:57	\N	\N
730	3UGMSKKJBY	AN10-01	SCHALLDÄMPFER	NONE	2018-02-25 07:59:57	\N	\N
731	3UGMSKKJBY	KQ2S06-02AS	STECKVERSCHRAUBUNG-GERADE MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
732	3UGMSKKJBY	AS3002F-10	DROSSELRÜCKSCHLAGVENTIL	NONE	2018-02-25 07:59:57	\N	\N
733	3UGMSKKJBY	161820	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
734	3UGMSKKJBY	KQ2S10-02AS	STECKVERSCHRAUBUNG-GERADE MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
735	3UGMSKKJBY	KQ2S10-04AS	STECKVERSCHRAUBUNG-GERADE MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
736	3UGMSKKJBY	KQ2S12-04AS	STECKVERSCHRAUBUNG-GERADE MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
737	3UGMSKKJBY	KQ2T06-00A	STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
738	3UGMSKKJBY	KQ2T10-00A	T-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
739	3UGMSKKJBY	VF3133-5YZD1-02F	5-2-WEGE VENTIL	NONE	2018-02-25 07:59:57	\N	\N
740	3UGMSKKJBY	KQ2T12-10A	T-STECKVERBINDER	NONE	2018-02-25 07:59:57	\N	\N
741	3UGMSKKJBY	IFS244	INDUKTIVER SENSOR	NONE	2018-02-25 07:59:57	\N	\N
742	3UGMSKKJBY	IF5904	INDUKTIVER SENSOR	NONE	2018-02-25 07:59:57	\N	\N
743	3UGMSKKJBY	GN 471.1-50-V8	HANDKURBEL	NONE	2018-02-25 07:59:57	\N	\N
744	3UGMSKKJBY	GN 319.2-50-M12-A	DREHBARER KUGELKNOPF	NONE	2018-02-25 07:59:57	\N	\N
745	3UGMSKKJBY	DIN 6335-KU-63-M12-K	KREUZGRIFF	NONE	2018-02-25 07:59:57	\N	\N
746	3UGMSKKJBY	BCW-POD-080-R	BOCKROLLE	NONE	2018-02-25 07:59:57	\N	\N
747	3UGMSKKJBY	LCW-POD-080-R	LENKROLLE	NONE	2018-02-25 07:59:57	\N	\N
748	3UGMSKKJBY	20246002000	ROTEX24 - NABE 1.0 AL-D Ø20 MIT PASSFEDERNUT UND FESTSTELLGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
749	3UGMSKKJBY	20241000002	ROTEX 24 ZAHNKRANZ 98 SH-A	NONE	2018-02-25 07:59:57	\N	\N
750	3UGMSKKJBY	ROTEX GS28 SPANNRINGNABE 32H7 KUPPLUNG	ROTEX GS28 SPANNRINGNABE Ø32H7	NONE	2018-02-25 07:59:57	\N	\N
751	3UGMSKKJBY	550287900040	DKM MITTELSTÜCK	NONE	2018-02-25 07:59:57	\N	\N
752	3UGMSKKJBY	4012802794342	LFR5201-10-2Z	NONE	2018-02-25 07:59:57	\N	\N
753	3UGMSKKJBY	ISO 4032 M6-V2A	SECHSKANTMUTTER, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
754	3UGMSKKJBY	ISO 4032 M4-V2A	SECHSKANTMUTTER, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
755	3UGMSKKJBY	ISO 4032 M5	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
756	3UGMSKKJBY	ISO 4032 M10	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
757	3UGMSKKJBY	ISO 4032 M3	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
758	3UGMSKKJBY	ISO 4032 M12	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
759	3UGMSKKJBY	ISO 4032 M6	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
760	3UGMSKKJBY	ISO 4032 M8	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
761	3UGMSKKJBY	ISO 4032 M16	SECHSKANTMUTTER	NONE	2018-02-25 07:59:57	\N	\N
762	3UGMSKKJBY	DIN 125-A 21	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
763	3UGMSKKJBY	DIN 125-A 8.4	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
764	3UGMSKKJBY	DIN 125-A 10.5	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
765	3UGMSKKJBY	DIN 125-A 4.3-V2A	SCHEIBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
766	3UGMSKKJBY	DIN 125-A 6.4-V2A	SCHEIBE, NICHTROSTEND	NONE	2018-02-25 07:59:57	\N	\N
767	3UGMSKKJBY	DIN 125-A 6.4	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
768	3UGMSKKJBY	DIN 125-A 13	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
769	3UGMSKKJBY	DIN 125-A 4.3	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
770	3UGMSKKJBY	DIN 125-A 5.3	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
771	3UGMSKKJBY	Z81-9-R1_4	WZ ANSCHLUSS	NONE	2018-02-25 07:59:57	\N	\N
772	3UGMSKKJBY	11.401.00.00.03-A	KURBELBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
773	3UGMSKKJBY	DIN 913 M6X6	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
774	3UGMSKKJBY	DIN 913 M6X8	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
775	3UGMSKKJBY	DIN 913 M12X20	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
776	3UGMSKKJBY	DIN 913 M16X25	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
777	3UGMSKKJBY	DIN 913 M6X12	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
778	3UGMSKKJBY	DIN 913 M10X12	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
779	3UGMSKKJBY	DIN 906 R 3_8	VERSCHLUSSSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
780	3UGMSKKJBY	DIN 913 M8X16	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
781	3UGMSKKJBY	DIN 913 M8X12	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
782	3UGMSKKJBY	DIN 906 R 1_8	VERSCHLUSSSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
783	3UGMSKKJBY	DIN 913 M8X10	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
784	3UGMSKKJBY	DIN 913 M20X20	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
785	3UGMSKKJBY	DIN 906 R 1_2	VERSCHLUSSSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
786	3UGMSKKJBY	DIN 913 M12X25	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
787	3UGMSKKJBY	DIN 913 M8X8	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
788	3UGMSKKJBY	DIN 906 R 1_4	VERSCHLUSSSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
789	3UGMSKKJBY	DIN 913 M16X20	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
790	3UGMSKKJBY	DIN 913 M12X16	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
791	3UGMSKKJBY	DIN 913 M8X20	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
792	3UGMSKKJBY	DIN 913 M10X10	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
793	3UGMSKKJBY	DIN 913 M6X20	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
794	3UGMSKKJBY	DIN 913 M8X6	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
795	3UGMSKKJBY	DIN 913 M6X10	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
796	3UGMSKKJBY	DIN 913 M6X16	GEWINDESTIFTE MIT KEGELKUPPE - MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
797	3UGMSKKJBY	101215	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
798	3UGMSKKJBY	121412	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
799	3UGMSKKJBY	121415	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
800	3UGMSKKJBY	121420	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
801	3UGMSKKJBY	141620	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
802	3UGMSKKJBY	151720	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
803	3UGMSKKJBY	4012802721706	NKS25	NONE	2018-02-25 07:59:57	\N	\N
804	3UGMSKKJBY	4012802838985	PASE25-N	NONE	2018-02-25 07:59:57	\N	\N
805	3UGMSKKJBY	16003-2RSR	RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
806	3UGMSKKJBY	4012801053396	6003.2RSR	NONE	2018-02-25 07:59:57	\N	\N
807	3UGMSKKJBY	4012802384598	6004-C-2HRS,RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
808	3UGMSKKJBY	4012801068178	DIN 625-6009.2RSR	NONE	2018-02-25 07:59:57	\N	\N
809	3UGMSKKJBY	4012801076104	DIN 625 6012-2RSR RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
810	3UGMSKKJBY	4047643293655	DIN 625 6208-C-2HRS RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
811	3UGMSKKJBY	4012801078016	DIN 625-6210-2RSR	NONE	2018-02-25 07:59:57	\N	\N
812	3UGMSKKJBY	GN 617-6-AK	RASTBOLZEN	NONE	2018-02-25 07:59:57	\N	\N
813	3UGMSKKJBY	GN 851-700-T2	VERSCHLUSS-SPANNER	NONE	2018-02-25 07:59:57	\N	\N
814	3UGMSKKJBY	161825	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
815	3UGMSKKJBY	202220	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
816	3UGMSKKJBY	202225	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
817	3UGMSKKJBY	202325	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
818	3UGMSKKJBY	202330	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
819	3UGMSKKJBY	222520	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
820	3UGMSKKJBY	242825	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
821	3UGMSKKJBY	252825	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
822	3UGMSKKJBY	303430	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
823	3UGMSKKJBY	404425	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
824	3UGMSKKJBY	404440	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
825	3UGMSKKJBY	60808	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
826	3UGMSKKJBY	60810	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
827	3UGMSKKJBY	81010	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
828	3UGMSKKJBY	SIB22X28X30	BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
829	3UGMSKKJBY	SIB20X26X25	BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
830	3UGMSKKJBY	SIB16X22X25	BUCHSE	NONE	2018-02-25 07:59:57	\N	\N
831	3UGMSKKJBY	DIN 1481 8X50	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
832	3UGMSKKJBY	DIN 1481 6X30	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
833	3UGMSKKJBY	DIN 1481 6X24	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
834	3UGMSKKJBY	DIN 1481 5X26	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
835	3UGMSKKJBY	DIN 1481 5X24	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
836	3UGMSKKJBY	DIN 1481 4X18	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
837	3UGMSKKJBY	DIN 1481 3X20	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
838	3UGMSKKJBY	DIN 1481 3X16	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
839	3UGMSKKJBY	DIN 1481 12X30	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
840	3UGMSKKJBY	DIN 1481 10X36	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
841	3UGMSKKJBY	DIN 912 M5X10-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
842	3UGMSKKJBY	DIN 912 M5X16-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
843	3UGMSKKJBY	DIN 912 M6X55-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
844	3UGMSKKJBY	DIN 912 M12X25-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
845	3UGMSKKJBY	DIN 912 M6X25-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
846	3UGMSKKJBY	DIN 912 M8X25-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
847	3UGMSKKJBY	DIN 912 M6X8-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
848	3UGMSKKJBY	DIN 912 M5X30-V2A	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
849	3UGMSKKJBY	DIN 7380 M6X16-V2A	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
850	3UGMSKKJBY	DIN 7380 M6X20-V2A	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
851	3UGMSKKJBY	DIN 7380 M6X40-V2A	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
852	3UGMSKKJBY	DIN 7380 M5X16-V2A	FLACHKOPFSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
853	3UGMSKKJBY	DIN 7991 M6X16-V2A	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
854	3UGMSKKJBY	DIN 7991 M12X20-V2A	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
855	3UGMSKKJBY	DIN 7991 M8X16-V2A	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
856	3UGMSKKJBY	DIN 7991 M6X20-V2A	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
857	3UGMSKKJBY	DIN 7991 M4X30-V2A	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
858	3UGMSKKJBY	O-RING 104X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
859	3UGMSKKJBY	O-RING 108X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
860	3UGMSKKJBY	O-RING 10X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
861	3UGMSKKJBY	O-RING 110X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
862	3UGMSKKJBY	O-RING 11X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
863	3UGMSKKJBY	O-RING 122X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
864	3UGMSKKJBY	O-RING 12X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
865	3UGMSKKJBY	O-RING 12X3	O-RING	NONE	2018-02-25 07:59:57	\N	\N
866	3UGMSKKJBY	O-RING 13X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
867	3UGMSKKJBY	O-RING 140X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
868	3UGMSKKJBY	O-RING 15X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
869	3UGMSKKJBY	O-RING 15X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
870	3UGMSKKJBY	O-RING 22X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
871	3UGMSKKJBY	O-RING 25X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
872	3UGMSKKJBY	O-RING 30X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
873	3UGMSKKJBY	O-RING 30X3	O-RING	NONE	2018-02-25 07:59:57	\N	\N
874	3UGMSKKJBY	O-RING 34X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
875	3UGMSKKJBY	O-RING 35X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
876	3UGMSKKJBY	O-RING 44X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
877	3UGMSKKJBY	O-RING 45X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
878	3UGMSKKJBY	O-RING 52X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
879	3UGMSKKJBY	O-RING 52X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
880	3UGMSKKJBY	O-RING 57X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
881	3UGMSKKJBY	O-RING 5X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
882	3UGMSKKJBY	O-RING 63X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
883	3UGMSKKJBY	O-RING 65X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
884	3UGMSKKJBY	O-RING 69X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
885	3UGMSKKJBY	O-RING 6X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
886	3UGMSKKJBY	O-RING 6X3	O-RING	NONE	2018-02-25 07:59:57	\N	\N
887	3UGMSKKJBY	O-RING 70X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
888	3UGMSKKJBY	O-RING 77X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
889	3UGMSKKJBY	O-RING 80X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
890	3UGMSKKJBY	O-RING 82X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
891	3UGMSKKJBY	O-RING 84X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
892	3UGMSKKJBY	O-RING 8X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
893	3UGMSKKJBY	O-RING 8X3	O-RING	NONE	2018-02-25 07:59:57	\N	\N
894	3UGMSKKJBY	O-RING 90X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
895	3UGMSKKJBY	O-RING 98X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
896	3UGMSKKJBY	O-RING 9X1.5	O-RING	NONE	2018-02-25 07:59:57	\N	\N
897	3UGMSKKJBY	Q-RING 10.20X2.62	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
898	3UGMSKKJBY	Q-RING 12.37X2.62	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
899	3UGMSKKJBY	Q-RING 14.70X2.72	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
900	3UGMSKKJBY	Q-RING 15.47X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
901	3UGMSKKJBY	202320	TEFLONBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
902	3UGMSKKJBY	DIN 6885 A8X7X60	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
903	3UGMSKKJBY	DIN 6885 A8X7X28	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
904	3UGMSKKJBY	DIN 6885 A8X7X20	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
905	3UGMSKKJBY	DIN 6885 A6X6X28	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
906	3UGMSKKJBY	DIN 6885 A6X6X25	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
907	3UGMSKKJBY	DIN 6885 A5X5X12	PASSFEDER	NONE	2018-02-25 07:59:57	\N	\N
908	3UGMSKKJBY	Q-RING 18.72X2.62	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
909	3UGMSKKJBY	Q-RING 20.22X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
910	3UGMSKKJBY	Q-RING 34.52X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
911	3UGMSKKJBY	Q-RING 37.59X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
912	3UGMSKKJBY	Q-RING 72.62X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
913	3UGMSKKJBY	Q-RING 91.67X3.53	QUADRAD-RING	NONE	2018-02-25 07:59:57	\N	\N
914	3UGMSKKJBY	DIN 471 12X1	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
915	3UGMSKKJBY	DIN 471 45X2.5	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
916	3UGMSKKJBY	DIN 912 M8X100	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
917	3UGMSKKJBY	DIN 912 M20X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
918	3UGMSKKJBY	DIN 912 M8X95	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
919	3UGMSKKJBY	DIN 912 M12X75	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
920	3UGMSKKJBY	DIN 912 M14X45	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
921	3UGMSKKJBY	DIN 912 M4X16	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
922	3UGMSKKJBY	DIN 7991 M5X20	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
923	3UGMSKKJBY	DIN 128-A 4-V2A	FEDERRINGE FORM A	NONE	2018-02-25 07:59:57	\N	\N
924	3UGMSKKJBY	DIN 128-A 6-V2A	FEDERRINGE FORM A	NONE	2018-02-25 07:59:57	\N	\N
925	3UGMSKKJBY	DIN 6336-KU-63-M12-K	STERNGRIFF	NONE	2018-02-25 07:59:57	\N	\N
926	3UGMSKKJBY	GT 1025 MS	GEWINDETÜLLE	NONE	2018-02-25 07:59:57	\N	\N
927	3UGMSKKJBY	Z82-9-R1_4	SCHNELLVERSCHLUSS	NONE	2018-02-25 07:59:57	\N	\N
928	3UGMSKKJBY	Z811-9-R1_4	VERSCHLUSSNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
929	3UGMSKKJBY	LFZ12	ZAPFEN-ZENTRISCH	NONE	2018-02-25 07:59:57	\N	\N
930	3UGMSKKJBY	226.03-10.0	SICHERHEITSVENTIL	NONE	2018-02-25 07:59:57	\N	\N
931	3UGMSKKJBY	22.403.00.05.04	HALTEWINKEL-ADAM FOLIENEINLAUF	NONE	2018-02-25 07:59:57	\N	\N
932	3UGMSKKJBY	22.403.00.05.01	SCHEIBENHALTER-EINLAUF VORNE	NONE	2018-02-25 07:59:57	\N	\N
933	3UGMSKKJBY	22.403.00.05.02	SCHEIBENHALTER-EINLAUF HINTEN	NONE	2018-02-25 07:59:57	\N	\N
934	3UGMSKKJBY	7350024452037	2TLA020051R0000-ADAM	NONE	2018-02-25 07:59:57	\N	\N
935	3UGMSKKJBY	7350024460087	EMERGNECY STOP-SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
936	3UGMSKKJBY	DIN 912 M12X150	ZYLINDERSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
937	3UGMSKKJBY	VS 20 ST	VERSCHLUSSSTOPFEN	NONE	2018-02-25 07:59:57	\N	\N
938	3UGMSKKJBY	VS 14 ST	VERSCHLUSSSTOPFEN	NONE	2018-02-25 07:59:57	\N	\N
939	3UGMSKKJBY	DIN 625 - 6016-2RS	RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
940	3UGMSKKJBY	DIN EN-ISO 7042 M6	SICHERUNGSMUTTER	NONE	2018-02-25 07:59:57	\N	\N
941	3UGMSKKJBY	DIN 7991 M4X40	SENKSCHRAUBEN MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
942	3UGMSKKJBY	DIN 9021 8.4	SCHEIBE-AUSSENDURCHMESSER-3X-SCHRAUBENDURCHMESSER	NONE	2018-02-25 07:59:57	\N	\N
943	3UGMSKKJBY	WE 114 ST	"EINSCHRAUBWINKEL 90° MIT INNEN-UND AUSSENGEWINDE-R1 1/4"""	NONE	2018-02-25 07:59:57	\N	\N
944	3UGMSKKJBY	WE 10 ST	VERSCHLUSSSTOPFEN	NONE	2018-02-25 07:59:57	\N	\N
945	3UGMSKKJBY	DN 1010 ST	"DOPPELNIPPEL-R1"""	NONE	2018-02-25 07:59:57	\N	\N
946	3UGMSKKJBY	GT 189 MS	GEWINDETÜLLE	NONE	2018-02-25 07:59:57	\N	\N
947	3UGMSKKJBY	VF5123-5YZD1-02F	5-2-WEGE VENTIL	NONE	2018-02-25 07:59:57	\N	\N
948	3UGMSKKJBY	DIN 71412 M10X1-SW11	SCHMIERNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
949	3UGMSKKJBY	DIN 440-R-6.6	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
950	3UGMSKKJBY	DIN 9021 6.4	SCHEIBE-AUSSENDURCHMESSER-3X-SCHRAUBENDURCHMESSER	NONE	2018-02-25 07:59:57	\N	\N
951	3UGMSKKJBY	DIN 912 M16X90	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
952	3UGMSKKJBY	DIN 912 M16X80	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
953	3UGMSKKJBY	KQ2R08-10A	STECKVERBINDUNG, RED	NONE	2018-02-25 07:59:57	\N	\N
954	3UGMSKKJBY	IF300-06-00-Q	DURCHFLUSSMESSGERÄT	NONE	2018-02-25 07:59:57	\N	\N
955	3UGMSKKJBY	7350024452372	M12-C101-10M-CABLE 5X0.34	NONE	2018-02-25 07:59:57	\N	\N
956	3UGMSKKJBY	VXD252LGA	WASSER-VENTIL	NONE	2018-02-25 07:59:57	\N	\N
957	3UGMSKKJBY	14.400.01.00.11	LEISTE-FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
958	3UGMSKKJBY	14.400.01.00.01-B	LEISTE-FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
959	3UGMSKKJBY	14.400.01.00.02-B	DECKLEISTE-FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
960	3UGMSKKJBY	14.400.01.00.10	FÜHRUNGSLEISTE-FOLIENFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
961	3UGMSKKJBY	GFM121417	TEFLONBUCHSE-MIT-RING	NONE	2018-02-25 07:59:57	\N	\N
962	3UGMSKKJBY	4030777010038	GEWINDE-DICHTUNGPASTE	NONE	2018-02-25 07:59:57	\N	\N
963	3UGMSKKJBY	4-AM-ARV-119	DRUCKLUFTMOTOR	NONE	2018-02-25 07:59:57	\N	\N
964	3UGMSKKJBY	TK-28-320	3-2-WEGE VENTIL	NONE	2018-02-25 07:59:57	\N	\N
965	3UGMSKKJBY	DIN 71412 M6X1-SW7	SCHMIERNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
966	3UGMSKKJBY	11.400.00.00.57	HALTEBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
967	3UGMSKKJBY	DIN 8187-1_2X3_16	ROLLENKETTE-178 GLIEDER-INKL SCHLOSS	NONE	2018-02-25 07:59:57	\N	\N
968	3UGMSKKJBY	8000-016-024	SPANNSATZ-BIKON	NONE	2018-02-25 07:59:57	\N	\N
969	3UGMSKKJBY	VS 10 ST	VERSCHLUSSSTOPFEN	NONE	2018-02-25 07:59:57	\N	\N
970	3UGMSKKJBY	Z80-9	SCHNELLVERSCHLUSS-ONHEGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
971	3UGMSKKJBY	DIN 912 M5X115	ZYLINDERSCHRAUBEN, M	NONE	2018-02-25 07:59:57	\N	\N
972	3UGMSKKJBY	7260406651672	MSK071D-0300-NN-M3-UG1-NNNN	NONE	2018-02-25 07:59:57	\N	\N
973	3UGMSKKJBY	TAPER-1210-32	TAPER SPANNBUCHSE 1210-Ø32	NONE	2018-02-25 07:59:57	\N	\N
974	3UGMSKKJBY	8000-025-034	SPANNSATZ	NONE	2018-02-25 07:59:57	\N	\N
975	3UGMSKKJBY	FND453	FREILAUF	NONE	2018-02-25 07:59:57	\N	\N
976	3UGMSKKJBY	4026677804839	22210-E1-PENDELROLLENLAGER	NONE	2018-02-25 07:59:57	\N	\N
977	3UGMSKKJBY	4204-2RS	RILLENKUGELLAGER, ZW	NONE	2018-02-25 07:59:57	\N	\N
978	3UGMSKKJBY	DIN 471 10X1	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
979	3UGMSKKJBY	DIN 471 14X1	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
980	3UGMSKKJBY	DIN 471 17X1	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
981	3UGMSKKJBY	DIN 471 20X1.2	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
982	3UGMSKKJBY	DIN 471 30X1.5	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
983	3UGMSKKJBY	DIN 471 40X1.75	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
984	3UGMSKKJBY	DIN 471 45X1.75	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
985	3UGMSKKJBY	DIN 472 58X2	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
986	3UGMSKKJBY	DIN 472 80X2.5	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
987	3UGMSKKJBY	DIN 472 90X3	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
988	3UGMSKKJBY	DIN 472 100X3	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
989	3UGMSKKJBY	DIN 705-A 20	STELLRING	NONE	2018-02-25 07:59:57	\N	\N
990	3UGMSKKJBY	DIN 472 110X4	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
991	3UGMSKKJBY	HTD 800-8M-30	ZAHNRIEMEN-BELT	NONE	2018-02-25 07:59:57	\N	\N
992	3UGMSKKJBY	8059265761371	SPANNHEBEL	NONE	2018-02-25 07:59:57	\N	\N
993	3UGMSKKJBY	7350024452471	M12-C103-10M-CABLE 8X0.34 FEMALE	NONE	2018-02-25 07:59:57	\N	\N
994	3UGMSKKJBY	KAR40103	KARASTO GEKA PLUS-SCHLAUCHST-3_4-19MM-MS	NONE	2018-02-25 07:59:57	\N	\N
995	3UGMSKKJBY	KAR76-3SK	KARASTO1_3-VERSCHRAUBUNG-SAUGER AG G3_4-19MM	NONE	2018-02-25 07:59:57	\N	\N
996	3UGMSKKJBY	2116INOX	SCHLAUCHSCHELLEN IDEAL EDELSTAHL NR.16-SPANNBEREICH:10-16	NONE	2018-02-25 07:59:57	\N	\N
997	3UGMSKKJBY	2132INOX	SCHLAUCHSCHELLEN IDEAL EDELSTAHL NR.32 SPANNBEREICH:20-32	NONE	2018-02-25 07:59:57	\N	\N
1083	3UGMSKKJBY	LFS52-726MM	INA-LAUFROLLENTRAGSCHIENE	NONE	2018-02-25 07:59:57	\N	\N
998	3UGMSKKJBY	2140INOX	SCHLAUCHSCHELLEN IDEAL EDELSTAHL NR.40 SPANNBEREICH:25-40	NONE	2018-02-25 07:59:57	\N	\N
999	3UGMSKKJBY	DIN 9021 4.3	SCHEIBE-AUSSENDURCHMESSER-3X-SCHRAUBENDURCHMESSER	NONE	2018-02-25 07:59:57	\N	\N
1000	3UGMSKKJBY	DIN 9021 10.5	SCHEIBE-AUSSENDURCHMESSER-3X-SCHRAUBENDURCHMESSER	NONE	2018-02-25 07:59:57	\N	\N
1001	3UGMSKKJBY	DIN 128-A M6	FEDERRING	NONE	2018-02-25 07:59:57	\N	\N
1002	3UGMSKKJBY	DIN 128-A M8	FEDERRING	NONE	2018-02-25 07:59:57	\N	\N
1003	3UGMSKKJBY	DIN 128-A M10	FEDERRING	NONE	2018-02-25 07:59:57	\N	\N
1004	3UGMSKKJBY	DIN 128-A M12	FEDERRING	NONE	2018-02-25 07:59:57	\N	\N
1005	3UGMSKKJBY	DIN 128-A M16	FEDERRING	NONE	2018-02-25 07:59:57	\N	\N
1006	3UGMSKKJBY	22.403.02.00.11	HALTEWINKEL-TÜR FOLIENAUSLAUF	NONE	2018-02-25 07:59:57	\N	\N
1007	3UGMSKKJBY	00.105.05.04.03	DECKBLECH-FOLIENTRANSPORTANTRIEB	NONE	2018-02-25 07:59:57	\N	\N
1008	3UGMSKKJBY	10.401.01.00.01-A	BLECH FÜR RAHMEN	NONE	2018-02-25 07:59:57	\N	\N
1009	3UGMSKKJBY	14.400.00.01.00-A	EINLAUFTISCH	NONE	2018-02-25 07:59:57	\N	\N
1010	3UGMSKKJBY	PN1655305-R2-WO1603222	ULTRASENSOR	NONE	2018-02-25 07:59:57	\N	\N
1011	3UGMSKKJBY	14.401.00.01.00	WAGNE-VORNE	NONE	2018-02-25 07:59:57	\N	\N
1012	3UGMSKKJBY	14.401.00.02.00	WAGNE HINTEN	NONE	2018-02-25 07:59:57	\N	\N
1013	3UGMSKKJBY	16.402.02.00.04-A	EINLAUF-ABDECKUNG-UNTEN	NONE	2018-02-25 07:59:57	\N	\N
1014	3UGMSKKJBY	18.400.03.00.01-A	STRAHLERBLECH, OBERHEIZUNG	NONE	2018-02-25 07:59:57	\N	\N
1015	3UGMSKKJBY	14.401.08.00.01	OBERES DECKBLECH WÄRMETUNNEL	NONE	2018-02-25 07:59:57	\N	\N
1016	3UGMSKKJBY	MZA70175	PNEUMATIKZYLINDERSENSOR	NONE	2018-02-25 07:59:57	\N	\N
1017	3UGMSKKJBY	14.401.08.00.02	UNTERES DECKBLECH WÄRMETUNNEL	NONE	2018-02-25 07:59:57	\N	\N
1018	3UGMSKKJBY	SMX-VERTEILER	VERTEILER	NONE	2018-02-25 07:59:57	\N	\N
1019	3UGMSKKJBY	1003-020-047	SPANNSATZ	NONE	2018-02-25 07:59:57	\N	\N
1020	3UGMSKKJBY	14.400.00.06.00-C	SPINDELLAGER-FOLIENTRANSPORT	NONE	2018-02-25 07:59:57	\N	\N
1021	3UGMSKKJBY	ISO 4014 M12X120	SECHSKANTSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1022	3UGMSKKJBY	ISO 7379 20X40	SCHULTERPASSSCHRAUBE-Z38_20X40	NONE	2018-02-25 07:59:57	\N	\N
1023	3UGMSKKJBY	DIN 7991 M12X30	SENKSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1024	3UGMSKKJBY	EFFBE LM 3-33	LEVELMOUNT MASCHINEN	NONE	2018-02-25 07:59:57	\N	\N
1025	3UGMSKKJBY	160236-CRVZS-2	DRUCKLUFTSPEICHER	NONE	2018-02-25 07:59:57	\N	\N
1026	3UGMSKKJBY	KL2 EG 04-202-024	DRUCKLUFTTANK 90L	NONE	2018-02-25 07:59:57	\N	\N
1027	3UGMSKKJBY	AE 1055.500	KOMPAKTSCHALTSCHRANK	NONE	2018-02-25 07:59:57	\N	\N
1028	3UGMSKKJBY	DN 1212 ST	DOPPELNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
1029	3UGMSKKJBY	KH 12	KUGELHAHN	NONE	2018-02-25 07:59:57	\N	\N
1030	3UGMSKKJBY	RN 20114 K ST	REDUZIERNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
1031	3UGMSKKJBY	RN 11410 K ST	REDUZIERNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
1032	3UGMSKKJBY	WE 12 ST	EINSCHRAUBWINKEL 90°	NONE	2018-02-25 07:59:57	\N	\N
1033	3UGMSKKJBY	SS 40 ES	SCHLAUCHSCHELLEN	NONE	2018-02-25 07:59:57	\N	\N
1034	3UGMSKKJBY	DIN 912 M6X105	ZYLINDERSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1035	3UGMSKKJBY	DIN 912 M8X65	ZYLINDERSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
1036	3UGMSKKJBY	DIN 912 M16X45	ZYLINDERSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
1037	3UGMSKKJBY	DIN 912 M24X90	ZYLINDERSCHRAUBEN	NONE	2018-02-25 07:59:57	\N	\N
1038	3UGMSKKJBY	ISO 7379 16X100	SCHULTERPASSSCHRAUBE-Z38_16X100	NONE	2018-02-25 07:59:57	\N	\N
1039	3UGMSKKJBY	ISO 8735 12X60	ZYLINDERSTIFT MIT INNENGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
1040	3UGMSKKJBY	ISO 8735 12X40	ZYLINDERSTIFT MIT INNENGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
1041	3UGMSKKJBY	ISO 8735 12X80	ZYLINDERSTIFT MIT INNENGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
1042	3UGMSKKJBY	RUNDSCHNURRING 12X80	RUNDSCHNURRING	NONE	2018-02-25 07:59:57	\N	\N
1043	3UGMSKKJBY	14242	ISKT SMX-SCHRAUBE 1_4-28 X1-1_4 UNF	NONE	2018-02-25 07:59:57	\N	\N
1044	3UGMSKKJBY	39999	STECKER M12X1	NONE	2018-02-25 07:59:57	\N	\N
1045	3UGMSKKJBY	GN 6339-16.5-30-4-BT	HOCHFESTE UNTERLEGSCHEIBE, NIEDRIGE FORM	NONE	2018-02-25 07:59:57	\N	\N
1046	3UGMSKKJBY	Z62-34X2-16.3	TELLERFEDER	NONE	2018-02-25 07:59:57	\N	\N
1047	3UGMSKKJBY	ST7491 40X095	STEINEL FÜHRUNGSBUCHSE MIT SCHMIERNUT	NONE	2018-02-25 07:59:57	\N	\N
1048	3UGMSKKJBY	K513 SG 0290 MR30	KEGELRADGETRIEBE, I= 29,18	NONE	2018-02-25 07:59:57	\N	\N
1049	3UGMSKKJBY	GH02AL001-040	KLEMMKASTEN	NONE	2018-02-25 07:59:57	\N	\N
1050	3UGMSKKJBY	14.400.01.01.02	WÄRMEKLAPPE	NONE	2018-02-25 07:59:57	\N	\N
1051	3UGMSKKJBY	14.400.01.01.03	STÜTZE FÜR WÄRMEKLAPPE	NONE	2018-02-25 07:59:57	\N	\N
1052	3UGMSKKJBY	14.401.08.00.03	BEFESTIGUNGSWINKEL	NONE	2018-02-25 07:59:57	\N	\N
1053	3UGMSKKJBY	22.403.00.05.03	SCHEIBE, FOLIENEINLAUF	NONE	2018-02-25 07:59:57	\N	\N
1054	3UGMSKKJBY	ISO 4017 M10X40	SECHSKANTSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1055	3UGMSKKJBY	LFR5201-10-2Z	PROFILLAUFROLLE	NONE	2018-02-25 07:59:57	\N	\N
1056	3UGMSKKJBY	ROTEX GS28 SPANNRINGNABE-24H7-KUPPLUNG	ROTEX-SPANNRINGNABE-24H7-KUPPLUNG	NONE	2018-02-25 07:59:57	\N	\N
1057	3UGMSKKJBY	ISO 8735 10X32	ZYLINDERSTIFT MIT INNENGEWINDE	NONE	2018-02-25 07:59:57	\N	\N
1058	3UGMSKKJBY	Z825-9	BLINDKUPPLUNG	NONE	2018-02-25 07:59:57	\N	\N
1059	3UGMSKKJBY	R911347947	SERVO-KABEL	NONE	2018-02-25 07:59:57	\N	\N
1060	3UGMSKKJBY	R911310645	SERVO-KABEL	NONE	2018-02-25 07:59:57	\N	\N
1061	3UGMSKKJBY	ISO 7379 10X25	SCHULTERPASSSCHRAUBE-Z38_10X25	NONE	2018-02-25 07:59:57	\N	\N
1062	3UGMSKKJBY	303426	BUNDBUCHSE MIT TEFLO	NONE	2018-02-25 07:59:57	\N	\N
1063	3UGMSKKJBY	11.401.00.00.01-B	ANTRIEBSWELLE	NONE	2018-02-25 07:59:57	\N	\N
1064	3UGMSKKJBY	HTS/2-400W	HTS-2-400W	NONE	2018-02-25 07:59:57	\N	\N
1065	3UGMSKKJBY	TFB 3026	BUNDBUCHSE MIT TEFLONBESCHICHTUNG	NONE	2018-02-25 07:59:57	\N	\N
1066	3UGMSKKJBY	KAR78-4SK	KARASTO 1_3-VERSCHRAUBUNG-SAUGER AG G1-25MM	NONE	2018-02-25 07:59:57	\N	\N
1067	3UGMSKKJBY	DIN 471 28X1.5	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
1068	3UGMSKKJBY	DIN 472 35X1.5	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
1069	3UGMSKKJBY	DIN 472 47X1.75	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
1070	3UGMSKKJBY	O-RING 88X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
1071	3UGMSKKJBY	O-RING 97X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
1072	3UGMSKKJBY	O-RING 100X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
1073	3UGMSKKJBY	8000-050-065	SPANNSATZ (41 N.M )	NONE	2018-02-25 07:59:57	\N	\N
1074	3UGMSKKJBY	HTS/2-400W	HTS-2-200W	NONE	2018-02-25 07:59:57	\N	\N
1075	3UGMSKKJBY	HTS-2 400W	KERAMISCHER INFRAROT STRAHLER HTS/2 400W	NONE	2018-02-25 07:59:57	\N	\N
1076	3UGMSKKJBY	THTS/2-400W	KERAMISCHER INFRAROT	NONE	2018-02-25 07:59:57	\N	\N
1077	3UGMSKKJBY	THTS-2 400W	KERAMISCHER INFRAROT	NONE	2018-02-25 07:59:57	\N	\N
1078	3UGMSKKJBY	AK	ANSCHLUSSKLEMME	NONE	2018-02-25 07:59:57	\N	\N
1079	3UGMSKKJBY	11.401.00.00.02-C	KURBEL	NONE	2018-02-25 07:59:57	\N	\N
1080	3UGMSKKJBY	22.403.02.00.12	HALTER LICHTSCHRANKE, FOLIENDURCHHANG	NONE	2018-02-25 07:59:57	\N	\N
1081	3UGMSKKJBY	HTD 800-8M-50	ZAHNRIEMEN-BELT	NONE	2018-02-25 07:59:57	\N	\N
1082	3UGMSKKJBY	11.400.00.02.00-C	FORMTISCH	NONE	2018-02-25 07:59:57	\N	\N
1084	3UGMSKKJBY	LFS52-1120MM	INA - LAUFROLLENTRAGSCHIENE	NONE	2018-02-25 07:59:57	\N	\N
1085	3UGMSKKJBY	18.400.01.01.00-A	HEIZUNSGRAHMEN-UNTERHEZUNG	NONE	2018-02-25 07:59:57	\N	\N
1086	3UGMSKKJBY	18.400.03.01.00-A	HEIZUNGSRAHMEN-OBEN	NONE	2018-02-25 07:59:57	\N	\N
1087	3UGMSKKJBY	KQ2S12-03AS	ANSCHLUSS	NONE	2018-02-25 07:59:57	\N	\N
1088	3UGMSKKJBY	KQ2T12-00A	T-STECKVERBINDER	NONE	2018-02-25 07:59:57	\N	\N
1089	3UGMSKKJBY	42142195	WASSER	NONE	2018-02-25 07:59:57	\N	\N
1090	3UGMSKKJBY	KQ2L08-00A	WINKEL-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
1091	3UGMSKKJBY	CP96SDB40-60	ISO-ZYLINDER-STANDARD-DOPPELT	NONE	2018-02-25 07:59:57	\N	\N
1092	3UGMSKKJBY	CD85N25-25-B	ISO-DRUCKLUFTZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
1093	3UGMSKKJBY	GKM10-20	GABELGELENK	NONE	2018-02-25 07:59:57	\N	\N
1094	3UGMSKKJBY	AN20-C10	SCHALLDÄMPFER-STECKVERBINDUNG	NONE	2018-02-25 07:59:57	\N	\N
1095	3UGMSKKJBY	KK4P-10H	S-KUPPLUNG -STECKER	NONE	2018-02-25 07:59:57	\N	\N
1096	3UGMSKKJBY	KAR40111A	KARASTO GEKA PLUS-SCHLAUCHST-3	NONE	2018-02-25 07:59:57	\N	\N
1097	3UGMSKKJBY	2125INOX	SCHLAUCHSCHELLEN IDEAL EDELSTAHL	NONE	2018-02-25 07:59:57	\N	\N
1098	3UGMSKKJBY	CP96SDB40-600	ISO-ZYLINDER	NONE	2018-02-25 07:59:57	\N	\N
1099	3UGMSKKJBY	42143819	STILLES WASSER	NONE	2018-02-25 07:59:57	\N	\N
1100	3UGMSKKJBY	HF-14-530	5-2 VENTIL	NONE	2018-02-25 07:59:57	\N	\N
1101	3UGMSKKJBY	HR-18-320	3-2 VENTIL	NONE	2018-02-25 07:59:57	\N	\N
1102	3UGMSKKJBY	HR-18-520	5-2 VENTIL	NONE	2018-02-25 07:59:57	\N	\N
1103	3UGMSKKJBY	21.400.00.01.03	DISTANZLEISTE FORMLUFTVENTIL	NONE	2018-02-25 07:59:57	\N	\N
1104	3UGMSKKJBY	11.401.00.00.17	REFLEKTORHALTER	NONE	2018-02-25 07:59:57	\N	\N
1105	3UGMSKKJBY	11.401.00.00.15-A	REFLEKTOR-FOLIENDURCHHANG	NONE	2018-02-25 07:59:57	\N	\N
1106	3UGMSKKJBY	14.401.09.00.09-A	GABELARM	NONE	2018-02-25 07:59:57	\N	\N
1107	3UGMSKKJBY	14.401.09.00.02	SAULE	NONE	2018-02-25 07:59:57	\N	\N
1108	3UGMSKKJBY	14.401.09.00.04	ELLBOGENBODEN	NONE	2018-02-25 07:59:57	\N	\N
1109	3UGMSKKJBY	14.401.09.00.05	LESITE	NONE	2018-02-25 07:59:57	\N	\N
1110	3UGMSKKJBY	14.401.09.00.08	GEGENGEWICHTSTANGE	NONE	2018-02-25 07:59:57	\N	\N
1111	3UGMSKKJBY	14.401.09.00.10	GEGENGEWICHT	NONE	2018-02-25 07:59:57	\N	\N
1112	3UGMSKKJBY	14.401.09.00.06	WELLE	NONE	2018-02-25 07:59:57	\N	\N
1113	3UGMSKKJBY	BLR4040 - 3.6UU	KUGELGEWINDETRIEB ROTATIONSMUTTER	NONE	2018-02-25 07:59:57	\N	\N
1114	3UGMSKKJBY	8888400	BRAVO-PUMPE	NONE	2018-02-25 07:59:57	\N	\N
1115	3UGMSKKJBY	Z835-9	BLINDNIPPEL	NONE	2018-02-25 07:59:57	\N	\N
1116	3UGMSKKJBY	19.402.00.00.14-A	BETÄTIGUNGSWINKEL	NONE	2018-02-25 07:59:57	\N	\N
1117	3UGMSKKJBY	19.402.00.00.25	VORSTRECKERKUPPLUNG	NONE	2018-02-25 07:59:57	\N	\N
1118	3UGMSKKJBY	19.410.00.00.01	GRUNDPLATTE	NONE	2018-02-25 07:59:57	\N	\N
1119	3UGMSKKJBY	19.410.00.00.06	ABFANGBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
1120	3UGMSKKJBY	11.400.00.00.39A	RING FÜR FÜHRUNGSSÄULE	NONE	2018-02-25 07:59:57	\N	\N
1121	3UGMSKKJBY	11.400.00.04.04 A	WERKZEUGWECHSELFÜHRUNG, BOLZEN	NONE	2018-02-25 07:59:57	\N	\N
1122	3UGMSKKJBY	11.400.00.04.09	WERKZEUGWECHSELFÜHRUNG, HALTERUNG	NONE	2018-02-25 07:59:57	\N	\N
1123	3UGMSKKJBY	11.401.00.01.00-A	OBERJOCH	NONE	2018-02-25 07:59:57	\N	\N
1124	3UGMSKKJBY	11.300.02.00.04-C	ACHSE ZUR VERSTELLUNG, LANG	NONE	2018-02-25 07:59:57	\N	\N
1125	3UGMSKKJBY	GSP6	GUMMISCHLAUCH FÜR STECKANSCHLÜSSE 6.3X12.7 MM	NONE	2018-02-25 07:59:57	\N	\N
1126	3UGMSKKJBY	GTP186	STECKANSCHLUSS G1_8 AG (60°KONUS), 6.3MM GSP-SCHLAUCH	NONE	2018-02-25 07:59:57	\N	\N
1127	3UGMSKKJBY	WE 18 MSV	90° EINSCHRAUBWINKEL G 1_8 (IG) – R 1_8 (AG) - VERNICKEL	NONE	2018-02-25 07:59:57	\N	\N
1128	3UGMSKKJBY	10.401.00.00.19	STREBE-EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
1129	3UGMSKKJBY	11.400.00.00.37-C	TISCHANLENKUNG, HINTERTEIL	NONE	2018-02-25 07:59:57	\N	\N
1130	3UGMSKKJBY	10.401.00.00.13-A	ABSTANDSHALTER FÜR ROLLBAHN, EINLAUFSEITE	NONE	2018-02-25 07:59:57	\N	\N
1131	3UGMSKKJBY	10.401.00.00.14-A	ABSTANDHALTER FÜR ROLLBAHN, HARTING STECKER	NONE	2018-02-25 07:59:57	\N	\N
1132	3UGMSKKJBY	11.402.00.00.04	SCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
1133	3UGMSKKJBY	11.300.02.00.01-B	DECKEL	NONE	2018-02-25 07:59:57	\N	\N
1134	3UGMSKKJBY	11.400.00.00.61	WECHSELPLATTE ANSCHLUSSBOX	NONE	2018-02-25 07:59:57	\N	\N
1135	3UGMSKKJBY	11.401.00.00.16	ZYLINDERSCHRAUBE MIT M6 GEWINDE IM KOPF	NONE	2018-02-25 07:59:57	\N	\N
1136	3UGMSKKJBY	11.402.00.00.01	FÜHRUNGSWELLE MIT LUFTDURCHFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
1137	3UGMSKKJBY	11.402.00.00.02	FÜHRUNGSWELLE MIT KABELDURCHFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
1138	3UGMSKKJBY	11.402.00.00.03	FÜHRUNGSBLECH, AUSWERFERKABEL	NONE	2018-02-25 07:59:57	\N	\N
1139	3UGMSKKJBY	11.402.00.00.05	FÜHRUNGSSTANGE FÜR FORMTISCH	NONE	2018-02-25 07:59:57	\N	\N
1140	3UGMSKKJBY	11.402.01.00.01	SEITENWAND, HINTEN	NONE	2018-02-25 07:59:57	\N	\N
1141	3UGMSKKJBY	11.402.01.00.02	SEITENWAND, VORNE	NONE	2018-02-25 07:59:57	\N	\N
1142	3UGMSKKJBY	10.401.00.00.20	VERKLEIDUNGSBLECH GRUNDRAHMEN	NONE	2018-02-25 07:59:57	\N	\N
1143	3UGMSKKJBY	11.401.01.00.04-B	PLATTE, STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
1144	3UGMSKKJBY	11.401.00.00.17-A	REFLEKTORWINKEL	NONE	2018-02-25 07:59:57	\N	\N
1145	3UGMSKKJBY	16.402.01.01.08-A	WINKEL, VORNE FÜR UMLENKROLLE	NONE	2018-02-25 07:59:57	\N	\N
1146	3UGMSKKJBY	16.402.01.01.09-A	WINKEL, HINTEN FÜR UMLENKROLLE	NONE	2018-02-25 07:59:57	\N	\N
1147	3UGMSKKJBY	11.410.01.00.05	DISTANZACHSE, BREITE FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
1148	3UGMSKKJBY	16.402.01.01.12	"KLEMMKONUS	NONE	2018-02-25 07:59:57	\N	\N
1149	3UGMSKKJBY	11.300.02.00.17	WERKZEUGHALTER	NONE	2018-02-25 07:59:57	\N	\N
1150	3UGMSKKJBY	14.400.00.05.00-A	STÜTZARM	NONE	2018-02-25 07:59:57	\N	\N
1151	3UGMSKKJBY	19.402.01.00.05-B	WELLE	NONE	2018-02-25 07:59:57	\N	\N
1152	3UGMSKKJBY	11.410.01.01.01	TRÄGERPLATTE ANTRIEB - STÖBER GETRIEBE - BREITE FORMSTATION	NONE	2018-02-25 07:59:57	\N	\N
1153	3UGMSKKJBY	11.410.01.00.03	BODENPLATTE FORMSTATION - STÖBER GETRIEBE - BREIT	NONE	2018-02-25 07:59:57	\N	\N
1154	3UGMSKKJBY	11.410.01.00.04	PLATTE - STÖBER GETRIEBE - BREIT	NONE	2018-02-25 07:59:57	\N	\N
1155	3UGMSKKJBY	4012801165501	DIN 625 6310-2RSR	NONE	2018-02-25 07:59:57	\N	\N
1156	3UGMSKKJBY	11.410.01.01.02	STREBE - STÖBER GETRIEBE	NONE	2018-02-25 07:59:57	\N	\N
1157	3UGMSKKJBY	11.410.00.00.01	ANTRIEBSWELLE	NONE	2018-02-25 07:59:57	\N	\N
1158	3UGMSKKJBY	DIN 912 M16X55	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
1159	3UGMSKKJBY	ISO 7379 16X120	SCHULTERPASSSCHRAUBE-Z38_16X120	NONE	2018-02-25 07:59:57	\N	\N
1160	3UGMSKKJBY	4012801165372	DIN 625 6309-2RSR - RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
1161	3UGMSKKJBY	3084654	PUSH-IN 90 GRAD M10X1  4MM	NONE	2018-02-25 07:59:57	\N	\N
1162	3UGMSKKJBY	11.400.00.00.36-C	FÜHRUNGSWELLE - HINTEN	NONE	2018-02-25 07:59:57	\N	\N
1163	3UGMSKKJBY	3084577	PUSH-IN VERSCHRAUBUNG - GE-4-R1_8	NONE	2018-02-25 07:59:57	\N	\N
1164	3UGMSKKJBY	DIN 912 M16X75	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
1165	3UGMSKKJBY	DIN 6912 M16X45	ZYLINDERSCHRAUBEN MIT INNENSECHSKANT, NIEDRIGER KOPF MIT SCHLÜSSELFÜHRUNG	NONE	2018-02-25 07:59:57	\N	\N
1166	3UGMSKKJBY	EVC006	KABELDOSE  M12-WINKEL-4ADR-MS 0  LED-10M-PUR	NONE	2018-02-25 07:59:57	\N	\N
1167	3UGMSKKJBY	ISO 8752 6X24	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
1168	3UGMSKKJBY	EB 1547.500	RITTAL E-BOX	NONE	2018-02-25 07:59:57	\N	\N
1169	3UGMSKKJBY	LKM 60060	KABELKANAL 60X60 - L=1650MM	NONE	2018-02-25 07:59:57	\N	\N
1170	3UGMSKKJBY	SMX VERTEILER	DROPSA - SMX VERTEILER KOMPLETT	NONE	2018-02-25 07:59:57	\N	\N
1171	3UGMSKKJBY	DIN 912 M16X50	ZYLINDERSCHRAUBEN, MIT INNENSECHSKANT	NONE	2018-02-25 07:59:57	\N	\N
1172	3UGMSKKJBY	39182-2-4	GEWINDETÜLLE	NONE	2018-02-25 07:59:57	\N	\N
1173	3UGMSKKJBY	ISO 8752 6X30	SPANNSTIFT	NONE	2018-02-25 07:59:57	\N	\N
1174	3UGMSKKJBY	DIN 7349 8.4	SCHEIBEN FÜR SCHRAUBEN MIT SCHWEREN SPANNSTIFTEN	NONE	2018-02-25 07:59:57	\N	\N
1175	3UGMSKKJBY	17477214	ZAHNRIEMENRAD HTD-8M-TAPER-RIEMENBREITE 30MM-Z=28	NONE	2018-02-25 07:59:57	\N	\N
1176	3UGMSKKJBY	62250332	TAPER SPANNBUCHSE  1210 - Ø32	NONE	2018-02-25 07:59:57	\N	\N
1177	3UGMSKKJBY	LFE12-1_M12	ZAPFEN-EXZENTRISCH	NONE	2018-02-25 07:59:57	\N	\N
1178	3UGMSKKJBY	DIN ISO 606-05 B1	ROLLENKETTE - 400 GLIEDER INKL. SCHLOSS	NONE	2018-02-25 07:59:57	\N	\N
1179	3UGMSKKJBY	4012802197440	RNA69/32-ZW-XL	NONE	2018-02-25 07:59:57	\N	\N
1180	3UGMSKKJBY	4012802428063	NATR30-PP-A	NONE	2018-02-25 07:59:57	\N	\N
1181	3UGMSKKJBY	DIN 471 80X4	WELLENSICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
1182	3UGMSKKJBY	PM 1615 DS	PTFE-GLEITLAGERBUCHSE	NONE	2018-02-25 07:59:57	\N	\N
1183	3UGMSKKJBY	KAR894	KARASTO1_3-VERSCHRAUBUNG-SAUGE	NONE	2018-02-25 07:59:57	\N	\N
1184	3UGMSKKJBY	DIN 472 75X2.5	SICHERUNGSRING	NONE	2018-02-25 07:59:57	\N	\N
1185	3UGMSKKJBY	4012802361827	6001-C-2HRS	NONE	2018-02-25 07:59:57	\N	\N
1186	3UGMSKKJBY	4012801068765	DIN 625 6011-2RSR - RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
1187	3UGMSKKJBY	4047643293525	DIN 625 6207-C-2HRS RILLENKUGELLAGER	NONE	2018-02-25 07:59:57	\N	\N
1188	3UGMSKKJBY	O-RING 28X2	O-RING	NONE	2018-02-25 07:59:57	\N	\N
1189	3UGMSKKJBY	23.410.02.00.07	POSITIONSCHEIBE	NONE	2018-02-25 07:59:57	\N	\N
1190	3UGMSKKJBY	23.410.02.00.03	VERBINDER	NONE	2018-02-25 07:59:57	\N	\N
1191	3UGMSKKJBY	23.410.02.00.04	VERBINDER	NONE	2018-02-25 07:59:57	\N	\N
1192	3UGMSKKJBY	23.410.01.00.01	GRUNDPLATTE-UNTEN	NONE	2018-02-25 07:59:57	\N	\N
1193	3UGMSKKJBY	23.410.01.00.02	FÜHRUNGSHALTER - VORNE	NONE	2018-02-25 07:59:57	\N	\N
1194	3UGMSKKJBY	23.410.02.02.03	WELLE	NONE	2018-02-25 07:59:57	\N	\N
1195	3UGMSKKJBY	ISO 4017 M8X16	SECHSKANTSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1196	3UGMSKKJBY	ISO 4017 M8X25	SECHSKANTSCHRAUBE	NONE	2018-02-25 07:59:57	\N	\N
1197	3UGMSKKJBY	23.410.02.00.02	SCHLITTPLATE	NONE	2018-02-25 07:59:57	\N	\N
1198	3UGMSKKJBY	23.410.02.00.11	ABSTANDPIN	NONE	2018-02-25 07:59:57	\N	\N
1199	3UGMSKKJBY	23.410.02.00.12	KLOTZ	NONE	2018-02-25 07:59:57	\N	\N
1200	3UGMSKKJBY	550281000002	ROTEX GS28 ZAHNKRANZ 98 SHORE A - GS	NONE	2018-02-25 07:59:57	\N	\N
1201	3UGMSKKJBY	ROTEX GS28 SPANNRINGNABE 28H7 KUPPLUNG	ROTEX GS28 SPANNRINGNABE 28H7 KUPPLUNG	NONE	2018-02-25 07:59:57	\N	\N
1202	3UGMSKKJBY	19.402.01.00.08	GEWINDESTIFT MIT INNENSECHSKANT UND  KEGELKUPPE	NONE	2018-02-25 07:59:57	\N	\N
1203	3UGMSKKJBY	19.402.01.00.02-B	GLOCKE MOTORBEFESTIGUNG, UNTEN  (OHNE BREMSE)	NONE	2018-02-25 07:59:57	\N	\N
1204	3UGMSKKJBY	23.410.01.00.03	ITEM-WINKEL	NONE	2018-02-25 07:59:57	\N	\N
1205	3UGMSKKJBY	GN 333.1-20-200-A EL	ROHRGRIFF	NONE	2018-02-25 07:59:57	\N	\N
1206	3UGMSKKJBY	GN 558-75-K16	SCHALTKURBEL	NONE	2018-02-25 07:59:57	\N	\N
2	3UGMSKKJBY	22.403.00.00.01-A	RAHMEN-ECKE-nojoda loco	\N	2018-02-25 07:59:57	\N	\N
1209	3UGMSKKJBY	traseros	verga	NONE	2018-05-20 09:15:00	\N	\N
1212	3UGMSKKJBY	Joda loco	22	NONE	2018-05-20 08:16:54	NONE	NONE
1213	3UGMSKKJBY	jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj	aaaaaaaaaaaaaaaaa	NONE	2018-05-20 09:47:19	NONE	NONE
1	3UGMSKKJBY	22.400.00.00.26-C	VORDERES SEITENBLECH-AUSLAUFSEITE-juan	NONE	2018-02-25 07:59:57	22.400.00.00.26-C	256478
336	3UGMSKKJBY	14.400.00.00.01	EXZENTERACHSE	NONE	2018-02-25 07:59:57	14.400.00.00.01	NONE
\.


--
-- Data for Name: items_in_assemblies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_assemblies (iia_id, assembly_id, item_id, item_amount) FROM stdin;
1	2	1	1.0
2	2	2	1.0
3	2	3	1.0
4	2	4	1.0
5	4	19	6.0
6	4	83	5.0
7	4	285	4.0
8	4	333	3.0
9	4	369	2.0
10	3	442	5.0
11	3	929	17.0
12	3	1177	18.0
13	6	35	4.0
14	6	36	4.0
15	2	32	3.0
16	2	33	1.0
18	5	51	2.0
19	5	30	2.0
20	5	32	3.0
21	21	15	1.0
22	21	1	1.0
23	21	3	2.0
24	21	4	2.0
25	21	5	3.0
26	21	6	3.0
28	21	8	4.0
29	21	9	4.0
30	21	10	3.0
31	21	11	3.0
32	21	12	2.0
33	21	13	1.0
34	21	14	1.0
35	21	2	1.0
36	21	16	2.0
37	21	17	2.0
38	21	18	3.0
39	21	19	3.0
40	21	20	3.0
41	21	21	4.0
42	21	22	4.0
43	21	23	3.0
44	21	24	3.0
45	21	25	2.0
46	21	26	1.0
47	21	27	1.0
48	21	28	1.0
49	21	29	2.0
50	2	8	4.0
51	2	9	4.0
52	2	10	3.0
53	2	11	3.0
54	2	12	2.0
55	2	13	1.0
56	2	14	1.0
57	2	29	2.0
58	2	16	2.0
59	2	17	2.0
62	2	20	3.0
63	2	21	4.0
64	2	22	4.0
66	2	24	3.0
68	2	26	1.0
70	2	28	1.0
71	2	15	1.0
72	22	1022	2.0
73	22	1038	4.0
74	22	1061	6.0
75	2	7	3.0
76	2	6	3.0
77	2	5	3.0
27	21	7	14.0
65	2	23	25.0
69	2	27	1005.0
79	23	30	2.0
78	23	51	14.0
81	25	35	4.0
82	25	36	5.0
\.


--
-- Data for Name: items_in_offers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_offers (iioffer_id, offer_id, item_id, item_amount, item_offered_price, user_id) FROM stdin;
3	1	30	2.0	1.00	3
4	2	32	3.0	1.00	3
5	2	33	1.0	1.00	3
1	1	3	8.0	10.00	2
6	3	51	2.0	1.00	3
7	3	32	3.0	1.00	3
8	3	34	2.0	1.00	3
13	3	33	1.0	1.00	3
14	3	35	3.0	1.00	3
15	3	37	2.0	1.00	3
\.


--
-- Data for Name: items_in_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_orders (iio_id, item_id, order_id, item_amount, received_amount, last_amount_added, item_buy_price, user_id, item_received_date, item_delivered) FROM stdin;
13	239	2	11.0	0.0	0.0	\N	\N	2018-04-02 16:03:32	f
14	240	2	12.0	0.0	0.0	\N	\N	2018-04-02 16:03:32	f
15	242	2	14.0	0.0	0.0	\N	\N	2018-04-02 16:03:32	f
17	509	2	15.0	0.0	0.0	\N	\N	2018-04-02 16:03:32	f
18	1123	2	2.0	0.0	0.0	\N	\N	2018-04-02 16:03:32	f
160	1022	2	15.0	0.0	0.0	\N	\N	2018-04-14 10:30:29	f
174	1091	4	4.0	0.0	0.0	\N	\N	2018-04-15 19:56:55	f
175	239	2	11.0	0.0	0.0	\N	\N	2018-04-20 15:18:32	f
176	240	2	12.0	0.0	0.0	\N	\N	2018-04-20 15:18:32	f
38	1022	4	15.0	0.0	0.0	\N	\N	2018-04-02 16:07:36	f
39	1038	4	12.0	0.0	0.0	\N	\N	2018-04-02 16:07:36	f
40	1061	4	16.0	0.0	0.0	\N	\N	2018-04-02 16:07:36	f
41	1159	4	19.0	0.0	0.0	\N	\N	2018-04-02 16:07:36	f
167	477	2	5.0	0.0	0.0	\N	\N	2018-04-14 10:57:03	f
146	358	4	16.0	0.0	0.0	\N	\N	2018-04-08 08:39:05	f
187	358	15	16.0	0.0	0.0	\N	\N	2018-05-05 07:04:37	f
188	51	15	2.0	0.0	0.0	\N	\N	2018-05-05 07:05:10	f
189	74	15	4.0	0.0	0.0	\N	\N	2018-05-05 07:05:10	f
190	142	15	6.0	0.0	0.0	\N	\N	2018-05-05 07:05:10	f
199	311	2	5.0	0.0	0.0	\N	\N	2018-05-20 15:03:49	f
200	421	2	3.0	0.0	0.0	\N	\N	2018-05-20 15:03:49	f
201	9	17	2.0	0.0	0.0	\N	\N	2018-05-20 20:22:09	f
202	1	17	2.0	0.0	0.0	\N	\N	2018-05-20 20:46:22	f
6	336	2	12.0	0.0	0.0	10.00	\N	2018-02-25 11:55:13	f
203	2	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
204	3	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
206	5	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
207	6	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
208	7	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
209	8	17	4.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
210	10	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
211	11	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
212	12	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
213	13	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
214	14	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
215	15	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
216	16	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
217	17	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
218	18	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
219	19	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
220	20	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
221	21	17	4.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
222	22	17	4.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
223	23	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
224	24	17	3.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
225	25	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
226	26	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
227	27	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
228	28	17	1.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
229	29	17	2.0	0.0	0.0	\N	\N	2018-05-27 11:16:06	f
230	1	2	1.0	0.0	0.0	\N	\N	2018-05-30 18:07:50	f
231	2	2	1.0	0.0	0.0	\N	\N	2018-05-30 18:07:50	f
232	358	2	2.0	0.0	0.0	\N	\N	2018-05-30 19:29:48	f
\.


--
-- Data for Name: items_in_projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_projects (iip_id, project_id, item_id, item_amount, assembly_id, aip_id, saip_id, state_id, state_date, user_id) FROM stdin;
2	1	2	1.0	2	2	\N	1	2018-02-25 08:07:59	1
4	1	4	1.0	2	2	\N	1	2018-02-25 08:07:59	1
6	1	929	17.0	3	3	\N	1	2018-02-25 08:07:59	1
7	1	1177	18.0	3	3	\N	1	2018-02-25 08:07:59	1
8	1	19	6.0	4	\N	1	1	2018-02-25 08:08:30	1
9	1	83	5.0	4	\N	1	1	2018-02-25 08:08:30	1
10	1	285	4.0	4	\N	1	1	2018-02-25 08:08:30	1
11	1	333	3.0	4	\N	1	1	2018-02-25 08:08:30	1
12	1	369	2.0	4	\N	1	1	2018-02-25 08:08:30	1
13	1	19	6.0	4	\N	2	1	2018-02-25 08:08:30	1
14	1	83	5.0	4	\N	2	1	2018-02-25 08:08:30	1
15	1	285	4.0	4	\N	2	1	2018-02-25 08:08:30	1
16	1	333	3.0	4	\N	2	1	2018-02-25 08:08:30	1
17	1	369	2.0	4	\N	2	1	2018-02-25 08:08:30	1
18	1	745	2.0	1	1	\N	1	2018-02-25 09:15:36	1
19	1	925	2.0	1	1	\N	1	2018-02-25 09:15:36	1
20	1	1205	2.0	1	1	\N	1	2018-02-25 09:15:36	1
21	1	37	2.0	1	1	\N	1	2018-02-25 09:24:25	1
22	1	38	1.0	1	1	\N	1	2018-02-25 09:24:25	1
23	1	702	2.0	1	1	\N	1	2018-02-25 09:24:25	1
24	1	24	5.0	1	1	\N	1	2018-02-25 09:31:02	1
25	1	25	6.0	1	1	\N	1	2018-02-25 09:31:02	1
26	1	311	7.0	1	1	\N	1	2018-02-25 09:40:24	1
27	1	420	2.0	1	1	\N	1	2018-02-25 09:40:24	1
28	1	52	5.0	1	1	\N	1	2018-02-25 09:42:03	1
29	1	1114	6.0	1	1	\N	1	2018-02-25 09:42:03	1
30	1	19	6.0	4	\N	4	1	2018-02-25 08:52:47	1
31	1	83	5.0	4	\N	4	1	2018-02-25 08:52:47	1
32	1	285	4.0	4	\N	4	1	2018-02-25 08:52:47	1
33	1	333	3.0	4	\N	4	1	2018-02-25 08:52:47	1
34	1	369	2.0	4	\N	4	1	2018-02-25 08:52:47	1
35	1	19	6.0	4	\N	5	1	2018-02-25 08:52:47	1
36	1	83	5.0	4	\N	5	1	2018-02-25 08:52:47	1
37	1	285	4.0	4	\N	5	1	2018-02-25 08:52:47	1
38	1	333	3.0	4	\N	5	1	2018-02-25 08:52:47	1
39	1	369	2.0	4	\N	5	1	2018-02-25 08:52:47	1
40	1	19	6.0	4	\N	9	1	2018-02-25 08:53:55	1
41	1	83	5.0	4	\N	9	1	2018-02-25 08:53:55	1
42	1	285	4.0	4	\N	9	1	2018-02-25 08:53:55	1
43	1	333	3.0	4	\N	9	1	2018-02-25 08:53:55	1
44	1	369	2.0	4	\N	9	1	2018-02-25 08:53:55	1
45	1	19	6.0	4	\N	10	1	2018-02-25 08:53:55	1
46	1	83	5.0	4	\N	10	1	2018-02-25 08:53:55	1
47	1	285	4.0	4	\N	10	1	2018-02-25 08:53:55	1
48	1	333	3.0	4	\N	10	1	2018-02-25 08:53:55	1
49	1	369	2.0	4	\N	10	1	2018-02-25 08:53:55	1
50	1	19	6.0	4	\N	12	1	2018-02-25 08:55:13	1
51	1	83	5.0	4	\N	12	1	2018-02-25 08:55:13	1
52	1	285	4.0	4	\N	12	1	2018-02-25 08:55:13	1
53	1	333	3.0	4	\N	12	1	2018-02-25 08:55:13	1
54	1	369	2.0	4	\N	12	1	2018-02-25 08:55:13	1
55	2	19	6.0	4	\N	22	1	2018-02-25 12:46:16	2
56	2	83	5.0	4	\N	22	1	2018-02-25 12:46:16	2
57	2	285	4.0	4	\N	22	1	2018-02-25 12:46:16	2
58	2	333	3.0	4	\N	22	1	2018-02-25 12:46:16	2
59	2	369	2.0	4	\N	22	1	2018-02-25 12:46:16	2
60	2	19	6.0	4	\N	23	1	2018-02-25 12:46:16	2
61	2	83	5.0	4	\N	23	1	2018-02-25 12:46:16	2
62	2	285	4.0	4	\N	23	1	2018-02-25 12:46:16	2
63	2	333	3.0	4	\N	23	1	2018-02-25 12:46:16	2
64	2	369	2.0	4	\N	23	1	2018-02-25 12:46:16	2
65	2	19	6.0	4	\N	24	1	2018-02-25 12:46:16	2
66	2	83	5.0	4	\N	24	1	2018-02-25 12:46:16	2
67	2	285	4.0	4	\N	24	1	2018-02-25 12:46:16	2
68	2	333	3.0	4	\N	24	1	2018-02-25 12:46:16	2
69	2	369	2.0	4	\N	24	1	2018-02-25 12:46:16	2
70	2	35	4.0	6	\N	25	1	2018-02-25 12:46:16	2
71	2	36	4.0	6	\N	25	1	2018-02-25 12:46:16	2
72	2	35	4.0	6	\N	26	1	2018-02-25 12:46:16	2
73	2	36	4.0	6	\N	26	1	2018-02-25 12:46:16	2
74	2	35	4.0	6	\N	27	1	2018-02-25 12:46:16	2
75	2	36	4.0	6	\N	27	1	2018-02-25 12:46:16	2
76	2	35	4.0	6	\N	28	1	2018-02-25 12:46:16	2
77	2	36	4.0	6	\N	28	1	2018-02-25 12:46:16	2
84	2	15	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
85	2	1	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
86	2	3	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
87	2	4	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
88	2	5	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
89	2	6	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
90	2	8	4.0	21	27	\N	1	2018-05-02 17:00:40	\N
91	2	9	4.0	21	27	\N	1	2018-05-02 17:00:40	\N
92	2	10	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
93	2	11	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
94	2	12	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
95	2	13	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
96	2	14	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
97	2	2	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
98	2	16	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
99	2	17	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
100	2	18	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
101	2	19	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
102	2	20	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
103	2	21	4.0	21	27	\N	1	2018-05-02 17:00:40	\N
104	2	22	4.0	21	27	\N	1	2018-05-02 17:00:40	\N
105	2	23	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
106	2	24	3.0	21	27	\N	1	2018-05-02 17:00:40	\N
107	2	25	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
108	2	26	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
109	2	27	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
110	2	28	1.0	21	27	\N	1	2018-05-02 17:00:40	\N
111	2	29	2.0	21	27	\N	1	2018-05-02 17:00:40	\N
112	2	1022	2.0	22	28	\N	1	2018-05-02 17:00:40	\N
113	2	1038	4.0	22	28	\N	1	2018-05-02 17:00:40	\N
114	2	1061	6.0	22	28	\N	1	2018-05-02 17:00:40	\N
115	2	7	14.0	21	27	\N	1	2018-05-02 17:00:40	\N
116	3	442	5.0	3	29	\N	1	2018-05-02 17:13:16	\N
117	3	929	17.0	3	29	\N	1	2018-05-02 17:13:16	\N
118	3	1177	18.0	3	29	\N	1	2018-05-02 17:13:16	\N
119	3	15	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
120	3	1	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
121	3	3	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
122	3	4	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
123	3	5	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
124	3	6	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
125	3	8	4.0	21	30	\N	1	2018-05-02 17:13:16	\N
126	3	9	4.0	21	30	\N	1	2018-05-02 17:13:16	\N
127	3	10	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
128	3	11	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
129	3	12	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
130	3	13	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
131	3	14	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
132	3	2	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
133	3	16	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
134	3	17	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
135	3	18	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
136	3	19	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
137	3	20	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
138	3	21	4.0	21	30	\N	1	2018-05-02 17:13:16	\N
139	3	22	4.0	21	30	\N	1	2018-05-02 17:13:16	\N
140	3	23	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
141	3	24	3.0	21	30	\N	1	2018-05-02 17:13:16	\N
142	3	25	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
143	3	26	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
144	3	27	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
145	3	28	1.0	21	30	\N	1	2018-05-02 17:13:16	\N
146	3	29	2.0	21	30	\N	1	2018-05-02 17:13:16	\N
147	3	7	14.0	21	30	\N	1	2018-05-02 17:13:16	\N
148	3	19	6.0	4	29	29	1	2018-05-02 17:13:16	\N
149	3	83	5.0	4	29	29	1	2018-05-02 17:13:16	\N
150	3	285	4.0	4	29	29	1	2018-05-02 17:13:16	\N
151	3	333	3.0	4	29	29	1	2018-05-02 17:13:16	\N
152	3	369	2.0	4	29	29	1	2018-05-02 17:13:16	\N
153	3	19	6.0	4	29	30	1	2018-05-02 17:13:16	\N
154	3	83	5.0	4	29	30	1	2018-05-02 17:13:16	\N
155	3	285	4.0	4	29	30	1	2018-05-02 17:13:16	\N
156	3	333	3.0	4	29	30	1	2018-05-02 17:13:16	\N
157	3	369	2.0	4	29	30	1	2018-05-02 17:13:16	\N
158	3	51	2.0	5	29	31	1	2018-05-02 17:13:16	\N
159	3	30	2.0	5	29	31	1	2018-05-02 17:13:16	\N
160	3	32	3.0	5	29	31	1	2018-05-02 17:13:16	\N
161	3	51	2.0	5	29	32	1	2018-05-02 17:13:16	\N
162	3	30	2.0	5	29	32	1	2018-05-02 17:13:16	\N
163	3	32	3.0	5	29	32	1	2018-05-02 17:13:16	\N
164	3	51	2.0	5	29	33	1	2018-05-02 17:13:16	\N
165	3	30	2.0	5	29	33	1	2018-05-02 17:13:16	\N
166	3	32	3.0	5	29	33	1	2018-05-02 17:13:16	\N
167	4	35	4.0	25	31	\N	1	2018-05-02 17:15:56	\N
168	4	36	5.0	25	31	\N	1	2018-05-02 17:15:56	\N
170	6	1	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
171	6	2	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
172	6	3	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
173	6	4	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
174	6	442	5.0	3	33	\N	1	2018-05-02 17:57:20	\N
175	6	929	17.0	3	33	\N	1	2018-05-02 17:57:20	\N
176	6	1177	18.0	3	33	\N	1	2018-05-02 17:57:20	\N
177	6	32	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
178	6	33	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
179	6	15	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
180	6	1	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
181	6	3	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
182	6	4	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
183	6	5	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
184	6	6	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
185	6	8	4.0	21	34	\N	1	2018-05-02 17:57:20	\N
186	6	9	4.0	21	34	\N	1	2018-05-02 17:57:20	\N
187	6	10	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
188	6	11	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
189	6	12	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
190	6	13	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
191	6	14	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
192	6	2	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
193	6	16	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
194	6	17	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
195	6	18	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
196	6	19	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
197	6	20	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
198	6	21	4.0	21	34	\N	1	2018-05-02 17:57:20	\N
199	6	22	4.0	21	34	\N	1	2018-05-02 17:57:20	\N
200	6	23	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
201	6	24	3.0	21	34	\N	1	2018-05-02 17:57:20	\N
202	6	25	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
203	6	26	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
204	6	27	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
205	6	28	1.0	21	34	\N	1	2018-05-02 17:57:20	\N
206	6	29	2.0	21	34	\N	1	2018-05-02 17:57:20	\N
207	6	8	4.0	2	32	\N	1	2018-05-02 17:57:20	\N
208	6	9	4.0	2	32	\N	1	2018-05-02 17:57:20	\N
209	6	10	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
210	6	11	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
211	6	12	2.0	2	32	\N	1	2018-05-02 17:57:20	\N
212	6	13	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
213	6	14	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
214	6	29	2.0	2	32	\N	1	2018-05-02 17:57:20	\N
215	6	16	2.0	2	32	\N	1	2018-05-02 17:57:20	\N
216	6	17	2.0	2	32	\N	1	2018-05-02 17:57:20	\N
217	6	20	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
218	6	21	4.0	2	32	\N	1	2018-05-02 17:57:20	\N
219	6	22	4.0	2	32	\N	1	2018-05-02 17:57:20	\N
220	6	24	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
221	6	26	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
222	6	28	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
223	6	15	1.0	2	32	\N	1	2018-05-02 17:57:20	\N
224	6	1022	2.0	22	35	\N	1	2018-05-02 17:57:20	\N
225	6	1038	4.0	22	35	\N	1	2018-05-02 17:57:20	\N
226	6	1061	6.0	22	35	\N	1	2018-05-02 17:57:20	\N
227	6	7	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
228	6	6	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
229	6	5	3.0	2	32	\N	1	2018-05-02 17:57:20	\N
230	6	7	14.0	21	34	\N	1	2018-05-02 17:57:20	\N
231	6	23	25.0	2	32	\N	1	2018-05-02 17:57:20	\N
232	6	27	1005.0	2	32	\N	1	2018-05-02 17:57:20	\N
233	6	35	4.0	25	37	\N	1	2018-05-02 17:57:20	\N
234	6	36	5.0	25	37	\N	1	2018-05-02 17:57:20	\N
235	6	727	6.0	25	37	\N	1	2018-05-02 17:57:20	\N
236	6	19	6.0	4	33	34	1	2018-05-02 17:57:20	\N
237	6	83	5.0	4	33	34	1	2018-05-02 17:57:20	\N
238	6	285	4.0	4	33	34	1	2018-05-02 17:57:20	\N
239	6	333	3.0	4	33	34	1	2018-05-02 17:57:20	\N
240	6	369	2.0	4	33	34	1	2018-05-02 17:57:20	\N
241	6	19	6.0	4	33	35	1	2018-05-02 17:57:20	\N
242	6	83	5.0	4	33	35	1	2018-05-02 17:57:20	\N
243	6	285	4.0	4	33	35	1	2018-05-02 17:57:20	\N
244	6	333	3.0	4	33	35	1	2018-05-02 17:57:20	\N
245	6	369	2.0	4	33	35	1	2018-05-02 17:57:20	\N
246	6	51	2.0	5	33	36	1	2018-05-02 17:57:20	\N
247	6	30	2.0	5	33	36	1	2018-05-02 17:57:20	\N
248	6	32	3.0	5	33	36	1	2018-05-02 17:57:20	\N
249	6	51	2.0	5	33	37	1	2018-05-02 17:57:20	\N
250	6	30	2.0	5	33	37	1	2018-05-02 17:57:20	\N
251	6	32	3.0	5	33	37	1	2018-05-02 17:57:20	\N
252	6	51	2.0	5	33	38	1	2018-05-02 17:57:20	\N
253	6	30	2.0	5	33	38	1	2018-05-02 17:57:20	\N
254	6	32	3.0	5	33	38	1	2018-05-02 17:57:20	\N
\.


--
-- Data for Name: items_in_projects_states; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_projects_states (iips_id, iip_id, state_id, state_date, user_id) FROM stdin;
2	2	1	2018-02-25 08:07:59	1
4	4	1	2018-02-25 08:07:59	1
6	6	1	2018-02-25 08:07:59	1
7	7	1	2018-02-25 08:07:59	1
8	8	1	2018-02-25 08:08:30	1
9	9	1	2018-02-25 08:08:30	1
10	10	1	2018-02-25 08:08:30	1
11	11	1	2018-02-25 08:08:30	1
12	12	1	2018-02-25 08:08:30	1
13	13	1	2018-02-25 08:08:30	1
14	14	1	2018-02-25 08:08:30	1
15	15	1	2018-02-25 08:08:30	1
16	16	1	2018-02-25 08:08:30	1
17	17	1	2018-02-25 08:08:30	1
18	18	1	2018-02-25 09:15:36	1
19	19	1	2018-02-25 09:15:36	1
20	20	1	2018-02-25 09:15:36	1
21	21	1	2018-02-25 09:24:25	1
22	22	1	2018-02-25 09:24:25	1
23	23	1	2018-02-25 09:24:25	1
24	24	1	2018-02-25 09:31:02	1
25	25	1	2018-02-25 09:31:02	1
26	26	1	2018-02-25 09:40:24	1
27	27	1	2018-02-25 09:40:24	1
28	28	1	2018-02-25 09:42:03	1
29	29	1	2018-02-25 09:42:03	1
30	30	1	2018-02-25 08:52:47	1
31	31	1	2018-02-25 08:52:47	1
32	32	1	2018-02-25 08:52:47	1
33	33	1	2018-02-25 08:52:47	1
34	34	1	2018-02-25 08:52:47	1
35	35	1	2018-02-25 08:52:47	1
36	36	1	2018-02-25 08:52:47	1
37	37	1	2018-02-25 08:52:47	1
38	38	1	2018-02-25 08:52:47	1
39	39	1	2018-02-25 08:52:47	1
40	40	1	2018-02-25 08:53:55	1
41	41	1	2018-02-25 08:53:55	1
42	42	1	2018-02-25 08:53:55	1
43	43	1	2018-02-25 08:53:55	1
44	44	1	2018-02-25 08:53:55	1
45	45	1	2018-02-25 08:53:55	1
46	46	1	2018-02-25 08:53:55	1
47	47	1	2018-02-25 08:53:55	1
48	48	1	2018-02-25 08:53:55	1
49	49	1	2018-02-25 08:53:55	1
50	50	1	2018-02-25 08:55:13	1
51	51	1	2018-02-25 08:55:13	1
52	52	1	2018-02-25 08:55:13	1
53	53	1	2018-02-25 08:55:13	1
54	54	1	2018-02-25 08:55:13	1
55	55	1	2018-02-25 12:46:16	2
56	56	1	2018-02-25 12:46:16	2
57	57	1	2018-02-25 12:46:16	2
58	58	1	2018-02-25 12:46:16	2
59	59	1	2018-02-25 12:46:16	2
60	60	1	2018-02-25 12:46:16	2
61	61	1	2018-02-25 12:46:16	2
62	62	1	2018-02-25 12:46:16	2
63	63	1	2018-02-25 12:46:16	2
64	64	1	2018-02-25 12:46:16	2
65	65	1	2018-02-25 12:46:16	2
66	66	1	2018-02-25 12:46:16	2
67	67	1	2018-02-25 12:46:16	2
68	68	1	2018-02-25 12:46:16	2
69	69	1	2018-02-25 12:46:16	2
70	70	1	2018-02-25 12:46:16	2
71	71	1	2018-02-25 12:46:16	2
72	72	1	2018-02-25 12:46:16	2
73	73	1	2018-02-25 12:46:16	2
74	74	1	2018-02-25 12:46:16	2
75	75	1	2018-02-25 12:46:16	2
76	76	1	2018-02-25 12:46:16	2
77	77	1	2018-02-25 12:46:16	2
78	84	1	2018-05-02 17:00:40	\N
79	85	1	2018-05-02 17:00:40	\N
80	86	1	2018-05-02 17:00:40	\N
81	87	1	2018-05-02 17:00:40	\N
82	88	1	2018-05-02 17:00:40	\N
83	89	1	2018-05-02 17:00:40	\N
84	90	1	2018-05-02 17:00:40	\N
85	91	1	2018-05-02 17:00:40	\N
86	92	1	2018-05-02 17:00:40	\N
87	93	1	2018-05-02 17:00:40	\N
88	94	1	2018-05-02 17:00:40	\N
89	95	1	2018-05-02 17:00:40	\N
90	96	1	2018-05-02 17:00:40	\N
91	97	1	2018-05-02 17:00:40	\N
92	98	1	2018-05-02 17:00:40	\N
93	99	1	2018-05-02 17:00:40	\N
94	100	1	2018-05-02 17:00:40	\N
95	101	1	2018-05-02 17:00:40	\N
96	102	1	2018-05-02 17:00:40	\N
97	103	1	2018-05-02 17:00:40	\N
98	104	1	2018-05-02 17:00:40	\N
99	105	1	2018-05-02 17:00:40	\N
100	106	1	2018-05-02 17:00:40	\N
101	107	1	2018-05-02 17:00:40	\N
102	108	1	2018-05-02 17:00:40	\N
103	109	1	2018-05-02 17:00:40	\N
104	110	1	2018-05-02 17:00:40	\N
105	111	1	2018-05-02 17:00:40	\N
106	112	1	2018-05-02 17:00:40	\N
107	113	1	2018-05-02 17:00:40	\N
108	114	1	2018-05-02 17:00:40	\N
109	115	1	2018-05-02 17:00:40	\N
110	116	1	2018-05-02 17:13:16	\N
111	117	1	2018-05-02 17:13:16	\N
112	118	1	2018-05-02 17:13:16	\N
113	119	1	2018-05-02 17:13:16	\N
114	120	1	2018-05-02 17:13:16	\N
115	121	1	2018-05-02 17:13:16	\N
116	122	1	2018-05-02 17:13:16	\N
117	123	1	2018-05-02 17:13:16	\N
118	124	1	2018-05-02 17:13:16	\N
119	125	1	2018-05-02 17:13:16	\N
120	126	1	2018-05-02 17:13:16	\N
121	127	1	2018-05-02 17:13:16	\N
122	128	1	2018-05-02 17:13:16	\N
123	129	1	2018-05-02 17:13:16	\N
124	130	1	2018-05-02 17:13:16	\N
125	131	1	2018-05-02 17:13:16	\N
126	132	1	2018-05-02 17:13:16	\N
127	133	1	2018-05-02 17:13:16	\N
128	134	1	2018-05-02 17:13:16	\N
129	135	1	2018-05-02 17:13:16	\N
130	136	1	2018-05-02 17:13:16	\N
131	137	1	2018-05-02 17:13:16	\N
132	138	1	2018-05-02 17:13:16	\N
133	139	1	2018-05-02 17:13:16	\N
134	140	1	2018-05-02 17:13:16	\N
135	141	1	2018-05-02 17:13:16	\N
136	142	1	2018-05-02 17:13:16	\N
137	143	1	2018-05-02 17:13:16	\N
138	144	1	2018-05-02 17:13:16	\N
139	145	1	2018-05-02 17:13:16	\N
140	146	1	2018-05-02 17:13:16	\N
141	147	1	2018-05-02 17:13:16	\N
142	148	1	2018-05-02 17:13:16	\N
143	149	1	2018-05-02 17:13:16	\N
144	150	1	2018-05-02 17:13:16	\N
145	151	1	2018-05-02 17:13:16	\N
146	152	1	2018-05-02 17:13:16	\N
147	153	1	2018-05-02 17:13:16	\N
148	154	1	2018-05-02 17:13:16	\N
149	155	1	2018-05-02 17:13:16	\N
150	156	1	2018-05-02 17:13:16	\N
151	157	1	2018-05-02 17:13:16	\N
152	158	1	2018-05-02 17:13:16	\N
153	159	1	2018-05-02 17:13:16	\N
154	160	1	2018-05-02 17:13:16	\N
155	161	1	2018-05-02 17:13:16	\N
156	162	1	2018-05-02 17:13:16	\N
157	163	1	2018-05-02 17:13:16	\N
158	164	1	2018-05-02 17:13:16	\N
159	165	1	2018-05-02 17:13:16	\N
160	166	1	2018-05-02 17:13:16	\N
161	167	1	2018-05-02 17:15:56	\N
162	168	1	2018-05-02 17:15:56	\N
164	170	1	2018-05-02 17:57:20	\N
165	171	1	2018-05-02 17:57:20	\N
166	172	1	2018-05-02 17:57:20	\N
167	173	1	2018-05-02 17:57:20	\N
168	174	1	2018-05-02 17:57:20	\N
169	175	1	2018-05-02 17:57:20	\N
170	176	1	2018-05-02 17:57:20	\N
171	177	1	2018-05-02 17:57:20	\N
172	178	1	2018-05-02 17:57:20	\N
173	179	1	2018-05-02 17:57:20	\N
174	180	1	2018-05-02 17:57:20	\N
175	181	1	2018-05-02 17:57:20	\N
176	182	1	2018-05-02 17:57:20	\N
177	183	1	2018-05-02 17:57:20	\N
178	184	1	2018-05-02 17:57:20	\N
179	185	1	2018-05-02 17:57:20	\N
180	186	1	2018-05-02 17:57:20	\N
181	187	1	2018-05-02 17:57:20	\N
182	188	1	2018-05-02 17:57:20	\N
183	189	1	2018-05-02 17:57:20	\N
184	190	1	2018-05-02 17:57:20	\N
185	191	1	2018-05-02 17:57:20	\N
186	192	1	2018-05-02 17:57:20	\N
187	193	1	2018-05-02 17:57:20	\N
188	194	1	2018-05-02 17:57:20	\N
189	195	1	2018-05-02 17:57:20	\N
190	196	1	2018-05-02 17:57:20	\N
191	197	1	2018-05-02 17:57:20	\N
192	198	1	2018-05-02 17:57:20	\N
193	199	1	2018-05-02 17:57:20	\N
194	200	1	2018-05-02 17:57:20	\N
195	201	1	2018-05-02 17:57:20	\N
196	202	1	2018-05-02 17:57:20	\N
197	203	1	2018-05-02 17:57:20	\N
198	204	1	2018-05-02 17:57:20	\N
199	205	1	2018-05-02 17:57:20	\N
200	206	1	2018-05-02 17:57:20	\N
201	207	1	2018-05-02 17:57:20	\N
202	208	1	2018-05-02 17:57:20	\N
203	209	1	2018-05-02 17:57:20	\N
204	210	1	2018-05-02 17:57:20	\N
205	211	1	2018-05-02 17:57:20	\N
206	212	1	2018-05-02 17:57:20	\N
207	213	1	2018-05-02 17:57:20	\N
208	214	1	2018-05-02 17:57:20	\N
209	215	1	2018-05-02 17:57:20	\N
210	216	1	2018-05-02 17:57:20	\N
211	217	1	2018-05-02 17:57:20	\N
212	218	1	2018-05-02 17:57:20	\N
213	219	1	2018-05-02 17:57:20	\N
214	220	1	2018-05-02 17:57:20	\N
215	221	1	2018-05-02 17:57:20	\N
216	222	1	2018-05-02 17:57:20	\N
217	223	1	2018-05-02 17:57:20	\N
218	224	1	2018-05-02 17:57:20	\N
219	225	1	2018-05-02 17:57:20	\N
220	226	1	2018-05-02 17:57:20	\N
221	227	1	2018-05-02 17:57:20	\N
222	228	1	2018-05-02 17:57:20	\N
223	229	1	2018-05-02 17:57:20	\N
224	230	1	2018-05-02 17:57:20	\N
225	231	1	2018-05-02 17:57:20	\N
226	232	1	2018-05-02 17:57:20	\N
227	233	1	2018-05-02 17:57:20	\N
228	234	1	2018-05-02 17:57:20	\N
229	235	1	2018-05-02 17:57:20	\N
230	236	1	2018-05-02 17:57:20	\N
231	237	1	2018-05-02 17:57:20	\N
232	238	1	2018-05-02 17:57:20	\N
233	239	1	2018-05-02 17:57:20	\N
234	240	1	2018-05-02 17:57:20	\N
235	241	1	2018-05-02 17:57:20	\N
236	242	1	2018-05-02 17:57:20	\N
237	243	1	2018-05-02 17:57:20	\N
238	244	1	2018-05-02 17:57:20	\N
239	245	1	2018-05-02 17:57:20	\N
240	246	1	2018-05-02 17:57:20	\N
241	247	1	2018-05-02 17:57:20	\N
242	248	1	2018-05-02 17:57:20	\N
243	249	1	2018-05-02 17:57:20	\N
244	250	1	2018-05-02 17:57:20	\N
245	251	1	2018-05-02 17:57:20	\N
246	252	1	2018-05-02 17:57:20	\N
247	253	1	2018-05-02 17:57:20	\N
248	254	1	2018-05-02 17:57:20	\N
\.


--
-- Data for Name: items_in_stock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_in_stock (iis_id, item_id, user_id, item_amount, last_amount_added, last_update, item_location) FROM stdin;
3	3	\N	3.0	0.0	\N	NONE
4	4	\N	4.0	0.0	\N	NONE
5	5	\N	5.0	0.0	\N	NONE
6	6	\N	6.0	0.0	\N	NONE
7	7	\N	7.0	0.0	\N	NONE
8	8	\N	8.0	0.0	\N	NONE
9	9	\N	9.0	0.0	\N	NONE
10	10	\N	10.0	0.0	\N	NONE
11	11	\N	11.0	0.0	\N	NONE
12	12	\N	12.0	0.0	\N	NONE
13	13	\N	13.0	0.0	\N	NONE
14	14	\N	14.0	0.0	\N	NONE
15	15	\N	15.0	0.0	\N	NONE
16	16	\N	16.0	0.0	\N	NONE
17	17	\N	17.0	0.0	\N	NONE
18	18	\N	18.0	0.0	\N	NONE
19	19	\N	19.0	0.0	\N	NONE
20	20	\N	1.0	0.0	\N	NONE
21	21	\N	2.0	0.0	\N	NONE
22	22	\N	3.0	0.0	\N	NONE
23	23	\N	4.0	0.0	\N	NONE
24	24	\N	5.0	0.0	\N	NONE
25	25	\N	6.0	0.0	\N	NONE
26	26	\N	7.0	0.0	\N	NONE
27	27	\N	8.0	0.0	\N	NONE
28	28	\N	9.0	0.0	\N	NONE
29	29	\N	10.0	0.0	\N	NONE
30	30	\N	11.0	0.0	\N	NONE
31	31	\N	12.0	0.0	\N	NONE
32	32	\N	13.0	0.0	\N	NONE
33	33	\N	14.0	0.0	\N	NONE
34	34	\N	15.0	0.0	\N	NONE
35	35	\N	16.0	0.0	\N	NONE
36	36	\N	17.0	0.0	\N	NONE
37	37	\N	18.0	0.0	\N	NONE
38	38	\N	19.0	0.0	\N	NONE
39	39	\N	1.0	0.0	\N	NONE
40	40	\N	2.0	0.0	\N	NONE
41	41	\N	3.0	0.0	\N	NONE
43	43	\N	5.0	0.0	\N	NONE
44	44	\N	6.0	0.0	\N	NONE
45	45	\N	7.0	0.0	\N	NONE
46	46	\N	8.0	0.0	\N	NONE
47	47	\N	9.0	0.0	\N	NONE
48	48	\N	10.0	0.0	\N	NONE
49	49	\N	11.0	0.0	\N	NONE
51	51	\N	13.0	0.0	\N	NONE
52	52	\N	14.0	0.0	\N	NONE
53	53	\N	15.0	0.0	\N	NONE
54	54	\N	16.0	0.0	\N	NONE
55	55	\N	17.0	0.0	\N	NONE
56	56	\N	18.0	0.0	\N	NONE
57	57	\N	19.0	0.0	\N	NONE
58	58	\N	1.0	0.0	\N	NONE
59	59	\N	2.0	0.0	\N	NONE
60	60	\N	3.0	0.0	\N	NONE
61	61	\N	4.0	0.0	\N	NONE
62	62	\N	5.0	0.0	\N	NONE
63	63	\N	6.0	0.0	\N	NONE
64	64	\N	7.0	0.0	\N	NONE
65	65	\N	8.0	0.0	\N	NONE
66	66	\N	9.0	0.0	\N	NONE
67	67	\N	10.0	0.0	\N	NONE
68	68	\N	11.0	0.0	\N	NONE
69	69	\N	12.0	0.0	\N	NONE
70	70	\N	13.0	0.0	\N	NONE
71	71	\N	14.0	0.0	\N	NONE
72	72	\N	15.0	0.0	\N	NONE
73	73	\N	16.0	0.0	\N	NONE
74	74	\N	17.0	0.0	\N	NONE
75	75	\N	18.0	0.0	\N	NONE
76	76	\N	19.0	0.0	\N	NONE
77	77	\N	1.0	0.0	\N	NONE
78	78	\N	2.0	0.0	\N	NONE
79	79	\N	3.0	0.0	\N	NONE
80	80	\N	4.0	0.0	\N	NONE
81	81	\N	5.0	0.0	\N	NONE
82	82	\N	6.0	0.0	\N	NONE
83	83	\N	7.0	0.0	\N	NONE
84	84	\N	8.0	0.0	\N	NONE
85	85	\N	9.0	0.0	\N	NONE
86	86	\N	10.0	0.0	\N	NONE
87	87	\N	11.0	0.0	\N	NONE
88	88	\N	12.0	0.0	\N	NONE
89	89	\N	13.0	0.0	\N	NONE
90	90	\N	14.0	0.0	\N	NONE
91	91	\N	15.0	0.0	\N	NONE
92	92	\N	16.0	0.0	\N	NONE
93	93	\N	17.0	0.0	\N	NONE
94	94	\N	18.0	0.0	\N	NONE
95	95	\N	19.0	0.0	\N	NONE
96	96	\N	1.0	0.0	\N	NONE
97	97	\N	2.0	0.0	\N	NONE
98	98	\N	3.0	0.0	\N	NONE
99	99	\N	4.0	0.0	\N	NONE
100	100	\N	5.0	0.0	\N	NONE
101	101	\N	6.0	0.0	\N	NONE
102	102	\N	7.0	0.0	\N	NONE
103	103	\N	8.0	0.0	\N	NONE
104	104	\N	9.0	0.0	\N	NONE
105	105	\N	10.0	0.0	\N	NONE
106	106	\N	11.0	0.0	\N	NONE
107	107	\N	12.0	0.0	\N	NONE
108	108	\N	13.0	0.0	\N	NONE
109	109	\N	14.0	0.0	\N	NONE
110	110	\N	15.0	0.0	\N	NONE
111	111	\N	16.0	0.0	\N	NONE
112	112	\N	17.0	0.0	\N	NONE
113	113	\N	18.0	0.0	\N	NONE
114	114	\N	19.0	0.0	\N	NONE
115	115	\N	1.0	0.0	\N	NONE
116	116	\N	2.0	0.0	\N	NONE
117	117	\N	3.0	0.0	\N	NONE
118	118	\N	4.0	0.0	\N	NONE
119	119	\N	5.0	0.0	\N	NONE
120	120	\N	6.0	0.0	\N	NONE
121	121	\N	7.0	0.0	\N	NONE
122	122	\N	8.0	0.0	\N	NONE
123	123	\N	9.0	0.0	\N	NONE
124	124	\N	10.0	0.0	\N	NONE
125	125	\N	11.0	0.0	\N	NONE
126	126	\N	12.0	0.0	\N	NONE
127	127	\N	13.0	0.0	\N	NONE
128	128	\N	14.0	0.0	\N	NONE
129	129	\N	15.0	0.0	\N	NONE
130	130	\N	16.0	0.0	\N	NONE
131	131	\N	17.0	0.0	\N	NONE
132	132	\N	18.0	0.0	\N	NONE
133	133	\N	19.0	0.0	\N	NONE
134	134	\N	1.0	0.0	\N	NONE
135	135	\N	2.0	0.0	\N	NONE
136	136	\N	3.0	0.0	\N	NONE
137	137	\N	4.0	0.0	\N	NONE
138	138	\N	5.0	0.0	\N	NONE
139	139	\N	6.0	0.0	\N	NONE
140	140	\N	7.0	0.0	\N	NONE
141	141	\N	8.0	0.0	\N	NONE
142	142	\N	9.0	0.0	\N	NONE
143	143	\N	10.0	0.0	\N	NONE
144	144	\N	11.0	0.0	\N	NONE
145	145	\N	12.0	0.0	\N	NONE
146	146	\N	13.0	0.0	\N	NONE
147	147	\N	14.0	0.0	\N	NONE
148	148	\N	15.0	0.0	\N	NONE
149	149	\N	16.0	0.0	\N	NONE
150	150	\N	17.0	0.0	\N	NONE
151	151	\N	18.0	0.0	\N	NONE
152	152	\N	19.0	0.0	\N	NONE
153	153	\N	1.0	0.0	\N	NONE
154	154	\N	2.0	0.0	\N	NONE
155	155	\N	3.0	0.0	\N	NONE
156	156	\N	4.0	0.0	\N	NONE
157	157	\N	5.0	0.0	\N	NONE
158	158	\N	6.0	0.0	\N	NONE
159	159	\N	7.0	0.0	\N	NONE
160	160	\N	8.0	0.0	\N	NONE
161	161	\N	9.0	0.0	\N	NONE
162	162	\N	10.0	0.0	\N	NONE
163	163	\N	11.0	0.0	\N	NONE
164	164	\N	12.0	0.0	\N	NONE
165	165	\N	13.0	0.0	\N	NONE
166	166	\N	14.0	0.0	\N	NONE
167	167	\N	15.0	0.0	\N	NONE
168	168	\N	16.0	0.0	\N	NONE
169	169	\N	17.0	0.0	\N	NONE
170	170	\N	18.0	0.0	\N	NONE
171	171	\N	19.0	0.0	\N	NONE
172	172	\N	1.0	0.0	\N	NONE
173	173	\N	2.0	0.0	\N	NONE
174	174	\N	3.0	0.0	\N	NONE
175	175	\N	4.0	0.0	\N	NONE
176	176	\N	5.0	0.0	\N	NONE
177	177	\N	6.0	0.0	\N	NONE
178	178	\N	7.0	0.0	\N	NONE
179	179	\N	8.0	0.0	\N	NONE
180	180	\N	9.0	0.0	\N	NONE
181	181	\N	10.0	0.0	\N	NONE
182	182	\N	11.0	0.0	\N	NONE
183	183	\N	12.0	0.0	\N	NONE
184	184	\N	13.0	0.0	\N	NONE
185	185	\N	14.0	0.0	\N	NONE
186	186	\N	15.0	0.0	\N	NONE
187	187	\N	16.0	0.0	\N	NONE
188	188	\N	17.0	0.0	\N	NONE
189	189	\N	18.0	0.0	\N	NONE
190	190	\N	19.0	0.0	\N	NONE
191	191	\N	1.0	0.0	\N	NONE
192	192	\N	2.0	0.0	\N	NONE
193	193	\N	3.0	0.0	\N	NONE
194	194	\N	4.0	0.0	\N	NONE
195	195	\N	5.0	0.0	\N	NONE
196	196	\N	6.0	0.0	\N	NONE
197	197	\N	7.0	0.0	\N	NONE
198	198	\N	8.0	0.0	\N	NONE
199	199	\N	9.0	0.0	\N	NONE
200	200	\N	10.0	0.0	\N	NONE
201	201	\N	11.0	0.0	\N	NONE
202	202	\N	12.0	0.0	\N	NONE
203	203	\N	13.0	0.0	\N	NONE
204	204	\N	14.0	0.0	\N	NONE
205	205	\N	15.0	0.0	\N	NONE
206	206	\N	16.0	0.0	\N	NONE
207	207	\N	17.0	0.0	\N	NONE
208	208	\N	18.0	0.0	\N	NONE
209	209	\N	19.0	0.0	\N	NONE
210	210	\N	1.0	0.0	\N	NONE
211	211	\N	2.0	0.0	\N	NONE
212	212	\N	3.0	0.0	\N	NONE
213	213	\N	4.0	0.0	\N	NONE
214	214	\N	5.0	0.0	\N	NONE
215	215	\N	6.0	0.0	\N	NONE
216	216	\N	7.0	0.0	\N	NONE
217	217	\N	8.0	0.0	\N	NONE
218	218	\N	9.0	0.0	\N	NONE
219	219	\N	10.0	0.0	\N	NONE
220	220	\N	11.0	0.0	\N	NONE
221	221	\N	12.0	0.0	\N	NONE
222	222	\N	13.0	0.0	\N	NONE
223	223	\N	14.0	0.0	\N	NONE
224	224	\N	15.0	0.0	\N	NONE
225	225	\N	16.0	0.0	\N	NONE
226	226	\N	17.0	0.0	\N	NONE
227	227	\N	18.0	0.0	\N	NONE
228	228	\N	19.0	0.0	\N	NONE
229	229	\N	1.0	0.0	\N	NONE
230	230	\N	2.0	0.0	\N	NONE
231	231	\N	3.0	0.0	\N	NONE
232	232	\N	4.0	0.0	\N	NONE
233	233	\N	5.0	0.0	\N	NONE
234	234	\N	6.0	0.0	\N	NONE
235	235	\N	7.0	0.0	\N	NONE
236	236	\N	8.0	0.0	\N	NONE
237	237	\N	9.0	0.0	\N	NONE
238	238	\N	10.0	0.0	\N	NONE
239	239	\N	11.0	0.0	\N	NONE
240	240	\N	12.0	0.0	\N	NONE
241	241	\N	13.0	0.0	\N	NONE
242	242	\N	14.0	0.0	\N	NONE
243	243	\N	15.0	0.0	\N	NONE
244	244	\N	16.0	0.0	\N	NONE
245	245	\N	17.0	0.0	\N	NONE
246	246	\N	18.0	0.0	\N	NONE
247	247	\N	19.0	0.0	\N	NONE
248	248	\N	1.0	0.0	\N	NONE
249	249	\N	2.0	0.0	\N	NONE
250	250	\N	3.0	0.0	\N	NONE
251	251	\N	4.0	0.0	\N	NONE
252	252	\N	5.0	0.0	\N	NONE
253	253	\N	6.0	0.0	\N	NONE
254	254	\N	7.0	0.0	\N	NONE
255	255	\N	8.0	0.0	\N	NONE
256	256	\N	9.0	0.0	\N	NONE
257	257	\N	10.0	0.0	\N	NONE
258	258	\N	11.0	0.0	\N	NONE
259	259	\N	12.0	0.0	\N	NONE
260	260	\N	13.0	0.0	\N	NONE
261	261	\N	14.0	0.0	\N	NONE
262	262	\N	15.0	0.0	\N	NONE
263	263	\N	16.0	0.0	\N	NONE
264	264	\N	17.0	0.0	\N	NONE
265	265	\N	18.0	0.0	\N	NONE
266	266	\N	19.0	0.0	\N	NONE
267	267	\N	1.0	0.0	\N	NONE
268	268	\N	2.0	0.0	\N	NONE
269	269	\N	3.0	0.0	\N	NONE
270	270	\N	4.0	0.0	\N	NONE
271	271	\N	5.0	0.0	\N	NONE
272	272	\N	6.0	0.0	\N	NONE
273	273	\N	7.0	0.0	\N	NONE
274	274	\N	8.0	0.0	\N	NONE
275	275	\N	9.0	0.0	\N	NONE
276	276	\N	10.0	0.0	\N	NONE
277	277	\N	11.0	0.0	\N	NONE
278	278	\N	12.0	0.0	\N	NONE
279	279	\N	13.0	0.0	\N	NONE
280	280	\N	14.0	0.0	\N	NONE
281	281	\N	15.0	0.0	\N	NONE
282	282	\N	16.0	0.0	\N	NONE
283	283	\N	17.0	0.0	\N	NONE
284	284	\N	18.0	0.0	\N	NONE
285	285	\N	19.0	0.0	\N	NONE
286	286	\N	1.0	0.0	\N	NONE
287	287	\N	2.0	0.0	\N	NONE
288	288	\N	3.0	0.0	\N	NONE
289	289	\N	4.0	0.0	\N	NONE
290	290	\N	5.0	0.0	\N	NONE
291	291	\N	6.0	0.0	\N	NONE
292	292	\N	7.0	0.0	\N	NONE
293	293	\N	8.0	0.0	\N	NONE
294	294	\N	9.0	0.0	\N	NONE
295	295	\N	10.0	0.0	\N	NONE
296	296	\N	11.0	0.0	\N	NONE
297	297	\N	12.0	0.0	\N	NONE
298	298	\N	13.0	0.0	\N	NONE
299	299	\N	14.0	0.0	\N	NONE
300	300	\N	15.0	0.0	\N	NONE
301	301	\N	16.0	0.0	\N	NONE
302	302	\N	17.0	0.0	\N	NONE
303	303	\N	18.0	0.0	\N	NONE
304	304	\N	19.0	0.0	\N	NONE
305	305	\N	1.0	0.0	\N	NONE
306	306	\N	2.0	0.0	\N	NONE
307	307	\N	3.0	0.0	\N	NONE
308	308	\N	4.0	0.0	\N	NONE
309	309	\N	5.0	0.0	\N	NONE
310	310	\N	6.0	0.0	\N	NONE
311	311	\N	7.0	0.0	\N	NONE
312	312	\N	8.0	0.0	\N	NONE
313	313	\N	9.0	0.0	\N	NONE
314	314	\N	10.0	0.0	\N	NONE
315	315	\N	11.0	0.0	\N	NONE
316	316	\N	12.0	0.0	\N	NONE
317	317	\N	13.0	0.0	\N	NONE
318	318	\N	14.0	0.0	\N	NONE
319	319	\N	15.0	0.0	\N	NONE
320	320	\N	16.0	0.0	\N	NONE
321	321	\N	17.0	0.0	\N	NONE
322	322	\N	18.0	0.0	\N	NONE
323	323	\N	19.0	0.0	\N	NONE
324	324	\N	1.0	0.0	\N	NONE
325	325	\N	2.0	0.0	\N	NONE
326	326	\N	3.0	0.0	\N	NONE
327	327	\N	4.0	0.0	\N	NONE
328	328	\N	5.0	0.0	\N	NONE
329	329	\N	6.0	0.0	\N	NONE
330	330	\N	7.0	0.0	\N	NONE
331	331	\N	8.0	0.0	\N	NONE
332	332	\N	9.0	0.0	\N	NONE
333	333	\N	10.0	0.0	\N	NONE
334	334	\N	11.0	0.0	\N	NONE
335	335	\N	12.0	0.0	\N	NONE
337	337	\N	14.0	0.0	\N	NONE
338	338	\N	15.0	0.0	\N	NONE
339	339	\N	16.0	0.0	\N	NONE
340	340	\N	17.0	0.0	\N	NONE
341	341	\N	18.0	0.0	\N	NONE
342	342	\N	19.0	0.0	\N	NONE
343	343	\N	1.0	0.0	\N	NONE
344	344	\N	2.0	0.0	\N	NONE
345	345	\N	3.0	0.0	\N	NONE
346	346	\N	4.0	0.0	\N	NONE
347	347	\N	5.0	0.0	\N	NONE
348	348	\N	6.0	0.0	\N	NONE
349	349	\N	7.0	0.0	\N	NONE
350	350	\N	8.0	0.0	\N	NONE
351	351	\N	9.0	0.0	\N	NONE
352	352	\N	10.0	0.0	\N	NONE
353	353	\N	11.0	0.0	\N	NONE
354	354	\N	12.0	0.0	\N	NONE
355	355	\N	13.0	0.0	\N	NONE
356	356	\N	14.0	0.0	\N	NONE
357	357	\N	15.0	0.0	\N	NONE
358	358	\N	16.0	0.0	\N	NONE
359	359	\N	17.0	0.0	\N	NONE
360	360	\N	18.0	0.0	\N	NONE
361	361	\N	19.0	0.0	\N	NONE
362	362	\N	1.0	0.0	\N	NONE
363	363	\N	2.0	0.0	\N	NONE
364	364	\N	3.0	0.0	\N	NONE
365	365	\N	4.0	0.0	\N	NONE
366	366	\N	5.0	0.0	\N	NONE
367	367	\N	6.0	0.0	\N	NONE
368	368	\N	7.0	0.0	\N	NONE
369	369	\N	8.0	0.0	\N	NONE
370	370	\N	9.0	0.0	\N	NONE
371	371	\N	10.0	0.0	\N	NONE
372	372	\N	11.0	0.0	\N	NONE
373	373	\N	12.0	0.0	\N	NONE
374	374	\N	13.0	0.0	\N	NONE
375	375	\N	14.0	0.0	\N	NONE
376	376	\N	15.0	0.0	\N	NONE
377	377	\N	16.0	0.0	\N	NONE
378	378	\N	17.0	0.0	\N	NONE
379	379	\N	18.0	0.0	\N	NONE
380	380	\N	19.0	0.0	\N	NONE
381	381	\N	1.0	0.0	\N	NONE
382	382	\N	2.0	0.0	\N	NONE
383	383	\N	3.0	0.0	\N	NONE
384	384	\N	4.0	0.0	\N	NONE
385	385	\N	5.0	0.0	\N	NONE
386	386	\N	6.0	0.0	\N	NONE
387	387	\N	7.0	0.0	\N	NONE
388	388	\N	8.0	0.0	\N	NONE
389	389	\N	9.0	0.0	\N	NONE
390	390	\N	10.0	0.0	\N	NONE
391	391	\N	11.0	0.0	\N	NONE
392	392	\N	12.0	0.0	\N	NONE
393	393	\N	13.0	0.0	\N	NONE
394	394	\N	14.0	0.0	\N	NONE
395	395	\N	15.0	0.0	\N	NONE
396	396	\N	16.0	0.0	\N	NONE
397	397	\N	17.0	0.0	\N	NONE
398	398	\N	18.0	0.0	\N	NONE
399	399	\N	19.0	0.0	\N	NONE
400	400	\N	1.0	0.0	\N	NONE
401	401	\N	2.0	0.0	\N	NONE
402	402	\N	3.0	0.0	\N	NONE
403	403	\N	4.0	0.0	\N	NONE
404	404	\N	5.0	0.0	\N	NONE
405	405	\N	6.0	0.0	\N	NONE
406	406	\N	7.0	0.0	\N	NONE
407	407	\N	8.0	0.0	\N	NONE
408	408	\N	9.0	0.0	\N	NONE
409	409	\N	10.0	0.0	\N	NONE
410	410	\N	11.0	0.0	\N	NONE
411	411	\N	12.0	0.0	\N	NONE
412	412	\N	13.0	0.0	\N	NONE
413	413	\N	14.0	0.0	\N	NONE
414	414	\N	15.0	0.0	\N	NONE
415	415	\N	16.0	0.0	\N	NONE
416	416	\N	17.0	0.0	\N	NONE
417	417	\N	18.0	0.0	\N	NONE
418	418	\N	19.0	0.0	\N	NONE
419	419	\N	1.0	0.0	\N	NONE
420	420	\N	2.0	0.0	\N	NONE
421	421	\N	3.0	0.0	\N	NONE
422	422	\N	4.0	0.0	\N	NONE
423	423	\N	5.0	0.0	\N	NONE
424	424	\N	6.0	0.0	\N	NONE
425	425	\N	7.0	0.0	\N	NONE
426	426	\N	8.0	0.0	\N	NONE
427	427	\N	9.0	0.0	\N	NONE
428	428	\N	10.0	0.0	\N	NONE
429	429	\N	11.0	0.0	\N	NONE
430	430	\N	12.0	0.0	\N	NONE
431	431	\N	13.0	0.0	\N	NONE
432	432	\N	14.0	0.0	\N	NONE
433	433	\N	15.0	0.0	\N	NONE
434	434	\N	16.0	0.0	\N	NONE
435	435	\N	17.0	0.0	\N	NONE
436	436	\N	18.0	0.0	\N	NONE
437	437	\N	19.0	0.0	\N	NONE
438	438	\N	1.0	0.0	\N	NONE
439	439	\N	2.0	0.0	\N	NONE
440	440	\N	3.0	0.0	\N	NONE
441	441	\N	4.0	0.0	\N	NONE
442	442	\N	5.0	0.0	\N	NONE
443	443	\N	6.0	0.0	\N	NONE
444	444	\N	7.0	0.0	\N	NONE
445	445	\N	8.0	0.0	\N	NONE
446	446	\N	9.0	0.0	\N	NONE
447	447	\N	10.0	0.0	\N	NONE
448	448	\N	11.0	0.0	\N	NONE
449	449	\N	12.0	0.0	\N	NONE
450	450	\N	13.0	0.0	\N	NONE
451	451	\N	14.0	0.0	\N	NONE
452	452	\N	15.0	0.0	\N	NONE
453	453	\N	16.0	0.0	\N	NONE
454	454	\N	17.0	0.0	\N	NONE
455	455	\N	18.0	0.0	\N	NONE
456	456	\N	19.0	0.0	\N	NONE
457	457	\N	1.0	0.0	\N	NONE
458	458	\N	2.0	0.0	\N	NONE
459	459	\N	3.0	0.0	\N	NONE
460	460	\N	4.0	0.0	\N	NONE
461	461	\N	5.0	0.0	\N	NONE
462	462	\N	6.0	0.0	\N	NONE
463	463	\N	7.0	0.0	\N	NONE
464	464	\N	8.0	0.0	\N	NONE
465	465	\N	9.0	0.0	\N	NONE
466	466	\N	10.0	0.0	\N	NONE
467	467	\N	11.0	0.0	\N	NONE
468	468	\N	12.0	0.0	\N	NONE
469	469	\N	13.0	0.0	\N	NONE
470	470	\N	14.0	0.0	\N	NONE
471	471	\N	15.0	0.0	\N	NONE
472	472	\N	16.0	0.0	\N	NONE
473	473	\N	17.0	0.0	\N	NONE
474	474	\N	18.0	0.0	\N	NONE
475	475	\N	19.0	0.0	\N	NONE
476	476	\N	1.0	0.0	\N	NONE
477	477	\N	2.0	0.0	\N	NONE
478	478	\N	3.0	0.0	\N	NONE
479	479	\N	4.0	0.0	\N	NONE
480	480	\N	5.0	0.0	\N	NONE
481	481	\N	6.0	0.0	\N	NONE
482	482	\N	7.0	0.0	\N	NONE
483	483	\N	8.0	0.0	\N	NONE
484	484	\N	9.0	0.0	\N	NONE
485	485	\N	10.0	0.0	\N	NONE
486	486	\N	11.0	0.0	\N	NONE
487	487	\N	12.0	0.0	\N	NONE
488	488	\N	13.0	0.0	\N	NONE
489	489	\N	14.0	0.0	\N	NONE
490	490	\N	15.0	0.0	\N	NONE
491	491	\N	16.0	0.0	\N	NONE
492	492	\N	17.0	0.0	\N	NONE
493	493	\N	18.0	0.0	\N	NONE
494	494	\N	19.0	0.0	\N	NONE
495	495	\N	1.0	0.0	\N	NONE
496	496	\N	2.0	0.0	\N	NONE
497	497	\N	3.0	0.0	\N	NONE
498	498	\N	4.0	0.0	\N	NONE
499	499	\N	5.0	0.0	\N	NONE
500	500	\N	6.0	0.0	\N	NONE
501	501	\N	7.0	0.0	\N	NONE
502	502	\N	8.0	0.0	\N	NONE
503	503	\N	9.0	0.0	\N	NONE
504	504	\N	10.0	0.0	\N	NONE
505	505	\N	11.0	0.0	\N	NONE
506	506	\N	12.0	0.0	\N	NONE
507	507	\N	13.0	0.0	\N	NONE
508	508	\N	14.0	0.0	\N	NONE
509	509	\N	15.0	0.0	\N	NONE
510	510	\N	16.0	0.0	\N	NONE
511	511	\N	17.0	0.0	\N	NONE
512	512	\N	18.0	0.0	\N	NONE
513	513	\N	19.0	0.0	\N	NONE
514	514	\N	1.0	0.0	\N	NONE
515	515	\N	2.0	0.0	\N	NONE
516	516	\N	3.0	0.0	\N	NONE
517	517	\N	4.0	0.0	\N	NONE
518	518	\N	5.0	0.0	\N	NONE
519	519	\N	6.0	0.0	\N	NONE
520	520	\N	7.0	0.0	\N	NONE
521	521	\N	8.0	0.0	\N	NONE
522	522	\N	9.0	0.0	\N	NONE
523	523	\N	10.0	0.0	\N	NONE
524	524	\N	11.0	0.0	\N	NONE
525	525	\N	12.0	0.0	\N	NONE
526	526	\N	13.0	0.0	\N	NONE
527	527	\N	14.0	0.0	\N	NONE
528	528	\N	15.0	0.0	\N	NONE
529	529	\N	16.0	0.0	\N	NONE
530	530	\N	17.0	0.0	\N	NONE
531	531	\N	18.0	0.0	\N	NONE
532	532	\N	19.0	0.0	\N	NONE
533	533	\N	1.0	0.0	\N	NONE
534	534	\N	2.0	0.0	\N	NONE
535	535	\N	3.0	0.0	\N	NONE
536	536	\N	4.0	0.0	\N	NONE
537	537	\N	5.0	0.0	\N	NONE
538	538	\N	6.0	0.0	\N	NONE
539	539	\N	7.0	0.0	\N	NONE
540	540	\N	8.0	0.0	\N	NONE
541	541	\N	9.0	0.0	\N	NONE
542	542	\N	10.0	0.0	\N	NONE
543	543	\N	11.0	0.0	\N	NONE
544	544	\N	12.0	0.0	\N	NONE
545	545	\N	13.0	0.0	\N	NONE
546	546	\N	14.0	0.0	\N	NONE
547	547	\N	15.0	0.0	\N	NONE
548	548	\N	16.0	0.0	\N	NONE
549	549	\N	17.0	0.0	\N	NONE
550	550	\N	18.0	0.0	\N	NONE
551	551	\N	19.0	0.0	\N	NONE
552	552	\N	1.0	0.0	\N	NONE
553	553	\N	2.0	0.0	\N	NONE
554	554	\N	3.0	0.0	\N	NONE
555	555	\N	4.0	0.0	\N	NONE
556	556	\N	5.0	0.0	\N	NONE
557	557	\N	6.0	0.0	\N	NONE
558	558	\N	7.0	0.0	\N	NONE
559	559	\N	8.0	0.0	\N	NONE
560	560	\N	9.0	0.0	\N	NONE
561	561	\N	10.0	0.0	\N	NONE
562	562	\N	11.0	0.0	\N	NONE
563	563	\N	12.0	0.0	\N	NONE
564	564	\N	13.0	0.0	\N	NONE
565	565	\N	14.0	0.0	\N	NONE
566	566	\N	15.0	0.0	\N	NONE
567	567	\N	16.0	0.0	\N	NONE
568	568	\N	17.0	0.0	\N	NONE
569	569	\N	18.0	0.0	\N	NONE
570	570	\N	19.0	0.0	\N	NONE
571	571	\N	1.0	0.0	\N	NONE
572	572	\N	2.0	0.0	\N	NONE
573	573	\N	3.0	0.0	\N	NONE
574	574	\N	4.0	0.0	\N	NONE
575	575	\N	5.0	0.0	\N	NONE
576	576	\N	6.0	0.0	\N	NONE
577	577	\N	7.0	0.0	\N	NONE
578	578	\N	8.0	0.0	\N	NONE
579	579	\N	9.0	0.0	\N	NONE
580	580	\N	10.0	0.0	\N	NONE
581	581	\N	11.0	0.0	\N	NONE
582	582	\N	12.0	0.0	\N	NONE
583	583	\N	13.0	0.0	\N	NONE
584	584	\N	14.0	0.0	\N	NONE
585	585	\N	15.0	0.0	\N	NONE
586	586	\N	16.0	0.0	\N	NONE
587	587	\N	17.0	0.0	\N	NONE
588	588	\N	18.0	0.0	\N	NONE
589	589	\N	19.0	0.0	\N	NONE
590	590	\N	1.0	0.0	\N	NONE
591	591	\N	2.0	0.0	\N	NONE
592	592	\N	3.0	0.0	\N	NONE
593	593	\N	4.0	0.0	\N	NONE
594	594	\N	5.0	0.0	\N	NONE
595	595	\N	6.0	0.0	\N	NONE
596	596	\N	7.0	0.0	\N	NONE
597	597	\N	8.0	0.0	\N	NONE
598	598	\N	9.0	0.0	\N	NONE
599	599	\N	10.0	0.0	\N	NONE
600	600	\N	11.0	0.0	\N	NONE
601	601	\N	12.0	0.0	\N	NONE
602	602	\N	13.0	0.0	\N	NONE
603	603	\N	14.0	0.0	\N	NONE
604	604	\N	15.0	0.0	\N	NONE
605	605	\N	16.0	0.0	\N	NONE
606	606	\N	17.0	0.0	\N	NONE
607	607	\N	18.0	0.0	\N	NONE
608	608	\N	19.0	0.0	\N	NONE
609	609	\N	1.0	0.0	\N	NONE
610	610	\N	2.0	0.0	\N	NONE
611	611	\N	3.0	0.0	\N	NONE
612	612	\N	4.0	0.0	\N	NONE
613	613	\N	5.0	0.0	\N	NONE
614	614	\N	6.0	0.0	\N	NONE
615	615	\N	7.0	0.0	\N	NONE
616	616	\N	8.0	0.0	\N	NONE
617	617	\N	9.0	0.0	\N	NONE
618	618	\N	10.0	0.0	\N	NONE
619	619	\N	11.0	0.0	\N	NONE
620	620	\N	12.0	0.0	\N	NONE
621	621	\N	13.0	0.0	\N	NONE
622	622	\N	14.0	0.0	\N	NONE
623	623	\N	15.0	0.0	\N	NONE
624	624	\N	16.0	0.0	\N	NONE
625	625	\N	17.0	0.0	\N	NONE
626	626	\N	18.0	0.0	\N	NONE
627	627	\N	19.0	0.0	\N	NONE
628	628	\N	1.0	0.0	\N	NONE
629	629	\N	2.0	0.0	\N	NONE
630	630	\N	3.0	0.0	\N	NONE
631	631	\N	4.0	0.0	\N	NONE
632	632	\N	5.0	0.0	\N	NONE
633	633	\N	6.0	0.0	\N	NONE
634	634	\N	7.0	0.0	\N	NONE
635	635	\N	8.0	0.0	\N	NONE
636	636	\N	9.0	0.0	\N	NONE
637	637	\N	10.0	0.0	\N	NONE
638	638	\N	11.0	0.0	\N	NONE
639	639	\N	12.0	0.0	\N	NONE
640	640	\N	13.0	0.0	\N	NONE
641	641	\N	14.0	0.0	\N	NONE
642	642	\N	15.0	0.0	\N	NONE
643	643	\N	16.0	0.0	\N	NONE
644	644	\N	17.0	0.0	\N	NONE
645	645	\N	18.0	0.0	\N	NONE
646	646	\N	19.0	0.0	\N	NONE
647	647	\N	1.0	0.0	\N	NONE
648	648	\N	2.0	0.0	\N	NONE
649	649	\N	3.0	0.0	\N	NONE
650	650	\N	4.0	0.0	\N	NONE
651	651	\N	5.0	0.0	\N	NONE
652	652	\N	6.0	0.0	\N	NONE
653	653	\N	7.0	0.0	\N	NONE
654	654	\N	8.0	0.0	\N	NONE
655	655	\N	9.0	0.0	\N	NONE
656	656	\N	10.0	0.0	\N	NONE
657	657	\N	11.0	0.0	\N	NONE
658	658	\N	12.0	0.0	\N	NONE
659	659	\N	13.0	0.0	\N	NONE
660	660	\N	14.0	0.0	\N	NONE
661	661	\N	15.0	0.0	\N	NONE
662	662	\N	16.0	0.0	\N	NONE
663	663	\N	17.0	0.0	\N	NONE
664	664	\N	18.0	0.0	\N	NONE
665	665	\N	19.0	0.0	\N	NONE
666	666	\N	1.0	0.0	\N	NONE
667	667	\N	2.0	0.0	\N	NONE
668	668	\N	3.0	0.0	\N	NONE
669	669	\N	4.0	0.0	\N	NONE
670	670	\N	5.0	0.0	\N	NONE
671	671	\N	6.0	0.0	\N	NONE
672	672	\N	7.0	0.0	\N	NONE
673	673	\N	8.0	0.0	\N	NONE
674	674	\N	9.0	0.0	\N	NONE
675	675	\N	10.0	0.0	\N	NONE
676	676	\N	11.0	0.0	\N	NONE
677	677	\N	12.0	0.0	\N	NONE
678	678	\N	13.0	0.0	\N	NONE
679	679	\N	14.0	0.0	\N	NONE
680	680	\N	15.0	0.0	\N	NONE
681	681	\N	16.0	0.0	\N	NONE
682	682	\N	17.0	0.0	\N	NONE
683	683	\N	18.0	0.0	\N	NONE
684	684	\N	19.0	0.0	\N	NONE
685	685	\N	1.0	0.0	\N	NONE
686	686	\N	2.0	0.0	\N	NONE
687	687	\N	3.0	0.0	\N	NONE
688	688	\N	4.0	0.0	\N	NONE
689	689	\N	5.0	0.0	\N	NONE
690	690	\N	6.0	0.0	\N	NONE
691	691	\N	7.0	0.0	\N	NONE
692	692	\N	8.0	0.0	\N	NONE
693	693	\N	9.0	0.0	\N	NONE
694	694	\N	10.0	0.0	\N	NONE
695	695	\N	11.0	0.0	\N	NONE
696	696	\N	12.0	0.0	\N	NONE
697	697	\N	13.0	0.0	\N	NONE
698	698	\N	14.0	0.0	\N	NONE
699	699	\N	15.0	0.0	\N	NONE
700	700	\N	16.0	0.0	\N	NONE
701	701	\N	17.0	0.0	\N	NONE
702	702	\N	18.0	0.0	\N	NONE
703	703	\N	19.0	0.0	\N	NONE
704	704	\N	1.0	0.0	\N	NONE
705	705	\N	2.0	0.0	\N	NONE
706	706	\N	3.0	0.0	\N	NONE
707	707	\N	4.0	0.0	\N	NONE
708	708	\N	5.0	0.0	\N	NONE
709	709	\N	6.0	0.0	\N	NONE
710	710	\N	7.0	0.0	\N	NONE
711	711	\N	8.0	0.0	\N	NONE
712	712	\N	9.0	0.0	\N	NONE
713	713	\N	10.0	0.0	\N	NONE
714	714	\N	11.0	0.0	\N	NONE
715	715	\N	12.0	0.0	\N	NONE
716	716	\N	13.0	0.0	\N	NONE
717	717	\N	14.0	0.0	\N	NONE
718	718	\N	15.0	0.0	\N	NONE
719	719	\N	16.0	0.0	\N	NONE
720	720	\N	17.0	0.0	\N	NONE
721	721	\N	18.0	0.0	\N	NONE
722	722	\N	19.0	0.0	\N	NONE
723	723	\N	1.0	0.0	\N	NONE
724	724	\N	2.0	0.0	\N	NONE
725	725	\N	3.0	0.0	\N	NONE
726	726	\N	4.0	0.0	\N	NONE
727	727	\N	5.0	0.0	\N	NONE
728	728	\N	6.0	0.0	\N	NONE
729	729	\N	7.0	0.0	\N	NONE
730	730	\N	8.0	0.0	\N	NONE
731	731	\N	9.0	0.0	\N	NONE
732	732	\N	10.0	0.0	\N	NONE
733	733	\N	11.0	0.0	\N	NONE
734	734	\N	12.0	0.0	\N	NONE
735	735	\N	13.0	0.0	\N	NONE
736	736	\N	14.0	0.0	\N	NONE
737	737	\N	15.0	0.0	\N	NONE
738	738	\N	16.0	0.0	\N	NONE
739	739	\N	17.0	0.0	\N	NONE
740	740	\N	18.0	0.0	\N	NONE
741	741	\N	19.0	0.0	\N	NONE
742	742	\N	1.0	0.0	\N	NONE
743	743	\N	2.0	0.0	\N	NONE
744	744	\N	3.0	0.0	\N	NONE
745	745	\N	4.0	0.0	\N	NONE
746	746	\N	5.0	0.0	\N	NONE
747	747	\N	6.0	0.0	\N	NONE
748	748	\N	7.0	0.0	\N	NONE
749	749	\N	8.0	0.0	\N	NONE
750	750	\N	9.0	0.0	\N	NONE
751	751	\N	10.0	0.0	\N	NONE
752	752	\N	11.0	0.0	\N	NONE
753	753	\N	12.0	0.0	\N	NONE
754	754	\N	13.0	0.0	\N	NONE
755	755	\N	14.0	0.0	\N	NONE
756	756	\N	15.0	0.0	\N	NONE
757	757	\N	16.0	0.0	\N	NONE
758	758	\N	17.0	0.0	\N	NONE
759	759	\N	18.0	0.0	\N	NONE
760	760	\N	19.0	0.0	\N	NONE
761	761	\N	1.0	0.0	\N	NONE
762	762	\N	2.0	0.0	\N	NONE
763	763	\N	3.0	0.0	\N	NONE
764	764	\N	4.0	0.0	\N	NONE
765	765	\N	5.0	0.0	\N	NONE
766	766	\N	6.0	0.0	\N	NONE
767	767	\N	7.0	0.0	\N	NONE
768	768	\N	8.0	0.0	\N	NONE
769	769	\N	9.0	0.0	\N	NONE
770	770	\N	10.0	0.0	\N	NONE
771	771	\N	11.0	0.0	\N	NONE
772	772	\N	12.0	0.0	\N	NONE
773	773	\N	13.0	0.0	\N	NONE
774	774	\N	14.0	0.0	\N	NONE
775	775	\N	15.0	0.0	\N	NONE
776	776	\N	16.0	0.0	\N	NONE
777	777	\N	17.0	0.0	\N	NONE
778	778	\N	18.0	0.0	\N	NONE
779	779	\N	19.0	0.0	\N	NONE
780	780	\N	1.0	0.0	\N	NONE
781	781	\N	2.0	0.0	\N	NONE
782	782	\N	3.0	0.0	\N	NONE
783	783	\N	4.0	0.0	\N	NONE
784	784	\N	5.0	0.0	\N	NONE
785	785	\N	6.0	0.0	\N	NONE
786	786	\N	7.0	0.0	\N	NONE
787	787	\N	8.0	0.0	\N	NONE
788	788	\N	9.0	0.0	\N	NONE
789	789	\N	10.0	0.0	\N	NONE
790	790	\N	11.0	0.0	\N	NONE
791	791	\N	12.0	0.0	\N	NONE
792	792	\N	13.0	0.0	\N	NONE
793	793	\N	14.0	0.0	\N	NONE
794	794	\N	15.0	0.0	\N	NONE
795	795	\N	16.0	0.0	\N	NONE
796	796	\N	17.0	0.0	\N	NONE
797	797	\N	18.0	0.0	\N	NONE
798	798	\N	19.0	0.0	\N	NONE
799	799	\N	1.0	0.0	\N	NONE
800	800	\N	2.0	0.0	\N	NONE
801	801	\N	3.0	0.0	\N	NONE
802	802	\N	4.0	0.0	\N	NONE
803	803	\N	5.0	0.0	\N	NONE
804	804	\N	6.0	0.0	\N	NONE
805	805	\N	7.0	0.0	\N	NONE
806	806	\N	8.0	0.0	\N	NONE
807	807	\N	9.0	0.0	\N	NONE
808	808	\N	10.0	0.0	\N	NONE
809	809	\N	11.0	0.0	\N	NONE
810	810	\N	12.0	0.0	\N	NONE
811	811	\N	13.0	0.0	\N	NONE
812	812	\N	14.0	0.0	\N	NONE
813	813	\N	15.0	0.0	\N	NONE
814	814	\N	16.0	0.0	\N	NONE
815	815	\N	17.0	0.0	\N	NONE
816	816	\N	18.0	0.0	\N	NONE
817	817	\N	19.0	0.0	\N	NONE
818	818	\N	1.0	0.0	\N	NONE
819	819	\N	2.0	0.0	\N	NONE
820	820	\N	3.0	0.0	\N	NONE
821	821	\N	4.0	0.0	\N	NONE
822	822	\N	5.0	0.0	\N	NONE
823	823	\N	6.0	0.0	\N	NONE
824	824	\N	7.0	0.0	\N	NONE
825	825	\N	8.0	0.0	\N	NONE
826	826	\N	9.0	0.0	\N	NONE
827	827	\N	10.0	0.0	\N	NONE
828	828	\N	11.0	0.0	\N	NONE
829	829	\N	12.0	0.0	\N	NONE
830	830	\N	13.0	0.0	\N	NONE
831	831	\N	14.0	0.0	\N	NONE
832	832	\N	15.0	0.0	\N	NONE
833	833	\N	16.0	0.0	\N	NONE
834	834	\N	17.0	0.0	\N	NONE
835	835	\N	18.0	0.0	\N	NONE
836	836	\N	19.0	0.0	\N	NONE
837	837	\N	1.0	0.0	\N	NONE
838	838	\N	2.0	0.0	\N	NONE
839	839	\N	3.0	0.0	\N	NONE
840	840	\N	4.0	0.0	\N	NONE
841	841	\N	5.0	0.0	\N	NONE
842	842	\N	6.0	0.0	\N	NONE
843	843	\N	7.0	0.0	\N	NONE
844	844	\N	8.0	0.0	\N	NONE
845	845	\N	9.0	0.0	\N	NONE
846	846	\N	10.0	0.0	\N	NONE
847	847	\N	11.0	0.0	\N	NONE
848	848	\N	12.0	0.0	\N	NONE
849	849	\N	13.0	0.0	\N	NONE
850	850	\N	14.0	0.0	\N	NONE
851	851	\N	15.0	0.0	\N	NONE
852	852	\N	16.0	0.0	\N	NONE
853	853	\N	17.0	0.0	\N	NONE
854	854	\N	18.0	0.0	\N	NONE
855	855	\N	19.0	0.0	\N	NONE
856	856	\N	1.0	0.0	\N	NONE
857	857	\N	2.0	0.0	\N	NONE
858	858	\N	3.0	0.0	\N	NONE
859	859	\N	4.0	0.0	\N	NONE
860	860	\N	5.0	0.0	\N	NONE
861	861	\N	6.0	0.0	\N	NONE
862	862	\N	7.0	0.0	\N	NONE
863	863	\N	8.0	0.0	\N	NONE
864	864	\N	9.0	0.0	\N	NONE
865	865	\N	10.0	0.0	\N	NONE
866	866	\N	11.0	0.0	\N	NONE
867	867	\N	12.0	0.0	\N	NONE
868	868	\N	13.0	0.0	\N	NONE
869	869	\N	14.0	0.0	\N	NONE
870	870	\N	15.0	0.0	\N	NONE
871	871	\N	16.0	0.0	\N	NONE
872	872	\N	17.0	0.0	\N	NONE
873	873	\N	18.0	0.0	\N	NONE
874	874	\N	19.0	0.0	\N	NONE
875	875	\N	1.0	0.0	\N	NONE
876	876	\N	2.0	0.0	\N	NONE
877	877	\N	3.0	0.0	\N	NONE
878	878	\N	4.0	0.0	\N	NONE
879	879	\N	5.0	0.0	\N	NONE
880	880	\N	6.0	0.0	\N	NONE
881	881	\N	7.0	0.0	\N	NONE
882	882	\N	8.0	0.0	\N	NONE
883	883	\N	9.0	0.0	\N	NONE
884	884	\N	10.0	0.0	\N	NONE
885	885	\N	11.0	0.0	\N	NONE
886	886	\N	12.0	0.0	\N	NONE
887	887	\N	13.0	0.0	\N	NONE
888	888	\N	14.0	0.0	\N	NONE
889	889	\N	15.0	0.0	\N	NONE
890	890	\N	16.0	0.0	\N	NONE
891	891	\N	17.0	0.0	\N	NONE
892	892	\N	18.0	0.0	\N	NONE
893	893	\N	19.0	0.0	\N	NONE
894	894	\N	1.0	0.0	\N	NONE
895	895	\N	2.0	0.0	\N	NONE
896	896	\N	3.0	0.0	\N	NONE
897	897	\N	4.0	0.0	\N	NONE
898	898	\N	5.0	0.0	\N	NONE
899	899	\N	6.0	0.0	\N	NONE
900	900	\N	7.0	0.0	\N	NONE
901	901	\N	8.0	0.0	\N	NONE
902	902	\N	9.0	0.0	\N	NONE
903	903	\N	10.0	0.0	\N	NONE
904	904	\N	11.0	0.0	\N	NONE
905	905	\N	12.0	0.0	\N	NONE
906	906	\N	13.0	0.0	\N	NONE
907	907	\N	14.0	0.0	\N	NONE
908	908	\N	15.0	0.0	\N	NONE
909	909	\N	16.0	0.0	\N	NONE
910	910	\N	17.0	0.0	\N	NONE
911	911	\N	18.0	0.0	\N	NONE
912	912	\N	19.0	0.0	\N	NONE
913	913	\N	1.0	0.0	\N	NONE
914	914	\N	2.0	0.0	\N	NONE
915	915	\N	3.0	0.0	\N	NONE
916	916	\N	4.0	0.0	\N	NONE
917	917	\N	5.0	0.0	\N	NONE
918	918	\N	6.0	0.0	\N	NONE
919	919	\N	7.0	0.0	\N	NONE
920	920	\N	8.0	0.0	\N	NONE
921	921	\N	9.0	0.0	\N	NONE
922	922	\N	10.0	0.0	\N	NONE
923	923	\N	11.0	0.0	\N	NONE
924	924	\N	12.0	0.0	\N	NONE
925	925	\N	13.0	0.0	\N	NONE
926	926	\N	14.0	0.0	\N	NONE
927	927	\N	15.0	0.0	\N	NONE
928	928	\N	16.0	0.0	\N	NONE
929	929	\N	17.0	0.0	\N	NONE
930	930	\N	18.0	0.0	\N	NONE
931	931	\N	19.0	0.0	\N	NONE
932	932	\N	1.0	0.0	\N	NONE
933	933	\N	2.0	0.0	\N	NONE
934	934	\N	3.0	0.0	\N	NONE
935	935	\N	4.0	0.0	\N	NONE
936	936	\N	5.0	0.0	\N	NONE
937	937	\N	6.0	0.0	\N	NONE
938	938	\N	7.0	0.0	\N	NONE
939	939	\N	8.0	0.0	\N	NONE
940	940	\N	9.0	0.0	\N	NONE
941	941	\N	10.0	0.0	\N	NONE
942	942	\N	11.0	0.0	\N	NONE
943	943	\N	12.0	0.0	\N	NONE
944	944	\N	13.0	0.0	\N	NONE
945	945	\N	14.0	0.0	\N	NONE
946	946	\N	15.0	0.0	\N	NONE
947	947	\N	16.0	0.0	\N	NONE
948	948	\N	17.0	0.0	\N	NONE
949	949	\N	18.0	0.0	\N	NONE
950	950	\N	19.0	0.0	\N	NONE
951	951	\N	1.0	0.0	\N	NONE
952	952	\N	2.0	0.0	\N	NONE
953	953	\N	3.0	0.0	\N	NONE
954	954	\N	4.0	0.0	\N	NONE
955	955	\N	5.0	0.0	\N	NONE
956	956	\N	6.0	0.0	\N	NONE
957	957	\N	7.0	0.0	\N	NONE
958	958	\N	8.0	0.0	\N	NONE
959	959	\N	9.0	0.0	\N	NONE
960	960	\N	10.0	0.0	\N	NONE
961	961	\N	11.0	0.0	\N	NONE
962	962	\N	12.0	0.0	\N	NONE
963	963	\N	13.0	0.0	\N	NONE
964	964	\N	14.0	0.0	\N	NONE
965	965	\N	15.0	0.0	\N	NONE
966	966	\N	16.0	0.0	\N	NONE
967	967	\N	17.0	0.0	\N	NONE
968	968	\N	18.0	0.0	\N	NONE
969	969	\N	19.0	0.0	\N	NONE
970	970	\N	1.0	0.0	\N	NONE
971	971	\N	2.0	0.0	\N	NONE
972	972	\N	3.0	0.0	\N	NONE
973	973	\N	4.0	0.0	\N	NONE
974	974	\N	5.0	0.0	\N	NONE
975	975	\N	6.0	0.0	\N	NONE
976	976	\N	7.0	0.0	\N	NONE
977	977	\N	8.0	0.0	\N	NONE
978	978	\N	9.0	0.0	\N	NONE
979	979	\N	10.0	0.0	\N	NONE
980	980	\N	11.0	0.0	\N	NONE
981	981	\N	12.0	0.0	\N	NONE
982	982	\N	13.0	0.0	\N	NONE
983	983	\N	14.0	0.0	\N	NONE
984	984	\N	15.0	0.0	\N	NONE
985	985	\N	16.0	0.0	\N	NONE
986	986	\N	17.0	0.0	\N	NONE
987	987	\N	18.0	0.0	\N	NONE
988	988	\N	19.0	0.0	\N	NONE
989	989	\N	1.0	0.0	\N	NONE
990	990	\N	2.0	0.0	\N	NONE
991	991	\N	3.0	0.0	\N	NONE
992	992	\N	4.0	0.0	\N	NONE
993	993	\N	5.0	0.0	\N	NONE
994	994	\N	6.0	0.0	\N	NONE
995	995	\N	7.0	0.0	\N	NONE
996	996	\N	8.0	0.0	\N	NONE
997	997	\N	9.0	0.0	\N	NONE
998	998	\N	10.0	0.0	\N	NONE
999	999	\N	11.0	0.0	\N	NONE
1000	1000	\N	12.0	0.0	\N	NONE
1001	1001	\N	13.0	0.0	\N	NONE
1002	1002	\N	14.0	0.0	\N	NONE
1003	1003	\N	15.0	0.0	\N	NONE
1004	1004	\N	16.0	0.0	\N	NONE
1005	1005	\N	17.0	0.0	\N	NONE
1006	1006	\N	18.0	0.0	\N	NONE
1007	1007	\N	19.0	0.0	\N	NONE
1008	1008	\N	1.0	0.0	\N	NONE
1009	1009	\N	2.0	0.0	\N	NONE
1010	1010	\N	3.0	0.0	\N	NONE
1011	1011	\N	4.0	0.0	\N	NONE
1012	1012	\N	5.0	0.0	\N	NONE
1013	1013	\N	6.0	0.0	\N	NONE
1014	1014	\N	7.0	0.0	\N	NONE
1015	1015	\N	8.0	0.0	\N	NONE
1016	1016	\N	9.0	0.0	\N	NONE
1017	1017	\N	10.0	0.0	\N	NONE
1018	1018	\N	11.0	0.0	\N	NONE
1019	1019	\N	12.0	0.0	\N	NONE
1020	1020	\N	13.0	0.0	\N	NONE
1021	1021	\N	14.0	0.0	\N	NONE
1022	1022	\N	15.0	0.0	\N	NONE
1023	1023	\N	16.0	0.0	\N	NONE
1024	1024	\N	17.0	0.0	\N	NONE
1025	1025	\N	18.0	0.0	\N	NONE
1026	1026	\N	19.0	0.0	\N	NONE
1027	1027	\N	1.0	0.0	\N	NONE
1028	1028	\N	2.0	0.0	\N	NONE
1029	1029	\N	3.0	0.0	\N	NONE
1030	1030	\N	4.0	0.0	\N	NONE
1031	1031	\N	5.0	0.0	\N	NONE
1032	1032	\N	6.0	0.0	\N	NONE
1033	1033	\N	7.0	0.0	\N	NONE
1034	1034	\N	8.0	0.0	\N	NONE
1035	1035	\N	9.0	0.0	\N	NONE
1036	640	\N	10.0	0.0	\N	NONE
1037	1037	\N	11.0	0.0	\N	NONE
1038	1038	\N	12.0	0.0	\N	NONE
1039	1039	\N	13.0	0.0	\N	NONE
1040	1040	\N	14.0	0.0	\N	NONE
1041	1041	\N	15.0	0.0	\N	NONE
1042	1042	\N	16.0	0.0	\N	NONE
1043	1043	\N	17.0	0.0	\N	NONE
1044	1044	\N	18.0	0.0	\N	NONE
1045	1045	\N	19.0	0.0	\N	NONE
1046	1046	\N	1.0	0.0	\N	NONE
1047	1047	\N	2.0	0.0	\N	NONE
1048	1048	\N	3.0	0.0	\N	NONE
1049	1049	\N	4.0	0.0	\N	NONE
1050	1050	\N	5.0	0.0	\N	NONE
1051	1051	\N	6.0	0.0	\N	NONE
1052	1052	\N	7.0	0.0	\N	NONE
1053	1053	\N	8.0	0.0	\N	NONE
1054	1054	\N	9.0	0.0	\N	NONE
1055	1055	\N	10.0	0.0	\N	NONE
1056	1056	\N	11.0	0.0	\N	NONE
1057	1057	\N	12.0	0.0	\N	NONE
1058	1058	\N	13.0	0.0	\N	NONE
1059	1059	\N	14.0	0.0	\N	NONE
1060	1060	\N	15.0	0.0	\N	NONE
1061	1061	\N	16.0	0.0	\N	NONE
1062	1062	\N	17.0	0.0	\N	NONE
1063	1063	\N	18.0	0.0	\N	NONE
1064	1064	\N	19.0	0.0	\N	NONE
1065	1065	\N	1.0	0.0	\N	NONE
1066	1066	\N	2.0	0.0	\N	NONE
1067	1067	\N	3.0	0.0	\N	NONE
1068	1068	\N	4.0	0.0	\N	NONE
1069	1069	\N	5.0	0.0	\N	NONE
1070	1070	\N	6.0	0.0	\N	NONE
1071	1071	\N	7.0	0.0	\N	NONE
1072	1072	\N	8.0	0.0	\N	NONE
1073	1073	\N	9.0	0.0	\N	NONE
1074	1064	\N	10.0	0.0	\N	NONE
1075	1075	\N	11.0	0.0	\N	NONE
1076	1076	\N	12.0	0.0	\N	NONE
1077	1077	\N	13.0	0.0	\N	NONE
1078	1078	\N	14.0	0.0	\N	NONE
1079	1079	\N	15.0	0.0	\N	NONE
1080	1080	\N	16.0	0.0	\N	NONE
1081	1081	\N	17.0	0.0	\N	NONE
1082	1082	\N	18.0	0.0	\N	NONE
1083	1083	\N	19.0	0.0	\N	NONE
1084	1084	\N	1.0	0.0	\N	NONE
1085	1085	\N	2.0	0.0	\N	NONE
1086	1086	\N	3.0	0.0	\N	NONE
1087	1087	\N	4.0	0.0	\N	NONE
1088	1088	\N	5.0	0.0	\N	NONE
1089	1089	\N	6.0	0.0	\N	NONE
1090	1090	\N	7.0	0.0	\N	NONE
1091	1091	\N	8.0	0.0	\N	NONE
1092	1092	\N	9.0	0.0	\N	NONE
1093	1093	\N	10.0	0.0	\N	NONE
1094	1094	\N	11.0	0.0	\N	NONE
1095	1095	\N	12.0	0.0	\N	NONE
1096	1096	\N	13.0	0.0	\N	NONE
1097	1097	\N	14.0	0.0	\N	NONE
1098	1098	\N	15.0	0.0	\N	NONE
1099	1099	\N	16.0	0.0	\N	NONE
1100	1100	\N	17.0	0.0	\N	NONE
1101	1101	\N	18.0	0.0	\N	NONE
1102	1102	\N	19.0	0.0	\N	NONE
1103	1103	\N	1.0	0.0	\N	NONE
1104	1104	\N	2.0	0.0	\N	NONE
1105	1105	\N	3.0	0.0	\N	NONE
1106	1106	\N	4.0	0.0	\N	NONE
1107	1107	\N	5.0	0.0	\N	NONE
1108	1108	\N	6.0	0.0	\N	NONE
1109	1109	\N	7.0	0.0	\N	NONE
1110	1110	\N	8.0	0.0	\N	NONE
1111	1111	\N	9.0	0.0	\N	NONE
1112	1112	\N	10.0	0.0	\N	NONE
1113	1113	\N	11.0	0.0	\N	NONE
1114	1114	\N	12.0	0.0	\N	NONE
1115	1115	\N	13.0	0.0	\N	NONE
1116	1116	\N	14.0	0.0	\N	NONE
1117	1117	\N	15.0	0.0	\N	NONE
1118	1118	\N	16.0	0.0	\N	NONE
1119	1119	\N	17.0	0.0	\N	NONE
1120	1120	\N	18.0	0.0	\N	NONE
1121	1121	\N	19.0	0.0	\N	NONE
1122	1122	\N	1.0	0.0	\N	NONE
1123	1123	\N	2.0	0.0	\N	NONE
1124	1124	\N	3.0	0.0	\N	NONE
1125	1125	\N	4.0	0.0	\N	NONE
1126	1126	\N	5.0	0.0	\N	NONE
1127	1127	\N	6.0	0.0	\N	NONE
1128	1128	\N	7.0	0.0	\N	NONE
1129	1129	\N	8.0	0.0	\N	NONE
1130	1130	\N	9.0	0.0	\N	NONE
1131	1131	\N	10.0	0.0	\N	NONE
1132	1132	\N	11.0	0.0	\N	NONE
1133	1133	\N	12.0	0.0	\N	NONE
1134	1134	\N	13.0	0.0	\N	NONE
1135	1135	\N	14.0	0.0	\N	NONE
1136	1136	\N	15.0	0.0	\N	NONE
1137	1137	\N	16.0	0.0	\N	NONE
1138	1138	\N	17.0	0.0	\N	NONE
1139	1139	\N	18.0	0.0	\N	NONE
1140	1140	\N	19.0	0.0	\N	NONE
1141	1141	\N	1.0	0.0	\N	NONE
1142	1142	\N	2.0	0.0	\N	NONE
1143	1143	\N	3.0	0.0	\N	NONE
1144	1144	\N	4.0	0.0	\N	NONE
1145	1145	\N	5.0	0.0	\N	NONE
1146	1146	\N	6.0	0.0	\N	NONE
1147	1147	\N	7.0	0.0	\N	NONE
1148	1148	\N	8.0	0.0	\N	NONE
1149	1149	\N	9.0	0.0	\N	NONE
1150	1150	\N	10.0	0.0	\N	NONE
1151	1151	\N	11.0	0.0	\N	NONE
1152	1152	\N	12.0	0.0	\N	NONE
1153	1153	\N	13.0	0.0	\N	NONE
1154	1154	\N	14.0	0.0	\N	NONE
1155	1155	\N	15.0	0.0	\N	NONE
1156	1156	\N	16.0	0.0	\N	NONE
1157	1157	\N	17.0	0.0	\N	NONE
1158	1158	\N	18.0	0.0	\N	NONE
1159	1159	\N	19.0	0.0	\N	NONE
1160	1160	\N	1.0	0.0	\N	NONE
1161	1161	\N	2.0	0.0	\N	NONE
1162	1162	\N	3.0	0.0	\N	NONE
1163	1163	\N	4.0	0.0	\N	NONE
1164	1164	\N	5.0	0.0	\N	NONE
1165	1165	\N	6.0	0.0	\N	NONE
1166	1166	\N	7.0	0.0	\N	NONE
1167	1167	\N	8.0	0.0	\N	NONE
1168	1168	\N	9.0	0.0	\N	NONE
1169	1169	\N	10.0	0.0	\N	NONE
1170	1170	\N	11.0	0.0	\N	NONE
1171	1171	\N	12.0	0.0	\N	NONE
1172	1172	\N	13.0	0.0	\N	NONE
1173	1173	\N	14.0	0.0	\N	NONE
1174	1174	\N	15.0	0.0	\N	NONE
1175	1175	\N	16.0	0.0	\N	NONE
1176	1176	\N	17.0	0.0	\N	NONE
1177	1177	\N	18.0	0.0	\N	NONE
1178	1178	\N	19.0	0.0	\N	NONE
1179	1179	\N	1.0	0.0	\N	NONE
1180	1180	\N	2.0	0.0	\N	NONE
1181	1181	\N	3.0	0.0	\N	NONE
1182	1182	\N	4.0	0.0	\N	NONE
1183	1183	\N	5.0	0.0	\N	NONE
1184	1184	\N	6.0	0.0	\N	NONE
1185	1185	\N	7.0	0.0	\N	NONE
1186	1186	\N	8.0	0.0	\N	NONE
1187	1187	\N	9.0	0.0	\N	NONE
1188	1188	\N	10.0	0.0	\N	NONE
1189	1189	\N	11.0	0.0	\N	NONE
1190	1190	\N	12.0	0.0	\N	NONE
1191	1191	\N	13.0	0.0	\N	NONE
1192	1192	\N	14.0	0.0	\N	NONE
1193	1193	\N	15.0	0.0	\N	NONE
1194	1194	\N	16.0	0.0	\N	NONE
1195	1195	\N	17.0	0.0	\N	NONE
1196	1196	\N	18.0	0.0	\N	NONE
1197	1197	\N	19.0	0.0	\N	NONE
1198	1198	\N	1.0	0.0	\N	NONE
1199	1199	\N	2.0	0.0	\N	NONE
1200	1200	\N	3.0	0.0	\N	NONE
1201	1201	\N	4.0	0.0	\N	NONE
1202	1202	\N	5.0	0.0	\N	NONE
1203	1203	\N	6.0	0.0	\N	NONE
1204	1204	\N	7.0	0.0	\N	NONE
1205	1205	\N	8.0	0.0	\N	NONE
1206	1206	\N	9.0	0.0	\N	NONE
2	2	3	25.0	25.0	2018-05-01 10:55:02	NONE
1209	1209	\N	3.0	0.0	\N	NONE
1212	1212	\N	12.0	0.0	\N	NONE
1213	1213	\N	3.0	0.0	\N	NONE
1	1	3	1.0	1.0	2018-05-20 19:07:29	PALTETA UNO
336	336	3	13.0	13.0	2018-05-27 15:44:01	NONE
\.


--
-- Data for Name: items_processes_times; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_processes_times (ipt_id, company_id, item_id, state_id, user_id, processed_amount, process_time, process_time_date, project_numbers) FROM stdin;
\.


--
-- Data for Name: items_with_filters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items_with_filters (iwf_id, item_id, filter_id, filter_group_id) FROM stdin;
1	1	3	2
\.


--
-- Data for Name: offers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offers (offer_id, offer_number, company_id, client_id, client_contact_id, inquiry_id, offer_creation_date, offer_expiration_date, payment_method, payment_conditions, user_id) FROM stdin;
1	123456	3UGMSKKJBY	2	4	\N	2018-05-05 08:36:45	\N	\N	\N	\N
3	333333333	3UGMSKKJBY	3	9	\N	2018-05-06 06:03:19	\N	\N	\N	\N
2	20182222555555555555	3UGMSKKJBY	2	3	\N	2018-05-05 08:37:41	\N	\N	\N	3
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (order_id, company_id, order_number, project_numbers, order_creation_date, provider_id, order_state, order_status, user_id, order_delivery_date, pc_id, offer_number) FROM stdin;
4	3UGMSKKJBY	QWERTGFYHRGTFR	{PROALF1111}	2018-02-25 11:00:53	7	0	0	2	2018-03-27 22:00:00	1	12345678
15	3UGMSKKJBY	1111111111	{2,3}	2018-05-05 05:04:19	8	0	3	1	2018-05-16 22:00:00	5	00000111111
17	3UGMSKKJBY	333333333333	{7,10}	2018-05-05 05:07:44	5	1	0	1	2018-05-16 22:00:00	4	3333333333333
2	3UGMSKKJBY	AB7425	{2,7}	2018-02-25 10:55:13	5	1	0	2	2018-02-26 23:00:00	6	00000
\.


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.projects (project_id, company_id, project_name, project_number, project_state, project_type, project_client, project_creation_date, project_deadline, user_id) FROM stdin;
3	3UGMSKKJBY	PROJECT ALFA	PROALF1111	1	PERSONAL	INIX_juanJose	2018-04-18 15:31:47	2018-03-02 23:00:00	\N
4	3UGMSKKJBY	MANCHESTER UNITED	1218288	1	PEROSNAL	DRFGBH	2018-04-18 15:33:36	2018-02-26 23:00:00	\N
7	3UGMSKKJBY	name name	test now	1	joda	loco	2018-04-18 16:36:37	2018-04-19 22:00:00	\N
10	3UGMSKKJBY	22222222222	11111111111111	1	333333333333	4444444444	2018-04-18 16:41:52	2018-04-26 22:00:00	\N
12	3UGMSKKJBY	Que vaina	Barranquilla Nojoda	1	pimo	joe	2018-04-18 16:46:46	2018-04-25 22:00:00	\N
8	3UGMSKKJBY	uno	ahora	0	dos	tres	2018-04-18 16:39:38	2018-04-23 22:00:00	\N
5	3UGMSKKJBY	PROJECT BETA	nuevo nojoda	0	PERSONAL	INIX	2018-04-18 15:35:00	2018-03-29 22:00:00	\N
11	3UGMSKKJBY	5555555555	444444444	0	666666666666	7777777777	2018-04-18 16:42:51	2018-04-24 22:00:00	\N
9	3UGMSKKJBY	fecd	qwert	0	eddd	deeded	2018-04-18 16:41:04	2018-04-18 22:00:00	\N
6	3UGMSKKJBY	Nojoda loco	el nuevo	0	Perosnal	El loco	2018-04-18 15:48:20	2018-04-25 22:00:00	\N
2	3UGMSKKJBY	MANCHESTER	12345678	1	PEROSNAL	DRFGBH	2018-02-25 09:01:56	2018-05-09 22:00:00	\N
1	3UGMSKKJBY	CM490_ASDF	1812288	1	maquinaria	INIX	2018-02-25 08:07:40	2018-03-15 23:00:00	\N
13	3UGMSKKJBY	MODEL S	nuevo test	1	AUTONOMUS	elon	2018-05-25 16:56:30	2018-05-22 22:00:00	\N
14	3UGMSKKJBY	Joda mijo	hoy test	1	PEROSNAL	INIX_juanJose	2018-05-25 17:59:55	2018-05-23 22:00:00	\N
\.


--
-- Data for Name: provider_contacts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.provider_contacts (pc_id, provider_id, contact_name, contact_last_name, contact_telephone, contact_email) FROM stdin;
1	7	Peter	La anguila	123456789	Perter@laanguila.com
2	6	pepe	guardiola	865325636	pep@shity.com
3	6	Zlatan	Ibra	12345678	y@ibra.com
4	5	Juancho	Polo	123456	juan@joda.com
5	8	Jose	Mou	3333333333	jose@mou.com
7	9	test contact provider	Nojoda Loco	todo bien?	pleno@joda.com
6	5	Joda loco vamos	cinco	444444555555	cuantro@cinco.com
\.


--
-- Data for Name: providers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.providers (provider_id, company_id, provider_name, provider_address, provider_telephone) FROM stdin;
5	3UGMSKKJBY	ICE GMBH	hfhfjdk3	+48789452
6	3UGMSKKJBY	FESTO  GMBH	qwer12345	+49123456741
7	3UGMSKKJBY	SNOW GMBH	jhrhe4847	+49789456123
8	3UGMSKKJBY	SMC	Joda	1234567890
9	3UGMSKKJBY	HOOK	Germany	12121212121
\.


--
-- Data for Name: states; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.states (state_id, company_id, state_name, state_description) FROM stdin;
1	LaBitacora	BOOKED	\N
2	LaBitacora	FINISHED	\N
3	LaBitacora	ASSEMBLED	\N
4	3UGMSKKJBY	SOLDADO	JUNTAR DOS MATERIALES CON SOLDADURA
\.


--
-- Data for Name: sub_assemblies_in_assemblies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sub_assemblies_in_assemblies (saia_id, sub_assembly_id, assembly_id) FROM stdin;
1	4	3
2	4	3
3	5	3
4	5	3
5	5	3
\.


--
-- Data for Name: sub_assemblies_in_projects; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sub_assemblies_in_projects (saip_id, project_id, sub_assembly_id, assembly_id, aip_id, state_id, state_date, user_id) FROM stdin;
1	1	4	2	2	1	2018-02-25 09:08:30	1
2	1	4	2	2	1	2018-02-25 09:08:30	1
3	1	5	2	2	1	2018-02-25 09:08:30	1
4	1	4	1	1	1	2018-02-25 09:52:47	1
5	1	4	1	1	1	2018-02-25 09:52:47	1
6	1	5	1	1	1	2018-02-25 09:52:47	1
7	1	5	1	1	1	2018-02-25 09:52:47	1
8	1	5	1	1	1	2018-02-25 09:52:47	1
9	1	4	1	1	1	2018-02-25 09:53:55	1
10	1	4	1	1	1	2018-02-25 09:53:55	1
11	1	5	1	1	1	2018-02-25 09:53:55	1
12	1	4	3	3	1	2018-02-25 09:55:13	1
13	1	5	3	3	1	2018-02-25 09:55:13	1
14	1	5	3	3	1	2018-02-25 09:55:13	1
15	1	5	3	3	1	2018-02-25 09:55:13	1
16	1	5	3	3	1	2018-02-25 09:55:13	1
17	1	5	3	3	1	2018-02-25 09:55:13	1
18	1	5	2	2	1	2018-02-25 09:57:46	1
19	1	5	2	2	1	2018-02-25 09:57:46	1
20	2	5	1	4	1	2018-02-25 10:02:40	1
21	2	5	1	4	1	2018-02-25 10:02:40	1
22	2	4	1	4	1	2018-02-25 13:46:16	2
23	2	4	1	4	1	2018-02-25 13:46:16	2
24	2	4	1	4	1	2018-02-25 13:46:16	2
25	2	6	1	4	1	2018-02-25 13:46:16	2
26	2	6	1	4	1	2018-02-25 13:46:16	2
27	2	6	1	4	1	2018-02-25 13:46:16	2
28	2	6	1	4	1	2018-02-25 13:46:16	2
29	3	4	3	29	1	2018-05-02 17:13:16	\N
30	3	4	3	29	1	2018-05-02 17:13:16	\N
31	3	5	3	29	1	2018-05-02 17:13:16	\N
32	3	5	3	29	1	2018-05-02 17:13:16	\N
33	3	5	3	29	1	2018-05-02 17:13:16	\N
34	6	4	3	33	1	2018-05-02 17:57:20	\N
35	6	4	3	33	1	2018-05-02 17:57:20	\N
36	6	5	3	33	1	2018-05-02 17:57:20	\N
37	6	5	3	33	1	2018-05-02 17:57:20	\N
38	6	5	3	33	1	2018-05-02 17:57:20	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (user_id, company_id, user_name, user_last_name, user_email, user_selected_language, user_creation_date, user_active, salt, hash, user_telephone) FROM stdin;
1	3UGMSKKJBY	Norbert	Rohde	ROHDE@ROHDE-MASCHINENBAU.DE	de	2018-02-25 08:55:04	t	8bcfcbf5fa2205eec6735e64bd03fc570e00172398ebc722408f81731c8ab1d9	98717496e0408c364fbea16635d325f505b039328cdd4af0d4c7f6389ef6037cdde658e0ed63e55021db945248222da854ac36692fdcdf86f93a11eaab16f0203e59a79e384ad182a4171051a5eb1e110b39d5f71feb780a0929092561c648f045ef2571419a99148226dbf1e9c34be49c5cbc904bb769f85eb5776fd9b248e3	\N
3	3UGMSKKJBY	Jose	Mourinho	JOSE@MOU.COM	en	2018-02-25 10:17:15	t	fb99a549b9685bd09ff15fe36477430d5788dc2e696f088c34c648dedb22cd38	c078c21b996b11338b5dd07d7045d9b8108a48c4d2c248249180aed04d522dd6807bb94d9d947d37a50ab34bba570fd8e2b65013c2ba952e25c937e6d5bb2e9361106d84bdf2bd3f39b9f83d7fffd813097c2eca51175c29f4a1843c39f4bbb6d925cfb5e419f41eb5727cdc7f31be0329b8ca232b589f02f76eabc1545b2541	+784592484
2	3UGMSKKJBY	Juan	Garcia	JUAN@GARCIA.COM	de	2018-02-25 07:55:35	t	153866e71ef0c7b781c1d75ed3e93d4eecedc974f4c5fa5f9313e043dc44a9e7	75e11bdfe457adb9e45bf4ecde147e7e5c6ccaf63192fcd0c6ac962d9d1e4efdac138ad8638992bf35919d9bc023db41498b2163342f9456b9be9f7bd69b47b0d93dee117842cbfa71e3b093de4f5c2f5756a4b4e8f471dfb91739eb8bb2bafa1189b0bc5c5fe86228f5291a4879074d81bce7b5b72c4f2338d95ac195f9a657	3642440
4	3UGMSKKJBY	uno	dos	U@U.COM	en	2018-05-26 13:43:54	t	5d607778bcd182b76803dfa67dca8db65286353519cbf1047e8b3a9a4ea898e1	bcc268368cdde878381fac02bf04b7fa4d107ab4fb55c244d8ad709e9e4773fcc79bc56a3db39bddbb2ef2875c4e1fb9dccbef3b80785d32a5df12a54255ebc69627d81fea8dafccec2b6d6e74eadc330a526cc2834fd38aad3184b663f8972141a1f7d053be0b56fea4e183bf7df2bafd64610fa195193abeb3259ef30919e6	3
5	3UGMSKKJBY	tres	cuatro	HSD@MONDA.COM	en	2018-05-26 13:53:19	t	a8babd4c2d6a0757ccfa993d14bd628cdf89f95adc64d307b719fce9116502ea	a7ee7ea7e97fe04f45d470d766c9ac7b7e7b42134070a787c3e5162c933369fc519eb737a645f3fa34302b50a039cee751f4781aeba28688affccd5023c96948a78208f2841377599923cf05dfafb968d428254376aed5331978bf3f7c9392fac543c919c1f3a49ef6faf10c4069c10c69e3ef2cd37d6e71b2f771455de3e814	533333
\.


--
-- Name: assemblies_assembly_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assemblies_assembly_id_seq', 26, true);


--
-- Name: assemblies_in_projects_aip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assemblies_in_projects_aip_id_seq', 39, true);


--
-- Name: client_contacts_cc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_contacts_cc_id_seq', 9, true);


--
-- Name: clients_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clients_client_id_seq', 5, true);


--
-- Name: columns_names_column_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.columns_names_column_id_seq', 18, true);


--
-- Name: companies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.companies_id_seq', 2, true);


--
-- Name: customized_columns_names_ccn_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customized_columns_names_ccn_id_seq', 19, true);


--
-- Name: filter_groups_filter_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filter_groups_filter_group_id_seq', 4, true);


--
-- Name: filter_values_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.filter_values_filter_id_seq', 10, true);


--
-- Name: items_in_assemblies_iia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_assemblies_iia_id_seq', 83, true);


--
-- Name: items_in_offers_iioffer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_offers_iioffer_id_seq', 15, true);


--
-- Name: items_in_orders_iio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_orders_iio_id_seq', 232, true);


--
-- Name: items_in_projects_iip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_projects_iip_id_seq', 254, true);


--
-- Name: items_in_projects_states_iips_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_projects_states_iips_id_seq', 248, true);


--
-- Name: items_in_stock_iis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_in_stock_iis_id_seq', 1215, true);


--
-- Name: items_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_item_id_seq', 1215, true);


--
-- Name: items_processes_times_ipt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_processes_times_ipt_id_seq', 1, false);


--
-- Name: items_with_filters_iwf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_with_filters_iwf_id_seq', 1, true);


--
-- Name: offers_offer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.offers_offer_id_seq', 6, true);


--
-- Name: orders_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_order_id_seq', 24, true);


--
-- Name: projects_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.projects_project_id_seq', 14, true);


--
-- Name: provider_contacts_pc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.provider_contacts_pc_id_seq', 7, true);


--
-- Name: providers_provider_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.providers_provider_id_seq', 10, true);


--
-- Name: states_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.states_state_id_seq', 4, true);


--
-- Name: sub_assemblies_in_assemblies_saia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sub_assemblies_in_assemblies_saia_id_seq', 5, true);


--
-- Name: sub_assemblies_in_projects_saip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sub_assemblies_in_projects_saip_id_seq', 38, true);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_user_id_seq', 5, true);


--
-- Name: assemblies_in_projects assemblies_in_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies_in_projects
    ADD CONSTRAINT assemblies_in_projects_pkey PRIMARY KEY (aip_id);


--
-- Name: assemblies assemblies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies
    ADD CONSTRAINT assemblies_pkey PRIMARY KEY (assembly_id);


--
-- Name: client_contacts client_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_contacts
    ADD CONSTRAINT client_contacts_pkey PRIMARY KEY (cc_id);


--
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (client_id);


--
-- Name: columns_names columns_names_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.columns_names
    ADD CONSTRAINT columns_names_pkey PRIMARY KEY (column_id);


--
-- Name: companies companies_company_tax_payer_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_company_tax_payer_id_key UNIQUE (company_tax_payer_id);


--
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (company_id);


--
-- Name: customized_columns_names customized_columns_names_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customized_columns_names
    ADD CONSTRAINT customized_columns_names_pkey PRIMARY KEY (ccn_id);


--
-- Name: filter_groups filter_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_groups
    ADD CONSTRAINT filter_groups_pkey PRIMARY KEY (filter_group_id);


--
-- Name: filter_values filter_values_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_values
    ADD CONSTRAINT filter_values_pkey PRIMARY KEY (filter_id);


--
-- Name: items_in_assemblies items_in_assemblies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_assemblies
    ADD CONSTRAINT items_in_assemblies_pkey PRIMARY KEY (iia_id);


--
-- Name: items_in_offers items_in_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_offers
    ADD CONSTRAINT items_in_offers_pkey PRIMARY KEY (iioffer_id);


--
-- Name: items_in_orders items_in_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_orders
    ADD CONSTRAINT items_in_orders_pkey PRIMARY KEY (iio_id);


--
-- Name: items_in_projects items_in_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_pkey PRIMARY KEY (iip_id);


--
-- Name: items_in_projects_states items_in_projects_states_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects_states
    ADD CONSTRAINT items_in_projects_states_pkey PRIMARY KEY (iips_id);


--
-- Name: items_in_stock items_in_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_stock
    ADD CONSTRAINT items_in_stock_pkey PRIMARY KEY (iis_id);


--
-- Name: items items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (item_id);


--
-- Name: items_processes_times items_processes_times_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times
    ADD CONSTRAINT items_processes_times_pkey PRIMARY KEY (ipt_id);


--
-- Name: items_with_filters items_with_filters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_with_filters
    ADD CONSTRAINT items_with_filters_pkey PRIMARY KEY (iwf_id);


--
-- Name: offers offers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (offer_id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (order_id);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (project_id);


--
-- Name: provider_contacts provider_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_contacts
    ADD CONSTRAINT provider_contacts_pkey PRIMARY KEY (pc_id);


--
-- Name: providers providers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.providers
    ADD CONSTRAINT providers_pkey PRIMARY KEY (provider_id);


--
-- Name: states states_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states
    ADD CONSTRAINT states_pkey PRIMARY KEY (state_id);


--
-- Name: sub_assemblies_in_assemblies sub_assemblies_in_assemblies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_assemblies
    ADD CONSTRAINT sub_assemblies_in_assemblies_pkey PRIMARY KEY (saia_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_pkey PRIMARY KEY (saip_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: users users_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_email_key UNIQUE (user_email);


--
-- Name: assemblies assemblies_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies
    ADD CONSTRAINT assemblies_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: assemblies_in_projects assemblies_in_projects_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies_in_projects
    ADD CONSTRAINT assemblies_in_projects_assembly_id_fkey FOREIGN KEY (assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: assemblies_in_projects assemblies_in_projects_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assemblies_in_projects
    ADD CONSTRAINT assemblies_in_projects_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.projects(project_id);


--
-- Name: client_contacts client_contacts_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_contacts
    ADD CONSTRAINT client_contacts_client_id_fkey FOREIGN KEY (client_id) REFERENCES public.clients(client_id);


--
-- Name: clients clients_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: customized_columns_names customized_columns_names_column_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customized_columns_names
    ADD CONSTRAINT customized_columns_names_column_id_fkey FOREIGN KEY (column_id) REFERENCES public.columns_names(column_id);


--
-- Name: customized_columns_names customized_columns_names_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customized_columns_names
    ADD CONSTRAINT customized_columns_names_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: filter_groups filter_groups_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_groups
    ADD CONSTRAINT filter_groups_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: filter_values filter_values_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_values
    ADD CONSTRAINT filter_values_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: filter_values filter_values_filter_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.filter_values
    ADD CONSTRAINT filter_values_filter_group_id_fkey FOREIGN KEY (filter_group_id) REFERENCES public.filter_groups(filter_group_id);


--
-- Name: items items_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: items_in_assemblies items_in_assemblies_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_assemblies
    ADD CONSTRAINT items_in_assemblies_assembly_id_fkey FOREIGN KEY (assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: items_in_assemblies items_in_assemblies_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_assemblies
    ADD CONSTRAINT items_in_assemblies_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_in_offers items_in_offers_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_offers
    ADD CONSTRAINT items_in_offers_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_in_offers items_in_offers_offer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_offers
    ADD CONSTRAINT items_in_offers_offer_id_fkey FOREIGN KEY (offer_id) REFERENCES public.offers(offer_id);


--
-- Name: items_in_offers items_in_offers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_offers
    ADD CONSTRAINT items_in_offers_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_in_orders items_in_orders_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_orders
    ADD CONSTRAINT items_in_orders_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_in_orders items_in_orders_order_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_orders
    ADD CONSTRAINT items_in_orders_order_id_fkey FOREIGN KEY (order_id) REFERENCES public.orders(order_id);


--
-- Name: items_in_orders items_in_orders_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_orders
    ADD CONSTRAINT items_in_orders_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_in_projects items_in_projects_aip_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_aip_id_fkey FOREIGN KEY (aip_id) REFERENCES public.assemblies_in_projects(aip_id);


--
-- Name: items_in_projects items_in_projects_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_assembly_id_fkey FOREIGN KEY (assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: items_in_projects items_in_projects_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_in_projects items_in_projects_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.projects(project_id);


--
-- Name: items_in_projects items_in_projects_saip_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_saip_id_fkey FOREIGN KEY (saip_id) REFERENCES public.sub_assemblies_in_projects(saip_id);


--
-- Name: items_in_projects items_in_projects_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_state_id_fkey FOREIGN KEY (state_id) REFERENCES public.states(state_id);


--
-- Name: items_in_projects_states items_in_projects_states_iip_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects_states
    ADD CONSTRAINT items_in_projects_states_iip_id_fkey FOREIGN KEY (iip_id) REFERENCES public.items_in_projects(iip_id);


--
-- Name: items_in_projects_states items_in_projects_states_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects_states
    ADD CONSTRAINT items_in_projects_states_state_id_fkey FOREIGN KEY (state_id) REFERENCES public.states(state_id);


--
-- Name: items_in_projects_states items_in_projects_states_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects_states
    ADD CONSTRAINT items_in_projects_states_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_in_projects items_in_projects_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_projects
    ADD CONSTRAINT items_in_projects_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_in_stock items_in_stock_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_stock
    ADD CONSTRAINT items_in_stock_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_in_stock items_in_stock_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_in_stock
    ADD CONSTRAINT items_in_stock_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_processes_times items_processes_times_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times
    ADD CONSTRAINT items_processes_times_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: items_processes_times items_processes_times_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times
    ADD CONSTRAINT items_processes_times_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: items_processes_times items_processes_times_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times
    ADD CONSTRAINT items_processes_times_state_id_fkey FOREIGN KEY (state_id) REFERENCES public.states(state_id);


--
-- Name: items_processes_times items_processes_times_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_processes_times
    ADD CONSTRAINT items_processes_times_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: items_with_filters items_with_filters_filter_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_with_filters
    ADD CONSTRAINT items_with_filters_filter_group_id_fkey FOREIGN KEY (filter_group_id) REFERENCES public.filter_groups(filter_group_id);


--
-- Name: items_with_filters items_with_filters_filter_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_with_filters
    ADD CONSTRAINT items_with_filters_filter_id_fkey FOREIGN KEY (filter_id) REFERENCES public.filter_values(filter_id);


--
-- Name: items_with_filters items_with_filters_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items_with_filters
    ADD CONSTRAINT items_with_filters_item_id_fkey FOREIGN KEY (item_id) REFERENCES public.items(item_id);


--
-- Name: offers offers_client_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_client_contact_id_fkey FOREIGN KEY (client_contact_id) REFERENCES public.client_contacts(cc_id);


--
-- Name: offers offers_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_client_id_fkey FOREIGN KEY (client_id) REFERENCES public.clients(client_id);


--
-- Name: offers offers_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: offers offers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: orders orders_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: orders orders_pc_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pc_id_fkey FOREIGN KEY (pc_id) REFERENCES public.provider_contacts(pc_id);


--
-- Name: orders orders_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_provider_id_fkey FOREIGN KEY (provider_id) REFERENCES public.filter_values(filter_id);


--
-- Name: orders orders_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: projects projects_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: projects projects_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: provider_contacts provider_contacts_provider_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.provider_contacts
    ADD CONSTRAINT provider_contacts_provider_id_fkey FOREIGN KEY (provider_id) REFERENCES public.providers(provider_id);


--
-- Name: providers providers_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.providers
    ADD CONSTRAINT providers_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: states states_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.states
    ADD CONSTRAINT states_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- Name: sub_assemblies_in_assemblies sub_assemblies_in_assemblies_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_assemblies
    ADD CONSTRAINT sub_assemblies_in_assemblies_assembly_id_fkey FOREIGN KEY (assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: sub_assemblies_in_assemblies sub_assemblies_in_assemblies_sub_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_assemblies
    ADD CONSTRAINT sub_assemblies_in_assemblies_sub_assembly_id_fkey FOREIGN KEY (sub_assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_aip_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_aip_id_fkey FOREIGN KEY (aip_id) REFERENCES public.assemblies_in_projects(aip_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_assembly_id_fkey FOREIGN KEY (assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.projects(project_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_state_id_fkey FOREIGN KEY (state_id) REFERENCES public.states(state_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_sub_assembly_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_sub_assembly_id_fkey FOREIGN KEY (sub_assembly_id) REFERENCES public.assemblies(assembly_id);


--
-- Name: sub_assemblies_in_projects sub_assemblies_in_projects_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sub_assemblies_in_projects
    ADD CONSTRAINT sub_assemblies_in_projects_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(user_id);


--
-- Name: users users_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(company_id);


--
-- PostgreSQL database dump complete
--

